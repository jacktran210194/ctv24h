<?php

namespace App\Providers;

use App\Models\CategoryProductChildModel;
use App\Models\CategoryProductModel;
use App\Models\CityModel;
use App\Models\CustomerModel;
use App\Models\OrderModel;
use App\Models\ProductModel;
use App\Models\ShippingModel;
use App\Models\SubCategoryProductChildModel;
use App\Models\TheBankModel;
use Exception;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            $data['display_category'] = CategoryProductModel::all();
            $data['display_category_child'] = CategoryProductChildModel::all();
            $data['category_product'] = SubCategoryProductChildModel::all();
            $data['shipping'] = ShippingModel::all();
            $data['list_city'] = CityModel::all();
            if (isset($data['display_category']) && isset($data['display_category_child']) && isset($data['category_product']) && $data['shipping'] && isset($data['list_city'])) {
                view()->share($data);
                Paginator::useBootstrap();

            } else {
                $data = null;
                view()->share($data);
            }
        } catch (Exception $e) {
            $data = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra !',
                'error' => $e
            ];
            view()->share($data);
        }
    }
}
