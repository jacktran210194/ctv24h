<?php

namespace App\Http\Controllers;

use App\Models\CategoryShopModel;
use App\Models\FollowShopModel;
use App\Models\LovelyShopModel;
use App\Models\ProductCategoryShopModel;
use App\Models\ProductModel;
use App\Models\ReviewProductModel;
use App\Models\ShopImageModel;
use App\Models\ShopModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SeeShopController extends Controller
{
    public function __construct(Request $request)
    {
        if (!$request->session()->has('data_customer')) {
            Redirect::to('login')->send();
        }
    }

    //Profile
    public function index($id)
    {
//        $dataCustomer = session()->get('data_customer');
        $dataInfoShop = ShopModel::find($id);
        $data_follow_shop = FollowShopModel::where('shop_id',$id)->count();
        $data_love_shop = LovelyShopModel::where('shop_id', $id)->count();
        $data_following = FollowShopModel::where('customer_id',$dataInfoShop->customer_id)->count();
        $count_review_1 = ReviewProductModel::where('shop_id',$id)->avg('ratings');
        $count_review = round($count_review_1,1);
        $dataCountProduct = ProductModel::where('shop_id', $id)->where('active', 1)->count();
        $dataSuggestToday = ProductModel::join('attribute_value', 'product.id', 'attribute_value.product_id')
            ->where('attribute_value.quantity', '!=', 0)
            ->orderBy('product.id', 'desc')->where('shop_id', $dataInfoShop->id)->where('active', 1)->get();
        if (count($dataSuggestToday)) {
            foreach ($dataSuggestToday as $key => $value) {
                $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
                $avg_rating = round($total_rating, 1);
                $dataSuggestToday[$key]->avg_rating = $avg_rating;
            }
        }
        $dataShopImage = ShopImageModel::where('shop_id', $id)->get();
        $dataCategoryShop = CategoryShopModel::select('id as cate_id', 'name as name')->where('shop_id',$id)->where('status', 1)->get();
        $dataReturn = [
            'dataInfoShop' => $dataInfoShop,
            'dataCountProduct' => $dataCountProduct,
            'dataSuggestToday' => $dataSuggestToday,
            'dataShopImage' => $dataShopImage,
            'dataCategoryShop' => $dataCategoryShop,
            'title' => $dataInfoShop->name . ', Cửa hàng trực tuyến | CTV24h',
            'count_follow' => $data_follow_shop,
            'count_love' => $data_love_shop,
            'avg_review' => $count_review ?? 0
        ];
        return view('frontend.seeshop.index', $dataReturn);
    }
    
    public function sortRelated(Request $request, $id){
        $dataInfoShop = ShopModel::find($id);
        $dataRelated = ProductModel::where('shop_id', $dataInfoShop->id)->where('active', 1)->paginate(25);
        if (count($dataRelated)) {
            foreach ($dataRelated as $key => $value) {
                $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
                $avg_rating = round($total_rating, 1);
                $dataRelated[$key]->avg_rating = $avg_rating;
            }
        }
        $dataReturn = [
            'dataRelated'=>$dataRelated,
        ];
        return view('frontend.seeshop.sort_related', $dataReturn);
    }
    public function sortNew(Request $request, $id){
        $dataInfoShop = ShopModel::find($id);
        $dataRelated = ProductModel::orderBy('id', 'desc')->where('shop_id', $dataInfoShop->id)->where('active', 1)->paginate(25);
        if (count($dataRelated)) {
            foreach ($dataRelated as $key => $value) {
                $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
                $avg_rating = round($total_rating, 1);
                $dataRelated[$key]->avg_rating = $avg_rating;
            }
        }
        $dataReturn = [
            'dataRelated'=>$dataRelated,
        ];
        return view('frontend.seeshop.sort_new', $dataReturn);
    }
    public function BestSeller(Request $request, $id){
        $dataInfoShop = ShopModel::find($id);
        $dataRelated = ProductModel::orderBy('is_sell', 'desc')
            ->where('active', 1)
            ->where('shop_id', $dataInfoShop->id)->paginate(25);
        if (count($dataRelated)) {
            foreach ($dataRelated as $key => $value) {
                $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
                $avg_rating = round($total_rating, 1);
                $dataRelated[$key]->avg_rating = $avg_rating;
            }
        }
        $dataReturn = [
            'dataRelated'=>$dataRelated,
        ];
        return view('frontend.seeshop.best_seller', $dataReturn);
    }
    public function priceASC(Request $request, $id){
        $dataInfoShop = ShopModel::find($id);
        $dataRelated = ProductModel::orderBy('price_discount', 'asc')
            ->where('active', 1)
            ->where('shop_id', $dataInfoShop->id)->paginate(25);
        if (count($dataRelated)) {
            foreach ($dataRelated as $key => $value) {
                $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
                $avg_rating = round($total_rating, 1);
                $dataRelated[$key]->avg_rating = $avg_rating;
            }
        }
        $dataReturn = [
            'dataRelated'=>$dataRelated,
        ];
        return view('frontend.seeshop.price_asc', $dataReturn);
    }

    /**
     *  Danh sách sản phẩm theo danh mục shop
     * @param $id
     * @param $cate_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function productCategory($id, $cate_id){
        $dataInfoShop = ShopModel::find($id);
        $dataCategoryShop = CategoryShopModel::find($cate_id);
        $dataProduct = ProductCategoryShopModel::where('category_id', $dataCategoryShop->id)->get();
        if($dataProduct){
            foreach ($dataProduct as $k => $v){
                $product = ProductModel::find($v->product_id);
                $dataProduct[$k]->name = $product->name ?? '';
                $dataProduct[$k]->image = $product->image ?? '';
                $dataProduct[$k]->price = $product->price ?? '';
                $dataProduct[$k]->price_discount = $product->price_discount ?? '';
                $dataProduct[$k]->price_ctv = $product->price_ctv ?? '';
                $dataProduct[$k]->is_sell = $product->is_sell ?? '';
                $dataProduct[$k]->place = $product->place ?? '';
                $total_rating = ReviewProductModel::where('product_id', $v->product_id)->avg('ratings') ?? 0;
                $avg_rating = round($total_rating, 1);
                $dataProduct[$k]->avg_rating = $avg_rating;
            }
        }
        $dataCategoryAll = CategoryShopModel::where('shop_id', $id)->where('status', 1)->get();
        $dataReturn = [
            'dataInfoShop' => $dataInfoShop,
            'dataCategoryShop' => $dataCategoryShop,
            'dataProduct' => $dataProduct,
            'categoryAll' => $dataCategoryAll,
            'title' => $dataCategoryShop->name
        ];
        return view('frontend.seeshop.filter_category', $dataReturn);
    }
}
