<?php
namespace App\Http\Controllers;

use App\Models\CustomerVoucherCtv24hModel;
use App\Models\VoucherRefundCoinModel;
use Illuminate\Http\Request;

class AddVoucherRefundCoinController extends Controller
{
    public function index(Request $request){
        $customer = session()->get('data_customer');
        $voucher_customer = CustomerVoucherCtv24hModel::where('customer_id',$customer->id)->where('type', 3)->get();
        $voucher_refund_coin = VoucherRefundCoinModel::where('id', $request->id)->first();
        if (isset($voucher_customer)){
            $check = false;
            foreach ($voucher_customer as $value){
                if ($value->voucher_id == $request->id){
                    $check = true;
                }
            }
            if ($check){
                $data_return = [
                    'status'=> false,
                    'msg'=>'mã đã tồn tại',
                ];
            }else{
                $voucher_refund_coin->quantity = $voucher_refund_coin->quantity -1;
                $voucher_refund_coin->save();
                $voucher = new  CustomerVoucherCtv24hModel([
                    'customer_id'=> $customer->id,
                    'voucher_id'=> $request->id,
                    'type' => 3
                ]);
                $voucher->save();
                $data_return = [
                    'status'=> true,
                    'msg'=>'lấy mã thành công',
                ];
            }
        }else{
            $voucher_refund_coin->quantity = $voucher_refund_coin->quantity -1;
            $voucher_refund_coin->save();
            $voucher = new  CustomerVoucherCtv24hModel([
                'customer_id'=> $customer->id,
                'voucher_id'=> $request->id,
                'type' => 3
            ]);
            $voucher->save();
            $data_return = [
                'status'=> true,
                'msg'=>'lấy mã thành công',
            ];
        }
        return $data_return;
    }
}
