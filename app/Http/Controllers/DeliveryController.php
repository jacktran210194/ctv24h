<?php

namespace App\Http\Controllers;

use App\Models\CategoryProductModel;
use App\Models\DeliveryGHTKModel;
use App\Models\JAndTModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use function PHPUnit\Framework\throwException;

class DeliveryController extends AdminBaseController
{
    public function ghtk()
    {
        $data_return['data'] = DeliveryGHTKModel::first();
        return view('admin.delivery.ghtk', $data_return);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function ghtk_save(Request $request)
    {
        try {
            $rules = [
                'api_token_key' => 'required',
            ];
            $customMessages = [
                'required' => 'Vui lòng điền đầy đủ thông tin.',
            ];
            $dataReturn = [];
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $errors = $messages->all();
                $dataReturn['status'] = false;
                $dataReturn['msg'] = $errors[0];
            } else {
                if (!empty($request->id)) {
                    $data = DeliveryGHTKModel::find($request->id);
                } else {
                    $data = new DeliveryGHTKModel();
                }
                $data->api_token_key = $request->api_token_key;
                $data->save();
                $dataReturn['status'] = true;
            }
            return response()->json($dataReturn, 200);
        } catch (\Exception $e) {
            throwException($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ghtk_connect(Request $request)
    {
        $curl = curl_init('https://services.giaohangtietkiem.vn/authentication-request-sample');

        curl_setopt_array($curl, array(
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                "Token: ".$request->api_token_key,
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $dataReturn['response'] = $response;
        return response()->json($dataReturn, 200);
    }

    public function j_and_t()
    {
        $data_return['data'] = JAndTModel::first();
        return view('admin.delivery.j_and_t', $data_return);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function j_and_t_save(Request $request)
    {
        try {
            $rules = [
                'api_token' => 'required',
            ];
            $customMessages = [
                'required' => 'Vui lòng điền đầy đủ thông tin.',
            ];
            $dataReturn = [];
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $errors = $messages->all();
                $dataReturn['status'] = false;
                $dataReturn['msg'] = $errors[0];
            } else {
                if (!empty($request->id)) {
                    $data = JAndTModel::find($request->id);
                } else {
                    $data = new JAndTModel();
                }
                $data->api_token = $request->api_token;
                $data->save();
                $dataReturn['status'] = true;
            }
            return response()->json($dataReturn, 200);
        } catch (\Exception $e) {
            throwException($e);
        }
    }
}
