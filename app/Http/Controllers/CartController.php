<?php

namespace App\Http\Controllers;

use App\Models\AddressCustomerCTVModel;
use App\Models\BuyMorePercent;
use App\Models\CartModel;
use App\Models\CityModel;
use App\Models\CoinModel;
use App\Models\CustomerAddressModel;
use App\Models\CustomerModel;
use App\Models\DistrictModel;
use App\Models\FeeCTV24HModel;
use App\Models\HistoryDealCTV24;
use App\Models\HistoryOrderPaymentCollaboratorModel;
use App\Models\HistoryOrderPaymentModel;
use App\Models\ManageCTVModel;
use App\Models\OrderCTVModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductDropShipModel;
use App\Models\ProductModel;
use App\Models\PromotionModel;
use App\Models\ShippingModel;
use App\Models\ShippingSupplierModel;
use App\Models\ShopModel;
use App\Models\SupplierModel;
use App\Models\WardModel;
use App\Models\AttributeValueModel;
use App\Models\ProductAttributeModel;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use function Matrix\trace;

class CartController extends Controller
{
    public function __construct(Request $request)
    {
        if (!$request->session()->has('data_customer')) {
            Redirect::to('login')->send();
        }
    }

    /**
     * Giỏ hàng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $dataCustomer = session()->get('data_customer');
        $customer = CustomerModel::find($dataCustomer->id);
        session()->forget('data_check_coin');
        if ($dataCustomer) {
            $data_order_old = OrderModel::where('customer_id', $dataCustomer->id)->where('is_selected', 0)->get();
            if ($data_order_old) {
                foreach ($data_order_old as $key => $value) {
                    $data_order_old[$key]->delete();
                    unset($data_order_old[$key]);
                }
            }
        }
        switch ($request->type) {
            case 'OrderDeliveryToCTV':
                $type = 'OrderDeliveryToCTV';
                break;
            case 'OrderDeliveryCustomersCTV':
                $type = 'OrderDeliveryCustomersCTV';
                break;
            default:
                $type = 'cart';
                break;
        }
        if ($type === 'cart') {
            CartModel::where('customer_id', $dataCustomer->id)->update(['selected' => 0]);
        }
        $address = CustomerAddressModel::where('customer_id', $dataCustomer->id)->get();
        if (isset($address)) {
            foreach ($address as $k => $v) {
                $city = CityModel::find($v['city_id']);
                $district = DistrictModel::find($v['district_id']);
                $ward = WardModel::where('ward_id', '=', $v['ward_id'])->first();
                if ($ward) {
                    $address[$k]['full_address'] = $v['address'] . ', ' . $ward->name . ', ' . $district->name . ', ' . $city->name . '.';
                }
            }
        }
        $data_cart = CartModel::where('customer_id', $dataCustomer->id)->get();
        foreach ($data_cart as $value){
            $product_value = AttributeValueModel::find($value->value_attr);
            if ($value->quantity > $product_value->quantity || $value->quantity == 0){
                $cart = CartModel::find($value->id);
                $cart->delete();
            }
        }
        $dataShop = DB::table('cart')
            ->select('cart.shop_id', 'shop.name as name_shop', 'shop.id as id_shop')
            ->distinct('shop_id')
            ->join('shop', 'cart.shop_id', '=', 'shop.id')
            ->join('customer', 'cart.customer_id', '=', 'customer.id')
            ->where('cart.customer_id', $dataCustomer->id)
            ->get();
        foreach ($dataShop as $k => $v) {
            $dataProduct = DB::table('cart')
                ->join('product', 'cart.product_id', '=', 'product.id')
                ->join('attribute_value', 'cart.value_attr', '=', 'attribute_value.id')
                ->join('product_attribute', 'attribute_value.attribute_id', '=', 'product_attribute.id')
                ->select('product.*', 'cart.quantity as quantity', 'cart.id as id', 'cart.percent as percent', 'cart.price_cart as price_cart' , 'cart.value_attr as value_attr', 'product.id as id_product',
                    'attribute_value.value as value_attr', 'attribute_value.size_of_value as size_of_value', 'attribute_value.price_ctv as price_attr',
                    'product_attribute.category_2 as cate_2', 'attribute_value.quantity as quantity_product')
                ->where('cart.customer_id', $dataCustomer->id)
                ->where('cart.shop_id', $v->shop_id)
                ->get();
            $dataShop[$k]->prd = $dataProduct;
        }
        $data['address'] = $address;
        $dataOrderShop = DB::table('order_detail')
            ->select('order_detail.shop_id', 'shop.name as name_shop', 'shop.id as shop_id',
                'order_detail.customer_id as customer_id', 'order_detail.order_id as order_id', 'customer.coin as coin')
            ->distinct('shop_id')
            ->join('shop', 'order_detail.shop_id', '=', 'shop.id')
            ->join('order', 'order_detail.order_id', '=', 'order.id')
            ->join('customer', 'order_detail.customer_id', '=', 'customer.id')
            ->where('order_detail.customer_id', $dataCustomer->id)
            ->where('order.is_selected', 0)
            ->get();
        foreach ($dataOrderShop as $k => $v) {
            $dataProduct = DB::table('order_detail')
                ->select('product.*', 'order_detail.quantity as quantity', 'order_detail.id as id',
                    'order_detail.type_product as type_product', 'product.id as product_id', 'order.id as order_id',
                    'attribute_value.value as value_attr', 'attribute_value.size_of_value as size_of_value', 'attribute_value.price_ctv as price_attr')
                ->join('product', 'order_detail.product_id', '=', 'product.id')
                ->join('order', 'order_detail.order_id', '=', 'order.id')
                ->join('attribute_value', 'order_detail.type_product', '=', 'attribute_value.id')
                ->where('order.is_selected', 0)
                ->where('order_detail.customer_id', $dataCustomer->id)
                ->where('order_detail.shop_id', $v->shop_id)
                ->get();
            $dataOrderShop[$k]->prd = $dataProduct;
        }
        $dataFeeCTV24h = FeeCTV24HModel::first();
        $dataCity = CityModel::all();
        $data_return = [
            'title' => 'Giỏ hàng',
            'type' => $type,
            'data' => $data,
            'dataShop' => $dataShop,
            'dataOrderShop' => $dataOrderShop,
            'dataRelatedProduct' => isset($dataRelatedProduct) ? $dataRelatedProduct : null,
            'city' => $dataCity,
            'value' => $dataCustomer,
            'feeCTV24h' => $dataFeeCTV24h,
        ];
        return view('frontend.cart.index', $data_return, $data);
    }

//    protected function getDataVlueAttr($value)
//    {
//        $item = explode(',', $value);
//        $data = [];
//        if (isset($item)) {
//            foreach ($item as $v) {
//                $__dataAttrValue = AttributeValueModel::find($v);
//                if (isset($__dataAttrValue)) {
//                    $__dataAttrProduct = ProductAttributeModel::find($__dataAttrValue->attribute_id);
//                    $__data['type'] = isset($__dataAttrProduct) ? $__dataAttrProduct['name'] : '';
//                    $__data['value'] = isset($__dataAttrValue) ? $__dataAttrValue['value'] : '';
//                    array_push($data, $__data);
//                }
//            }
//        }
//        return $data;
//    }

    /**
     *  Xóa sản phẩm khỏi giỏ hàng
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function deleteProduct($id)
    {
        try {
            $data = CartModel::find($id);
            if ($data) {
                File::delete($data->image);
            }
            CartModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
//            return response()->json($dataReturn, Response::HTTP_OK);
            return back();
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     *  Cập nhật số lượng sản phẩm
     * @param Request $request
     * @return array
     */
    public function updateQuantity(Request $request)
    {
        $customer = session()->get('data_customer');
        $mini_cart = $request->get('mini_cart');
        $data_cart = CartModel::find($request->id);
        $data_attriute = AttributeValueModel::find($data_cart->value_attr);
        $total_price = 0;
        $total_price_cart = 0;
        if ($request->type == 1){
            $quantity_cart = $data_cart->quantity + 1;
            if ($quantity_cart > $data_attriute->quantity) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'quá giới hạn số lượng'
                ];
            } else {
                $quantity = $data_cart->quantity + 1;
                $max = BuyMorePercent::where('product_id', $data_attriute->product_id)->max('quantity_2');
                if ($quantity > $max){
                    $percent_ctv = BuyMorePercent::where('product_id', $data_attriute->product_id)->max('percent');
                }else{
                    $percent = BuyMorePercent::where('quantity_1', '<=', $quantity)->where('product_id', $data_attriute->product_id)
                        ->where('quantity_2', '>=', $quantity)->first();
                    if (isset($percent)){
                        $percent_ctv = $percent->percent;
                    }else{
                        $percent_ctv = 0;
                    }
                }
                $price_percent = $data_attriute->price_ctv - $data_attriute->price_ctv * $percent_ctv / 100;
                $data_cart->update([
                    'quantity' => $quantity,
                    'percent' => $percent_ctv,
                    'price_cart' => $price_percent
                ]);
                $data_price = $data_cart->quantity * $data_cart->price_cart;
                $dataReturn = [
                    'data_price' => number_format($data_price),
                    'quantity' => $data_cart->quantity ,
                    'price_cart' => number_format($data_cart->price_cart),
                    'title' => 'Cập nhật giỏ hàng',
                    'status' => true,
                ];
            }
        }elseif ($request->type == 3){
            $max = BuyMorePercent::where('product_id', $data_attriute->product_id)->max('quantity_2');
            if ($request->quantity > $data_attriute->quantity){
                if ( $data_attriute->quantity > $max ){
                    $percent_ctv = BuyMorePercent::where('product_id', $data_attriute->product_id)->max('percent');
                }else{
                    $percent = BuyMorePercent::where('quantity_1', '<=', $data_attriute->quantity)->where('product_id', $data_attriute->product_id)
                        ->where('quantity_2', '>=', $data_attriute->quantity)->first();
                    if (isset($percent)){
                        $percent_ctv = $percent->percent;
                    }else{
                        $percent_ctv = 0;
                    }
                }
                $price_percent = $data_attriute->price_ctv - $data_attriute->price_ctv * $percent_ctv / 100;
                $data_cart->update([
                    'quantity' => $data_attriute->quantity,
                    'percent' => $percent_ctv,
                    'price_cart' => $price_percent
                ]);
                $data_price = $data_cart->quantity * $data_cart->price_cart;
                $check = true;
                $msg = 'Không thể vượt quá số lượng hàng tồn kho';
            }elseif ($request->quantity > 1){
                if ($request->quantity > $max){
                    $percent_ctv = BuyMorePercent::where('product_id', $data_attriute->product_id)->max('percent');
                }else{
                    $percent = BuyMorePercent::where('quantity_1', '<=', $request->quantity)->where('product_id', $data_attriute->product_id)
                        ->where('quantity_2', '>=', $request->quantity)->first();
                    if (isset($percent)){
                        $percent_ctv = $percent->percent;
                    }else{
                        $percent_ctv = 0;
                    }
                }
                $price_percent = $data_attriute->price_ctv - $data_attriute->price_ctv * $percent_ctv / 100;
                $data_cart->update([
                    'quantity' => $request->quantity,
                    'percent' => $percent_ctv,
                    'price_cart' => $price_percent,
                ]);
                $data_price = $data_cart->quantity * $data_cart->price_cart;
                $check = false;
                $msg = null;
            }else{
                $percent = BuyMorePercent::where('quantity_1', '<=', 1)->where('product_id', $data_attriute->product_id)
                    ->where('quantity_2', '>=', 1)->first();
                if (isset($percent)){
                    $percent_ctv = $percent->percent;
                }else{
                    $percent_ctv = 0;
                }
                $price_percent = $data_attriute->price_ctv - $data_attriute->price_ctv * $percent_ctv / 100;
                $data_cart->update([
                    'quantity' => 1,
                    'percent' => $percent_ctv,
                    'price_cart' => $price_percent
                ]);
                $data_price = $data_cart->price_cart;
                $check = false;
                $msg = null;
            }
            $dataReturn = [
                'status' => true,
                'data_price' => number_format($data_price),
                'quantity' => $data_cart->quantity,
                'check' => $check,
                'msg' => $msg,
                'price_cart' => number_format($data_cart->price_cart)
            ];
        }else{
            $number_product = $data_cart->quantity -1;
            $max = BuyMorePercent::where('product_id', $data_attriute->product_id)->max('quantity_2');
            if ( $number_product < 1 ) {
                $percent = BuyMorePercent::where('quantity_1', '<=', 1)->where('product_id', $data_attriute->product_id)
                    ->where('quantity_2', '>=', 1)->first();
                if (isset($percent)){
                    $percent_ctv = $percent->percent;
                }else{
                    $percent_ctv = 0;
                }
                $price_percent = $data_attriute->price_ctv - $data_attriute->price_ctv * $percent_ctv / 100;
                $data_cart->update([
                    'quantity' => 1,
                    'percent' => $percent_ctv,
                    'price_cart' => $price_percent
                ]);
                $dataReturn = [
                    'status' => true,
                    'data_price' => number_format($data_cart->price_cart),
                    'quantity' => 1,
                    'price_cart' => number_format($data_cart->price_cart)
                ];
            } else {
                if ($number_product > $max){
                    $percent_ctv = BuyMorePercent::where('product_id', $data_attriute->product_id)->max('percent');
                }else{
                    $percent = BuyMorePercent::where('quantity_1', '<=', $number_product)->where('product_id', $data_attriute->product_id)
                        ->where('quantity_2', '>=', $number_product)->first();
                    if (isset($percent)){
                        $percent_ctv = $percent->percent;
                    }else{
                        $percent_ctv = 0;
                    }
                }
                $price_percent = $data_attriute->price_ctv - $data_attriute->price_ctv * $percent_ctv / 100;
                $data_cart->update([
                    'quantity' => $number_product,
                    'percent' => $percent_ctv,
                    'price_cart' => $price_percent
                ]);
                $data_price = $data_cart->quantity * $data_cart->price_cart;
                $dataReturn = [
                    'data_price' => number_format($data_price),
                    'quantity' => $number_product ,
                    'price_cart' => number_format($data_cart->price_cart),
                    'title' => 'Cập nhật giỏ hàng',
                    'status' => true,
                ];
            }
        }
        if ($request->checked == 1){
            $cart = CartModel::where('customer_id', $customer->id)->where('selected', 1)->get();
            foreach ($cart as $value){
                $price_cart = $value->price_cart * $value->quantity;
                $total_price += $price_cart;
            }
            $dataReturn['total_price'] = number_format($total_price);
            $dataReturn['checked'] = true;
        }
        if (isset($mini_cart)){
            $cart_user = CartModel::where('customer_id', $customer->id)->where('selected', 0)->get();
            foreach ($cart_user as $value){
                $total = $value->price_cart * $value->quantity;
                $total_price_cart += $total;
            }
            $dataReturn['total_cart'] = number_format($total_price_cart);
        }
        return $dataReturn;
    }
    /**
     * create new order
     */
    protected function createOrder($request, $key, $val, $today)
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $check = true;
        $total_price = 0;
        $shop_parent = 0;
        foreach ($val as $value){
            $total_price += $value->price_cart * $value->quantity;
            $product = ProductModel::find($value->product_id);
            if($product){
                if ($product->dropship == 1){
                    $product_dropship = ProductDropShipModel::where('product_id', $product->id)->first();
                    $shop_parent = $product_dropship->shop_parent;
                    $check = true;
                }else{
                    $check = false;
                }
            }
        }
        if ($check){
            $order = new OrderModel([
                'order_code' => 'CTV' . mb_strtoupper(Str::random(9), 'utf-8'),
                'customer_id' => $request->customer_id,
                'shop_id' => $shop_parent,
                'total_price' => $total_price,
                'status' => 0,
                'date' => $today,
                'address' => 0,
                'payment_id' => 0,
                'note' => '',
            ]);
            $order->save();
        }else{
            $order = new OrderModel([
                'order_code' => 'CTV' . mb_strtoupper(Str::random(9), 'utf-8'),
                'customer_id' => $request->customer_id,
                'shop_id' => $key,
                'total_price' => $total_price,
                'status' => 0,
                'date' => $today,
                'address' => 0,
                'payment_id' => 0,
                'note' => '',
            ]);
            $order->save();
        }
        $checkCTV = ManageCTVModel::where('shop_id', $key)
            ->where('collaborator_id', $request->customer_id)
            ->first();
        if(!isset($checkCTV)){
            $dataManageCTV = new ManageCTVModel([
                'shop_id' => $key,
                'collaborator_id' => $request->customer_id
            ]);
            $dataManageCTV->save();
        }
        return $order;
    }

    /**
     * create order detail
     */
    protected function createOrderDetail($dataCustomer, $v, $_dataOrder)
    {
        $address = CustomerAddressModel::where('customer_id', $dataCustomer->id)->where('default', 1)->first();
        if (!isset($address)) {
            $address = CustomerAddressModel::where('customer_id', $dataCustomer->id)->get();
            $address = count($address) ? $address[0] : '';
        }
//        $product = ProductModel::find($v['product_id']);
//        $total_price = isset($product) ? $product->price_discount * $v['quantity'] : 0;
        $product = ProductModel::find($v->product_id);
        if($product){
            if ($product->dropship == 1){
                $product_dropship = ProductDropShipModel::where('product_id', $v->product_id)->first();
                $id_product = $product_dropship->product_parent;
                $id_shop = $product_dropship->shop_parent;
            }else{
                $id_product = $v->product_id;
                $id_shop = $v->shop_id;
            }
        }else{
            $id_shop = $v->shop_id;
        }
        $total_price = $v->price_cart * $v->quantity;
        $orderDetail = new OrderDetailModel([
            'order_id' => $_dataOrder->id,
            'shop_id' => $id_shop,
            'product_id' => $v->product_id,
            'quantity' => $v->quantity,
            'customer_id' => $v->customer_id,
            'type_product' => $v->value_attr,
            'product_price' => $v->price_cart,
            'total_price' => $total_price,
            'address' => isset($address->address_maps) ? $address->address_maps : '',
        ]);
        $orderDetail->save();
        return $orderDetail;
    }

    /**
     * sort data place order by shop
     */
    protected function sortPlaceOrderByShop($data_cart)
    {
        $__data_place_order = [];
        foreach ($data_cart as $value) {
            $shop_id = $value->shop_id;
            $product = ProductModel::find($value['product_id']);
            if (isset($__data_place_order[$shop_id])) {
                array_push($__data_place_order[$shop_id], $value);
//                $__data_place_order[$shop_id]['total_price'] += isset($product) ? $product->price * $value['quantity'] : 0;
            } else {
                $__data_place_order[$shop_id] = [];
                array_push($__data_place_order[$shop_id], $value);
//                $__data_place_order[$shop_id]['total_price'] = isset($product) ? $product->price * $value['quantity'] : 0;
            }
        }

        return $__data_place_order;
    }

    /**
     *  Nút đặt giao cho CTV
     * @param Request $request
     * @return array
     */
    public function saveCart(Request $request)
    {
        $dataCustomer = session()->get('data_customer');
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $check_coin = $request->check_coin;
        $request->session()->put('data_check_coin', $check_coin);
        $data_cart = CartModel::where('customer_id', $request->customer_id)->where('selected', 1)->get();
        foreach ($data_cart as $value){
            $product_value = AttributeValueModel::find($value->value_attr);
            if ($value->quantity > $product_value->quantity){
                $dataReturn['status'] = false;
                $dataReturn['msg'] = 'Rất tiếc Phân loại sản phẩm đã hết hàng. Vui lòng chọn phân loại khác để tiếp tục';
                return $dataReturn;
                break;
            }
        }
        if (count($data_cart)) {
            $order = OrderModel::where('customer_id', $request->customer_id)->where('is_selected', 0)->first();
            $__data_place_order = $this->sortPlaceOrderByShop($data_cart);
            foreach ($__data_place_order as $__key => $__val) {
                if (!isset($order)) {
                    $_dataOrder = $this->createOrder($request, $__key, $__val, $today);
                    foreach ($__val as $k => $v) {
                        $this->createOrderDetail($dataCustomer, $v, $_dataOrder);
                    }
                }
            }
        }
        $dataReturn = [
            'url' => URL::to('cart/order_delivery_to_ctv'),
            'status' => true,
            'msg' => 'Chọn sản phẩm giao cho CTV thành công !'
        ];
        return $dataReturn;
    }

    /**
     *  Nút mua hàng | Đặt giao cho CTV
     * @param Request $request
     * @return array nút đỏ
     */
    public function checkoutCart(Request $request)
    {
        try {
            $payment = $request->payment_id;
            $fee = FeeCTV24HModel::first();
            $dataCustomer = session()->get('data_customer');
            $customer = CustomerModel::find($dataCustomer->id);
            $address = CustomerAddressModel::where('customer_id', $dataCustomer->id)->where('default', 1)->first();
            if (!isset($address)) {
                $address = CustomerAddressModel::where('customer_id', $dataCustomer->id)->get();
                $address = count($address) ? $address[0] : '';
            }
            if ($payment == 0){
                $data_cart = CartModel::where('customer_id', $dataCustomer->id)->where('selected', 1)->get();
                $total_cart = 0;
                foreach ($data_cart as $value){
                    $price_cart = $value->quantity * $value->price_cart;
                    $total_cart += $price_cart;
                }
                $total_cart_all = $total_cart + 10000;
                if ($total_cart_all > $customer->wallet){
                    $dataReturn['status'] = false;
                    $dataReturn['msg'] = 'Số tiền trong ví không đủ. Vui lòng chọn phương thức thanh toán khác';
                    return $dataReturn;
                }else{
                    $customer->wallet = $customer->wallet - $total_cart_all;
                    $customer->save();
                }
            }
            if (!empty($address)) {
                $data = OrderModel::where('customer_id', $dataCustomer->id)
                    ->where('is_selected', 0)
                    ->get();
                foreach ($data as $key => $value) {
                    $dataCheckout = OrderModel::find($value->id);
                    $dataCheckout->is_selected = 1;
                    $dataCheckout->note = $request->note;
                    $dataCheckout->shipping_id = $request->shipping_id;
                    $dataCheckout->status_confirm = 0;
                    if ($payment == null) {
                        $dataCheckout->payment_id = 2;
                    } else {
                        $dataCheckout->payment_id = $request->payment_id;
                    }
                    $dataCheckout->delivery_method = $request->delivery_method;
//                $dataCheckout->address = $address->address_maps;
                    $dataCheckout->save();
//                if($dataCheckout->payment_id == 0 || $dataCheckout->payment_id == 3){
//                    $dataCheckout['status'] = 1;
//                    $dataCheckout->save();
//                }

                }
                $dataOrderttl = OrderModel::where('customer_id', $dataCustomer->id)->where('collaborator_id', null)->where('is_selected', 1)->get();
                foreach ($dataOrderttl as $kOrttl => $vOrttl) {
                    $dataOrderDetailttl = OrderDetailModel::where('customer_id', $dataCustomer->id)->where('order_id', $vOrttl->id)->get();
                    foreach ($dataOrderDetailttl as $kOrDetll => $vOrDettl) {
                        $dataOrderttl[$kOrttl]->total += $vOrDettl->total_price;
                        $dataOrderttl[$kOrttl]->total_quantity += $vOrDettl->quantity;
                        $dataOrderttl[$kOrttl]->quantity = $vOrDettl->quantity;
                        $total_price_all = $dataOrderttl[$kOrttl]->total + 10000;
                        $data_order = OrderModel::find($vOrttl->id);
                        $price_fee = round(($total_price_all - 10000) * ($fee->fee / 100), 0);
                        $data_order->total_price = $total_price_all;
                        $data_order->total_price_product = $dataOrderttl[$kOrttl]->total;
                        $data_order->fee_ctv24h = $price_fee;
                        $data_order->price_supplier = $data_order->total_price_product - $price_fee;
                        $data_order->is_selected = 2;
                        $data_order->save();
                    }
                    if ($data_order->payment_id == 0 || $data_order->payment_id == 3) {
                        $data_order['status'] = 1;
                        $data_order['shipping_confirm'] = 0;
                        $data_order->save();
                    }
                }
                //tạo doanh thu cho shop

                $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
                $data_shop = ShopModel::find($data_order->shop_id);
                $data_history_order_payment = new HistoryOrderPaymentModel([
                    'order_id' => $data_order->id,
                    'order_code' => $data_order->order_code,
                    'price' => ($data_order->total_price - 10000) - $price_fee,
                    'status' => 0,
                    'type' => 0,
                    'is_dropship' => 0,
                    'customer_id' => $data_shop->customer_id,
                    'created_at' => $today
                ]);
                $data_history_order_payment->save();
                //end doanh thu shop

                //tạo doanh thu sàn
                $data_history_deal_ctv24h = new HistoryDealCTV24([
                    'order_id' => $data_order->id,
                    'price' => $price_fee,
                    'status' => 0,
                    'type' => 0,
                    'created_at' => $today
                ]);
                $data_history_deal_ctv24h->save();
                //end doanh thu sàn

                //Cộng doanh thu cho CTV
                $customer->total_revenue += $data_order->total_price;
                $customer->total_quantity += $dataOrderttl[$kOrttl]->total_quantity;
                $customer->save();

                $this->updateQuantityProduct($dataCustomer->id);
                $dataReturn = [
                    'status' => true,
                    'url' => URL::to('/my_history/by_product'),
                    'msg' => 'Đặt hàng thành công',
                ];
//                $checkCustomerWallet = CustomerModel::find($dataCustomer->id);
//                if($payment == 0) {
//                    $checkCustomerWallet->wallet -= $total_price_all;
//                    $checkCustomerWallet->save();
//                }
                $this->saveOrderCTV($dataOrderttl);
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Vui lòng chọn địa chỉ',
                ];
            }
            return $dataReturn;
        } catch (Exception $e) {
            $dataReturn = [
                'status' => false,
                'msg' => $e
            ];
            return $dataReturn;
        }
    }

    protected function saveOrderCTV($invoice)
    {
        foreach ($invoice as $value){
            $order = OrderDetailModel::where('order_id', $value->id)->get();
            foreach ($order as $item){
                $product = ProductModel::find($item->product_id);
                $product_attr = AttributeValueModel::find($item->type_product);
                if ($product->dropship == 1){
                    $this->addWalletCtv($product, $product_attr, $value->id, $item->quantity);
                }
            }
        }
    }

    /**
     * add wallet for ctv
     *
     * return null
    */
    public function addWalletCtv($product, $product_attr, $invoice_id, $quantity){
        try{
            $product_dropship = ProductDropShipModel::orderBy('id', 'desc')->where('product_id', $product->id)->get();
            $product_parent = 0;
            $shop_parent = 0;
            if (count($product_dropship) > 0){
                foreach ($product_dropship as $key => $v){
                    $_attr_parent = AttributeValueModel::where('product_id', $v->product_parent)
                        ->where('size_of_value', $product_attr->size_of_value)->first();
                    if ($product_parent == 0){
                        $product_parent = $v->product_parent;
                        $shop_parent = $v->shop_parent;
                        $_attr_children = AttributeValueModel::where('product_id', $v->product_id)
                            ->where('size_of_value', $product_attr->size_of_value)->first();
                        $shop = ShopModel::find($v->shop_id);

                    }else{
                        $_attr_children = AttributeValueModel::where('product_id', $product_parent)
                            ->where('size_of_value', $product_attr->size_of_value)->first();
                        $shop = ShopModel::find($shop_parent);
                    }
                    $data_customer = CustomerModel::find($shop->customer_id);
                    if ($data_customer){
                        $total_price = $_attr_children->price_ctv - $_attr_parent->price_ctv;
                        $shop_parent = $v->shop_parent;
                        $total_order = $total_price * $quantity;
                        $total_price_product = $_attr_children->price_ctv * $quantity;
                        $oder_ctv = new OrderCTVModel([
                            'order_id' =>  $invoice_id,
                            'shop_id' => $shop->id,
                            'profit' => $total_order,
                            'total_price' => $total_price_product,
                            'active' => 0,
                        ]);
                        $oder_ctv->save();
                    }
                }
            }
        }catch (\Exception $e){
            return false;
        }
    }

    /**
     *  Chọn sản phẩm muốn mua | Là ô Checkbox
     * @param Request $request
     * @return array
     */
    public function checkCart(Request $request)
    {
        $dataCustomer = session()->get('data_customer');
        $total_price = 0;
        if ($request->type == 1){
            $data_cart = CartModel::where('customer_id', $dataCustomer->id)->where('selected', 0)->get();
            if (isset($data_cart)){
                foreach ($data_cart as $key => $value){
                    $data_cart[$key]['selected'] = 1;
                    $data_cart[$key]->save();
                }
            }
            $cart = CartModel::where('customer_id', $dataCustomer->id)->where('selected', 1)->get();
            foreach ($cart as $value){
                $price = $value->price_cart * $value->quantity;
                $total_price += $price;
            }
        }else{
            $cart = CartModel::where('customer_id', $dataCustomer->id)->where('selected', 1)->get();
            if (isset($cart)){
                foreach ($cart as $key => $value){
                    $cart[$key]['selected'] = 0;
                    $cart[$key]->save();
                }
            }
        }
        $dataReturn = [
            'msg' => 'success',
            'status' => true,
            'total_price' => number_format($total_price),
            'total_price_order' => $total_price
        ];
        return $dataReturn;
    }

    public function checkCartShop(Request $request)
    {
        $customer = session()->get('data_customer');
        $data_check_cart = CartModel::where('customer_id', $customer->id)->get();
        $price_check = 0;
        foreach ($data_check_cart as $item){
            $price = $item->price_cart * $item->quantity;
            $price_check += $price;
        }
        if ($request->type == 1){
            $data_cart = CartModel::where('customer_id', $customer->id)->where('shop_id', $request->shop_id)->where('selected', 0)->get();
            foreach ($data_cart as $key => $value){
                $data_cart[$key]['selected'] = 1;
                $data_cart[$key]->save();
            }
        }else{
            $data_cart = CartModel::where('customer_id', $customer->id)->where('shop_id', $request->shop_id)
                ->where('selected', 1)->get();
            foreach ($data_cart as $key => $value){
                $data_cart[$key]['selected'] = 0;
                $data_cart[$key]->save();
            }
        }
        $data_all = CartModel::where('customer_id', $customer->id)->where('selected', 1)->get();
        $data_total_price = 0;
        if (isset($data_all)){
            foreach ($data_all as $value){
                $price = $value->price_cart * $value->quantity;
                $data_total_price += $price;
            }
        }
        if ($price_check == $data_total_price){
            $data['checked_all'] = true;
        }else{
            $data['checked_all'] = false;
        }
        $data['checked'] = true;
        $data['status'] = true;
        $data['total_price'] = number_format($data_total_price);
        $data['total_price_order'] = $data_total_price;
        $data['msg'] = 'success';
        return $data;
    }

    public function checkCartProduct(Request $request)
    {
        $customer = session()->get('data_customer');
        $cart = CartModel::find($request->id);
        $data_cart_shop = CartModel::where('shop_id', $cart->shop_id)->where('customer_id', $customer->id)->get();
        $data_cart = CartModel::where('customer_id', $customer->id)->where('selected', 1)->get();
        $data_total_cart = CartModel::where('customer_id', $customer->id)->get();
        $price_total = 0;
        $price_total_shop = 0;
        $price = 0;
        $price_shop = 0;
        if (isset($data_total_cart)){
            foreach ($data_total_cart as $item){
                $pr = $item->price_cart * $item->quantity;
                $price_total += $pr;
            }
        }
        if (isset($data_cart_shop)){
            foreach ($data_cart_shop as $item){
                $pr = $item->price_cart * $item->quantity;
                $price_total_shop += $pr;
            }
        }
        if (isset($data_cart)){
            foreach ($data_cart as $value){
                $price_cart = $value->price_cart * $value->quantity;
                $price += $price_cart;
            }
        }
        if ($request->type == 1){
            $cart->selected = 1;
            $cart->save();
            $price_cart_child = $cart->price_cart * $cart->quantity;
            $total_price = $price + $price_cart_child;
            $data_cart_shop = CartModel::where('shop_id', $cart->shop_id)->where('customer_id', $customer->id)
                ->where('selected', 1)
                ->get();
            foreach ($data_cart_shop as $value){
                $price_cart = $value->price_cart * $value->quantity;
                $price_shop += $price_cart;
            }
        }else{
            $cart->selected = 0;
            $cart->save();
            $price_cart_child = $cart->price_cart * $cart->quantity;
            $total_price = $price - $price_cart_child;
        }
        if ( $total_price == $price_total){
            $data['checked_all'] = true;
        }else{
            $data['checked_all'] = false;
        }
        if ( $price_shop == $price_total_shop){
            $data['data_shop'] = true;
        }else{
            $data['data_shop'] = false;
        }
        $data['checked_shop'] = true;
        $data['checked'] = true;
        $data['status'] = true;
        $data['total_price'] = number_format($total_price);
        $data['total_price_order'] = $total_price;
        $data['msg'] = 'success';
        return $data;
    }

    /**
     *  Nút đặt giao cho khách của CTV | Nút màu xanh
     * @param Request $request
     * @return array
     */
    public function saveCartCustomerCTV(Request $request)
    {
        $dataCustomer = session()->get('data_customer');
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $total_price_order = $request->total_price_order;
        $data_cart = CartModel::where('customer_id', $request->customer_id)->where('selected', 1)->get();
        foreach ($data_cart as $value){
            $product_value = AttributeValueModel::find($value->value_attr);
            if ($value->quantity > $product_value->quantity){
                $dataReturn['status'] = false;
                $dataReturn['msg'] = 'Rất tiếc Phân loại sản phẩm đã hết hàng. Vui lòng chọn phân loại khác để tiếp tục';
                return $dataReturn;
                break;
            }
        }
        if (count($data_cart)) {
            $order = OrderModel::where('customer_id', $request->customer_id)->where('is_selected', 0)->first();
            $__data_place_order = $this->sortPlaceOrderByShop($data_cart);
            foreach ($__data_place_order as $__key => $__val) {
                if (!isset($order)) {
                    $_dataOrder = $this->createOrder($request, $__key, $__val, $today);
                    foreach ($__val as $k => $v) {
                        $this->createOrderDetail($dataCustomer, $v, $_dataOrder);
                    }
                }
            }
        }
        $dataReturn = [
            'url' => URL::to('cart/order_delivery_customer_ctv'),
            'status' => true,
            'msg' => 'Chọn sản phẩm giao cho khách của CTV thành công !'
        ];
        return $dataReturn;
    }

    /**
     *  Nút mua hàng | Đặt giao cho khách của CTV
     * @param Request $request
     * @return array
     */
    public function checkoutCartCustomerCTV(Request $request)
    {
        try {
            $payment = $request->payment_id;
            $total_price_ctv = $request->get('total_payment') + 10000;
            $feeCTV24h = FeeCTV24HModel::first();
            $today = Carbon::now('Asia/Ho_Chi_Minh');
            $dataCustomer = session()->get('data_customer');
            $customer = CustomerModel::find($dataCustomer->id);
            $address = CustomerAddressModel::where('customer_id', $dataCustomer->id)->where('default', 1)->first();
//        if (!isset($address)) {
//            $address = CustomerAddressModel::where('customer_id', $dataCustomer->id)->get();
//            $address = count($address) ? $address[0] : '';
//        }
            if ($payment == 0){
                if ($total_price_ctv > $customer->wallet){
                    $dataReturn['status'] = false;
                    $dataReturn['msg'] = 'Số tiền trong ví không đủ. Vui lòng chọn phương thức thanh toán khác';
                    return $dataReturn;
                }else{
                    $customer->wallet = $customer->wallet - $total_price_ctv;
                    $customer->save();
                }
            }
//        if (!empty($address)) {
            $data = OrderModel::where('customer_id', $dataCustomer->id)
                ->where('is_selected', 0)
                ->get();
            foreach ($data as $key => $value) {
                $dataCheckout = OrderModel::find($value->id);
                $dataCheckout->is_selected = 1;
                $dataCheckout->note = $request->note;
                $dataCheckout->cod = $request->cod;
                $dataCheckout->bonus = $request->cod - $value->total_price;
                $dataCheckout->is_selected = 1;
//                $dataCheckout->address = $address->address_maps;
                $dataCheckout->collaborator_id = 1;
                $dataCheckout->shipping_id = $request->shipping_id;
                $dataCheckout->status_confirm = 0;
                if ($payment == null) {
                    $dataCheckout->payment_id = 0;
                } else {
                    $dataCheckout->payment_id = $request->payment_id;
                }
                $dataCheckout->delivery_method = $request->delivery_method;
                $dataCheckout->address_ctv = $request->address_ctv;
                $dataCheckout->save();
            }

            $dataOrderttl = OrderModel::where('customer_id', $dataCustomer->id)->where('collaborator_id', 1)->where('is_selected', 1)->get();
            foreach ($dataOrderttl as $kOrttl => $vOrttl) {
                $dataOrderDetailttl = OrderDetailModel::where('customer_id', $dataCustomer->id)->where('order_id', $vOrttl->id)->get();
                foreach ($dataOrderDetailttl as $kOrDetll => $vOrDettl) {
                    $dataOrderttl[$kOrttl]->total += $vOrDettl->total_price;
                    $dataOrderttl[$kOrttl]->total_quantity += $vOrDettl->quantity;
                    $dataOrderttl[$kOrttl]->quantity = $vOrDettl->quantity;
                }
                $total_price_all = $request->cod + 10000;
                $data_order = OrderModel::find($vOrttl->id);
                $data_order->is_selected = 2;
                $price_fee = round($dataOrderttl[$kOrttl]->total * ($feeCTV24h->fee / 100), 0);
                $data_order->total_price = $total_price_all;
                $data_order->total_price_product = $dataOrderttl[$kOrttl]->total;
                $data_order->fee_ctv24h = $price_fee;
                $data_order->price_supplier = $data_order->total_price_product - $price_fee;
                $data_order->save();

                if ($data_order->payment_id == 0 || $data_order->payment_id == 3) {
                    $data_order['status'] = 1;
                    $data_order['shipping_confirm'] = 0;
                    $data_order->save();
                }
            }
            $this->saveOrderCTV($dataOrderttl);
            $dataAttr = AttributeValueModel::find($vOrDettl->type_product);
            $dataAttr->is_sell += $dataOrderttl[$kOrttl]->quantity;
            $dataAttr->save();

            //Tạo doanh thu cho shop
            $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
            $data_shop = ShopModel::find($data_order->shop_id);
            $data_history_order_payment = new HistoryOrderPaymentModel([
                'order_id' => $data_order->id,
                'order_code' => $data_order->order_code,
                'price' => $data_order->total_price_product - $price_fee,
                'status' => 0,
                'type' => 0,
                'is_dropship' => 1,
                'customer_id' => $data_shop->customer_id,
                'created_at' => $today
            ]);
            $data_history_order_payment->save();
            //End Tạo doanh thu cho shop

            //Tạo doanh thu cho CTV
            $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
            $data_history_order_payment_collaborator = new HistoryOrderPaymentCollaboratorModel([
                'order_id' => $data_order->id,
                'order_code' => $data_order->order_code,
                'price' => $data_order->bonus,
                'status' => 0,
                'type' => 0,
                'customer_id' => $dataCustomer->id,
                'created_at' => $today
            ]);
            $data_history_order_payment_collaborator->save();
            //End Tạo doanh thu cho CTV

            //Tạo doanh thu cho sàn
            $data_history_deal_ctv24h = new HistoryDealCTV24([
                'order_id' => $data_order->id,
                'price' => $price_fee,
                'status' => 0,
                'type' => 0,
                'created_at' => $today
            ]);
            $data_history_deal_ctv24h->save();
            //End tạo doanh thu cho sàn

            //Cộng doanh thu cho CTV
            $customer->total_revenue += $data_order->total_price;
            $customer->total_quantity += $dataOrderttl[$kOrttl]->total_quantity;
            $customer->save();

            $this->updateQuantityProduct($dataCustomer->id);

            $dataReturn = [
                'status' => true,
                'url' => URL::to('/my_history/by_product'),
                'msg' => 'Đặt hàng thành công',
            ];
            return $dataReturn;
        } catch (Exception $e) {
            $dataReturn = [
                'status' => false,
                'msg' => $e
            ];
            return $dataReturn;
        }
    }

    /**
     * update quantity product
    **/
    protected function updateQuantityProduct($customer_id)
    {
        $dataCart = CartModel::where('customer_id', $customer_id)->where('selected', 1)->get();
        foreach ($dataCart as $k => $v) {
            $product = AttributeValueModel::find($v->value_attr);
            $product->quantity = $product->quantity - $v->quantity;
            $product->save();
            CartModel::destroy($v->id);
        }
        return true;
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
//    public function filterAddress(Request $request)
//    {
//        try {
//            $type = $request->type ?? '';
//            $id = $request->id ?? '';
//            if ($type === 'city') {
//                $filter = DistrictModel::where('city_id', $id)->get();
//                $data['title'] = 'Quận/Huyện';
//                $data['ward'] = false;
//            } else {
//                $filter = WardModel::where('district_id', $id)->get();
//                $data['title'] = 'Phường/Xã';
//                $data['ward'] = true;
//            }
//            $data['data'] = $filter;
//            $html = view('frontend.cart.address', $data);
//            $dataReturn['html'] = strval($html);
//            return response()->json($dataReturn, Response::HTTP_OK);
//        } catch (\Exception $e) {
//            $dataReturn = [
//                'error' => $e,
//            ];
//            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
//        }
//    }

    /** Danh sách quận/huyện
     * @param Request $request
     */
    public function district(Request $request)
    {
        $data = $request->all();
        if ($data['action']) {
            $output = '';
            if ($data['action'] == 'city') {
                $city = DistrictModel::where('city_id', $data['id'])->orderBy('name', 'desc')->get();
                $output .= '
                    <option>Quận/huyện</option>
                ';
                foreach ($city as $key => $district) {
                    $output .= '<option value="' . $district->id . '" data-district-name="' . $district->name . '">' . $district->name . '</option>';
                }
            }
        }
        echo $output;
    }

    /**
     *  Danh sách phường/xã
     * @param Request $request
     */
    public function ward(Request $request)
    {
        $data = $request->all();
        if ($data['action']) {
            $output = '';
            if ($data['action'] == 'district') {
                $district = WardModel::where('district_id', $data['id'])->get();
                $output .= '
                    <option>Phường/xã</option>
                ';
                foreach ($district as $key => $ward) {
                    $output .= '<option value="' . $ward->id . '" data-ward-name="' . $ward->name . '">' . $ward->name . '</option>';
                }
            }
        }
        echo $output;
    }

    /**
     *  View Đặt giao cho CTV | Nút đỏ
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function orderDeliveryToCTV()
    {
        $dataCustomer = session()->get('data_customer');
        $address = CustomerAddressModel::where('customer_id', $dataCustomer->id)->get();
        if (isset($address)) {
            foreach ($address as $k => $v) {
//                $city = CityModel::find($v['city_id']);
//                $district = DistrictModel::find($v['district_id']);
                $ward = WardModel::where('ward_id', '=', $v['ward_id'])->first();
                if ($ward) {
                    $address[$k]['full_address'] = $v['address'];
                }
            }
        }
        $data_order = OrderModel::where('customer_id', $dataCustomer->id)->where('is_selected', 0)->first();
        $dataShop = DB::table('cart')
            ->select('cart.shop_id', 'shop.name as name_shop', 'shop.id as id_shop')
            ->distinct('shop_id')
            ->join('shop', 'cart.shop_id', '=', 'shop.id')
            ->join('customer', 'cart.customer_id', '=', 'customer.id')
            ->where('cart.customer_id', $dataCustomer->id)
            ->get();
        foreach ($dataShop as $k => $v) {
            $dataProduct = DB::table('cart')
                ->select('product.*', 'cart.quantity as quantity', 'cart.id as id', 'cart.value_attr as value_attr', 'product.id as id_product',
                    'attribute_value.value as value_attr', 'attribute_value.size_of_value as size_of_value', 'attribute_value.price_ctv as price_attr',
                    'product_attribute.category_2 as cate_2')
                ->join('product', 'cart.product_id', '=', 'product.id')
                ->join('attribute_value', 'cart.value_attr', '=', 'attribute_value.id')
                ->join('product_attribute', 'attribute_value.attribute_id', '=', 'product_attribute.id')
                ->where('cart.customer_id', $dataCustomer->id)
                ->where('cart.shop_id', $v->shop_id)
                ->get();
            $dataShop[$k]->prd = $dataProduct;
//            if (isset($v->prd)) {
//                $dataRelatedProduct = ProductModel::all();
//            }
        }
        $data['address'] = $address;
        $dataOrderShop = DB::table('order_detail')
            ->select('order_detail.shop_id', 'shop.name as name_shop', 'shop.id as shop_id',
                'order_detail.customer_id as customer_id', 'order_detail.order_id as order_id', 'customer.coin as coin',
                'shop.supplier_id as supplier_id')
            ->distinct('shop_id')
            ->join('shop', 'order_detail.shop_id', '=', 'shop.id')
            ->join('order', 'order_detail.order_id', '=', 'order.id')
            ->join('customer', 'order_detail.customer_id', '=', 'customer.id')
            ->where('order_detail.customer_id', $dataCustomer->id)
            ->where('order.is_selected', 0)
            ->get();
        foreach ($dataOrderShop as $k => $v) {
            $data_shop = ShopModel::find($v->shop_id);
            $dataShippingNCC = DB::table('shipping_supplier')
                ->select('shipping_supplier.*', 'shipping_supplier.shipping_id as shipping_id')
                ->distinct('shipping_id')
                ->where('shipping_supplier.supplier_id', $data_shop->supplier_id)
                ->get();
            if ($dataShippingNCC) {
                foreach ($dataShippingNCC as $__keyShipNCC => $__valueShipNCC) {
                    $dataShip = ShippingModel::find($__valueShipNCC->shipping_id);
                    $dataShippingNCC[$__keyShipNCC]->name_shipping = $dataShip->name ?? '';
                    $dataShippingNCC[$__keyShipNCC]->image_shipping = $dataShip->image ?? '';
                    $dataShippingNCC[$__keyShipNCC]->default = $dataShip->default ?? '';
                }
            }
            $dataProduct = DB::table('order_detail')
                ->select('product.*', 'order_detail.quantity as quantity', 'order_detail.id as id',
                    'order_detail.type_product as type_product', 'product.id as product_id', 'order.id as order_id',
                    'attribute_value.value as value_attr', 'attribute_value.size_of_value as size_of_value', 'order_detail.product_price as price_attr',
                    'product_attribute.category_2 as cate_2', 'attribute_value.price_ctv')
                ->join('product', 'order_detail.product_id', '=', 'product.id')
                ->join('order', 'order_detail.order_id', '=', 'order.id')
                ->join('attribute_value', 'order_detail.type_product', '=', 'attribute_value.id')
                ->join('product_attribute', 'attribute_value.attribute_id', '=', 'product_attribute.id')
                ->where('order.is_selected', 0)
                ->where('order_detail.customer_id', $dataCustomer->id)
                ->where('order_detail.shop_id', $v->shop_id)
                ->get();
            $dataOrderShop[$k]->prd = $dataProduct;
        }
        $dataFeeCTV24h = FeeCTV24HModel::first();
        $dataCity = CityModel::all();
        $customer = CustomerModel::find($dataCustomer->id);
        $dataReturn = [
            'title' => 'Giỏ hàng',
            'data' => $data,
            'dataShop' => $dataShop,
            'dataOrderShop' => $dataOrderShop,
            'dataRelatedProduct' => isset($dataRelatedProduct) ? $dataRelatedProduct : null,
            'city' => $dataCity,
            'value' => $dataCustomer,
            'feeCTV24h' => $dataFeeCTV24h,
            'data_shipping' => $dataShippingNCC,
            'data_order' => $data_order,
            'customer' => $customer
        ];
        return view('frontend.cart.OrderDeliveryToCTV', $dataReturn, $data);
    }

    /**
     *  View Đặt giao cho khách của CTV | Nút xanh
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function orderDeliveryCustomerCTV()
    {
        $dataCustomer = session()->get('data_customer');
        $address = CustomerAddressModel::where('customer_id', $dataCustomer->id)->get();
        if (isset($address)) {
            foreach ($address as $k => $v) {
                $city = CityModel::find($v['city_id']);
                $district = DistrictModel::find($v['district_id']);
                $ward = WardModel::where('ward_id', '=', $v['ward_id'])->first();
                if ($ward) {
                    $address[$k]['full_address'] = $v['address'] . ', ' . $ward->name . ', ' . $district->name . ', ' . $city->name . '.';
                }
            }
        }
        $dataShop = DB::table('cart')
            ->select('cart.shop_id', 'shop.name as name_shop', 'shop.id as id_shop')
            ->distinct('shop_id')
            ->join('shop', 'cart.shop_id', '=', 'shop.id')
            ->join('customer', 'cart.customer_id', '=', 'customer.id')
            ->where('cart.customer_id', $dataCustomer->id)
            ->get();
        foreach ($dataShop as $k => $v) {
            $dataProduct = DB::table('cart')
                ->select('product.*', 'cart.quantity as quantity', 'cart.id as id', 'cart.value_attr as value_attr', 'product.id as id_product',
                    'attribute_value.value as value_attr', 'attribute_value.size_of_value as size_of_value', 'attribute_value.price_ctv as price_attr',
                    'product_attribute.category_2 as cate_2')
                ->join('product', 'cart.product_id', '=', 'product.id')
                ->join('attribute_value', 'cart.value_attr', '=', 'attribute_value.id')
                ->join('product_attribute', 'attribute_value.attribute_id', '=', 'product_attribute.id')
                ->where('cart.customer_id', $dataCustomer->id)
                ->where('cart.shop_id', $v->shop_id)
                ->get();
            $dataShop[$k]->prd = $dataProduct;
//            if (isset($v->prd)) {
//                $dataRelatedProduct = ProductModel::all();
//            }
        }
        $data['address'] = $address;
        $dataOrderShop = DB::table('order_detail')
            ->select('order_detail.shop_id', 'shop.name as name_shop', 'shop.id as shop_id',
                'order_detail.customer_id as customer_id', 'order_detail.order_id as order_id', 'customer.coin as coin',
                'shop.supplier_id as supplier_id')
            ->distinct('shop_id')
            ->join('shop', 'order_detail.shop_id', '=', 'shop.id')
            ->join('order', 'order_detail.order_id', '=', 'order.id')
            ->join('customer', 'order_detail.customer_id', '=', 'customer.id')
            ->where('order_detail.customer_id', $dataCustomer->id)
            ->where('order.is_selected', 0)
            ->get();
        foreach ($dataOrderShop as $k => $v) {
            $data_shop = ShopModel::find($v->shop_id);
            $dataShippingNCC = DB::table('shipping_supplier')
                ->select('shipping_supplier.*', 'shipping_supplier.shipping_id as shipping_id')
                ->distinct('shipping_id')
                ->where('shipping_supplier.supplier_id', $data_shop->supplier_id)
                ->get();
            if ($dataShippingNCC) {
                foreach ($dataShippingNCC as $__keyShipNCC => $__valueShipNCC) {
                    $dataShip = ShippingModel::find($__valueShipNCC->shipping_id);
                    $dataShippingNCC[$__keyShipNCC]->name_shipping = $dataShip->name ?? '';
                    $dataShippingNCC[$__keyShipNCC]->image_shipping = $dataShip->image ?? '';
                    $dataShippingNCC[$__keyShipNCC]->default = $dataShip->default ?? '';
                }
            }
            $dataProduct = DB::table('order_detail')
                ->select('product.*', 'order_detail.quantity as quantity', 'order_detail.id as id', 'order_detail.product_price as product_price',
                    'order_detail.type_product as type_product', 'product.id as product_id', 'order.id as order_id',
                    'attribute_value.value as value_attr', 'attribute_value.size_of_value as size_of_value', 'attribute_value.price_ctv as price_attr', 'attribute_value.price as price_value',
                    'product_attribute.category_2 as cate_2')
                ->join('product', 'order_detail.product_id', '=', 'product.id')
                ->join('order', 'order_detail.order_id', '=', 'order.id')
                ->join('attribute_value', 'order_detail.type_product', '=', 'attribute_value.id')
                ->join('product_attribute', 'attribute_value.attribute_id', '=', 'product_attribute.id')
                ->where('order.is_selected', 0)
                ->where('order_detail.customer_id', $dataCustomer->id)
                ->where('order_detail.shop_id', $v->shop_id)
                ->get();
            $dataOrderShop[$k]->prd = $dataProduct;
        }
        $address_ctv = AddressCustomerCTVModel::where('customer_id', $dataCustomer->id)->get();
        foreach ($address_ctv as $key => $value){
            $city = CityModel::find($value->city);
            $address_ctv[$key]->city_name = $city->name;
            $district = DistrictModel::find($value->district);
            $address_ctv[$key]->district_name = $district->name;
            $ward = WardModel::find($value->ward);
            $address_ctv[$key]->ward_name = $ward->name;
        }
        $dataFeeCTV24h = FeeCTV24HModel::first();
        $dataCity = CityModel::all();
        $dataReturn = [
            'title' => 'Giỏ hàng',
            'data' => $data,
            'dataShop' => $dataShop,
            'dataOrderShop' => $dataOrderShop,
            'dataRelatedProduct' => isset($dataRelatedProduct) ? $dataRelatedProduct : null,
            'city' => $dataCity,
            'value' => $dataCustomer,
            'feeCTV24h' => $dataFeeCTV24h,
            'data_shipping' => $dataShippingNCC,
            'address_ctv' => isset($address_ctv) ? $address_ctv : null,
        ];
        return view('frontend.cart.OrderDeliveryCustomersCTV', $dataReturn, $data);
    }

    public function getAddress(Request $request)
    {
        try{
            $address_ctv = AddressCustomerCTVModel::find($request->get('id'));
            $city = CityModel::find($address_ctv->city);
            $district = DistrictModel::find($address_ctv->district);
            $ward = WardModel::find($address_ctv->ward);
            $html = '<div class="form-group">
                    <p>Họ và tên: <span class="name-show">'.$address_ctv->name.'</span></p>
                </div>
                <div class="form-group">
                    <p>Số điện thoại: <span class="phone-show">'.$address_ctv->phone.'</span></p>
                </div>

                <div class="form-group">
                    <p>Địa chỉ nhận hàng: <span class="address-show">'.$address_ctv->address.''. "-" .''.$ward->name.''. "-" .''.$district->name.''."-".''.$city->name.'</span></p>
                </div>';
            $data['status'] = true;
            $data['prod'] = $html;
            return $data;
        }catch (\Exception $exception){
            return $exception;
        }
    }

    public function createAddressCtv(Request $request)
    {
        try{
            $dataCustomer = session()->get('data_customer');
            $dataAddressCustomerCTV = AddressCustomerCTVModel::create([
                'name' => $request->get('name'),
                'phone' => $request->get('phone'),
                'city' => $request->get('city'),
                'district' => $request->get('district'),
                'ward' => $request->ward,
                'address' => $request->get('address_customer_ctv'),
                'customer_id' => $dataCustomer->id,
            ]);
            $dataAddressCustomerCTV->save();
            $ward = WardModel::find($request->get('ward'));
            $district = DistrictModel::find($request->get('district'));
            $city = CityModel::find($request->get('city'));
            $html = '<div class="form-group">
                    <p>Họ và tên: <span class="name-show">'.$dataAddressCustomerCTV->name.'</span></p>
                </div>
                <div class="form-group">
                    <p>Số điện thoại: <span class="phone-show">'.$dataAddressCustomerCTV->phone.'</span></p>
                </div>

                <div class="form-group">
                    <p>Địa chỉ nhận hàng: <span class="address-show">'.$dataAddressCustomerCTV->address.''. "-" .''.$ward->name.''. "-" .''.$district->name.''."-".''.$city->name.'</span></p>
                </div>';
            $data['status'] = true;
            $data['address_id'] = $dataAddressCustomerCTV->id;
            $data['prod'] = $html;
            return $data;
        }catch (\Exception $exception){
            return $exception;
        }

    }
}
