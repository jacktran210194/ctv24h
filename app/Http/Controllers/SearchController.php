<?php

namespace App\Http\Controllers;

use App\Models\AddressCustomerCTVModel;
use App\Models\CityModel;
use App\Models\DistrictModel;
use App\Models\FollowShopModel;
use App\Models\KeySearchModel;
use App\Models\OrderModel;
use App\Models\ProductModel;
use App\Models\ReviewProductModel;
use App\Models\ShopModel;
use App\Models\WardModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $dataCustomer = session()->get('data_customer');
        if ($dataCustomer) {
            $data_order_old = OrderModel::where('customer_id', $dataCustomer->id)->where('is_selected', 0)->get();
            if ($data_order_old) {
                foreach ($data_order_old as $key => $value) {
                    $data_order_old[$key]->delete();
                    unset($data_order_old[$key]);
                }
            }
        }
        switch ($request->type) {
            case 'best_seller':
                $type = 'best_seller';
                break;
            case 'new':
                $type = 'new';
                break;
            default:
                $type = 'relate';
                break;
        }
        $keyword_1 = $request->keyword;
        $keyword = str_replace(' ', '%', $keyword_1);
        if ($keyword_1) {
            if ($type == 'relate') {
                $data = ProductModel::where('name', 'like', '%' . $keyword . '%')
                    ->where('active', 1)
                    ->get();
            } elseif ($type == 'new') {
                $data = ProductModel::where('name', 'like', '%' . $keyword . '%')
                    ->where('active', 1)
                    ->orderBy('id', 'desc')->get();
            } elseif ($type == 'best_seller') {
                $data = ProductModel::where('name', 'like', '%' . $keyword . '%')
                    ->where('active', 1)
                    ->orderBy('is_sell','desc')->get();
            }
            if (count($data)) {
                $dataKey = KeySearchModel::where('title', '=', $keyword_1)->first();
                if ($dataKey) {
                    $dataKey->number += 1;
                    $dataKey->save();
                } else {
                    $data_key = new KeySearchModel();
                    $data_key->title = $keyword_1;
                    $data_key->number = 1;
                    $data_key->save();
                }
                foreach ($data as $key => $value) {
                    $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
                    $avg_rating = round($total_rating, 1);
                    $data[$key]->avg_rating = $avg_rating;
                }
            }

            $data_shop = ShopModel::where('name', 'like', '%' . $keyword . '%')->first();
            if ($data_shop) {
                $count_product = ProductModel::where('shop_id', $data_shop['id'])->where('active', 1)->count() ?? 0;
                $count_review_1 = ReviewProductModel::where('shop_id', $data_shop['id'])->avg('ratings') ?? 0;
                $count_review = round($count_review_1, 1);
                $data_follow_shop = FollowShopModel::where('shop_id', $data_shop['id'])->count();
                $data_following = FollowShopModel::where('customer_id', $data_shop['customer_id'])->count();
                $data_shop['count_product'] = $count_product;
                $data_shop['rating'] = $count_review;
                $data_shop['follow'] = $data_follow_shop;
                $data_shop['following'] = $data_following;
            }
            $dataReturn = [
                'status' => true,
                'data' => $data,
                'data_shop' => $data_shop ?? null,
                'keyword' => $keyword_1,
                'title' => 'Kết quả tìm kiếm với: ' . $keyword_1,
                'type' => $type

            ];
        }
        if ($keyword == null) {
            $dataReturn = [
                'status' => true,
                'data' => null,
                'data_shop' => null,
                'keyword' => null,
                'title' => 'Kết quả tìm kiếm với: ' . null,
                'type' => $type
            ];
        }
        return view('frontend.search.index', $dataReturn);
    }

    public function searchAddress(Request $request)
    {
        try{
            $customer = session()->get('data_customer');
            $keyword = $request->get('keyword');
            if (isset($keyword)){
                $data_address = AddressCustomerCTVModel::where('customer_id', $customer->id)->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('phone', 'LIKE', '%'.$keyword.'%')->get();
                foreach ($data_address as $key => $value){
                    $city = CityModel::find($value->city);
                    $data_address[$key]->city_name = $city->name;
                    $district = DistrictModel::find($value->district);
                    $data_address[$key]->district_name = $district->name;
                    $ward = WardModel::find($value->ward);
                    $data_address[$key]->ward_name = $ward->name;
                }
            }else{
                $data_address = AddressCustomerCTVModel::where('customer_id', $customer->id)->get();
                foreach ($data_address as $key => $value){
                    $city = CityModel::find($value->city);
                    $data_address[$key]->city_name = $city->name;
                    $district = DistrictModel::find($value->district);
                    $data_address[$key]->district_name = $district->name;
                    $ward = WardModel::find($value->ward);
                    $data_address[$key]->ward_name = $ward->name;
                }
            }
            $data['address_ctv'] = $data_address;
            $views = view('frontend.cart.address_ctv', $data)->render();
            return response()->json(['prod' => $views]);
        }catch (\Exception $exception){
            return $exception;
        }
    }
}
