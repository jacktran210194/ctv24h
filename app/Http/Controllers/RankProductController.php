<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RankProductController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $type = $request->type === 'shop' ? 'shop' : 'product';

        $data_return = [
            'type' => $type
        ];

        return view('frontend.rank_product.index', $data_return);
    }
}
