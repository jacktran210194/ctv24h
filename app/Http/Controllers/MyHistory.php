<?php

namespace App\Http\Controllers;

use App\Models\AttributeValueModel;
use App\Models\CoinModel;
use App\Models\CustomerModel;
use App\Models\HistoryDealCTV24;
use App\Models\HistoryOrderPaymentCollaboratorModel;
use App\Models\HistoryOrderPaymentModel;
use App\Models\OrderCTVModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\PointModel;
use App\Models\ProductAttributeModel;
use App\Models\ProductModel;
use App\Models\ReviewProductModel;
use App\Models\ShopModel;
use App\Models\WalletCTV24Model;
use App\Models\WardModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MyHistory extends BaseController
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        return view('frontend.my_history.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    //tất cả
    public function by_product()
    {
        $data_login = session()->get('data_customer');
        if ($data_login) {
            $data_order = DB::table('order')->join('shop', 'shop.id', '=', 'order.shop_id')
                ->where([
                    'order.customer_id' => $data_login['id'],
                    'order.is_selected' => 2
                ])
                ->orderBy('order.created_at', 'desc')
                ->select('order.*', 'shop.name as shop_name', 'shop.image as shop_image')
                ->get();
            $data_order_detail = DB::table('order_detail')
                ->join('product', 'product.id', '=', 'order_detail.product_id')
                ->join('attribute_value', 'attribute_value.id', '=', 'order_detail.type_product')
                ->where('order_detail.customer_id', $data_login['id'])
                ->select('order_detail.*', 'attribute_value.value as value_1', 'attribute_value.size_of_value as value_2', 'product.name as product_name', 'product.image as product_image')
                ->get();
            $dataReturn = [
                'title' => 'Đơn mua của tôi',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
            ];
            return view('frontend.my_history.by_product', $dataReturn);
        } else {
            return redirect('login');
        }
    }

    //chờ xác nhận
    public function wait_confirm(){
        $data_login = session()->get('data_customer');
        if ($data_login) {
            $data_order = DB::table('order')->join('shop', 'shop.id', '=', 'order.shop_id')
                ->where([
                    'order.customer_id' => $data_login['id'],
                    'order.is_selected' => 2,
                    'order.status' => 0
                ])
                ->orderBy('order.created_at', 'desc')
                ->select('order.*', 'shop.name as shop_name', 'shop.image as shop_image')
                ->get();
            $data_order_detail = DB::table('order_detail')
                ->join('product', 'product.id', '=', 'order_detail.product_id')
                ->join('attribute_value', 'attribute_value.id', '=', 'order_detail.type_product')
                ->where('order_detail.customer_id', $data_login['id'])
                ->select('order_detail.*', 'attribute_value.value as value_1', 'attribute_value.size_of_value as value_2', 'product.name as product_name', 'product.image as product_image')
                ->get();
            $dataReturn = [
                'title' => 'Đơn mua của tôi',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
            ];
            return view('frontend.my_history.wait_confirm', $dataReturn);
        } else {
            return redirect('login');
        }
    }

    public function wait_shipping(){
        $data_login = session()->get('data_customer');
        if ($data_login) {
            $data_order = DB::table('order')->join('shop', 'shop.id', '=', 'order.shop_id')
                ->where([
                    'order.customer_id' => $data_login['id'],
                    'order.is_selected' => 2,
                    'order.status' => 1
                ])
                ->orderBy('order.created_at', 'desc')
                ->select('order.*', 'shop.name as shop_name', 'shop.image as shop_image')
                ->get();
            $data_order_detail = DB::table('order_detail')
                ->join('product', 'product.id', '=', 'order_detail.product_id')
                ->join('attribute_value', 'attribute_value.id', '=', 'order_detail.type_product')
                ->where('order_detail.customer_id', $data_login['id'])
                ->select('order_detail.*', 'attribute_value.value as value_1', 'attribute_value.size_of_value as value_2', 'product.name as product_name', 'product.image as product_image')
                ->get();
            $dataReturn = [
                'title' => 'Đơn mua của tôi',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
            ];
            return view('frontend.my_history.wait_shipping', $dataReturn);
        } else {
            return redirect('login');
        }
    }

    //đang giao hàng
    public function shipping(){
        $data_login = session()->get('data_customer');
        if ($data_login) {
            $data_order = DB::table('order')->join('shop', 'shop.id', '=', 'order.shop_id')
                ->where([
                    'order.customer_id' => $data_login['id'],
                    'order.is_selected' => 2,
                    'order.status' => 2
                ])
                ->orderBy('order.created_at', 'desc')
                ->select('order.*', 'shop.name as shop_name', 'shop.image as shop_image')
                ->get();
            $data_order_detail = DB::table('order_detail')
                ->join('product', 'product.id', '=', 'order_detail.product_id')
                ->join('attribute_value', 'attribute_value.id', '=', 'order_detail.type_product')
                ->where('order_detail.customer_id', $data_login['id'])
                ->select('order_detail.*', 'attribute_value.value as value_1', 'attribute_value.size_of_value as value_2', 'product.name as product_name', 'product.image as product_image')
                ->get();
            $dataReturn = [
                'title' => 'Đơn mua của tôi',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
            ];
            return view('frontend.my_history.shipping', $dataReturn);
        } else {
            return redirect('login');
        }
    }

    public function shipped(){
        $data_login = session()->get('data_customer');
        if ($data_login) {
            $data_order = DB::table('order')->join('shop', 'shop.id', '=', 'order.shop_id')
                ->where([
                    'order.customer_id' => $data_login['id'],
                    'order.is_selected' => 2,
                    'order.status' => 3
                ])
                ->orderBy('order.created_at', 'desc')
                ->select('order.*', 'shop.name as shop_name', 'shop.image as shop_image')
                ->get();
            $data_order_detail = DB::table('order_detail')
                ->join('product', 'product.id', '=', 'order_detail.product_id')
                ->join('attribute_value', 'attribute_value.id', '=', 'order_detail.type_product')
                ->where('order_detail.customer_id', $data_login['id'])
                ->select('order_detail.*', 'attribute_value.value as value_1', 'attribute_value.size_of_value as value_2', 'product.name as product_name', 'product.image as product_image')
                ->get();
            $dataReturn = [
                'title' => 'Đơn mua của tôi',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
            ];
            return view('frontend.my_history.shipped', $dataReturn);
        } else {
            return redirect('login');
        }
    }

    //huỷ
    public function cancel(){
        $data_login = session()->get('data_customer');
        if ($data_login) {
            $data_order = DB::table('order')->join('shop', 'shop.id', '=', 'order.shop_id')
                ->where([
                    'order.customer_id' => $data_login['id'],
                    'order.is_selected' => 2,
                    'order.status' => 4
                ])
                ->orderBy('order.created_at', 'desc')
                ->select('order.*', 'shop.name as shop_name', 'shop.image as shop_image')
                ->get();
            $data_order_detail = DB::table('order_detail')
                ->join('product', 'product.id', '=', 'order_detail.product_id')
                ->join('attribute_value', 'attribute_value.id', '=', 'order_detail.type_product')
                ->where('order_detail.customer_id', $data_login['id'])
                ->select('order_detail.*', 'attribute_value.value as value_1', 'attribute_value.size_of_value as value_2', 'product.name as product_name', 'product.image as product_image')
                ->get();
            $dataReturn = [
                'title' => 'Đơn mua của tôi',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
            ];
            return view('frontend.my_history.cancel', $dataReturn);
        } else {
            return redirect('login');
        }
    }

    //trả hàng
    public function return_order(){
        $data_login = session()->get('data_customer');
        if ($data_login) {
            $data_order = DB::table('order')->join('shop', 'shop.id', '=', 'order.shop_id')
                ->where([
                    'order.customer_id' => $data_login['id'],
                    'order.is_selected' => 2,
                    'order.status' => 5
                ])
                ->orderBy('order.created_at', 'desc')
                ->select('order.*', 'shop.name as shop_name', 'shop.image as shop_image')
                ->get();
            $data_order_detail = DB::table('order_detail')
                ->join('product', 'product.id', '=', 'order_detail.product_id')
                ->join('attribute_value', 'attribute_value.id', '=', 'order_detail.type_product')
                ->where('order_detail.customer_id', $data_login['id'])
                ->select('order_detail.*', 'attribute_value.value as value_1', 'attribute_value.size_of_value as value_2', 'product.name as product_name', 'product.image as product_image')
                ->get();
            $dataReturn = [
                'title' => 'Đơn mua của tôi',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
            ];
            return view('frontend.my_history.return_order', $dataReturn);
        } else {
            return redirect('login');
        }
    }

    /**
     * check review product
     */
//    protected function checkReviewProduct($customer_id, $order_detail_id)
//    {
//        $checkReivew = ReviewProductModel::where('customer_id', $customer_id)
//            ->where('order_detail_id', $order_detail_id)
//            ->get();
//        $is_review = true;
//        if (count($checkReivew) > 0) {
//            $is_review = false;
//        }
//        return $is_review;
//    }

//    protected function getDataVlueAttr($value)
//    {
//        $item = explode(',', $value);
//        $data = [];
//        if (isset($item)) {
//            foreach ($item as $v) {
//                $__dataAttrValue = AttributeValueModel::find($v);
//                if (isset($__dataAttrValue)) {
//                    $__dataAttrProduct = ProductAttributeModel::find($__dataAttrValue->attribute_id);
//                    $__data['type'] = isset($__dataAttrProduct) ? $__dataAttrProduct['name'] : '';
//                    $__data['value'] = isset($__dataAttrValue) ? $__dataAttrValue['value'] : '';
//                    array_push($data, $__data);
//                }
//            }
//        }
//        return $data;
//    }

    //yeu cau Huỷ đơn hàng
    public function cancel_order(Request $request)
    {
        $data_customer = session()->get('data_customer');
        $customer = CustomerModel::find($data_customer->id);
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $dataOrder = OrderModel::find($request->order_id);
        if ($dataOrder['status'] == 0) {
            $dataOrder->cancel_confirm = 1;
            $dataOrder->cancel_user = 1;
            $dataOrder->status = 4;
            $dataOrder->cancel_reason = $request->cancel_reason;
            $dataOrder->created_at = $today;
            $dataOrder->save();
        } else {
            $dataOrder->cancel_confirm = 0;
            $dataOrder->cancel_user = 1;
            $dataOrder->status = 4;
            $dataOrder->cancel_reason = $request->cancel_reason;
            $dataOrder->created_at = $today;
            $dataOrder->save();
        }
        $data_history_supplier = HistoryOrderPaymentModel::where('order_id', $dataOrder['id'])->first();
        $data_history_supplier->status = 2;
        $data_history_supplier->save();
        if ($dataOrder['collaborator_id'] == 1) {
            $data_history_collaborator = HistoryOrderPaymentCollaboratorModel::where('order_id', $dataOrder['id'])->first();
            $data_history_collaborator->status = 2;
            $data_history_collaborator->save();
        }
        $data_deal_ctv24h = HistoryDealCTV24::where('order_id', $dataOrder['id'])->first();
        $data_deal_ctv24h->status = 2;
        $data_deal_ctv24h->save();
        if ($dataOrder->payment_id == 0){
            $customer->wallet = $dataOrder->total_price + $customer->wallet;
            $customer->save();
        }
        $this->returnQuantityProduct($dataOrder);
        $dataReturn = [
            'status' => true,
            'msg' => 'Yêu cầu huỷ đơn của bạn đã được xác nhận !',
            'url' => url('my_history/by_product'),
        ];
        return $dataReturn;
    }

    /**
     * return quantity product
    **/

    protected function returnQuantityProduct($order)
    {
        $order_detail = OrderDetailModel::where('order_id', $order->id)->get();
        foreach ($order_detail as $value){
            $product_attr = AttributeValueModel::find($value->type_product);
            $product_attr->quantity = $product_attr->quantity + $value->quantity;
            $product_attr->save();
        }
        return true;
    }

    //Xác nhận đã nhận hàng
    public function check_product(Request $request)
    {
        $dataLogin = session()->get('data_customer');
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $data_order = OrderModel::find($request->id);
        $data_order->status_confirm = 1;
        $data_order->save();
        $data_order_detail = OrderDetailModel::where('order_id', $data_order['id'])->get();
        foreach ($data_order_detail as $key => $value) {
            $data_order_detail[$key]['status_confirm'] = 1;
            $data_order_detail[$key]['status'] = 3;
            $data_order_detail[$key]->save();
            $data_product = ProductModel::find($value['product_id']);
            $data_product->is_sell += $value->quantity;
            $data_product->save();
            $dataAttribute = AttributeValueModel::find($value['type_product']);
            $dataAttribute->is_sell += $value->quantity;
            $dataAttribute->save();
            $order_ctv = OrderCTVModel::where('order_id', $value->order_id)->get();
            if($order_ctv){
                foreach ($order_ctv as $item){
                    $shop = ShopModel::find($item->shop_id);
                    $customer = CustomerModel::find($shop->customer_id);
                    $customer->wallet = $customer->wallet + $item->profit;
                    $customer->save();
                }
            }
        }

        //Cộng xu khi xác nhận Đã nhận được hàng và cộng điểm
        $coin = $data_order->total_price_product * 0.1 / 100;
        $point = $data_order->total_price_product * 0.1 / 100;
        $dataCoin = new CoinModel([
            'customer_id' => $dataLogin->id,
            'value' => $coin,
            'reason' => 2,
            'created_at' => $today
        ]);
        $dataCoin->save();
        $dataPoint = new PointModel([
            'customer_id' => $dataLogin->id,
            'point' => $point,
            'type' => 1,
            'title' => 'Cộng điểm từ đơn hàng #' . $data_order->order_code,
            'date' => $today
        ]);
        $dataPoint->save();
        $dataCustomer = CustomerModel::find($dataLogin->id);
        $dataCustomer->point += $point;
        $dataCustomer->coin += $coin;
        $dataCustomer->save();
        if ($data_order->status_confirm == 1) {
            //doanh thu của ncc
            $data_history_order_payment = HistoryOrderPaymentModel::where('order_id', $data_order->id)->first();
            $data_history_order_payment->status = 1;
            $data_history_order_payment->created_at = $today;
            $data_history_order_payment->type = 1;
            $data_history_order_payment->save();
            $data_shop = ShopModel::find($data_order->shop_id);
            $data_customer_ncc = CustomerModel::find($data_shop->customer_id);
            $data_customer_ncc->wallet += $data_history_order_payment->price;
            $data_customer_ncc->save();
            //end doanh thu ncc

            //doanh thu sàn
            $data_history_deal_ctv24h = HistoryDealCTV24::where('order_id', $data_order->id)->first();
            $data_history_deal_ctv24h->status = 1;
            $data_history_deal_ctv24h->created_at = $today;
            $data_history_deal_ctv24h->type = 1;
            $data_history_deal_ctv24h->save();
            $data_wallet_ctv24h_old = WalletCTV24Model::first();
            if ($data_wallet_ctv24h_old) {
                $data_wallet_ctv24h_old->price += $data_history_deal_ctv24h->price;
                $data_wallet_ctv24h_old->save();
            } else {
                $data_wallet_ctv24h = new WalletCTV24Model();
                $data_wallet_ctv24h->price = $data_history_deal_ctv24h->price;
                $data_wallet_ctv24h->save();
            }
            //end doanh thu sàn

            if ($data_order->collaborator_id == 1) {
                //Doanh thu CTV
                $dataHistoryOrderPaymentCTV = HistoryOrderPaymentCollaboratorModel::where('order_id', $data_order->id)->first();
                $dataHistoryOrderPaymentCTV->status = 1;
                $dataHistoryOrderPaymentCTV->created_at = $today;
                $dataHistoryOrderPaymentCTV->type = 1;
                $dataHistoryOrderPaymentCTV->save();
                $dataCustomer = CustomerModel::find($dataLogin->id);
                if ($data_order->payment_id == 0){
                    $dataCustomer->wallet = $dataCustomer->wallet + $data_order->total_price;
                }else{
                    $dataCustomer->wallet += $dataHistoryOrderPaymentCTV->price;
                }
                $dataCustomer->save();
                //End Doanh thu CTV
            }
        }
        $dataReturn = [
            'status' => true,
            'msg' => 'Xác nhận đơn hàng thành công !',
            'url' => url('my_history/by_product')
        ];
        return $dataReturn;
    }

//Trả hàng
    public
    function return_product($id)
    {
        /**
         *
         *
         */
    }

    //Danh sách order detail
    public function list_order($id){
        $data_order_detail = DB::table('order_detail')
            ->join('product', 'product.id', '=', 'order_detail.product_id')
            ->join('attribute_value', 'attribute_value.id', '=', 'order_detail.type_product')
            ->where('order_detail.order_id','=', $id)
            ->select('order_detail.*', 'attribute_value.value as value_1', 'attribute_value.size_of_value as value_2', 'product.name as product_name', 'product.image as product_image')
            ->get();
        $data_return = [
          'status' => true,
          'data_order_detail' => $data_order_detail
        ];
        return view('frontend.my_history.list_order',$data_return);
    }
}
