<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SupplierBaseController extends Controller
{
    public function __construct(Request $request)
    {
        if (!$request->session()->has('data_supplier')) {
            Redirect::to('admin/supplier_admin/login')->send();
        }
        $session = $request->session()->get('data_supplier');
        if(!$session->supplier_admin){
            Redirect::to('admin/login')->send();
        } else {
            $request->session()->put('data_supplier', $session);
        }
    }
}
