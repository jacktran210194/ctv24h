<?php
namespace App\Http\Controllers;
use App\Models\CategoryShopModel;
use App\Models\CollaboratorCategoryProductModel;
use App\Models\CollaboratorModel;
use App\Models\ProductModel;
use App\Models\ShopModel;
use Illuminate\Http\Request;

class DropShipController extends Controller
{
    public function index(){
        $data_customer = session()->get('data_customer');
        $shop = ShopModel::where('customer_id', $data_customer->id)->first();
        if (isset($data_customer)){
            $category = CategoryShopModel::where('shop_id', $shop->id)->where('created_by', 0)->where('status', 1)->get();
            $data_return = [
                'category' => $category,
                'shop' => $shop
            ];
            return view('frontend.drop_ship.index', $data_return);
        }else{
            return redirect('/login');
        }
    }
    public function addCategory(Request $request){
        $data_customer = session()->get('data_customer');
        $shop = ShopModel::where('customer_id', $data_customer->id)->first();
        if ($this->check($shop->id, $request->name)){
            $category = new CategoryShopModel([
                'name' => $request->name,
                'shop_id' => $shop->id,
                'created_by' => 0,
                'status' => 1
            ]);
            $category->save();
            $data_return =[
                'status' => true
            ];
        }else{
            $data_return = [
              'status' => false,
              'msg' => 'Danh mục đã tồn tại'
            ];
        }
        return response()->json($data_return, 200);
    }

    protected function check($shop_id, $name)
    {
        $category = CategoryShopModel::where('shop_id', $shop_id)->where('name', $name)->first();
        if (isset($category)){
            $check= false;
        }else{
            $check = true;
        }
        return $check;
    }
}
