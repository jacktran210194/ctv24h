<?php

namespace App\Http\Controllers;

use App\Models\FollowShopModel;
use App\Models\LovelyShopModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class FollowShopController extends Controller
{
    public function __construct(Request $request)
    {
        if (!$request->session()->has('data_customer')) {
            Redirect::to('login')->send();
        }
    }

    /**
     *  Theo dõi shop
     * @param Request $request
     * @return array
     */
    public function followShop(Request $request)
    {
        $dataCustomer = session()->get('data_customer');
        $data = FollowShopModel::where('customer_id', $dataCustomer->id)
            ->where('shop_id', $request->shop_id)
            ->first();
        if(isset($dataCustomer)) {
            if (!$data) {
                $data = new FollowShopModel();
                $data->customer_id = $dataCustomer->id;
                $data->shop_id = $request->shop_id;
                $data->save();
            }
            $data_id = $request->shop_id;
            $data_url = url('seeshop/unfollow/'.$request->shop_id);
            $html = '
                 <button style="width: 120px!important;" class="button-unfollow btn-shop-followed button"
                            data-id="'.$data_id.'"
                            data-url="'.$data_url.'"
                    >
                        <object data="Icon/products/gold_shop.svg" width="12px"></object>
                        Bỏ Theo Dõi
                    </button>
            ';
            $count_follow_shop = FollowShopModel::where('shop_id', $request->shop_id)->count();
            $dataReturn = [
                'status' => true,
                'msg' => 'Đã theo dõi',
                'number_follow' => $count_follow_shop,
                'html' => $html
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Vui lòng thử lại',
            ];
        }
        return $dataReturn;
    }

    /**
     *  Bỏ theo dõi shop
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function unfollowShop(Request $request, $id)
    {
        try {
            $dataCustomer = session()->get('data_customer');
            FollowShopModel::where('shop_id', $id)->where('customer_id', $dataCustomer->id)->delete();
            $data_id = $id;
            $data_url = url('seeshop/follow');
            $html = '
                 <button style="width: 120px!important;" class="button-follow btn-shop button"
                            data-id="'.$data_id.'"
                            data-url="'.$data_url.'"
                    >
                        <object data="Icon/products/gold_shop.svg" width="12px"></object>
                        Theo Dõi
                    </button>
            ';
            $count_follow_shop = FollowShopModel::where('shop_id', $id)->count();
            $dataReturn = [
                'status' => true,
                'msg' => 'Bỏ theo dõi thành công',
                'number_follow' => $count_follow_shop,
                'html' => $html
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
