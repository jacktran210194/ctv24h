<?php

namespace App\Http\Controllers;

use App\Models\FavouriteProductModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class FavouriteProductController extends Controller
{
    /**
     *  Thích sản phẩm
     * @param $id
     * @return array|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function like($id)
    {
        $dataCustomer = session()->get('data_customer');
        if(!$dataCustomer){
            return redirect('login');
        }
        if(isset($dataCustomer)){
            $data = FavouriteProductModel::where('customer_id', $dataCustomer->id)
                ->where('product_id', $id)
                ->first();
            if (!$data) {
                $data = FavouriteProductModel::create([
                    'customer_id' => $dataCustomer->id,
                    'product_id' => $id,
                ]);
                $data->save();
            }
            $data_id = $id;
            $data_url = url('details_product/'.$id.'/dislike');
            $html = '
                <img data-id="'.$data_id.'" data-url="'.$data_url.'" class="dislike-product" src="Icon/like/like_xanh.png">
            ';
            $dataReturn = [
                'status' => true,
                'msg' => 'Đã thích',
                'html' => $html
//                'url' => URL::to('details_product/'.$data->product_id)
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Vui lòng thử lại',
            ];
        }
        return $dataReturn;
    }

    /**
     *  Bỏ thích sản phẩm
     * @param $id
     * @return array
     */
    public function dislike($id)
    {
        try {
            $dataCustomer = session()->get('data_customer');
            FavouriteProductModel::where('product_id', $id)->where('customer_id', $dataCustomer->id)->delete();
            $data_id = $id;
            $data_url = url('details_product/'.$id.'/like');
            $html = '
                <img data-id="'.$id.'" data-url="'.$data_url.'" class="like-product" src="Icon/like/like_xam.png">
            ';
            $dataReturn = [
                'status' => true,
                'msg' => 'Đã bỏ thích',
                'html' => $html
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
