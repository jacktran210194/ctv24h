<?php

namespace App\Http\Controllers;

use App\Models\CustomerModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class LoginUserController extends Controller
{
    public function login(Request $request)
    {
        if ($request->session()->has('data_customer')) {
            Redirect::to('/')->send();
        }
        $dataReturn = [
          'title' => 'Đăng nhập mua hàng | CTV24h'
        ];
        return view('frontend.login.index',$dataReturn);

    }

    public function check(Request $request)
    {
        $section = $request ->session_start;
        $phone = $request->username ?? '';
        $password = $request->password ?? '';
        if ($section == ""){
            $url = URL::to('/');
        }else{
            $url = $section;
        }
        if (empty($phone) || empty($password)) {
            $dataReturn = [
                'status' => false,
                'msg' => 'Vui lòng điền đầy đủ thông tin !'
            ];
        } else {
            if (is_numeric($request->get('username'))) {
                $dataUser = CustomerModel::where('phone', $phone)->where('password', md5($password))->first();

            } else {
                $dataUser = CustomerModel::where('email', $phone)->where('password', md5($password))->first();
            }
            if ($dataUser) {
                $dataUser['customer'] = true;
                $request->session()->put('data_customer', $dataUser);
                $dataReturn = [
                    'status' => true,
                    'msg' => 'Đăng nhập thành công',
                    'url' => $url
                ];
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Đăng nhập không thành công'
                ];
            }
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function logout()
    {
        if (session()->has('data_customer')) {
            session()->flush();
        }
        return redirect('/');
    }

    //Quên mật khẩu
    public function getForgotpassword()
    {
        $dataReturn = [
            'title' => 'Quên mật khẩu',
        ];
        return view('frontend.login.forgot_password', $dataReturn);
    }

    public function checkPhone(Request $request)
    {
        $phone = $request->phone;
        $data = CustomerModel::where('phone', $phone)->first();
        if (!$data) {
            $dataReturn = [
                'status' => false,
                'msg' => 'Số điện thoại bạn vừa nhập chưa đăng ký tài khoản !'
            ];
        } else {
            $request->session()->put('data_phone', $phone);
            $dataReturn = [
                'status' => true,
                'url' => URL::to('forgot_password/new_password'),
                'data' => $phone
            ];
        }
        return $dataReturn;
    }

    public function new_password()
    {
        $dataReturn = [
            'title' => 'Tạo mới mật khẩu',
        ];
        return view('frontend.login.new_password', $dataReturn);
    }

    public function savePassword(Request $request)
    {
        $check_phone = session()->get('data_phone');
        $rules = [
            'password' => 'required|min:8|max:50',
            're_password' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'min' => 'Mật khẩu tối thiểu 8 ký tự !',
            'max' => 'Mật khẩu tối đa 50 ký tự !'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if ($request->password != $request->re_password) {
                $dataReturn['status'] = false;
                $dataReturn['msg'] = 'Mật khẩu không trùng nhau';
            } else {
                $data_user = CustomerModel::where('phone', $check_phone)->first();
                if ($data_user) {
                    $data_user->password = md5($request->password);
                    $data_user->save();
                    session()->flush();
                    $dataReturn = [
                        'status' => true,
                        'msg' => 'Cập nhật mật khẩu thành công !',
                        'url' => URL::to('login')
                    ];
                } else {
                    $dataReturn['status'] = false;
                    $dataReturn['msg'] = 'Đã có lỗi xảy ra';
                }
            }
        }
        return $dataReturn;
    }
}
