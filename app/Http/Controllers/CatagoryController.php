<?php

namespace App\Http\Controllers;

use App\Models\CategoryProductChildModel;
use App\Models\CategoryProductModel;
use App\Models\ProductModel;
use Illuminate\Http\Request;
use DB;

class CatagoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('frontend.catagory.index');
    }
}
