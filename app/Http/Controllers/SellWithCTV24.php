<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SellWithCTV24 extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('frontend.sell_with_ctv24.index');
    }
}
