<?php

namespace App\Http\Controllers;

use App\Models\OrderModel;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
    //
    public function updateShipment(Request $request)
    {
        try {
            //code...
            $where = [
                'label' => $request->label_id,
                'partner_id' => $request->partner_id
            ];
            $order = OrderModel::where($where)->first();
            if ($request->status_id == OrderModel::CANCEL_ORDER || $request->status_id == OrderModel::NOT_DELIVERY_ORDER) {
                $orderUpdate['status'] = OrderModel::CANCEL_STATUS;
            }
            if ($request->status_id == OrderModel::DELIVERED_ORDER) {
                $orderUpdate['status'] = OrderModel::DELIVERED_STATUS;
            }
            if ($request->status_id == OrderModel::DELIVERING_ORDER) {
                $orderUpdate['status'] = OrderModel::DELIVERING_STATUS;
            }
            $data = $order->update($orderUpdate);
            if ($data) {
                return response()->json([
                    'code' => 200,
                    'order' => $order
                ]);
            }
            return response()->json([
                'code' => 500,
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'code' => 500,
            ]);
        }
    }
}
