<?php

namespace App\Http\Controllers;

use App\Models\AttributeValueModel;
use App\Models\BrandModel;
use App\Models\BuyMorePercent;
use App\Models\CartModel;
use App\Models\CategoryProductChildModel;
use App\Models\CategoryProductModel;
use App\Models\CityModel;
use App\Models\CollaboratorModel;
use App\Models\CustomerAddressModel;
use App\Models\CustomerModel;
use App\Models\DistrictModel;
use App\Models\FavouriteProductModel;
use App\Models\FollowShopModel;
use App\Models\GroupMessengerModel;
use App\Models\LovelyShopModel;
use App\Models\MessegeModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductAccessModel;
use App\Models\ProductAttributeModel;
use App\Models\ProductCategoryShopModel;
use App\Models\ProductDropShipModel;
use App\Models\ProductImageModel;
use App\Models\ProductModel;
use App\Models\RatingProductModel;
use App\Models\ReviewProductModel;
use App\Models\SeenProductModel;
use App\Models\ShopModel;
use App\Models\SupplierModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use phpDocumentor\Reflection\Type;

class ProductsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index($id)
    {
        $dataCustomer = session()->get('data_customer');
        if ($dataCustomer) {
            $data_order_old = OrderModel::where('customer_id', $dataCustomer->id)->where('is_selected', 0)->get();
            if ($data_order_old) {
                foreach ($data_order_old as $key => $value) {
                    $data_order_old[$key]->delete();
                    unset($data_order_old[$key]);
                }
            }
            $address_customer = CustomerAddressModel::where('customer_id', $dataCustomer->id)->where('default', 1)->first();
        }

        $data = ProductModel::find($id);
        if (!$data) {
            return redirect('/');
        }
        if ($data->dropship == 1){
            if ( $data->active_drop_ship != 2 ){
                return redirect('/');
            }
        }
        $data_like_product = FavouriteProductModel::where('product_id', $id)->count() ?? 0;
        $percent = ceil((100 - ($data->price_discount / $data->price) * 100));
        $data['percent'] = $percent;
        $place = $data['place_district'] . ', ' . $data['place'];
        $data['place_product'] = $place;
        if (isset($dataCustomer)) {
            $data_address_customer = CustomerAddressModel::where('customer_id', $dataCustomer->id)->where('default', 1)->first();
            if ($data_address_customer) {
                $data_city = CityModel::find($data_address_customer->city_id);
                $data_district = DistrictModel::find($data_address_customer->district_id);
                $send_to = $data_district->name . ', ' . $data_city->name;
                $data['send_to'] = $send_to;
            } else {
                $data['send_to'] = null;
            }
        }
        $dataCategory = CategoryProductChildModel::find($data->category_id);
        $data->name_category = $dataCategory->name;
        if ($data->brand == 0) {
            $data->name_brand = 'No Brand';
        } else {
            $dataBrand = BrandModel::find($data->brand);
            $data->name_brand = $dataBrand->name;
        }

        $dataShop = ShopModel::find($data->shop_id);
        $customer_id = CustomerModel::where('id', $dataShop->customer_id)->first();
        $dataShop->name_shop = $dataShop->name ?: '';
        $dataOrtherProduct = ProductModel::where('shop_id', $data->shop_id)->where('id', '!=', $id)
            ->where('active', 1)
            ->orderBy('id', 'desc')
            ->get();
        if (count($dataOrtherProduct)) {
            foreach ($dataOrtherProduct as $key => $value) {
                $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
                $avg_rating = round($total_rating, 1);
                $dataOrtherProduct[$key]->avg_rating = $avg_rating;
                $product_percent = BuyMorePercent::where('product_id', $value->id)->get()->last();
                $dataOrtherProduct[$key]->percent = $product_percent;
            }
        }
        $dataSimilarProduct = ProductModel::where('category_id', $data->category_id)
            ->where('active', 1)
            ->where('id', '!=', $id)
            ->orderBy('id', 'desc')
            ->get();
        if (count($dataSimilarProduct)) {
            foreach ($dataSimilarProduct as $key => $value) {
                $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
                $avg_rating = round($total_rating, 1);
                $dataSimilarProduct[$key]->avg_rating = $avg_rating;
                $product_percent = BuyMorePercent::where('product_id', $value->id)->get()->last();
                $dataSimilarProduct[$key]->percent = $product_percent;
            }
        }
        $dataCountProduct = ProductModel::where('shop_id', $data->shop_id)
                ->where('active', 1)
                ->count() ?? 0;

        $data_image = ProductImageModel::where('product_id', $id)->get();

        $title_group_attr = ProductAttributeModel::where('product_id',$id)->first();
        $title_value_attr = AttributeValueModel::where('product_id',$id)->first();

        $data_attr = ProductAttributeModel::where('product_id',$id)->get();
        $data_item = AttributeValueModel::where('product_id',$id)->get();


        if ($dataCustomer) {
            $data_group_messenger = GroupMessengerModel::where(function ($query) use ($dataCustomer, $dataShop) {
                $query->where('user_id_1', $dataCustomer->id)
                    ->where('user_id_2', $dataShop->customer_id);
            })->orWhere(function ($query) use ($dataCustomer, $dataShop) {
                $query->where('user_id_1', $dataShop->customer_id)
                    ->where('user_id_2', $dataCustomer->id);
            })->first();
        } else {
            $data_group_messenger = null;
        }
        if ($dataCustomer) {
            $dataSeenProduct = SeenProductModel::where('product_id', $id)
                ->where('customer_id', $dataCustomer->id)
                ->first();
        }
        if ($dataCustomer && !$dataSeenProduct) {
            $seenProduct = new SeenProductModel([
                'product_id' => $data->id,
                'customer_id' => $dataCustomer->id
            ]);
            $seenProduct->save();
        }
        if ($dataCustomer) {
            $dataGetSeenProduct = DB::table('seen_product')
                ->select('product.*')
                ->join('product', 'seen_product.product_id', '=', 'product.id')
                ->where('seen_product.customer_id', $dataCustomer->id)
                ->orderBy('seen_product.created_at', 'desc')
                ->limit(10)
                ->get();
            foreach ($dataGetSeenProduct as $key => $value){
                $product_percent = BuyMorePercent::where('product_id', $value->id)->get()->last();
                $dataGetSeenProduct[$key]->percent = $product_percent;
            }
        } else {
            $dataGetSeenProduct = null;
        }
        if ($dataGetSeenProduct) {
            foreach ($dataGetSeenProduct as $key => $value) {
                $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
                $avg_rating = round($total_rating, 1);
                $dataGetSeenProduct[$key]->avg_rating = $avg_rating;
            }
        }
        $review = ReviewProductModel::where('product_id', $id)->paginate(2);
        $count = ReviewProductModel::where('product_id', $id)->count();
        $sum = ReviewProductModel::where('product_id', $id)->sum('ratings');
        if ($sum == 0 || $count == 0) {
            $average_review = 0;
        } else {
            $average_review = round($sum / $count, 1);
        }
        $count_5_star = ReviewProductModel::where('product_id', $id)->where('ratings', 5)->count();
        $count_4_star = ReviewProductModel::where('product_id', $id)->where('ratings', 4)->count();
        $count_3_star = ReviewProductModel::where('product_id', $id)->where('ratings', 3)->count();
        $count_2_star = ReviewProductModel::where('product_id', $id)->where('ratings', 2)->count();
        $count_1_star = ReviewProductModel::where('product_id', $id)->where('ratings', 1)->count();

        //đếm lượt truy cập sp
        if ($dataCustomer) {
            $today = Carbon::now('Asia/Ho_Chi_Minh');
            $data_address_customer = ProductAccessModel::where('product_id', $id)->where('customer_id', $dataCustomer->id)->first();
            if ($data_address_customer) {
                $data_address_customer->created_at = $today;
                $data_address_customer->save();
            } else {
                $data_product_access = new ProductAccessModel([
                    'product_id' => $data->id,
                    'shop_id' => $data->shop_id,
                    'customer_id' => $dataCustomer->id,
                    'created_at' => $today
                ]);
                $data_product_access->save();
            }
        }
        //-------check drop ship---------//
        if (isset($dataCustomer)){
            $shop = ShopModel::where('customer_id', $dataCustomer->id)->first();
            $product_shop = ProductModel::where('product_id', $id)->orWhere('product_drop_ship_id', $id)
                ->where('shop_id', $shop->id)->first();
            if (isset($product_shop)){
                $check = true;
            }else{
                $check = false;
            }
        }
        //sản phẩm bán chạy
        $data_seller = ProductModel::where('shop_id', $data->shop_id)->where('is_sell', '>', 0)
            ->where('active', 1)->orderBy('is_sell', 'desc')->limit(5)->get();
        if (count($data_seller)) {
            foreach ($data_seller as $key => $value) {
                $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
                $avg_rating = round($total_rating, 1);
                $data_seller[$key]->avg_rating = $avg_rating;
            }
        }
        /**
         * buy more percent
         */
        $data_percent = BuyMorePercent::where('product_id', $id)->get();
        $product_attribute = AttributeValueModel::where('product_id', $id)->first();
        $percent_attribute = ( $product_attribute->price - $product_attribute->price_ctv ) * 100 / $product_attribute->price;

        $response_rate = $this->responseRate($dataShop);
        $dataReturn = [
            'title' => 'Chi tiết sản phẩm',
            'data' => $data,
            'dataOrtherProduct' => $dataOrtherProduct,
            'dataSimilarProduct' => $dataSimilarProduct,
            'dataShop' => $dataShop,
            'dataCountProduct' => $dataCountProduct,
            'data_image' => $data_image,
            'title_group_attr' => $title_group_attr,
            'title_value_attr' => $title_value_attr,
            'attr' => $data_attr,
//            'value_of_category_1' => $value_of_category_1,
            'item' => $data_item,
            'dataGetSeenProduct' => $dataGetSeenProduct,
            'dataMessenger' => $data_group_messenger,
            'data_review' => $review,
            'average_review' => $average_review,
            'total_comment' => $count,
            'count_5_star' => $count_5_star,
            'count_4_star' => $count_4_star,
            'count_3_star' => $count_3_star,
            'count_2_star' => $count_2_star,
            'count_1_star' => $count_1_star,
            'id_customer' => $customer_id->id,
            'check_drop_ship' => $check ?? null,
            'data_customer' => $dataCustomer ?? null,
            'count_like' => $data_like_product,
            'data_seller' => $data_seller,
            'data_percent' => $data_percent,
            'product_attribute' => $product_attribute,
            'percent' => ceil($percent_attribute),
            'response_rate' => ceil($response_rate),
            'address_customer' => isset($address_customer) ? $address_customer : null
        ];
        return view('frontend.products.index', $dataReturn);
    }

    //--------------get response rate shop---------//
    public function responseRate($shop)
    {
        $customer = CustomerModel::find($shop->customer_id);
        $total_group = GroupMessengerModel::where('user_id_2', $customer->id)->count();
        $response = 0;
        if ($total_group > 0){
            $group_messenger = GroupMessengerModel::where('user_id_2', $customer->id)->get();
            foreach ($group_messenger as $value){
                $messenger = MessegeModel::where('group_messenger_id', $value->id)->where('user_send_id', $customer->id)->first();
                if (isset($messenger)){
                    $response += 1;
                }
            }
            $response_rate = $response / $total_group * 100;
        }else{
            $response_rate = 0;
        }
        return $response_rate;
    }
    //all review
    public function getAllReview($id){
        $data_review = DB::table('review_product')
            ->join('shop','shop.id','=','review_product.shop_id')
            ->where('review_product.product_id',$id)
            ->select('shop.name as shop_name','shop.image as shop_image','review_product.*')
            ->paginate(2);
        $dataReturn = [
            'data_review' => $data_review,
        ];
        return view('frontend.products.all_review', $dataReturn);
    }

    /**
     *  lấy video ảnh đánh giá sản phẩn
     * @param Request $request
     * @return array
     */
    public function getDataVideoImageReview($id)
    {
        $data_video_image = ReviewProductModel::where('product_id', $id)->where('is_image', 1)->paginate(2);
        foreach ($data_video_image as $key => $value) {
            $data_shop = ShopModel::find($value->shop_id);
            $data_video_image[$key]['shop_name'] = $data_shop->name;
            $data_video_image[$key]['shop_image'] = $data_shop->image;
        }
        $dataReturn = [
            'data_video_image' => $data_video_image,
        ];
        return view('frontend.products.video_image_review', $dataReturn);
    }

    /**
     *  lấy bình luận sản phẩn
     * @param Request $request
     * @return array
     */
    public function getDataCommentReview($id)
    {
        $data_comment = ReviewProductModel::where('product_id', $id)->where('is_image', 0)->paginate(2);
        foreach ($data_comment as $key => $value) {
            $data_shop = ShopModel::find($value->shop_id);
            $data_comment[$key]['shop_name'] = $data_shop->name;
            $data_comment[$key]['shop_image'] = $data_shop->image;
        }
        $dataReturn = [
            'data_comment' => $data_comment,
        ];
        return view('frontend.products.comment_review', $dataReturn);
    }

    /**
     *  lấy đánh giá 1 sao
     * @param Request $request
     * @return array
     */
    public function getOneStarReview($id)
    {
        $data_star = ReviewProductModel::where('product_id', $id)->where('ratings', 1)->paginate(2);
        foreach ($data_star as $key => $value) {
            $data_shop = ShopModel::find($value->shop_id);
            $data_star[$key]['shop_name'] = $data_shop->name;
            $data_star[$key]['shop_image'] = $data_shop->image;
        }
        $dataReturn = [
            'data_star' => $data_star,
        ];
        return view('frontend.products.review_one_star', $dataReturn);
    }

    /**
     *  lấy đánh giá 2 sao
     * @param Request $request
     * @return array
     */
    public function getTwoStarReview($id)
    {
        $data_star = ReviewProductModel::where('product_id', $id)->where('ratings', 2)->paginate(2);
        foreach ($data_star as $key => $value) {
            $data_shop = ShopModel::find($value->shop_id);
            $data_star[$key]['shop_name'] = $data_shop->name;
            $data_star[$key]['shop_image'] = $data_shop->image;
        }
        $dataReturn = [
            'data_star' => $data_star,
        ];
        return view('frontend.products.review_two_star', $dataReturn);
    }

    /**
     *  lấy đánh giá 3 sao
     * @param Request $request
     * @return array
     */
    public function getThreeStarReview($id)
    {
        $data_star = ReviewProductModel::where('product_id', $id)->where('ratings', 3)->paginate(2);
        foreach ($data_star as $key => $value) {
            $data_shop = ShopModel::find($value->shop_id);
            $data_star[$key]['shop_name'] = $data_shop->name;
            $data_star[$key]['shop_image'] = $data_shop->image;
        }
        $dataReturn = [
            'data_star' => $data_star,
        ];
        return view('frontend.products.review_three_star', $dataReturn);
    }

    /**
     *  lấy đánh giá 4 sao
     * @param Request $request
     * @return array
     */
    public function getFourStarReview($id)
    {
        $data_star = ReviewProductModel::where('product_id', $id)->where('ratings', 4)->paginate(2);
        foreach ($data_star as $key => $value) {
            $data_shop = ShopModel::find($value->shop_id);
            $data_star[$key]['shop_name'] = $data_shop->name;
            $data_star[$key]['shop_image'] = $data_shop->image;
        }
        $dataReturn = [
            'data_star' => $data_star,
        ];
        return view('frontend.products.review_four_star', $dataReturn);
    }

    /**
     *  lấy đánh giá 5 sao
     * @param Request $request
     * @return array
     */
    public function getFiveStarReview($id)
    {
        $data_star = ReviewProductModel::where('product_id', $id)->where('ratings', 5)->paginate(2);
        foreach ($data_star as $key => $value) {
            $data_shop = ShopModel::find($value->shop_id);
            $data_star[$key]['shop_name'] = $data_shop->name;
            $data_star[$key]['shop_image'] = $data_shop->image;
        }
        $dataReturn = [
            'data_star' => $data_star,
        ];
        return view('frontend.products.review_five_star', $dataReturn);
    }

    /**
     *  Thêm vào giỏ hàng
     * @param Request $request
     * @return array
     */
    public
    function addToCart(Request $request)
    {

        $dataCustomer = session()->get('data_customer');
        $data = new CartModel();
        $dataCart = CartModel::where('customer_id', $dataCustomer->id)
            ->where('shop_id', $request->shop_id)
            ->where('product_id', $request->id)
            ->where('value_attr', $request->id_attribute)
            ->first();
        $data_attribute = AttributeValueModel::find($request->id_attribute);
        $quantity = $request->quantity;
        if ($dataCart) {
            if ($dataCart->quantity > $data_attribute->quantity) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'quá giới hạn số lượng'
                ];
                return response()->json($dataReturn, 200);
            } else {
                $total_quantity = $dataCart->quantity + $quantity;
                $percent = BuyMorePercent::where('quantity_1', '<=', $total_quantity)->where('product_id', $request->get('id'))
                    ->where('quantity_2', '>=', $total_quantity)->first();
                if (isset($percent)){
                    $percent_ctv = $percent->percent;
                }else{
                    $percent_ctv = 0;
                }
                $price_product = $data_attribute->price_ctv - $data_attribute->price_ctv * $percent_ctv / 100;
                CartModel::where('id', $dataCart->id)->update([
                    'quantity' => $total_quantity,
                    'price_cart' => $price_product,
                    'percent' => $percent_ctv
                ]);
            }
        } else {
            if ($request->quantity > $data_attribute->quantity) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'quá giới hạn số lượng'
                ];
                return response()->json($dataReturn, 200);
            } else {
                $percent = BuyMorePercent::where('quantity_1', '<=', $request->get('quantity'))->where('product_id', $request->get('id'))
                    ->where('quantity_2', '>=', $request->get('quantity'))->first();
                if (isset($percent)){
                    $percent_ctv = $percent->percent;
                }else{
                    $percent_ctv = 0;
                }
                $price_product = $data_attribute->price_ctv - $data_attribute->price_ctv * $percent_ctv / 100;
                $data->customer_id = $dataCustomer->id;
                $data->quantity = $request->quantity;
                $data->product_id = $request->id;
                $data->shop_id = $request->shop_id;
                $data->value_attr = $request->id_attribute;
                $data->percent = $percent_ctv;
                $data->price_cart = $price_product;
                $data->save();
            }
        }

        $count_item_cart = CartModel::where('customer_id', $dataCustomer->id)->count();
        $dataReturn = [
            'status' => true,
            'items' => $count_item_cart,
            'msg' => 'Thêm vào giỏ hàng thành công',
        ];
        return response()->json($dataReturn, 200);
    }

    /**
     *  Ajax download file ảnh
     * @param Request $request
     * @return array
     */
    public function getImageProduct($id)
    {
        $data_image = ProductImageModel::where('product_id', $id)->get();
        $dataReturn = [
            "data_image" => $data_image
        ];
        return view('frontend.products.file_image', $dataReturn);
    }

    public function add_to_mini_cart()
    {
        $dataCustomer = session()->get('data_customer');
        $dataCart = DB::table('cart')
            ->select('product.name as name_prd', 'shop.name as name_shop', 'customer.name as name_cus',
                'cart.quantity as quantity_prd', 'attribute_value.price_ctv as price_prd', 'attribute_value.quantity as value_quantity', 'cart.id as cart_id',
                'product_attribute.category_1 as name_category', 'product_attribute.category_2 as name_value', 'attribute_value.size_of_value as name_size'
            )
            ->join('product', 'cart.product_id', '=', 'product.id')
            ->join('customer', 'cart.customer_id', '=', 'customer.id')
            ->join('shop', 'cart.shop_id', '=', 'shop.id')
            ->join('attribute_value', 'cart.value_attr', '=', 'attribute_value.id')
            ->join('product_attribute', 'product_attribute.id', '=', 'attribute_value.attribute_id')
            ->where('cart.customer_id', $dataCustomer->id)->orderBy('cart.created_at', 'DESC')
            ->get();
        $count_item_cart = CartModel::where('customer_id', $dataCustomer->id)->count();
        $dataReturn = [
            'status' => true,
            'miniCart' => $dataCart,
            'items' => $count_item_cart,
            'msg' => 'Thêm vào giỏ hàng thành công',
        ];
        return view('frontend.products.mini_cart', $dataReturn);
    }

    public function addProductDropShip(Request $request)
    {
        $data_customer = session()->get('data_customer');
        $shop = ShopModel::where('customer_id', $data_customer->id)->first();
        $product_shop = ProductModel::where('product_id', $request->get('product_id'))->orWhere('product_drop_ship_id', $request->get('product_id'))
            ->where('shop_id', $shop->id)->first();
        if (isset($product_shop)){
            return back();
        }
        $product = ProductModel::find($request->product_id);
        $product_drop_ship = new ProductModel([
           'name' => $product->name,
            'category_id' => $product->category_id,
            'price' => $product->price,
            'sku_type' => $product->sku_type,
            'warehouse' => $product->warehouse,
            'is_sell' => $product->is_sell,
            'is_status' => $product->is_status,
            'image' => $product->image,
            'desc' => $product->desc,
            'price_discount' => $product->price_discount,
            'dropship' => 1,
            'is_trending' => $product->is_trending,
            'sponsored' => $product->sponsored,
            'brand' => $product->brand,
            'original' => $product->original,
            'material' => $product->material,
            'shop_id' => $shop->id,
            'weight' => $product->weight,
            'length' => $product->length,
            'width' => $product->width,
            'height' => $product->height,
            'slug' => $product->slug,
            'supplier_id' => $product->supplier_id,
            'sort_desc' => $product->sort_desc,
            'is_review' => $product->is_review,
            'category_shop' => $product->category_shop,
            'place' => $product->place,
            'status_product' => $product->status_product,
            'active' => 0,
            'category_sub_child' => $product->category_sub_child,
            'video' => $product->video,
            'category_product' => $product->category_product,
            'price_ctv' => $product->price_ctv,
            'place_district' => $product->place_district,
        ]);
        $product_drop_ship->save();
        if (isset($product->product_id)){
            $product_drop_ship->product_id = $product->product_id;
            $product_drop_ship->product_drop_ship_id = $product->id;
            $product_drop_ship->save();
        }else{
            $product_drop_ship->product_id = $product->id;
            $product_drop_ship->save();
        }
        if (isset($product->shop_parent_id)){
            $product_drop_ship->shop_parent_id = $product->shop_parent_id;
            $product_drop_ship->shop_drop_ship_id = $product->shop_id;
            $product_drop_ship->save();
        }else{
            $product_drop_ship->shop_parent_id = $product->shop_id;
            $product_drop_ship->save();
        }

        $product_attribute = ProductAttributeModel::where('product_id', $product->id)->get();
        foreach ($product_attribute as $value){
            $attr_product = AttributeValueModel::where('attribute_id', $value->id)->get();
            $product_value = new ProductAttributeModel([
                'product_id' => $product_drop_ship->id,
                'category_1' => $value->category_1,
                'category_2' => $value->category_2,
                'name' => ''
            ]);
            $product_value->save();

            foreach ($attr_product as $key){
                $attr_drop_ship = new AttributeValueModel([
                    'attribute_id' => $product_value->id,
                    'value' => $key->value,
                    'price' => $key->price,
                    'quantity' => $key->quantity,
                    'size_of_value' => $key->size_of_value,
                    'is_sell' => $key->is_sell,
                    'price_ctv' => $key->price_ctv,
                    'product_id' => $product_drop_ship->id,
                    'product_attribute_id' => $key->id
                ]);
                $attr_drop_ship->save();
            }
        }

        $product_image = ProductImageModel::where('product_id', $product->id)->get();
        if (isset($product_image)){
            foreach ($product_image as $value){
                $product_image_drop_ship = new ProductImageModel([
                   'product_id' => $product_drop_ship->id,
                    'image' => $value->image,
                ]);
                $product_image_drop_ship->save();
            }
        }

        $category_product = new ProductCategoryShopModel([
            'category_id' => $request->category,
            'product_id' => $product_drop_ship->id,
            'shop_id' => $shop->id,
        ]);
        $category_product->save();

        $this->productDropShip($product->id, $shop->id,$product_drop_ship->id, $product->shop_id);
        $product_attribute = ProductAttributeModel::where('product_id', $product_drop_ship->id)->get();
        $product = ProductModel::find($product_drop_ship->id);
        $data['product'] = $product;
        $data['product_attribute'] = $product_attribute;
        $views = view('frontend.products.edit-product-dropship', $data)->render();
        return response()->json(['prod' => $views]);
    }

    protected function productDropShip( $id, $shop_id, $id_product_dropship, $shop_parent )
    {

        $product = ProductDropShipModel::where('product_id', $id)->get();
        if (isset($product) && count($product)){
            foreach ($product as $value)
            {
                $product_dropship = new ProductDropShipModel([
                   'product_id' => $id_product_dropship,
                   'shop_id' => $shop_id,
                   'product_parent' => $value->product_parent,
                   'shop_parent' => $value->shop_parent
                ]);
                $product_dropship->save();
            }
            $product_dropship = new ProductDropShipModel([
                'product_id' => $id_product_dropship,
                'shop_id' => $shop_id,
                'product_parent' => $id,
                'shop_parent' => $shop_parent
            ]);
            $product_dropship->save();
        }else{
            $product_dropship = new ProductDropShipModel([
                'product_id' => $id_product_dropship,
                'shop_id' => $shop_id,
                'product_parent' => $id,
                'shop_parent' => $shop_parent
            ]);
            $product_dropship->save();
        }
        return true;
    }

    public function checkPricePercent(Request $request)
    {
        try{
            $quantity = $request->get('quantity');
            $product_attribute = AttributeValueModel::find($request->get('attribute_id'));
            $price = $product_attribute->price_ctv;
            $price_old = $product_attribute->price;
            $max_quantity = BuyMorePercent::where('product_id', $request->get('product_id'))->max('quantity_2');
            $percent = BuyMorePercent::where('quantity_1', '<=', $quantity)->where('product_id', $request->get('product_id'))
                ->where('quantity_2', '>=', $quantity)->first();
            if ($quantity > $max_quantity){
                $percent_ctv = BuyMorePercent::where('product_id', $request->get('product_id'))->max('percent');
                $price_percent = $price - $price * $percent_ctv / 100;
                $price_profit = $price_old - $price_percent;
                $data['price'] = number_format($price_percent);
                $data['profit'] = number_format($price_profit);
                $data['status'] = true;
                return $data;
            }else{
                if (isset($percent)){
                    $percent_ctv = $percent->percent;
                    $price_percent = $price - $price * $percent_ctv / 100;
                    $price_profit = $price_old - $price_percent;
                    $data['price'] = number_format($price_percent);
                    $data['profit'] = number_format($price_profit);
                }else{
                    $price_profit = $product_attribute->price - $product_attribute->price_ctv;
                    $data['profit'] = number_format($price_profit);
                    $data['price'] = number_format($price);
                }
                $data['status'] = true;
                return $data;
            }
        }catch (Exception $exception){
            $data['error'] = $exception;
            return $data;
        }
    }

    public function editProductDropship (Request $request)
    {
        $attribute = $request->get('attribute');
        $product = ProductModel::find($request->get('product_id'));
        $product->active_drop_ship = 2;
        $product->active = 1;
        $product->save();
        $price_max = 0;
        $price_mim = 0;
        foreach ($attribute as $value){
            $attribute_value = AttributeValueModel::find($value['id_attr']);
            $attribute_value->price = $value['price'];
            $attribute_value->price_ctv = $value['price_ctv'];
            $attribute_value->save();
            if ($price_max == 0){
                $price_max = $value['price'];
            }
            if ($price_max < $value['price']){
                $price_max = $value['price'];
            }
            if ($price_mim == 0){
                $price_mim = $value['price'];
            }
            if ($price_mim > $value['price']){
                $price_mim = $value['price'];
            }
        }
        $product->price = $price_max;
        $product->price_discount = $price_mim;
        $product->save();
        return back();
    }
}
