<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MallController extends Controller
{
    public function index(){
        return view('frontend.mall.index');
    }
}
