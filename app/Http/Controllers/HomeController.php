<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Admin\TrendingProductController;
use App\Models\AddressCustomerCTVModel;
use App\Models\AttributeValueModel;
use App\Models\BrandModel;
use App\Models\CategoryProductChildModel;
use App\Models\CategoryProductModel;
use App\Models\CityModel;
use App\Models\CustomerAddressModel;
use App\Models\CustomerModel;
use App\Models\DistrictModel;
use App\Models\FavouriteProductModel;
use App\Models\HotKeyModel;
use App\Models\KeySearchModel;
use App\Models\NotificationCustomerModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductAttributeModel;
use App\Models\ProductModel;
use App\Models\ReviewProductModel;
use App\Models\SeenProductModel;
use App\Models\ShippingModel;
use App\Models\ShopModel;
use App\Models\SponsoredProductModel;
use App\Models\TrendingProductModel;
use App\Models\WardModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Http\Response;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $dataCustomer = session()->get('data_customer');
        if ($dataCustomer) {
            $data_order_old = OrderModel::where('customer_id', $dataCustomer->id)->where('is_selected', 0)->get();
            if ($data_order_old) {
                foreach ($data_order_old as $key => $value) {
                    $data_order_old[$key]->delete();
                    unset($data_order_old[$key]);
                }
            }
        }
        $dataKeySearch = KeySearchModel::orderBy('number', 'desc')->limit(10)->get();
        if ($dataKeySearch) {
            foreach ($dataKeySearch as $k => $v) {
                $dataCategory = CategoryProductModel::find($v['product_category']);
                $dataKeySearch[$k]['category_name'] = $dataCategory['name'] ?? '';
            }
        }
        $dataCategory = CategoryProductModel::orderBy('sort', 'asc')->get();
        $dataBrand = BrandModel::where('is_hot', 1)->orderBy('id', 'desc')->get();
        $dataHotKey = HotKeyModel::orderBy('id', 'desc')->get();
        $dataTrendingProduct = DB::table('trending_product')
            ->select('product.*')
            ->join('product', 'trending_product.product_id', '=', 'product.id')
            ->orderBy('trending_product.id', 'desc')
            ->get();
        $dataSponsoredProduct = DB::table('sponsored_product')
            ->select('product.*', 'sponsored_product.value as value')
            ->join('product', 'sponsored_product.product_id', '=', 'product.id')
            ->orderBy('sponsored_product.id', 'desc')
            ->get();
        $dataSugessionProduct = ProductModel::orderBy('id', 'desc')
            ->where('active', 1)->paginate(25);
        if (count($dataSugessionProduct)) {
            foreach ($dataSugessionProduct as $key => $value) {
                $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
                $avg_rating = round($total_rating,1);
                $dataSugessionProduct[$key]->avg_rating = $avg_rating;
            }
        }
        $dataReturn = [
            'dataKeySearch' => isset($dataKeySearch) ? $dataKeySearch : '',
            'dataCategory' => $dataCategory,
            'dataBrand' => $dataBrand,
            'dataHotKey' => isset($dataHotKey) ? $dataHotKey : '',
            'title' => 'CTV 24h',
            'dataTrendingProduct' => isset($dataTrendingProduct) ? $dataTrendingProduct : '',
            'dataSponsoredProduct' => isset($dataSponsoredProduct) ? $dataSponsoredProduct : '',
            'dataSugesstionProduct' => isset($dataSugessionProduct) ? $dataSugessionProduct : ''
        ];
        return view('frontend.home.index', $dataReturn);
    }

    public function getSecondhand()
    {
        return view('frontend.home.secondhand');
    }

    //kiểm trA ĐƠN HÀNG
    public function check_order(Request $request)
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $data_login = session()->get('data_customer');
        $order_code = $request->order_code;
        $dataOrder = OrderModel::where('customer_id', $data_login->id)->where('order_code', $order_code)->where('is_selected', 2)->first();
        if ($dataOrder) {
            $data_check_notification = NotificationCustomerModel::where('customer_id',$data_login['id'])->where('order_id',$dataOrder['id'])->first();
            if($data_check_notification){
                $data_check_notification->created_at = $today;
                $data_check_notification->save();
            }else{
                NotificationCustomerModel::create([
                   'customer_id' => $data_login['id'],
                   'order_id' => $dataOrder['id'],
                   'created_at' => $today
                ]);
            }
            $data_shop = ShopModel::find($dataOrder['shop_id']);
            $dataOrder['shop_name'] = $data_shop['name'];

            if (isset($dataOrder->collaborator_id) && $dataOrder->collaborator_id == 1) {
                $data_address_customer = AddressCustomerCTVModel::find($dataOrder->address_ctv);
                $dataOrder->name_customer_ctv = $data_address_customer->name ?? '';
                $dataOrder->phone_customer_ctv = $data_address_customer->phone ?? '';
                $data_city = CityModel::find($data_address_customer->city);
                $data_district = DistrictModel::find($data_address_customer->district);
                $data_ward = WardModel::find($data_address_customer->ward);
                $dataOrder->address_customer_ctv = $data_address_customer->address . ', ' . $data_ward['name'] . ', ' . $data_district['name'] . ', ' . $data_city['name'] ?? '';
            } elseif (!isset($dataOrder->collaborator_id)) {
                $data_address_order = CustomerAddressModel::where('customer_id', $data_login->id)->where('default', 1)->first();
                if ($data_address_order) {
                    $city = CityModel::find($data_address_order->city_id);
                    $district = DistrictModel::find($data_address_order->district_id);
                    $ward = WardModel::where('ward_id', $data_address_order->ward_id)->first();
                    if ($ward) {
                        $dataOrder->customer_address = $data_address_order->address . ', ' . $ward->name . ', ' . $district->name . ', ' . $city->name . '.';
                    }
                    $dataOrder->customer_phone = $data_address_order->phone;
                    $dataOrder->customer_name = $data_address_order->name;
                }
            }
            if ($dataOrder['status'] == 0) {
                $dataOrder['is_status'] = 'Chờ xác nhận';
            } elseif ($dataOrder['status'] == 1) {
                $dataOrder['is_status'] = 'Chờ lấy hàng';
            } elseif ($dataOrder['status'] == 2) {
                $dataOrder['is_status'] = 'Đang giao hàng';
            } elseif ($dataOrder['status'] == 3) {
                $dataOrder['is_status'] = 'Đã giao hàng';
            } elseif ($dataOrder['status'] == 4) {
                $dataOrder['is_status'] = 'Đã huỷ';
            } else {
                $dataOrder['is_status'] = 'Hoàn tiền';
            }

            if ($dataOrder['payment_id'] == 0) {
                $dataOrder['payment'] = 'Ví CTV24H';
            } elseif ($dataOrder['payment_id'] == 1) {
                $dataOrder['payment'] = 'Trả góp';
            } elseif ($dataOrder['payment_id'] == 2) {
                $dataOrder['payment'] = 'Thanh toán khi nhận hàng';
            } elseif ($dataOrder['payment_id'] == 3) {
                $dataOrder['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
            } elseif ($dataOrder['payment_id'] == 4) {
                $dataOrder['payment'] = 'CTV24H Pay';
            } else {
                $dataOrder['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
            }

            $data_shipping = ShippingModel::find($dataOrder['shipping_id']);
            $dataOrder['shipping'] = $data_shipping['name'];
            $data_order_detail = OrderDetailModel::where('order_id', $dataOrder['id'])->get();
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v['product_id']);
                $data_order_detail[$k]['product_name'] = $data_product['name'];
                $data_order_detail[$k]['product_image'] = $data_product['image'];
                $data_order_detail[$k]['price_product'] = $data_product['price_discount'];
                $dataValue_Attribute = AttributeValueModel::find($v->type_product);
                $data_order_detail[$k]->value_attr = $dataValue_Attribute->value ?: '';
                $data_order_detail[$k]->size_of_value = $dataValue_Attribute->size_of_value ?: '';
            }
            $dataReturn = [
                'status' => true,
                'title' => 'Kiểm tra đơn hàng',
                'data' => $dataOrder,
                'data_order' => $data_order_detail
            ];
        } else {
            $dataReturn = [
                'status' => false,
            ];
        }
        return view('frontend.DetailOrder.index', $dataReturn);
    }

//    protected function getDataVlueAttr($value)
//    {
//        $item = explode(',', $value);
//        $data = [];
//        if (isset($item)) {
//            foreach ($item as $v) {
//                $__dataAttrValue = AttributeValueModel::find($v);
//                if (isset($__dataAttrValue)) {
//                    $__dataAttrProduct = ProductAttributeModel::find($__dataAttrValue->attribute_id);
//                    $__data['type'] = isset($__dataAttrProduct) ? $__dataAttrProduct['name'] : '';
//                    $__data['value'] = isset($__dataAttrValue) ? $__dataAttrValue['value'] : '';
//                    array_push($data, $__data);
//                }
//            }
//        }
//        return $data;
//    }
}
