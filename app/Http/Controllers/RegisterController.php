<?php

namespace App\Http\Controllers;

use App\Models\CollaboratorModel;
use App\Models\CustomerModel;
use App\Models\ShippingModel;
use App\Models\ShippingSupplierModel;
use App\Models\ShopModel;
use App\Models\SupplierModel;
use App\Models\TheBankModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function register()
    {
        $dataReturn = [
            'title' => 'Đăng ký | Khách hàng'
        ];
        return view('frontend.register.register', $dataReturn);
    }

    public function registerNcc()
    {
        $dataReturn = [
            'title' => 'Đăng ký | Nhà cung cấp'
        ];
        return view('frontend.register.register_ncc', $dataReturn);
    }

    public function registerCtv()
    {
        $dataReturn = [
            'title' => 'Đăng ký | Cộng tác viên'
        ];
        return view('frontend.register.register_ctv', $dataReturn);
    }

    public function save(Request $request)
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y/m/d H:i:s');
        $rules = [
            'name' => 'required',
            'phone' => 'required|unique:customer,phone',
            'password' => 'required|min:8|max:50',
//            'birthday' => 'required|before:today',
//            'gender' => 'required',
            'email' => 'email|required|unique:customer,email'
        ];
        $customMessages = [
            'name.required' => 'Vui lòng nhập họ tên !',
            'phone.required' => 'Vui lòng nhập số điện thoại !',
            'email.required' => 'Vui lòng nhập email !',
            'phone.unique' => 'Số điện thoại đã tồn tại !',
//            'before' => 'Ngày sinh không được quá ngày hiện tại !',
            'max' => 'Mật khẩu không quá 50 ký tự !',
            'min' => 'Mật khẩu tối thiểu 8 ký tự !',
            'email.unique' => 'Email đã tồn tại !',
            'email.email' => 'Email không hợp lệ !'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $birthday = date('Y/m/d H:i:s', strtotime($request->birthday));
            $data = new CustomerModel([
                'name' => $request->name,
                'password' => md5($request->password),
                'phone' => $request->phone,
                'birthday' => $today,
                'gender' => 3,
                'email' => $request->email,
            ]);
            $data->save();
            $data_supplier = new SupplierModel([
                'name' => $data->name,
                'password' => $data->password,
                'phone' => $data->phone,
                'customer_id' => $data->id,
//                'birthday' => date('Y-m-d', strtotime($data->birthday)),
//                'gender' => $data->gender,
                'birthday' => $today,
                'gender' => 3,
                'email' => $data->email,
                'image' => '',
            ]);
            $data_supplier->save();
            $data_shop = new ShopModel([
                'name' => $data['name'],
                'image' => '',
                'video' => '',
                'desc' => '',
                'percent_feedback' => rand(90,100),
                'date' => $today,
                'time_feedback' => '1 Giờ',
                'report' => '',
                'supplier_id' => $data_supplier->id,
                'customer_id' => $data->id
            ]);
            $data_shop->save();
            $data_shipping = ShippingModel::first();
            $data_shipping_supplier = new ShippingSupplierModel([
               'supplier_id' => $data_supplier['id'],
               'shipping_id' => $data_shipping['id'],
            ]);
            $data_shipping_supplier->save();
            $data_collaborator = new CollaboratorModel([
                'name' => $data->name,
                'password' => $data->password,
                'phone' => $data->phone,
                'customer_id' => $data->id,
//                'birthday' => date('Y-m-d', strtotime($data->birthday)),
//                'sex' => $data->gender,
                'birthday' => $today,
                'sex' => 3,
                'email' => $data->email,
                'image' => '',
            ]);
            $data_collaborator->save();
            $data_login = CustomerModel::where('phone', $data['phone'])->where('password', $data['password'])->first();
            if ($data_login) {
                $dataUser['customer'] = true;
                $request->session()->put('data_customer', $data_login);
            }
            $dataReturn = [
                'status' => true,
                'msg' => 'Đăng ký thành công !',
                'url' => URL::to('/')
            ];
        }
        return $dataReturn;
    }

//    public function saveCTV(Request $request)
//    {
//        $rules = [
//            'name' => 'required',
//            'phone' => 'required|unique:collaborator,phone',
//            'password' => 'required|min:8|max:50',
//        ];
//        $customMessages = [
//            'required' => 'Vui lòng điền đầy đủ thông tin !',
//            'unique' => 'Số điện thoại đã tồn tại !',
//            'min' => 'Mật khẩu phải tối thiểu 8 ký tự !',
//            'max' => 'Mật khẩu tối đa 50 ký tự !',
//        ];
//        $validator = Validator::make($request->all(), $rules, $customMessages);
//        if ($validator->fails()) {
//            $messages = $validator->messages();
//            $errors = $messages->all();
//            $dataReturn['status'] = false;
//            $dataReturn['msg'] = $errors[0];
//        } else {
//            $data = new CollaboratorModel([
//                'name' => $request->name,
//                'password' => md5($request->password),
//                'phone' => $request->phone,
//                'email' => '',
//                'username' => '',
//                'birthday' => null,
//                'sex' => 0,
//                'image' => ''
//            ]);
//            $data->save();
//            $dataReturn = [
//                'status' => true,
//                'msg' => 'Đăng ký cộng tác viên thành công ! Đăng nhập ngay !',
//                'url' => URL::to('admin/collaborator_admin/login')
//            ];
//        }
//        return $dataReturn;
//    }

//    public function saveNCC(Request $request)
//    {
//        $rules = [
//            'name' => 'required',
//            'phone' => 'required|unique:supplier,phone',
//            'password' => 'required',
//            'id_card' => 'required',
//            'business_license' => 'required',
//            'product_supply' => 'required',
//            'import_papers' => 'required'
//        ];
//        $customMessages = [
//            'required' => 'Vui lòng điền đầy đủ thông tin !',
//            'unique' => 'Số điện thoại đã tồn tại !'
//        ];
//        $validator = Validator::make($request->all(), $rules, $customMessages);
//        if ($validator->fails()) {
//            $messages = $validator->messages();
//            $errors = $messages->all();
//            $dataReturn['status'] = false;
//            $dataReturn['msg'] = $errors[0];
//        } else {
//            $data = new SupplierModel([
//                'name' => $request->name,
//                'password' => md5($request->password),
//                'phone' => $request->phone,
//                'id_card' => $request->id_card,
//                'business_license' => $request->business_license,
//                'product_supply' => $request->product_supply,
//                'import_papers' => $request->import_papers,
//                'email' => '',
//                'username' => '',
//                'image' => '',
//            ]);
//            $data->save();
//            $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d');
//            $data_shop = new ShopModel([
//                'name' => $data['name'],
//                'image' => '',
//                'video' => '',
//                'desc' => '',
//                'date' => $today,
//                'time_feedback' => '',
//                'report' => '',
//                'supplier_id' => $data['id']
//            ]);
//            $data_shop->save();
//            $data_login = SupplierModel::where('phone', $data['phone'])->where('password', $data['password'])->first();
//            if ($data_login) {
//                $dataUser['supplier'] = true;
//                $request->session()->put('data_supplier', $data_login);
//            }
//            $dataReturn = [
//                'status' => true,
//                'msg' => 'Đăng ký thành công ! Bạn đã trở thành nhà cung cấp !',
//                'url' => URL::to('admin/supplier_admin/login')
//            ];
//        }
//        return $dataReturn;
//    }

//    public function saveNCC1(Request $request)
//    {
//        $request->session()->forget('data_supplier');
//        $data_customer = CustomerModel::find($request->customer_id);
//        $rules = [
//            'business_license' => 'required',
//            'address' => 'required',
//            'bank_account' => 'required',
//            'bank_number' => 'required',
//        ];
//        $customMessages = [
//            'required' => 'Vui lòng điền đầy đủ thông tin !',
//        ];
//        $validator = Validator::make($request->all(), $rules, $customMessages);
//        if ($validator->fails()) {
//            $messages = $validator->messages();
//            $errors = $messages->all();
//            $dataReturn['status'] = false;
//            $dataReturn['msg'] = $errors[0];
//        } else {
//            $data_bank = TheBankModel::find($request->bank_name);
//            $data = new SupplierModel([
//                'name' => $data_customer->name,
//                'password' => $data_customer->password,
//                'phone' => $data_customer->phone,
//                'business_license' => $request->business_license,
//                'customer_id' => $data_customer->id,
//                'birthday' => date('Y-m-d', strtotime($data_customer->birthday)),
//                'gender' => $data_customer->gender,
//                'bank_name' => isset($data_bank) ? $data_bank->name : '',
//                'bank_number' => $request->bank_number,
//                'bank_account' => $request->bank_account,
//                'address' => $request->address,
//                'email' => $data_customer->email,
//                'image' => '',
//            ]);
//            $data->save();
//            $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d');
//            $data_shop = new ShopModel([
//                'name' => $data['name'],
//                'image' => '',
//                'video' => '',
//                'desc' => '',
//                'date' => $today,
//                'time_feedback' => '',
//                'report' => '',
//                'supplier_id' => $data['id'],
//                'customer_id' => $data_customer->id
//            ]);
//            $data_shop->save();
//            $data_login = SupplierModel::where('phone', $data['phone'])->where('password', $data['password'])->first();
//            if ($data_login) {
//                $dataSuppliers['supplier'] = true;
//                $request->session()->put('data_supplier', $data_login);
//            }
//            $dataReturn = [
//                'status' => true,
//                'msg' => 'Đăng ký thành công ! Bạn đã trở thành nhà cung cấp !',
//                'url' => URL::to('admin/supplier_admin/')
//            ];
//        }
//        return $dataReturn;
//    }
//
//    public function saveCTV1(Request $request)
//    {
//        $request->session()->forget('data_collaborator');
//        $data_customer = CustomerModel::find($request->customer_id);
//        $rules = [
//            'address' => 'required',
//            'bank_account' => 'required',
//            'bank_number' => 'required',
//        ];
//        $customMessages = [
//            'required' => 'Vui lòng điền đầy đủ thông tin !',
//        ];
//        $validator = Validator::make($request->all(), $rules, $customMessages);
//        if ($validator->fails()) {
//            $messages = $validator->messages();
//            $errors = $messages->all();
//            $dataReturn['status'] = false;
//            $dataReturn['msg'] = $errors[0];
//        } else {
//            $data_bank = TheBankModel::find($request->bank_name);
//            $data = new CollaboratorModel([
//                'name' => $data_customer->name,
//                'password' => $data_customer->password,
//                'phone' => $data_customer->phone,
//                'customer_id' => $data_customer->id,
//                'birthday' => date('Y-m-d', strtotime($data_customer->birthday)),
//                'sex' => $data_customer->gender,
//                'bank_name' => isset($data_bank) ? $data_bank->name : '',
//                'bank_number' => $request->bank_number,
//                'bank_account' => $request->bank_account,
//                'address' => $request->address,
//                'email' => $data_customer->email,
//                'image' => '',
//            ]);
//            $data->save();
//            $data_login = CollaboratorModel::where('phone', $data['phone'])->where('password', $data['password'])->first();
//            if ($data_login) {
//                $dataCollaborator['collaborator'] = true;
//                $request->session()->put('data_collaborator', $data_login);
//            }
//            $dataReturn = [
//                'status' => true,
//                'msg' => 'Đăng ký thành công ! Bạn đã trở thành cộng tác viên !',
//                'url' => URL::to('admin/collaborator_admin/')
//            ];
//        }
//        return $dataReturn;
//    }
}
