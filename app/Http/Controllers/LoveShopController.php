<?php

namespace App\Http\Controllers;

use App\Models\BankAccountModel;
use App\Models\FollowShopModel;
use App\Models\LovelyShopModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class LoveShopController extends Controller
{
    public function __construct(Request $request)
    {
        if(!$request->session()->has('data_customer')){
            Redirect::to('login')->send();
        }
    }

    /**
     *  Yêu thích shop
     * @param Request $request
     * @return array
     */
    public function likeShop(Request $request){
        $dataCustomer = session()->get('data_customer');
        $data = LovelyShopModel::where('customer_id', $dataCustomer->id)
            ->where('shop_id', $request->shop_id)
            ->first();
        if(isset($dataCustomer)){
            if(!$data){
                $data = new LovelyShopModel();
                $data->customer_id = $dataCustomer->id;
                $data->shop_id = $request->shop_id;
                $data->save();
            }
            $data_id = $request->shop_id;
            $data_url = url('seeshop/dislike/'.$request->shop_id);
            $html = '
                <button style="width: 120px!important;"
                                class="button btn-shop-followed button-dislike"
                                data-id="'.$data_id.'"
                                data-url="'.$data_url.'"
                            >
                                <object data="Icon/products/gold_like.svg" width="12px"></object>
                                Bỏ Thích
                            </button>
            ';
            $count_like_shop = LovelyShopModel::where('shop_id', $request->shop_id)->count();
            $dataReturn = [
                'status' => true,
                'msg' => 'Đã yêu thích',
                'count_like' => $count_like_shop,
                'html' => $html
            ];
        } else{
            $dataReturn = [
                'status' => false,
                'msg' => 'Vui lòng thử lại'
            ];
        }
        return $dataReturn;
    }
    /**
     *  Bỏ thích shop
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function dislikeShop(Request $request, $id)
    {
        try {
            $dataCustomer = session()->get('data_customer');
            LovelyShopModel::where('shop_id', $id)->where('customer_id', $dataCustomer->id)->delete();
            $data_id = $id;
            $data_url = url('seeshop/like');
            $html = '
                <button style="width: 120px!important;"
                                class="button btn-shop button-like"
                                data-id="'.$data_id.'"
                                data-url="'.$data_url.'"
                            >
                                <object data="Icon/products/gold_like.svg" width="12px"></object>
                                Yêu Thích
                            </button>
            ';
            $count_like_shop = LovelyShopModel::where('shop_id', $request->shop_id)->count();
            $dataReturn = [
                'status' => true,
                'msg' => 'Bỏ yêu thích thành công',
                'count_like' => $count_like_shop,
                'html' => $html
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
