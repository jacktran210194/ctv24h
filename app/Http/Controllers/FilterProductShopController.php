<?php

namespace App\Http\Controllers;

use App\Models\ProductModel;
use App\Models\ReviewProductModel;
use Illuminate\Http\Request;

class FilterProductShopController extends Controller
{
    public function index(Request $request)
    {
        $data_filter = ProductModel::query();
        $data_filter = $data_filter->where('product.shop_id', $request->get('id_shop'))->where('product.active', 1);
        $shipping = $request->get('shipping_id');
        $ratings = $request->get('ratings');
        $category = $request->get('category_sub_child');
        $place = $request->get('place');
        $price_1 = $request->get('price_filter_1');
        $price_2 = $request->get('price_filter_2');
        if ($request->get('type') == 1 ){
            if (isset($shipping)){
                $data_filter = $data_filter->join('product_shipping', 'product.id', '=', 'product_shipping.product_id')
                    ->select('product_shipping.product_id as id_product','product.*')
                    ->where('product_shipping.shipping_supplier_id', $shipping);
            }
            if (isset($ratings)){
                $data_filter = $data_filter->join('review_product', 'review_product.product_id', '=', 'product.id')
                    ->select('review_product.product_id as id_product','product.*')
                    ->where('ratings', '>=', $ratings);
            }
            if (isset($category)){
                $data_filter = $data_filter->join('product_category_shop', 'product_category_shop.product_id', '=', 'product.id')
                    ->select('product_category_shop.product_id as id_product','product.*')
                    ->where('product_category_shop.category_id', '=', $category);
            }
            if (isset($place)){
                $data_filter = $data_filter->where('place', $place);
            }
            if (isset($price_1) && isset($price_2)){
                $data_filter = $data_filter->where('price', '>=', $price_1)->where('price', '<=', $price_2);
            }
        }
        $data_filter = $data_filter->paginate(25);
        if (count($data_filter)) {
            foreach ($data_filter as $key => $value) {
                $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
                $avg_rating = round($total_rating, 1);
                $data_filter[$key]->avg_rating = $avg_rating;
            }
        }
        $view = view('frontend.seeshop.filter_product_shop', ['dataSugesstionProduct' => $data_filter])->render();
        return response()->json(['prod' => $view]);
    }
}
