<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PromotionController extends Controller
{
    /**
     * Khuyen mai
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $dataReturn = [
            'title' => 'Khuyến mại'
        ];
        return view('frontend.promotion.index', $dataReturn);
    }
}
