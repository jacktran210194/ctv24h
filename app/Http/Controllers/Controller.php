<?php

namespace App\Http\Controllers;

use App\Models\CategoryProductChildModel;
use App\Models\CategoryProductModel;
use App\Models\CustomerModel;
use App\Models\HistoryDealCTV24;
use App\Models\HistoryOrderPaymentCollaboratorModel;
use App\Models\HistoryOrderPaymentModel;
use App\Models\OrderModel;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function editStatusOrder ($shop_id)
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh');
        $data_order_confirm = OrderModel::where('shop_id', $shop_id)->where('is_selected', 2)->where('status', 1)->where('shipping_confirm', 0)->get();
        foreach ($data_order_confirm as $key => $value){
            $date = strtotime($value->created_at);
            $date = strtotime("+3 day", $date);
            if (strtotime($today) > $date ){
                $data_order_confirm[$key]->status = 4;
                $data_order_confirm[$key]->cancel_user = 0;
                $data_order_confirm[$key]->cancel_confirm = 1;
                $data_order_confirm[$key]->save();
                if ($value->payment_id == 0){
                    if (empty($value->collaborator_id)){
                        $price_order = $value->total_price;
                    }else{
                        $price_order = $value->total_price - $value->bonus;
                        $data_history_collaborator = HistoryOrderPaymentCollaboratorModel::where('order_id', $value->id)->first();
                        $data_history_collaborator->status = 2;
                        $data_history_collaborator->save();
                    }
                    $customer = CustomerModel::find($value->customer_id);
                    $customer->wallet = $customer->wallet + $price_order;
                    $customer->save();
                    $data_history_supplier = HistoryOrderPaymentModel::where('order_id', $value->id)->first();
                    $data_history_supplier->status = 2;
                    $data_history_supplier->save();
                    $data_deal_ctv24h = HistoryDealCTV24::where('order_id', $value->id)->first();
                    $data_deal_ctv24h->status = 2;
                    $data_deal_ctv24h->save();
                }
            }
        }
        return true;
    }

    public function overdueForDelivery ($shop_id)
    {
        $today_end = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 23:59:59');
        $data_order_confirm = OrderModel::where('shop_id', $shop_id)->where('is_selected', 2)->where('status', 1)->where('shipping_confirm', 1)->get();
        foreach ($data_order_confirm as $k => $v) {
            $time_qua_han_lay_hang = Carbon::parse($v->time_pick)->addDay(1)->format('Y-m-d H:i:s');
            if (strtotime($time_qua_han_lay_hang) < strtotime($today_end)) {
                $qua_han_lay_hang = true;
            } else {
                $qua_han_lay_hang = false;
            }
            if ($qua_han_lay_hang) {
                $data_order_confirm[$k]['status'] = 4;
                $data_order_confirm[$k]['cancel_user'] = 0;
                $data_order_confirm[$k]['cancel_confirm'] = 1;
                $data_order_confirm[$k]->save();
                if ($v->payment_id == 0){
                    if (empty($v->collaborator_id)){
                        $price_order = $v->total_price;
                    }else{
                        $price_order = $v->total_price - $v->bonus;
                        $data_history_collaborator = HistoryOrderPaymentCollaboratorModel::where('order_id', $v->id)->first();
                        $data_history_collaborator->status = 2;
                        $data_history_collaborator->save();
                    }
                    $customer = CustomerModel::find($v->customer_id);
                    $customer->wallet = $customer->wallet + $price_order;
                    $customer->save();
                    $data_history_supplier = HistoryOrderPaymentModel::where('order_id', $v->id)->first();
                    $data_history_supplier->status = 2;
                    $data_history_supplier->save();
                    $data_deal_ctv24h = HistoryDealCTV24::where('order_id', $v->id)->first();
                    $data_deal_ctv24h->status = 2;
                    $data_deal_ctv24h->save();
                }
            }
        }
        return true;
    }

}
