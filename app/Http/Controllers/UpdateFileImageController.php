<?php
namespace App\Http\Controllers;
use App\Models\ImageFileModel;
use App\Models\ProductImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use TheSeer\Tokenizer\Exception;

class UpdateFileImageController extends Controller
{
    public function uploadFile(Request $request)
    {
        if (!$request->hasFile('file')) {
            return response(['message' => __('Upload file not found')],400);
        }
        $files = $request->file('file');
        $fileName = Str::random(20) . '.' . $files->getClientOriginalExtension();
        $path = public_path() . '/uploads/images/file/';
        $files->move($path, $fileName);
        $src = '/uploads/images/file/'.$fileName;
        $image_file = ImageFileModel::create(['src' => $src]);
        $data['status'] = true;
        $data['id'] = $image_file->id;
        $data['src'] = $image_file->src;
        return $data;
    }

    public function uploadFileVariant(Request $request)
    {
        try{
            if (!$request->hasFile('file')) {
                return response(['message' => __('Upload file not found')],400);
            }
            $files = $request->file('file');
            $fileName = Str::random(20) . '.' . $files->getClientOriginalExtension();
            switch ($request->get('type')){
                case 1:
                    $path = public_path() . '/uploads/images/product/';
                    $files->move($path, $fileName);
                    $src = '/uploads/images/product/'.$fileName;
                    $image_file = ImageFileModel::create(['src' => $src]);
                    $data['status'] = true;
                    $data['id'] = $image_file->id;
                    $data['src'] = $image_file->src;
                    $data['type'] = 1;
                    return $data;
                    break;
                case 2:
                    $path = public_path() . '/uploads/images/upload_variant_product/'. $request->get('id_product') .'/';
                    $files->move($path, $fileName);
                    $src = '/uploads/images/upload_variant_product/'.$request->get('id_product') .'/'.$fileName;
                    $image_file = ImageFileModel::create(['src' => $src]);
                    $data['status'] = true;
                    $data['id'] = $image_file->id;
                    $data['src'] = $image_file->src;
                    $data['type'] = 2;
                    return $data;
                    break;
            }
        }catch (Exception $exception){
            return back()->withError($exception->getMessage())->withInput();
        }
    }
}
