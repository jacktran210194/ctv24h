<?php

namespace App\Http\Controllers;

use App\Models\CategoryProductChildModel;
use App\Models\CategoryProductModel;
use App\Models\CustomerModel;
use App\Models\ProductModel;
use App\Models\ReviewProductModel;
use App\Models\SubCategoryProductChildModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ElectronicDeviceController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request, $id)
    {
        $data_category = CategoryProductModel::find($id);
        $data_category_child = CategoryProductChildModel::where('category_id', $data_category['id'])->orderBy('sort', 'asc')->get();
        $data_category_product_sub_child = SubCategoryProductChildModel::where('category_product_id', $id)->get();
        $data_product = DB::table('product')
            ->select('product.*', 'product.name as name_product')
            ->join('category_product_child', 'category_product_child.id', '=', 'product.category_id')
            ->join('category_product', 'category_product.id', '=', 'category_product_child.category_id')
            ->where('category_product.id','=',$id);
        switch ($request->type){
            case 'new':
                $type = 'new';
                $data_product = $data_product->orderBy('product.id', 'DESC');
                break;
            case 'best_seller':
                $type = 'best_seller';
                $data_product = $data_product->orderBy('product.is_sell', 'DESC');
                break;
            case 'price':
                $type = 'price';
                $data_product = $data_product->orderBy('product.price', 'asc');
                break;
            default:
                $type = 'related';
                $data_product = $data_product->orderBy('product.id', 'desc');
                break;
        }
        $data_product = $data_product->get();
        foreach ($data_product as $key => $value) {
            $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
            $avg_rating = round($total_rating,1);
            $data_product[$key]->avg_rating = $avg_rating;
        }
        $dataReturn = [
            'title' => 'Danh mục sản phẩm ' . $data_category['name'],
            'category' => $data_category,
            'category_child' => $data_category_child,
            'product' => $data_product,
            'type' => $type,
            'data_product' => $data_product,
            'category_product_sub_child' => $data_category_product_sub_child,
        ];
        return view('frontend.ElectronicDevice.index',$dataReturn);
    }
}
