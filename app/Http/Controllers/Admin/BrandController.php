<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BrandModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class BrandController extends Controller
{
    public function index()
    {
        $data = BrandModel::orderBy('id', 'desc')->get();
        if (count($data)) {
            foreach ($data as $key => $value) {
                if ($value['is_hot'] == 0) {
                    $data[$key]['is_hot'] = 'Không';
                } else {
                    $data[$key]['is_hot'] = 'Có';
                }
            }
        }
        $data_return = [
            'title' => 'Quản lý thương hiệu',
            'data' => $data
        ];
        return view('admin.brand.index', $data_return);
    }

    public function add()
    {
        $data_return = [
            'title' => 'Thêm thương hiệu',
        ];
        return view('admin.brand.form', $data_return);
    }

    public function save(Request $request)
    {
        if ($request->id) {
            $dataReturn = $this->update($request);
        } else {
            $dataReturn = $this->create($request);
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function create($request)
    {
        $rules = [
            'name' => 'required|unique:brand,name',
            'is_hot' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'unique' => 'Tên thương hiệu đã tồn tại !'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $data_category = BrandModel::create([
                'name' => $request->name,
                'is_hot' => $request->is_hot,
                'image' => ''
            ]);
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/brand/';
            $file->move($path, $fileName);
            $data_category->image = '/uploads/images/brand/' . $fileName;
            $data_category->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/brand'),
            ];
        }
        return $dataReturn;
    }

    public function edit(Request $request)
    {
        $data_category = BrandModel::find($request->id);
        $data_return = [
            'title' => 'Cập nhật thương hiệu',
            'data' => $data_category
        ];
        return view('admin.brand.form', $data_return);
    }

    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    public function update($request)
    {
        $rules = [
            'name' => 'required|unique:brand,name,' . $request->id,
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'unique' => 'Tên thương hiệu đã tồn tại !'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data_category = BrandModel::find($request->id);
        if ($data_category) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_category->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/brand/';
                $file->move($path, $fileName);
                $data_category->image = '/uploads/images/brand/' . $fileName;
                $data_category->save();
            }
            $data_category->name = $request->name;
            $data_category->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/brand'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function delete($id)
    {
        try {
            BrandModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
