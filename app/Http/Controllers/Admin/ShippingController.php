<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ShippingModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ShippingController extends Controller
{
    /**
     * danh sách đơn vị vận chuyển
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     *
     */
    public function index()
    {
        $data_shipping = ShippingModel::orderBy('id', 'desc')->get();
        $data_return = [
            'category' => true,
            'title' => 'Quản lý đơn vị vận chuyển',
            'data' => $data_shipping
        ];
        return view('admin.shipping.index', $data_return);
    }

    /**
     * add
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     *
     */
    public function add()
    {
        $data_return = [
            'title' => 'Thêm đơn vị vận chuyển',
            'status' => true,
        ];
        return view('admin.shipping.form', $data_return);
    }

    /**
     * save
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function save(Request $request)
    {
        if ($request->id) {
            $dataReturn = $this->update($request);
        } else {
            $dataReturn = $this->create($request);
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     * thêm mới
     *
     * @param $request
     * @return array
     *
     */
    public function create($request)
    {
        $rules = [
            'name' => 'required|unique:shipping,name',
            'phone' => 'required',
            'address' => 'required',
            'max_weight' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'name.unique' => 'Tên đơn vị vận chuyển đã tồn tại'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $data_shipping = ShippingModel::create([
                'name' => $request->name,
                'phone' => $request->phone,
                'address' => $request->address,
                'max_weight' => $request->max_weight,
                'image' => '',
            ]);
            $data_shipping->save();
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/shipping/';
            $file->move($path, $fileName);
            $data_shipping->image = '/uploads/images/shipping/' . $fileName;
            $data_shipping->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/shipping'),
            ];
        }
        return $dataReturn;
    }

    /**
     * edit
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     *
     */
    public function edit(Request $request)
    {
        $data_shipping = ShippingModel::find($request->id);

        $data_return = [
            'title' => 'Cập nhật đơn vị vận chuyển',
            'status' => true,
            'data' => $data_shipping
        ];
        return view('admin.shipping.form', $data_return);
    }

    /**
     * Update
     *
     * @param $request
     * @return array
     *
     */
    public function update($request)
    {
        $rules = [
            'name' => 'required|unique:shipping,name,'.$request->id,
            'phone' => 'required',
            'address' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'name.unique' => 'Tên đơn vị vận chuyển đã tồn tại'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data_shipping = ShippingModel::find($request->id);

        if ($data_shipping) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_shipping->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/shipping/';
                $file->move($path, $fileName);
                $data_shipping->image = '/uploads/images/shipping/' . $fileName;
                $data_shipping->save();
            }

            $data_shipping->name = $request->name;
            $data_shipping->address = $request->address;
            $data_shipping->phone = $request->phone;
            $data_shipping->max_weight = $request->max_weight;
            $data_shipping->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/shipping'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }
    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /**
     * Delete
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function delete($id)
    {
        try {
            ShippingModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
