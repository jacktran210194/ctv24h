<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryProductModel;
use App\Models\HotKeyModel;
use App\Models\VoucherModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class HotKeyController extends Controller
{
    /**
     *  Danh sách từ khóa hót
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $data = HotKeyModel::orderBy('id', 'desc')->get();
        $data_return = [
            'title' => 'Từ khóa hot',
            'data' => $data
        ];
        return view('admin.hot_key.index', $data_return);
    }

    /**
     *Thêm từ khóa
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add(){
        $data_return = [
            'title' => 'Thêm từ khóa hot',
        ];
        return view('admin.hot_key.form',$data_return);
    }

    /**
     *  Lưu từ khóa
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        if ($request->id) {
            $dataReturn = $this->update($request);
        } else {
            $dataReturn = $this->create($request);
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     *  Thêm từ khóa
     * @param $request
     * @return array
     */
    public function create($request){
        $rules = [
            'key' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $data = HotKeyModel::create([
                'key' => $request->key,
                'image' => ''
            ]);
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/hot_key/';
            $file->move($path, $fileName);
            $data->image = '/uploads/images/hot_key/' . $fileName;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/hot_key'),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Cập nhật từ khóa
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Request $request){
        $data = HotKeyModel::find($request->id);
        $data_return = [
            'title' => 'Cập nhật từ khóa hot',
            'data' => $data
        ];
        return view('admin.hot_key.form',$data_return);
    }

    /**
     * @param $request
     * @param $rules
     * @param $customMessages
     * @return array
     */
    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /**
     *  Cập nhật từ khóa
     * @param $request
     * @return array
     */
    public function update($request){
        $rules = [
            'key' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data = HotKeyModel::find($request->id);

        if ($data) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/hot_key/';
                $file->move($path, $fileName);
                $data->image = '/uploads/images/hot_key/' . $fileName;
                $data->save();
            }
            $data->key = $request->key;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/hot_key'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    /**
     *  Xóa từ khóa
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        try {
            HotKeyModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
