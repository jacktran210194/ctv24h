<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryProductModel;
use App\Models\VoucherCategoryCTV24hModel;
use App\Models\VoucherCategoryModel;
use App\Models\VoucherCTV24hModel;
use App\Models\VoucherModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class VoucherController extends Controller
{
    /**
     *  Danh sách mã giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexVoucher(){
        $data = VoucherCTV24hModel::orderBy('id', 'desc')->get();
        if ($data) {
            foreach ($data as $k => $v){
                $today = Carbon::now('Asia/Ho_Chi_Minh');
                $category = VoucherCategoryCTV24hModel::find($v['category_id']);
                $data[$k]['cat_name'] = $category['name'] ?? '';
                if ($v->time_end > $today && $v->time_start < $today){
                    $data[$k]['status'] = 'Đang diễn ra';
                    $data[$k]['color'] = 'success';
                } else if ($v->time_end > $today && $v->time_start > $today) {
                    $data[$k]['status'] = 'Sắp diễn ra';
                    $data[$k]['color'] = 'warning';
                } else if ($v->time_start < $today && $v->time_end < $today) {
                    $data[$k]['status'] = 'Đã diễn ra';
                    $data[$k]['color'] = 'danger';
                }
                if ($v->type == 0){
                    $data[$k]['type'] = 'Nhà cung cấp';
                } else if ($v->type == 1) {
                    $data[$k]['type'] = 'Cộng tác viên';
                } else {
                    $data[$k]['type'] = 'Khách hàng';
                }
            }
        }
        $dataReturn = [
            'data' => $data,
            'title' => 'Phiếu giảm giá',
            'voucher' => true
        ];
        return view('admin.voucher.index', $dataReturn);
    }

    /**
     *  Thêm mã giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addVoucher()
    {
        $data_category = CategoryProductModel::orderBy('name', 'asc')->get();
        $data_return = [
            'category' => $data_category,
            'title' => 'Phiếu giảm giá',
            'voucher' => true
        ];
        return view('admin.voucher.form', $data_return);
    }

    /**
     *  Lưu mã giảm giá
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveVoucher(Request $request)
    {
        if ($request->id) {
            $data_return = $this->updateVoucher($request);
        } else {
            $data_return = $this->createVoucher($request);
        }
        return response()->json($data_return, Response::HTTP_OK);
    }

    /**
     * @param $request
     * @param $rules
     * @param $customMessages
     * @return array
     */
    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /**
     *  Thêm mã giảm giá
     * @param $request
     * @return array
     */
    public function createVoucher($request){
        $rules = [
//            'title' => 'required',
//            'code' => 'required',
//            'time_start' => 'required|after:today',
//            'time_end' => 'required',
//            'number' => 'required',
//            'type' => 'required',
//            'min_order_value' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'time_start.after' => 'Ngày bắt đầu không được nhỏ hơn ngày hiện tại'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            if ($request->time_end < $request->time_start){
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Ngày kết thúc phải lớn hơn ngày bắt đầu'
                ];
                return $dataReturn;
            } else {
                $type = $request->type;
                $title = $request->title;
                $code = $request->code;
                $time_start = $request->time_start;
                $time_end = $request->time_end;
                $min_order_value = $request->min_order_value;
                $number = $request->number;
                if($type == 0){
                    $category_id = null;
                    $content = $request->content;
                    $money_or_percent = null;
                    $discount_value = null;
                    $money_max = null;
                    $coin_max = null;
                    $discount_percent = $request->discount_percent;
                    $voucher_value = $request->voucher_value;
                } else {
                    $category_id = $request->category_id;
                    $content = null ;
                    $discount_percent = null;
                    $voucher_value = null;
                    $money_or_percent = $request->money_or_percent;
                    $discount_value = $request->discount_value;
                    $money_max = $request->money_max;
                    $coin_max = $request->coin_max;
                }
                $data = VoucherCTV24hModel::create([
                    'title' => $title,
                    'content' => $content,
                    'code' => $code,
                    'time_start' => $time_start,
                    'time_end' => $time_end,
                    'category_id' => $category_id,
                    'number' => $number,
                    'type' => $type,
                    'min_order_value' => $min_order_value,
                    'discount_percent' => $discount_percent,
                    'voucher_value' => $voucher_value,
                    'money_or_percent' => $money_or_percent,
                    'discount_value' => $discount_value,
                    'money_max' => $money_max,
                    'coin_max' => $coin_max,
                    'image' => ''
                ]);
                $data->save();
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/voucher_ctv24h/';
                $file->move($path, $fileName);
                $data->image = '/uploads/images/voucher_ctv24h/' . $fileName;
                $data->save();
            }
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/voucher'),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Sửa mã giảm giá
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function editVoucher(Request $request){
        $data = VoucherCTV24hModel::find($request->id);
        $data_cate = VoucherCategoryCTV24hModel::orderBy('id', 'desc')->get();
        $data_return = [
            'title' => 'Phiếu giảm giá',
            'voucher' => true,
            'data' => $data,
            'category' => $data_cate,
        ];
        return view('admin.voucher.form', $data_return);
    }

    /**
     *  Cập nhật mã giảm giá
     * @param $request
     * @return array
     */
    public function updateVoucher($request){
        $rules = [
            'title' => 'required',
            'content' => 'required',
            'code' => 'required',
            'time_start' => 'required|after:today',
            'time_end' => 'required',
            'number' => 'required',
            'type' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'time_start.after' => 'Ngày bắt đầu không được nhỏ hơn ngày hiện tại'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data = VoucherCTV24hModel::find($request->id);

        if ($data) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/voucher_ctv24h/';
                $file->move($path, $fileName);
                $data->image = '/uploads/images/voucher_ctv24h/' . $fileName;
                $data->save();
            }
            $data->title = $request->title;
            $data->content = $request->content;
            $data->code = $request->code;
            $data->time_start = $request->time_start;
            $data->time_end = $request->time_end;
            $data->category_id = $request->category_id ? : '';
            $data->number = $request->number;
            $data->type = $request->type;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/voucher'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    /**
     *  Xóa mã giảm giá
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteVoucher($id)
    {
        try {
            $data = VoucherCTV24hModel::find($id);
            if ($data) {
                File::delete($data->image);
            }
            VoucherCTV24hModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }


    /** Danh mục mã giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexCategory()
    {
        $data = VoucherCategoryCTV24hModel::orderBy('id', 'desc')->get();
        $data_return = [
            'voucher_category_ctv24h' => true,
            'data' => $data,
            'title' => 'Phiếu giảm giá',
        ];
        return view('admin.voucher.voucher_category_index', $data_return);
    }

    /** Thêm danh mục mã giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addCategory()
    {
        $data_return = [
            'title' => 'Phiếu giảm giá',
            'voucher_category' => true
        ];
        return view('admin.voucher.voucher_category_form', $data_return);
    }

    /**
     * Lưu thông tin danh mục mã giảm giá
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveCategory(Request $request)
    {
        if ($request->id) {
            $data_return = $this->updateCategory($request);
        } else {
            $data_return = $this->createCategory($request);
        }
        return response()->json($data_return, Response::HTTP_OK);
    }

    /**
     *  Thêm danh mục mã giảm giá
     * @param $request
     * @return array
     */
    public function createCategory($request){
        $rules = [
            'name' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000', // max 10000kb = 10MB
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'image.mimes' => 'Không đúng định dạng ảnh',
            'image.max' => 'Dung lượng ảnh vượt quá 10MB'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $name = $request->name;
            $data = VoucherCategoryCTV24hModel::create([
                'name' => $name,
                'image' => ''
            ]);
            $data->save();
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/voucher_ctv24/category/';
            $file->move($path, $fileName);
            $data->image = '/uploads/images/voucher_ctv24/category/' . $fileName;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/voucher/category'),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Sửa danh mục mã giảm giá
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function editCategory(Request $request){
        $data = VoucherCategoryCTV24hModel::find($request->id);
        $data_return = [
            'title' => 'Phiếu giảm giá',
            'voucher_category_ctv24h' => true,
            'data' => $data,
        ];
        return view('admin.voucher.voucher_category_form', $data_return);
    }

    /**
     *  Cập nhật danh mục mã giảm giá
     * @param $request
     * @return array
     */
    public function updateCategory($request){
        $rules = [
            'name' => 'required',
            'image' => 'max:10000', // max 10000kb = 10MB
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'image.max' => 'Dung lượng ảnh vượt quá 10MB'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data = VoucherCategoryCTV24hModel::find($request->id);

        if ($data) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/voucher_category/';
                $file->move($path, $fileName);
                $data->image = '/uploads/images/voucher_category/' . $fileName;
                $data->save();
            }

            $data->name = $request->name;
            $data->active = $request->active;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/voucher/category'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    /**
     *  Xóa danh mục mã giảm giá
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCategory($id)
    {
        try {
            $data = VoucherCategoryCTV24hModel::find($id);
            if ($data) {
                File::delete($data->image);
            }
            VoucherCategoryCTV24hModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
