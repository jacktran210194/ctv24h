<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BankAccountModel;
use App\Models\CollaboratorModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
class CollaboratorController extends Controller
{
    /**
     * CollaboratorController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        if (!$request->session()->has('data_user')) {
            Redirect::to('admin/login')->send();
        }
    }

    /** Danh sách CTV
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $data = CollaboratorModel::orderBy('id', 'desc')->get();
        $data_return = [
            'collaborator' => true,
            'data' => $data,
            'title' => 'Danh sách cộng tác viên',
        ];
        return view('admin.collaborator.index', $data_return);
    }


    /** Thêm CTV
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $data_return = [
            'title' => 'Thêm cộng tác viên',
            'collaborator' => true
        ];
        return view('admin.collaborator.form', $data_return);
    }

    /**
     * Lưu thông tin CTV
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        if ($request->id) {
            $data_return = $this->update($request);
        } else {
            $data_return = $this->create($request);
        }
        return response()->json($data_return, Response::HTTP_OK);
    }

    /**
     *  Thêm mới CTV
     * @param $request
     * @return array
     */
    public function create($request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|unique:collaborator,phone,' . $request->id,
            'password' => 'required',
            'birthday' => 'required',
            'sex' => 'required',
            'status' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000', // max 10000kb = 10MB
            'username' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'email.email' => 'Email không hợp lệ',
            'phone.unique' => 'Số điện thoại đã được đăng kí',
            'image.mimes' => 'Không đúng định dạng ảnh',
            'image.max' => 'Dung lượng ảnh vượt quá 10MB'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $name = $request->name;
            $email = $request->email;
            $phone = $request->phone;
            $username = $request->username;
            $password = md5($request->password);
            $birthday = date('Y-m-d', strtotime($request->birthday));
            $sex = $request->sex;
            $status = $request->status;

            $data = CollaboratorModel::create([
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'username' => $username,
                'password' => $password,
                'birthday' => $birthday,
                'sex' => $sex,
                'status' => $status,
                'image' => ''
            ]);
            $data->save();
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/collaborator/';
            $file->move($path, $fileName);
            $data->image = '/uploads/images/collaborator/' . $fileName;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/collaborator'),
            ];
        }
        return $dataReturn;
    }

    /** Cập nhật thông tin CTV
     * @param $request
     * @return array
     */
    public function update($request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|unique:collaborator,phone,' . $request->id,
            'password' => 'required',
            'birthday' => 'required',
            'sex' => 'required',
            'status' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'email.email' => 'Email không hợp lệ',
            'phone.unique' => 'Số điện thoại đã được đăng kí',
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data = CollaboratorModel::find($request->id);
        if ($data) {
            $data->name = $request->name;
            $data->email = $request->email;
            $data->phone = $request->phone;
            $data->password = md5($request->password);
            $data->birthday = $request->birthday;
            $data->sex = $request->sex;
            $data->status = $request->status;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/collaborator'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    /**
     * @param $request
     * @param $rules
     * @param $customMessages
     * @return array
     */
    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /**
     *  Cập nhập CTV
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Request $request)
    {
        $data = CollaboratorModel::find($request->id);
        $data_return = [
            'title' => 'Sửa thông tin cộng tác viên',
            'collaborator' => true,
            'data' => $data,
        ];
        return view('admin.collaborator.form', $data_return);
    }

    /** Xóa CTV
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        try {
            CollaboratorModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     *  Ví CTV
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getWallet(Request $request)
    {
        $data = BankAccountModel::where('user_id', '=', $request->id)
            ->where('type', 2)
            ->orderBy('created_at', 'desc')
            ->get();
        if ($data) {
            foreach ($data as $k => $v) {
                $data_user = CollaboratorModel::find($v['user_id']);
                $data[$k]['name_ctv'] = $data_user['name'];
                $data[$k]['user_id'] = $data_user['id'];
                $data[$k]['wallet_ctv'] = $data_user['wallet'];
            }
        }

        $data_ctv = CollaboratorModel::find($request->id);
        $data_return = [
            'collaborator' => true,
            'data' => $data,
            'data_ctv' => $data_ctv,
            'title' => 'Tài khoản ngân hàng',
        ];
        return view('admin.collaborator.wallet', $data_return);
    }

    /**
     *  Thêm tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addBank(Request $request)
    {
        $data = CollaboratorModel::find($request->id);
        $data_return = [
            'title' => 'Thêm tài khoản ngân hàng',
            'data' => $data
        ];
        return view('admin.collaborator.form_bank', $data_return);
    }

    /**
     *  Thêm tài khoản ngân hàng
     * @param Request $request
     * @return array
     */
    public function createBank(Request $request)
    {
        $rules = [
            'user_name' => 'required',
            'bank_name' => 'required',
            'bank_account' => 'required|unique:bank_account,bank_account,' . $request->id,
            'date_active' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'bank_account.unique' => 'Số tài khoản đã tồn tại'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $data = new BankAccountModel([
                'user_id' => $request->id,
                'user_name' => $request->user_name,
                'bank_name' => $request->bank_name,
                'bank_account' => $request->bank_account,
                'type' => 2,
                'date_active' => $request->date_active,
            ]);
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/collaborator/wallet/' . $request->id),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Xóa tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteBank(Request $request)
    {
        try {
            BankAccountModel::destroy($request->id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     *  Liên kết tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function linkBank(Request $request){
        $data = BankAccountModel::where('id', $request->id)->update(['is_active' => 1]);
        $dataReturn = [
            'status' => true,
            'msg' => 'Liên kết thành công',
            'data' => $data
        ];
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     *  Hủy liên kết tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
     public function unlinkBank(Request $request){
         $data = BankAccountModel::where('id', $request->id)->update(['is_active' => 0]);
         $dataReturn = [
             'status' => true,
             'msg' => 'Hủy liên kết thành công',
             'data' => $data
         ];
         return response()->json($dataReturn, Response::HTTP_OK);
     }

     public function getSupplier(){
         $data_return = [
             'title' => 'Quản lý nhà cung cấp',
         ];
         return view('admin.collaborator.supplier', $data_return);
     }
}
