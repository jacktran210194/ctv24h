<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryShopModel;
use App\Models\ProductImageModel;
use App\Models\ShopImageModel;
use App\Models\ShopModel;
use App\Models\SupplierModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;

class ShopController extends Controller
{
    public function index()
    {
        $data_shop = ShopModel::orderBy('id', 'desc')->get();
        foreach ($data_shop as $key => $value) {
            $data_category_shop = CategoryShopModel::find($value['category_id']);
            $data_shop[$key]['category'] = $data_category_shop['name'];
        }
        $data_return = [
            'status' => true,
            'title' => 'Quản lý cửa hàng',
            'data' => $data_shop
        ];
        return view('admin.shop.index', $data_return);
    }

    public function add()
    {
        $dataSupplier  = SupplierModel::orderBy('id', 'desc')->get();
        $data_category_shop = CategoryShopModel::orderBy('id', 'desc')->get();
        $data_return = [
            'title' => 'Thêm mới cửa hàng',
            'status' => true,
            'category_shop' => $data_category_shop,
            'dataSupplier' => $dataSupplier
        ];
        return view('admin.shop.form', $data_return);
    }

    public function save(Request $request)
    {
        if ($request->id) {
            $dataReturn = $this->update($request);
        } else {
            $dataReturn = $this->create($request);
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function create($request)
    {
        $rules = [
            'name' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $data_shop = new ShopModel([
                'name' => $request->name,
                'slug' => Str::slug($request->name),
                'date' => $request->date,
                'time_feedback' => $request->time_feedback,
                'report' => $request->report,
                'category_id' => $request->category_id,
                'desc' => $request->desc,
                'video' => $request->video,
                'supplier_id' => $request->supplier_id,
                'image' => ''
            ]);
            $data_shop->save();
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/shop/';
            $file->move($path, $fileName);
            $data_shop->image = '/uploads/images/shop/' . $fileName;
            $data_shop->save();
            $get_image = $request->file('img');
            if (is_array($get_image) || is_object($get_image)) {
                foreach ($get_image as $file1) {
                    $fileName1 = Str::random(20) . '.' . $file1->getClientOriginalExtension();
                    $path1 = public_path() . '/uploads/images/shop/' . $data_shop['id'] . '/';
                    $file1->move($path1, $fileName1);
                    $data_image = new ShopImageModel([
                        'shop_id' => $data_shop['id'],
                        'image' => '/uploads/images/shop/' . $data_shop['id'] . '/' . $fileName1
                    ]);
                    $data_image->save();
                }
            }

            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/shop'),
            ];
        }
        return $dataReturn;
    }

    public function edit(Request $request, $id)
    {
        $dataSupplier  = SupplierModel::orderBy('id', 'desc')->get();
        $data_shop = ShopModel::find($request->id);
        $data_image = ShopImageModel::where('shop_id', $id)->get();
//        $data_category_shop = CategoryShopModel::orderBy('id', 'desc')->get();
        $data_return = [
            'title' => 'Cập nhật cửa hàng',
            'status' => true,
            'data' => $data_shop,
//            'category_shop' => $data_category_shop,
            'dataSupplier' => $dataSupplier,
            'data_image' => $data_image,
        ];
        return view('admin.shop.form', $data_return);
    }

    public function update($request)
    {
//        $rules = [
//            'name' => 'required',
//        ];
//        $customMessages = [
//            'required' => 'Vui lòng điền đầy đủ thông tin.',
//        ];
//        $dataReturn = $this->checkRule($request, $rules, $customMessages);
//        if (!$dataReturn['status']) {
//            return $dataReturn;
//        }
        $data_shop = ShopModel::find($request->id);
        if ($data_shop) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_shop->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/shop/';
                $file->move($path, $fileName);
                $data_shop->image = '/uploads/images/shop/' . $fileName;
                $data_shop->save();
            }
            $data_shop->name = $request->name;
//            $data_shop->date = $request->date;
//            $data_shop->time_feedback = $request->time_feedback;
//            $data_shop->report = $request->report;
//            $data_shop->category_id = $request->category_id;
            $data_shop->video = $request->video;
            $data_shop->desc = $request->desc;
            $data_shop->supplier_id = $request->supplier_id;
            $data_shop->save();
            $get_image = $request->file('img');
            if (is_array($get_image) || is_object($get_image)) {
                foreach ($request->file('img') as $file1) {
                    $fileName1 = Str::random(20) . '.' . $file1->getClientOriginalExtension();
                    $path1 = public_path() . '/uploads/images/shop/' . $data_shop->id . '/';
                    $file1->move($path1, $fileName1);
                    $data_image = ShopImageModel::create([
                        'shop_id' => $data_shop->id,
                        'image' => '/uploads/images/shop/' . $data_shop->id . '/' . $fileName1
                    ]);
                    $data_image->save();
                }
            }
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/shop'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    public function delete($id)
    {
        try {
            ShopModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete_image($id)
    {
        try {
            ShopImageModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     *
     * Category Shop
     *
     */

    public function indexCategory()
    {
        $data_category_shop = CategoryShopModel::orderBy('id', 'desc')->get();
        $data_return = [
            'status' => true,
            'title' => 'Quản lý danh mục cửa hàng',
            'data' => $data_category_shop
        ];
        return view('admin.category_shop.index', $data_return);
    }

    public function addCategory(){
        $data_return = [
          'status' => true,
          'title' => 'Thêm mới danh mục cửa hàng'
        ];

        return view('admin.category_shop.form',$data_return);
    }

    public function saveCategory(Request $request)
    {
        if ($request->id) {
            $dataReturn = $this->updateCategory($request);
        } else {
            $dataReturn = $this->createCategory($request);
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function createCategory($request)
    {
        $rules = [
            'name' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $data_category = CategoryShopModel::create([
                'name' => $request->name,
                'image' => ''
            ]);
            $data_category->save();
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/category_shop/';
            $file->move($path, $fileName);
            $data_category->image = '/uploads/images/category_shop/' . $fileName;
            $data_category->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/shop/category'),
            ];
        }
        return $dataReturn;
    }

    public function editCategory(Request $request){
        $data_category = CategoryShopModel::find($request->id);
        $data_return = [
          'title' => 'Cập nhật danh mục cửa hàng',
          'data' => $data_category
        ];
        return view('admin.category_shop.form',$data_return);
    }

    public function updateCategory($request)
    {
        $rules = [
            'name' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data_category = CategoryShopModel::find($request->id);

        if ($data_category) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_category->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/category_shop/';
                $file->move($path, $fileName);
                $data_category->image = '/uploads/images/category_shop/' . $fileName;
                $data_category->save();
            }
            $data_category->name = $request->name;
            $data_category->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/shop/category'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function deleteCategory($id)
    {
        try {
            CategoryShopModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
