<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BankAccountModel;
use App\Models\SupplierModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;

class SupplierController extends Controller
{
    /**
     * danh sách nhà cung cấp
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     *
     */
    public function index()
    {
        $data_supplier = SupplierModel::orderBy('id', 'desc')->get();
        $data_return = [
            'category' => true,
            'title' => 'Quản lý nhà cung cấp',
            'data' => $data_supplier
        ];
        return view('admin.supplier.index', $data_return);
    }

    /**
     * add
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     *
     */
    public function add()
    {
        $data_return = [
            'title' => 'Thêm nhà cung cấp',
            'supplier' => true,
        ];
        return view('admin.supplier.form', $data_return);
    }

    public function save(Request $request)
    {
        if ($request->id) {
            $dataReturn = $this->update($request);
        } else {
            $dataReturn = $this->create($request);
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     * thêm mới NCC
     *
     * @param $request
     * @return array
     *
     */
    public function create($request)
    {
        $rules = [
            'name' => 'required',
            'phone' => 'required|unique:supplier,phone',
            'password' => 'required|min:8',
            'id_card' => 'required|unique:supplier,id_card|numeric',
            'business_license' => 'required',
            'product_supply' => 'required',
            'import_papers' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'phone.unique' => 'Số điện thoại đã tồn tại !',
            'id_card.unique' => 'Số CMND đã tồn tại !',
            'password.min' => 'Mật khẩu phải tối thiểu 8 ký tự, bao gồm cả chữ và số !',
            're_password.same' => 'Mật khẩu không trùng khớp !',
            'id_card.numeric' => 'CMND chỉ được nhập số !'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $data_supplier = SupplierModel::create([
                'name' => $request->name,
                'phone' => $request->phone,
                'username' => $request->username,
                'email' => $request->email,
                'birthday' => $request->birthday,
                'gender' => $request->gender,
                'password' => md5($request->password),
                'id_card' => $request->id_card,
                'business_license' => $request->business_license,
                'product_supply' => $request->product_supply,
                'import_papers' => $request->import_papers,
                'image' => ''
            ]);
            $data_supplier->save();
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/supplier/';
            $file->move($path, $fileName);
            $data_supplier->image = '/uploads/images/supplier/' . $fileName;
            $data_supplier->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/supplier'),
            ];
        }
        return $dataReturn;
    }

    /**
     * edit
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     *
     */
    public function edit(Request $request)
    {
        $data_supplier = SupplierModel::find($request->id);

        $data_return = [
            'title' => 'Cập nhật nhà cung cấp',
            'status' => true,
            'data' => $data_supplier
        ];
        return view('admin.supplier.form', $data_return);
    }

    /**
     * update
     *
     * @param $request
     * @return array
     *
     */
    public function update($request)
    {
        $rules = [
            'name' => 'required',
            'phone' => 'required|unique:supplier,phone,' . $request->id,
            'password' => 'required|min:8',
            'id_card' => 'required|numeric|unique:supplier,id_card,' . $request->id,
            'business_license' => 'required',
            'product_supply' => 'required',
            'import_papers' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'phone.unique' => 'Số điện thoại đã tồn tại !',
            'id_card.unique' => 'Số CMND đã tồn tại !',
            'password.min' => 'Mật khẩu phải tối thiểu 8 ký tự, bao gồm cả chữ và số !',
            're_password.same' => 'Mật khẩu không trùng khớp !',
            'id_card.numeric' => 'CMND chỉ được nhập số !'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data_supplier = SupplierModel::find($request->id);

        if ($data_supplier) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_supplier->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/supplier/';
                $file->move($path, $fileName);
                $data_supplier->image = '/uploads/images/supplier/' . $fileName;
                $data_supplier->save();
            }
            $data_supplier->name = $request->name;
            $data_supplier->email = $request->email;
            $data_supplier->username = $request->username;
            $data_supplier->phone = $request->phone;
            $data_supplier->password = md5($request->password);
            $data_supplier->id_card = $request->id_card;
            $data_supplier->business_license = $request->business_license;
            $data_supplier->product_supply = $request->product_supply;
            $data_supplier->import_papers = $request->import_papers;
            $data_supplier->birthday = $request->birthday;
            $data_supplier->gender = $request->gender;
            $data_supplier->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/supplier'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /**
     * delete
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function delete($id)
    {
        try {
            SupplierModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * get ví
     *
     */

    public function getWallet($id)
    {
        $data = BankAccountModel::where('user_id', $id)
            ->where('type', 3)
            ->orderBy('created_at', 'desc')
            ->get();
        $data_ncc = SupplierModel::find($id);
        $data_return = [
            'status' => true,
            'data' => $data,
            'data_ncc' => $data_ncc,
            'title' => 'Tài khoản ngân hàng',
        ];
        return view('admin.supplier.wallet', $data_return);
    }

    public function addBank(Request $request)
    {
        $data = SupplierModel::find($request->id);
        $data_return = [
            'status' => true,
            'title' => 'Thêm tài khoản ngân hàng',
            'data' => $data
        ];
        return view('admin.supplier.form_bank', $data_return);
    }

    public function postAddbank(Request $request)
    {
        $rules = [
            'user_name' => 'required',
            'bank_account' => 'required|unique:bank_account,bank_account',
            'bank_name' => 'required',
            'date_active' => 'required'

        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'bank_account.unique' => 'Số tài khoản đã tồn tại !'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $data = $request->all();
            $data_bank = new BankAccountModel([
                'user_name' => $data['user_name'],
                'bank_name' => $data['bank_name'],
                'bank_account' => $data['bank_account'],
                'type' => 3,
                'date_active' => $data['date_active'],
                'user_id' => $data['user_id']
            ]);
            $data_bank->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/supplier/wallet/' . $data_bank['user_id']),
            ];
        }
        return $dataReturn;
    }

    public function active($id){
        $data_bank = BankAccountModel::find($id);
        if($data_bank['is_active']==0){
            DB::table('bank_account')->where('id', $data_bank['id'])->update(['is_active' => 1]);
        }else{
            DB::table('bank_account')->where('id', $data_bank['id'])->update(['is_active' => 0]);
        }
        return back();
    }

    public function deleteBank($id){
        try {
            BankAccountModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

}
