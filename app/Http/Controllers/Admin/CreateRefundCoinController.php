<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use App\Http\Controllers\Controller;
use App\Models\CategoryProductModel;
use App\Models\VoucherRefundCoinModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class CreateRefundCoinController extends Controller
{
    public function index()
    {
        $data = VoucherRefundCoinModel::orderBy('id', 'desc')->get();
        $time = date('Y-m-d'). ' ' .date("h:i:s");
        $data_voucher = VoucherRefundCoinModel::get();
        if (isset($data_voucher)){
            foreach ($data_voucher as $value){
                if (date(strtotime($value->time_stop)) <= date(strtotime($time))){
                    $voucher = VoucherRefundCoinModel::where('time_stop', $value->time_stop)->first();
                    $voucher->status = 2;
                    $voucher->save();
                }
            }
        }
        $dataReturn = [
            'data' => $data,
            'title' => 'Hoàn Đơn Xu Bất Kỳ',
            'voucher' => true
        ];
        return view('admin.refund_coin.index', $dataReturn);
    }
    public function add(){
        $category = CategoryProductModel::get();
        $data_return = [
            'category' => $category,
        ];
        return view('admin.refund_coin.add', $data_return);
    }
    public function addVoucher(Request $request){
        $rules = [
            'title' => 'required',
            'code' => 'required',
            'time_start' => 'required',
            'time_end' => 'required',
            'number'=> 'required',
            'max_refund'=> 'required',
            'number_coin'=> 'required',
            'min_price' => 'required'
        ];
        $data_request = [
            'title.required' => 'vui lòng điền title',
            'code.required' => 'vui lòng điền mã voucher',
            'time_start.required' => 'vui lòng chọn thời gian bắt đầu',
            'time_end.required' => 'vui lòng chọn thời gian kết thúc',
            'number.required' => 'vui lòng điền số lượng',
            'max_refund.required' => 'vui lòng điền số xu hoàn tối đa',
            'number_coin.required' => 'vui lòng điền hoàn xu',
            'min_price.required' => 'vui lòng điền đơn giá tối thiểu'
        ];
        $validator = Validator::make($request->all(), $rules, $data_request);
        if ($validator->fails()){
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        }else{
            if ($request->number < 0 || $request->min_price < 0 || $request->max_refund < 0 || $request->number_coin < 0){
                $dataReturn['status'] = false;
                $dataReturn['msg'] = 'giá trị không được nhỏ hơn 0';
            }else if (date(strtotime($request->time_start)) > date(strtotime($request->time_end))){
                $dataReturn['status'] = false;
                $dataReturn['msg'] = 'ngày bắt đầu không được trước ngày kết thúc';
            }else if ( $request->style_refund_coin == 1 && $request->max_refund < $request->number_coin ){
                $dataReturn['status'] = false;
                $dataReturn['msg'] = 'số xu không được lớn hơn số hoàn xu tối đa';
            }else{
                $voucher_refund_coin = VoucherRefundCoinModel::get();
                if (isset($voucher_refund_coin)){
                    $check = false;
                    foreach ($voucher_refund_coin as $value){
                        if ($value->code_voucher == $request->code){
                            $check = true;
                        }
                    }
                    if ($check){
                        $dataReturn['status'] = false;
                        $dataReturn['msg'] = 'mã code đã tồn tại';
                    }else{
                        $voucher = new VoucherRefundCoinModel([
                            'code_voucher' => $request->code,
                            'time_star' => $request->time_start,
                            'time_stop' => $request->time_end,
                            'min_price' => $request->min_price,
                            'max_point' => $request->max_refund,
                            'type' => $request->style_refund_coin,
                            'category_id' => $request->category_id,
                            'name' => $request->title,
                            'quantity'=>$request->number,
                            'refund_coin' =>$request->number_coin
                        ]);
                        $voucher->save();
                        $dataReturn=[
                            'status' => true,
                            'msg' => 'thêm mã hoàn xu thành công'
                        ];
                    }
                }else{
                    $voucher = new VoucherRefundCoinModel([
                        'code_voucher' => $request->code,
                        'time_star' => $request->time_start,
                        'time_stop' => $request->time_end,
                        'min_price' => $request->min_price,
                        'max_point' => $request->max_refund,
                        'type' => $request->style_refund_coin,
                        'category_id' => $request->category_id,
                        'name' => $request->title,
                        'quantity'=>$request->number,
                        'refund_coin' =>$request->number_coin,
                    ]);
                    $voucher->save();
                    $dataReturn=[
                        'status' => true,
                        'msg' => 'thêm mã hoàn xu thành công'
                    ];
                }
            }
        }
        return $dataReturn;
    }
    function delete($id){
        VoucherRefundCoinModel::where('id', $id)->delete();
        $data_return=[
            'status'=> true,
            'msg'=> 'Xóa thành công'
        ];
        return $data_return;
    }
}
