<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\HistoryDealCTV24;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductModel;
use App\Models\WalletCTV24Model;
use Illuminate\Http\Request;

class RevenueCTV24HController extends Controller
{
    /**
     *  Doanh thu sàn CTV24H
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){

        $data_wallet_ctv24 = WalletCTV24Model::first();
        $dataReturn = [
            'title' => 'Doanh thu sàn CTV24H',
            'wallet_ctv24h' => $data_wallet_ctv24
        ];
        return view('admin.revenue_ctv24h.index', $dataReturn);
    }

    public function statusAll(){
        $data = HistoryDealCTV24::orderBy('id', 'desc')->get();
        if($data){
            foreach ($data as $k => $v){
                if($v->type == 0){
                    $data[$k]->type = 'Doanh thu';
                } else if($v->type == 1){
                    $data[$k]->type = 'Rút tiền';
                } else if($v->type ==2){
                    $data[$k]->type = 'Hoàn tiền';
                } else {
                    $data[$k]->type = 'Hoa hồng giới thiệu';
                }
                $dataOrder = OrderModel::find($v['order_id']);
                $data[$k]->order_code = $dataOrder['order_code'] ??'';
                $dataOrderDetail = OrderDetailModel::where('order_id', $dataOrder['id'])->first();
                $dataProduct = ProductModel::find($dataOrderDetail['product_id']);
                $data[$k]->name_product = $dataProduct['name'] ?? '';
            }
        }
//        dd($data);
        $dataReturn = [
            'data' => $data,
            'title' => 'Doanh thu sàn CTV24h'
        ];
        return view('admin.revenue_ctv24h.status_all', $dataReturn);
    }

    /**
     *  Doanh thu từ đơn hàng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function statusRevenue(){
        $data = HistoryDealCTV24::where('type', 0)->orderBy('id', 'desc')->get();
        if($data){
            foreach ($data as $k => $v){
                if($v->type == 0){
                    $data[$k]->type = 'Doanh thu';
                } else if($v->type == 1){
                    $data[$k]->type = 'Rút tiền';
                } else if($v->type ==2){
                    $data[$k]->type = 'Hoàn tiền';
                } else {
                    $data[$k]->type = 'Hoa hồng giới thiệu';
                }
                $dataOrder = OrderModel::find($v['order_id']);
                $data[$k]->order_code = $dataOrder['order_code'] ??'';
                $dataOrderDetail = OrderDetailModel::where('order_id', $dataOrder['id'])->first();
                $dataProduct = ProductModel::find($dataOrderDetail['product_id']);
                $data[$k]->name_product = $dataProduct['name'] ?? '';
            }
        }
        $dataReturn = [
            'data' => $data,
            'title' => 'Doanh thu sàn CTV24h'
        ];
        return view('admin.revenue_ctv24h.status_revenue', $dataReturn);
    }

    /**
     *  Rút tiền từ đơn hàng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function statusWithdrawal(){
        $data = HistoryDealCTV24::where('type', 1)->orderBy('id', 'desc')->get();
        if($data){
            foreach ($data as $k => $v){
                if($v->type == 0){
                    $data[$k]->type = 'Doanh thu';
                } else if($v->type == 1){
                    $data[$k]->type = 'Rút tiền';
                } else if($v->type ==2){
                    $data[$k]->type = 'Hoàn tiền';
                } else {
                    $data[$k]->type = 'Hoa hồng giới thiệu';
                }
                $dataOrder = OrderModel::find($v['order_id']);
                $data[$k]->order_code = $dataOrder['order_code'] ??'';
                $dataOrderDetail = OrderDetailModel::where('order_id', $dataOrder['id'])->first();
                $dataProduct = ProductModel::find($dataOrderDetail['product_id']);
                $data[$k]->name_product = $dataProduct['name'] ?? '';
            }
        }
        $dataReturn = [
            'data' => $data,
            'title' => 'Doanh thu sàn CTV24h'
        ];
        return view('admin.revenue_ctv24h.status_withdrawal', $dataReturn);
    }

    public function statusRefund(){
        $dataReturn = [
            'title' => 'Doanh thu sàn CTV24h'
        ];
        return view('admin.revenue_ctv24h.status_refund', $dataReturn);
    }

    public function statusBonus(){
        $dataReturn = [
            'title' => 'Doanh thu sàn CTV24h'
        ];
        return view('admin.revenue_ctv24h.status_bonus', $dataReturn);
    }
}
