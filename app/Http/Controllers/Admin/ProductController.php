<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AttributeValueModel;
use App\Models\BrandModel;
use App\Models\CategoryProductChildModel;
use App\Models\CategoryProductModel;
use App\Models\ProductAttributeModel;
use App\Models\ProductImageModel;
use App\Models\ProductModel;
use App\Models\ShopModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index()
    {
//        $data = ProductModel::orderBy('id', 'desc')->get();
//        if (count($data)) {
//            foreach ($data as $key => $value) {
//                $data[$key]['dropship'] = ($value['dropship'] == 1) ? 'Có' : 'Không';
//                $data_shop = ShopModel::find($value['shop_id']);
//                $data[$key]['shop_name'] = isset($data_shop) ? $data_shop['name'] : 'Chưa đặt tên';
//                $data_category = CategoryProductChildModel::find($value['category_id']);
//                $data[$key]['category'] = $data_category['name'] ?? '';
//                if ($value['is_status'] == 0) {
//                    $data[$key]['status'] = 'Sản phẩm còn hàng';
//                } elseif ($value['is_status'] == 1) {
//                    $data[$key]['status'] = 'Sản phẩm hết hàng';
//                } elseif ($value['is_status'] == 2) {
//                    $data[$key]['status'] = 'Sản phẩm bị đánh vi phạm';
//                } else {
//                    $data[$key]['status'] = 'Sản phẩm bị ẩn';
//                }
//            }
//        }
        $data_return = [
            'status' => true,
            'title' => 'Quản lý sản phẩm',
//            'data' => $data
        ];

        return view('admin.product.index', $data_return);
    }

    public function add()
    {
        $data_shop = ShopModel::orderBy('id', 'desc')->get();
        $data_brand = BrandModel::orderBy('id', 'desc')->get();
        $data_category = CategoryProductChildModel::orderBy('id', 'desc')->get();
        $data_return = [
            'status' => true,
            'title' => 'Thêm mới sản phẩm',
            'category' => isset($data_category) ? $data_category : '',
            'brand' => isset($data_brand) ? $data_shop : '',
            'shop' => isset($data_shop) ? $data_shop : ''
        ];
        return view('admin.product.form', $data_return);
    }

    public function save(Request $request)
    {
        if ($request->id) {
            $dataReturn = $this->update($request);
        } else {
            $dataReturn = $this->create($request);
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function create($request)
    {
        $rules = [
            'name' => 'required',
            'price' => 'required',
            'price_discount' => 'required',
            'sku_type' => 'required',
            'desc' => 'required',
            'warehouse' => 'required',
            'width' => 'required',
            'height' => 'required',
            'length' => 'required',
            'weight' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->getMessageBag();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $data_shop = new ProductModel([
                'name' => $request->name,
                'slug' => Str::slug($request->name),
                'price' => $request->price,
                'price_discount' => $request->price_discount,
                'sku_type' => $request->sku_type,
                'warehouse' => $request->warehouse,
                'desc' => $request->desc,
                'brand' => $request->brand,
                'dropship' => $request->dropship,
                'is_status' => $request->is_status,
                'category_id' => $request->category_id,
                'weight' => $request->weight,
                'height' => $request->height,
                'length' => $request->length,
                'width' => $request->width,
                'original' => $request->original,
                'material' => $request->material,
                'shop_id' => $request->shop_id,
                'sort_desc' => $request->sort_desc,
                'image' => '',
            ]);
            $data_shop->save();
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/product/';
            $file->move($path, $fileName);
            $data_shop->image = '/uploads/images/product/' . $fileName;
            $data_shop->save();
            $get_image = $request->file('img');
            if (is_array($get_image) || is_object($get_image)) {
                foreach ($get_image as $file1) {
                    $fileName1 = Str::random(20) . '.' . $file1->getClientOriginalExtension();
                    $path1 = public_path() . '/uploads/images/product/' . $data_shop['id'] . '/';
                    $file1->move($path1, $fileName1);
                    $data_image = new ProductImageModel([
                        'product_id' => $data_shop['id'],
                        'image' => '/uploads/images/product/' . $data_shop['id'] . '/' . $fileName1
                    ]);
                    $data_image->save();
                }
            }
            $attr = json_decode($request->attr);
            if ($request->check_attr == '1' && count($attr)) {
                foreach ($attr as $value) {
                    $product_attribute = new ProductAttributeModel();
                    $product_attribute->name = $value->name;
                    $product_attribute->product_id = $data_shop->id;
                    $product_attribute->save();
                    foreach ($value->item as $item) {
                        $attribute_value = new AttributeValueModel();
                        $attribute_value->value = $item->name;
                        $attribute_value->attribute_id = $product_attribute->id;
                        $attribute_value->save();
                    }
                }
            }
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/product'),
            ];
        }
        return $dataReturn;
    }

    public function edit($id)
    {
        $data = ProductModel::find($id);
        $data_shop = ShopModel::orderBy('id', 'desc')->get();
        $data_image = ProductImageModel::where('product_id', $id)->get();
        $data_category = CategoryProductChildModel::orderBy('id', 'desc')->get();
        $data_brand = BrandModel::orderBy('id', 'desc')->get();
        $attr = ProductAttributeModel::where('product_id', $data->id)->get();
        foreach ($attr as $key => $value) {
            $attr_value = AttributeValueModel::where('attribute_id', $value['id'])->get();
            $attr[$key]['item'] = $attr_value;
        }
        $data_attr['attr'] = $attr;
        $html_attr = view('admin.product.form_attr', $data_attr);
//        $html_item = view('admin.product.form_item',$attr_value);
        $data_return = [
            'status' => true,
            'title' => 'Cập nhật sản phẩm',
            'category' => $data_category,
            'brand' => $data_brand,
            'data' => $data,
            'data_image' => $data_image,
            'attr' => $attr,
            'html_attr' => $html_attr,
            'shop' => $data_shop
        ];
        return view('admin.product.form', $data_return);
    }

    public function update($request)
    {
        $rules = [
            'name' => 'required',
            'price' => 'required',
            'sku_type' => 'required',
            'desc' => 'required',
            'warehouse' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data_shop = ProductModel::find($request->id);
        if ($data_shop) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_shop->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/product/';
                $file->move($path, $fileName);
                $data_shop->image = '/uploads/images/product/' . $fileName;
                $data_shop->save();
            }
            $data_shop->name = $request->name;
            $data_shop->slug = Str::slug($request->name);
            $data_shop->price = $request->price;
            $data_shop->price_discount = $request->price_discount;
            $data_shop->sku_type = $request->sku_type;
            $data_shop->category_id = $request->category_id;
            $data_shop->brand = $request->brand;
            $data_shop->desc = $request->desc;
            $data_shop->is_status = $request->is_status;
            $data_shop->warehouse = $request->warehouse;
            $data_shop->dropship = $request->dropship;
            $data_shop->original = $request->original;
            $data_shop->material = $request->material;
            $data_shop->weight = $request->weight;
            $data_shop->length = $request->length;
            $data_shop->width = $request->width;
            $data_shop->height = $request->height;
            $data_shop->sort_desc = $request->sort_desc;
            $data_shop->shop_id = $request->shop_id;
            $data_shop->save();
            $get_image = $request->file('img');
            if (is_array($get_image) || is_object($get_image)) {
                foreach ($request->file('img') as $file1) {
                    $fileName1 = Str::random(20) . '.' . $file1->getClientOriginalExtension();
                    $path1 = public_path() . '/uploads/images/product/' . $data_shop->id . '/';
                    $file1->move($path1, $fileName1);
                    $data_image = ProductImageModel::create([
                        'product_id' => $data_shop->id,
                        'image' => '/uploads/images/product/' . $data_shop->id . '/' . $fileName1
                    ]);
                    $data_image->save();
                }
            }
            $attribute = json_decode($request->attr);
            if ($request->check_attr === '1') {
                foreach ($attribute as $value) {
                    if (!empty($value->id)) {
                        $product_attribute = ProductAttributeModel::find($value->id);
                        $product_attribute->name = $value->name;
                        $product_attribute->save();
                        foreach ($value->item as $item) {
                            if (!empty($item->id)) {
                                $attribute_value = AttributeValueModel::find($item->id);
                                $attribute_value->value = $item->name;
                                $attribute_value->save();
                            } else {
                                $attribute_value = new AttributeValueModel();
                                $attribute_value->value = $item->name;
                                $attribute_value->attribute_id = $product_attribute->id;
                                $attribute_value->save();
                            }
                        }
                    } else {
                        $product_attribute = new ProductAttributeModel();
                        $product_attribute->name = $value->name;
                        $product_attribute->product_id = $data_shop->id;
                        $product_attribute->save();
                        foreach ($value->item as $item) {
                            $attribute_value = new AttributeValueModel();
                            $attribute_value->value = $item->name;
                            $attribute_value->attribute_id = $product_attribute->id;
                            $attribute_value->save();
                        }
                    }
                }
            } elseif ($request->check_attr === '0') {
                $attr = ProductAttributeModel::where('product_id', $data_shop->id)->get();
                foreach ($attr as $value) {
                    AttributeValueModel::where('attribute_id', $value['id'])->delete();
                    ProductAttributeModel::destroy($value['id']);
                }
            }
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/product'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->getMessageBag();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    public function delete($id)
    {
        try {
            ProductModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete_image($id)
    {
        try {
            ProductImageModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    public function formAttr(Request $request)
    {
        $html_per = view('admin.product.form_attribute');
        $data_return = [
            'html' => strval($html_per)
        ];
        return response()->json($data_return, Response::HTTP_OK);
    }

    public function formItem()
    {
        $html_per = view('admin.product.form_item');
        $data_return = [
            'html' => strval($html_per)
        ];
        return response()->json($data_return, Response::HTTP_OK);
    }

    public function delete_item($id)
    {
        try {
            AttributeValueModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete_attr($id)
    {
        try {
            ProductAttributeModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    //search
    public function search(Request $request)
    {
        if ($request->ajax()) {
            $output = '';
            $query = $request->get('query');
            if ($query != '') {
                $data = ProductModel::where('name', 'like', '%' . $query . '%')
                    ->orderBy('id', 'desc')
                    ->get();
                if (count($data)) {
                    foreach ($data as $key => $value) {
                        $data[$key]['dropship'] = ($value['dropship'] == 1) ? 'Hàng Dropship' : 'Hàng Có Sẵn';
                        $data_shop = ShopModel::find($value['shop_id']);
                        $data[$key]['shop_name'] = isset($data_shop) ? $data_shop['name'] : 'Chưa đặt tên';
                        $data_category = CategoryProductChildModel::find($value['category_id']);
                        $data[$key]['category'] = $data_category['name'] ?? '';
                        if ($value['is_status'] == 0) {
                            $data[$key]['status'] = 'Sản phẩm còn hàng';
                        } elseif ($value['is_status'] == 1) {
                            $data[$key]['status'] = 'Sản phẩm hết hàng';
                        } elseif ($value['is_status'] == 2) {
                            $data[$key]['status'] = 'Sản phẩm bị đánh vi phạm';
                        } else {
                            $data[$key]['status'] = 'Sản phẩm bị ẩn';
                        }
                    }
                }
            } else {
                $data = ProductModel::orderBy('id', 'desc')->get();
                if (count($data)) {
                    foreach ($data as $key => $value) {
                        $data[$key]['dropship'] = ($value['dropship'] == 1) ? 'Hàng Dropship' : 'Hàng Có Sẵn';
                        $data_shop = ShopModel::find($value['shop_id']);
                        $data[$key]['shop_name'] = isset($data_shop) ? $data_shop['name'] : 'Chưa đặt tên';
                        $data_category = CategoryProductChildModel::find($value['category_id']);
                        $data[$key]['category'] = $data_category['name'] ?? '';
                        if ($value['is_status'] == 0) {
                            $data[$key]['status'] = 'Sản phẩm còn hàng';
                        } elseif ($value['is_status'] == 1) {
                            $data[$key]['status'] = 'Sản phẩm hết hàng';
                        } elseif ($value['is_status'] == 2) {
                            $data[$key]['status'] = 'Sản phẩm bị đánh vi phạm';
                        } else {
                            $data[$key]['status'] = 'Sản phẩm bị ẩn';
                        }
                    }
                }
            }
            $total_row = $data->count();
            if ($total_row > 0) {
                foreach ($data as $k => $row) {
                    $data_url_edit = URL::to('admin/product/edit/' . $row->id);
                    $data_url_delete = URL::to('admin/product/delete/' . $row->id);
                    $output .= '
                                <tr>
                                    <td>' . $row->name . '</td>
                                    <td><img width="40px" height="40px" class="image-preview" src="' . $row->image . '"
                                             alt=""></td>
                                    <td>' . $row->category . '</td>
                                    <td>' . $row->shop_name . '</td>
                                    <td>' . $row->dropship . '</td>
                                    <td>' . $row->warehouse . '</td>
                                    <td>' . $row->is_sell . '</td>
                                    <td>' . $row->status . '</td>
                                    <td>
                                         <a href="' . $data_url_edit . '">
                                            <button class="btn btn-warning" title="Sửa"><i class="fa fa-pencil-square-o"
                                                                                           aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <button
                                            class="btn btn-delete btn-danger" title="Xoá"
                                            data-url="' . $data_url_delete . '"
                                        >
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                                ';
                }
            } else {
                $output = '
                           <tr>
                            <td colspan="5">Không có dữ liệu</td>
                           </tr>
                          ';
            }
            $data = array(
                'table_data' => $output,
                'total_data' => $total_row
            );
            echo json_encode($data);
        }
    }

    public function filter($status)
    {
        $output = '';
        if ($status == 'all') {
            $data = ProductModel::orderBy('id', 'desc')->get();
            if (count($data)) {
                foreach ($data as $key => $value) {
                    $data[$key]['dropship'] = ($value['dropship'] == 1) ? 'Hàng Dropship' : 'Hàng Có Sẵn';
                    $data_shop = ShopModel::find($value['shop_id']);
                    $data[$key]['shop_name'] = isset($data_shop) ? $data_shop['name'] : 'Chưa đặt tên';
                    $data_category = CategoryProductChildModel::find($value['category_id']);
                    $data[$key]['category'] = $data_category['name'] ?? '';
                    if ($value['is_status'] == 0) {
                        $data[$key]['status'] = 'Sản phẩm còn hàng';
                    } elseif ($value['is_status'] == 1) {
                        $data[$key]['status'] = 'Sản phẩm hết hàng';
                    } elseif ($value['is_status'] == 2) {
                        $data[$key]['status'] = 'Sản phẩm bị đánh vi phạm';
                    } else {
                        $data[$key]['status'] = 'Sản phẩm bị ẩn';
                    }
                }
            }
        } else {
            $data = ProductModel::where('is_status', $status)->orderBy('id', 'desc')->get();
            if (count($data)) {
                foreach ($data as $key => $value) {
                    $data[$key]['dropship'] = ($value['dropship'] == 1) ? 'Hàng Dropship' : 'Hàng Có Sẵn';
                    $data_shop = ShopModel::find($value['shop_id']);
                    $data[$key]['shop_name'] = isset($data_shop) ? $data_shop['name'] : 'Chưa đặt tên';
                    $data_category = CategoryProductChildModel::find($value['category_id']);
                    $data[$key]['category'] = $data_category['name'] ?? '';
                    if ($value['is_status'] == 0) {
                        $data[$key]['status'] = 'Sản phẩm còn hàng';
                    } elseif ($value['is_status'] == 1) {
                        $data[$key]['status'] = 'Sản phẩm hết hàng';
                    } elseif ($value['is_status'] == 2) {
                        $data[$key]['status'] = 'Sản phẩm bị đánh vi phạm';
                    } else {
                        $data[$key]['status'] = 'Sản phẩm bị ẩn';
                    }
                }
            }
        }
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $data_url_edit = URL::to('admin/product/edit/' . $row->id);
                $data_url_delete = URL::to('admin/product/delete/' . $row->id);
                $output .= '
                                <tr>
                                    <td>' . $row->name . '</td>
                                    <td><img width="40px" height="40px" class="image-preview" src="' . $row->image . '"
                                             alt=""></td>
                                    <td>' . $row->category . '</td>
                                    <td>' . $row->shop_name . '</td>
                                    <td>' . $row->dropship . '</td>
                                    <td>' . $row->warehouse . '</td>
                                    <td>' . $row->is_sell . '</td>
                                    <td>' . $row->status . '</td>
                                    <td>
                                         <a href="' . $data_url_edit . '">
                                            <button class="btn btn-warning" title="Sửa"><i class="fa fa-pencil-square-o"
                                                                                           aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <button
                                            class="btn btn-delete btn-danger" title="Xoá"
                                            data-url="' . $data_url_delete . '"
                                        >
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td colspan="5">Không có dữ liệu</td>
                        </tr>
            ';
        }
        echo $output;
    }
}
