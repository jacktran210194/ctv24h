<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ShopModel;
use App\Models\SupplierModel;
use Illuminate\Http\Request;

class ShopConfirmController extends Controller
{
    public function index()
    {
        $data_shop = ShopModel::where('active', 0)->orWhere('active', 1)->orWhere('active', 2)->orderBy('date', 'desc')->get();
        foreach ($data_shop as $key => $value) {
            $data_supplier = SupplierModel::find($value->supplier_id);
            if($data_supplier){
                $data_shop[$key]['name_supplier'] = $data_supplier->name ?? '';
            }
            $data_shop[$key]['date'] = date('d-m-Y', strtotime($value->date));
            if ($value['active'] == 0) {
                $data_shop[$key]['is_active'] = 'Chờ xét duyệt';
            } elseif ($value['active'] == 1) {
                $data_shop[$key]['is_active'] = 'Đã xét duyệt';
            } else {
                $data_shop[$key]['is_active'] = 'Từ chối';
            }
        }
        $dataReturn = [
            'title' => 'Xét duyệt cửa hàng',
            'data_shop' => $data_shop
        ];
        return view('admin.shop_confirm.index', $dataReturn);
    }

    public function detail($id)
    {
        $data_shop = ShopModel::find($id);
        $data_supplier = SupplierModel::find($data_shop->supplier_id);
        $data_shop['name_supplier'] = $data_supplier->name;
        $data_shop['date_supplier'] = date('d-m-Y',strtotime($data_supplier->birthday));
        $data_shop['date'] = date('d-m-Y', strtotime($data_shop->date));
        $dataReturn = [
            'title' => 'Chi tiết cửa hàng',
            'data_shop' => $data_shop
        ];
        return view('admin.shop_confirm.detail', $dataReturn);
    }

    public function confirm(Request $request)
    {
        $data_shop = ShopModel::find($request->id);
        $data_shop->active = 1;
        $data_shop->save();
        $dataReturn = [
            'status' => true,
            'url' => url('admin/shop_confirm')
        ];
        return $dataReturn;
    }

    public function cancel(Request $request)
    {
        $data_shop = ShopModel::find($request->id);
        $data_shop->active = 2;
        $data_shop->save();
        $dataReturn = [
            'status' => true,
            'url' => url('admin/shop_confirm')
        ];
        return $dataReturn;
    }
}
