<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FeeCTV24HModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class FeeCTV24HController extends Controller
{
    /**
     *  Xem phí sàn
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){
        $data = FeeCTV24HModel::first();
        $dataReturn = [
            'title' => 'Thiết lập phí sàn',
            'data' => $data
        ];
        return view('admin.fee_ctv24h.form', $dataReturn);
    }

    /**
     *  Lưu phí sàn
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request){
        $rules = [
            'fee' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $data = FeeCTV24HModel::find($request->id);
            $data->fee = $request->fee;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => url('admin/fee_ctv24h'),
            ];
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }
}
