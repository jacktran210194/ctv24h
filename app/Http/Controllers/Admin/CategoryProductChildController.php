<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryProductChildModel;
use App\Models\CategoryProductModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CategoryProductChildController extends Controller
{
    public function index()
    {
        $data = CategoryProductChildModel::orderBy('sort', 'desc')->get();
        foreach($data as $key => $value){
            $category = CategoryProductModel::find($value['category_id']);
            $data[$key]['category_name'] = isset($category) ? $category['name'] : '';
        }
        $data_return = [
            'title' => 'Quản lý danh mục sản phẩm con',
            'data' => $data
        ];
        return view('admin.category_product_child.index', $data_return);
    }

    public function add(){
        $category = CategoryProductModel::orderBy('id','desc')->get();
        $data_return = [
            'category' => $category,
            'title' => 'Thêm danh mục sản phẩm con',
        ];
        return view('admin.category_product_child.form',$data_return);
    }

    public function save(Request $request)
    {
        if ($request->id) {
            $dataReturn = $this->update($request);
        } else {
            $dataReturn = $this->create($request);
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function create($request){
        $rules = [
            'name' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $sort = $request->sort;
            $data_old = CategoryProductChildModel::where('sort', $sort)->first();
            if (isset($data_old)){
                $d = CategoryProductChildModel::where('sort', '>=', $data_old->sort)->get();
                foreach ($d as $k => $v){
                    $d[$k]['sort'] += 1;
                    $d[$k]->save();
                }
            }
            $data_category = CategoryProductChildModel::create([
                'name' => $request->name,
                'category_id' => $request->category_id,
                'sort' => $sort,
                'image' => ''
            ]);
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/category_product_child/';
            $file->move($path, $fileName);
            $data_category->image = '/uploads/images/category_product_child/' . $fileName;
            $data_category->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/category_product_child'),
            ];
        }
        return $dataReturn;
    }

    public function edit(Request $request){
        $data_category = CategoryProductChildModel::find($request->id);
        $category = CategoryProductModel::orderBy('id','desc')->get();
        $data_return = [
            'title' => 'Cập nhật danh mục sản phẩm',
            'data' => $data_category,
            'category' => $category
        ];
        return view('admin.category_product_child.form',$data_return);
    }

    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    public function update($request){
        $rules = [
            'name' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data_category = CategoryProductChildModel::find($request->id);
        if ($data_category) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_category->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/category_product_child/';
                $file->move($path, $fileName);
                $data_category->image = '/uploads/images/category_product_child/' . $fileName;
                $data_category->save();
            }
            $sort = $request->sort;
            $data_old = CategoryProductChildModel::where('sort', $sort)->first();
            if (isset($data_old)) {
                $d = CategoryProductChildModel::where('sort', '>=',$sort)->where('sort', '<', $data_category->sort)->get();
                foreach ($d as $k => $v){
                    $d[$k]['sort'] += 1;
                    $d[$k]->save();
                }
            }
            $data_category->name = $request->name;
            $data_category->category_id = $request->category_id;
            $data_category->sort = $sort;
            $data_category->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/category_product_child'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function delete($id){
        try {
            CategoryProductChildModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
