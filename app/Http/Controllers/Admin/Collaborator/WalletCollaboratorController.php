<?php

namespace App\Http\Controllers\Admin\Collaborator;

use App\Exports\Collaborator\HistoryPaymentExport;
use App\Http\Controllers\Controller;
use App\Models\BankAccountModel;
use App\Models\CustomerModel;
use App\Models\TheBankModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use session;
use DB;
class WalletCollaboratorController extends Controller
{
    /**
     *  Ví CTV
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $dataUser = session()->get('data_collaborator');
        $dataWallet = CustomerModel::where('id', $dataUser->customer_id)->first();
        $data = BankAccountModel::where('customer_id', '=', $dataUser->customer_id)->orderBy('id', 'desc')->get();
        foreach ($data as $k => $v) {
            $dataBank = TheBankModel::where('id', $v['bank_name'])->first();
            $data[$k]['bank_name'] = $dataBank['name'] ?? '';
            $data[$k]['short_name'] = $dataBank['short_name'] ?? '';
        }
        $data_return = [
            'collaborator' => true,
            'data' => $data,
            'dataWallet' => $dataWallet,
            'title' => 'Tài khoản ngân hàng',
        ];
        return view('collaborator.wallet.index', $data_return);
    }

    /**
     *  Thêm tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addBank(Request $request)
    {
        $dataCustomer = session()->get('data_collaborator');
        $dataCollaborator = CustomerModel::find($dataCustomer->customer_id);
        $dataBank = TheBankModel::orderBy('id', 'desc')->get();
        $data_return = [
            'title' => 'Thêm tài khoản ngân hàng',
            'dataBank' => $dataBank,
            '$dataCollaborator' => $dataCollaborator
        ];
        return view('collaborator.wallet.form_bank', $data_return);
    }

    /**
     *  Thêm tài khoản ngân hàng
     * @param Request $request
     * @return array
     */
    public function createBank(Request $request)
    {
        $rules = [
            'user_name' => 'required',
            'bank_name' => 'required',
            'bank_account' => 'required|unique:bank_account,bank_account,' . $request->id,
            'date_active' => 'required',
            'identify_card' => 'required',
            'branch' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'bank_account.unique' => 'Số tài khoản đã tồn tại'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $dataUser = session()->get('data_collaborator');
            $data = new BankAccountModel([
                'customer_id' => $dataUser->id,
                'user_name' => $request->user_name,
                'bank_name' => $request->bank_name,
                'bank_account' => $request->bank_account,
                'date_active' => $request->date_active,
                'identify_card' => $request->identify_card,
                'branch' => $request->branch
            ]);
            $data->save();
            $dataReturn = [
                'status' => true,
                'msg' => 'Thêm tài khoản ngân hàng thành công',
                'url' => url('admin/wallet'),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Xóa tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteBank(Request $request)
    {
        try {
            BankAccountModel::destroy($request->id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     *  Liên kết tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function linkBank(Request $request){
        $data = BankAccountModel::where('id', $request->id)->update(['is_active' => 1]);
        $dataReturn = [
            'status' => true,
            'msg' => 'Liên kết thành công',
            'data' => $data
        ];
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     *  Hủy liên kết tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unlinkBank(Request $request){
        $data = BankAccountModel::where('id', $request->id)->update(['is_active' => 0]);
        $dataReturn = [
            'status' => true,
            'msg' => 'Hủy liên kết thành công',
            'data' => $data
        ];
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     *  Lịch sử giao dịch
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function payment(Request $request){
        $dataUser = session()->get('data_collaborator');
        $data_withdrwal = DB::table('payment')
            ->join('bank_account', 'payment.account', '=', 'bank_account.id')
            ->select('bank_account.*', 'payment.created_at as created_at', 'payment.*')
            ->join('customer', 'bank_account.customer_id', '=','customer.id')
            ->where('bank_account.customer_id', $dataUser->customer_id)
            ->where('type', 0)
            ->orderBy('payment.id', 'desc')
            ->get();
        $data_return = [
            'title' => 'Lịch sử giao dịch',
            'data_withdrawl' => $data_withdrwal
        ];
        return view('collaborator.wallet.history_payment', $data_return);
    }

    public function export()
    {
        return Excel::download(new HistoryPaymentExport(), 'History_Payment.xlsx');
    }
}
