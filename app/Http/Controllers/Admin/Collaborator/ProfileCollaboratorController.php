<?php

namespace App\Http\Controllers\Admin\Collaborator;

use App\Http\Controllers\Controller;
use App\Models\CollaboratorModel;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ProfileCollaboratorController extends Controller
{
    public function index(){
        $admin = session()->get('data_user');
        $data = CollaboratorModel::find($admin['id']);
        $data_return = [
            'title' => 'Quản lý thông tin cá nhân',
            'data' => $data
        ];
        return view('collaborator.profile.index',$data_return);
    }

    public function update(Request $request){
        $admin = session()->get('data_user');
        $rules = [
            'name1' => 'required',
            'email1' => 'email|required|unique:collaborator,email,'.$request->id,
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'unique' => 'Email đã tồn tại !',
            'email' => 'Không đúng định dạng email !'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data = CollaboratorModel::find($admin['id']);
        if ($data) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/collaborator/';
                $file->move($path, $fileName);
                $data->image = '/uploads/images/collaborator/' . $fileName;
                $data->save();
            }
            $data->name = $request->name1;
            $data->email = $request->email1;
            $data->save();
            $dataReturn = [
                'status' => true,
                'msg' => 'Thay đổi thông tin thành công !',
                'url' => URL::to('admin/profile'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }
}
