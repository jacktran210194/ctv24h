<?php
namespace App\Http\Controllers\Admin\Collaborator;
use App\Http\Controllers\Controller;

class DataSaleController extends Controller{
    public function dataSale(){
        return view('collaborator.data_sale.index');
    }
    public function accessTimes(){
        return view('collaborator.data_sale.access_times');
    }
    public function products(){
        return view('collaborator.data_sale.products');
    }
    public function productsOverview(){
        return view('collaborator.data_sale.productsOverview');
    }
    public function needImprove(){
        return view('collaborator.data_sale.needImprove');
    }
    public function revenue(){
        return view('collaborator.data_sale.revenue');
    }
    public function marketing(){
        return view('collaborator.data_sale.marketing.index');
    }
    public function comboSale(){
        return view('collaborator.data_sale.marketing.comboSale');
    }
    public function followerOffers(){
        return view('collaborator.data_sale.marketing.followerOffers');
    }
    public function voucher(){
        return view('collaborator.data_sale.marketing.voucher');
    }
    public function flashSale(){
        return view('collaborator.data_sale.marketing.flashSale');
    }
    public function dealShock(){
        return view('collaborator.data_sale.marketing.dealShock');
    }
    public function chat(){
        return view('collaborator.data_sale.chat');
    }
    public function CTVLiveFeed(){
        return view('collaborator.data_sale.CTVLiveFeed');
    }
    public function saleTactician(){
        return view('collaborator.data_sale.saleTactician.index');
    }
    public function competitiveProduct(){
        return view('collaborator.data_sale.saleTactician.competitiveProduct');
    }
    public function topSearch(){
        return view('collaborator.data_sale.saleTactician.topSearch');
    }
    public function optimizeProduct(){
        return view('collaborator.data_sale.saleTactician.optimizeProduct');
    }
}
