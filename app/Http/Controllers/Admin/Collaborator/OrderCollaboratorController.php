<?php

namespace App\Http\Controllers\Admin\Collaborator;

use App\Exports\Collaborator\OrderCollaboratorExport;
use App\Exports\Supplier\OrderSupplierExport;
use App\Http\Controllers\Controller;
use App\Models\AddressCustomerCTVModel;
use App\Models\AttributeValueModel;
use App\Models\CityModel;
use App\Models\CustomerAddressModel;
use App\Models\CustomerModel;
use App\Models\DistrictModel;
use App\Models\FeeCTV24HModel;
use App\Models\OrderCTVModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductModel;
use App\Models\ShippingModel;
use App\Models\ShopModel;
use App\Models\SupplierModel;
use App\Models\WardModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Facades\Excel;

class OrderCollaboratorController extends Controller
{

    public function __construct(Request $request)
    {
        if (!$request->session()->has('data_collaborator')) {
            Redirect::to('login')->send();
        }
    }

    /**
     *  Danh sách đơn hàng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $dataReturn = [
            'status' => true,
            'title' => 'Đơn hàng',
        ];
        return view('collaborator.order.index', $dataReturn);
    }

    //filter status
    public function all()
    {
        $data_login = session()->get('data_collaborator');
        $data_customer = session()->get('data_customer');
        $data_shop_ctv = ShopModel::where('customer_id',$data_customer->id)->first();
        $data_order_ctv = OrderCTVModel::where('shop_id',$data_shop_ctv->id)->get();
        if($data_order_ctv){
            foreach($data_order_ctv as $key => $value){
                $order_parent = OrderModel::find($value->order_id);
//                $data_order_ctv[$key]['total_price'] = $order_parent->total_price;
                $data_order_ctv[$key]['order_code'] = $order_parent->order_code;
                $data_shop_1 = ShopModel::find($order_parent['shop_id']);
                $data_order_ctv[$key]['shop_name'] = $data_shop_1->name;
                $data_order_ctv[$key]['shop_image'] = $data_shop_1->image;
                $data_supplier = SupplierModel::find($data_shop_1->supplier_id);
                $data_order_ctv[$key]['supplier_name'] = $data_supplier->name;
                if ($order_parent['status'] == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($order_parent['status'] == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ lấy hàng';
                } elseif ($order_parent['status'] == 2) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($order_parent['status'] == 3) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao hàng';
                }
                if ($order_parent->status == 4 && $order_parent->cancel_confirm == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                } elseif ($order_parent->status == 4 && $order_parent->cancel_confirm == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Đã huỷ';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                }
                $data_shipping = ShippingModel::find($order_parent['shipping_id']);
                $data_order_ctv[$key]['shipping'] = $data_shipping['name'];

                if ($order_parent['payment_id'] == 0) {
                    $data_order_ctv[$key]['payment'] = 'Ví CTV24h';
                } elseif ($order_parent['payment_id'] == 1) {
                    $data_order_ctv[$key]['payment'] = 'Trả góp';
                } elseif ($order_parent['payment_id'] == 2) {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } elseif ($order_parent['payment_id'] == 3) {
                    $data_order_ctv[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
                } elseif ($order_parent['payment_id'] == 4) {
                    $data_order_ctv[$key]['payment'] = 'CTV24H PAY';
                } else {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
                }
            }
        }


        $data_order = OrderModel::where('customer_id', $data_login->customer_id)->where('collaborator_id', 1)->where('is_selected', 2)->get();
        $total_order = (count($data_order)??0) + (count($data_order_ctv)??0);
        foreach ($data_order as $key => $value) {
            $data_shop = ShopModel::find($value['shop_id']);
            $data_order[$key]['shop_name'] = $data_shop->name;
            $data_order[$key]['shop_image'] = $data_shop->image;
            $data_supplier = SupplierModel::find($data_shop->supplier_id);
            $data_order[$key]['supplier_name'] = $data_supplier->name;
            if ($value['status'] == 0) {
                $data_order[$key]['is_status'] = 'Chờ xác nhận';
            } elseif ($value['status'] == 1) {
                $data_order[$key]['is_status'] = 'Chờ lấy hàng';
            } elseif ($value['status'] == 2) {
                $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
            } elseif ($value['status'] == 3) {
                $data_order[$key]['is_status'] = 'Đã giao hàng';
            }
            if ($value->status == 4 && $value->cancel_confirm == 0) {
                $data_order[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            } elseif ($value->status == 4 && $value->cancel_confirm == 1) {
                $data_order[$key]['is_status'] = 'Đã huỷ';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            }
            $data_shipping = ShippingModel::find($value['shipping_id']);
            $data_order[$key]['shipping'] = $data_shipping['name'];

            if ($value['payment_id'] == 0) {
                $data_order[$key]['payment'] = 'Ví CTV24h';
            } elseif ($value['payment_id'] == 1) {
                $data_order[$key]['payment'] = 'Trả góp';
            } elseif ($value['payment_id'] == 2) {
                $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
            } elseif ($value['payment_id'] == 3) {
                $data_order[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
            } elseif ($value['payment_id'] == 4) {
                $data_order[$key]['payment'] = 'CTV24H PAY';
            } else {
                $data_order[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
            }
        }
        $data_order_detail = OrderDetailModel::all();
        if (count($data_order_detail)) {
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v['product_id']);
                $data_order_detail[$k]['product_name'] = $data_product['name'];
                $data_order_detail[$k]['product_image'] = $data_product['image'];
                $dataValue_Attribute = AttributeValueModel::find($v->type_product);
                $data_order_detail[$k]->value_attr = $dataValue_Attribute->value ?? '';
                $data_order_detail[$k]->size_of_value = $dataValue_Attribute->size_of_value ?? '';
            }
        }
        $dataReturn = [
            'status' => true,
            'title' => 'Đơn hàng',
            'data_order' => $data_order,
            'data_order_detail' => $data_order_detail,
            'total_order' => $total_order,
            'data_order_ctv' => $data_order_ctv ?? null,
        ];
        return view('collaborator.order.all', $dataReturn);
    }

    public function wait_confirm()
    {
        $data_login = session()->get('data_collaborator');
        $data_customer = session()->get('data_customer');
        $data_shop_ctv = ShopModel::where('customer_id',$data_customer->id)->first();
        $data_order_ctv = OrderCTVModel::where('shop_id',$data_shop_ctv->id)->get();
        if($data_order_ctv){
            foreach($data_order_ctv as $key => $value){
                $order_parent = OrderModel::find($value->order_id);
                $data_order_ctv[$key]['total_price'] = $order_parent->total_price;
                $data_order_ctv[$key]['order_code'] = $order_parent->order_code;
                $data_shop_1 = ShopModel::find($order_parent['shop_id']);
                $data_order_ctv[$key]['shop_name'] = $data_shop_1->name;
                $data_order_ctv[$key]['shop_image'] = $data_shop_1->image;
                $data_supplier = SupplierModel::find($data_shop_1->supplier_id);
                $data_order_ctv[$key]['supplier_name'] = $data_supplier->name;
                if ($order_parent['status'] == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($order_parent['status'] == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ lấy hàng';
                } elseif ($order_parent['status'] == 2) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($order_parent['status'] == 3) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao hàng';
                }
                if ($order_parent->status == 4 && $order_parent->cancel_confirm == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                } elseif ($order_parent->status == 4 && $order_parent->cancel_confirm == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Đã huỷ';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                }
                $data_shipping = ShippingModel::find($order_parent['shipping_id']);
                $data_order_ctv[$key]['shipping'] = $data_shipping['name'];

                if ($order_parent['payment_id'] == 0) {
                    $data_order_ctv[$key]['payment'] = 'Ví CTV24h';
                } elseif ($order_parent['payment_id'] == 1) {
                    $data_order_ctv[$key]['payment'] = 'Trả góp';
                } elseif ($order_parent['payment_id'] == 2) {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } elseif ($order_parent['payment_id'] == 3) {
                    $data_order_ctv[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
                } elseif ($order_parent['payment_id'] == 4) {
                    $data_order_ctv[$key]['payment'] = 'CTV24H PAY';
                } else {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
                }
            }
        }


        $data_order = OrderModel::where('customer_id', $data_login->customer_id)->where(['collaborator_id' => 1, 'is_selected' => 2,'status' => 0])->get();
        $total_order = (count($data_order)??0) + (count($data_order_ctv)??0);
        foreach ($data_order as $key => $value) {
            $data_shop = ShopModel::find($value['shop_id']);
            $data_order[$key]['shop_name'] = $data_shop->name;
            $data_order[$key]['shop_image'] = $data_shop->image;
            $data_supplier = SupplierModel::find($data_shop->supplier_id);
            $data_order[$key]['supplier_name'] = $data_supplier->name;
            if ($value['status'] == 0) {
                $data_order[$key]['is_status'] = 'Chờ xác nhận';
            } elseif ($value['status'] == 1) {
                $data_order[$key]['is_status'] = 'Chờ lấy hàng';
            } elseif ($value['status'] == 2) {
                $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
            } elseif ($value['status'] == 3) {
                $data_order[$key]['is_status'] = 'Đã giao hàng';
            }
            if ($value->status == 4 && $value->cancel_confirm == 0) {
                $data_order[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            } elseif ($value->status == 4 && $value->cancel_confirm == 1) {
                $data_order[$key]['is_status'] = 'Đã huỷ';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            }
            $data_shipping = ShippingModel::find($value['shipping_id']);
            $data_order[$key]['shipping'] = $data_shipping['name'];

            if ($value['payment_id'] == 0) {
                $data_order[$key]['payment'] = 'Ví CTV24h';
            } elseif ($value['payment_id'] == 1) {
                $data_order[$key]['payment'] = 'Trả góp';
            } elseif ($value['payment_id'] == 2) {
                $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
            } elseif ($value['payment_id'] == 3) {
                $data_order[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
            } elseif ($value['payment_id'] == 4) {
                $data_order[$key]['payment'] = 'CTV24H PAY';
            } else {
                $data_order[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
            }
        }
        $data_order_detail = OrderDetailModel::all();
        if (count($data_order_detail)) {
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v['product_id']);
                $data_order_detail[$k]['product_name'] = $data_product['name'];
                $data_order_detail[$k]['product_image'] = $data_product['image'];
                $dataValue_Attribute = AttributeValueModel::find($v->type_product);
                $data_order_detail[$k]->value_attr = $dataValue_Attribute->value ?? '';
                $data_order_detail[$k]->size_of_value = $dataValue_Attribute->size_of_value ?? '';
            }
        }
        $dataReturn = [
            'status' => true,
            'title' => 'Đơn hàng',
            'data_order' => $data_order,
            'data_order_detail' => $data_order_detail,
            'total_order' => $total_order,
            'data_order_ctv' => $data_order_ctv ?? null,
        ];
        return view('collaborator.order.wait_confirm', $dataReturn);
    }

    public function wait_shipping()
    {
        $data_login = session()->get('data_collaborator');
        $data_customer = session()->get('data_customer');
        $data_shop_ctv = ShopModel::where('customer_id',$data_customer->id)->first();
        $data_order_ctv = OrderCTVModel::where('shop_id',$data_shop_ctv->id)->get();
        if($data_order_ctv){
            foreach($data_order_ctv as $key => $value){
                $order_parent = OrderModel::find($value->order_id);
                $data_order_ctv[$key]['total_price'] = $order_parent->total_price;
                $data_order_ctv[$key]['order_code'] = $order_parent->order_code;
                $data_shop_1 = ShopModel::find($order_parent['shop_id']);
                $data_order_ctv[$key]['shop_name'] = $data_shop_1->name;
                $data_order_ctv[$key]['shop_image'] = $data_shop_1->image;
                $data_supplier = SupplierModel::find($data_shop_1->supplier_id);
                $data_order_ctv[$key]['supplier_name'] = $data_supplier->name;
                if ($order_parent['status'] == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($order_parent['status'] == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ lấy hàng';
                } elseif ($order_parent['status'] == 2) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($order_parent['status'] == 3) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao hàng';
                }
                if ($order_parent->status == 4 && $order_parent->cancel_confirm == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                } elseif ($order_parent->status == 4 && $order_parent->cancel_confirm == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Đã huỷ';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                }
                $data_shipping = ShippingModel::find($order_parent['shipping_id']);
                $data_order_ctv[$key]['shipping'] = $data_shipping['name'];

                if ($order_parent['payment_id'] == 0) {
                    $data_order_ctv[$key]['payment'] = 'Ví CTV24h';
                } elseif ($order_parent['payment_id'] == 1) {
                    $data_order_ctv[$key]['payment'] = 'Trả góp';
                } elseif ($order_parent['payment_id'] == 2) {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } elseif ($order_parent['payment_id'] == 3) {
                    $data_order_ctv[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
                } elseif ($order_parent['payment_id'] == 4) {
                    $data_order_ctv[$key]['payment'] = 'CTV24H PAY';
                } else {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
                }
            }
        }


        $data_order = OrderModel::where('customer_id', $data_login->customer_id)->where(['collaborator_id' => 1, 'is_selected' => 2,'status' => 1])->get();
        $total_order = (count($data_order)??0) + (count($data_order_ctv)??0);
        foreach ($data_order as $key => $value) {
            $data_shop = ShopModel::find($value['shop_id']);
            $data_order[$key]['shop_name'] = $data_shop->name;
            $data_order[$key]['shop_image'] = $data_shop->image;
            $data_supplier = SupplierModel::find($data_shop->supplier_id);
            $data_order[$key]['supplier_name'] = $data_supplier->name;
            if ($value['status'] == 0) {
                $data_order[$key]['is_status'] = 'Chờ xác nhận';
            } elseif ($value['status'] == 1) {
                $data_order[$key]['is_status'] = 'Chờ lấy hàng';
            } elseif ($value['status'] == 2) {
                $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
            } elseif ($value['status'] == 3) {
                $data_order[$key]['is_status'] = 'Đã giao hàng';
            }
            if ($value->status == 4 && $value->cancel_confirm == 0) {
                $data_order[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            } elseif ($value->status == 4 && $value->cancel_confirm == 1) {
                $data_order[$key]['is_status'] = 'Đã huỷ';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            }
            $data_shipping = ShippingModel::find($value['shipping_id']);
            $data_order[$key]['shipping'] = $data_shipping['name'];

            if ($value['payment_id'] == 0) {
                $data_order[$key]['payment'] = 'Ví CTV24h';
            } elseif ($value['payment_id'] == 1) {
                $data_order[$key]['payment'] = 'Trả góp';
            } elseif ($value['payment_id'] == 2) {
                $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
            } elseif ($value['payment_id'] == 3) {
                $data_order[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
            } elseif ($value['payment_id'] == 4) {
                $data_order[$key]['payment'] = 'CTV24H PAY';
            } else {
                $data_order[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
            }
        }
        $data_order_detail = OrderDetailModel::all();
        if (count($data_order_detail)) {
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v['product_id']);
                $data_order_detail[$k]['product_name'] = $data_product['name'];
                $data_order_detail[$k]['product_image'] = $data_product['image'];
                $dataValue_Attribute = AttributeValueModel::find($v->type_product);
                $data_order_detail[$k]->value_attr = $dataValue_Attribute->value ?? '';
                $data_order_detail[$k]->size_of_value = $dataValue_Attribute->size_of_value ?? '';
            }
        }
        $dataReturn = [
            'status' => true,
            'title' => 'Đơn hàng',
            'data_order' => $data_order,
            'data_order_detail' => $data_order_detail,
            'total_order' => $total_order,
            'data_order_ctv' => $data_order_ctv ?? null,
        ];
        return view('collaborator.order.wait_shipping', $dataReturn);
    }

    public function shipping()
    {
        $data_login = session()->get('data_collaborator');
        $data_customer = session()->get('data_customer');
        $data_shop_ctv = ShopModel::where('customer_id',$data_customer->id)->first();
        $data_order_ctv = OrderCTVModel::where('shop_id',$data_shop_ctv->id)->get();
        if($data_order_ctv){
            foreach($data_order_ctv as $key => $value){
                $order_parent = OrderModel::find($value->order_id);
                $data_order_ctv[$key]['total_price'] = $order_parent->total_price;
                $data_order_ctv[$key]['order_code'] = $order_parent->order_code;
                $data_shop_1 = ShopModel::find($order_parent['shop_id']);
                $data_order_ctv[$key]['shop_name'] = $data_shop_1->name;
                $data_order_ctv[$key]['shop_image'] = $data_shop_1->image;
                $data_supplier = SupplierModel::find($data_shop_1->supplier_id);
                $data_order_ctv[$key]['supplier_name'] = $data_supplier->name;
                if ($order_parent['status'] == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($order_parent['status'] == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ lấy hàng';
                } elseif ($order_parent['status'] == 2) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($order_parent['status'] == 3) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao hàng';
                }
                if ($order_parent->status == 4 && $order_parent->cancel_confirm == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                } elseif ($order_parent->status == 4 && $order_parent->cancel_confirm == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Đã huỷ';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                }
                $data_shipping = ShippingModel::find($order_parent['shipping_id']);
                $data_order_ctv[$key]['shipping'] = $data_shipping['name'];

                if ($order_parent['payment_id'] == 0) {
                    $data_order_ctv[$key]['payment'] = 'Ví CTV24h';
                } elseif ($order_parent['payment_id'] == 1) {
                    $data_order_ctv[$key]['payment'] = 'Trả góp';
                } elseif ($order_parent['payment_id'] == 2) {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } elseif ($order_parent['payment_id'] == 3) {
                    $data_order_ctv[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
                } elseif ($order_parent['payment_id'] == 4) {
                    $data_order_ctv[$key]['payment'] = 'CTV24H PAY';
                } else {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
                }
            }
        }


        $data_order = OrderModel::where('customer_id', $data_login->customer_id)->where(array('collaborator_id' => 1, 'is_selected' => 2,'status' => 2))->get();
        $total_order = (count($data_order)??0) + (count($data_order_ctv)??0);
        foreach ($data_order as $key => $value) {
            $data_shop = ShopModel::find($value['shop_id']);
            $data_order[$key]['shop_name'] = $data_shop->name;
            $data_order[$key]['shop_image'] = $data_shop->image;
            $data_supplier = SupplierModel::find($data_shop->supplier_id);
            $data_order[$key]['supplier_name'] = $data_supplier->name;
            if ($value['status'] == 0) {
                $data_order[$key]['is_status'] = 'Chờ xác nhận';
            } elseif ($value['status'] == 1) {
                $data_order[$key]['is_status'] = 'Chờ lấy hàng';
            } elseif ($value['status'] == 2) {
                $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
            } elseif ($value['status'] == 3) {
                $data_order[$key]['is_status'] = 'Đã giao hàng';
            }
            if ($value->status == 4 && $value->cancel_confirm == 0) {
                $data_order[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            } elseif ($value->status == 4 && $value->cancel_confirm == 1) {
                $data_order[$key]['is_status'] = 'Đã huỷ';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            }
            $data_shipping = ShippingModel::find($value['shipping_id']);
            $data_order[$key]['shipping'] = $data_shipping['name'];

            if ($value['payment_id'] == 0) {
                $data_order[$key]['payment'] = 'Ví CTV24h';
            } elseif ($value['payment_id'] == 1) {
                $data_order[$key]['payment'] = 'Trả góp';
            } elseif ($value['payment_id'] == 2) {
                $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
            } elseif ($value['payment_id'] == 3) {
                $data_order[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
            } elseif ($value['payment_id'] == 4) {
                $data_order[$key]['payment'] = 'CTV24H PAY';
            } else {
                $data_order[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
            }
        }
        $data_order_detail = OrderDetailModel::all();
        if (count($data_order_detail)) {
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v['product_id']);
                $data_order_detail[$k]['product_name'] = $data_product['name'];
                $data_order_detail[$k]['product_image'] = $data_product['image'];
                $dataValue_Attribute = AttributeValueModel::find($v->type_product);
                $data_order_detail[$k]->value_attr = $dataValue_Attribute->value ?? '';
                $data_order_detail[$k]->size_of_value = $dataValue_Attribute->size_of_value ?? '';
            }
        }
        $dataReturn = [
            'status' => true,
            'title' => 'Đơn hàng',
            'data_order' => $data_order,
            'data_order_detail' => $data_order_detail,
            'total_order' => $total_order,
            'data_order_ctv' => $data_order_ctv ?? null,
        ];
        return view('collaborator.order.shipping', $dataReturn);
    }

    public function shipped()
    {
        $data_login = session()->get('data_collaborator');
        $data_customer = session()->get('data_customer');
        $data_shop_ctv = ShopModel::where('customer_id',$data_customer->id)->first();
        $data_order_ctv = OrderCTVModel::where('shop_id',$data_shop_ctv->id)->get();
        if($data_order_ctv){
            foreach($data_order_ctv as $key => $value){
                $order_parent = OrderModel::find($value->order_id);
                $data_order_ctv[$key]['total_price'] = $order_parent->total_price;
                $data_order_ctv[$key]['order_code'] = $order_parent->order_code;
                $data_shop_1 = ShopModel::find($order_parent['shop_id']);
                $data_order_ctv[$key]['shop_name'] = $data_shop_1->name;
                $data_order_ctv[$key]['shop_image'] = $data_shop_1->image;
                $data_supplier = SupplierModel::find($data_shop_1->supplier_id);
                $data_order_ctv[$key]['supplier_name'] = $data_supplier->name;
                if ($order_parent['status'] == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($order_parent['status'] == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ lấy hàng';
                } elseif ($order_parent['status'] == 2) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($order_parent['status'] == 3) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao hàng';
                }
                if ($order_parent->status == 4 && $order_parent->cancel_confirm == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                } elseif ($order_parent->status == 4 && $order_parent->cancel_confirm == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Đã huỷ';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                }
                $data_shipping = ShippingModel::find($order_parent['shipping_id']);
                $data_order_ctv[$key]['shipping'] = $data_shipping['name'];

                if ($order_parent['payment_id'] == 0) {
                    $data_order_ctv[$key]['payment'] = 'Ví CTV24h';
                } elseif ($order_parent['payment_id'] == 1) {
                    $data_order_ctv[$key]['payment'] = 'Trả góp';
                } elseif ($order_parent['payment_id'] == 2) {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } elseif ($order_parent['payment_id'] == 3) {
                    $data_order_ctv[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
                } elseif ($order_parent['payment_id'] == 4) {
                    $data_order_ctv[$key]['payment'] = 'CTV24H PAY';
                } else {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
                }
            }
        }


        $data_order = OrderModel::where('customer_id', $data_login->customer_id)->where(array('collaborator_id' => 1, 'is_selected' => 2,'status' => 3))->get();
        $total_order = (count($data_order)??0) + (count($data_order_ctv)??0);
        foreach ($data_order as $key => $value) {
            $data_shop = ShopModel::find($value['shop_id']);
            $data_order[$key]['shop_name'] = $data_shop->name;
            $data_order[$key]['shop_image'] = $data_shop->image;
            $data_supplier = SupplierModel::find($data_shop->supplier_id);
            $data_order[$key]['supplier_name'] = $data_supplier->name;
            if ($value['status'] == 0) {
                $data_order[$key]['is_status'] = 'Chờ xác nhận';
            } elseif ($value['status'] == 1) {
                $data_order[$key]['is_status'] = 'Chờ lấy hàng';
            } elseif ($value['status'] == 2) {
                $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
            } elseif ($value['status'] == 3) {
                $data_order[$key]['is_status'] = 'Đã giao hàng';
            }
            if ($value->status == 4 && $value->cancel_confirm == 0) {
                $data_order[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            } elseif ($value->status == 4 && $value->cancel_confirm == 1) {
                $data_order[$key]['is_status'] = 'Đã huỷ';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            }
            $data_shipping = ShippingModel::find($value['shipping_id']);
            $data_order[$key]['shipping'] = $data_shipping['name'];

            if ($value['payment_id'] == 0) {
                $data_order[$key]['payment'] = 'Ví CTV24h';
            } elseif ($value['payment_id'] == 1) {
                $data_order[$key]['payment'] = 'Trả góp';
            } elseif ($value['payment_id'] == 2) {
                $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
            } elseif ($value['payment_id'] == 3) {
                $data_order[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
            } elseif ($value['payment_id'] == 4) {
                $data_order[$key]['payment'] = 'CTV24H PAY';
            } else {
                $data_order[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
            }
        }
        $data_order_detail = OrderDetailModel::all();
        if (count($data_order_detail)) {
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v['product_id']);
                $data_order_detail[$k]['product_name'] = $data_product['name'];
                $data_order_detail[$k]['product_image'] = $data_product['image'];
                $dataValue_Attribute = AttributeValueModel::find($v->type_product);
                $data_order_detail[$k]->value_attr = $dataValue_Attribute->value ?? '';
                $data_order_detail[$k]->size_of_value = $dataValue_Attribute->size_of_value ?? '';
            }
        }
        $dataReturn = [
            'status' => true,
            'title' => 'Đơn hàng',
            'data_order' => $data_order,
            'data_order_detail' => $data_order_detail,
            'total_order' => $total_order,
            'data_order_ctv' => $data_order_ctv ?? null,
        ];
        return view('collaborator.order.shipped', $dataReturn);
    }

    public function cancel()
    {
        $data_login = session()->get('data_collaborator');
        $data_customer = session()->get('data_customer');
        $data_shop_ctv = ShopModel::where('customer_id',$data_customer->id)->first();
        $data_order_ctv = OrderCTVModel::where('shop_id',$data_shop_ctv->id)->get();
        if($data_order_ctv){
            foreach($data_order_ctv as $key => $value){
                $order_parent = OrderModel::find($value->order_id);
                $data_order_ctv[$key]['total_price'] = $order_parent->total_price;
                $data_order_ctv[$key]['order_code'] = $order_parent->order_code;
                $data_shop_1 = ShopModel::find($order_parent['shop_id']);
                $data_order_ctv[$key]['shop_name'] = $data_shop_1->name;
                $data_order_ctv[$key]['shop_image'] = $data_shop_1->image;
                $data_supplier = SupplierModel::find($data_shop_1->supplier_id);
                $data_order_ctv[$key]['supplier_name'] = $data_supplier->name;
                if ($order_parent['status'] == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($order_parent['status'] == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ lấy hàng';
                } elseif ($order_parent['status'] == 2) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($order_parent['status'] == 3) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao hàng';
                }
                if ($order_parent->status == 4 && $order_parent->cancel_confirm == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                } elseif ($order_parent->status == 4 && $order_parent->cancel_confirm == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Đã huỷ';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                }
                $data_shipping = ShippingModel::find($order_parent['shipping_id']);
                $data_order_ctv[$key]['shipping'] = $data_shipping['name'];

                if ($order_parent['payment_id'] == 0) {
                    $data_order_ctv[$key]['payment'] = 'Ví CTV24h';
                } elseif ($order_parent['payment_id'] == 1) {
                    $data_order_ctv[$key]['payment'] = 'Trả góp';
                } elseif ($order_parent['payment_id'] == 2) {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } elseif ($order_parent['payment_id'] == 3) {
                    $data_order_ctv[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
                } elseif ($order_parent['payment_id'] == 4) {
                    $data_order_ctv[$key]['payment'] = 'CTV24H PAY';
                } else {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
                }
            }
        }


        $data_order = OrderModel::where('customer_id', $data_login->customer_id)->where(array('collaborator_id' => 1, 'is_selected' => 2,'status' => 4))->get();
        $total_order = (count($data_order)??0) + (count($data_order_ctv)??0);
        foreach ($data_order as $key => $value) {
            $data_shop = ShopModel::find($value['shop_id']);
            $data_order[$key]['shop_name'] = $data_shop->name;
            $data_order[$key]['shop_image'] = $data_shop->image;
            $data_supplier = SupplierModel::find($data_shop->supplier_id);
            $data_order[$key]['supplier_name'] = $data_supplier->name;
            if ($value['status'] == 0) {
                $data_order[$key]['is_status'] = 'Chờ xác nhận';
            } elseif ($value['status'] == 1) {
                $data_order[$key]['is_status'] = 'Chờ lấy hàng';
            } elseif ($value['status'] == 2) {
                $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
            } elseif ($value['status'] == 3) {
                $data_order[$key]['is_status'] = 'Đã giao hàng';
            }
            if ($value->status == 4 && $value->cancel_confirm == 0) {
                $data_order[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            } elseif ($value->status == 4 && $value->cancel_confirm == 1) {
                $data_order[$key]['is_status'] = 'Đã huỷ';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            }
            $data_shipping = ShippingModel::find($value['shipping_id']);
            $data_order[$key]['shipping'] = $data_shipping['name'];

            if ($value['payment_id'] == 0) {
                $data_order[$key]['payment'] = 'Ví CTV24h';
            } elseif ($value['payment_id'] == 1) {
                $data_order[$key]['payment'] = 'Trả góp';
            } elseif ($value['payment_id'] == 2) {
                $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
            } elseif ($value['payment_id'] == 3) {
                $data_order[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
            } elseif ($value['payment_id'] == 4) {
                $data_order[$key]['payment'] = 'CTV24H PAY';
            } else {
                $data_order[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
            }
        }
        $data_order_detail = OrderDetailModel::all();
        if (count($data_order_detail)) {
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v['product_id']);
                $data_order_detail[$k]['product_name'] = $data_product['name'];
                $data_order_detail[$k]['product_image'] = $data_product['image'];
                $dataValue_Attribute = AttributeValueModel::find($v->type_product);
                $data_order_detail[$k]->value_attr = $dataValue_Attribute->value ?? '';
                $data_order_detail[$k]->size_of_value = $dataValue_Attribute->size_of_value ?? '';
            }
        }
        $dataReturn = [
            'status' => true,
            'title' => 'Đơn hàng',
            'data_order' => $data_order,
            'data_order_detail' => $data_order_detail,
            'total_order' => $total_order,
            'data_order_ctv' => $data_order_ctv ?? null,
        ];
        return view('collaborator.order.cancel', $dataReturn);
    }

    public function return_money()
    {
        $data_login = session()->get('data_collaborator');
        $data_customer = session()->get('data_customer');
        $data_shop_ctv = ShopModel::where('customer_id',$data_customer->id)->first();
        $data_order_ctv = OrderCTVModel::where('shop_id',$data_shop_ctv->id)->get();
        if($data_order_ctv){
            foreach($data_order_ctv as $key => $value){
                $order_parent = OrderModel::find($value->order_id);
                $data_order_ctv[$key]['total_price'] = $order_parent->total_price;
                $data_order_ctv[$key]['order_code'] = $order_parent->order_code;
                $data_shop_1 = ShopModel::find($order_parent['shop_id']);
                $data_order_ctv[$key]['shop_name'] = $data_shop_1->name;
                $data_order_ctv[$key]['shop_image'] = $data_shop_1->image;
                $data_supplier = SupplierModel::find($data_shop_1->supplier_id);
                $data_order_ctv[$key]['supplier_name'] = $data_supplier->name;
                if ($order_parent['status'] == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($order_parent['status'] == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Chờ lấy hàng';
                } elseif ($order_parent['status'] == 2) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($order_parent['status'] == 3) {
                    $data_order_ctv[$key]['is_status'] = 'Đã giao hàng';
                }
                if ($order_parent->status == 4 && $order_parent->cancel_confirm == 0) {
                    $data_order_ctv[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                } elseif ($order_parent->status == 4 && $order_parent->cancel_confirm == 1) {
                    $data_order_ctv[$key]['is_status'] = 'Đã huỷ';
                    if ($order_parent['cancel_user'] == 0) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order_ctv[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($order_parent['cancel_user'] == 1) {
                        $data_order_ctv[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($order_parent['cancel_reason'] === 0) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($order_parent['cancel_reason'] === 1) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($order_parent['cancel_reason'] === 2) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($order_parent['cancel_reason'] === 3) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($order_parent['cancel_reason'] === 4) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($order_parent['cancel_reason'] === 5) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Không mua nữa';
                        } elseif ($order_parent['cancel_reason'] === 6) {
                            $data_order_ctv[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                }
                $data_shipping = ShippingModel::find($order_parent['shipping_id']);
                $data_order_ctv[$key]['shipping'] = $data_shipping['name'];

                if ($order_parent['payment_id'] == 0) {
                    $data_order_ctv[$key]['payment'] = 'Ví CTV24h';
                } elseif ($order_parent['payment_id'] == 1) {
                    $data_order_ctv[$key]['payment'] = 'Trả góp';
                } elseif ($order_parent['payment_id'] == 2) {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } elseif ($order_parent['payment_id'] == 3) {
                    $data_order_ctv[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
                } elseif ($order_parent['payment_id'] == 4) {
                    $data_order_ctv[$key]['payment'] = 'CTV24H PAY';
                } else {
                    $data_order_ctv[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
                }
            }
        }


        $data_order = OrderModel::where('customer_id', $data_login->customer_id)->where(array('collaborator_id' => 1, 'is_selected' => 2,'status' => 5))->get();
        $total_order = (count($data_order)??0) + (count($data_order_ctv)??0);
        foreach ($data_order as $key => $value) {
            $data_shop = ShopModel::find($value['shop_id']);
            $data_order[$key]['shop_name'] = $data_shop->name;
            $data_order[$key]['shop_image'] = $data_shop->image;
            $data_supplier = SupplierModel::find($data_shop->supplier_id);
            $data_order[$key]['supplier_name'] = $data_supplier->name;
            if ($value['status'] == 0) {
                $data_order[$key]['is_status'] = 'Chờ xác nhận';
            } elseif ($value['status'] == 1) {
                $data_order[$key]['is_status'] = 'Chờ lấy hàng';
            } elseif ($value['status'] == 2) {
                $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
            } elseif ($value['status'] == 3) {
                $data_order[$key]['is_status'] = 'Đã giao hàng';
            }
            if ($value->status == 4 && $value->cancel_confirm == 0) {
                $data_order[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            } elseif ($value->status == 4 && $value->cancel_confirm == 1) {
                $data_order[$key]['is_status'] = 'Đã huỷ';
                if ($value['cancel_user'] == 0) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                    $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                } elseif ($value['cancel_user'] == 1) {
                    $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    if ($value['cancel_reason'] === 0) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] === 1) {
                        $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] === 2) {
                        $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] === 3) {
                        $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] === 4) {
                        $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } elseif ($value['cancel_reason'] === 5) {
                        $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                    } elseif ($value['cancel_reason'] === 6) {
                        $data_order[$key]['reason_cancel'] = 'Lý do khác';
                    }
                }
            }
            $data_shipping = ShippingModel::find($value['shipping_id']);
            $data_order[$key]['shipping'] = $data_shipping['name'];

            if ($value['payment_id'] == 0) {
                $data_order[$key]['payment'] = 'Ví CTV24h';
            } elseif ($value['payment_id'] == 1) {
                $data_order[$key]['payment'] = 'Trả góp';
            } elseif ($value['payment_id'] == 2) {
                $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
            } elseif ($value['payment_id'] == 3) {
                $data_order[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
            } elseif ($value['payment_id'] == 4) {
                $data_order[$key]['payment'] = 'CTV24H PAY';
            } else {
                $data_order[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
            }
        }
        $data_order_detail = OrderDetailModel::all();
        if (count($data_order_detail)) {
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v['product_id']);
                $data_order_detail[$k]['product_name'] = $data_product['name'];
                $data_order_detail[$k]['product_image'] = $data_product['image'];
                $dataValue_Attribute = AttributeValueModel::find($v->type_product);
                $data_order_detail[$k]->value_attr = $dataValue_Attribute->value ?? '';
                $data_order_detail[$k]->size_of_value = $dataValue_Attribute->size_of_value ?? '';
            }
        }
        $dataReturn = [
            'status' => true,
            'title' => 'Đơn hàng',
            'data_order' => $data_order,
            'data_order_detail' => $data_order_detail,
            'total_order' => $total_order,
            'data_order_ctv' => $data_order_ctv ?? null,
        ];
        return view('collaborator.order.return_money', $dataReturn);
    }

    /**
     *  Chi tiết đơn hàng
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function detail($id)
    {
        $fee = FeeCTV24HModel::first();
        $data_order = OrderModel::find($id);
        $data_customer = CustomerModel::find($data_order->customer_id);
        $data_address = AddressCustomerCTVModel::where('order_id', $id)->first();
        if($data_address){
            $data_order['name_shipping'] = $data_address->name;
            $data_order['phone_shipping'] = $data_address->phone;
            $data_city = CityModel::find($data_address->city);
            $data_district = DistrictModel::find($data_address->district);
            $data_ward = WardModel::find($data_address->ward);
            $data_order->address_shipping = $data_address->address . ', ' . $data_ward->name . ', ' . $data_district->name . ', ' . $data_city->name;
        }else{
            $data_address = CustomerAddressModel::where('customer_id', $data_customer->id)->where('default', 1)->first();
            if ($data_address) {
                $data_order['name_shipping'] = $data_address->name;
                $data_order['phone_shipping'] = $data_address->phone;
                $data_city = CityModel::find($data_address->city_id);
                $data_district = DistrictModel::find($data_address->district_id);
                $data_ward = WardModel::where('ward_id', $data_address->ward_id)->first();
                $data_order->address_shipping = $data_address->address . ', ' . $data_ward->name . ', ' . $data_district->name . ', ' . $data_city->name;
            }
        }


        $data_shipping = ShippingModel::find($data_order['shipping_id']);
        $data_order['shipping'] = $data_shipping['name'];
        if ($data_order['payment_id'] == 0) {
            $data_order['payment'] = 'Ví CTV24h';
        } elseif ($data_order['payment_id'] == 1) {
            $data_order['payment'] = 'Trả góp';
        } elseif ($data_order['payment_id'] == 2) {
            $data_order['payment'] = 'Thanh toán khi nhận hàng';
        } elseif ($data_order['payment_id'] == 3) {
            $data_order['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
        } elseif ($data_order['payment_id'] == 4) {
            $data_order['payment'] = 'CTV24H PAY';
        } else {
            $data_order['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
        }
        if ($data_order->status == 4 && $data_order->cancel_confirm == 0) {
            $data_order['is_status'] = 'Yêu cầu huỷ hàng';
            if ($data_order['cancel_user'] == 0) {
                $data_order['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                $data_order['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
            } elseif ($data_order['cancel_user'] == 1) {
                $data_order['user_cancel'] = 'Đã huỷ bởi người mua';
                if ($data_order['cancel_reason'] == 0) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                } elseif ($data_order['cancel_reason'] == 1) {
                    $data_order['reason_cancel'] = 'Muốn nhập mã Voucher';
                } elseif ($data_order['cancel_reason'] == 2) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                } elseif ($data_order['cancel_reason'] == 3) {
                    $data_order['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                } elseif ($data_order['cancel_reason'] == 4) {
                    $data_order['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                } elseif ($data_order['cancel_reason'] == 5) {
                    $data_order['reason_cancel'] = 'Đổi ý không muốn mua nữa';
                } else {
                    $data_order['reason_cancel'] = 'Lý do khác';
                }
            }
        } elseif ($data_order->status == 4 && $data_order->cancel_confirm == 1) {
            $data_order['is_status'] = 'Đã huỷ';
            if ($data_order['cancel_user'] == 0) {
                $data_order['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                $data_order['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
            } elseif ($data_order['cancel_user'] == 1) {
                $data_order['user_cancel'] = 'Đã huỷ bởi người mua';
                if ($data_order['cancel_reason'] == 0) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                } elseif ($data_order['cancel_reason'] == 1) {
                    $data_order['reason_cancel'] = 'Muốn nhập mã Voucher';
                } elseif ($data_order['cancel_reason'] == 2) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                } elseif ($data_order['cancel_reason'] == 3) {
                    $data_order['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                } elseif ($data_order['cancel_reason'] == 4) {
                    $data_order['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                } else {
                    $data_order['reason_cancel'] = 'Lý do khác';
                }
            }
        }
        $data_order_detail = OrderDetailModel::where('order_id', $data_order->id)->get();
        foreach ($data_order_detail as $key => $value) {
            $data_product = ProductModel::find($value['product_id']);
            $data_order_detail[$key]['product_name'] = $data_product['name'];
            $data_order_detail[$key]['product_image'] = $data_product['image'];
            $dataValue_Attribute = AttributeValueModel::find($value->type_product);
            $data_order_detail[$key]->value_attr = $dataValue_Attribute->value ?? '';
            $data_order_detail[$key]->size_of_value = $dataValue_Attribute->size_of_value ?? '';
        }
        $data_return = [
            'title' => 'Chi tiết đơn hàng: ' . $data_order['order_code'],
            'customer' => $data_customer,
            'order' => $data_order,
            'order_detail' => $data_order_detail,
            'fee' => $fee
        ];
        return view('collaborator.order.detail', $data_return);
    }

    /**
     *  Lọc theo ngày
     * @param Request $request
     */
    public
    function filterOrderDate(Request $request)
    {
        $time_start = $request->time_start;
        $time_stop = $request->time_stop;
        $data_login = session()->get('data_collaborator');
        $output = '';
        $data_order = OrderModel::where('customer_id', $data_login->customer_id)
            ->where('collaborator_id', 1)
            ->where('date', '>=', $time_start)
            ->where('date', '<=', $time_stop)
            ->where('is_selected', 2)
            ->orderBy('id', 'desc')
            ->get();
        $total_row = $data_order->count();
        if ($total_row > 0) {
            foreach ($data_order as $key => $value) {
                $data_shop = ShopModel::find($value->shop_id);
                $data_supplier = SupplierModel::find($data_shop->supplier_id);
                //Danh sách url
                $url_detail = url('admin/order_collaborator/detail/' . $value->id);
                $data_order[$key]['btn_chitiet'] = '<a href="' . $url_detail . '" class="text-blue text-underline text-bold-700 btn-detail-order">Chi tiết</a>';

                //nút xem thêm
                $btn_xemthem = '<a href="' . $url_detail . '" class="text-underline text-bold-700">Xem thêm</a>';
                $data_order[$key]['shop_name'] = $data_shop->name;
                $data_order[$key]['shop_image'] = $data_shop->image;
                $data_order[$key]['supplier_name'] = $data_supplier->name;
                $data_shipping = ShippingModel::find($value['shipping_id']);
                $data_order[$key]['shipping'] = $data_shipping['name'];
                //check trạng thái và các thông báo, các nút theo trạng thái tương ứng
                if ($value['status'] == 0) {
                    $data_order[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($value['status'] == 1) {
                    $data_order[$key]['is_status'] = 'Chờ lấy hàng';
                } elseif ($value['status'] == 2) {
                    $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($value['status'] == 3) {
                    $data_order[$key]['is_status'] = 'Đã giao hàng';
                } elseif ($value['status'] == 4) {
                    if ($value['cancel_confirm'] == 0) {
                        $data_order[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                        if ($value['cancel_reason'] == 0) {
                            $data_order[$key]['reason_cancel'] = 'Lý do: Muốn thay đổi địa chỉ hàng';
                        } elseif ($value['cancel_reason'] == 1) {
                            $data_order[$key]['reason_cancel'] = 'Lý do: Muốn nhập mã Voucher';
                        } elseif ($value['cancel_reason'] == 2) {
                            $data_order[$key]['reason_cancel'] = 'Lý do: Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($value['cancel_reason'] == 3) {
                            $data_order[$key]['reason_cancel'] = 'Lý do: Thủ tục thanh toán quá rắc rối';
                        } elseif ($value['cancel_reason'] == 4) {
                            $data_order[$key]['reason_cancel'] = 'Lý do: Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($value['cancel_reason'] == 5) {
                            $data_order[$key]['reason_cancel'] = 'Lý do: Đổi ý không mua nữa';
                        } else {
                            $data_order[$key]['reason_cancel'] = 'Lý do: Lý do khác';
                        }
                        $data_order[$key]['notification_bottom'] = '
                                                                    <br>
                                                                    <span class="text-sliver f-12 text-bold-700">' . $data_order[$key]["reason_cancel"] . '</span>
                                                                ';
                    } else {
                        $data_order[$key]['is_status'] = 'Đã huỷ';
                        if ($value->cancel_user == 0) {
                            $data_order[$key]['user_cancel'] = 'Đã huỷ bởi hệ thống';
                        } else {
                            $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        }
                        $data_order[$key]['notification_bottom'] = '
                                                                    <br>
                                                                    <span class="text-sliver f-12 text-bold-700">' . $data_order[$key]['user_cancel'] . '</span>
                                                                    <br>
                                                                ';
                    }
                } else {
                    $data_order[$key]['is_status'] = 'Hoàn tiền';
                    $data_order[$key]['btn_phanhoi'] = '<a class="text-blue text-underline text-bold-700 btn-feedback">Phản hồi</a>';
                    $data_order[$key]['btn_chitiet_trahang'] = '<br>
                                                <a class="text-blue text-underline text-bold-700 btn-detail-return-money">Chi tiết trả hàng hoàn tiền</a>
                                                ';
                }
                if ($value['payment_id'] == 0) {
                    $data_order[$key]['payment'] = 'Ví CTV24h';
                } elseif ($value['payment_id'] == 1) {
                    $data_order[$key]['payment'] = 'Trả góp';
                } elseif ($value['payment_id'] == 2) {
                    $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } elseif ($value['payment_id'] == 3) {
                    $data_order[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
                } elseif ($value['payment_id'] == 4) {
                    $data_order[$key]['payment'] = 'CTV24H PAY';
                } else {
                    $data_order[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
                }
                $data_order_detail = OrderDetailModel::where('shop_id', $data_shop->id)->orderBy('id', 'desc')->get();
                $html_order_detail = '';
                foreach ($data_order_detail as $k => $v) {
                    $data_product = ProductModel::find($v->product_id);
                    $data_order_detail[$k]['product_name'] = $data_product->name;
                    $data_order_detail[$k]['product_image'] = $data_product->image;
//                    if ($data_product->dropship == 1) {
//                        $data_order_detail[$k]['product_dropship'] = 'Hàng Dropship';
//                    } else {
                    $data_order[$key]['order_dropship'] = 'Hàng dropship';
//                    }
                    if ($v->order_id == $value->id) {
                        $html_order_detail .= '
                                             <div class="list_product d-flex align-items-center mb-1">
                                                <div class="img_product">
                                                    <img class="border_radius" width="70px" height="70px"
                                                         src="' . $v->product_image . '"
                                                         alt="">
                                                </div>
                                                <div class="info_product ml-1">
                                                    <span class="text-bold-700">' . $v->product_name . '<span
                                                            class="text-default"> (Số lượng: ' . $v->quantity . ')</span></span>
                                                    <br>
                                                    <span
                                                        class="text-sliver f-12 m-0 text-bold-700">ID đơn hàng: ' . $value->order_code . '</span>
                                                </div>
                                            </div>
                            ';
                    }
                }
                $output .= '
                        <tr class="border_tr">
                            <td colspan="10">
                                <div class="d-flex align-items-center">
                                    <img style="border-radius: 50%" width="20px" height="20px"
                                         src="' . $value->shop_image . '" alt="">
                                    <b style="margin-left: 10px;">' . $value->shop_name . '</b>
                                </div>
                            </td>
                        </tr>
                        <tr class="border_tr">
                            <td>' . $html_order_detail . '' . $btn_xemthem . '</td>
                            <td class="text-blue text-bold-700">' . $value->order_dropship . '</td>
                            <td class="text-blue text-bold-700">' . $value->supplier_name . '</td>
                            <td class="text-center">' . $value->notification_status . '<span class="text-bold-700">' . $value->is_status . '</span>' . $value->notification_bottom . '<br>' . $value->btn_cod . '</td>
                            <td class="text-center">
                                <p class="text-bold-700">' . $value->shipping . '</p>
                            </td>
                            <td class="text-center">
                                <span
                                    class="text-red f-18 text-bold-700">' . number_format($value->total_price) . ' đ</span>
                                <p class="text-sliver">' . $value->payment . '</p>
                            </td>
                            <td class="text-red f-18 text-bold-700">' . number_format($value->bonus) . ' đ</td>
                            <td class="text-center">
                                ' . $value->btn_chitiet . '
                            </td>
                        </tr>

                    ';
            }
        } else {
            $output .= '
                            <tr class="border_tr">
                                <td class="text-center text-bold-700" colspan="100">Không có đơn hàng nào !</td>
                            </tr>
                              ';
        }

        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Tìm kiếm đơn hàng
     * @param Request $request
     */
    public function search(Request $request)
    {
        $data_login = session()->get('data_collaborator');
        $data_customer = CustomerModel::find($data_login['customer_id']);
        $order_code_1 = $request->order_code;
        $order_code = str_replace(' ', '%', $order_code_1);
        $data_order = OrderModel::where('collaborator_id', 1)->where('is_selected', 2)->where('customer_id', $data_customer['id'])->where('order_code', 'like', '%' . $order_code . '%')->orderBy('id', 'desc')->get();
        $total_row = $data_order->count();
        if ($total_row > 0) {
            foreach ($data_order as $key => $value) {
                $data_shop = ShopModel::find($value['shop_id']);
                $data_order[$key]['shop_name'] = $data_shop->name;
                $data_order[$key]['shop_image'] = $data_shop->image;
                $data_supplier = SupplierModel::find($data_shop->supplier_id);
                $data_order[$key]['supplier_name'] = $data_supplier->name;
                if ($value['status'] == 0) {
                    $data_order[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($value['status'] == 1) {
                    $data_order[$key]['is_status'] = 'Chờ lấy hàng';
                } elseif ($value['status'] == 2) {
                    $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($value['status'] == 3) {
                    $data_order[$key]['is_status'] = 'Đã giao hàng';
                }
                if ($value->status == 4 && $value->cancel_confirm == 0) {
                    $data_order[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                    if ($value['cancel_user'] == 0) {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($value['cancel_user'] == 1) {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($value['cancel_reason'] === 0) {
                            $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($value['cancel_reason'] === 1) {
                            $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($value['cancel_reason'] === 2) {
                            $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($value['cancel_reason'] === 3) {
                            $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($value['cancel_reason'] === 4) {
                            $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($value['cancel_reason'] === 5) {
                            $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                        } else {
                            $data_order[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                } elseif ($value->status == 4 && $value->cancel_confirm == 1) {
                    $data_order[$key]['is_status'] = 'Đã huỷ';
                    if ($value['cancel_user'] == 0) {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                        $data_order[$key]['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
                    } elseif ($value['cancel_user'] == 1) {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        if ($value['cancel_reason'] === 0) {
                            $data_order[$key]['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                        } elseif ($value['cancel_reason'] === 1) {
                            $data_order[$key]['reason_cancel'] = 'Muốn nhập mã Voucher';
                        } elseif ($value['cancel_reason'] === 2) {
                            $data_order[$key]['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($value['cancel_reason'] === 3) {
                            $data_order[$key]['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                        } elseif ($value['cancel_reason'] === 4) {
                            $data_order[$key]['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                        } elseif ($value['cancel_reason'] === 5) {
                            $data_order[$key]['reason_cancel'] = 'Không mua nữa';
                        } else {
                            $data_order[$key]['reason_cancel'] = 'Lý do khác';
                        }
                    }
                }
                $data_shipping = ShippingModel::find($value['shipping_id']);
                $data_order[$key]['shipping'] = $data_shipping['name'];

                if ($value['payment_id'] == 0) {
                    $data_order[$key]['payment'] = 'Ví CTV24h';
                } elseif ($value['payment_id'] == 1) {
                    $data_order[$key]['payment'] = 'Trả góp';
                } elseif ($value['payment_id'] == 2) {
                    $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } elseif ($value['payment_id'] == 3) {
                    $data_order[$key]['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
                } elseif ($value['payment_id'] == 4) {
                    $data_order[$key]['payment'] = 'CTV24H PAY';
                } else {
                    $data_order[$key]['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
                }
            }
            $data_order_detail = OrderDetailModel::where('customer_id', $data_login->customer_id)->get();
            if (count($data_order_detail)) {
                $data_total_product = 0;
                foreach ($data_order_detail as $k => $v) {
                    $data_product = ProductModel::find($v['product_id']);
                    $data_order_detail[$k]['product_name'] = $data_product['name'];
                    $data_order_detail[$k]['product_image'] = $data_product['image'];
                    $dataValue_Attribute = AttributeValueModel::find($v->type_product);
                    $data_order_detail[$k]->value_attr = $dataValue_Attribute->value ?? '';
                    $data_order_detail[$k]->size_of_value = $dataValue_Attribute->size_of_value ?? '';
                    $data_total_product += $v->total_price;
                }
                $data_order['total_price_product'] = $data_total_product;
            }
        }
        $dataReturn = [
            'status' => true,
            'data_order' => $data_order,
            'data_order_detail' => $data_order_detail ?? null,
            'order_code' => $order_code,
            'total_data' => $total_row ?? 0,
            'title' => 'Tìm kiếm đơn hàng: ' . $order_code
        ];
        return view('collaborator.order.search', $dataReturn);
    }

    public
    function exportOrder()
    {
        return Excel::download(new OrderCollaboratorExport(), 'Order_Collaborator.xlsx');
    }
}
