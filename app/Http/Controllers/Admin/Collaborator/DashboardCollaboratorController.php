<?php

namespace App\Http\Controllers\Admin\Collaborator;

use App\Http\Controllers\CollaboratorBaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class DashboardCollaboratorController extends CollaboratorBaseController
{
    public function index(){
        $data_return = [
            'title' => 'Kênh CTV | CTV24H'
        ];
        return view('admin.dashboard.dashboard_collaborator', $data_return);
    }
}
