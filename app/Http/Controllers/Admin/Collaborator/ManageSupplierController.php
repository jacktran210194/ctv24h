<?php

namespace App\Http\Controllers\Admin\Collaborator;

use App\Exports\Collaborator\SupplierExport;
use App\Http\Controllers\Controller;
use App\Models\AttributeValueModel;
use App\Models\CategoryProductChildModel;
use App\Models\CollaboratorModel;
use App\Models\DropshipModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductAttributeModel;
use App\Models\ProductModel;
use App\Models\PromotionModel;
use App\Models\ShopModel;
use App\Models\SupplierModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Fakes\ExcelFake;
use DB;

class ManageSupplierController extends Controller
{
    /**
     *  Danh sách NCC
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){
        $data_user = session()->get('data_collaborator');
        $dataShopSupplier = DB::table('shop')
            ->select('order.shop_id as shop_id', 'supplier.id as id_supplier', 'shop.name as name_shop')
            ->distinct('shop_id')
            ->join('order', 'shop.id', '=', 'order.shop_id')
            ->join('supplier', 'shop.supplier_id', 'supplier.id')
            ->where('order.customer_id', $data_user->customer_id)
            ->where('order.collaborator_id', 1)
            ->get();
        foreach ($dataShopSupplier as $k => $v){
            $dataShopSupplier[$k]->count = 0;
            $dataOrder = OrderModel::where('shop_id', $v->shop_id)->where('collaborator_id', 1)->get();
            $dataShopSupplier[$k]->cod = 0;
            foreach ($dataOrder as $key => $value){
                $dataShopSupplier[$k]->count += 1;
                $dataOrderDetail = OrderDetailModel::where('shop_id', $v->shop_id)->get();
                $dataShopSupplier[$k]->total = 0;
                $dataShopSupplier[$k]->cod += $value->cod;
                foreach ($dataOrderDetail as $kOrDe => $vOrDe){
                    $dataShopSupplier[$k]->total += $vOrDe->product_price * $vOrDe->quantity;
//                    $dataShopSupplier[$k]->cod += $vOrDe->cod;
                }
            }
        }
        $dataReturn = [
            'data' => $dataShopSupplier,
            'title' => 'Quản lý nhà cung cấp',
            'manage_supplier' => true,
        ];
        return view('collaborator.manage_supplier.index', $dataReturn);
    }

    /**
     *  Tất cả
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function filterAll(){
        $data_user = session()->get('data_collaborator');
        $dataShopSupplier = DB::table('shop')
            ->select('order.shop_id as shop_id', 'supplier.id as id_supplier', 'shop.name as name_shop')
            ->distinct('shop_id')
            ->join('order', 'shop.id', '=', 'order.shop_id')
            ->join('supplier', 'shop.supplier_id', 'supplier.id')
            ->where('order.customer_id', $data_user->customer_id)
            ->where('order.collaborator_id', 1)
            ->get();
        foreach ($dataShopSupplier as $k => $v){
            $dataShopSupplier[$k]->count = 0;
            $dataOrder = OrderModel::where('shop_id', $v->shop_id)->where('collaborator_id', 1)->get();
            $dataShopSupplier[$k]->cod = 0;
            foreach ($dataOrder as $key => $value){
                $dataShopSupplier[$k]->count += 1;
                $dataOrderDetail = OrderDetailModel::where('shop_id', $v->shop_id)->get();
                $dataShopSupplier[$k]->total = 0;
                $dataShopSupplier[$k]->cod += $value->cod;
                foreach ($dataOrderDetail as $kOrDe => $vOrDe){
                    $dataShopSupplier[$k]->total += $vOrDe->product_price * $vOrDe->quantity;
                }
            }
        }
        $dataReturn = [
            'data' => $dataShopSupplier,
            'title' => 'Quản lý nhà cung cấp',
            'manage_supplier' => true,
        ];
        return view('collaborator.manage_supplier.all', $dataReturn);
    }

    /**
     *  Đang bán hàng NCC
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function filterSelling(){
        $dataReturn = [
            'title' => 'Quản lý nhà cung cấp',
        ];
        return view('collaborator.manage_supplier.selling', $dataReturn);
    }

    /**
     * Đã đặt hàng NCC
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function filterOrder(){
        $dataReturn = [
            'title' => 'Quản lý nhà cung cấp',
        ];
        return view('collaborator.manage_supplier.selling', $dataReturn);
    }

    /**
     * NCC đang theo dõi
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function filterFollow(){
        $dataReturn = [
            'title' => 'Quản lý nhà cung cấp',
        ];
        return view('collaborator.manage_supplier.selling', $dataReturn);
    }


    /**
     *  Thêm mã giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $data_return = [
            'title' => 'Mã giảm giá',
            'promotion' => true
        ];
        return view('supplier.promotion.form', $data_return);
    }

    /**
     *  Lưu mã giảm giá
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        if ($request->id) {
            $data_return = $this->update($request);
        } else {
            $data_return = $this->create($request);
        }
        return response()->json($data_return, Response::HTTP_OK);
    }

    /**
     * @param $request
     * @param $rules
     * @param $customMessages
     * @return array
     */
    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /**
     *  Thêm mã giảm giá
     * @param $request
     * @return array
     */
    public function create($request){
        $rules = [
            'title' => 'required',
            'content' => 'required',
            'code' => 'required',
            'value' => 'required',
            'time_start' => 'required',
            'time_end' => 'required',
            'number' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $data_user = session()->get('data_collaborator');
            $title = $request->title;
            $content = $request->content;
            $code = $request->code;
            $value = $request->value;
            $time_start = $request->time_start;
            $time_end = $request->time_end;
            $number = $request->number;
            $supplier_id = $data_user->id;
            $data = PromotionModel::create([
                'title' => $title,
                'content' => $content,
                'code' => $code,
                'value' => $value,
                'time_start' => $time_start,
                'time_end' => $time_end,
                'number' => $number,
                'supplier_id' => $supplier_id ,
                'image' => ''
            ]);
            $data->save();
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/promotion/';
            $file->move($path, $fileName);
            $data->image = '/uploads/images/promotion/' . $fileName;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/promotion'),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Xóa mã NCC
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            SupplierModel::where('id', $id)->update(['collaborator_id' => null]);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     *  Chi tiết NCC
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function detail(Request $request, $id){
        $dataProduct = ProductModel::where('shop_id', $id)->where('active', 1)->get();
        $dataReturn = [
            'title' => 'Chi tiết Nhà cung cấp',
            'data' => $dataProduct
        ];
        return view('collaborator.manage_supplier.detail', $dataReturn);
    }

    /**
     *  Tìm kiếm sản phẩm của NCC
     */
    public function searchProduct(Request $request){
        if ($request->ajax()) {
            $output = '';
            $query = $request->get('query');
            if ($query != '') {
                $data = ProductModel::where('name', 'like', '%' . $query . '%')
                    ->where('active', 1)
                    ->orderBy('id', 'desc')
                    ->get();
                if (count($data)) {
                    foreach ($data as $key => $value) {
                        $data[$key]['dropship'] = ($value['dropship'] == 1) ? 'Hàng Dropship' : 'Hàng Có Sẵn';
                        $data_category = CategoryProductChildModel::find($value['category_id']);
                        $data[$key]['category'] = $data_category['name'] ?? '';
                        if ($value['is_status'] == 0) {
                            $data[$key]['status'] = 'Sản phẩm còn hàng';
                        } elseif ($value['is_status'] == 1) {
                            $data[$key]['status'] = 'Sản phẩm hết hàng';
                        } elseif ($value['is_status'] == 2) {
                            $data[$key]['status'] = 'Sản phẩm bị đánh vi phạm';
                        } else {
                            $data[$key]['status'] = 'Sản phẩm bị ẩn';
                        }
                    }
                }
            } else {
                $data = ProductModel::orderBy('id', 'desc')->where('active', 1)->get();
                if (count($data)) {
                    foreach ($data as $key => $value) {
                        $data_category = CategoryProductChildModel::find($value['category_id']);
                        $data[$key]['category'] = $data_category['name'] ?? '';
                        if ($value['is_status'] == 0) {
                            $data[$key]['status'] = 'Sản phẩm còn hàng';
                        } elseif ($value['is_status'] == 1) {
                            $data[$key]['status'] = 'Sản phẩm hết hàng';
                        } elseif ($value['is_status'] == 2) {
                            $data[$key]['status'] = 'Sản phẩm bị đánh vi phạm';
                        } else {
                            $data[$key]['status'] = 'Sản phẩm bị ẩn';
                        }
                    }
                }
            }
            $total_row = $data->count();
            if ($total_row > 0) {
                foreach ($data as $k => $row) {
                    if(!($row->sku_type)){
                        $data[$k]->sku_type = '-';
                    }else{
                        $data[$k]->sku_type = $row->sku_type;
                    }
                    $title_attr = ProductAttributeModel::where('product_id', $row->id)->first();
                    $product_category = '';
                    $product_price = '';
                    $product_quantity = '';
                    $product_sold = '';
                    $attr_product = AttributeValueModel::where('attribute_id', $row->id)->get();
                    if (!$attr_product || !$title_attr) {
                        $product_category .= '-';
                        $product_price .= '<div class="text-red text-bold-700 text-center">' . number_format($row->price) . ' đ</div>';
                        $product_quantity .= '<div class="text-bold-700 text-center">' . $row->warehouse . '</div>';
                        $product_sold .= '<div class="text-bold-700">' . $row->is_sell . '</div>';
                    }
                    foreach ($attr_product as $item) {
                        if ($item->quantity > 0) {
                            $quantity = $item->quantity;
                        } else {
                            $quantity = '<button class="disabled" disabled="disabled">hết hàng</button>';
                        }
                        if ($item->attribute_id == $row->id) {
                            $product_category .= '<div class="padding-10 color-gray font-size-14 text-left">' . $title_attr->category_1 . ':' . $item->value . '/' . $item->size_of_value . '</div>';
                            $product_price .= '<div class="padding-10 color-red font-weight-bold text-center">' . number_format($item->price) . 'đ</div>';
                            $product_quantity .= '<div class="padding-10 m-0 text-center font-weight-bold font-size-14" >' . $quantity . '</div>';
                            $product_sold .= '<div class="padding-10 m-0 text-center font-weight-bold font-size-14" >' . $item->is_sell . '</div>';
                        }
                    }
                    $output .= '
                                <tr class="border_box mg-top-30">
                                    <td class="position-relative" style="min-width: 200px"><div class="content-product position-absolute padding-10 d-flex align-items-lg-start font-weight-bold font-size-14">' . '<img width="60px" height="60px" class="border-radius-8" src="' . $row->image . '"
                                             alt="">' . $row->name . '</div></td>
                                    <td class="d-flex align-items-lg-start"><span class="padding-10 font-weight-bold color-black">' . $row->sku_type . '</span></td>
                                    <td>' . $product_category . '</td>
                                    <td>' . $product_price . '</td>
                                    <td>' . $product_quantity . '</td>
                                </tr>
                                ';
                }
            } else {
                $output .= '
                        <tr data-count="0">
                            <td colspan="5">Không có dữ liệu</td>
                        </tr>
            ';
            }
            $data = array(
                'table_data' => $output,
                'total_data' => $total_row
            );
            echo json_encode($data);
        }
    }

    /**
     *  Xuất báo cáo
     * @return mixed
     */
    public function export()
    {
        return Excel::download(new SupplierExport(), 'Supplier.xlsx');
    }

    /**
     *  Tìm kiếm NCC | All
     * @param Request $request
     */
    public function searchAll(Request $request){
        $data_user = session()->get('data_collaborator');
        if ($request->ajax()) {
            $output = '';
            $query = $request->get('query');
            if ($query != '') {
                $dataShopSupplier = DB::table('shop')
                    ->select('order.shop_id as shop_id', 'supplier.id as id_supplier', 'shop.name as name_shop')
                    ->distinct('shop_id')
                    ->join('order', 'shop.id', '=', 'order.shop_id')
                    ->join('supplier', 'shop.supplier_id', 'supplier.id')
                    ->where('order.customer_id', $data_user->customer_id)
                    ->where('order.collaborator_id', 1)
                    ->where('shop.name', 'like', '%' . $query .'%')
                    ->get();
                foreach ($dataShopSupplier as $k => $v){
                    $dataShopSupplier[$k]->count = 0;
                    $dataOrder = OrderModel::where('shop_id', $v->shop_id)->where('collaborator_id', 1)->get();
                    $dataShopSupplier[$k]->cod = 0;
                    foreach ($dataOrder as $key => $value){
                        $dataShopSupplier[$k]->count += 1;
                        $dataOrderDetail = OrderDetailModel::where('shop_id', $v->shop_id)->get();
                        $dataShopSupplier[$k]->total = 0;
                        $dataShopSupplier[$k]->cod += $value->cod;
                        foreach ($dataOrderDetail as $kOrDe => $vOrDe){
                            $dataShopSupplier[$k]->total += $vOrDe->product_price * $vOrDe->quantity;
                        }
                    }
                }
            } else {
                $dataShopSupplier = DB::table('shop')
                    ->select('order.shop_id as shop_id', 'supplier.id as id_supplier', 'shop.name as name_shop')
                    ->distinct('shop_id')
                    ->join('order', 'shop.id', '=', 'order.shop_id')
                    ->join('supplier', 'shop.supplier_id', 'supplier.id')
                    ->where('order.customer_id', $data_user->customer_id)
                    ->where('order.collaborator_id', 1)
                    ->get();
                foreach ($dataShopSupplier as $k => $v){
                    $dataShopSupplier[$k]->count = 0;
                    $dataOrder = OrderModel::where('shop_id', $v->shop_id)->where('collaborator_id', 1)->get();
                    $dataShopSupplier[$k]->cod = 0;
                    foreach ($dataOrder as $key => $value){
                        $dataShopSupplier[$k]->count += 1;
                        $dataOrderDetail = OrderDetailModel::where('shop_id', $v->shop_id)->get();
                        $dataShopSupplier[$k]->total = 0;
                        $dataShopSupplier[$k]->cod += $value->cod;
                        foreach ($dataOrderDetail as $kOrDe => $vOrDe){
                            $dataShopSupplier[$k]->total += $vOrDe->product_price * $vOrDe->quantity;
                        }
                    }
                }
            }
            $total_row = $dataShopSupplier->count();
            if ($total_row > 0) {
                foreach ($dataShopSupplier as $k => $row) {
                    $data_url_detail = URL::to('admin/manage_supplier/detail/' . $row->shop_id);
                    $output .= '
                              <tr class="text-center">
                                    <td>
                                        <div class="d-flex align-items-center justify-content-center">
                                            <span class="mr-1">'.$row->name_shop.'</span>
                                            <img src="../../assets/admin/app-assets/images/icons/chat-small.png">
                                        </div>
                                    </td>
                                    <td>'.$row->count.'</td>
                                    <td>'.number_format($row->total).'đ</td>
                                    <td>'.number_format($row->cod).'đ</td>
                                    <td>
                                        <a href="'.$data_url_detail.'" class="text-blue text-bold-700 text-underline">Chi tiết</a>
                                        <p style="cursor: pointer" class="text-blue text-bold-700 text-underline mt-1">Xóa NCC</p>
                                    </td>
                               </tr>
                                ';
                }
            } else {
                $output = '
                           <tr>
                            <td colspan="5">Không có dữ liệu</td>
                           </tr>
                          ';
            }
            $data = array(
                'table_data' => $output,
                'total_data' => $total_row
            );
            echo json_encode($data);
        }
    }
}
