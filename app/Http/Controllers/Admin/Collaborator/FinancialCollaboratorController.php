<?php

namespace App\Http\Controllers\Admin\Collaborator;

use App\Exports\Collaborator\HistoryPaymentExport;
use App\Exports\Collaborator\HistoryPaymentPointExport;
use App\Exports\Collaborator\RevenueDonePaymentExport;
use App\Exports\Collaborator\RevenueWaitingPaymentExport;
use App\Http\Controllers\Controller;
use App\Models\AddressCustomerCTVModel;
use App\Models\BankAccountModel;
use App\Models\CityModel;
use App\Models\CustomerModel;
use App\Models\HistoryOrderPaymentCollaboratorModel;
use App\Models\HistoryOrderPaymentModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\PaymentModel;
use App\Models\PointModel;
use App\Models\ProductModel;
use App\Models\ShopModel;
use App\Models\TheBankModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use session;
class FinancialCollaboratorController extends Controller
{
    public function indexBanking(){
        $dataUser = session()->get('data_collaborator');
        $dataWallet = CustomerModel::where('id', $dataUser->customer_id)->first();
        $data = BankAccountModel::where('customer_id', '=', $dataUser->customer_id)->orderBy('id', 'desc')->get();
        foreach ($data as $k => $v) {
            $dataBank = TheBankModel::where('id', $v['bank_name'])->first();
            $data[$k]['bank_name'] = $dataBank['name'] ?? '';
            $data[$k]['short_name'] = $dataBank['short_name'] ?? '';
        }
        $data_return = [
            'collaborator' => true,
            'data' => $data,
            'dataWallet' => $dataWallet,
            'title' => 'Tài khoản ngân hàng',
        ];
        return view('collaborator.financial.banking.index', $data_return);
    }

    /**
     *  Thêm tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addBanking(Request $request)
    {
        $dataUser = session()->get('data_collaborator');
        $data = BankAccountModel::where('customer_id', '=', $dataUser->customer_id)->orderBy('id', 'desc')->get();
        foreach ($data as $k => $v) {
            $dataBank = TheBankModel::where('id', $v['bank_name'])->first();
            $data[$k]['bank_name'] = $dataBank['name'] ?? '';
            $data[$k]['short_name'] = $dataBank['short_name'] ?? '';
            $str = $v->bank_account;
            $arr_first = substr($str, 0, 4);
            $arr_last = substr($str, -4, 4);
            $data[$k]->first_num_acc = $arr_first;
            $data[$k]->last_num_acc = $arr_last;
        }
        $dataSupplier = CustomerModel::find($dataUser->customer_id);
        $dataBank = TheBankModel::orderBy('id', 'desc')->get();

        $data_return = [
            'title' => 'Thêm tài khoản ngân hàng',
            'dataBank' => $dataBank,
            'dataSupplier' => $dataSupplier,
            'dataBankAccount' => $data,
        ];
        return view('collaborator.financial.banking.form', $data_return);
    }

    /**
     *  Thông tin tài khoản ngân hàng
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function infoBankAccount(Request $request, $id){
        $dataInfoBankAcc = BankAccountModel::find($id);
        $dataBank = TheBankModel::find($dataInfoBankAcc->bank_name);
        $dataInfoBankAcc->bank_name = $dataBank->name ?? '';
        $dataInfoBankAcc->short_name = $dataBank->short_name ?? '';
        $str = $dataInfoBankAcc->bank_account;
        $arr_last = substr($str, -3, 3);
        $dataInfoBankAcc->last_num_acc = $arr_last;
        $dataCity = CityModel::find($dataInfoBankAcc->branch);
        $dataInfoBankAcc->branch = $dataCity->name ?? '';
        $data_return = [
            'status' => true,
            'msg' => 'OK',
            'dataInfoBankAcc' => $dataInfoBankAcc,
        ];
        return view('collaborator.financial.banking.detail', $data_return);
    }

    /**
     *  Đặt tài khoản làm mặc định
     * @param Request $request
     * @return array
     */
    public function checkBankAccount(Request $request){
        $dataLogin = session()->get('data_collaborator');
        BankAccountModel::where('customer_id', $dataLogin->customer_id)->update(['is_default' => 0]);
        BankAccountModel::where('customer_id', $dataLogin->customer_id)->where('id', $request->bank_id)->update(['is_default' => 1]);
        $dataReturn = [
            'status' => true,
            'msg' => 'Thành công',
        ];
        return $dataReturn;
    }

    /**
     *  Thêm tài khoản ngân hàng
     * @param Request $request
     * @return array
     */
    public function createBank(Request $request)
    {
        $rules = [
            'user_name' => 'required',
            'bank_name' => 'required',
            'bank_account' => 'required|unique:bank_account,bank_account,' . $request->id,
            'date_active' => 'required',
            'identify_card' => 'required',
//            'branch ' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'bank_account.unique' => 'Số tài khoản đã tồn tại'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $dataUser = session()->get('data_collaborator');
            $data = new BankAccountModel([
                'customer_id' => $dataUser->customer_id,
                'user_name' => $request->user_name,
                'bank_name' => $request->bank_name,
                'bank_account' => $request->bank_account,
                'date_active' => $request->date_active,
                'identify_card' => $request->identify_card,
                'branch' => $request->branch
            ]);
            $data->save();
            $dataReturn = [
                'status' => true,
                'msg' => 'Thêm tài khoản ngân hàng thành công',
                'url' => url('admin/collaborator_financial/banking'),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Hủy liên kết tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteBank(Request $request)
    {
        try {
            BankAccountModel::destroy($request->id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Hủy liên kết thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     *  Liên kết tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function linkBank(Request $request){
        $data = BankAccountModel::where('id', $request->id)->update(['is_active' => 1]);
        $dataReturn = [
            'status' => true,
            'msg' => 'Liên kết thành công',
            'data' => $data
        ];
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     *  Hủy liên kết tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unlinkBank(Request $request){
        $data = BankAccountModel::where('id', $request->id)->update(['is_active' => 0]);
        $dataReturn = [
            'status' => true,
            'msg' => 'Hủy liên kết thành công',
            'data' => $data
        ];
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     *  Ví CTV
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexWallet(Request $request){
        $dataLogin = session()->get('data_collaborator');
        switch ($request->type) {
            case 'revenue' :
                $type = 'revenue';
                break;
            case 'withdrawal' :
                $type = 'withdrawal';
                break;
            case 'refund' :
                $type = 'refund';
                break;
            case 'bonus' :
                $type = 'bonus';
                break;
            default :
                $type = 'all';
                break;
        }
        $dataBankAccount = BankAccountModel::where('customer_id', $dataLogin['customer_id'])->orderBy('id', 'desc')->get();
        if ($dataBankAccount) {
            foreach ($dataBankAccount as $k => $v) {
                $dataTheBank = TheBankModel::find($v->bank_name);
                $dataBankAccount[$k]->short_name = $dataTheBank->short_name ?? '';
                $dataBankAccount[$k]->bank_name = $dataTheBank->name ?? '';
                $str = $v->bank_account;
                $arr = substr($str, -6, 6);
                $dataBankAccount[$k]->sub_bank_account = $arr;
            }
        }
        $dataBank = TheBankModel::orderBy('id', 'desc')->get();
        $data = CustomerModel::find($dataLogin['customer_id']);
        $dataCity = CityModel::orderBy('name', 'asc')->get();

        $dataBankAccountRight = BankAccountModel::where('customer_id', $dataLogin['customer_id'])->where('is_default', 1)->first();
        if($dataBankAccountRight){
            $dataTheBank = TheBankModel::find($dataBankAccountRight['bank_name']);
            $dataBankAccountRight->short_name = $dataTheBank->short_name ?? '';
            $dataBankAccountRight->bank_name = $dataTheBank->name ?? '';
            $str = $dataBankAccountRight->bank_account;
            $arr_first = substr($str, 0, 4);
            $arr_last = substr($str, -4, 4);
            $dataBankAccountRight->first_num_acc = $arr_first;
            $dataBankAccountRight->last_num_acc = $arr_last;
        }
        $data_return = [
            'collaborator' => true,
            'data' => $data,
            'title' => 'Quản lý ví',
            'type' => $type,
            'dataBank' => $dataBank,
            'dataCity' => $dataCity,
            'dataBankAccount' => $dataBankAccount,
            'dataBankAccountRight' => $dataBankAccountRight
        ];
        return view('collaborator.financial.wallet.index', $data_return);
    }

    /**
     *  Lịch sử giao dịch
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function payment(Request $request){
        $dataUser = session()->get('data_collaborator');
        $data_withdrawal = DB::table('payment')
            ->select('payment.*', 'bank_account.*', 'payment.created_at as created_at', 'the_bank.name as name_bank', 'the_bank.short_name as short_name_bank', 'bank_account.bank_name as bank_name')
            ->join('bank_account', 'payment.account', '=', 'bank_account.id')
            ->join('the_bank', 'bank_account.bank_name', '=', 'the_bank.id')
            ->where('bank_account.customer_id', $dataUser->customer_id)
            ->where('payment.type', 0)
            ->orderBy('payment.id', 'desc')
            ->get();
        $data_return = [
            'title' => 'Lịch sử rút tiền',
            'data_withdrawal' => $data_withdrawal
        ];
        return view('collaborator.financial.wallet.history_payment', $data_return);
    }

    /**
     *  Rút tiền
     * @param Request $request
     * @return array
     */
    public function createWithdrawal(Request $request){
        $rules = [
            'price' => 'required',
            'bank_id' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $dataUser = session()->get('data_collaborator');
            $dataWallet = CustomerModel::find($dataUser->customer_id);
            if (($request->price + 11000) > $dataWallet->wallet) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Số tiền không đủ để thực hiện giao dịch này.'
                ];
            } else {
                $today = Carbon::now('Asia/Ho_Chi_Minh');
                $data = new PaymentModel([
                    'title' => $request->title,
                    'account' => $request->bank_id,
                    'price' => $request->price,
                    'type' => 0,
                    'created_at' => $today
                ]);
                $dataReturn = [
                    'msg' => 'Rút tiền thành công',
                    'status' => true,
                    'url' => url('admin/collaborator_financial/wallet'),
                ];
            }
        }
        return $dataReturn;
    }

    /**
     *  Xác nhận rút tiền
     * @param Request $request
     * @return array
     */
    public function saveWithdrawal(Request $request){
        $rules = [
            'pin_code_input' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng nhập mã Pin',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $dataLogin = session()->get('data_collaborator');
            $dataCustomer = CustomerModel::find($dataLogin->customer_id);
            if(md5($request->pin_code_input) != $dataCustomer->pin_code){
                $dataReturn = [
                    'msg' => 'Mã pin không chính xác'
                ];
                return $dataReturn;
            } else {
                $today = Carbon::now('Asia/Ho_Chi_Minh');
                $dataPayment = new PaymentModel([
                    'title' => $request->title,
                    'account' => $request->bank_id,
                    'price' => $request->price,
                    'type' => 0,
                    'created_at' => $today
                ]);
                $dataPayment->save();
                $dataPrice = $dataCustomer->wallet - ($request->price + 11000);
                CustomerModel::where('id', $dataLogin->customer_id)->update(['wallet' => $dataPrice]);
                $dataReturn = [
                    'msg' => 'Rút tiền thành công',
                    'status' => true,
                    'url' => url('admin/collaborator_financial/wallet'),
                ];
                return $dataReturn;
            }
        }
    }

    /**
     *  Thêm tài khoản ngân hàng
     * @param Request $request
     * @return array
     */
    public function createBanking(Request $request){
        $dataUser = session()->get('data_collaborator');
        $rules = [
            'user_name' => 'required',
            'bank_name' => 'required',
            'bank_account' => 'required|unique:bank_account,bank_account,' . $request->id,
            'identify_card' => 'required|max:12',
            'branch' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'bank_account.unique' => 'Số tài khoản đã tồn tại',
            'identify_card.max' => 'Số CMND không được vượt quá 12 số'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $data = new BankAccountModel();
            $data->customer_id = $dataUser->customer_id;
            $data->user_name = $request->user_name;
            $data->bank_name = $request->bank_name;
            $data->bank_account = $request->bank_account;
            $data->identify_card = $request->identify_card;
            $data->branch = $request->branch;
//            $data->save();
            $dataReturn = [
                'status' => true,
                'msg' => 'Thêm tài khoản ngân hàng thành công',
                'url' => url('admin/collaborator_financial/wallet'),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Lưu tài khoản ngân hàng
     * @param Request $request
     * @return array
     */
    public function saveBankAccount(Request $request){
        $dataUser = session()->get('data_collaborator');
        $data = new BankAccountModel();
        $data->customer_id = $dataUser->customer_id;
        $data->user_name = $request->user_name;
        $data->bank_name = $request->bank_name;
        $data->bank_account = $request->bank_account;
        $data->identify_card = $request->identify_card;
        $data->branch = $request->branch;
        $data->save();
        $dataReturn = [
            'status' => true,
            'msg' => 'Thêm tài khoản ngân hàng thành công',
            'url' => url('admin/collaborator_financial/wallet'),
        ];
        return $dataReturn;
    }

    /**
     *  Yêu cầu rút tiền
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function withdrawl(){
        $dataUser = session()->get('data_collaborator');
        $dataCollaborator = CustomerModel::where('id', $dataUser->customer_id)->first();
        $dataAccount = BankAccountModel::where('customer_id', $dataUser->customer_id)->where('is_active', 1)->orderBy('id', 'desc')->get();
        foreach ($dataAccount as $k => $v){
            $bank = TheBankModel::find($v['bank_name']);
            $dataAccount[$k]['bank_name'] = $bank['name'] ?? '';
            $dataAccount[$k]['short_name'] = $bank['short_name'] ?? '';
        }

        $dataBank = TheBankModel::orderBy('id', 'desc')->get();
        $dataReturn = [
            'collaborator' => true,
            'dataBank' => $dataBank,
            'dataAccount' => $dataAccount,
            'data' => $dataCollaborator,
            'title' => 'Yêu cầu rút tiền',
        ];
        return view('collaborator.financial.wallet.withdrawl', $dataReturn);
    }

    /**
     *  Rút tiền
     * @param Request $request
     * @return array
     */
    public function createWithdrawl(Request $request){
        $rules = [
            'price' => 'required',
            'title' => 'required',
            'bank_account' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $today = Carbon::now('Asia/Ho_Chi_Minh');
            $dataSave = new CustomerModel();
            $dataUser = session()->get('data_collaborator');
            $dataWallet = CustomerModel::where('id', $dataUser->customer_id)->first();
            if ($request->price > $dataWallet->wallet){
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Số tiền không đủ để thực hiện giao dịch này.'
                ];
            } else {
                $data = new PaymentModel([
                    'price' => $request->price,
                    'title' => $request->title,
                    'account' => $request->bank_account,
                    'type' => 0,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
                $data->save();
                $dataSave->wallet = $dataWallet->wallet - $request->price;
                CustomerModel::where('id', $dataUser->customer_id)->update(['wallet' => $dataSave->wallet]);
                CustomerModel::where('id', $dataUser->customer_id)->update(['created_at' => $today]);
                $dataReturn = [
                    'status' => true,
                    'url' => url('admin/collaborator_financial/wallet'),
                ];
            }
        }
        return $dataReturn;
    }

    /**
     *  Lịch sử điểm
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function historyPoint(Request $request){
        $dataUser = session()->get('data_collaborator');
        $data_minus = PointModel::where('customer_id', $dataUser->customer_id)
            ->where('type', 0)
            ->orderBy('id', 'desc')
            ->get();
        foreach ($data_minus as $k => $v) {
            $dataCollaborator = CustomerModel::find($v['customer_id']);
            $data_minus[$k]['name'] = $dataCollaborator['name'] ?? '';
        }
        $data_return = [
            'title' => 'Lịch sử điểm',
            'data_minus' => $data_minus,
        ];
        return view('collaborator.financial.wallet.history_point', $data_return);
    }

    /**
     *  Đổi điểm
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function changePoint(){
        $dataUser = session()->get('data_collaborator');
        $data = CustomerModel::where('id', $dataUser->customer_id)->first();
        $dataReturn = [
            'collaborator' => true,
            'data' => $data,
            'title' => 'Đổi điểm',
        ];
        return view('collaborator.financial.wallet.change_point', $dataReturn);
    }

    /**
     *  Đổi điểm
     * @param Request $request
     * @return array
     */
    public function savePoint(Request $request){
        $rules = [
            'point' => 'required',
            'title' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $today = Carbon::now('Asia/Ho_Chi_Minh');
            $dataUser = session()->get('data_collaborator');
            $dataPoint = CustomerModel::where('id', $dataUser->customer_id)->first();
            if ($request->point > $dataPoint->point){
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Số điểm của bạn không đủ để thực hiện giao dịch này.'
                ];
            } else {
                $data = new PointModel([
                'point' => $request->point,
                'title' => $request->title,
                'customer_id' => $dataUser->customer_id,
                'type' => 0,
                'active' => 1,
                'date' => $today
                 ]);
                $data->save();
                $data->point = $dataPoint->point - $request->point;
                CustomerModel::where('id', $dataUser->customer_id)->update(['point' => $data->point]);
                $change = $dataPoint->wallet + ($request->point * 1000);
                CustomerModel::where('id', $dataUser->customer_id)->update(['wallet' => $change]);
                $dataReturn = [
                    'status' => true,
                    'url' => url('admin/financial/wallet'),
                ];

            }
        }
        return $dataReturn;
    }

    /**
     *  Lịch sử xu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function historyCoin(){
        $dataReturn = [
            'title' => 'Lịch sử xu'
        ];
        return view('collaborator.financial.wallet.history_coin', $dataReturn);
    }

    /**
     *  Đổi xu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function changeCoin(){
        $dataUser = session()->get('data_collaborator');
        $data = CustomerModel::where('id', $dataUser->customer_id)->first();
        $dataReturn = [
            'collaborator' => true,
            'data' => $data,
            'title' => 'Đổi xu',
        ];
        return view('collaborator.financial.wallet.change_coin', $dataReturn);
    }

    /**
     *  Xuất báo cáo lịch sử rút tiền
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new HistoryPaymentExport(), 'History_Payment.xlsx');
    }

    /**
     *  Xuất báo cáo lịch sử điểm
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportPoint()
    {
        return Excel::download(new HistoryPaymentPointExport(), 'History_Payment_Point.xlsx');
    }

    /**
     *  Thiết lập thanh toán
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexPaymentSetting(){
        $dataReturn = [
            'title' => 'Doanh thu | Thiết lập thanh toán'
        ];
        return view('collaborator.financial.payment_setting.index', $dataReturn);
    }

    /**
     *  Thiết lập mã Pin
     * @param Request $request
     * @return array
     */
    public function createPinCode(Request $request){
        $rules = [
            'pin_code' => 'required|min:6|max:6',
            're_pin_code' => 'required|same:pin_code',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'min' => 'Mã pin gồm 6 chữ số',
            'max' => 'Mã pin gồm 6 chữ số ',
            're_pin_code.same' => 'Mã pin không trùng khớp'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $dataUser = session()->get('data_collaborator');
            $dataPoint = CustomerModel::find($dataUser->customer_id);
            $dataPoint->pin_code = md5($request->pin_code);
            $dataReturn = [
                'status' => true,
                'msg' => 'Thiết lập mã pin thành công',
                'url' => url('admin/collaborator_financial/payment_setting'),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Lưu mã Pin
     * @param Request $request
     * @return array
     */
    public function savePinCode(Request $request){
        $dataUser = session()->get('data_collaborator');
        $data = CustomerModel::find($dataUser->customer_id);
        $data->pin_code = md5($request->pin_code);
        $data->save();
        $dataReturn = [
            'status' => true,
            'msg' => 'Thiết lập mã pin thành công',
            'url' => url('admin/collaborator_financial/payment_setting'),
        ];
        return $dataReturn;
    }

    /**
     *  Doanh thu
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     *
     */
    public function indexRevenue(Request $request){
        $dataUser = session()->get('data_collaborator');
        $dataReceived = DB::table('financial_revenue')
            ->distinct('order_detail.shop_id')
            ->select('order.*', 'order.total_price as total_price', 'financial_revenue.status as status')
            ->join('order', 'financial_revenue.order_id', '=', 'order.id')
            ->join('order_detail', 'order.id', '=', 'order_detail.order_id')
            ->join('shop', 'order_detail.shop_id', '=', 'shop.id')
            ->where('order.status', '=', 4)
            ->where('shop.customer_id', '=', $dataUser['customer_id'])
            ->orderBy('order.id', 'desc')
            ->get();
        if ($dataReceived) {
            foreach ($dataReceived as $k => $v) {
                if ($v->status == 0) {
                    $dataReceived[$k]->status = 'Đang xử lý';
                    $dataReceived[$k]->color = 'warning';
                } else if ($v->status == 1) {
                    $dataReceived[$k]->status = 'Hoàn thành';
                    $dataReceived[$k]->color = 'success';
                } else {
                    $dataReceived[$k]->status = 'Đã hủy';
                    $dataReceived[$k]->color = 'danger';
                }
            }
        }
        $dataBankAccount = BankAccountModel::where('customer_id', $dataUser['customer_id'])->orderBy('id', 'desc')->get();
        if ($dataBankAccount) {
            foreach ($dataBankAccount as $k => $v) {
                $dataTheBank = TheBankModel::find($v->bank_name);
                $dataBankAccount[$k]->short_name = $dataTheBank->short_name ?? '';
                $dataBankAccount[$k]->bank_name = $dataTheBank->name ?? '';
                $str = $v->bank_account;
                $arr = substr($str, -6, 6);
                $dataBankAccount[$k]->sub_bank_account = $arr;
            }
        }

        //Đếm doanh thu đã thanh toán
        $countRevenueDonePayment = HistoryOrderPaymentCollaboratorModel::where('customer_id', $dataUser['customer_id'])
            ->where('type', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        if($countRevenueDonePayment){
            $countRevenueDonePayment->countDone = 0;
            foreach($countRevenueDonePayment as $key_done => $value_done){
                $countRevenueDonePayment->countDone += $value_done->price ?? '';
            }
        }
        //Đếm doanh thu sẽ thanh toán
        $countRevenueWaitingPayment = OrderModel::where('status', '<=', 3)
            ->where('is_selected', 1)
            ->where('status_confirm', 0)
            ->where('collaborator_id', 1)
            ->where('customer_id', $dataUser['customer_id'])
            ->get();
        if($countRevenueWaitingPayment){
            $countRevenueWaitingPayment->countWaiting = 0;
            foreach ($countRevenueWaitingPayment as $key_waiting => $value_waiting){
                $countRevenueWaitingPayment->countWaiting += $value_waiting->bonus ?? '';
            }
        }
        $dataCustomer = CustomerModel::find($dataUser['customer_id']);
        $dataReturn = [
            'dataReceived' => $dataReceived,
            'title' => 'Theo dõi tài chính | Doanh thu',
            'dataBankAccount' => $dataBankAccount,
            'dataCustomer' => $dataCustomer,
            'countRevenueDonePayment' => $countRevenueDonePayment,
            'countRevenueWaitingPayment' => $countRevenueWaitingPayment
        ];
        return view('collaborator.financial.revenue.index', $dataReturn);
    }

    /**
     *  Rút tiền từ đơn hàng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getWithdrawal(){
        $dataUser = session()->get('data_collaborator');
        $dataWithdrawal = DB::table('financial_withdrawal')
            ->distinct('order_detail.shop_id')
            ->select('order.*', 'order.total_price as total_price', 'financial_withdrawal.status as status')
            ->join('order', 'financial_withdrawal.order_id', '=', 'order.id')
            ->join('order_detail', 'order.id', '=', 'order_detail.order_id')
            ->join('shop', 'order_detail.shop_id', '=', 'shop.id')
            ->where('order.status', '=', 4)
            ->where('shop.customer_id', '=', $dataUser->customer_id)
            ->orderBy('order.id', 'desc')
            ->get();
        if ($dataWithdrawal) {
            foreach ($dataWithdrawal as $k => $v) {
                if ($v->status == 0) {
                    $dataWithdrawal[$k]->status = 'Đang xử lý';
                    $dataWithdrawal[$k]->color = 'warning';
                } else if ($v->status == 1) {
                    $dataWithdrawal[$k]->status = 'Hoàn thành';
                    $dataWithdrawal[$k]->color = 'success';
                } else {
                    $dataWithdrawal[$k]->status = 'Đã hủy';
                    $dataWithdrawal[$k]->color = 'danger';
                }
            }
        }
        $dataReturn = [
            'dataWithdrawal' => $dataWithdrawal,
            'title' => 'Theo dõi tài chính | Rút tiền'
        ];
        return view('collaborator.financial.revenue.withdrawal', $dataReturn);
    }

    /**
     * Hoàn tiền từ đơn hàng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     *
     */
    public function getRefund(){
        $dataUser = session()->get('data_collaborator');
        $dataRefund = DB::table('financial_refund')
            ->distinct('order_detail.shop_id')
            ->select('order.*', 'order.total_price as total_price', 'financial_refund.status as status')
            ->join('order', 'financial_refund.order_id', '=', 'order.id')
            ->join('order_detail', 'order.id', '=', 'order_detail.order_id')
            ->join('shop', 'order_detail.shop_id', '=', 'shop.id')
            ->where('order.status', '=', 5)
            ->where('shop.customer_id', '=', $dataUser->customer_id)
            ->orderBy('order.id', 'desc')
            ->get();
        if ($dataRefund) {
            foreach ($dataRefund as $k => $v) {
                if ($v->status == 0) {
                    $dataRefund[$k]->status = 'Đang xử lý';
                    $dataRefund[$k]->color = 'warning';
                } else if ($v->status == 1) {
                    $dataRefund[$k]->status = 'Hoàn thành';
                    $dataRefund[$k]->color = 'success';
                } else {
                    $dataRefund[$k]->status = 'Đã hủy';
                    $dataRefund[$k]->color = 'danger';
                }
            }
        }
        $dataReturn = [
            'dataRefund' => $dataRefund,
            'title' => 'Theo dõi tài chính | Hoàn tiền từ đơn hàng'
        ];
        return view('collaborator.financial.revenue.refund', $dataReturn);
    }

    /**
     *  Doanh thu đã thanh toán
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function filterDone(){
        $dataUser = session()->get('data_collaborator');
        $data = HistoryOrderPaymentCollaboratorModel::where('customer_id', $dataUser->customer_id)
            ->where('type', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        if($data){
            foreach ($data as $k => $v){
                $data[$k]->type_order = 'Hoa hồng';
                $data[$k]->color = 'bg-red';
                $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->order_id)->first();
                $data[$k]->name_customer = $dataCustomerCTV->name ?? '';
                $dataOrder = OrderModel::find($v->order_id);
                $dataOrderDetail = OrderDetailModel::where('order_id', $dataOrder['id'])->first();
                $dataProduct = ProductModel::find($dataOrderDetail['product_id']);
                $data[$k]->name_product = $dataProduct->name ?? '';
                $data[$k]->image_product = $dataProduct->image ?? '';
            }
        }
        $dataReturn = [
            'title' => 'Theo dõi tài chính | Doanh thu',
            'data' => $data
        ];
        return view('collaborator.financial.revenue.done_payment', $dataReturn);
    }

    /**
     *  Doanh thu sẽ thanh toán
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function filterWaiting(){
        $dataUser = session()->get('data_collaborator');
        $data = OrderModel::where('customer_id', $dataUser->customer_id)
            ->where('collaborator_id', 1)
            ->where('is_selected', 1)
            ->where('status_confirm', 0)
            ->where('status', '<=', 3)
            ->orderBy('created_at', 'desc')
            ->get();
        if($data){
            foreach ($data as $k => $v){
                $data[$k]->type_order = 'Hoa hồng';
                $data[$k]->color = 'bg-red';
                $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                $data[$k]->name_customer = $dataCustomerCTV->name ?? '';
                $dataOrderDetail = OrderDetailModel::where('order_id', $v->id)->first();
                $dataProduct = ProductModel::find($dataOrderDetail->product_id);
                $data[$k]->name_product = $dataProduct->name ?? '';
                $data[$k]->image_product = $dataProduct->image ?? '';
                $data[$k]->price = $v->bonus ?? '';
            }
        }
        $dataReturn = [
            'title' => 'Theo dõi tài chính | Doanh thu',
            'data' => $data,
        ];
        return view('collaborator.financial.revenue.waiting_payment', $dataReturn);
    }

    /**
     *  Lọc theo ngày | Doanh thu đã thanh toán
     * @param Request $request
     */
    public function filterDateDonePayment(Request $request){
        $time_start = $request->time_start;
        $time_end = $request->time_end;
        $dataUser = session()->get('data_collaborator');
        $output = '';
        if ($request->time_start != null || $request->time_end != null) {
            $data =  HistoryOrderPaymentCollaboratorModel::where('customer_id', $dataUser->customer_id)
                ->where('type', 1)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->orderBy('created_at', 'desc')
                ->get();
            if($data){
                foreach ($data as $k => $v){
                    $data[$k]->type_order = 'Hoa hồng';
                    $data[$k]->color = 'bg-red';
                    $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->order_id)->first();
                    $data[$k]->name_customer = $dataCustomerCTV->name ?? '';
                    $dataOrder = OrderModel::find($v->order_id);
                    $dataOrderDetail = OrderDetailModel::where('order_id', $dataOrder['id'])->first();
                    $dataProduct = ProductModel::find($dataOrderDetail['product_id']);
                    $data[$k]->name_product = $dataProduct->name ?? '';
                    $data[$k]->image_product = $dataProduct->image ?? '';
                }
            }
        }
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $output .= '
                                <tr class="text-center">
                                    <td>
                                        <span class="'.$row->color.'">'.$row->type_order.'</span>
                                    </td>
                                    <td>
                                         <div class="d-flex align-items-center justify-content-center">
                                            <img style="width: 60px; height: 60px; object-fit: cover; border-radius: 14px;"
                                                 src="'.$row->image_product.'" class="image-preview">
                                            <span class="text-over">'.$row->name_product.'</span>
                                         </div>
                                    </td>
                                    <td>'.$row->name_customer.'</td>
                                    <td>
                                        <span>'.$row->created_at.'</span>
                                        <br>
                                    </td>
                                    <td>Đã nhận được hàng</td>
                                    <td>
                                        <span>'.number_format($row->price).'đ</span>
                                        <br>
                                    </td>
                                </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td colspan="5">Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Lọc theo ngày | Doanh thu sẽ thanh toán
     * @param Request $request
     */
    public function filterDateWaitingPayment(Request $request){
        $time_start = $request->time_start;
        $time_end = $request->time_end;
        $dataUser = session()->get('data_collaborator');
        $output = '';
        if ($time_start == !null || $time_end == !null) {
            $data =  OrderModel::where('customer_id', $dataUser->customer_id)
                ->where('collaborator_id', 1)
                ->where('is_selected', 1)
                ->where('status_confirm', 0)
                ->where('status', '<=', 3)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->orderBy('created_at', 'desc')
                ->get();
            if($data){
                foreach ($data as $k => $v){
                    $data[$k]->type_order = 'Hoa hồng';
                    $data[$k]->color = 'bg-red';
                    $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                    $data[$k]->name_customer = $dataCustomerCTV->name ?? '';
                    $dataOrderDetail = OrderDetailModel::where('order_id', $v->id)->first();
                    $dataProduct = ProductModel::find($dataOrderDetail->product_id);
                    $data[$k]->name_product = $dataProduct->name ?? '';
                    $data[$k]->image_product = $dataProduct->image ?? '';
                    $data[$k]->price = $v->bonus ?? '';
                }
            }
        }
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $output .= '
                                <tr class="text-center">
                                    <td>
                                        <span class="'.$row->color.'">'.$row->type_order.'</span>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center justify-content-center">
                                            <img style="width: 60px; height: 60px; object-fit: cover; border-radius: 14px;"
                                                 src="'.$row->image_product.'" class="image-preview">
                                            <span class="text-over">'.$row->name_product.'</span>
                                         </div>
                                    </td>
                                    <td>'.$row->name_customer.'</td>
                                    <td>
                                        <span>'.$row->created_at.'</span>
                                        <br>
                                    </td>
                                    <td>Chờ người mua xác nhận đã nhận hàng</td>
                                    <td>
                                        <span>'.number_format($row->bonus).'đ</span>
                                        <br>
                                    </td>
                                </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td colspan="5">Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Lọc theo trạng thái | Đã thanh toán
     * @param $id
     */
    public function filterStatusDone($id){
        $dataUser = session()->get('data_collaborator');
        $output = '';
        if ($id == 'all') {
            $dataReceived = OrderDetailModel::where('status_confirm', 1)
                ->where('customer_id', $dataUser->customer_id)
                ->orderBy('id', 'desc')
                ->get();
            if($dataReceived){
                foreach ($dataReceived as $k => $v){
                    $dataProduct = ProductModel::find($v->product_id);
                    $dataReceived[$k]->name_product = $dataProduct->name ?? '';
                    $dataReceived[$k]->image_prd = $dataProduct->image ?? '';
                    $dataCustomer = CustomerModel::where('id', $v->customer_id)->first();
                    $dataOrder = OrderModel::find($v->order_id);
                    if($dataOrder->collaborator_id == 1){
                        $dataReceived[$k]->type_order = 'Hoa hồng' ?? '';
                        $dataReceived[$k]->color = 'bg-red' ?? '';
                        $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $dataOrder->id)->first();
                        $dataReceived[$k]->name_customer = $dataCustomerCTV->name ?? '';
                    } else {
                        $dataReceived[$k]->type_order = 'Bán hàng' ?? '';
                        $dataReceived[$k]->color = 'bg-blue' ?? '';
                        $dataReceived[$k]->name_customer = $dataCustomer->name ?? '';
                    }
                }
            }
        } else if($id == 1) {
            $dataReceived = DB::table('order_detail')
                ->select('order_detail.product_id as product_id', 'order_detail.*')
                ->join('order', 'order_detail.order_id', '=', 'order.id')
                ->where('order_detail.customer_id', $dataUser->customer_id)
                ->where('order.collaborator_id', 1)
                ->where('order_detail.status_confirm', 1)
                ->get();
            if($dataReceived){
                foreach ($dataReceived as $k => $v){
                    $dataProduct = ProductModel::find($v->product_id);
                    $dataReceived[$k]->name_product = $dataProduct->name ?? '';
                    $dataReceived[$k]->image_prd = $dataProduct->image ?? '';
                    $dataReceived[$k]->type_order = 'Hoa hồng';
                    $dataReceived[$k]->color = 'bg-red';
                    $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                    $dataReceived[$k]->name_customer = $dataCustomerCTV->name ?? '';
                }
            }
        } else {
            $dataReceived = DB::table('order_detail')
                ->select('order_detail.product_id as product_id', 'order_detail.*')
                ->join('order', 'order_detail.order_id', '=', 'order.id')
                ->where('order_detail.customer_id', $dataUser->customer_id)
                ->where('order.collaborator_id', '!=', 1)
                ->where('order_detail.status_confirm', 1)
                ->get();
            if($dataReceived){
                foreach ($dataReceived as $k => $v){
                    $dataProduct = ProductModel::find($v->product_id);
                    $dataCustomer = CustomerModel::find($v->customer_id);
                    $dataReceived[$k]->name_product = $dataProduct->name ?? '';
                    $dataReceived[$k]->image_prd = $dataProduct->image ?? '';
                    $dataReceived[$k]->type_order = 'Bán hàng';
                    $dataReceived[$k]->color = 'bg-blue';
                    $dataReceived[$k]->name_customer = $dataCustomer->name ?? '';
                }
            }
        }

        $total_row = $dataReceived->count();
        if ($total_row > 0) {
            foreach ($dataReceived as $k => $row) {
                $output .= '
                                <tr class="text-center">
                                    <td>
                                        <span class="'.$row->color.'">'.$row->type_order.'</span>
                                    </td>
                                    <td>
                                       <div class="d-flex align-items-center justify-content-center">
                                            <img style="width: 60px; height: 60px; object-fit: cover; border-radius: 14px;"
                                                 src="'.$row->image_prd.'" class="image-preview">
                                            <span class="text-over">'.$row->name_product.'</span>
                                         </div>
                                    </td>
                                    <td>'.$row->name_customer.'</td>
                                    <td>
                                        <span>'.$row->updated_at.'</span>
                                        <br>
                                    </td>
                                    <td>Đã nhận được hàng</td>
                                    <td>
                                        <span>'.number_format($row->total_price).'đ</span>
                                        <br>
                                    </td>
                                </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td colspan="5">Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Lọc theo trạng thái | Sẽ thanh toán
     * @param $id
     */
    public function filterStatusWaiting($id){
        $dataUser = session()->get('data_collaborator');
        $output = '';
        if ($id == 'all') {
            $dataReceived = OrderModel::where('status_confirm', 0)
                ->where('customer_id', $dataUser->customer_id)
                ->orderBy('id', 'desc')
                ->get();
            if($dataReceived){
                foreach ($dataReceived as $k => $v){
                    $dataProduct = ProductModel::find($v->product_id);
                    $dataReceived[$k]->name_product = $dataProduct->name ?? '';
                    $dataReceived[$k]->image_prd = $dataProduct->image ?? '';
                    $dataCustomer = CustomerModel::where('id', $v->customer_id)->first();
                    $dataOrder = OrderModel::find($v->order_id);
                    if($dataOrder->collaborator_id == 1){
                        $dataReceived[$k]->type_order = 'Hoa hồng' ?? '';
                        $dataReceived[$k]->color = 'bg-red' ?? '';
                        $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $dataOrder->id)->first();
                        $dataReceived[$k]->name_customer = $dataCustomerCTV->name ?? '';
                    } else {
                        $dataReceived[$k]->type_order = 'Bán hàng' ?? '';
                        $dataReceived[$k]->color = 'bg-blue' ?? '';
                        $dataReceived[$k]->name_customer = $dataCustomer->name ?? '';
                    }
                }
            }
        } else if($id == 1) {
            $dataReceived = DB::table('order_detail')
                ->select('order_detail.product_id as product_id', 'order_detail.*')
                ->join('order', 'order_detail.order_id', '=', 'order.id')
                ->where('order_detail.customer_id', $dataUser->customer_id)
                ->where('order.collaborator_id', 1)
                ->where('order_detail.status_confirm', 0)
                ->get();
            if($dataReceived){
                foreach ($dataReceived as $k => $v){
                    $dataProduct = ProductModel::find($v->product_id);
                    $dataReceived[$k]->name_product = $dataProduct->name ?? '';
                    $dataReceived[$k]->image_prd = $dataProduct->image ?? '';
                    $dataReceived[$k]->type_order = 'Hoa hồng';
                    $dataReceived[$k]->color = 'bg-red';
                    $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                    $dataReceived[$k]->name_customer = $dataCustomerCTV->name ?? '';
                }
            }
        } else {
            $dataReceived = DB::table('order_detail')
                ->select('order_detail.product_id as product_id', 'order_detail.*')
                ->join('order', 'order_detail.order_id', '=', 'order.id')
                ->where('order_detail.customer_id', $dataUser->customer_id)
                ->where('order.collaborator_id', '!=', 1)
                ->where('order_detail.status_confirm', 0)
                ->get();
            if($dataReceived){
                foreach ($dataReceived as $k => $v){
                    $dataProduct = ProductModel::find($v->product_id);
                    $dataCustomer = CustomerModel::find($v->customer_id);
                    $dataReceived[$k]->name_product = $dataProduct->name ?? '';
                    $dataReceived[$k]->image_prd = $dataProduct->image ?? '';
                    $dataReceived[$k]->type_order = 'Bán hàng';
                    $dataReceived[$k]->color = 'bg-blue';
                    $dataReceived[$k]->name_customer = $dataCustomer->name ?? '';
                }
            }
        }

        $total_row = $dataReceived->count();
        if ($total_row > 0) {
            foreach ($dataReceived as $k => $row) {
                $output .= '
                                <tr class="text-center">
                                    <td>
                                        <span class="'.$row->color.'">'.$row->type_order.'</span>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center justify-content-center">
                                            <img style="width: 60px; height: 60px; object-fit: cover; border-radius: 14px;"
                                                 src="'.$row->image_prd.'" class="image-preview">
                                            <span class="text-over">'.$row->name_product.'</span>
                                         </div>
                                    </td>
                                    <td>'.$row->name_customer.'</td>
                                    <td>
                                        <span>'.$row->updated_at.'</span>
                                        <br>
                                    </td>
                                    <td>Chờ người mua xác nhận đã nhận hàng</td>
                                    <td>
                                        <span>'.number_format($row->total_price).'đ</span>
                                        <br>
                                    </td>
                                </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td colspan="5">Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Xuất file | Doanh thu đã thanh toán
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportRevenueDonePayment(){
        return Excel::download(new RevenueDonePaymentExport(), 'Revenue_Done_Payment.xlsx');
    }

    /**
     *  Xuất file | Doanh thu sẽ thanh toán
     */
    public function exportRevenueWaitingPayment(){
        return Excel::download(new RevenueWaitingPaymentExport(), 'Revenue_Waiting_Payment.xlsx');
    }

    /**
     *  Tất cả
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function statusAll()
    {
        $data_login = session()->get('data_collaborator');
        $data_history_order_payment = HistoryOrderPaymentCollaboratorModel::where('customer_id', $data_login->customer_id)->orderBy('id', 'desc')->get();
        foreach ($data_history_order_payment as $key => $value) {
            $data_history_order_payment[$key]['date'] = date('d-m-Y', strtotime($value->created_at));
            $data_order = OrderModel::find($value->order_id);
            $data_order_detail = OrderDetailModel::where('order_id', $data_order['id'])->first();
            $data_product = ProductModel::find($data_order_detail['product_id']);
            $data_history_order_payment[$key]['name_product_order'] = $data_product['name'];
            if ($value['type'] == 0) {
                $data_history_order_payment[$key]['is_type'] = 'Doanh thu';
            } elseif ($value['type'] == 1) {
                $data_history_order_payment[$key]['is_type'] = 'Hoa hồng';
            } elseif ($value['type'] == 2) {
                $data_history_order_payment[$key]['is_type'] = 'Hoàn tiền';
            } else {
                $data_history_order_payment[$key]['is_type'] = 'Hoa hồng giới thiệu';
            }
        }
        $dataReturn = [
            'title' => 'Ví',
            'data_history' => $data_history_order_payment
        ];
        return view('collaborator.financial.wallet.status_all', $dataReturn);
    }

    /**
     *  Doanh thu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function statusRevenue()
    {
        $data_login = session()->get('data_collaborator');
        $data_history_order_payment = HistoryOrderPaymentCollaboratorModel::where('customer_id', $data_login->customer_id)->where('type', 0)->orderBy('id', 'desc')->get();
        foreach ($data_history_order_payment as $key => $value) {
            $data_history_order_payment[$key]['date'] = date('d-m-Y', strtotime($value->created_at));
            $data_order = OrderModel::find($value->order_id);
            $data_order_detail = OrderDetailModel::where('order_id', $data_order['id'])->first();
            $data_product = ProductModel::find($data_order_detail['product_id']);
            $data_history_order_payment[$key]['name_product_order'] = $data_product['name'];
            if ($value['type'] == 0) {
                $data_history_order_payment[$key]['is_type'] = 'Doanh thu';
            } elseif ($value['type'] == 1) {
                $data_history_order_payment[$key]['is_type'] = 'Rút tiền';
            } elseif ($value['type'] == 2) {
                $data_history_order_payment[$key]['is_type'] = 'Hoàn tiền';
            } else {
                $data_history_order_payment[$key]['is_type'] = 'Hoa hồng giới thiệu';
            }
        }
        $dataReturn = [
            'title' => 'Ví',
            'data_history' => $data_history_order_payment
        ];
        return view('collaborator.financial.wallet.status_revenue', $dataReturn);
    }

    /**
     *  Rút tiền
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function statusWithdrawal()
    {
        $data_login = session()->get('data_collaborator');
        $data_history_order_payment = HistoryOrderPaymentCollaboratorModel::where('customer_id', $data_login->customer_id)->where('type', 1)->orderBy('id', 'desc')->get();
        foreach ($data_history_order_payment as $key => $value) {
            $data_history_order_payment[$key]['date'] = date('d-m-Y', strtotime($value->created_at));
            $data_order = OrderModel::find($value->order_id);
            $data_order_detail = OrderDetailModel::where('order_id', $data_order->id)->first();
            $data_product = ProductModel::find($data_order_detail->product_id);
            $data_history_order_payment[$key]['name_product_order'] = $data_product->name;
        }
        $dataReturn = [
            'title' => 'Ví',
            'data_history' => $data_history_order_payment
        ];
        return view('collaborator.financial.wallet.status_withdrawal', $dataReturn);
    }

    /**
     * Hoàn tiền
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function statusRefund()
    {
        $dataReturn = [
            'title' => 'Ví',
        ];
        return view('collaborator.financial.wallet.status_refund', $dataReturn);
    }

    /**
     *  Hoa hồng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function statusBonus()
    {
        $dataReturn = [
            'title' => 'Ví',
        ];
        return view('collaborator.financial.wallet.status_bonus', $dataReturn);
    }

    /**
     *  Các loại giao dịch gần đây | Lọc theo ngày | Tất cả
     * @param Request $request
     */
    public function filterDateStatusAll(Request $request){
        $time_start = $request->time_start;
        $time_end = $request->time_end;
        $dataLogin = session()->get('data_collaborator');
        $output = '';
        if ($time_start != null || $time_end != null) {
            $data = HistoryOrderPaymentCollaboratorModel::where('customer_id', $dataLogin->customer_id)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->orderBy('id', 'desc')
                ->get();
            foreach ($data as $key => $value) {
                $data[$key]['date'] = date('d-m-Y', strtotime($value->created_at));
                $data_order = OrderModel::find($value['order_id']);
                $data_order_detail = OrderDetailModel::where('order_id', $data_order['id'])->first();
                $data_product = ProductModel::find($data_order_detail['product_id']);
                $data[$key]['name_product_order'] = $data_product['name'];
                if ($value['type'] == 0) {
                    $data[$key]['is_type'] = 'Doanh thu';
                } elseif ($value['type'] == 1) {
                    $data[$key]['is_type'] = 'Rút tiền';
                } elseif ($value['type'] == 2) {
                    $data[$key]['is_type'] = 'Hoàn tiền';
                } else {
                    $data[$key]['is_type'] = 'Hoa hồng giới thiệu';
                }
                if($value['status'] == 0){
                    $data[$key]['status'] = 'Đang xử lý';
                    $data[$key]['color'] = 'bg-orange';
                } else if($value['status'] == 1) {
                    $data[$key]['status'] = 'Hoàn thành';
                    $data[$key]['color'] = 'bg-green';
                } else {
                    $data[$key]['status'] = 'Đã hủy';
                    $data[$key]['color'] = 'bg-red';
                }
            }
        }
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $output .= '
                            <tr class="text-center">
                                <td>'.$row->date.'</td>
                                <td>
                                    <span class="title-dank">'.$row->is_type.' từ đơn hàng  #'.$row->order_code.'</span>
                                    <br>
                                    <span>'.$row->name_product_order.'</span>
                                </td>
                                <td>
                                    <div class="'.$row->color.' bg-span">'.$row->status.'</div>
                                </td>
                                <td class="title-dank">'.number_format($row->price).'đ</td>
                            </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td>Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Các loại giao dịch gần đây | Lọc theo ngày | Doanh thu
     * @param Request $request
     *
     */
    public function filterDateStatusRevenue(Request $request){
        $time_start = $request->time_start;
        $time_end = $request->time_end;
        $dataLogin = session()->get('data_collaborator');
        $output = '';
        if ($time_start != null || $time_end != null) {
            $data = HistoryOrderPaymentCollaboratorModel::where('customer_id', $dataLogin->customer_id)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->where('type', 0)
                ->orderBy('id', 'desc')
                ->get();
            foreach ($data as $key => $value) {
                $data[$key]['date'] = date('d-m-Y', strtotime($value->created_at));
                $data_order = OrderModel::find($value['order_id']);
                $data_order_detail = OrderDetailModel::where('order_id', $data_order['id'])->first();
                $data_product = ProductModel::find($data_order_detail['product_id']);
                $data[$key]['name_product_order'] = $data_product['name'];
                if ($value['type'] == 0) {
                    $data[$key]['is_type'] = 'Doanh thu';
                } elseif ($value['type'] == 1) {
                    $data[$key]['is_type'] = 'Rút tiền';
                } elseif ($value['type'] == 2) {
                    $data[$key]['is_type'] = 'Hoàn tiền';
                } else {
                    $data[$key]['is_type'] = 'Hoa hồng giới thiệu';
                }
                if($value['status'] == 0){
                    $data[$key]['status'] = 'Đang xử lý';
                    $data[$key]['color'] = 'bg-orange';
                } else if($value['status'] == 1) {
                    $data[$key]['status'] = 'Hoàn thành';
                    $data[$key]['color'] = 'bg-green';
                } else {
                    $data[$key]['status'] = 'Đã hủy';
                    $data[$key]['color'] = 'bg-red';
                }
            }
        }
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $output .= '
                            <tr class="text-center">
                                <td>'.$row->date.'</td>
                                <td>
                                    <span class="title-dank">'.$row->is_type.' từ đơn hàng  #'.$row->order_code.'</span>
                                    <br>
                                    <span>'.$row->name_product_order.'</span>
                                </td>
                                <td>
                                    <div class="'.$row->color.' bg-span">'.$row->status.'</div>
                                </td>
                                <td class="title-dank">'.number_format($row->price).'đ</td>
                            </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td>Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Các loại giao dịch gần đây | Lọc theo ngày | Rút tiền
     * @param Request $request
     */
    public function filterDateStatusWithdrawal(Request $request){
        $time_start = $request->time_start;
        $time_end = $request->time_end;
        $dataLogin = session()->get('data_collaborator');
        $output = '';
        if ($time_start != null || $time_end != null) {
            $data = HistoryOrderPaymentCollaboratorModel::where('customer_id', $dataLogin->customer_id)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->where('type', 1)
                ->orderBy('id', 'desc')
                ->get();
            foreach ($data as $key => $value) {
                $data[$key]['date'] = date('d-m-Y', strtotime($value->created_at));
                $data_order = OrderModel::find($value['order_id']);
                $data_order_detail = OrderDetailModel::where('order_id', $data_order['id'])->first();
                $data_product = ProductModel::find($data_order_detail['product_id']);
                $data[$key]['name_product_order'] = $data_product['name'];
                if ($value['type'] == 0) {
                    $data[$key]['is_type'] = 'Doanh thu';
                } elseif ($value['type'] == 1) {
                    $data[$key]['is_type'] = 'Rút tiền';
                } elseif ($value['type'] == 2) {
                    $data[$key]['is_type'] = 'Hoàn tiền';
                } else {
                    $data[$key]['is_type'] = 'Hoa hồng giới thiệu';
                }
                if($value['status'] == 0){
                    $data[$key]['status'] = 'Đang xử lý';
                    $data[$key]['color'] = 'bg-orange';
                } else if($value['status'] == 1) {
                    $data[$key]['status'] = 'Hoàn thành';
                    $data[$key]['color'] = 'bg-green';
                } else {
                    $data[$key]['status'] = 'Đã hủy';
                    $data[$key]['color'] = 'bg-red';
                }
            }
        }
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $output .= '
                            <tr class="text-center">
                                <td>'.$row->date.'</td>
                                <td>
                                    <span class="title-dank">'.$row->is_type.' từ đơn hàng  #'.$row->order_code.'</span>
                                    <br>
                                    <span>'.$row->name_product_order.'</span>
                                </td>
                                <td>
                                    <div class="'.$row->color.' bg-span">'.$row->status.'</div>
                                </td>
                                <td class="title-dank">'.number_format($row->price).'đ</td>
                            </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td>Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }
}
