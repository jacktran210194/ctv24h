<?php

namespace App\Http\Controllers\Admin\Collaborator;

use App\Http\Controllers\Controller;
use App\Models\CollaboratorModel;
use Illuminate\Http\Request;

class CollaboratorShopController extends Controller
{
    public function ratingShop(){
        return view('collaborator.shop.ratings_shop');
    }
    public function fileShop(){
        return view('collaborator.shop.file_shop');
    }
    public function decorateShop(){
        return view('collaborator.shop.decorate_shop');
    }
    public function decorateShopMobile(){
        return view('collaborator.shop.decorate_shop_mobile');
    }
    public function decorateShopDesktop(){
        return view('collaborator.shop.decorate_shop_desktop');
    }
    public function updateCTV(){
        $data_customer = session()->get('data_collaborator');
        $data_return = [
          'data_customer' => $data_customer
        ];
        return view('collaborator.shop.update_ctv', $data_return);
    }
    public function decorateShopPC(){
        return view('collaborator.shop.decorate_shop_pc');
    }
}
