<?php
namespace App\Http\Controllers\Admin\Collaborator;
use App\Http\Controllers\Controller;

class MyShopController extends Controller
{
    public function myShop(){
        return view('collaborator.myShop.index');
    }
}
