<?php

namespace App\Http\Controllers\Admin\Collaborator;

use App\Http\Controllers\Admin\CollaboratorController;
use App\Http\Controllers\Controller;
use App\Models\CollaboratorModel;
use App\Models\CustomerModel;
use App\Models\PointModel;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RankCtvController extends Controller
{
    public function index()
    {
        $data_customer = session()->get('data_customer');
        $point_ctv = CustomerModel::find($data_customer->id);
        if ($point_ctv['point'] < 500){
            $title_rank = 'thành viên ';
            $rank = 'bạc';
            $point = 500 - $point_ctv['point'];
        }elseif ($point_ctv->point >= 500 && $point_ctv->point < 1000){
            $rank = 'vàng';
            $title_rank = 'bạc';
            $point = 1000 - $point_ctv->point;
        }elseif ( $point_ctv->point >= 1000 && $point_ctv->point < 1500 ){
            $rank = 'kim cương';
            $title_rank = 'vàng';
            $point = 1500 - $point_ctv->point;
        }else{
            $point = 0;
            $rank = 'kim cương';
            $title_rank = 'kim cương';
        }
        $dataReturn = [
            'title' => 'Hạng CTV',
            'point_ctv'=> $point_ctv,
            'rank' => $rank,
            'point' => $point,
            'title_rank'=> $title_rank
        ];
        return view('collaborator.rank.index', $dataReturn);
    }
    public function buyPoints( Request $request ){
        $data_customer = session()->get('data_customer');
        $pass = $data_customer->password;
        $point_ctv = CustomerModel::find($data_customer->id);
        if ( $pass == md5($request->pass) ){
            $dataReturn = [
                'status' => true,
            ];
        }else{
            $dataReturn = [
                'status' => false,
            ];
        }
        $today = Carbon::now('Asia/Ho_Chi_Minh');
        if (isset($request->point)){
            if ($request->get('check') == 1){
                $point_ctv->wallet = $point_ctv->wallet - $request->get('price_rank');
            }
            $point_ctv->point = $point_ctv->point + $request->point ?? '';
            $point_ctv->save();
            $dataHistoryPoint = new PointModel([
                'title' => 'Bạn vừa mua '.$request->point.' điểm',
                'point' => $request->point,
                'customer_id' => $data_customer->id,
                'type' => 1,
                'date' => $today
            ]);
            $dataHistoryPoint->save();
            $dataReturn = [
                'status' => true,
            ];
        }
        return $dataReturn;
    }

    public function checkRank (Request $request)
    {
        try{
            $data_customer = session()->get('data_customer');
            $customer = CustomerModel::find($data_customer->id);
            if ($request->get('price_rank') > $customer->wallet){
                $data['status'] = false;
                $data['msg'] = 'Vui lòng nạp thêm tiền vào ví CTV24H để tiếp tục';
            }else{
                $data['status'] = true;
            }
            return $data;
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }
}
