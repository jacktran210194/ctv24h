<?php

namespace App\Http\Controllers\Admin\Collaborator;

use App\Http\Controllers\Controller;
use App\Models\AttributeValueModel;
use App\Models\CategoryProductModel;
use App\Models\ProductAttributeModel;
use App\Models\ProductDropShipModel;
use App\Models\ProductModel;
use App\Models\ShopModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProductCollaboratorController extends Controller
{
    /**
     *  Danh sách sản phẩm
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexProduct()
    {
        $data_customer = session()->get('data_customer');
        $shop = ShopModel::where('customer_id', $data_customer->id)->first();
        $products_drop_ship = ProductModel::where('shop_id', $shop->id)->where('dropship', 1)->get();
        $dataReturn = [
            'title' => 'Quản lý sản phẩm đăng bán',
            'data_product_dropship' => $products_drop_ship,
        ];
        return view('collaborator.product.index', $dataReturn);
    }

    public function editProduct(Request $request)
    {
        $product_attribute = ProductAttributeModel::where('product_id', $request->id)->get();
        $product = ProductModel::find($request->id);
        $data['product'] = $product;
        $data['product_attribute'] = $product_attribute;
        $views = view('collaborator.product.edit', $data)->render();
        return response()->json(['prod' => $views]);
    }

    public function updateProduct(Request $request)
    {
        $attribute = $request->get('attribute');
        $product = ProductModel::find($request->get('product_id'));
        $product->active_drop_ship = 1;
        $product->save();
        $price_max = 0;
        $price_mim = 0;
        foreach ($attribute as $value){
            $attribute_value = AttributeValueModel::find($value['id_attr']);
            $attribute_value->price = $value['price'];
            $attribute_value->price_ctv = $value['price_ctv'];
            $attribute_value->save();
            if ($price_max == 0){
                $price_max = $value['price'];
            }
            if ($price_max < $value['price']){
                $price_max = $value['price'];
            }
            if ($price_mim == 0){
                $price_mim = $value['price'];
            }
            if ($price_mim > $value['price']){
                $price_mim = $value['price'];
            }
        }
        $product->price = $price_max;
        $product->price_discount = $price_mim;
        $product->save();
        return redirect('/admin/collaborator_product');
    }

    public function activeAndHide(Request $request)
    {
        $product = ProductModel::find($request->get('id_product'));
        if ($request->get('active') == 1){
            $product->active_drop_ship = 2;
            $product->active = 1;
            $product->save();
        }else{
            $product->active_drop_ship = 1;
            $product->active = 0;
            $product->save();
        }
        return true;
    }
}
