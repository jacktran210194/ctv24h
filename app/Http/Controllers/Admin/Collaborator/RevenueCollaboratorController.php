<?php

namespace App\Http\Controllers\Admin\Collaborator;

use App\Http\Controllers\Controller;
use App\Models\AddressCustomerCTVModel;
use App\Models\CityModel;
use App\Models\CustomerAddressModel;
use App\Models\DistrictModel;
use App\Models\FeeCTV24HModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductModel;
use App\Models\WardModel;
use Illuminate\Http\Request;
use DB;

class RevenueCollaboratorController extends Controller
{
    /**
     *  Doanh thu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){
        $dataUser = session()->get('data_collaborator');
        $data = OrderModel::where('customer_id', $dataUser->customer_id)
            ->where('is_selected', 1)
            ->get();
        foreach ($data as $k => $v){
            $dataFeeCTV24H = FeeCTV24HModel::first();
            if ($v->collaborator_id == 1){
                $data[$k]['type_order'] = 'Hoa hồng';
                $data[$k]['color'] = 'danger';
                $dataInfo = AddressCustomerCTVModel::where('customer_id', $v->customer_id)->first();
                $data[$k]->name = $dataInfo->name ? : '';
                $data[$k]['revenue'] = $v->cod - $v->total_price - (($v->cod - $v->total_price) * $dataFeeCTV24H->fee/100);
            } else {
                $data[$k]['type_order'] = 'Bán hàng';
                $data[$k]['color'] = 'primary';
                $dataInfo2 = CustomerAddressModel::where('customer_id', $v->customer_id)->where('default', 1)->first();
                $data[$k]->name = $dataInfo2->name ? : '';
                $data[$k]['revenue'] = $v->total_price - ($v->total_price * $dataFeeCTV24H->fee/100);
            }
            $dataOrderDetail = OrderDetailModel::where('order_id', $v->id)->get();
            foreach ($dataOrderDetail as $key => $value){
                $dataProduct = ProductModel::find($value->product_id);
                $data[$k]->product_name = $dataProduct->name ? : '';
                $data[$k]->image = $dataProduct->image ? : '';
                if($value->status == 3){
                    $data[$k]->status = 'Chờ người mua xác nhận đã nhận hàng';
                } else {
                    $data[$k]->status = 'Chưa thanh toán';
                }
                $data[$k]['code'] = $v->order_code;
            }
        }
        $dataReturn = [
            'title' => 'Quản lý doanh thu',
            'data' => $data
        ];
        return view('collaborator.revenue.index', $dataReturn);
    }

    /**
     *  Chi tiết
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function detail(Request $request)
    {
        $dataUser = session()->get('data_collaborator');
        $dataReceived = DB::table('order')
            ->select('order.*','order.name as name_customer', 'order.phone as phone_customer',
                'order.payment_id as payment', 'order.note as note',
                'order_detail.*',
                'product.name as name_product', 'product.*', 'product.image as image')
            ->join('order_detail', 'order.id', '=', 'order_detail.order_id')
            ->join('product', 'product.id', '=', 'order_detail.product_id')
            ->where('order.collaborator_id', '=', $dataUser['id'])
            ->where('order.id', $request->id)
            ->orderBy('order.id', 'desc')
            ->first();
        if ($dataReceived->payment_id == 0){
            $dataReceived->payment = 'Thanh toán khi nhận hàng';
        } else if ($dataReceived->payment_id == 1) {
            $dataReceived->payment = 'Thẻ tín dụng/ghi nợ';
        } else {
            $dataReceived->payment = 'Chuyển khoản';
        }

        if ($dataReceived->status == 0){
            $dataReceived->status = 'Chờ xác nhận';
        } else if ($dataReceived->status == 1) {
            $dataReceived->status = 'Chờ lấy hàng';
        } else if ($dataReceived->status == 2) {
            $dataReceived->status = 'Đang giao hàng';
        } else if ($dataReceived->status == 3) {
            $dataReceived->status = 'Đã giao hàng';
        } else if($dataReceived->status == 4) {
            $dataReceived->status = 'Đã hủy';
        } else {
            $dataReceived->status = 'Hoàn tiền';
        }

        $dataCity = CityModel::where('id', $dataReceived->city_id)->first();
        $dataReceived->city = $dataCity->name;
        $dataDistrict = DistrictModel::where('id', $dataReceived->district_id)->first();
        $dataReceived->district = $dataDistrict->name;
        $dataWard = WardModel::where('id', $dataReceived->ward_id)->first();
        $dataReceived->ward = $dataWard->name;
        $dataReceived->Adds = $dataReceived->address.', '.$dataReceived->ward.', '.$dataReceived->district.', '.$dataReceived->city;

        $dataReturn = [
            'dataReceived' => $dataReceived,
            'title' => 'Quản lý doanh thu'
        ];
        return view('collaborator.revenue.detail', $dataReturn);
    }
}
