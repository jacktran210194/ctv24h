<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProductModel;
use App\Models\SponsoredProductModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Str;

class SponsoredProductController extends Controller
{
    /**
     *  Danh sách sản phẩm được tài trợ
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $data = DB::table('sponsored_product')
            ->select('product.name as name', 'product.image as image', 'product.sku_type as sku_type', 'sponsored_product.value as value',
                'category_product_child.name as category_product', 'product.price as price', 'product.warehouse as warehouse', 'sponsored_product.id as id')
            ->join('product', 'sponsored_product.product_id', '=', 'product.id')
            ->join('category_product_child', 'product.category_id', '=', 'category_product_child.id')
            ->where('product.sponsored', '=', '1')
            ->orderBy('sponsored_product.id', 'desc')
            ->get();
        $dataReturn = [
            'data' => $data,
            'title' => 'Sản phẩm được tài trợ',
            'sponsored_product' => true
        ];
        return view('admin.sponsored_product.index', $dataReturn);
    }

    /**
     *  Thêm sản phẩm được tài trợ
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $data = ProductModel::where('sponsored', 0)->orderBy('id', 'desc')->get();
        $data_return = [
            'status' => true,
            'title' => 'Thêm sản phẩm được tài trợ',
            'data' => $data
        ];
        return view('admin.sponsored_product.form', $data_return);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $dataReturn = $this->create($request);
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     *  Thêm sản phẩm được tài trợ
     * @param $request
     * @return array
     */
    public function create($request)
    {
        $rules = [
            'product_id' => 'required',
            'value' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $data = new SponsoredProductModel();
            $data->product_id = $request->product_id;
            $data->value = $request->value;
            $data->save();
            ProductModel::where('id', $request->product_id)->update(['sponsored' => 1]);
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/product/sponsored'),
            ];
        }
        return $dataReturn;
    }

    /**
     * @param $request
     * @param $rules
     * @param $customMessages
     * @return array
     */
    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /**
     *  Xóa sản phẩm được tài trợ
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            SponsoredProductModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
