<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IntroductionController extends Controller
{
    /**
     *  Giới thiệu thành viên
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function member(){
        $dataReturn = [
            'title' => 'Giới thiệu thành viên'
        ];
        return view('supplier.introduction.member', $dataReturn);
    }

    /**
     *  Giới thiệu sản phẩm
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function product(){
        $dataReturn = [
            'title' => 'Giới thiệu sản phẩm'
        ];
        return view('supplier.introduction.product', $dataReturn);
    }
}
