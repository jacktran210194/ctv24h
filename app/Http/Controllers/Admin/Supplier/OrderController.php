<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Exports\Supplier\OrderSupplierExport;
use App\Http\Controllers\Controller;
use App\Models\AddressCustomerCTVModel;
use App\Models\AddressSupplierModel;
use App\Models\AttributeValueModel;
use App\Models\CityModel;
use App\Models\CustomerAddressModel;
use App\Models\CustomerModel;
use App\Models\DistrictModel;
use App\Models\FeeCTV24HModel;
use App\Models\HistoryDealCTV24;
use App\Models\HistoryOrderPaymentCollaboratorModel;
use App\Models\HistoryOrderPaymentModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductAttributeModel;
use App\Models\ProductModel;
use App\Models\ReviewProductModel;
use App\Models\ShippingModel;
use App\Models\ShopModel;
use App\Models\WardModel;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Facades\Excel;

class OrderController extends Controller
{
    public function __construct(Request $request)
    {
        if (!$request->session()->has('data_supplier')) {
            Redirect::to('login')->send();
        }
    }

    public function index()
    {
        $data_login = session()->get('data_supplier');
        $data_address_supplier = AddressSupplierModel::where('type_pick_up', 1)->orderBy('id', 'desc')->where('supplier_id', $data_login->id)->first();
        $data_address = AddressSupplierModel::where('supplier_id', $data_login->id)->where('type_pick_up', 0)->get();
        $dataReturn = [
            'status' => true,
            'title' => 'Đơn hàng',
            'address' => $data_address,
            'data_address' => $data_address_supplier,
        ];
        return view('supplier.order.index', $dataReturn);
    }

    /**
     *
     * chi tiết đơn hàng
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     *
     */
    public function detail($id)
    {
        $fee = FeeCTV24HModel::first();
        $data_order = OrderModel::find($id);
        $data_order['date'] = date('d-m-Y H:i:s', strtotime($data_order['date']));
        $data_customer = CustomerModel::find($data_order->customer_id);
        if ($data_order->collaborator_id == 1) {
            $data_address = AddressCustomerCTVModel::where('order_id', $id)->first();
            if($data_address){
                $data_order['name_shipping'] = $data_address->name;
                $data_order['phone_shipping'] = $data_address->phone;
                $data_city = CityModel::find($data_address->city);
                $data_district = DistrictModel::find($data_address->district);
                $data_ward = WardModel::find($data_address->ward);
                $data_order->address_shipping = $data_address->address . ', ' . $data_ward->name . ', ' . $data_district->name . ', ' . $data_city->name;
            }else{
                $data_address = CustomerAddressModel::where('customer_id', $data_customer->id)->where('default', 1)->first();
                if ($data_address) {
                    $data_order['name_shipping'] = $data_address->name;
                    $data_order['phone_shipping'] = $data_address->phone;
                    $data_city = CityModel::find($data_address->city_id);
                    $data_district = DistrictModel::find($data_address->district_id);
                    $data_ward = WardModel::where('ward_id', $data_address->ward_id)->first();
                    $data_order->address_shipping = $data_address->address . ', ' . $data_ward->name . ', ' . $data_district->name . ', ' . $data_city->name;
                }
            }

        } else {
            $data_address = CustomerAddressModel::where('customer_id', $data_customer->id)->where('default', 1)->first();
            if ($data_address) {
                $data_order['name_shipping'] = $data_address->name;
                $data_order['phone_shipping'] = $data_address->phone;
                $data_city = CityModel::find($data_address->city_id);
                $data_district = DistrictModel::find($data_address->district_id);
                $data_ward = WardModel::where('ward_id', $data_address->ward_id)->first();
                $data_order->address_shipping = $data_address->address . ', ' . $data_ward->name . ', ' . $data_district->name . ', ' . $data_city->name;
            }
        }
        $data_shipping = ShippingModel::find($data_order['shipping_id']);
        $data_order['shipping'] = $data_shipping['name'];
        if ($data_order['payment_id'] == 0) {
            $data_order['payment'] = 'Ví CTV24H';
        } elseif ($data_order['payment_id'] == 1) {
            $data_order['payment'] = 'Trả góp';
        } elseif ($data_order['payment_id'] == 2) {
            $data_order['payment'] = 'Thanh toán khi nhận hàng';
        } elseif ($data_order['payment_id'] == 3) {
            $data_order['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
        } elseif ($data_order['payment_id'] == 4) {
            $data_order['payment'] = 'CTV24H Pay';
        } else {
            $data_order['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
        }
        if ($data_order->status == 4 && $data_order->cancel_confirm == 0) {
            $data_order['is_status'] = 'Yêu cầu huỷ hàng';
            if ($data_order['cancel_user'] == 0) {
                $data_order['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                $data_order['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
            } elseif ($data_order['cancel_user'] == 1) {
                $data_order['user_cancel'] = 'Đã huỷ bởi người mua';
                if ($data_order['cancel_reason'] === 0) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                } elseif ($data_order['cancel_reason'] === 1) {
                    $data_order['reason_cancel'] = 'Muốn nhập mã Voucher';
                } elseif ($data_order['cancel_reason'] === 2) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                } elseif ($data_order['cancel_reason'] === 3) {
                    $data_order['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                } elseif ($data_order['cancel_reason'] === 4) {
                    $data_order['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                } elseif ($data_order['cancel_reason'] === 5) {
                    $data_order['reason_cancel'] = 'Không mua nữa';
                } elseif ($data_order['cancel_reason'] === 6) {
                    $data_order['reason_cancel'] = 'Lý do khác';
                }
            }
        } elseif ($data_order->status == 4 && $data_order->cancel_confirm == 1) {
            $data_order['is_status'] = 'Đã huỷ';
            if ($data_order['cancel_user'] == 0) {
                $data_order['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                $data_order['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
            } elseif ($data_order['cancel_user'] == 1) {
                $data_order['user_cancel'] = 'Đã huỷ bởi người mua';
                if ($data_order['cancel_reason'] === 0) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                } elseif ($data_order['cancel_reason'] === 1) {
                    $data_order['reason_cancel'] = 'Muốn nhập mã Voucher';
                } elseif ($data_order['cancel_reason'] === 2) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                } elseif ($data_order['cancel_reason'] === 3) {
                    $data_order['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                } elseif ($data_order['cancel_reason'] === 4) {
                    $data_order['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                } elseif ($data_order['cancel_reason'] === 5) {
                    $data_order['reason_cancel'] = 'Không mua nữa';
                } elseif ($data_order['cancel_reason'] === 6) {
                    $data_order['reason_cancel'] = 'Lý do khác';
                }
            }
        }
        $data_order_detail = OrderDetailModel::where('order_id', $data_order->id)->get();
        $data_total_product = 0;
        foreach ($data_order_detail as $key => $value) {
            $data_product = ProductModel::find($value['product_id']);
            $data_order_detail[$key]['product_name'] = $data_product['name'];
            $data_order_detail[$key]['product_image'] = $data_product['image'];
            $dataValue_Attribute = AttributeValueModel::find($value->type_product);
            $data_order_detail[$key]->value_attr = $dataValue_Attribute->value ?? '';
            $data_order_detail[$key]->size_of_value = $dataValue_Attribute->size_of_value ?? '';
            $data_total_product += $value->total_price;
        }
        $data_order->total_price_product = $data_total_product;
        $data_return = [
            'title' => 'Chi tiết đơn hàng: ' . $data_order['order_code'],
            'customer' => $data_customer,
            'order' => $data_order,
            'order_detail' => $data_order_detail,
            'fee' => $fee
        ];
        return view('supplier.order.detail', $data_return);
    }

    /**
     *
     * cập nhật trạng thái đơn hàng
     * @param Request $request
     * @return array
     *
     */
    public function update(Request $request)
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $data_order = OrderModel::find($request->id);
        $data_order->status = $request->status;
        $data_order->created_at = $today;
        $data_order->save();
        if ($data_order->status == 3) {
            $data_order_detail = OrderDetailModel::where('order_id', $data_order->id)->get();
            foreach ($data_order_detail as $k => $v) {
                $data_order_detail[$k]['status'] = 3;
                $data_order_detail[$k]['status_confirm'] = 0;
                $data_order_detail[$k]->save();
            }
        }
        $dataReturn = [
            'status' => true,
            'url' => URL::to('admin/order'),
        ];
        return $dataReturn;
    }

    /**
     * tìm kiếm theo thời gian
     * @param Request $request
     *
     */
    public function filter_order(Request $request)
    {
        $time_start = $request->time_start;
        $time_stop = $request->time_stop;
        $data_login = session()->get('data_supplier');
        $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
        $output = '';
        $data_order = OrderModel::where('shop_id', $data_shop->id)->where('date', '>=', $time_start)->where('date', '<=', $time_stop)->where('is_selected', 1)->orderBy('id', 'desc')->get();
        $total_row = $data_order->count();
        if ($total_row > 0) {
            foreach ($data_order as $key => $value) {
                //Danh sách url
                $url_detail = url('admin/order/detail/' . $value->id);
                $url_show_confirm = url('admin/order/show_confirm/' . $value->id);
                $url_show_wait_shipping = url('admin/order/show_wait_shipping/' . $value->id);

                //nút xem thêm
                $btn_xemthem = '<a href="' . $url_detail . '" class="text-underline text-bold-700">Xem thêm</a>';
                $time_order = $value->created_at;
                $time_layhang = $time_order->addDay(2)->format('d-m-Y H:i:s');
                $data_order[$key]['time_layhang'] = $time_layhang;
                $data_order[$key]['shop_name'] = $data_shop->name;
                $data_order[$key]['shop_image'] = $data_shop->image;

                //check trạng thái và các thông báo, các nút theo trạng thái tương ứng
                if ($value['status'] == 0) {
                    $data_order[$key]['is_status'] = 'Chờ xác nhận';
                    $data_order[$key]['notification_status'] = '<p class="badge badge-pill text-red bg-pink" style="font-weight: 700!important;">
                                                                        <img src="../../assets/admin/app-assets/images/icon_dashboard/warning.png"
                                                                             alt="">
                                                                        Vui lòng xác nhận đơn hàng
                                                                    </p>
                                                                    <br>';
                    $data_order[$key]['btn_xacnhan'] = '<a data-id="' . $value->id . '" data-url="' . $url_show_confirm . '" class="text-blue text-underline text-bold-700 btn-show-confirm">Xác nhận</a>';
                    $data_order[$key]['btn_cod'] = '<span class="text-sliver f-12">Thanh toán COD</span>';
                } elseif ($value['status'] == 1) {
                    $data_order[$key]['is_status'] = 'Chờ lấy hàng';
                    $data_order[$key]['notification_status'] = '
                                                                    <span class="text-center text-sliver text-bold-700 f-12">
                                                                        Để tránh việc giao hàng trễ, vui lòng giao hàng hoặc chuẩn bị hàng trước ' . date('d-m-Y', strtotime($value->time_pick)) . '
                                                                    </span>
                                                                    <br>
                                                                 ';
                    $data_order[$key]['btn_cholayhang'] = '<a data-id="' . $value->id . '" data-url="' . $url_show_wait_shipping . '" class="text-blue text-underline text-bold-700 btn-wait-shipping">Chờ lấy hàng</a>';
                    $data_order[$key]['btn_cod'] = '<span class="text-sliver f-12">Thanh toán COD</span>';
                } elseif ($value['status'] == 2) {
                    $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
                    $data_order[$key]['btn_chitiet'] = '<a href="' . $url_detail . '" class="text-blue text-underline text-bold-700 btn-detail-order">Chi tiết</a>';
                    $data_order[$key]['btn_cod'] = '<span class="text-sliver f-12">Thanh toán COD</span>';
                } elseif ($value['status'] == 3) {
                    $data_order[$key]['is_status'] = 'Đã giao hàng';
                    $data_order[$key]['btn_chitiet'] = '<a href="' . $url_detail . '" class="text-blue text-underline text-bold-700 btn-detail-order">Chi tiết</a>';
                    $data_order[$key]['btn_danhgia'] = '<br>
                                        <a class="text-blue text-underline text-bold-700 btn-review">Đánh giá người mua</a>
                                    ';
                    $data_order[$key]['notification_bottom'] = '
                                                                    <br>
                                                                    <span class="text-center text-sliver text-bold-700 f-12">Hãy đánh giá người mua</span>
                                                                    ';
                } elseif ($value['status'] == 4) {
                    if ($value['cancel_confirm'] == 0) {
                        $data_order[$key]['is_status'] = 'Yêu cầu huỷ hàng';
                        if ($value['cancel_reason'] == 0) {
                            $data_order[$key]['reason_cancel'] = 'Lý do: Muốn thay đổi địa chỉ hàng';
                        } elseif ($value['cancel_reason'] == 1) {
                            $data_order[$key]['reason_cancel'] = 'Lý do: Muốn nhập mã Voucher';
                        } elseif ($value['cancel_reason'] == 2) {
                            $data_order[$key]['reason_cancel'] = 'Lý do: Muốn thay đổi sản phẩm trong đơn hàng';
                        } elseif ($value['cancel_reason'] == 3) {
                            $data_order[$key]['reason_cancel'] = 'Lý do: Thủ tục thanh toán quá rắc rối';
                        } elseif ($value['cancel_reason'] == 4) {
                            $data_order[$key]['reason_cancel'] = 'Lý do: Tìm thấy giá rẻ hơn ở chỗ khác';
                        } else {
                            $data_order[$key]['reason_cancel'] = 'Lý do: Lý do khác';
                        }
                        $data_order[$key]['notification_bottom'] = '
                                                                    <br>
                                                                    <span class="text-sliver f-12 text-bold-700">' . $data_order[$key]["reason_cancel"] . '</span>
                                                                ';
                        $data_order[$key]['btn_phanhoi_huyhang'] = '<a data-id="' . $value->id . '" class="text-blue text-underline text-bold-700 btn_confirm_cancel">Phản hồi đơn huỷ</a>';
                    } else {
                        $data_order[$key]['is_status'] = 'Đã huỷ';
                        if ($value->cancel_user == 0) {
                            $data_order[$key]['user_cancel'] = 'Đã huỷ bởi hệ thống';
                        } else {
                            $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                        }
                        $data_order[$key]['notification_bottom'] = '
                                                                    <br>
                                                                    <span class="text-sliver f-12 text-bold-700">' . $data_order[$key]['user_cancel'] . '</span>
                                                                ';
                        $data_order[$key]['btn_chitiet'] = '<a href="' . $url_detail . '" class="text-blue text-underline text-bold-700 btn-detail-order">Chi tiết</a>';
                    }
                } else {
                    $data_order[$key]['is_status'] = 'Hoàn tiền';
                    $data_order[$key]['btn_phanhoi'] = '<a class="text-blue text-underline text-bold-700 btn-feedback">Phản hồi</a>';
                    $data_order[$key]['btn_chitiet_trahang'] = '<br>
                                                <a class="text-blue text-underline text-bold-700 btn-detail-return-money">Chi tiết trả hàng hoàn tiền</a>
                                                ';
                }
                if ($value['payment_id'] == 0) {
                    $data_order[$key]['payment'] = 'Ví CTV24h';
                } elseif ($value['payment_id'] == 1) {
                    $data_order[$key]['payment'] = 'Trả góp';
                } elseif ($value['payment_id'] == 2) {
                    $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } else {
                    $data_order[$key]['payment'] = 'Thẻ tín dụng hoặc thẻ ghi nợ';
                }
                $data_order_detail = OrderDetailModel::where('shop_id', $data_shop->id)->orderBy('id', 'desc')->get();
                $html_order_detail = '';
                foreach ($data_order_detail as $k => $v) {
                    $data_product = ProductModel::find($v->product_id);
                    $data_order_detail[$k]['product_name'] = $data_product->name;
                    $data_order_detail[$k]['product_image'] = $data_product->image;
                    if ($data_product->dropship == 1) {
                        $data_order_detail[$k]['product_dropship'] = 'Hàng Dropship';
                    } else {
                        $data_order_detail[$k]['product_dropship'] = 'Hàng tại kho';
                    }
                    if ($v->order_id == $value->id) {
                        $html_order_detail .= '
                                             <div class="list_product d-flex align-items-center mb-1">
                                                <div class="img_product">
                                                    <img class="border_radius" width="70px" height="70px"
                                                         src="' . $v->product_image . '"
                                                         alt="">
                                                </div>
                                                <div class="info_product ml-1">
                                                    <span class="text-bold-700">' . $v->product_name . '<span
                                                            class="text-default"> (Số lượng: ' . $v->quantity . ')</span></span>
                                                    <br>
                                                    <span
                                                        class="text-blue text-bold-700">' . $v->product_dropship . '</span>
                                                    <br>
                                                    <span
                                                        class="text-sliver f-12 m-0 text-bold-700">ID đơn hàng: ' . $value->order_code . '</span>
                                                </div>
                                            </div>
                            ';
                    }
                }
                $output .= '
                        <tr class="border_tr">
                            <td colspan="10">
                                <div class="d-flex align-items-center">
                                    <img style="border-radius: 50%" width="20px" height="20px"
                                         src="' . $value->shop_image . '" alt="">
                                    <b style="margin-left: 10px;">' . $value->shop_name . '</b>
                                </div>
                            </td>
                        </tr>
                        <tr class="border_tr">
                            <td>' . $html_order_detail . '' . $btn_xemthem . '</td>
                            <td class="text-center">' . $value->notification_status . '<span class="text-bold-700">' . $value->is_status . '</span>' . $value->notification_bottom . '<br>' . $value->btn_cod . '</td>
                            <td class="text-center">
                                <img width="46px" height="16px"
                                     src="https://yt3.ggpht.com/ytc/AAUvwnhU0GWahEHuL6Wp-IKCAmkyejpu95fxhh7Jqg2xCg=s48-c-k-c0x00ffffff-no-rj-mo"
                                     alt="">
                                <p class="text-bold-700">Giao hàng nhanh</p>
                            </td>
                            <td class="text-center">
                                <span
                                    class="text-red f-18 text-bold-700">' . number_format($value->total_price) . ' đ</span>
                                <p class="text-sliver">' . $value->payment . '</p>
                            </td>
                            <td class="text-center">
                                ' . $value->btn_xacnhan . '' . $value->btn_cholayhang . '' . $value->btn_chitiet . '' . $value->btn_danhgia . '' . $value->btn_phanhoi_huyhang . '' . $value->btn_phanhoi . '' . $value->btn_chitiet_trahang . '
                            </td>
                        </tr>

                    ';
            }
        } else {
            $output .= '
                            <tr class="border_tr">
                                <td class="text-center text-bold-700" colspan="100">Không có đơn hàng nào !</td>
                            </tr>
                              ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    public function export()
    {
        return Excel::download(new OrderSupplierExport(), 'order_supplier.xlsx');
    }

    //xác nhận huỷ đơn hàng cho khách
    public function confirm_cancel(Request $request)
    {
        $data_order = OrderModel::find($request->id);
        $data_order->cancel_confirm = 1;
        $data_order->save();
        $dataReturn = [
            'status' => true,
            'msg' => 'Huỷ đơn thành công'
        ];
        return $dataReturn;
    }

    //filter status

    //tất cả đơn hàng
    public function all()
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $today_start = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 00:00:00');
        $today_end = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 23:59:59');
        $data_login = session()->get('data_supplier');
        $data_address_supplier = CustomerAddressModel::where('type_pick_up', 1)->orderBy('id', 'desc')->where('customer_id', $data_login->customer_id)->first();
        $data_address = CustomerAddressModel::where('customer_id', $data_login->customer_id)->where('type_pick_up', 0)->get();
        if (!$data_login) {
            return redirect('admin/supplier_admin/login');
        } else {
            $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
            $data_order = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)->orderBy('id', 'desc')->paginate(5);
            $total_row = $data_order->count();

            //sau 10 phút tự động chuyển trạng thái chờ lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 0) {
                    $time_check_10_min = $v->created_at;
                    $time_10_min = $time_check_10_min->addMinute(10)->format('Y-m-d H:i:s');
                    if (strtotime($today) > strtotime($time_10_min)) {
                        $status_shipping = true;
                    } else {
                        $status_shipping = false;
                    }
                    if ($status_shipping) {
                        $data_order[$k]['status'] = 1;
                        $data_order[$k]['shipping_confirm'] = 0;
                        $data_order[$k]->save();
                    }
                }
            }
            //end sau 10phut chuyển status

            //Quá hạn mà ko xác nhận
            $this->editStatusOrder($data_shop->id);
            //end quá hạn mà ko xác nhận


            //Quá hạn lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 1 && $v['shipping_confirm'] == 1) {
                    $time_qua_han_lay_hang = Carbon::parse($v->time_pick)->addDay(1)->format('Y-m-d H:i:s');
                    if (strtotime($time_qua_han_lay_hang) >= strtotime($today_start) && strtotime($time_qua_han_lay_hang) <= strtotime($today_end)) {
                        $qua_han_lay_hang = true;
                    } else {
                        $qua_han_lay_hang = false;
                    }
                    if ($qua_han_lay_hang) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                        if ($v->payment_id == 0){
                            if (empty($v->collaborator_id)){
                                $price_order = $v->total_price;
                            }else{
                                $price_order = $v->total_price - $v->bonus;
                                $data_history_collaborator = HistoryOrderPaymentCollaboratorModel::where('order_id', $v->id)->first();
                                $data_history_collaborator->status = 2;
                                $data_history_collaborator->save();
                            }
                            $customer = CustomerModel::find($v->customer_id);
                            $customer->wallet = $customer->wallet + $price_order;
                            $customer->save();
                            $data_history_supplier = HistoryOrderPaymentModel::where('order_id', $v->id)->first();
                            $data_history_supplier->status = 2;
                            $data_history_supplier->save();
                            $data_deal_ctv24h = HistoryDealCTV24::where('order_id', $v->id)->first();
                            $data_deal_ctv24h->status = 2;
                            $data_deal_ctv24h->save();
                        }
                    }
                }
            }
            //end quá hạn lấy hàng

            //get data
            foreach ($data_order as $key => $value) {
                $time_order = $value->created_at;
                $time_3_day = $time_order->addDay(3)->format('Y-m-d H:i:s');
                $time_check_10_min = $value->created_at;
                $time_10_min = $time_check_10_min->addMinute(2)->format('Y-m-d H:i:s');
                $data_order[$key]['today'] = $today;
                $data_order[$key]['time_10_min'] = $time_10_min;
                $data_order[$key]['time_3_day'] = $time_3_day;
                $data_order[$key]['shop_name'] = $data_shop->name;
                $data_order[$key]['shop_image'] = $data_shop->image;

                if ($value->collaborator_id == 1) {
                    $data_order[$key]['order_dropship'] = 'Hàng Dropship';
                } else {
                    $data_order[$key]['order_dropship'] = 'Hàng tại kho';
                }

                if ($value['status'] == 0) {
                    $data_order[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($value['status'] == 1) {
                    //rủi ro giao hàng trễ (khi quá 1 ngày từ ngày dự kiến lấy hàng)
                    $time_rui_ro = Carbon::parse($value->time_pick)->subDay(1)->format('Y-m-d');
                    if (strtotime($time_rui_ro) >= strtotime($today_start) && strtotime($time_rui_ro) <= strtotime($today_end) && $value['time_pick'] != null && $value['address_pick_id'] != null) {
                        $data_order[$key]['rui_ro'] = true;
                    } else {
                        $data_order[$key]['rui_ro'] = false;
                    }
                    //end rui ro

                    $data_order[$key]['is_status'] = 'Chờ lấy hàng';
                    $data_order[$key]['notification_status'] = 'Để tránh việc giao hàng trễ, vui lòng giao hàng hoặc chuẩn bị hàng trước ' . date('d-m-Y', strtotime($value->time_pick)) . '';
                } elseif ($value['status'] == 2) {
                    $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($value['status'] == 3) {
                    $data_order[$key]['is_status'] = 'Đã giao hàng';
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 0) {
                    $data_order[$key]['is_status'] = 'Yêu cầu huỷ đơn hàng';
                    if ($value['cancel_reason'] == 0) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] == 1) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] == 2) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] == 3) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] == 4) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } else {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Lý do khác';
                    }
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 1) {
                    $data_order[$key]['is_status'] = 'Đã huỷ';
                    if ($value['cancel_user'] == 0) {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi hệ thống';
                    } else {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    }
                } else {
                    $data_order[$key]['is_status'] = 'Hoàn tiền';
                }
                if ($value['payment_id'] == 0) {
                    $data_order[$key]['payment'] = 'Ví CTV24h';
                } elseif ($value['payment_id'] == 1) {
                    $data_order[$key]['payment'] = 'Trả góp';
                } elseif ($value['payment_id'] == 2) {
                    $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } else {
                    $data_order[$key]['payment'] = 'Thẻ tín dụng hoặc thẻ ghi nợ';
                }
                $data_shipping = ShippingModel::find($value['shipping_id']);
                $data_order[$key]['shipping'] = $data_shipping['name'];
            }
            $data_order_detail = OrderDetailModel::where('shop_id', $data_shop->id)->orderBy('id', 'desc')->get();
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v->product_id);
                $data_order_detail[$k]['product_name'] = $data_product->name;
                $data_order_detail[$k]['product_image'] = $data_product->image;
            }
//            dd($data_order);
            $dataReturn = [
                'status' => true,
                'title' => 'Đơn hàng: Tất cả',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
                'total_data' => $total_row,
                'address' => $data_address,
                'data_address' => $data_address_supplier,
            ];
        }
        return view('supplier.order.all_order', $dataReturn);
    }

    //chờ xác nhận
    public function wait_confirm()
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $today_start = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 00:00:00');
        $today_end = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 23:59:59');
        $data_login = session()->get('data_supplier');
        $data_address_supplier = CustomerAddressModel::where('type_pick_up', 1)->orderBy('id', 'desc')->where('customer_id', $data_login->customer_id)->first();
        $data_address = CustomerAddressModel::where('customer_id', $data_login->customer_id)->where('type_pick_up', 0)->get();
        if (!$data_login) {
            return redirect('admin/supplier_admin/login');
        } else {
            $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
            $data_order = OrderModel::where('shop_id', $data_shop->id)->where('status', 1)->where('shipping_confirm', 0)->orderBy('id', 'desc')->paginate(5);
            $total_row = $data_order->count();
            //sau 10 phút tự động chuyển trạng thái chờ lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 0) {
                    $time_check_10_min = $v->created_at;
                    $time_10_min = $time_check_10_min->addMinute(10)->format('Y-m-d H:i:s');
                    if (strtotime($today) > strtotime($time_10_min)) {
                        $status_shipping = true;
                    } else {
                        $status_shipping = false;
                    }
                    if ($status_shipping) {
                        $data_order[$k]['status'] = 1;
                        $data_order[$k]['shipping_confirm'] = 0;
                        $data_order[$k]->save();
                    }
                }
            }
            //end sau 10phut chuyển status

            //Quá hạn mà ko xác nhận
            foreach ($data_order as $k => $v) {
                $time_order = $v->created_at;
                $time_qua_han = $time_order->addDay(1)->format('Y-m-d H:i:s');
                if ($v['status'] == 1 && $v['shipping_confirm'] == 0) {
                    if (strtotime($time_qua_han) >= strtotime($today_start) && strtotime($time_qua_han) <= strtotime($today_end)) {
                        $qua_han = true;
                    } else {
                        $qua_han = false;
                    }
                    if ($qua_han) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn mà ko xác nhận


            //Quá hạn lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 1 && $v['shipping_confirm'] == 1) {
                    $time_qua_han_lay_hang = Carbon::parse($v->time_pick)->addDay(1)->format('Y-m-d H:i:s');
                    if (strtotime($time_qua_han_lay_hang) >= strtotime($today_start) && strtotime($time_qua_han_lay_hang) <= strtotime($today_end)) {
                        $qua_han_lay_hang = true;
                    } else {
                        $qua_han_lay_hang = false;
                    }
                    if ($qua_han_lay_hang) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn lấy hàng

            //get data
            foreach ($data_order as $key => $value) {
                $time_order = $value->created_at;
                $time_3_day = $time_order->addDay(3)->format('Y-m-d H:i:s');
                $time_check_10_min = $value->created_at;
                $time_10_min = $time_check_10_min->addMinute(2)->format('Y-m-d H:i:s');
                $data_order[$key]['today'] = $today;
                $data_order[$key]['time_10_min'] = $time_10_min;
                $data_order[$key]['time_3_day'] = $time_3_day;
                $data_order[$key]['shop_name'] = $data_shop->name;
                $data_order[$key]['shop_image'] = $data_shop->image;

                if ($value->collaborator_id == 1) {
                    $data_order[$key]['order_dropship'] = 'Hàng Dropship';
                } else {
                    $data_order[$key]['order_dropship'] = 'Hàng tại kho';
                }

                if ($value['status'] == 0) {
                    $data_order[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($value['status'] == 1) {
                    //rủi ro giao hàng trễ (khi quá 1 ngày từ ngày dự kiến lấy hàng)
                    $time_rui_ro = Carbon::parse($value->time_pick)->subDay(1)->format('Y-m-d');
                    if (strtotime($time_rui_ro) >= strtotime($today_start) && strtotime($time_rui_ro) <= strtotime($today_end) && $value['time_pick'] != null && $value['address_pick_id'] != null) {
                        $data_order[$key]['rui_ro'] = true;
                    } else {
                        $data_order[$key]['rui_ro'] = false;
                    }
                    //end rui ro

                    $data_order[$key]['is_status'] = 'Chờ lấy hàng';
                    $data_order[$key]['notification_status'] = 'Để tránh việc giao hàng trễ, vui lòng giao hàng hoặc chuẩn bị hàng trước ' . date('d-m-Y', strtotime($value->time_pick)) . '';
                } elseif ($value['status'] == 2) {
                    $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($value['status'] == 3) {
                    $data_order[$key]['is_status'] = 'Đã giao hàng';
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 0) {
                    $data_order[$key]['is_status'] = 'Yêu cầu huỷ đơn hàng';
                    if ($value['cancel_reason'] == 0) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] == 1) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] == 2) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] == 3) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] == 4) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } else {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Lý do khác';
                    }
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 1) {
                    $data_order[$key]['is_status'] = 'Đã huỷ';
                    if ($value['cancel_user'] == 0) {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi hệ thống';
                    } else {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    }
                } else {
                    $data_order[$key]['is_status'] = 'Hoàn tiền';
                }
                if ($value['payment_id'] == 0) {
                    $data_order[$key]['payment'] = 'Ví CTV24h';
                } elseif ($value['payment_id'] == 1) {
                    $data_order[$key]['payment'] = 'Trả góp';
                } elseif ($value['payment_id'] == 2) {
                    $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } else {
                    $data_order[$key]['payment'] = 'Thẻ tín dụng hoặc thẻ ghi nợ';
                }
                $data_shipping = ShippingModel::find($value['shipping_id']);
                $data_order[$key]['shipping'] = $data_shipping['name'];
            }
            $data_order_detail = OrderDetailModel::where('shop_id', $data_shop->id)->orderBy('id', 'desc')->get();
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v->product_id);
                $data_order_detail[$k]['product_name'] = $data_product->name;
                $data_order_detail[$k]['product_image'] = $data_product->image;
            }
            $dataReturn = [
                'status' => true,
                'title' => 'Đơn hàng: Chờ xác nhận',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
                'total_data' => $total_row,
                'address' => $data_address,
                'data_address' => $data_address_supplier,
            ];
        }
        return view('supplier.order.wait_confirm', $dataReturn);
    }


    //Chờ lấy hàng
    public function wait_shipping()
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $today_start = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 00:00:00');
        $today_end = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 23:59:59');
        $data_login = session()->get('data_supplier');
        $data_address_supplier = CustomerAddressModel::where('type_pick_up', 1)->orderBy('id', 'desc')->where('customer_id', $data_login->customer_id)->first();
        $data_address = CustomerAddressModel::where('customer_id', $data_login->customer_id)->where('type_pick_up', 0)->get();
        if (!$data_login) {
            return redirect('admin/supplier_admin/login');
        } else {
            $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
            $data_order = OrderModel::where('shop_id', $data_shop->id)->where('status', 1)->where('shipping_confirm', 1)->orderBy('id', 'desc')->paginate(5);
            $total_row = $data_order->count();

            //sau 10 phút tự động chuyển trạng thái chờ lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 0) {
                    $time_check_10_min = $v->created_at;
                    $time_10_min = $time_check_10_min->addMinute(10)->format('Y-m-d H:i:s');
                    if (strtotime($today) > strtotime($time_10_min)) {
                        $status_shipping = true;
                    } else {
                        $status_shipping = false;
                    }
                    if ($status_shipping) {
                        $data_order[$k]['status'] = 1;
                        $data_order[$k]['shipping_confirm'] = 0;
                        $data_order[$k]->save();
                    }
                }
            }
            //end sau 10phut chuyển status

            //Quá hạn mà ko xác nhận
            foreach ($data_order as $k => $v) {
                $time_order = $v->created_at;
                $time_qua_han = $time_order->addDay(1)->format('Y-m-d H:i:s');
                if ($v['status'] == 1 && $v['shipping_confirm'] == 0) {
                    if (strtotime($time_qua_han) >= strtotime($today_start) && strtotime($time_qua_han) <= strtotime($today_end)) {
                        $qua_han = true;
                    } else {
                        $qua_han = false;
                    }
                    if ($qua_han) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn mà ko xác nhận


            //Quá hạn lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 1 && $v['shipping_confirm'] == 1) {
                    $time_qua_han_lay_hang = Carbon::parse($v->time_pick)->addDay(1)->format('Y-m-d H:i:s');
                    if (strtotime($time_qua_han_lay_hang) >= strtotime($today_start) && strtotime($time_qua_han_lay_hang) <= strtotime($today_end)) {
                        $qua_han_lay_hang = true;
                    } else {
                        $qua_han_lay_hang = false;
                    }
                    if ($qua_han_lay_hang) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn lấy hàng

            //get data
            foreach ($data_order as $key => $value) {
                $time_order = $value->created_at;
                $time_3_day = $time_order->addDay(3)->format('Y-m-d H:i:s');
                $time_check_10_min = $value->created_at;
                $time_10_min = $time_check_10_min->addMinute(2)->format('Y-m-d H:i:s');
                $data_order[$key]['today'] = $today;
                $data_order[$key]['time_10_min'] = $time_10_min;
                $data_order[$key]['time_3_day'] = $time_3_day;
                $data_order[$key]['shop_name'] = $data_shop->name;
                $data_order[$key]['shop_image'] = $data_shop->image;

                if ($value->collaborator_id == 1) {
                    $data_order[$key]['order_dropship'] = 'Hàng Dropship';
                } else {
                    $data_order[$key]['order_dropship'] = 'Hàng tại kho';
                }

                if ($value['status'] == 0) {
                    $data_order[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($value['status'] == 1) {
                    //rủi ro giao hàng trễ (khi quá 1 ngày từ ngày dự kiến lấy hàng)
                    $time_rui_ro = Carbon::parse($value->time_pick)->subDay(1)->format('Y-m-d');
                    if (strtotime($time_rui_ro) >= strtotime($today_start) && strtotime($time_rui_ro) <= strtotime($today_end) && $value['time_pick'] != null && $value['address_pick_id'] != null) {
                        $data_order[$key]['rui_ro'] = true;
                    } else {
                        $data_order[$key]['rui_ro'] = false;
                    }
                    //end rui ro

                    $data_order[$key]['is_status'] = 'Chờ lấy hàng';
                    $data_order[$key]['notification_status'] = 'Để tránh việc giao hàng trễ, vui lòng giao hàng hoặc chuẩn bị hàng trước ' . date('d-m-Y', strtotime($value->time_pick)) . '';
                } elseif ($value['status'] == 2) {
                    $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($value['status'] == 3) {
                    $data_order[$key]['is_status'] = 'Đã giao hàng';
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 0) {
                    $data_order[$key]['is_status'] = 'Yêu cầu huỷ đơn hàng';
                    if ($value['cancel_reason'] == 0) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] == 1) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] == 2) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] == 3) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] == 4) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } else {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Lý do khác';
                    }
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 1) {
                    $data_order[$key]['is_status'] = 'Đã huỷ';
                    if ($value['cancel_user'] == 0) {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi hệ thống';
                    } else {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    }
                } else {
                    $data_order[$key]['is_status'] = 'Hoàn tiền';
                }
                if ($value['payment_id'] == 0) {
                    $data_order[$key]['payment'] = 'Ví CTV24h';
                } elseif ($value['payment_id'] == 1) {
                    $data_order[$key]['payment'] = 'Trả góp';
                } elseif ($value['payment_id'] == 2) {
                    $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } else {
                    $data_order[$key]['payment'] = 'Thẻ tín dụng hoặc thẻ ghi nợ';
                }
                $data_shipping = ShippingModel::find($value['shipping_id']);
                $data_order[$key]['shipping'] = $data_shipping['name'];
            }
            $data_order_detail = OrderDetailModel::where('shop_id', $data_shop->id)->orderBy('id', 'desc')->get();
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v->product_id);
                $data_order_detail[$k]['product_name'] = $data_product->name;
                $data_order_detail[$k]['product_image'] = $data_product->image;
            }
//            dd($data_order);
            $dataReturn = [
                'status' => true,
                'title' => 'Đơn hàng: Chờ lấy hàng',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
                'total_data' => $total_row,
                'address' => $data_address,
                'data_address' => $data_address_supplier,
            ];
        }
        return view('supplier.order.wait_shipping', $dataReturn);
    }

    //Đag giao hàng
    public function shipping()
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $today_start = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 00:00:00');
        $today_end = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 23:59:59');
        $data_login = session()->get('data_supplier');
        $data_address_supplier = CustomerAddressModel::where('type_pick_up', 1)->orderBy('id', 'desc')->where('customer_id', $data_login->customer_id)->first();
        $data_address = CustomerAddressModel::where('customer_id', $data_login->customer_id)->where('type_pick_up', 0)->get();
        if (!$data_login) {
            return redirect('admin/supplier_admin/login');
        } else {
            $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
            $data_order = OrderModel::where('shop_id', $data_shop->id)->where('status', 2)->orderBy('id', 'desc')->paginate(5);
            $total_row = $data_order->count();

            //sau 10 phút tự động chuyển trạng thái chờ lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 0) {
                    $time_check_10_min = $v->created_at;
                    $time_10_min = $time_check_10_min->addMinute(10)->format('Y-m-d H:i:s');
                    if (strtotime($today) > strtotime($time_10_min)) {
                        $status_shipping = true;
                    } else {
                        $status_shipping = false;
                    }
                    if ($status_shipping) {
                        $data_order[$k]['status'] = 1;
                        $data_order[$k]['shipping_confirm'] = 0;
                        $data_order[$k]->save();
                    }
                }
            }
            //end sau 10phut chuyển status

            //Quá hạn mà ko xác nhận
            foreach ($data_order as $k => $v) {
                $time_order = $v->created_at;
                $time_qua_han = $time_order->addDay(1)->format('Y-m-d H:i:s');
                if ($v['status'] == 1 && $v['shipping_confirm'] == 0) {
                    if (strtotime($time_qua_han) >= strtotime($today_start) && strtotime($time_qua_han) <= strtotime($today_end)) {
                        $qua_han = true;
                    } else {
                        $qua_han = false;
                    }
                    if ($qua_han) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn mà ko xác nhận


            //Quá hạn lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 1 && $v['shipping_confirm'] == 1) {
                    $time_qua_han_lay_hang = Carbon::parse($v->time_pick)->addDay(1)->format('Y-m-d H:i:s');
                    if (strtotime($time_qua_han_lay_hang) >= strtotime($today_start) && strtotime($time_qua_han_lay_hang) <= strtotime($today_end)) {
                        $qua_han_lay_hang = true;
                    } else {
                        $qua_han_lay_hang = false;
                    }
                    if ($qua_han_lay_hang) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn lấy hàng

            //get data
            foreach ($data_order as $key => $value) {
                $time_order = $value->created_at;
                $time_3_day = $time_order->addDay(3)->format('Y-m-d H:i:s');
                $time_check_10_min = $value->created_at;
                $time_10_min = $time_check_10_min->addMinute(2)->format('Y-m-d H:i:s');
                $data_order[$key]['today'] = $today;
                $data_order[$key]['time_10_min'] = $time_10_min;
                $data_order[$key]['time_3_day'] = $time_3_day;
                $data_order[$key]['shop_name'] = $data_shop->name;
                $data_order[$key]['shop_image'] = $data_shop->image;

                if ($value->collaborator_id == 1) {
                    $data_order[$key]['order_dropship'] = 'Hàng Dropship';
                } else {
                    $data_order[$key]['order_dropship'] = 'Hàng tại kho';
                }

                if ($value['status'] == 0) {
                    $data_order[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($value['status'] == 1) {
                    //rủi ro giao hàng trễ (khi quá 1 ngày từ ngày dự kiến lấy hàng)
                    $time_rui_ro = Carbon::parse($value->time_pick)->subDay(1)->format('Y-m-d');
                    if (strtotime($time_rui_ro) >= strtotime($today_start) && strtotime($time_rui_ro) <= strtotime($today_end) && $value['time_pick'] != null && $value['address_pick_id'] != null) {
                        $data_order[$key]['rui_ro'] = true;
                    } else {
                        $data_order[$key]['rui_ro'] = false;
                    }
                    //end rui ro

                    $data_order[$key]['is_status'] = 'Chờ lấy hàng';
                    $data_order[$key]['notification_status'] = 'Để tránh việc giao hàng trễ, vui lòng giao hàng hoặc chuẩn bị hàng trước ' . date('d-m-Y', strtotime($value->time_pick)) . '';
                } elseif ($value['status'] == 2) {
                    $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($value['status'] == 3) {
                    $data_order[$key]['is_status'] = 'Đã giao hàng';
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 0) {
                    $data_order[$key]['is_status'] = 'Yêu cầu huỷ đơn hàng';
                    if ($value['cancel_reason'] == 0) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] == 1) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] == 2) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] == 3) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] == 4) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } else {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Lý do khác';
                    }
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 1) {
                    $data_order[$key]['is_status'] = 'Đã huỷ';
                    if ($value['cancel_user'] == 0) {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi hệ thống';
                    } else {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    }
                } else {
                    $data_order[$key]['is_status'] = 'Hoàn tiền';
                }
                if ($value['payment_id'] == 0) {
                    $data_order[$key]['payment'] = 'Ví CTV24h';
                } elseif ($value['payment_id'] == 1) {
                    $data_order[$key]['payment'] = 'Trả góp';
                } elseif ($value['payment_id'] == 2) {
                    $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } else {
                    $data_order[$key]['payment'] = 'Thẻ tín dụng hoặc thẻ ghi nợ';
                }
                $data_shipping = ShippingModel::find($value['shipping_id']);
                $data_order[$key]['shipping'] = $data_shipping['name'];
            }
            $data_order_detail = OrderDetailModel::where('shop_id', $data_shop->id)->orderBy('id', 'desc')->get();
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v->product_id);
                $data_order_detail[$k]['product_name'] = $data_product->name;
                $data_order_detail[$k]['product_image'] = $data_product->image;
            }
//            dd($data_order);
            $dataReturn = [
                'status' => true,
                'title' => 'Đơn hàng: Đang giao',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
                'total_data' => $total_row,
                'address' => $data_address,
                'data_address' => $data_address_supplier,
            ];
        }
        return view('supplier.order.shipping', $dataReturn);
    }

    //Đã giao hàng
    public function shipped()
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $today_start = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 00:00:00');
        $today_end = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 23:59:59');
        $data_login = session()->get('data_supplier');
        $data_address_supplier = CustomerAddressModel::where('type_pick_up', 1)->orderBy('id', 'desc')->where('customer_id', $data_login->customer_id)->first();
        $data_address = CustomerAddressModel::where('customer_id', $data_login->customer_id)->where('type_pick_up', 0)->get();
        if (!$data_login) {
            return redirect('admin/supplier_admin/login');
        } else {
            $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
            $data_order = OrderModel::where('shop_id', $data_shop->id)->where('status', 3)->orderBy('id', 'desc')->paginate(5);
            $total_row = $data_order->count();

            //sau 10 phút tự động chuyển trạng thái chờ lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 0) {
                    $time_check_10_min = $v->created_at;
                    $time_10_min = $time_check_10_min->addMinute(10)->format('Y-m-d H:i:s');
                    if (strtotime($today) > strtotime($time_10_min)) {
                        $status_shipping = true;
                    } else {
                        $status_shipping = false;
                    }
                    if ($status_shipping) {
                        $data_order[$k]['status'] = 1;
                        $data_order[$k]['shipping_confirm'] = 0;
                        $data_order[$k]->save();
                    }
                }
            }
            //end sau 10phut chuyển status

            //Quá hạn mà ko xác nhận
            foreach ($data_order as $k => $v) {
                $time_order = $v->created_at;
                $time_qua_han = $time_order->addDay(1)->format('Y-m-d H:i:s');
                if ($v['status'] == 1 && $v['shipping_confirm'] == 0) {
                    if (strtotime($time_qua_han) >= strtotime($today_start) && strtotime($time_qua_han) <= strtotime($today_end)) {
                        $qua_han = true;
                    } else {
                        $qua_han = false;
                    }
                    if ($qua_han) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn mà ko xác nhận


            //Quá hạn lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 1 && $v['shipping_confirm'] == 1) {
                    $time_qua_han_lay_hang = Carbon::parse($v->time_pick)->addDay(1)->format('Y-m-d H:i:s');
                    if (strtotime($time_qua_han_lay_hang) >= strtotime($today_start) && strtotime($time_qua_han_lay_hang) <= strtotime($today_end)) {
                        $qua_han_lay_hang = true;
                    } else {
                        $qua_han_lay_hang = false;
                    }
                    if ($qua_han_lay_hang) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn lấy hàng

            //get data
            foreach ($data_order as $key => $value) {
                $time_order = $value->created_at;
                $time_3_day = $time_order->addDay(3)->format('Y-m-d H:i:s');
                $time_check_10_min = $value->created_at;
                $time_10_min = $time_check_10_min->addMinute(2)->format('Y-m-d H:i:s');
                $data_order[$key]['today'] = $today;
                $data_order[$key]['time_10_min'] = $time_10_min;
                $data_order[$key]['time_3_day'] = $time_3_day;
                $data_order[$key]['shop_name'] = $data_shop->name;
                $data_order[$key]['shop_image'] = $data_shop->image;

                if ($value->collaborator_id == 1) {
                    $data_order[$key]['order_dropship'] = 'Hàng Dropship';
                } else {
                    $data_order[$key]['order_dropship'] = 'Hàng tại kho';
                }

                if ($value['status'] == 0) {
                    $data_order[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($value['status'] == 1) {
                    //rủi ro giao hàng trễ (khi quá 1 ngày từ ngày dự kiến lấy hàng)
                    $time_rui_ro = Carbon::parse($value->time_pick)->subDay(1)->format('Y-m-d');
                    if (strtotime($time_rui_ro) >= strtotime($today_start) && strtotime($time_rui_ro) <= strtotime($today_end) && $value['time_pick'] != null && $value['address_pick_id'] != null) {
                        $data_order[$key]['rui_ro'] = true;
                    } else {
                        $data_order[$key]['rui_ro'] = false;
                    }
                    //end rui ro

                    $data_order[$key]['is_status'] = 'Chờ lấy hàng';
                    $data_order[$key]['notification_status'] = 'Để tránh việc giao hàng trễ, vui lòng giao hàng hoặc chuẩn bị hàng trước ' . date('d-m-Y', strtotime($value->time_pick)) . '';
                } elseif ($value['status'] == 2) {
                    $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($value['status'] == 3) {
                    $data_order[$key]['is_status'] = 'Đã giao hàng';
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 0) {
                    $data_order[$key]['is_status'] = 'Yêu cầu huỷ đơn hàng';
                    if ($value['cancel_reason'] == 0) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] == 1) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] == 2) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] == 3) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] == 4) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } else {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Lý do khác';
                    }
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 1) {
                    $data_order[$key]['is_status'] = 'Đã huỷ';
                    if ($value['cancel_user'] == 0) {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi hệ thống';
                    } else {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    }
                } else {
                    $data_order[$key]['is_status'] = 'Hoàn tiền';
                }
                if ($value['payment_id'] == 0) {
                    $data_order[$key]['payment'] = 'Ví CTV24h';
                } elseif ($value['payment_id'] == 1) {
                    $data_order[$key]['payment'] = 'Trả góp';
                } elseif ($value['payment_id'] == 2) {
                    $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } else {
                    $data_order[$key]['payment'] = 'Thẻ tín dụng hoặc thẻ ghi nợ';
                }
                $data_shipping = ShippingModel::find($value['shipping_id']);
                $data_order[$key]['shipping'] = $data_shipping['name'];
            }
            $data_order_detail = OrderDetailModel::where('shop_id', $data_shop->id)->orderBy('id', 'desc')->get();
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v->product_id);
                $data_order_detail[$k]['product_name'] = $data_product->name;
                $data_order_detail[$k]['product_image'] = $data_product->image;
            }
//            dd($data_order);
            $dataReturn = [
                'status' => true,
                'title' => 'Đơn hàng: Đã giao',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
                'total_data' => $total_row,
                'address' => $data_address,
                'data_address' => $data_address_supplier,
            ];
        }
        return view('supplier.order.shipped', $dataReturn);
    }

    //Đã huỷ
    public function cancel()
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $today_start = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 00:00:00');
        $today_end = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 23:59:59');
        $data_login = session()->get('data_supplier');
        $data_address_supplier = CustomerAddressModel::where('type_pick_up', 1)->orderBy('id', 'desc')->where('customer_id', $data_login->customer_id)->first();
        $data_address = CustomerAddressModel::where('customer_id', $data_login->customer_id)->where('type_pick_up', 0)->get();
        if (!$data_login) {
            return redirect('admin/supplier_admin/login');
        } else {
            $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
            $data_order = OrderModel::where('shop_id', $data_shop->id)->where('status', 4)->orderBy('id', 'desc')->paginate(5);
            $total_row = $data_order->count();

            //sau 10 phút tự động chuyển trạng thái chờ lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 0) {
                    $time_check_10_min = $v->created_at;
                    $time_10_min = $time_check_10_min->addMinute(10)->format('Y-m-d H:i:s');
                    if (strtotime($today) > strtotime($time_10_min)) {
                        $status_shipping = true;
                    } else {
                        $status_shipping = false;
                    }
                    if ($status_shipping) {
                        $data_order[$k]['status'] = 1;
                        $data_order[$k]['shipping_confirm'] = 0;
                        $data_order[$k]->save();
                    }
                }
            }
            //end sau 10phut chuyển status

            //Quá hạn mà ko xác nhận
            foreach ($data_order as $k => $v) {
                $time_order = $v->created_at;
                $time_qua_han = $time_order->addDay(1)->format('Y-m-d H:i:s');
                if ($v['status'] == 1 && $v['shipping_confirm'] == 0) {
                    if (strtotime($time_qua_han) >= strtotime($today_start) && strtotime($time_qua_han) <= strtotime($today_end)) {
                        $qua_han = true;
                    } else {
                        $qua_han = false;
                    }
                    if ($qua_han) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn mà ko xác nhận


            //Quá hạn lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 1 && $v['shipping_confirm'] == 1) {
                    $time_qua_han_lay_hang = Carbon::parse($v->time_pick)->addDay(1)->format('Y-m-d H:i:s');
                    if (strtotime($time_qua_han_lay_hang) >= strtotime($today_start) && strtotime($time_qua_han_lay_hang) <= strtotime($today_end)) {
                        $qua_han_lay_hang = true;
                    } else {
                        $qua_han_lay_hang = false;
                    }
                    if ($qua_han_lay_hang) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn lấy hàng

            //get data
            foreach ($data_order as $key => $value) {
                $time_order = $value->created_at;
                $time_3_day = $time_order->addDay(3)->format('Y-m-d H:i:s');
                $time_check_10_min = $value->created_at;
                $time_10_min = $time_check_10_min->addMinute(2)->format('Y-m-d H:i:s');
                $data_order[$key]['today'] = $today;
                $data_order[$key]['time_10_min'] = $time_10_min;
                $data_order[$key]['time_3_day'] = $time_3_day;
                $data_order[$key]['shop_name'] = $data_shop->name;
                $data_order[$key]['shop_image'] = $data_shop->image;

                if ($value->collaborator_id == 1) {
                    $data_order[$key]['order_dropship'] = 'Hàng Dropship';
                } else {
                    $data_order[$key]['order_dropship'] = 'Hàng tại kho';
                }

                if ($value['status'] == 0) {
                    $data_order[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($value['status'] == 1) {
                    //rủi ro giao hàng trễ (khi quá 1 ngày từ ngày dự kiến lấy hàng)
                    $time_rui_ro = Carbon::parse($value->time_pick)->subDay(1)->format('Y-m-d');
                    if (strtotime($time_rui_ro) >= strtotime($today_start) && strtotime($time_rui_ro) <= strtotime($today_end) && $value['time_pick'] != null && $value['address_pick_id'] != null) {
                        $data_order[$key]['rui_ro'] = true;
                    } else {
                        $data_order[$key]['rui_ro'] = false;
                    }
                    //end rui ro

                    $data_order[$key]['is_status'] = 'Chờ lấy hàng';
                    $data_order[$key]['notification_status'] = 'Để tránh việc giao hàng trễ, vui lòng giao hàng hoặc chuẩn bị hàng trước ' . date('d-m-Y', strtotime($value->time_pick)) . '';
                } elseif ($value['status'] == 2) {
                    $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($value['status'] == 3) {
                    $data_order[$key]['is_status'] = 'Đã giao hàng';
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 0) {
                    $data_order[$key]['is_status'] = 'Yêu cầu huỷ đơn hàng';
                    if ($value['cancel_reason'] == 0) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] == 1) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] == 2) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] == 3) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] == 4) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } else {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Lý do khác';
                    }
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 1) {
                    $data_order[$key]['is_status'] = 'Đã huỷ';
                    if ($value['cancel_user'] == 0) {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi hệ thống';
                    } else {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    }
                } else {
                    $data_order[$key]['is_status'] = 'Hoàn tiền';
                }
                if ($value['payment_id'] == 0) {
                    $data_order[$key]['payment'] = 'Ví CTV24h';
                } elseif ($value['payment_id'] == 1) {
                    $data_order[$key]['payment'] = 'Trả góp';
                } elseif ($value['payment_id'] == 2) {
                    $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } else {
                    $data_order[$key]['payment'] = 'Thẻ tín dụng hoặc thẻ ghi nợ';
                }
                $data_shipping = ShippingModel::find($value['shipping_id']);
                $data_order[$key]['shipping'] = $data_shipping['name'];
            }
            $data_order_detail = OrderDetailModel::where('shop_id', $data_shop->id)->orderBy('id', 'desc')->get();
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v->product_id);
                $data_order_detail[$k]['product_name'] = $data_product->name;
                $data_order_detail[$k]['product_image'] = $data_product->image;
            }
//            dd($data_order);
            $dataReturn = [
                'status' => true,
                'title' => 'Đơn hàng: Đã hủy',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
                'total_data' => $total_row,
                'address' => $data_address,
                'data_address' => $data_address_supplier,
            ];
        }
        return view('supplier.order.cancel', $dataReturn);
    }

    //Hoàn tiền
    public function return_money()
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $today_start = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 00:00:00');
        $today_end = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 23:59:59');
        $data_login = session()->get('data_supplier');
        $data_address_supplier = CustomerAddressModel::where('type_pick_up', 1)->orderBy('id', 'desc')->where('customer_id', $data_login->customer_id)->first();
        $data_address = CustomerAddressModel::where('customer_id', $data_login->customer_id)->where('type_pick_up', 0)->get();
        if (!$data_login) {
            return redirect('admin/supplier_admin/login');
        } else {
            $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
            $data_order = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)->where('status', 5)->orderBy('id', 'desc')->paginate(5);
            $total_row = $data_order->count();

            //sau 10 phút tự động chuyển trạng thái chờ lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 0) {
                    $time_check_10_min = $v->created_at;
                    $time_10_min = $time_check_10_min->addMinute(10)->format('Y-m-d H:i:s');
                    if (strtotime($today) > strtotime($time_10_min)) {
                        $status_shipping = true;
                    } else {
                        $status_shipping = false;
                    }
                    if ($status_shipping) {
                        $data_order[$k]['status'] = 1;
                        $data_order[$k]['shipping_confirm'] = 0;
                        $data_order[$k]->save();
                    }
                }
            }
            //end sau 10phut chuyển status

            //Quá hạn mà ko xác nhận
            foreach ($data_order as $k => $v) {
                $time_order = $v->created_at;
                $time_qua_han = $time_order->addDay(1)->format('Y-m-d H:i:s');
                if ($v['status'] == 1 && $v['shipping_confirm'] == 0) {
                    if (strtotime($time_qua_han) >= strtotime($today_start) && strtotime($time_qua_han) <= strtotime($today_end)) {
                        $qua_han = true;
                    } else {
                        $qua_han = false;
                    }
                    if ($qua_han) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn mà ko xác nhận


            //Quá hạn lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 1 && $v['shipping_confirm'] == 1) {
                    $time_qua_han_lay_hang = Carbon::parse($v->time_pick)->addDay(1)->format('Y-m-d H:i:s');
                    if (strtotime($time_qua_han_lay_hang) >= strtotime($today_start) && strtotime($time_qua_han_lay_hang) <= strtotime($today_end)) {
                        $qua_han_lay_hang = true;
                    } else {
                        $qua_han_lay_hang = false;
                    }
                    if ($qua_han_lay_hang) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn lấy hàng

            //get data
            foreach ($data_order as $key => $value) {
                $time_order = $value->created_at;
                $time_3_day = $time_order->addDay(3)->format('Y-m-d H:i:s');
                $time_check_10_min = $value->created_at;
                $time_10_min = $time_check_10_min->addMinute(2)->format('Y-m-d H:i:s');
                $data_order[$key]['today'] = $today;
                $data_order[$key]['time_10_min'] = $time_10_min;
                $data_order[$key]['time_3_day'] = $time_3_day;
                $data_order[$key]['shop_name'] = $data_shop->name;
                $data_order[$key]['shop_image'] = $data_shop->image;

                if ($value->collaborator_id == 1) {
                    $data_order[$key]['order_dropship'] = 'Hàng Dropship';
                } else {
                    $data_order[$key]['order_dropship'] = 'Hàng tại kho';
                }

                if ($value['status'] == 0) {
                    $data_order[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($value['status'] == 1) {
                    //rủi ro giao hàng trễ (khi quá 1 ngày từ ngày dự kiến lấy hàng)
                    $time_rui_ro = Carbon::parse($value->time_pick)->subDay(1)->format('Y-m-d');
                    if (strtotime($time_rui_ro) >= strtotime($today_start) && strtotime($time_rui_ro) <= strtotime($today_end) && $value['time_pick'] != null && $value['address_pick_id'] != null) {
                        $data_order[$key]['rui_ro'] = true;
                    } else {
                        $data_order[$key]['rui_ro'] = false;
                    }
                    //end rui ro

                    $data_order[$key]['is_status'] = 'Chờ lấy hàng';
                    $data_order[$key]['notification_status'] = 'Để tránh việc giao hàng trễ, vui lòng giao hàng hoặc chuẩn bị hàng trước ' . date('d-m-Y', strtotime($value->time_pick)) . '';
                } elseif ($value['status'] == 2) {
                    $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($value['status'] == 3) {
                    $data_order[$key]['is_status'] = 'Đã giao hàng';
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 0) {
                    $data_order[$key]['is_status'] = 'Yêu cầu huỷ đơn hàng';
                    if ($value['cancel_reason'] == 0) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] == 1) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] == 2) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] == 3) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] == 4) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } else {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Lý do khác';
                    }
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 1) {
                    $data_order[$key]['is_status'] = 'Đã huỷ';
                    if ($value['cancel_user'] == 0) {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi hệ thống';
                    } else {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    }
                } else {
                    $data_order[$key]['is_status'] = 'Hoàn tiền';
                }
                if ($value['payment_id'] == 0) {
                    $data_order[$key]['payment'] = 'Ví CTV24h';
                } elseif ($value['payment_id'] == 1) {
                    $data_order[$key]['payment'] = 'Trả góp';
                } elseif ($value['payment_id'] == 2) {
                    $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } else {
                    $data_order[$key]['payment'] = 'Thẻ tín dụng hoặc thẻ ghi nợ';
                }
                $data_shipping = ShippingModel::find($value['shipping_id']);
                $data_order[$key]['shipping'] = $data_shipping['name'];
            }
            $data_order_detail = OrderDetailModel::where('shop_id', $data_shop->id)->orderBy('id', 'desc')->get();
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v->product_id);
                $data_order_detail[$k]['product_name'] = $data_product->name;
                $data_order_detail[$k]['product_image'] = $data_product->image;
            }
//            dd($data_order);
            $dataReturn = [
                'status' => true,
                'title' => 'Đơn hàng: Tất cả',
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
                'total_data' => $total_row,
                'address' => $data_address,
                'data_address' => $data_address_supplier,
            ];
        }
        return view('supplier.order.return_money', $dataReturn);
    }

    //show confirm xác nhận
    public function show_confirm($id)
    {
        $data_login = session()->get('data_supplier');
        $data_address_pick_up = AddressSupplierModel::where('supplier_id', $data_login->id)->where('type_pick_up', 1)->first();
        $data_address = AddressSupplierModel::where('supplier_id', $data_login->id)->where('type_pick_up', 0)->get();
//        dd($data_address);
        $data_order = OrderModel::find($id);
//        $data_order->time_layhang = $data_order['created_at']->addDay(2);
        $dataReturn = [
            'status' => true,
            'data' => $data_order,
            'data_address' => $data_address_pick_up,
            'address' => $data_address
        ];
        return view('supplier.order.popup_confirm', $dataReturn);
    }

    //show popup chờ lấy hàng
    public function show_wait_shipping($id)
    {
        $data_order = OrderModel::find($id);
        $dataReturn = [
            'status' => true,
            'data' => $data_order,
        ];
        return view('supplier.order.popup_wait_shipping', $dataReturn);
    }
    public function reciver($address_id)
    {
        return AddressCustomerCTVModel::where('id', $address_id)
            ->with(
                'province1',
                'district1',
                'ward1'
            )
            ->select(
                'id',
                'city',
                'district',
                'ward',
                'name',
                'phone',
                'address'
            )
            ->first();
    }
    public function picker($address_id)
    {
        return CustomerAddressModel::where('id', $address_id)
            ->with(
                'province',
                'district',
                'ward'
            )
            ->select(
                'id',
                'city_id',
                'district_id',
                'ward_id',
                'name',
                'phone',
                'address'
            )
            ->first();
    }
    public function prepareCreateDelivery($order, $request)
    {
        $info = [];
        $picker = $this->picker($request['address_pick']);
        $reciever = $this->reciver($order->address_ctv);
        // return $reciever['district1'];
        $weight = 0;
        $value_order = 0;
        $products = [];

        foreach ($order->load('orderDetails')['orderDetails'] as $orderDetail) {
            $value_order += $orderDetail->quantity * $orderDetail->product_price;
            $product =  $orderDetail->load('product')['product'];
            $weight += $product['weight'];
            $products[] = [
                'name' => $product['name'],
                'weight' => $product['weight'] / 1000,
                'quantity' => $orderDetail->quantity,
                'product_code' => $product['id'],
            ];
        }
        $pick_option = 'cod';
        if ($request['form_shipping'] == 0) {
            $pick_option = 'post';
        }
        $deliver_option = 'road';
        $info['weight'] = $weight;
        $info['value_order'] = $value_order;
        $info['pick_option'] = $pick_option;
        $info['deliver_option'] = $deliver_option;
        $res = $this->createOrderDelivery($info, $picker, $reciever, $order, $products);
        return $res;
    }
    public function createOrderDelivery($info, $picker, $reciever, $order, $products)
    {
        $orderSend = [
            "id" => 'DH' . uniqid() . $order['id'],
            "pick_name" => $picker['name'],
            "pick_address" => $picker['address'],
            "pick_province" => $picker['province']['name'],
            "pick_district" => $picker['district']['name'],
            "pick_ward" => $picker['ward']['name'],
            "pick_tel" => $picker['phone'],
            "tel" => $reciever['phone'],
            "name" => $reciever['name'],
            "address" => $reciever['address'],
            "province" =>  $reciever['province1']['name'],
            "district" => $reciever['district1']['name'],
            "ward" => $reciever['ward1']['name'],
            "hamlet" => "Khác",
            "is_freeship" => "1",
            "pick_date" => date('Y-m-y', strtotime($order['date'])),
            "pick_money" => $order['total_price'],
            "note" => $order['note'],
            // "weight_option" => "gram",
            "weight" => $info['weight'] / 1000,
            "value" => $info['value_order'],
            "transport" => "road", // fly: máy bay, road : đường bộ
            "pick_option" => $info['pick_option'], //mặc định là cod: đơn vị vận chuyển tới lấy, post giao tới bưu cục
            "deliver_option" => $info['deliver_option'], // xteam là vận chuyển nhanh
            "pick_session" => 2,
            "tags" => [
                1 // 1 hàng dễ vỡ 7 hàng nông sản
            ]
        ];
        $body = [
            "products" => $products,
            'order' => $orderSend
        ];
        $body = json_encode($body);
        $token = "6CeA7D6583b913E8e58FA9C24066EE3763cF557e";

        $url = env('URL_GHTK',"https://services.ghtklab.com/services/shipment/order");
        $client = new Client([
            'headers' => [
                'token' => $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'body' => $body,

            ],
        );

        $response = json_decode($req->getBody()->getContents(), true);
        return $response;
    }
    //confirm trạng thái chờ xác nhận
    public function confirm_order(Request $request)
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('d-m-Y');
        $data_order = OrderModel::find($request->id);
        $res =  $this->prepareCreateDelivery($data_order, $request->all());
        if (!$res['success']) {
            $dataReturn = [
                'status' => false,
                'msg' => 'Lên đơn vận chuyển thất bại !'
            ];
            return $dataReturn;
        }
        //data ghtkk
        $data_order->partner_id = $res['order']['partner_id'];
        $data_order->label = $res['order']['label'];
        $data_order->tracking_id = $res['order']['tracking_id'];
        $data_order->sorting_code = $res['order']['sorting_code'];
        //data ghtkk

        $created_at = $data_order->created_at;
        $time_order = date('d-m-Y', strtotime($created_at));
        $time_order_3_day = $created_at->addDay(3)->format('d-m-Y');
        $time_pick_format = date('d-m-Y', strtotime($request->time_pick));
        if (strtotime($time_pick_format) > strtotime($time_order_3_day)) {
            $dataReturn = [
                'status' => false,
                'msg' => 'Thời gian dự kiến lấy hàng tối đa là 3 ngày !'
            ];
            return $dataReturn;
        }
        if (strtotime($time_pick_format) < strtotime($time_order)) {
            $dataReturn = [
                'status' => false,
                'msg' => 'Thời gian dự kiến lấy hàng không được nhỏ hơn ngày đặt hàng !'
            ];
            return $dataReturn;
        }
        if (strtotime($time_pick_format) < strtotime($today)) {
            $dataReturn = [
                'status' => false,
                'msg' => 'Thời gian lấy hàng không được nhỏ hơn ngày hiện tại !'
            ];
            return $dataReturn;
        }
        $data_address_pick = CustomerAddressModel::find($request->address_pick);
        $data_order->address_pick_id = $request->address_pick;
        $data_order->address_pick = $data_address_pick->address;
        $data_order->name_address_pick = $data_address_pick->name;
        $data_order->phone_address_pick = $data_address_pick->phone;
        $time_pick = date('Y-m-d', strtotime($request->time_pick));
        $data_order->time_pick = $time_pick;
        $data_order->form_shipping = $request->form_shipping;
        $data_order->status = 1;
        $data_order->shipping_confirm = 1;
        $data_order->save();
        $dataReturn = [
            'status' => true,
            'msg' => 'Xác nhận lấy hàng thành công !'
        ];
        return $dataReturn;
    }

    //confirm trạng thái chờ lấy hàng
    public function confirm_wait_shipping(Request $request)
    {
        $data_order = OrderModel::find($request->id);
        $dataReturn = [
            'status' => true,
            'msg' => 'Xác nhận lấy hàng thành công !'
        ];
        return $dataReturn;
    }

    //tìm kiếm theo mã đơn hàng
    public function search_order(Request $request)
    {
        $order_code_1 = $request->order_code;
        $order_code = str_replace(' ', '%', $order_code_1);
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $today_start = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 00:00:00');
        $today_end = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 23:59:59');
        $data_login = session()->get('data_supplier');
        $data_address_supplier = CustomerAddressModel::where('type_pick_up', 1)->orderBy('id', 'desc')->where('customer_id', $data_login->customer_id)->first();
        $data_address = CustomerAddressModel::where('customer_id', $data_login->customer_id)->where('type_pick_up', 0)->get();
        $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
        $data_order = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)->where('order_code', 'like', '%' . $order_code . '%')->orderBy('id', 'desc')->get();
        $total_row = $data_order->count();
        if ($data_order) {
            //Quá hạn mà ko xác nhận
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 0) {
                    $time_check_10_min = $v->created_at;
                    $time_10_min = $time_check_10_min->addMinute(10)->format('Y-m-d H:i:s');
                    if (strtotime($today) > strtotime($time_10_min)) {
                        $status_shipping = true;
                    } else {
                        $status_shipping = false;
                    }
                    if ($status_shipping) {
                        $data_order[$k]['status'] = 1;
                        $data_order[$k]['shipping_confirm'] = 0;
                        $data_order[$k]->save();
                    }
                }
            }
            //end sau 10phut chuyển status

            //Quá hạn mà ko xác nhận
            foreach ($data_order as $k => $v) {
                $time_order = $v->created_at;
                $time_qua_han = $time_order->addDay(1)->format('Y-m-d H:i:s');
                if ($v['status'] == 1 && $v['shipping_confirm'] == 0) {
                    if (strtotime($time_qua_han) >= strtotime($today_start) && strtotime($time_qua_han) <= strtotime($today_end)) {
                        $qua_han = true;
                    } else {
                        $qua_han = false;
                    }
                    if ($qua_han) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn mà ko xác nhận


            //Quá hạn lấy hàng
            foreach ($data_order as $k => $v) {
                if ($v['status'] == 1 && $v['shipping_confirm'] == 1) {
                    $time_qua_han_lay_hang = Carbon::parse($v->time_pick)->addDay(1)->format('Y-m-d H:i:s');
                    if (strtotime($time_qua_han_lay_hang) >= strtotime($today_start) && strtotime($time_qua_han_lay_hang) <= strtotime($today_end)) {
                        $qua_han_lay_hang = true;
                    } else {
                        $qua_han_lay_hang = false;
                    }
                    if ($qua_han_lay_hang) {
                        $data_order[$k]['status'] = 4;
                        $data_order[$k]['cancel_user'] = 0;
                        $data_order[$k]['cancel_confirm'] = 1;
                        $data_order[$k]->save();
                    }
                }
            }
            //end quá hạn lấy hàng

            //get data
            foreach ($data_order as $key => $value) {
                $time_order = $value->created_at;
                $time_3_day = $time_order->addDay(3)->format('Y-m-d H:i:s');
                $time_check_10_min = $value->created_at;
                $time_10_min = $time_check_10_min->addMinute(2)->format('Y-m-d H:i:s');
                $data_order[$key]['today'] = $today;
                $data_order[$key]['time_10_min'] = $time_10_min;
                $data_order[$key]['time_3_day'] = $time_3_day;
                $data_order[$key]['shop_name'] = $data_shop->name;
                $data_order[$key]['shop_image'] = $data_shop->image;

                if ($value->collaborator_id == 1) {
                    $data_order[$key]['order_dropship'] = 'Hàng Dropship';
                } else {
                    $data_order[$key]['order_dropship'] = 'Hàng tại kho';
                }

                if ($value['status'] == 0) {
                    $data_order[$key]['is_status'] = 'Chờ xác nhận';
                } elseif ($value['status'] == 1) {
                    //rủi ro giao hàng trễ (khi quá 1 ngày từ ngày dự kiến lấy hàng)
                    $time_rui_ro = Carbon::parse($value->time_pick)->subDay(1)->format('Y-m-d');
                    if (strtotime($time_rui_ro) >= strtotime($today_start) && strtotime($time_rui_ro) <= strtotime($today_end) && $value['time_pick'] != null && $value['address_pick_id'] != null) {
                        $data_order[$key]['rui_ro'] = true;
                    } else {
                        $data_order[$key]['rui_ro'] = false;
                    }
                    //end rui ro

                    $data_order[$key]['is_status'] = 'Chờ lấy hàng';
                    $data_order[$key]['notification_status'] = 'Để tránh việc giao hàng trễ, vui lòng giao hàng hoặc chuẩn bị hàng trước ' . date('d-m-Y', strtotime($value->time_pick)) . '';
                } elseif ($value['status'] == 2) {
                    $data_order[$key]['is_status'] = 'Đã giao cho ĐVVC';
                } elseif ($value['status'] == 3) {
                    $data_order[$key]['is_status'] = 'Đã giao hàng';
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 0) {
                    $data_order[$key]['is_status'] = 'Yêu cầu huỷ đơn hàng';
                    if ($value['cancel_reason'] == 0) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi địa chỉ hàng';
                    } elseif ($value['cancel_reason'] == 1) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn nhập mã Voucher';
                    } elseif ($value['cancel_reason'] == 2) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Muốn thay đổi sản phẩm trong đơn hàng';
                    } elseif ($value['cancel_reason'] == 3) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Thủ tục thanh toán quá rắc rối';
                    } elseif ($value['cancel_reason'] == 4) {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Tìm thấy giá rẻ hơn ở chỗ khác';
                    } else {
                        $data_order[$key]['notification_status'] = 'Lý do: ' . 'Lý do khác';
                    }
                } elseif ($value['status'] == 4 && $value['cancel_confirm'] == 1) {
                    $data_order[$key]['is_status'] = 'Đã huỷ';
                    if ($value['cancel_user'] == 0) {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi hệ thống';
                    } else {
                        $data_order[$key]['user_cancel'] = 'Đã huỷ bởi người mua';
                    }
                } else {
                    $data_order[$key]['is_status'] = 'Hoàn tiền';
                }
                if ($value['payment_id'] == 0) {
                    $data_order[$key]['payment'] = 'Ví CTV24h';
                } elseif ($value['payment_id'] == 1) {
                    $data_order[$key]['payment'] = 'Trả góp';
                } elseif ($value['payment_id'] == 2) {
                    $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
                } else {
                    $data_order[$key]['payment'] = 'Thẻ tín dụng hoặc thẻ ghi nợ';
                }
                $data_shipping = ShippingModel::find($value['shipping_id']);
                $data_order[$key]['shipping'] = $data_shipping['name'];
            }
            $data_order_detail = OrderDetailModel::where('shop_id', $data_shop->id)->orderBy('id', 'desc')->get();
            foreach ($data_order_detail as $k => $v) {
                $data_product = ProductModel::find($v->product_id);
                $data_order_detail[$k]['product_name'] = $data_product->name;
                $data_order_detail[$k]['product_image'] = $data_product->image;
            }
            $dataReturn = [
                'status' => true,
                'data_order' => $data_order,
                'data_order_detail' => $data_order_detail,
                'total_data' => $total_row ?? 0,
                'title' => 'Tìm kiếm đơn hàng: ' . $order_code,
                'order_code' => $order_code,
                'address' => $data_address,
                'data_address' => $data_address_supplier
            ];
        }
        if ($order_code == null) {
            $dataReturn = [
                'status' => true,
                'data_order' => null,
                'data_order_detail' => null,
                'total_data' => 0,
                'title' => 'Tìm kiếm đơn hàng: ',
                'order_code' => $order_code,
                'data_address' => null,
                'address' => null
            ];
        }
        return view('supplier.order.search', $dataReturn);
    }

    public function update_status(Request $request)
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $data_order = OrderModel::find($request->order_id);
        $data_order->status = 3;
        $data_order->status_confirm = 0; //Đã giao hàng, khách chưa xác nhận đã nhận hàng
        $data_order->created_at = $today;
        $data_order->save();
        $dataReturn = [
            'status' => true,
            'msg' => 'Giao hàng thành công',
            'url' => url('admin/order/all')
        ];
        return $dataReturn;
    }

    public function update_shipping(Request $request)
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $data_order = OrderModel::find($request->order_id);
        $data_order->status = 2;
        $data_order->created_at = $today;
        $data_order->save();
        $dataReturn = [
            'status' => true,
            'msg' => 'Lấy hàng thành công !',
            'url' => url('admin/order/all')
        ];
        return $dataReturn;
    }

    public function show_review($id)
    {
        $data_order = OrderModel::find($id);
        $data_review = DB::table('review_product')
            ->select('review_product.*', 'product.name as product_name', 'product.image as product_image', 'review_product.created_at as date_review')
            ->join('order_detail', 'order_detail.id', '=', 'review_product.order_detail_id')
            ->join('order', 'order.id', '=', 'order_detail.order_id')
            ->join('product', 'product.id', '=', 'review_product.product_id')
            ->where('order_detail.order_id', '=', $data_order->id)
            ->get();
        $dataReturn = [
            'status' => true,
            'data_review' => isset($data_review) ? $data_review : null,
            'data_order' => $data_order
        ];
        return view('supplier.order.review', $dataReturn);
    }
}
