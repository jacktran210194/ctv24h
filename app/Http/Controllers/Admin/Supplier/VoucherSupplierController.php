<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use App\Models\VoucherCategoryModel;
use App\Models\VoucherModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class VoucherSupplierController extends Controller
{
    /**
     *  Danh sách mã giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexVoucher(){
        $data_supplier = session()->get('data_supplier');
        $data = VoucherModel::where('supplier_id', $data_supplier->id)->orderBy('id', 'desc')->get();
        if ($data) {
            foreach ($data as $k => $v){
                $today = Carbon::now('Asia/Ho_Chi_Minh');
                $category = VoucherCategoryModel::find($v['category_id']);
                $data[$k]['cat_name'] = $category['name'] ?? '';
                if ($v->time_end > $today && $v->time_start < $today){
                    $data[$k]['status'] = 'Đang diễn ra';
                    $data[$k]['color'] = 'success';
                } else if ($v->time_end > $today && $v->time_start > $today) {
                    $data[$k]['status'] = 'Sắp diễn ra';
                    $data[$k]['color'] = 'warning';
                } else if ($v->time_end < $today && $v->time_start < $today) {
                    $data[$k]['status'] = 'Đã diễn ra';
                    $data[$k]['color'] = 'danger';
                }
            }
        }
        $dataReturn = [
            'data' => $data,
            'title' => 'Voucher',
            'voucher' => true
        ];
        return view('supplier.voucher.index', $dataReturn);
    }

    /**
     *  Thêm mã giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addVoucher()
    {
        $dataUser = session()->get('data_supplier');
        $data_cate = VoucherCategoryModel::where('supplier_id', $dataUser->id)->orderBy('id', 'desc')->get();
        $data_return = [
            'category' => $data_cate,
            'title' => 'Voucher',
            'voucher' => true
        ];
        return view('supplier.voucher.form', $data_return);
    }

    /**
     *  Lưu mã giảm giá
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveVoucher(Request $request)
    {
        if ($request->id) {
            $data_return = $this->updateVoucher($request);
        } else {
            $data_return = $this->createVoucher($request);
        }
        return response()->json($data_return, Response::HTTP_OK);
    }

    /**
     * @param $request
     * @param $rules
     * @param $customMessages
     * @return array
     */
    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /**
     *  Thêm mã giảm giá
     * @param $request
     * @return array
     */
    public function createVoucher($request){
        $rules = [
            'category_id' => 'required',
            'title' => 'required',
            'content' => 'required',
            'code' => 'regex:/(^([a-zA-Z0-9]+)(\d+)?$)/u|required',
            'time_start' => 'required|after:today',
            'time_end' => 'required',
            'number' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'time_start.after' => 'Ngày bắt đầu không được nhỏ hơn ngày hiện tại',
            'regex' => 'Mã giảm giá không được nhập ký tự đặc biệt !',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            if ($request->time_end < $request->time_start){
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Ngày kết thúc phải lớn hơn ngày bắt đầu'
                ];
                return $dataReturn;
            } else {
                $data_supplier = session()->get('data_supplier');
                $title = $request->title;
                $content = $request->content;
                $code = $request->code;
                $time_start = $request->time_start;
                $time_end = $request->time_end;
                $category_id = $request->category_id;
                $number = $request->number;
                $supplier_id = $data_supplier->id;
                $data = VoucherModel::create([
                    'title' => $title,
                    'content' => $content,
                    'code' => $code,
                    'time_start' => $time_start,
                    'time_end' => $time_end,
                    'category_id' => $category_id,
                    'number' => $number,
                    'supplier_id' => $supplier_id ,
                    'image' => ''
                ]);
                $data->save();
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/voucher/';
                $file->move($path, $fileName);
                $data->image = '/uploads/images/voucher/' . $fileName;
                $data->save();
            }
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/my_voucher'),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Sửa mã giảm giá
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function editVoucher(Request $request){
        $dataUser = session()->get('data_supplier');
        $data = VoucherModel::find($request->id);
        $data_cate = VoucherCategoryModel::where('supplier_id', $dataUser->id)->orderBy('id', 'desc')->get();
        $data_return = [
            'title' => 'Voucher',
            'voucher' => true,
            'data' => $data,
            'category' => $data_cate,
        ];
        return view('supplier.voucher.form', $data_return);
    }

    /**
     *  Cập nhật mã giảm giá
     * @param $request
     * @return array
     */
    public function updateVoucher($request){
        $rules = [
            'category_id' => 'required',
            'title' => 'required',
            'content' => 'required',
            'code' => 'required',
            'time_start' => 'required|after:today',
            'time_end' => 'required',
            'number' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'time_start.after' => 'Ngày bắt đầu không được nhỏ hơn ngày hiện tại'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data = VoucherModel::find($request->id);

        if ($data) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/voucher/';
                $file->move($path, $fileName);
                $data->image = '/uploads/images/voucher/' . $fileName;
                $data->save();
            }
            $data_supplier = session()->get('data_supplier');
            $data->title = $request->title;
            $data->content = $request->content;
            $data->code = $request->code;
            $data->time_start = $request->time_start;
            $data->time_end = $request->time_end;
            $data->category_id = $request->category_id;
            $data->number = $request->number;
            $data->supplier_id = $data_supplier->id;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/my_voucher'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    /**
     *  Xóa mã giảm giá
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteVoucher($id)
    {
        try {
            $data = VoucherModel::find($id);
            if ($data) {
                File::delete($data->image);
            }
            VoucherModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }


    /** Danh mục mã giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexCategory()
    {
        $dataUser = session()->get('data_supplier');
        if(isset($dataUser)){
            $data = VoucherCategoryModel::where('supplier_id', $dataUser->id)
                ->orderBy('id', 'desc')
                ->get();
        }
        $data_return = [
            'voucher_category' => true,
            'data' => $data,
            'title' => 'Voucher',
        ];
        return view('supplier.voucher.voucher_category_index', $data_return);
    }

    /** Thêm danh mục mã giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addCategory()
    {
        $data_return = [
            'title' => 'Voucher',
            'voucher_category' => true
        ];
        return view('supplier.voucher.voucher_category_form', $data_return);
    }

    /**
     * Lưu thông tin danh mục mã giảm giá
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveCategory(Request $request)
    {
        if ($request->id) {
            $data_return = $this->updateCategory($request);
        } else {
            $data_return = $this->createCategory($request);
        }
        return response()->json($data_return, Response::HTTP_OK);
    }

    /**
     *  Thêm danh mục mã giảm giá
     * @param $request
     * @return array
     */
    public function createCategory($request){
        $rules = [
            'name' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000', // max 10000kb = 10MB
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'image.mimes' => 'Không đúng định dạng ảnh',
            'image.max' => 'Dung lượng ảnh vượt quá 10MB'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $dataUser = session()->get('data_supplier');
            $name = $request->name;
            $supplier_id = $dataUser->id;
            $data = VoucherCategoryModel::create([
                'name' => $name,
                'supplier_id' => $supplier_id,
                'image' => ''
            ]);
            $data->save();
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/voucher_category/';
            $file->move($path, $fileName);
            $data->image = '/uploads/images/voucher_category/' . $fileName;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/my_voucher/category'),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Sửa danh mục mã giảm giá
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function editCategory(Request $request){
        $data = VoucherCategoryModel::find($request->id);
        $data_return = [
            'title' => 'Voucher',
            'voucher_category' => true,
            'data' => $data,
        ];
        return view('supplier.voucher.voucher_category_form', $data_return);
    }

    /**
     *  Cập nhật danh mục mã giảm giá
     * @param $request
     * @return array
     */
    public function updateCategory($request){
        $rules = [
            'name' => 'required',
            'image' => 'max:10000', // max 10000kb = 10MB
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'image.max' => 'Dung lượng ảnh vượt quá 10MB'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data = VoucherCategoryModel::find($request->id);

        if ($data) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/voucher_category/';
                $file->move($path, $fileName);
                $data->image = '/uploads/images/voucher_category/' . $fileName;
                $data->save();
            }

            $data->name = $request->name;
            $data->active = $request->active;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/my_voucher/category'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    /**
     *  Xóa danh mục mã giảm giá
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCategry($id)
    {
        try {
            $data = VoucherCategoryModel::find($id);
            if ($data) {
                File::delete($data->image);
            }
            VoucherCategoryModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
