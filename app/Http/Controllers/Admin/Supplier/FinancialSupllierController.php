<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Exports\Collaborator\RevenueDonePaymentExport;
use App\Exports\Collaborator\RevenueWaitingPaymentExport;
use App\Exports\Supplier\HistoryPaymentSupplierExport;
use App\Exports\Supplier\RevenueDonePaymentSupplierExport;
use App\Http\Controllers\Controller;
use App\Models\AddressCustomerCTVModel;
use App\Models\BankAccountModel;
use App\Models\CityModel;
use App\Models\CustomerAddressModel;
use App\Models\CustomerModel;
use App\Models\DistrictModel;
use App\Models\HistoryOrderPaymentModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\PaymentModel;
use App\Models\PointModel;
use App\Models\ProductModel;
use App\Models\ShopModel;
use App\Models\TheBankModel;
use App\Models\WardModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\OLE\PPS;

class FinancialSupllierController extends Controller
{
    public function indexBanking()
    {
        $dataUser = session()->get('data_supplier');
        $dataWallet = CustomerModel::where('id', $dataUser->customer_id)->first();
        $data = BankAccountModel::where('customer_id', '=', $dataUser->customer_id)->orderBy('id', 'desc')->get();
        foreach ($data as $k => $v) {
            $dataBank = TheBankModel::where('id', $v['bank_name'])->first();
            $data[$k]['bank_name'] = $dataBank['name'] ?? '';
            $data[$k]['short_name'] = $dataBank['short_name'] ?? '';
        }
        $data_return = [
            'supplier' => true,
            'data' => $data,
            'dataWallet' => $dataWallet,
            'title' => 'Tài khoản ngân hàng',
        ];
        return view('supplier.financial.banking.index', $data_return);
    }

    /**
     *  Thêm tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addBanking(Request $request)
    {
        $dataUser = session()->get('data_supplier');
        $data = BankAccountModel::where('customer_id', '=', $dataUser->customer_id)->orderBy('id', 'desc')->get();
        foreach ($data as $k => $v) {
            $dataBank = TheBankModel::where('id', $v['bank_name'])->first();
            $data[$k]['bank_name'] = $dataBank['name'] ?? '';
            $data[$k]['short_name'] = $dataBank['short_name'] ?? '';
            $str = $v->bank_account;
            $arr_first = substr($str, 0, 4);
            $arr_last = substr($str, -4, 4);
            $data[$k]->first_num_acc = $arr_first;
            $data[$k]->last_num_acc = $arr_last;
        }
        $dataCustomer = session()->get('data_supplier');
        $dataSupplier = CustomerModel::find($dataCustomer->customer_id);
        $dataBank = TheBankModel::orderBy('id', 'desc')->get();

        $data_return = [
            'title' => 'Thêm tài khoản ngân hàng',
            'dataBank' => $dataBank,
            'dataSupplier' => $dataSupplier,
            'dataBankAccount' => $data,
        ];
        return view('supplier.financial.banking.form', $data_return);
    }

    /**
     *  Thông tin tài khoản ngân hàng
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function infoBankAccount(Request $request, $id)
    {
        $dataInfoBankAcc = BankAccountModel::find($id);
        $dataBank = TheBankModel::find($dataInfoBankAcc->bank_name);
        $dataInfoBankAcc->bank_name = $dataBank->name ?? '';
        $dataInfoBankAcc->short_name = $dataBank->short_name ?? '';
        $str = $dataInfoBankAcc->bank_account;
        $arr_last = substr($str, -3, 3);
        $dataInfoBankAcc->last_num_acc = $arr_last;
        $dataCity = CityModel::find($dataInfoBankAcc->branch);
        $dataInfoBankAcc->branch = $dataCity->name ?? '';
        $data_return = [
            'status' => true,
            'msg' => 'OK',
            'dataInfoBankAcc' => $dataInfoBankAcc,
        ];
        return view('supplier.financial.banking.detail', $data_return);
    }

    /**
     *  Đặt tài khoản làm mặc định
     * @param Request $request
     * @return array
     *
     */
    public function checkBankAccount(Request $request)
    {
        $dataLogin = session()->get('data_supplier');
        BankAccountModel::where('customer_id', $dataLogin->customer_id)->update(['is_default' => 0]);
        BankAccountModel::where('customer_id', $dataLogin->customer_id)->where('id', $request->bank_id)->update(['is_default' => 1]);
        $dataReturn = [
            'status' => true,
            'msg' => 'Thành công',
        ];
        return $dataReturn;
    }

    /**
     *  Thêm tài khoản ngân hàng
     * @param Request $request
     * @return array
     */
    public function createBanking(Request $request)
    {
        $dataUser = session()->get('data_supplier');
        $rules = [
            'user_name' => 'required',
            'bank_name' => 'required',
            'bank_account' => 'required|unique:bank_account,bank_account,' . $request->id,
            'identify_card' => 'required|max:12',
            'branch' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'bank_account.unique' => 'Số tài khoản đã tồn tại',
            'identify_card.max' => 'Số CMND không được vượt quá 12 số'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $data = new BankAccountModel();
            $data->customer_id = $dataUser->customer_id;
            $data->user_name = $request->user_name;
            $data->bank_name = $request->bank_name;
            $data->bank_account = $request->bank_account;
            $data->identify_card = $request->identify_card;
            $data->branch = $request->branch;
//            $data->save();
            $dataReturn = [
                'status' => true,
                'msg' => 'Thêm tài khoản ngân hàng thành công',
                'url' => url('admin/manage_financial/wallet'),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Lưu tài khoản ngân hàng
     * @param Request $request
     * @return array
     */
    public function saveBankAccount(Request $request)
    {
        $dataUser = session()->get('data_supplier');
        $data = new BankAccountModel();
        $data->customer_id = $dataUser->customer_id;
        $data->user_name = $request->user_name;
        $data->bank_name = $request->bank_name;
        $data->bank_account = $request->bank_account;
        $data->identify_card = $request->identify_card;
        $data->branch = $request->branch;
        $data->save();
        $dataReturn = [
            'status' => true,
            'msg' => 'Thêm tài khoản ngân hàng thành công',
            'url' => url('admin/manage_financial/wallet'),
        ];
        return $dataReturn;
    }

    /**
     * Hủy liên kết tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteBanking(Request $request)
    {
        try {
            BankAccountModel::destroy($request->id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Hủy liên kết thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     *  Liên kết tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function linkBanking(Request $request)
    {
        $data = BankAccountModel::where('id', $request->id)->update(['is_active' => 1]);
        $dataReturn = [
            'status' => true,
            'msg' => 'Liên kết thành công',
            'data' => $data
        ];
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     *  Hủy liên kết tài khoản ngân hàng
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unlinkBanking(Request $request)
    {
        $data = BankAccountModel::where('id', $request->id)->update(['is_active' => 0]);
        $dataReturn = [
            'status' => true,
            'msg' => 'Hủy liên kết thành công',
            'data' => $data
        ];
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     *  Lịch sử giao dịch
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function payment(Request $request)
    {
        $dataUser = session()->get('data_supplier');
        $data_withdrawal = DB::table('payment')
            ->select('payment.*', 'bank_account.*', 'payment.created_at as created_at', 'the_bank.name as name_bank', 'the_bank.short_name as short_name_bank', 'bank_account.bank_name as bank_name')
            ->join('bank_account', 'payment.account', '=', 'bank_account.id')
            ->join('the_bank', 'bank_account.bank_name', '=', 'the_bank.id')
            ->where('bank_account.customer_id', $dataUser->customer_id)
            ->where('payment.type', 0)
            ->orderBy('payment.id', 'desc')
            ->get();
        $data_return = [
            'title' => 'Lịch sử rút tiền',
            'data_withdrawal' => $data_withdrawal
        ];
        return view('supplier.financial.wallet.history_payment', $data_return);
    }

    /**
     *  Xuất báo lịch sử rút tiền
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportWithdrawal()
    {
        return Excel::download(new HistoryPaymentSupplierExport(), 'History_Payment.xlsx');
    }

    /**
     *  Rút tiền
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function withdrawl()
    {
        $dataUser = session()->get('data_supplier');
        $dataSupplier = CustomerModel::where('id', $dataUser->customer_id)->first();
        $dataAccount = BankAccountModel::where('customer_id', $dataUser->customer_id)->where('is_active', 1)->orderBy('id', 'desc')->get();
        foreach ($dataAccount as $k => $v) {
            $bank = TheBankModel::find($v['bank_name']);
            $dataAccount[$k]['bank_name'] = $bank['name'] ?? '';
            $dataAccount[$k]['short_name'] = $bank['short_name'] ?? '';
        }

        $dataBank = TheBankModel::orderBy('id', 'desc')->get();
        $dataReturn = [
            'supplier' => true,
            'dataBank' => $dataBank,
            'dataAccount' => $dataAccount,
            'data' => $dataSupplier,
            'title' => 'Yêu cầu rút tiền',
        ];
        return view('supplier.financial.wallet.withdrawl', $dataReturn);
    }

    /**
     *  Rút tiền
     * @param Request $request
     * @return array
     */
    public function createWithdrawal(Request $request)
    {
        $rules = [
            'price' => 'required',
            'bank_id' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $dataUser = session()->get('data_supplier');
            $dataWallet = CustomerModel::find($dataUser->customer_id);
            if (($request->price + 11000) > $dataWallet->wallet) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Số tiền không đủ để thực hiện giao dịch này.'
                ];
            } else {
                $today = Carbon::now('Asia/Ho_Chi_Minh');
                $data = new PaymentModel([
                    'title' => $request->title,
                    'account' => $request->bank_id,
                    'price' => $request->price,
                    'type' => 0,
                    'created_at' => $today
                ]);
                $dataReturn = [
                    'msg' => 'Rút tiền thành công',
                    'status' => true,
                    'url' => url('admin/manage_financial/wallet'),
                ];
            }
        }
        return $dataReturn;
    }

    /**
     *  Xác nhận rút tiền
     * @param Request $request
     * @return array
     */
    public function saveWithdrawal(Request $request)
    {
        $rules = [
            'pin_code_input' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng nhập mã Pin',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $dataLogin = session()->get('data_supplier');
            $dataCustomer = CustomerModel::find($dataLogin->customer_id);
            if (md5($request->pin_code_input) != $dataCustomer->pin_code) {
                $dataReturn = [
                    'msg' => 'Mã pin không chính xác'
                ];
                return $dataReturn;
            } else {
                $today = Carbon::now('Asia/Ho_Chi_Minh');
                $dataPayment = new PaymentModel([
                    'title' => $request->title,
                    'account' => $request->bank_id,
                    'price' => $request->price,
                    'type' => 0,
                    'created_at' => $today
                ]);
                $dataPayment->save();
                $dataPrice = $dataCustomer->wallet - ($request->price + 11000);
                CustomerModel::where('id', $dataLogin->customer_id)->update(['wallet' => $dataPrice]);
                $dataReturn = [
                    'msg' => 'Rút tiền thành công',
                    'status' => true,
                    'url' => url('admin/manage_financial/wallet'),
                ];
                return $dataReturn;
            }
        }
    }

    /**
     *  Lịch sử điểm
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     *
     */
    public function historyPoint(Request $request)
    {
        $dataUser = session()->get('data_supplier');
        $dataHistoryPoint = PointModel::where('customer_id', $dataUser->customer_id)
            ->orderBy('id', 'desc')
            ->get();
        foreach ($dataHistoryPoint as $k => $v) {
            $dataSupplier = CustomerModel::find($v['customer_id']);
            $dataHistoryPoint[$k]['name'] = $dataSupplier['name'] ?? '';
            if($v->type == 0){
                $dataHistoryPoint[$k]->type = '-';
            } else {
                $dataHistoryPoint[$k]->type = '+';
            }
        }
        $data_return = [
            'title' => 'Lịch sử điểm',
            'dataHistoryPoint' => $dataHistoryPoint,
        ];
        return view('supplier.financial.wallet.history_point', $data_return);
    }

    /**
     *  Đổi điểm
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function changePoint()
    {
        $dataUser = session()->get('data_supplier');
        $data = CustomerModel::where('id', $dataUser->customer_id)->first();
        $dataReturn = [
            'supplier' => true,
            'data' => $data,
            'title' => 'Đổi điểm',
        ];
        return view('supplier.financial.wallet.change_point', $dataReturn);
    }

    /**
     *  Đổi điểm
     * @param Request $request
     * @return array
     */
    public function savePoint(Request $request)
    {
        $rules = [
            'point' => 'required',
            'title' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $dataUser = session()->get('data_supplier');
            $dataPoint = CustomerModel::where('id', $dataUser->customer_id)->first();
            if ($request->point > $dataPoint->point) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Số điểm của bạn không đủ để thực hiện giao dịch này.'
                ];
            } else {
                $today = Carbon::now('Asia/Ho_Chi_Minh');
                $data = new PointModel([
                    'point' => $request->point,
                    'title' => $request->title,
                    'customer_id' => $dataUser->customer_id,
                    'type' => 0,
                    'active' => 1,
                    'date' => $today
                ]);
                $data->save();
                $data->point = $dataPoint->point - $request->point;
                CustomerModel::where('id', $dataUser->customer_id)->update(['point' => $data->point]);
                $change = $dataPoint->wallet + ($request->point * 1000);
                CustomerModel::where('id', $dataUser->customer_id)->update(['wallet' => $change]);
                $dataReturn = [
                    'status' => true,
                    'url' => url('admin/manage_financial/wallet'),
                ];

            }
        }
        return $dataReturn;
    }

    /**
     *
     *  Doanh thu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexRevenue(Request $request)
    {
        $dataUser = session()->get('data_supplier');
        $dataReceived = DB::table('financial_revenue')
            ->distinct('order_detail.shop_id')
            ->select('order.*', 'order.total_price as total_price', 'financial_revenue.status as status')
            ->join('order', 'financial_revenue.order_id', '=', 'order.id')
            ->join('order_detail', 'order.id', '=', 'order_detail.order_id')
            ->join('shop', 'order_detail.shop_id', '=', 'shop.id')
            ->where('order.status', '=', 4)
            ->where('shop.supplier_id', '=', $dataUser->id)
            ->orderBy('order.id', 'desc')
            ->get();
        if ($dataReceived) {
            foreach ($dataReceived as $k => $v) {
                if ($v->status == 0) {
                    $dataReceived[$k]->status = 'Đang xử lý';
                    $dataReceived[$k]->color = 'warning';
                } else if ($v->status == 1) {
                    $dataReceived[$k]->status = 'Hoàn thành';
                    $dataReceived[$k]->color = 'success';
                } else {
                    $dataReceived[$k]->status = 'Đã hủy';
                    $dataReceived[$k]->color = 'danger';
                }
            }
        }
        $dataBankAccount = BankAccountModel::where('customer_id', $dataUser->customer_id)->orderBy('id', 'desc')->get();
        if ($dataBankAccount) {
            foreach ($dataBankAccount as $k => $v) {
                $dataTheBank = TheBankModel::find($v->bank_name);
                $dataBankAccount[$k]->short_name = $dataTheBank->short_name ?? '';
                $dataBankAccount[$k]->bank_name = $dataTheBank->name ?? '';
                $str = $v->bank_account;
                $arr = substr($str, -6, 6);
                $dataBankAccount[$k]->sub_bank_account = $arr;
            }
        }
        //Đếm doanh thu đã thanh toán
        $countDonePayment = HistoryOrderPaymentModel::where('customer_id', $dataUser->customer_id)
            ->where('type', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        if($countDonePayment){
            $countDonePayment->countDone = 0;
            foreach ($countDonePayment as $kCountDone => $vCountDone){
                $countDonePayment->countDone += $vCountDone->price;
            }
        }
        //End Đếm doanh thu đã thanh toán

        //Đếm doanh thu sẽ thanh toán
        $dataShop = ShopModel::where('customer_id', $dataUser->customer_id)->first();
        $countWaitingPayment = OrderModel::where('status', '<=', 3)
            ->where('shop_id', $dataShop->id)
            ->where('is_selected', 1)
            ->where('status_confirm', 0)
            ->get();
        if ($countWaitingPayment) {
            $countWaitingPayment->countWaiting = 0;
            foreach ($countWaitingPayment as $k => $v) {
                $countWaitingPayment->countWaiting += $v->price_supplier;
            }
        }
        //End Đếm doanh thu sẽ thanh toán

        $dataCustomer = CustomerModel::find($dataUser->customer_id);
        $dataReturn = [
            'dataReceived' => $dataReceived,
            'title' => 'Theo dõi tài chính | Doanh thu',
            'dataBankAccount' => $dataBankAccount,
            'dataCustomer' => $dataCustomer,
            'countDonePayment' => $countDonePayment,
            'countWaitingPayment' => $countWaitingPayment
        ];
        return view('supplier.financial.revenue.index', $dataReturn);
    }

    /**
     *  Doanh thu đã thanh toán
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function filterDone()
    {
        $dataUser = session()->get('data_supplier');
        $data = HistoryOrderPaymentModel::where('customer_id', $dataUser->customer_id)
            ->where('type', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        if($data){
            foreach ($data as $k => $v){
                if($v->is_dropship == 1){
//                    $data[$k]->type_order = 'Hoa hồng';
//                    $data[$k]->color = 'bg-red';
                    $data[$k]->type_order = 'Bán hàng';
                    $data[$k]->color = 'bg-blue';
                    $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->order_id)->first();
                    $data[$k]->name_customer = $dataCustomerCTV->name ?? '';
                } else {
                    $data[$k]->type_order = 'Bán hàng';
                    $data[$k]->color = 'bg-blue';
                    $dataCustomer = CustomerModel::find($v->customer_id);
                    $data[$k]->name_customer = $dataCustomer->name ?? '';
                }
                $dataOrder = OrderModel::find($v->order_id);
                $dataOrderDetail = OrderDetailModel::where('order_id', $dataOrder['id'])->first();
                $dataProduct = ProductModel::find($dataOrderDetail['product_id']);
                $data[$k]->name_product = $dataProduct->name ?? '';
                $data[$k]->image = $dataProduct->image ?? '';
            }
        }
        $dataReturn = [
            'data' => $data,
            'title' => 'Theo dõi tài chính | Doanh thu',
        ];
        return view('supplier.financial.revenue.done_payment', $dataReturn);
    }

    /**
     *  Doanh thu sẽ thanh toán
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function filterWaiting()
    {
        $dataUser = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataUser->customer_id)->first();
        $dataReceived = OrderModel::where('status', '<=', 3)
            ->where('shop_id', $dataShop->id)
            ->where('is_selected', 1)
            ->where('status_confirm', 0)
            ->orderBy('created_at', 'desc')
            ->get();
        if ($dataReceived) {
            foreach ($dataReceived as $k => $v) {
                $dataCustomer = CustomerModel::find($v->customer_id);
                if ($v->collaborator_id == 1) {
//                    $dataReceived[$k]->type_order = 'Hoa hồng' ?? '';
//                    $dataReceived[$k]->color = 'bg-red' ?? '';
                    $dataReceived[$k]->type_order = 'Bán hàng';
                    $dataReceived[$k]->color = 'bg-blue';
                    $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                    $dataReceived[$k]->name_customer = $dataCustomerCTV->name ?? '';
                } else {
                    $dataReceived[$k]->type_order = 'Bán hàng' ?? '';
                    $dataReceived[$k]->color = 'bg-blue' ?? '';
                    $dataReceived[$k]->name_customer = $dataCustomer->name ?? '';
                }
                $dataOrderDetail = OrderDetailModel::where('order_id', $v->id)->first();
                $dataProduct = ProductModel::find($dataOrderDetail->product_id);
                $dataReceived[$k]->name_product = $dataProduct->name ?? '';
                $dataReceived[$k]->image_prd = $dataProduct->image ?? '';
                $dataReceived[$k]->total = ($v->total_price_product) - ($v->fee_ctv24h);
            }
        }
        $dataReturn = [
            'title' => 'Theo dõi tài chính | Doanh thu',
            'data' => $dataReceived,
        ];
        return view('supplier.financial.revenue.waiting_payment', $dataReturn);
    }

    /**
     *  Lọc theo ngày | Doanh thu đã thanh toán
     * @param Request $request
     */
    public function filterDateDonePayment(Request $request)
    {
        $time_start = $request->time_start;
        $time_end = $request->time_end;
        $dataUser = session()->get('data_supplier');
        $output = '';
        if ($time_start != null || $time_end != null) {
            $data = HistoryOrderPaymentModel::where('customer_id', $dataUser->customer_id)
                ->where('type', 1)
                ->where('updated_at', '>=', $time_start)
                ->where('updated_at', '<=', $time_end)
                ->orderBy('created_at', 'desc')
                ->get();
            if($data){
                foreach ($data as $k => $v){
                    $dataCustomer = CustomerModel::find($v->customer_id);
                    $dataOrder = OrderModel::find($v->order_id);
                    if($dataOrder->collaborator_id == 1){
//                        $data[$k]->type_order = 'Hoa hồng';
//                        $data[$k]->color = 'bg-red';
                        $data[$k]->type_order = 'Bán hàng';
                        $data[$k]->color = 'bg-blue';
                        $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                        $data[$k]->name_customer = $dataCustomerCTV->name ?? '';
                    } else {
                        $data[$k]->type_order = 'Bán hàng';
                        $data[$k]->color = 'bg-blue';
                        $data[$k]->name_customer = $dataCustomer->name ?? '';
                    }
                    $dataOrderDetail = OrderDetailModel::where('order_id', $dataOrder->id)->first();
                    $dataProduct = ProductModel::find($dataOrderDetail->product_id);
                    $data[$k]->name_product = $dataProduct->name ?? '';
                    $data[$k]->image = $dataProduct->image ?? '';
                }
            }
        }
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $url_detail_order = '/admin/order/detail/'.$row->order_id;
                $output .= '
                                <tr class="text-center">
                                    <td>
                                        <span class="' . $row->color . '">' . $row->type_order . '</span>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <img style="width: 60px; height: 60px; object-fit: cover; border-radius: 14px; margin-right: 5px;" src="' . $row->image . '" class="image-preview">
                                            <a style="color:#626262;" href="'.$url_detail_order.'">
                                                <span style="width: 200px!important;" class="m-0">' . $row->name_product . '</span>
                                            </a>
                                        </div>
                                    </td>
                                    <td>' . $row->name_customer . '</td>
                                    <td>
                                        <span>' . date('d-m-Y H:i:s', strtotime($row->created_at)) . '</span>
                                        <br>
                                    </td>
                                    <td>Đã nhận được hàng</td>
                                    <td>
                                        <span>' . number_format($row->price) . 'đ</span>
                                        <br>
                                    </td>
                                </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td colspan="5">Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Lọc theo ngày | Doanh thu sẽ thanh toán
     * @param Request $request
     */
    public function filterDateWaitingPayment(Request $request)
    {
        $time_start = $request->time_start;
        $time_end = $request->time_end;
        $dataUser = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataUser->customer_id)->first();
        $output = '';
        if ($time_start == null || $time_end == null) {
            $dataReceived = OrderModel::where('status', '<=', 3)
                ->where('is_selected', 1)
                ->where('shop_id', $dataShop->id)
                ->where('status_confirm', 0)
                ->orderBy('created_at', 'desc')
                ->get();
            if ($dataReceived) {
                foreach ($dataReceived as $k => $v) {
                    $dataCustomer = CustomerModel::find($v->customer_id);
                    if ($v->collaborator_id == 1) {
//                        $dataReceived[$k]->type_order = 'Hoa hồng' ?? '';
//                        $dataReceived[$k]->color = 'bg-red' ?? '';
                        $dataReceived[$k]->type_order = 'Bán hàng' ?? '';
                        $dataReceived[$k]->color = 'bg-blue' ?? '';
                        $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                        $dataReceived[$k]->name_customer = $dataCustomerCTV->name ?? '';
                    } else {
                        $dataReceived[$k]->type_order = 'Bán hàng' ?? '';
                        $dataReceived[$k]->color = 'bg-blue' ?? '';
                        $dataReceived[$k]->name_customer = $dataCustomer->name ?? '';
                    }
                    $dataOrderDetail = OrderDetailModel::where('order_id', $v->id)->first();
                    $dataProduct = ProductModel::find($dataOrderDetail->product_id);
                    $dataReceived[$k]->name_product = $dataProduct->name ?? '';
                    $dataReceived[$k]->image_prd = $dataProduct->image ?? '';
                    $dataReceived[$k]->total = ($v->total_price_product) - ($v->fee_ctv24h);
                }
            }
        } else {
            $dataReceived = OrderModel::where('status', '<=', 3)
                ->where('shop_id', $dataShop->id)
                ->where('is_selected', 1)
                ->where('updated_at', '>=', $time_start)
                ->where('updated_at', '<=', $time_end)
                ->where('status_confirm', 0)
                ->orderBy('created_at', 'desc')
                ->get();
            if ($dataReceived) {
                foreach ($dataReceived as $k => $v) {
                    $dataCustomer = CustomerModel::find($v->customer_id);
                    if ($v->collaborator_id == 1) {
                        $dataReceived[$k]->type_order = 'Hoa hồng' ?? '';
                        $dataReceived[$k]->color = 'bg-red' ?? '';
                        $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                        $dataReceived[$k]->name_customer = $dataCustomerCTV->name ?? '';
                    } else {
                        $dataReceived[$k]->type_order = 'Bán hàng' ?? '';
                        $dataReceived[$k]->color = 'bg-blue' ?? '';
                        $dataReceived[$k]->name_customer = $dataCustomer->name ?? '';
                    }
                    $dataOrderDetail = OrderDetailModel::where('order_id', $v->id)->first();
                    $dataProduct = ProductModel::find($dataOrderDetail->product_id);
                    $dataReceived[$k]->name_product = $dataProduct->name ?? '';
                    $dataReceived[$k]->image_prd = $dataProduct->image ?? '';
                    $dataReceived[$k]->total = ($v->total_price_product) - ($v->fee_ctv24h);
                }
            }
        }
        $total_row = $dataReceived->count();
        if ($total_row > 0) {
            foreach ($dataReceived as $k => $row) {
                $url_detail_order = '/admin/order/detail/'.$row->id;
                $output .= '
                                <tr class="text-center">
                                    <td>
                                        <span class="' . $row->color . '">' . $row->type_order . '</span>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <img style="width: 60px; height: 60px; object-fit: cover; border-radius: 14px; margin-right: 5px;" src="' . $row->image_prd . '" class="image-preview">
                                            <a style="color: #626262;" href="'.$url_detail_order.'">
                                                <span style="width: 200px!important;" class="m-0">' . $row->name_product . '</span>
                                            </a>
                                        </div>
                                    </td>
                                    <td>' . $row->name_customer . '</td>
                                    <td>
                                        <span>' . date('d-m-Y H:i:s', strtotime($row->created_at)) . '</span>
                                        <br>
                                    </td>
                                    <td>Chờ người mua xác nhận đã nhận hàng</td>
                                    <td>
                                        <span>' . number_format($row->total) . 'đ</span>
                                        <br>
                                    </td>
                                </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td colspan="5">Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Lọc theo trạng thái | Đã thanh toán
     * @param $id
     */
    public function filterStatusDone($id)
    {
        $dataUser = session()->get('data_supplier');
        $output = '';
        if ($id == 'all') {
            $data = HistoryOrderPaymentModel::where('customer_id', $dataUser->customer_id)
                ->where('type', 1)
                ->orderBy('created_at', 'desc')
                ->get();
            if($data){
                foreach ($data as $k => $v){
                    $dataCustomer = CustomerModel::find($v->customer_id);
                    $dataOrder = OrderModel::find($v->order_id);
                    if($dataOrder->collaborator_id == 1){
                        $data[$k]->type_order = 'Hoa hồng';
                        $data[$k]->color = 'bg-red';
                        $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                        $data[$k]->name_customer = $dataCustomerCTV->name ?? '';
                    } else {
                        $data[$k]->type_order = 'Bán hàng';
                        $data[$k]->color = 'bg-blue';
                        $data[$k]->name_customer = $dataCustomer->name ?? '';
                    }
                    $dataOrderDetail = OrderDetailModel::where('order_id', $dataOrder->id)->first();
                    $dataProduct = ProductModel::find($dataOrderDetail->product_id);
                    $data[$k]->name_product = $dataProduct->name ?? '';
                    $data[$k]->image = $dataProduct->image ?? '';
                }
            }
        } else if ($id == 1) {
            $data = HistoryOrderPaymentModel::where('customer_id', $dataUser->customer_id)
                ->where('type', 1)
                ->where('is_dropship', 1)
                ->orderBy('created_at', 'desc')
                ->get();
            if($data){
                foreach ($data as $k => $v){
                    $dataCustomer = CustomerModel::find($v->customer_id);
                    $dataOrder = OrderModel::find($v->order_id);
                    if($dataOrder->collaborator_id == 1){
                        $data[$k]->type_order = 'Hoa hồng';
                        $data[$k]->color = 'bg-red';
                        $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                        $data[$k]->name_customer = $dataCustomerCTV->name ?? '';
                    } else {
                        $data[$k]->type_order = 'Bán hàng';
                        $data[$k]->color = 'bg-blue';
                        $data[$k]->name_customer = $dataCustomer->name ?? '';
                    }
                    $dataOrderDetail = OrderDetailModel::where('order_id', $dataOrder->id)->first();
                    $dataProduct = ProductModel::find($dataOrderDetail->product_id);
                    $data[$k]->name_product = $dataProduct->name ?? '';
                    $data[$k]->image = $dataProduct->image ?? '';
                }
            }
        } else {
            $data = HistoryOrderPaymentModel::where('customer_id', $dataUser->customer_id)
                ->where('type', 1)
                ->where('is_dropship', 0)
                ->orderBy('created_at', 'desc')
                ->get();
            if($data){
                foreach ($data as $k => $v){
                    $dataCustomer = CustomerModel::find($v->customer_id);
                    $dataOrder = OrderModel::find($v->order_id);
                    if($dataOrder->collaborator_id == 1){
                        $data[$k]->type_order = 'Hoa hồng';
                        $data[$k]->color = 'bg-red';
                        $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                        $data[$k]->name_customer = $dataCustomerCTV->name ?? '';
                    } else {
                        $data[$k]->type_order = 'Bán hàng';
                        $data[$k]->color = 'bg-blue';
                        $data[$k]->name_customer = $dataCustomer->name ?? '';
                    }
                    $dataOrderDetail = OrderDetailModel::where('order_id', $dataOrder->id)->first();
                    $dataProduct = ProductModel::find($dataOrderDetail->product_id);
                    $data[$k]->name_product = $dataProduct->name ?? '';
                    $data[$k]->image = $dataProduct->image ?? '';
                }
            }
        }

        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $url_order_detail = '/admin/order/detail/'.$row->order_id;
                $output .= '
                                <tr class="text-center">
                                    <td>
                                        <span class="' . $row->color . '">' . $row->type_order . '</span>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <img style="width: 60px; height: 60px; object-fit: cover; border-radius: 14px; margin-right: 5px" src="' . $row->image . '" class="image-preview">
                                            <a style="color:#626262;" href="'.$url_order_detail.'">
                                                <span style="width: 200px!important;" class="m-0">' . $row->name_product . '</span>
                                            </a>
                                        </div>
                                    </td>
                                    <td>' . $row->name_customer . '</td>
                                    <td>
                                        <span>' . date('d-m-Y H:i:s', strtotime($row->created_at)) . '</span>
                                        <br>
                                    </td>
                                    <td>Đã nhận được hàng</td>
                                    <td>
                                        <span>' . number_format($row->price) . 'đ</span>
                                        <br>
                                    </td>
                                </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td colspan="5">Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Lọc theo trạng thái | Sẽ thanh toán
     * @param $id
     */
    public function filterStatusWaiting($id)
    {
        $dataUser = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataUser->customer_id)->first();
        $output = '';
        if ($id == 'all') {
            $dataReceived = OrderModel::where('status', '<=', 3)
                ->where('is_selected', 1)
                ->where('shop_id', $dataShop->id)
                ->where('status_confirm', 0)
                ->orderBy('created_at', 'desc')
                ->get();
            if ($dataReceived) {
                foreach ($dataReceived as $k => $v) {
                    $dataCustomer = CustomerModel::find($v->customer_id);
                    if ($v->collaborator_id == 1) {
                        $dataReceived[$k]->type_order = 'Hoa hồng' ?? '';
                        $dataReceived[$k]->color = 'bg-red' ?? '';
                        $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                        $dataReceived[$k]->name_customer = $dataCustomerCTV->name ?? '';
                    } else {
                        $dataReceived[$k]->type_order = 'Bán hàng' ?? '';
                        $dataReceived[$k]->color = 'bg-blue' ?? '';
                        $dataReceived[$k]->name_customer = $dataCustomer->name ?? '';
                    }
                    $dataOrderDetail = OrderDetailModel::where('order_id', $v->id)->first();
                    $dataProduct = ProductModel::find($dataOrderDetail->product_id);
                    $dataReceived[$k]->name_product = $dataProduct->name ?? '';
                    $dataReceived[$k]->image_prd = $dataProduct->image ?? '';
                    $dataReceived[$k]->total = ($v->total_price_product) - ($v->fee_ctv24h);
                }
            }
        } else if ($id == 1) {
            $dataReceived = OrderModel::where('status', '<=', 3)
                ->where('shop_id', $dataShop->id)
                ->where('is_selected', 1)
                ->where('collaborator_id', 1)
                ->where('status_confirm', 0)
                ->orderBy('created_at', 'desc')
                ->get();
            if ($dataReceived) {
                foreach ($dataReceived as $k => $v) {
                    $dataCustomer = CustomerModel::find($v->customer_id);
                    if ($v->collaborator_id == 1) {
                        $dataReceived[$k]->type_order = 'Hoa hồng' ?? '';
                        $dataReceived[$k]->color = 'bg-red' ?? '';
                        $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                        $dataReceived[$k]->name_customer = $dataCustomerCTV->name ?? '';
                    } else {
                        $dataReceived[$k]->type_order = 'Bán hàng' ?? '';
                        $dataReceived[$k]->color = 'bg-blue' ?? '';
                        $dataReceived[$k]->name_customer = $dataCustomer->name ?? '';
                    }
                    $dataOrderDetail = OrderDetailModel::where('order_id', $v->id)->first();
                    $dataProduct = ProductModel::find($dataOrderDetail->product_id);
                    $dataReceived[$k]->name_product = $dataProduct->name ?? '';
                    $dataReceived[$k]->image_prd = $dataProduct->image ?? '';
                    $dataReceived[$k]->total = ($v->total_price_product) - ($v->fee_ctv24h);
                }
            }
        } else {
            $dataReceived = OrderModel::where('status', '<=', 3)
                ->where('shop_id', $dataShop->id)
                ->where('is_selected', 1)
                ->where('collaborator_id', null)
                ->where('status_confirm', 0)
                ->orderBy('created_at', 'desc')
                ->get();
            if ($dataReceived) {
                foreach ($dataReceived as $k => $v) {
                    $dataCustomer = CustomerModel::find($v->customer_id);
                    if ($v->collaborator_id == 1) {
                        $dataReceived[$k]->type_order = 'Hoa hồng' ?? '';
                        $dataReceived[$k]->color = 'bg-red' ?? '';
                        $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $v->id)->first();
                        $dataReceived[$k]->name_customer = $dataCustomerCTV->name ?? '';
                    } else {
                        $dataReceived[$k]->type_order = 'Bán hàng' ?? '';
                        $dataReceived[$k]->color = 'bg-blue' ?? '';
                        $dataReceived[$k]->name_customer = $dataCustomer->name ?? '';
                    }
                    $dataOrderDetail = OrderDetailModel::where('order_id', $v->id)->first();
                    $dataProduct = ProductModel::find($dataOrderDetail->product_id);
                    $dataReceived[$k]->name_product = $dataProduct->name ?? '';
                    $dataReceived[$k]->image_prd = $dataProduct->image ?? '';
                    $dataReceived[$k]->total = ($v->total_price_product) - ($v->fee_ctv24h);
                }
            }
        }
        $total_row = $dataReceived->count();
        if ($total_row > 0) {
            foreach ($dataReceived as $k => $row) {
                $url_order_detail = '/admin/order/detail/'.$row->id;
                $output .= '
                                <tr class="text-center">
                                    <td>
                                        <span class="' . $row->color . '">' . $row->type_order . '</span>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <img style="width: 60px; height: 60px; object-fit: cover; border-radius: 14px; margin-right: 5px;" src="' . $row->image_prd . '" class="image-preview">
                                            <a style="color:#626262;" href="'.$url_order_detail.'">
                                                <span style="width: 200px!important;" class="m-0">' . $row->name_product . '</span>
                                            </a>
                                        </div>
                                    </td>
                                    <td>' . $row->name_customer . '</td>
                                    <td>
                                        <span>' . date('d-m-Y H:i:s', strtotime($row->created_at)) . '</span>
                                        <br>
                                    </td>
                                    <td>Chờ người mua xác nhận đã nhận hàng</td>
                                    <td>
                                        <span>' . number_format($row->total) . 'đ</span>
                                        <br>
                                    </td>
                                </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td colspan="5">Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Rút tiền từ đơn hàng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getWithdrawal()
    {
        $dataUser = session()->get('data_supplier');
        $dataWithdrawal = DB::table('financial_withdrawal')
            ->distinct('order_detail.shop_id')
            ->select('order.*', 'order.total_price as total_price', 'financial_withdrawal.status as status')
            ->join('order', 'financial_withdrawal.order_id', '=', 'order.id')
            ->join('order_detail', 'order.id', '=', 'order_detail.order_id')
            ->join('shop', 'order_detail.shop_id', '=', 'shop.id')
            ->where('order.status', '=', 4)
            ->where('shop.supplier_id', '=', $dataUser->id)
            ->orderBy('order.id', 'desc')
            ->get();
        if ($dataWithdrawal) {
            foreach ($dataWithdrawal as $k => $v) {
                if ($v->status == 0) {
                    $dataWithdrawal[$k]->status = 'Đang xử lý';
                    $dataWithdrawal[$k]->color = 'warning';
                } else if ($v->status == 1) {
                    $dataWithdrawal[$k]->status = 'Hoàn thành';
                    $dataWithdrawal[$k]->color = 'success';
                } else {
                    $dataWithdrawal[$k]->status = 'Đã hủy';
                    $dataWithdrawal[$k]->color = 'danger';
                }
            }
        }
        $dataReturn = [
            'dataWithdrawal' => $dataWithdrawal,
            'title' => 'Theo dõi tài chính | Rút tiền'
        ];
        return view('supplier.financial.revenue.withdrawal', $dataReturn);
    }

    /**
     *  Hoàn tiền từ đơn hàng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getRefund()
    {
        $dataUser = session()->get('data_supplier');
        $dataRefund = DB::table('financial_refund')
            ->distinct('order_detail.shop_id')
            ->select('order.*', 'order.total_price as total_price', 'financial_refund.status as status')
            ->join('order', 'financial_refund.order_id', '=', 'order.id')
            ->join('order_detail', 'order.id', '=', 'order_detail.order_id')
            ->join('shop', 'order_detail.shop_id', '=', 'shop.id')
            ->where('order.status', '=', 5)
            ->where('shop.supplier_id', '=', $dataUser->id)
            ->orderBy('order.id', 'desc')
            ->get();
        if ($dataRefund) {
            foreach ($dataRefund as $k => $v) {
                if ($v->status == 0) {
                    $dataRefund[$k]->status = 'Đang xử lý';
                    $dataRefund[$k]->color = 'warning';
                } else if ($v->status == 1) {
                    $dataRefund[$k]->status = 'Hoàn thành';
                    $dataRefund[$k]->color = 'success';
                } else {
                    $dataRefund[$k]->status = 'Đã hủy';
                    $dataRefund[$k]->color = 'danger';
                }
            }
        }
        $dataReturn = [
            'dataRefund' => $dataRefund,
            'title' => 'Theo dõi tài chính | Hoàn tiền từ đơn hàng'
        ];
        return view('supplier.financial.revenue.refund', $dataReturn);
    }

    /**
     *  Chi tiết doanh thu
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function detailRevenue(Request $request)
    {
        $dataUser = session()->get('data_supplier');
        $dataReceived = DB::table('order')
            ->select('order.*', 'order.name as name_customer', 'order.phone as phone_customer',
                'order.payment_id as payment', 'order.note as note',
                'order_detail.*',
                'product.name as name_product', 'product.*', 'product.image as image')
            ->join('order_detail', 'order.id', '=', 'order_detail.order_id')
            ->join('product', 'product.id', '=', 'order_detail.product_id')
            ->where('order.supplier_id', '=', $dataUser['id'])
            ->where('order.id', $request->id)
            ->orderBy('order.id', 'desc')
            ->first();
        if ($dataReceived->payment_id == 0) {
            $dataReceived->payment = 'Thanh toán khi nhận hàng';
        } else if ($dataReceived->payment_id == 1) {
            $dataReceived->payment = 'Thẻ tín dụng/ghi nợ';
        } else {
            $dataReceived->payment = 'Chuyển khoản';
        }

        if ($dataReceived->status == 0) {
            $dataReceived->status = 'Chờ xác nhận';
        } else if ($dataReceived->status == 1) {
            $dataReceived->status = 'Chờ lấy hàng';
        } else if ($dataReceived->status == 2) {
            $dataReceived->status = 'Đang giao hàng';
        } else if ($dataReceived->status == 3) {
            $dataReceived->status = 'Đã giao hàng';
        } else if ($dataReceived->status == 4) {
            $dataReceived->status = 'Đã hủy';
        } else {
            $dataReceived->status = 'Hoàn tiền';
        }

        $dataCity = CityModel::where('id', $dataReceived->city_id)->first();
        $dataReceived->city = $dataCity->name;
        $dataDistrict = DistrictModel::where('id', $dataReceived->district_id)->first();
        $dataReceived->district = $dataDistrict->name;
        $dataWard = WardModel::where('id', $dataReceived->ward_id)->first();
        $dataReceived->ward = $dataWard->name;
        $dataReceived->Adds = $dataReceived->address . ', ' . $dataReceived->ward . ', ' . $dataReceived->district . ', ' . $dataReceived->city;

        $dataReturn = [
            'dataReceived' => $dataReceived,
            'title' => 'Quản lý doanh thu'
        ];
        return view('supplier.financial.revenue.detail', $dataReturn);
    }

    /**
     *  Lịch sử xu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function historyCoin()
    {
        $dataReturn = [
            'title' => 'Lịch sử xu'
        ];
        return view('supplier.financial.wallet.history_coin', $dataReturn);
    }

    /**
     *  Đổi xu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function changeCoin()
    {
        $dataUser = session()->get('data_supplier');
        $data = CustomerModel::where('id', $dataUser->customer_id)->first();
        $dataReturn = [
            'data' => $data,
            'title' => 'Đổi xu',
        ];
        return view('supplier.financial.wallet.change_coin', $dataReturn);
    }

    /**
     *  Thiết lập thanh toán
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexPaymentSetting()
    {
        $dataReturn = [
            'title' => 'Doanh thu | Thiết lập thanh toán'
        ];
        return view('supplier.financial.payment_setting.index', $dataReturn);
    }

    /**
     * Thiết lập mã pin
     */
    public function createPinCode(Request $request)
    {
        $rules = [
            'pin_code' => 'required|min:6|max:6',
            're_pin_code' => 'required|same:pin_code',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'min' => 'Mã pin gồm 6 chữ số',
            'max' => 'Mã pin gồm 6 chữ số ',
            're_pin_code.same' => 'Mã pin không trùng khớp'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $dataUser = session()->get('data_supplier');
            $dataPoint = CustomerModel::find($dataUser->customer_id);
            $dataPoint->pin_code = md5($request->pin_code);
            $dataReturn = [
                'status' => true,
                'msg' => 'Thiết lập mã pin thành công',
                'url' => url('admin/manage_financial/payment_setting'),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Lưu mã pin
     * @param Request $request
     * @return array
     */
    public function savePinCode(Request $request)
    {
        $dataUser = session()->get('data_supplier');
        $data = CustomerModel::find($dataUser->customer_id);
        $data->pin_code = md5($request->pin_code);
        $data->save();
        $dataReturn = [
            'status' => true,
            'msg' => 'Thiết lập mã pin thành công',
            'url' => url('admin/manage_financial/payment_setting'),
        ];
        return $dataReturn;
    }

    /**
     *  Ví | Tất cả
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexWallet()
    {
        $dataLogin = session()->get('data_supplier');
        $dataBankAccount = BankAccountModel::where('customer_id', $dataLogin->customer_id)->orderBy('id', 'desc')->get();
        if ($dataBankAccount) {
            foreach ($dataBankAccount as $k => $v) {
                $dataTheBank = TheBankModel::find($v->bank_name);
                $dataBankAccount[$k]->short_name = $dataTheBank->short_name ?? '';
                $dataBankAccount[$k]->bank_name = $dataTheBank->name ?? '';
                $str = $v->bank_account;
                $arr = substr($str, -6, 6);
                $dataBankAccount[$k]->sub_bank_account = $arr;
                if ($v->is_default == 1) {
                    $dataBankAccount[$k]->default = 'Mặc định' ?: '';
                }
            }
        }
        $dataBank = TheBankModel::orderBy('id', 'desc')->get();
        $dataUser = session()->get('data_supplier');
        $data = CustomerModel::find($dataUser->customer_id);
        $dataCity = CityModel::orderBy('name', 'asc')->get();

        $dataBankAccountRight = BankAccountModel::where('customer_id', $dataLogin->customer_id)->where('is_default', 1)->first();
        if ($dataBankAccountRight) {
            $dataTheBank = TheBankModel::find($dataBankAccountRight->bank_name);
            $dataBankAccountRight->short_name = $dataTheBank->short_name ?? '';
            $dataBankAccountRight->bank_name = $dataTheBank->name ?? '';
            $str = $dataBankAccountRight->bank_account;
            $arr_first = substr($str, 0, 4);
            $arr_last = substr($str, -4, 4);
            $dataBankAccountRight->first_num_acc = $arr_first;
            $dataBankAccountRight->last_num_acc = $arr_last;
        }

        $data_history_order_payment = HistoryOrderPaymentModel::where('customer_id', $dataLogin->customer_id)->orderBy('id', 'desc')->paginate(5);
        foreach ($data_history_order_payment as $key => $value) {
            $data_history_order_payment[$key]['date'] = date('d-m-Y', strtotime($value->created_at));
            $data_order = OrderModel::find($value['order_id']);
            $data_order_detail = OrderDetailModel::where('order_id', $data_order['id'])->first();
            $data_product = ProductModel::find($data_order_detail['product_id']);
            $data_history_order_payment[$key]['name_product_order'] = $data_product['name'];
            if ($value['type'] == 0) {
                $data_history_order_payment[$key]['is_type'] = 'Doanh thu';
            } elseif ($value['type'] == 1) {
                $data_history_order_payment[$key]['is_type'] = 'Rút tiền';
            } elseif ($value['type'] == 2) {
                $data_history_order_payment[$key]['is_type'] = 'Hoàn tiền';
            } else {
                $data_history_order_payment[$key]['is_type'] = 'Hoa hồng giới thiệu';
            }
        }

        $dataReturn = [
            'supplier' => true,
            'data' => $data,
            'title' => 'Quản lý ví',
            'dataBank' => $dataBank,
            'dataCity' => $dataCity,
            'dataBankAccount' => $dataBankAccount,
            'dataBankAccountRight' => $dataBankAccountRight,
            'data_history' => $data_history_order_payment
        ];
        return view('supplier.financial.wallet.index', $dataReturn);
    }

    /**
     *  Ví | Rút tiền
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function statusWithdrawal()
    {
        $dataLogin = session()->get('data_supplier');
        $dataBankAccount = BankAccountModel::where('customer_id', $dataLogin->customer_id)->orderBy('id', 'desc')->get();
        if ($dataBankAccount) {
            foreach ($dataBankAccount as $k => $v) {
                $dataTheBank = TheBankModel::find($v->bank_name);
                $dataBankAccount[$k]->short_name = $dataTheBank->short_name ?? '';
                $dataBankAccount[$k]->bank_name = $dataTheBank->name ?? '';
                $str = $v->bank_account;
                $arr = substr($str, -6, 6);
                $dataBankAccount[$k]->sub_bank_account = $arr;
                if ($v->is_default == 1) {
                    $dataBankAccount[$k]->default = 'Mặc định' ?: '';
                }
            }
        }
        $dataBank = TheBankModel::orderBy('id', 'desc')->get();
        $dataUser = session()->get('data_supplier');
        $data = CustomerModel::find($dataUser->customer_id);
        $dataCity = CityModel::orderBy('name', 'asc')->get();

        $dataBankAccountRight = BankAccountModel::where('customer_id', $dataLogin->customer_id)->where('is_default', 1)->first();
        if ($dataBankAccountRight) {
            $dataTheBank = TheBankModel::find($dataBankAccountRight->bank_name);
            $dataBankAccountRight->short_name = $dataTheBank->short_name ?? '';
            $dataBankAccountRight->bank_name = $dataTheBank->name ?? '';
            $str = $dataBankAccountRight->bank_account;
            $arr_first = substr($str, 0, 4);
            $arr_last = substr($str, -4, 4);
            $dataBankAccountRight->first_num_acc = $arr_first;
            $dataBankAccountRight->last_num_acc = $arr_last;
        }

        $data_history_order_payment = HistoryOrderPaymentModel::where('customer_id', $dataLogin->customer_id)->where('type', 1)->orderBy('id', 'desc')->paginate(5);
        foreach ($data_history_order_payment as $key => $value) {
            $data_history_order_payment[$key]['date'] = date('d-m-Y', strtotime($value->created_at));
            $data_order = OrderModel::find($value['order_id']);
            $data_order_detail = OrderDetailModel::where('order_id', $data_order['id'])->first();
            $data_product = ProductModel::find($data_order_detail['product_id']);
            $data_history_order_payment[$key]['name_product_order'] = $data_product['name'];
            if ($value['type'] == 0) {
                $data_history_order_payment[$key]['is_type'] = 'Doanh thu';
            } elseif ($value['type'] == 1) {
                $data_history_order_payment[$key]['is_type'] = 'Rút tiền';
            } elseif ($value['type'] == 2) {
                $data_history_order_payment[$key]['is_type'] = 'Hoàn tiền';
            } else {
                $data_history_order_payment[$key]['is_type'] = 'Hoa hồng giới thiệu';
            }
        }
        $dataReturn = [
            'supplier' => true,
            'data' => $data,
            'title' => 'Quản lý ví',
            'dataBank' => $dataBank,
            'dataCity' => $dataCity,
            'dataBankAccount' => $dataBankAccount,
            'dataBankAccountRight' => $dataBankAccountRight,
            'data_history' => $data_history_order_payment
        ];
        return view('supplier.financial.wallet.status_withdrawal', $dataReturn);
    }

    /**
     *  Ví | Doanh thu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function statusRevenue()
    {
        $dataLogin = session()->get('data_supplier');
        $dataBankAccount = BankAccountModel::where('customer_id', $dataLogin->customer_id)->orderBy('id', 'desc')->get();
        if ($dataBankAccount) {
            foreach ($dataBankAccount as $k => $v) {
                $dataTheBank = TheBankModel::find($v->bank_name);
                $dataBankAccount[$k]->short_name = $dataTheBank->short_name ?? '';
                $dataBankAccount[$k]->bank_name = $dataTheBank->name ?? '';
                $str = $v->bank_account;
                $arr = substr($str, -6, 6);
                $dataBankAccount[$k]->sub_bank_account = $arr;
                if ($v->is_default == 1) {
                    $dataBankAccount[$k]->default = 'Mặc định' ?: '';
                }
            }
        }
        $dataBank = TheBankModel::orderBy('id', 'desc')->get();
        $dataUser = session()->get('data_supplier');
        $data = CustomerModel::find($dataUser->customer_id);
        $dataCity = CityModel::orderBy('name', 'asc')->get();

        $dataBankAccountRight = BankAccountModel::where('customer_id', $dataLogin->customer_id)->where('is_default', 1)->first();
        if ($dataBankAccountRight) {
            $dataTheBank = TheBankModel::find($dataBankAccountRight->bank_name);
            $dataBankAccountRight->short_name = $dataTheBank->short_name ?? '';
            $dataBankAccountRight->bank_name = $dataTheBank->name ?? '';
            $str = $dataBankAccountRight->bank_account;
            $arr_first = substr($str, 0, 4);
            $arr_last = substr($str, -4, 4);
            $dataBankAccountRight->first_num_acc = $arr_first;
            $dataBankAccountRight->last_num_acc = $arr_last;
        }

        $data_history_order_payment = HistoryOrderPaymentModel::where('customer_id', $dataLogin->customer_id)->where('type', 0)->orderBy('id', 'desc')->paginate(5);
        foreach ($data_history_order_payment as $key => $value) {
            $data_history_order_payment[$key]['date'] = date('d-m-Y', strtotime($value->created_at));
            $data_order = OrderModel::find($value['order_id']);
            $data_order_detail = OrderDetailModel::where('order_id', $data_order['id'])->first();
            $data_product = ProductModel::find($data_order_detail['product_id']);
            $data_history_order_payment[$key]['name_product_order'] = $data_product['name'];
            if ($value['type'] == 0) {
                $data_history_order_payment[$key]['is_type'] = 'Doanh thu';
            } elseif ($value['type'] == 1) {
                $data_history_order_payment[$key]['is_type'] = 'Rút tiền';
            } elseif ($value['type'] == 2) {
                $data_history_order_payment[$key]['is_type'] = 'Hoàn tiền';
            } else {
                $data_history_order_payment[$key]['is_type'] = 'Hoa hồng giới thiệu';
            }
        }
        $dataReturn = [
            'supplier' => true,
            'data' => $data,
            'title' => 'Quản lý ví',
            'dataBank' => $dataBank,
            'dataCity' => $dataCity,
            'dataBankAccount' => $dataBankAccount,
            'dataBankAccountRight' => $dataBankAccountRight,
            'data_history' => $data_history_order_payment
        ];
        return view('supplier.financial.wallet.status_revenue', $dataReturn);
    }

    /**
     * Ví | Hoàn tiền
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function statusRefund()
    {
        $dataReturn = [
            'title' => 'Ví',
        ];
        return view('supplier.financial.wallet.status_refund', $dataReturn);
    }

    /**
     *  Ví | Hoa hồng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function statusBonus()
    {
        $dataReturn = [
            'title' => 'Ví',
        ];
        return view('supplier.financial.wallet.status_bonus', $dataReturn);
    }

    /**
     *  Các loại giao dịch gần đây | Lọc theo ngày | Tất cả
     * @param Request $request
     */
    public function filterDateStatusAll(Request $request){
        $time_start = $request->time_start;
        $time_end = $request->time_end;
        $dataLogin = session()->get('data_supplier');
        $output = '';
        if ($time_start != null || $time_end != null) {
            $data_history = HistoryOrderPaymentModel::where('customer_id', $dataLogin->customer_id)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->orderBy('id', 'desc')
                ->paginate(5);
            foreach ($data_history as $key => $value) {
                $data_history[$key]['date'] = date('d-m-Y', strtotime($value->created_at));
                $data_order = OrderModel::find($value['order_id']);
                $data_order_detail = OrderDetailModel::where('order_id', $data_order['id'])->first();
                $data_product = ProductModel::find($data_order_detail['product_id']);
                $data_history[$key]['name_product_order'] = $data_product['name'];
                if ($value['type'] == 0) {
                    $data_history[$key]['is_type'] = 'Doanh thu';
                } elseif ($value['type'] == 1) {
                    $data_history[$key]['is_type'] = 'Rút tiền';
                } elseif ($value['type'] == 2) {
                    $data_history[$key]['is_type'] = 'Hoàn tiền';
                } else {
                    $data_history[$key]['is_type'] = 'Hoa hồng giới thiệu';
                }
                if($value['status'] == 0){
                    $data_history[$key]['status'] = 'Đang xử lý';
                    $data_history[$key]['color'] = 'bg-orange';
                } else if($value['status'] == 1) {
                    $data_history[$key]['status'] = 'Hoàn thành';
                    $data_history[$key]['color'] = 'bg-green';
                } else {
                    $data_history[$key]['status'] = 'Đã hủy';
                    $data_history[$key]['color'] = 'bg-red';
                }
            }
        }
        $total_row = $data_history->count();
        if ($total_row > 0) {
            foreach ($data_history as $k => $row) {
                $output .= '
                            <tr class="text-center">
                                <td>'.$row->date.'</td>
                                <td>
                                    <span class="title-dank">'.$row->is_type.' từ đơn hàng  #'.$row->order_code.'</span>
                                    <br>
                                    <span>'.$row->name_product_order.'</span>
                                </td>
                                <td>
                                    <div class="'.$row->color.' bg-span">'.$row->status.'</div>
                                </td>
                                <td class="title-dank">'.number_format($row->price).'đ</td>
                            </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td>Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data_history = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data_history);
    }

    /**
     *  Các loại giao dịch gần đây | Lọc theo ngày | Doanh thu
     * @param Request $request
     */
    public function filterDateStatusRevenue(Request $request){
        $time_start = $request->time_start;
        $time_end = $request->time_end;
        $dataLogin = session()->get('data_supplier');
        $output = '';
        if ($time_start != null || $time_end != null) {
            $data = HistoryOrderPaymentModel::where('customer_id', $dataLogin->customer_id)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->where('type', 0)
                ->orderBy('id', 'desc')
                ->paginate(5);
            foreach ($data as $key => $value) {
                $data[$key]['date'] = date('d-m-Y', strtotime($value->created_at));
                $data_order = OrderModel::find($value['order_id']);
                $data_order_detail = OrderDetailModel::where('order_id', $data_order['id'])->first();
                $data_product = ProductModel::find($data_order_detail['product_id']);
                $data[$key]['name_product_order'] = $data_product['name'];
                if ($value['type'] == 0) {
                    $data[$key]['is_type'] = 'Doanh thu';
                } elseif ($value['type'] == 1) {
                    $data[$key]['is_type'] = 'Rút tiền';
                } elseif ($value['type'] == 2) {
                    $data[$key]['is_type'] = 'Hoàn tiền';
                } else {
                    $data[$key]['is_type'] = 'Hoa hồng giới thiệu';
                }
                if($value['status'] == 0){
                    $data[$key]['status'] = 'Đang xử lý';
                    $data[$key]['color'] = 'bg-orange';
                } else if($value['status'] == 1) {
                    $data[$key]['status'] = 'Hoàn thành';
                    $data[$key]['color'] = 'bg-green';
                } else {
                    $data[$key]['status'] = 'Đã hủy';
                    $data[$key]['color'] = 'bg-red';
                }
            }
        }
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $output .= '
                            <tr class="text-center">
                                <td>'.$row->date.'</td>
                                <td>
                                    <span class="title-dank">'.$row->is_type.' từ đơn hàng  #'.$row->order_code.'</span>
                                    <br>
                                    <span>'.$row->name_product_order.'</span>
                                </td>
                                <td>
                                    <div class="'.$row->color.' bg-span">'.$row->status.'</div>
                                </td>
                                <td class="title-dank">'.number_format($row->price).'đ</td>
                            </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td>Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Các loại giao dịch gần đây | Lọc theo ngày | Rút tiền
     * @param Request $request
     */
    public function filterDateStatusWithdrawal(Request $request){
        $time_start = $request->time_start;
        $time_end = $request->time_end;
        $dataLogin = session()->get('data_supplier');
        $output = '';
        if ($time_start != null || $time_end != null) {
            $data = HistoryOrderPaymentModel::where('customer_id', $dataLogin->customer_id)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->where('type', 1)
                ->orderBy('id', 'desc')
                ->paginate(5);
            foreach ($data as $key => $value) {
                $data[$key]['date'] = date('d-m-Y', strtotime($value->created_at));
                $data_order = OrderModel::find($value['order_id']);
                $data_order_detail = OrderDetailModel::where('order_id', $data_order['id'])->first();
                $data_product = ProductModel::find($data_order_detail['product_id']);
                $data[$key]['name_product_order'] = $data_product['name'];
                if ($value['type'] == 0) {
                    $data[$key]['is_type'] = 'Doanh thu';
                } elseif ($value['type'] == 1) {
                    $data[$key]['is_type'] = 'Rút tiền';
                } elseif ($value['type'] == 2) {
                    $data[$key]['is_type'] = 'Hoàn tiền';
                } else {
                    $data[$key]['is_type'] = 'Hoa hồng giới thiệu';
                }
                if($value['status'] == 0){
                    $data[$key]['status'] = 'Đang xử lý';
                    $data[$key]['color'] = 'bg-orange';
                } else if($value['status'] == 1) {
                    $data[$key]['status'] = 'Hoàn thành';
                    $data[$key]['color'] = 'bg-green';
                } else {
                    $data[$key]['status'] = 'Đã hủy';
                    $data[$key]['color'] = 'bg-red';
                }
            }
        }
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $output .= '
                            <tr class="text-center">
                                <td>'.$row->date.'</td>
                                <td>
                                    <span class="title-dank">'.$row->is_type.' từ đơn hàng  #'.$row->order_code.'</span>
                                    <br>
                                    <span>'.$row->name_product_order.'</span>
                                </td>
                                <td>
                                    <div class="'.$row->color.' bg-span">'.$row->status.'</div>
                                </td>
                                <td class="title-dank">'.number_format($row->price).'đ</td>
                            </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td>Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Lịch sử rút tiền | Lọc theo ngày
     * @param Request $request
     *
     */
    public function filterDatePayment(Request $request){
        $time_start = $request->time_start;
        $time_end = $request->time_end;
        $dataUser = session()->get('data_supplier');
        $output = '';
        if ($time_start != null || $time_end != null) {
            $data = DB::table('payment')
                ->select('payment.*', 'bank_account.*', 'payment.created_at as created_at', 'the_bank.name as name_bank', 'the_bank.short_name as short_name_bank', 'bank_account.bank_name as bank_name')
                ->join('bank_account', 'payment.account', '=', 'bank_account.id')
                ->join('the_bank', 'bank_account.bank_name', '=', 'the_bank.id')
                ->where('bank_account.customer_id', $dataUser->customer_id)
                ->where('payment.type', 0)
                ->where('payment.created_at', '>=', $time_start)
                ->where('payment.created_at', '<=', $time_end)
                ->orderBy('payment.id', 'desc')
                ->get();
        }
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $date = date('d-m-Y H:i:s', strtotime($row->created_at));
                $output .= '
                                <tr>
                                    <td>'.$row->user_name.'</td>
                                    <td>'.$row->bank_account.'</td>
                                    <td>'.$row->name_bank.' - '.$row->short_name_bank.'</td>
                                    <td>- '.number_format($row->price).'đ</td>
                                    <td>'.number_format(11000).'đ</td>
                                    <td>'.$date.'</td>
                                </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr>
                            <td>Không có dữ liệu</td>
                        </tr>
            ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }
}
