<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use App\Models\AddressSupplierModel;
use App\Models\AttributeValueModel;
use App\Models\CategoryProductModel;
use App\Models\ProductAttributeModel;
use App\Models\ProductModel;
use App\Models\ProductPromotionModel;
use App\Models\PromotionModel;
use App\Models\PromotionShopModel;
use App\Models\ShopModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use function Matrix\trace;
use DB;

class PromotionShopController extends Controller
{
    /** Danh sách
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {

        $dataReturn = [
            'title' => 'Chương trình giảm giá của tôi'
        ];
        return view('supplier.marketing.promotion_shop.index', $dataReturn);
    }

    /**
     *  Tạo CT giảm giá
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add(Request $request)
    {
        $dataLogin = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataLogin->customer_id)->first();
        switch ($request->type_up){
            case 'list_product' :
                $type_up = 'list_product';
                break;
            default :
                $type_up = 'choose';
                break;
        }
        switch ($request->type_down){
            case 'reset' :
                $type_down = 'reset';
                break;
            default :
                $type_down = 'search';
                break;
        }
        $dataCategoryProduct = CategoryProductModel::orderBy('name', 'asc')->get();
        $dataProduct = ProductModel::where('shop_id', $dataShop->id)->orderBy('id', 'desc')->get();
        if($dataProduct){
            foreach ($dataProduct as $k => $v){
                if($v->dropship == 1){
                    $dataProduct[$k]->type_product = 'Hàng Dropship';
                } else {
                    $dataProduct[$k]->type_product = 'Hàng có sẵn';
                }
            }
        }

        $dataPromotion = ProductPromotionModel::where('shop_id', $dataShop->id)->orderBy('created_at', 'desc')->get();
        if($dataPromotion){
            foreach ($dataPromotion as $k => $v){
                $dataProduct_s = ProductModel::find($v->product_id);
                $dataPromotion[$k]->product_id = $dataProduct_s->id ?? '';
                $dataPromotion[$k]->name_product = $dataProduct_s->name ?? '';
                $dataPromotion[$k]->price = $dataProduct_s->price ?? '';
                $dataPromotion[$k]->image = $dataProduct_s->image ?? '';
                $dataPromotion[$k]->quantity = $dataProduct_s->warehouse ?? '';
            }
        }
        $dataReturn = [
            'type_up' => $type_up,
            'type_down' => $type_down,
            'title' => 'Tạo chương trình khuyến mãi mới',
            'dataProduct' => $dataProduct,
            'dataCategoryProduct' => $dataCategoryProduct,
            'dataPromotion' => $dataPromotion
        ];
        return view('supplier.marketing.promotion_shop.form', $dataReturn);
    }

    /**
     *  Thêm sản phẩm vào chương trình khuyến mại
     * @param Request $request
     * @return array
     */
    public function addProduct(Request $request)
    {
        $dataLogin = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataLogin->customer_id)->first();
        foreach ($request->product as $k => $v){
            $dataPromotionOld = ProductPromotionModel::where('shop_id', $dataShop->id)->where('product_id', $v)->first();
            if(!isset($dataPromotionOld)){
                $data = new ProductPromotionModel();
                $data->product_id = $v;
                $data->price_before = $request->price_before;
                $data->customer_id = $dataLogin->customer_id;
                $data->shop_id = $dataShop->id;
                $data->active = 1;
                $data->save();
            }
        }
        $dataReturn = [
            'title' => 'Chọn sản phẩm',
            'status' => true,
            'msg' => 'Thêm sản phẩm thành công',
            'url' => url('admin/marketing_supplier/promotion/add')
        ];
        return $dataReturn;
    }

    /**
     *  Xóa sản phẩm khuyến mại
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteProduct($id){
        try {
            ProductPromotionModel::destroy($id);
            $dataReturn = [
                'msg' => 'Xoá dữ liệu thành công !',
                'status' => true
            ];
            return \response()->json($dataReturn, 200);
        } catch (\Exception $e) {
            $dataReturn = [
                'msg' => 'Đã có lỗi xảy ra !',
                'status' => false
            ];
            return \response()->json($dataReturn, 500);
        }
    }

    /**
     *  Lưu mã giảm giá
     * @param Request $request
     * @return array
     */
    public function savePromotion(Request $request){
        $dataLogin = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataLogin->customer_id)->first();
        $product_id = $request->product_id;
        $time_start = date('Y-m-d H:i:s', strtotime($request->time_start));
        $time_10min = Carbon::parse($time_start)->addMinute(10)->format('Y-m-d H:i:s');
        $rules = [
            'name' => 'required',
            'time_start' => 'required|after:today',
            'time_stop' => 'required',
            'product_id' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'time_start.after' => 'Ngày bắt đầu không được nhỏ hơn ngày hiện tại',
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $dataPromotion = new PromotionShopModel([
                'name' => $request->name,
                'time_start' => $time_10min,
                'time_stop' => $request->time_stop,
                'shop_id' => $dataShop->id
            ]);
            $dataPromotion->save();
            foreach ($product_id as $k => $v){
                $dataProductPromotion = ProductPromotionModel::where('product_id', $v)->first();
                $dataProductPromotion->promotion_id = $dataPromotion->id;
                $dataProductPromotion->save();
            }
            $dataReturn = [
                'status' => true,
                'msg' => 'Tạo chương trình giảm giá thành công',
                'url' => url('admin/marketing_supplier/promotion')
            ];
        }
        return $dataReturn;
    }

    /**
     *  Danh sách | Tất cả
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getAllPromotion(){
        $today = Carbon::now('Asia/Ho_Chi_Minh');
        $dataLogin = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataLogin->customer_id)->first();
        $data = PromotionShopModel::where('shop_id', $dataShop->id)->orderBy('id', 'desc')->get();
        if($data){
            foreach ($data as $k => $v){
                if($v->time_start <= $today && $v->time_stop > $today){
                    $data[$k]->status = 'Đang diễn ra';
                    $data[$k]->color = 'bg-blue';
                } else if($v->time_start > $today && $v->time_stop > $today) {
                    $data[$k]->status = 'Sắp diễn ra';
                    $data[$k]->color = 'bg-green';
                } else if($v->time_start < $today && $v->time_stop < $today) {
                    $data[$k]->status = 'Đã kết thúc';
                    $data[$k]->color = 'bg-red';
                }
            }
        }
        $dataProductPromotion = ProductPromotionModel::orderBy('id', 'desc')->get();
        if ($dataProductPromotion){
            foreach ($dataProductPromotion as $key => $value){
                $dataProduct = ProductModel::find($value->product_id);
                $dataProductPromotion[$key]->image = $dataProduct->image ?? '';
            }
        }
        $dataReturn = [
            'data' => $data,
            'product' => $dataProductPromotion,
            'status' => true,
            'title' => 'Chương trình giảm giá của tôi | Tất cả'
        ];
        return view('supplier.marketing.promotion_shop.all', $dataReturn);
    }

    /**
     *  Danh sách | Đang diễn ra
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getBeingPromotion(){
        $today = Carbon::now('Asia/Ho_Chi_Minh');
        $dataLogin = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataLogin->customer_id)->first();
        $data = PromotionShopModel::where('shop_id', $dataShop->id)
            ->where('time_start', '<=', $today)
            ->where('time_stop', '>', $today)
            ->orderBy('id', 'desc')
            ->get();
        if($data){
            foreach ($data as $k => $v){
                if($v->time_start <= $today && $v->time_stop > $today){
                    $data[$k]->status = 'Đang diễn ra';
                    $data[$k]->color = 'bg-blue';
                } else if($v->time_start > $today && $v->time_stop > $today) {
                    $data[$k]->status = 'Sắp diễn ra';
                    $data[$k]->color = 'bg-green';
                } else if($v->time_start < $today && $v->time_stop < $today) {
                    $data[$k]->status = 'Đã kết thúc';
                    $data[$k]->color = 'bg-red';
                }
            }
        }
        $dataProductPromotion = ProductPromotionModel::orderBy('id', 'desc')->get();
        if ($dataProductPromotion){
            foreach ($dataProductPromotion as $key => $value){
                $dataProduct = ProductModel::find($value->product_id);
                $dataProductPromotion[$key]->image = $dataProduct->image ?? '';
            }
        }
        $dataReturn = [
            'data' => $data,
            'product' => $dataProductPromotion,
            'status' => true,
            'title' => 'Chương trình giảm giá của tôi | Đang diễn ra'
        ];
        return view('supplier.marketing.promotion_shop.being', $dataReturn);
    }

    /**
     *  Danh sách | Sắp diễn ra
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getComingUpPromotion(){
        $today = Carbon::now('Asia/Ho_Chi_Minh');
        $dataLogin = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataLogin->customer_id)->first();
        $data = PromotionShopModel::where('shop_id', $dataShop->id)
            ->where('time_start', '>', $today)
            ->where('time_stop', '>', $today)
            ->orderBy('id', 'desc')
            ->get();
        if($data){
            foreach ($data as $k => $v){
                if($v->time_start <= $today && $v->time_stop > $today){
                    $data[$k]->status = 'Đang diễn ra';
                    $data[$k]->color = 'bg-blue';
                } else if($v->time_start > $today && $v->time_stop > $today) {
                    $data[$k]->status = 'Sắp diễn ra';
                    $data[$k]->color = 'bg-green';
                } else if($v->time_start < $today && $v->time_stop < $today) {
                    $data[$k]->status = 'Đã kết thúc';
                    $data[$k]->color = 'bg-red';
                }
            }
        }
        $dataProductPromotion = ProductPromotionModel::orderBy('id', 'desc')->get();
        if ($dataProductPromotion){
            foreach ($dataProductPromotion as $key => $value){
                $dataProduct = ProductModel::find($value->product_id);
                $dataProductPromotion[$key]->image = $dataProduct->image ?? '';
            }
        }
        $dataReturn = [
            'data' => $data,
            'product' => $dataProductPromotion,
            'status' => true,
            'title' => 'Chương trình giảm giá của tôi | Sắp diễn ra'
        ];
        return view('supplier.marketing.promotion_shop.coming_up', $dataReturn);
    }

    /**
     *  Danh sách | Đã kết thúc
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getFinishPromotion(){
        $today = Carbon::now('Asia/Ho_Chi_Minh');
        $dataLogin = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataLogin->customer_id)->first();
        $data = PromotionShopModel::where('shop_id', $dataShop->id)
            ->where('time_start', '<', $today)
            ->where('time_stop', '<', $today)
            ->orderBy('id', 'desc')
            ->get();
        if($data){
            foreach ($data as $k => $v){
                if($v->time_start <= $today && $v->time_stop > $today){
                    $data[$k]->status = 'Đang diễn ra';
                    $data[$k]->color = 'bg-blue';
                } else if($v->time_start > $today && $v->time_stop > $today) {
                    $data[$k]->status = 'Sắp diễn ra';
                    $data[$k]->color = 'bg-green';
                } else if($v->time_start < $today && $v->time_stop < $today) {
                    $data[$k]->status = 'Đã kết thúc';
                    $data[$k]->color = 'bg-red';
                }
            }
        }
        $dataProductPromotion = ProductPromotionModel::orderBy('id', 'desc')->get();
        if ($dataProductPromotion){
            foreach ($dataProductPromotion as $key => $value){
                $dataProduct = ProductModel::find($value->product_id);
                $dataProductPromotion[$key]->image = $dataProduct->image ?? '';
            }
        }
        $dataReturn = [
            'data' => $data,
            'product' => $dataProductPromotion,
            'status' => true,
            'title' => 'Chương trình giảm giá của tôi | Đã kết thúc'
        ];
        return view('supplier.marketing.promotion_shop.finish', $dataReturn);
    }
}
