<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use App\Models\ProductShippingModel;
use App\Models\ShippingModel;
use App\Models\ShippingSupplierModel;
use App\Models\ShopModel;
use Illuminate\Http\Request;

class ShippingSupplierController extends Controller
{
    public function index()
    {
        $data_login = session()->get('data_supplier');
        $data_shipping = ShippingModel::all();
        foreach ($data_shipping as $k => $v) {
            $data = ShippingSupplierModel::where('supplier_id', $data_login['id'])->where('shipping_id', $v['id'])->first();
            if ($data) {
                $data_shipping[$k]['setting'] = 'Đã liên kết';
                $data_shipping[$k]['style'] = 'success';
                $data_shipping[$k]['shipping'] = $data->id;
            } else {
                $data_shipping[$k]['setting'] = 'Chưa liên kết';
                $data_shipping[$k]['style'] = 'danger';
            }
        }
        $dataReturn = [
            'title' => 'Cài đặt vận chuyển',
            'data_shipping' => $data_shipping,
//            'data' => $data
        ];
        return view('supplier.shipping.index', $dataReturn);
    }

    public function save(Request $request)
    {
        $check = $request->check;
        if ($check == 1) {
            $data = new ShippingSupplierModel([
                'shipping_id' => $request->shipping_id,
                'supplier_id' => $request->supplier_id
            ]);
            $data->save();
            $dataReturn = [
                'status' => true,
                'msg' => 'Cài đặt vận chuyển thành công !',
                'url' => url('admin/shipping_supplier')
            ];
            return $dataReturn;
        }
        if ($check == 0) {
            $data_shipping = ShippingSupplierModel::find($request->shipping);
            $data_shop = ShopModel::where('supplier_id', $data_shipping->supplier_id)->first();
            ShippingSupplierModel::destroy($request->shipping);
            $data_product_shipping = ProductShippingModel::where('shipping_supplier_id', $data_shipping->shipping_id)->where('shop_id', $data_shop->id)->get();
            foreach ($data_product_shipping as $key => $value) {
                $data_product_shipping[$key]->delete();
            }
            $dataReturn = [
                'status' => true,
                'msg' => 'Gỡ cài đặt vận chuyển thành công !',
                'url' => url('admin/shipping_supplier'),
            ];
            return $dataReturn;
        }
    }

    public function detail($id)
    {
        $data['shipping'] = ShippingModel::find($id);
        return view('supplier.shipping.detail', $data);
    }
}
