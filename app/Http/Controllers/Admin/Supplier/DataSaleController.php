<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use App\Models\OrderModel;
use App\Models\ProductAccessModel;
use App\Models\ShopModel;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DataSaleController extends Controller
{
    public function index()
    {
        $data_login = session()->get('data_supplier');
        $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();

        //Today and Before day
        $today_head = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 00:00:00');
        $today_end = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 23:59:59');
        $today_head_before = Carbon::now('Asia/Ho_Chi_Minh')->subDay(1)->format('Y-m-d 00:00:00');
        $today_end_before = Carbon::now('Asia/Ho_Chi_Minh')->subDay(1)->format('Y-m-d 23:59:59');
        //End day

        //Tổng số order
        $count_order_before = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)->where('date', '>=', $today_head_before)->where('date', '<=', $today_end_before)->count();
        $count_order_today = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)->where('date', '>=', $today_head)->where('date', '<=', $today_end)->count();
        if ($count_order_before == 0) {
            $order_rate = 0;
        } else {
            $order_rate = round($count_order_today / $count_order_before, 2);
        }
        if ($count_order_before <= $count_order_today && $count_order_before != 0) {
            $tang_truong_order = 1; //1 đi lên
            $order_rate_percent = round(($count_order_today - $count_order_before) / $count_order_before * 100, 2);
        } elseif ($count_order_before > $count_order_today && $count_order_before != 0) {
            $tang_truong_order = -1; //-1 đi xuống
            $order_rate_percent = 100 - ($order_rate * 100);
        } elseif ($count_order_before == 0 || $count_order_today == 0) {
            $tang_truong_order = 0; //ko thay đổi
            $order_rate_percent = 0;
        }
        $data_count_order = [
            'count_order_today' => $count_order_today,
            'order_rate_percent' => $order_rate_percent,
            'tang_truong_order' => $tang_truong_order
        ];
        //end tổng số order

        //Tổng doanh thu
        $total_price_order_today = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)->where('status', 3)->where('created_at', '>=', $today_head)->where('created_at', '<=', $today_end)->where('status_confirm',1)->sum('price_supplier');
        $total_price_order_before = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)->where('status', 3)->where('created_at', '>=', $today_head_before)->where('created_at', '<=', $today_end_before)->where('status_confirm',1)->sum('price_supplier');
        if ($total_price_order_before == 0) {
            $total_price_rate = 0;
        } else {
            $total_price_rate = round($total_price_order_today / $total_price_order_before, 2);
        }
        if ($total_price_order_before <= $total_price_order_today && $total_price_order_before != 0) {
            $tang_truong_total_price = 1;
            $total_price_rate_percent = round(($total_price_order_today - $total_price_order_before) / $total_price_order_before * 100, 2);
        } elseif ($total_price_order_before > $total_price_order_today && $total_price_order_before != 0) {
            $tang_truong_total_price = -1;
            $total_price_rate_percent = 100 - ($total_price_rate * 100);
        } elseif ($total_price_order_before == 0 || $total_price_order_today == 0) {
            $tang_truong_total_price = 0;
            $total_price_rate_percent = 0;
        }
        $data_total_price_order = [
            'total_price_order_today' => $total_price_order_today,
            'total_price_rate_percent' => $total_price_rate_percent,
            'tang_truong_total_price' => $tang_truong_total_price
        ];
        //end tổng doanh thu


        //Tổng trung bình doanh thu 1 đơn
        $count_order_shipped_today = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)->where('status', 3)->where('created_at', '>=', $today_head)->where('created_at', '<=', $today_end)->where('status_confirm',1)->count();
        $count_order_shipped_before = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)->where('status', 3)->where('created_at', '>=', $today_head_before)->where('created_at', '<=', $today_end_before)->where('status_confirm',1)->count();
        $total_price_order_shipped_today = $total_price_order_today;
        $total_price_order_shipped_before = $total_price_order_before;
        if ($count_order_shipped_before == 0 && $count_order_shipped_today == 0) {
            $average_total_1_order_today = 0;
            $average_total_1_order_before = 0;
            $average_total_1_order_rate = 0;
        } elseif ($count_order_shipped_before == 0) {
            $average_total_1_order_today = $total_price_order_shipped_today / $count_order_shipped_today;
            $average_total_1_order_before = 0;
            $average_total_1_order_rate = 0;
        } elseif ($count_order_shipped_today == 0) {
            $average_total_1_order_today = 0;
            $average_total_1_order_before = $total_price_order_shipped_before / $count_order_shipped_before;
            $average_total_1_order_rate = 0;
        } else {
            $average_total_1_order_today = $total_price_order_shipped_today / $count_order_shipped_today;
            $average_total_1_order_before = $total_price_order_shipped_before / $count_order_shipped_before;
            $average_total_1_order_rate = round($average_total_1_order_today / $average_total_1_order_before, 2);
        }
        if ($average_total_1_order_today >= $average_total_1_order_before && $average_total_1_order_before != 0) {
            $average_total_1_order_percent = round(($average_total_1_order_today - $average_total_1_order_before) / $average_total_1_order_before * 100, 2);
            $tang_truong_average_total_1_order = 1;
        } elseif ($average_total_1_order_today < $average_total_1_order_before && $average_total_1_order_before != 0) {
            $average_total_1_order_percent = 100 - ($average_total_1_order_rate * 100);
            $tang_truong_average_total_1_order = -1;
        } elseif ($average_total_1_order_before == 0) {
            $average_total_1_order_percent = 0;
            $tang_truong_average_total_1_order = 0;
        }
        $data_average_total_1_order = [
            'average_total_1_order' => $average_total_1_order_today,
            'tang_truong_average_total_1_order' => $tang_truong_average_total_1_order,
            'average_total_1_order_percent' => $average_total_1_order_percent
        ];
        //End tổng doanh thu 1 đơn

        //Lượt truy cập
        $count_access_today = ProductAccessModel::where('shop_id', $data_shop->id)->where('created_at', '>=', $today_head)->where('created_at', '<=', $today_end)->count();
        $count_access_before = ProductAccessModel::where('shop_id', $data_shop->id)->where('created_at', '>=', $today_head_before)->where('created_at', '<=', $today_end_before)->count();
        if ($count_access_today == 0 || $count_access_before == 0) {
            $rate_access = 0;
        } else {
            $rate_access = round($count_access_today / $count_access_before, 2);
        }
        if ($count_access_today >= $count_access_before && $count_access_before != 0) {
            $percent_access = round(($count_access_today - $count_access_before) / $count_access_before * 100, 2);
            $tang_truong_access = 1;
        } elseif ($count_access_today < $count_access_before && $count_access_before != 0) {
            $percent_access = 100 - ($rate_access * 100);
            $tang_truong_access = -1;
        } elseif ($count_access_today == 0 || $count_access_before == 0) {
            $percent_access = 0;
            $tang_truong_access = 0;
        }
        $data_product_access = [
            'percent_access' => $percent_access,
            'tang_truong_access' => $tang_truong_access,
            'count_access' => $count_access_today
        ];
        //End lượt truy cập

        //Tỷ lệ chuyển đổi
        $count_order_confirm_today = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)
            ->where('status', '>', 0)
            ->where('date', '>=', $today_head)
            ->where('date', '<=', $today_end)
            ->count();
        $count_order_confirm_before = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)
            ->where('status', '>', 0)
            ->where('date', '>=', $today_head_before)
            ->where('date', '<=', $today_end_before)
            ->count();
        if ($count_order_confirm_today != 0 && $count_access_today != 0) {
            $rate_convert_today = round(($count_order_confirm_today / $count_access_today) * 100, 2);
        } else {
            $rate_convert_today = 0;
        }
        if ($count_order_confirm_before != 0 && $count_access_before != 0) {
            $rate_convert_before = round(($count_order_confirm_before / $count_access_before) * 100, 2);
        } else {
            $rate_convert_before = 0;
        }
        if ($rate_convert_today == 0 || $rate_convert_before == 0) {
            $rate_convert = 0;
        }else{
            $rate_convert = round($rate_convert_today / $rate_convert_before, 2);
        }
        if ($rate_convert_today == 0 || $rate_convert_before == 0) {
            $tang_truong_convert = 0;
            $percent_convert = 0;
        } elseif ($rate_convert_today >= $rate_convert_before && $rate_convert_before != 0) {
            $tang_truong_convert = 1;
            $percent_convert = round(($rate_convert_today - $rate_convert_before) / $rate_convert_before * 100, 2);
        } elseif ($rate_convert_today < $rate_convert_before && $rate_convert_before != 0) {
            $tang_truong_convert = -1;
            $percent_convert = 100 - ($rate_convert * 100);
        }
        $data_rate_convert = [
            'rate_convert_today' => $rate_convert_today,
            'tang_truong_convert' => $tang_truong_convert,
            'percent_convert' => $percent_convert
        ];
        //End tỷ lệ chuyển đổi

        $dataReturn = [
            'title' => 'Dữ liệu bán hàng',
            'data_count_order' => $data_count_order,
            'data_total_price_order' => $data_total_price_order,
            'data_average_total_1_order' => $data_average_total_1_order,
            'data_product_access' => $data_product_access,
            'data_rate_convert' => $data_rate_convert
            ];
        return view('supplier.data_sale.index', $dataReturn);
    }

    public function accessTimes()
    {
        return view('supplier.data_sale.access_times');
    }

    public function products()
    {
        return view('supplier.data_sale.products');
    }

    public function productsOverview()
    {
        return view('supplier.data_sale.products_overview');
    }

    public function marketing()
    {
        return view('supplier.data_sale.marketing');
    }

    public function needImprove()
    {
        return view('supplier.data_sale.need_improve');
    }

    public function revenue()
    {
        return view('supplier.data_sale.revenue');
    }

    public function comboSale()
    {
        return view('supplier.data_sale.combo_sale');
    }

    public function followerOffers()
    {
        return view('supplier.data_sale.follower_offers');
    }

    public function voucher()
    {
        return view('supplier.data_sale.voucher');
    }

    public function flashSale()
    {
        return view('supplier.data_sale.flash_sale');
    }

    public function dealShock()
    {
        return view('supplier.data_sale.deal_shock');
    }

    public function CTVlifeFeed()
    {
        return view('supplier.data_sale.CTV_life_feed');
    }

    public function saleTactician()
    {
        return view('supplier.data_sale.sale_tactician');
    }

    public function competitiveProduct()
    {
        return view('supplier.data_sale.competitive_product');
    }

    public function topSearch()
    {
        return view('supplier.data_sale.top_search');
    }

    public function optimizeProduct()
    {
        return view('supplier.data_sale.optimize_product');
    }

    public function fixBrandProduct()
    {
        return view('supplier.data_sale.fixBrandProduct');
    }

    public function fixDescProduct()
    {
        return view('supplier.data_sale.fixDescProduct');
    }

    public function fixImgProduct()
    {
        return view('supplier.data_sale.fixImgProduct');
    }

    public function fixVariantProduct()
    {
        return view('supplier.data_sale.fixVariantProduct');
    }

    public function chat()
    {
        return view('supplier.data_sale.chat');
    }
}
