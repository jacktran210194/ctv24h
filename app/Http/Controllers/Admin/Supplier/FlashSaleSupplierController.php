<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FlashSaleSupplierController extends Controller
{
    public function index(Request $request)
    {
        switch ($request->status) {
            case 'is_coming':
                $status = 'is_coming';
                break;
            case 'is_online':
                $status = 'is_online';
                break;
            case 'is_end':
                $status = 'is_end';
                break;
            default:
                $status = 'all';
                break;
        }
        $dataReturn = [
            'title' => 'FLASH SALE CỦA SHOP',
            'status' => $status
        ];
        return view('supplier.marketing.flash_sale.index', $dataReturn);
    }

    public function add_flash_sale(Request $request)
    {
        $dataReturn = [
            'title' => 'Tạo chương trình Flash sale của shop'
        ];
        return view('supplier.marketing.flash_sale.add_flash_sale', $dataReturn);
    }

    public function add_product_flash_sale()
    {
        $dataReturn = [
            'title' => 'Chọn sản phẩm'
        ];
        return view('supplier.marketing.flash_sale.add_product_flash_sale', $dataReturn);
    }


    //get list product
    public function get_list_product()
    {
        $dataReturn = [
            'status' => true
        ];
        return view('supplier.marketing.flash_sale.show_list_product', $dataReturn);
    }

    //get import product
    public function get_import_product()
    {
        $dataReturn = [
            'status' => true,
        ];
        return view('supplier.marketing.flash_sale.show_import_product', $dataReturn);
    }
}
