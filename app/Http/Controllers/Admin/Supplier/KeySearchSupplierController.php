<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use App\Models\CategoryProductModel;
use App\Models\KeySearchModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class KeySearchSupplierController extends Controller
{
    /**
     * KeySearchController constructor.
     * @param Request $request
     */
//    public function __construct(Request $request)
//    {
//        if (!$request->session()->has('data_supplier')) {
//            Redirect::to('admin/supplier_admin/login')->send();
//        }
//    }

    /**
     *  Danh sách từ khóa
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){
        $data = KeySearchModel::orderBy('product_category', 'desc')->get();
        foreach ($data as $k => $v){
            $dataProductCategory = CategoryProductModel::find($v['product_category']);
            $data[$k]['name'] = $dataProductCategory['name'] ?? '';
        }
        $dataReturn = [
            'data' => $data,
            'title' => 'Xu hướng tìm kiếm',
            'key_search' => true
        ];
        return view('supplier.key_search.index', $dataReturn);
    }

    /**
     *  Xóa từ khóa
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        try {
            KeySearchModel::destroy($request->id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
