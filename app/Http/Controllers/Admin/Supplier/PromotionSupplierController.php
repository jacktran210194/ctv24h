<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use App\Models\PromotionModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;

class PromotionSupplierController extends Controller
{
    /**
     * VoucherController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        if (!$request->session()->has('data_supplier')) {
            Redirect::to('admin/login')->send();
        }
    }

    /**
     *  Danh sách mã giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $data_supplier = session()->get('data_supplier');
        if(isset($data_supplier)){
            $data = PromotionModel::where('shop_id', $data_supplier->customer_id)->orderBy('id', 'desc')->get();
            if ($data){
                foreach ($data as $k => $v){
                    $today = Carbon::now('Asia/Ho_Chi_Minh');
                    if ($v->time_end > $today && $v->time_start < $today){
                        $data[$k]['status'] = 'Đang diễn ra';
                        $data[$k]['color'] = 'success';
                    } else if ($v->time_end > $today && $v->time_start > $today) {
                        $data[$k]['status'] = 'Sắp diễn ra';
                        $data[$k]['color'] = 'warning';
                    } else if ($v->time_end < $today && $v->time_start < $today) {
                        $data[$k]['status'] = 'Đã diễn ra';
                        $data[$k]['color'] = 'danger';
                    }
                    if($v->apply_for == 0){
                        $data[$k]['apply_for'] = 'Mã giảm giá toàn shop';
                    } else {
                        $data[$k]['apply_for'] = 'Mã giảm giá sản phẩm';
                    }
                }
            }
        }
        $dataReturn = [
            'data' => $data,
            'title' => 'Mã giảm giá',
            'promotion' => true
        ];
        return view('supplier.promotion.index', $dataReturn);
    }

    /**
     *  Thêm mã giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $dataSupplier = session()->get('data_supplier');
        $dataProduct = DB::table('product')
            ->select('product.*')
            ->join('shop', 'product.shop_id', '=', 'shop.id')
            ->where('shop.customer_id', $dataSupplier->customer_id)
            ->orderBy('product.id', 'desc')
            ->get();
        $data_return = [
            'title' => 'Mã giảm giá',
            'promotion' => true,
            'dataProduct' => $dataProduct
        ];
        return view('supplier.promotion.form', $data_return);
    }

    /**
     *  Lưu mã giảm giá
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        if ($request->id) {
            $data_return = $this->update($request);
        } else {
            $data_return = $this->create($request);
        }
        return response()->json($data_return, Response::HTTP_OK);
    }

    /**
     * @param $request
     * @param $rules
     * @param $customMessages
     * @return array
     */
    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /**
     *  Thêm mã giảm giá
     * @param $request
     * @return array
     */
    public function create($request)
    {
        $rules = [
            'apply_for' => 'required',
            'title' => 'required',
            'code' => 'regex:/(^([a-zA-Z0-9]+)(\d+)?$)/u|required|unique:promotion,code',
            'time_start' => 'required|after:today',
            'time_end' => 'required',
            'type' => 'required',
            'money_or_percent' => 'required',
            'value_discount' => 'required',
//            'maximum' => 'required',
            'value_order_min' => 'required',
            'number_save' => 'required',
            'number_use' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'regex' => 'Mã giảm giá không được nhập ký tự đặc biệt !',
            'time_start.after' => 'Ngày bắt đầu không được nhỏ hơn ngày hiện tại'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if ((!$request->hasFile('image'))) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file1 = $request->file('image');
            if (!$file1->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $data_supplier = session()->get('data_supplier');
            $data = new PromotionModel();
            if($request->apply_for == 1){
                $data->apply_for = 1;
                $data->product_id = $request->product_id;
            }
            $data->apply_for = $request->apply_for;
            $data->title = $request->title;
//            $data->code = $request->code;
            $data->code = mb_strtoupper($request->code, 'utf-8');
            $data->time_start = $request->time_start;
            $data->time_end = $request->time_end;
            $data->type = $request->type;
            $data->money_or_percent = $request->money_or_percent;
            $data->value_discount = $request->value_discount;
            if($data->money_or_percent == 1){
                $data->maximum = $request->maximum;
            } else {
                $data->maximum = null;
            }
            $data->value_order_min = $request->value_order_min;
            $data->number_save = $request->number_save;
            $data->number_use = $request->number_use;
            $data->shop_id = $data_supplier->customer_id;
            $fileNameImg = Str::random(20) . '.' . $file1->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/promotion/';
            $file1->move($path, $fileNameImg);
            $data->image = '/uploads/images/promotion/' . $fileNameImg;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/promotion'),
            ];
        }
        return $dataReturn;
    }

    /**
     *  Sửa mã giảm giá
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Request $request)
    {
        $dataSupplier = session()->get('data_supplier');
        $dataProduct = DB::table('product')
            ->select('product.*')
            ->join('shop', 'product.shop_id', '=', 'shop.id')
            ->where('shop.customer_id', $dataSupplier->customer_id)
            ->orderBy('product.id', 'desc')
            ->get();
        $data = PromotionModel::find($request->id);
        $data_return = [
            'title' => 'Mã giảm giá',
            'promotion' => true,
            'data' => $data,
            'dataProduct' => $dataProduct
        ];
        return view('supplier.promotion.form', $data_return);
    }

    /**
     *  Cập nhật mã giảm giá
     * @param $request
     * @return array
     */
    public function update($request)
    {
        $rules = [
            'apply_for' => 'required',
            'title' => 'required',
            'code' => 'regex:/(^([a-zA-Z0-9]+)(\d+)?$)/u|required|unique:promotion,code',
            'time_start' => 'required',
            'time_end' => 'required',
            'type' => 'required',
            'money_or_percent' => 'required',
            'value_discount' => 'required',
//            'maximum' => 'required',
            'value_order_min' => 'required',
            'number_save' => 'required',
            'number_use' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'unique' => 'Mã giảm giá đã tồn tại !'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data = PromotionModel::find($request->id);
        if ($data) {
            if ($request->hasFile('image')) {
                $fileImg = $request->file('image');
                if (!$fileImg->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data->image);
                $fileNameImg = Str::random(20) . '.' . $fileImg->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/promotion/';
                $fileImg->move($path, $fileNameImg);
                $data->image = '/uploads/images/promotion/' . $fileNameImg;
                $data->save();
            }
            $data_supplier = session()->get('data_supplier');
            $data->apply_for = $request->apply_for;
            $data->title = $request->title;
            $data->code = mb_strtoupper($request->code, 'utf-8');
//            mb_strtoupper($title ?? '', 'utf-8')
//                $request->code;
            $data->time_start = $request->time_start;
            $data->time_end = $request->time_end;
            $data->type = $request->type;
            $data->money_or_percent = $request->money_or_percent;
            if($data->money_or_percent == 1){
                $data->maximum = $request->maximum;
            } else {
                $data->maximum = null;
            }
            $data->value_discount = $request->value_discount;
            $data->value_order_min = $request->value_order_min;
            $data->number_save = $request->number_save;
            $data->number_use = $request->number_use;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/promotion'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    /**
     *  Xóa mã giảm giá
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            $data = PromotionModel::find($id);
            if ($data) {
                File::delete($data->image);
            }
            PromotionModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
