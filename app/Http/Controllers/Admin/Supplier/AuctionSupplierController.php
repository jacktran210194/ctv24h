<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuctionSupplierController extends Controller
{
    //đấu giá
    public function index()
    {
        $dataReturn = [
            'title' => 'Đấu giá'
        ];
        return view('supplier.marketing.auction.index', $dataReturn);
    }

    //đăng ký đấu giá
    public function register()
    {
        $dataReturn = [
            'title' => 'Đấu giá'
        ];
        return view('supplier.marketing.auction.register_auction', $dataReturn);
    }

    //Đăng ký sản phẩm
    public function product()
    {
        $dataReturn = [
            'title' => 'Đấu giá'
        ];
        return view('supplier.marketing.auction.product', $dataReturn);
    }

    ////đấu giá của tôi
    public function auction_my()
    {
        $dataReturn = [
            'title' => 'Đấu giá'
        ];
        return view('supplier.marketing.auction.auction_my.index', $dataReturn);
    }
}
