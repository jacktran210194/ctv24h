<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Exports\Supplier\DropshipCollaboratorExport;
use App\Exports\Supplier\ListCollaboratorExport;
use App\Exports\Supplier\OrderSupplierExport;
use App\Http\Controllers\Controller;
use App\Models\AddressCustomerCTVModel;
use App\Models\AttributeValueModel;
use App\Models\CityModel;
use App\Models\CollaboratorModel;
use App\Models\CustomerAddressModel;
use App\Models\CustomerModel;
use App\Models\DistrictModel;
use App\Models\FeeCTV24HModel;
use App\Models\ManageCTVModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductAttributeModel;
use App\Models\ProductModel;
use App\Models\ProductShippingModel;
use App\Models\ShippingSupplierModel;
use App\Models\ShopModel;
use App\Models\WardModel;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Facades\Excel;

class ManageCollaboratorController extends Controller
{
    /**
     *
     *  Danh sách CTV
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
//        $type = $request->type === 'list' ? 'list' : 'dropship';
        $type = 'list';
        $dataSupplier = session()->get('data_supplier');
        $data = DB::table('shop')
            ->select('order.customer_id as customer_id', 'customer.name as name_collaborator', 'customer.point as point',
                'shop.id as shop_id', 'customer.total_revenue as total_revenue', 'customer.total_quantity as total_quantity')
            ->distinct('customer_id')
            ->join('order', 'shop.id', '=', 'order.shop_id')
            ->join('customer', 'order.customer_id', '=', 'customer.id')
            ->where('shop.customer_id', $dataSupplier->customer_id)
            ->get();
        foreach ($data as $k => $v) {
            $data[$k]->dropship = 0;
            $data[$k]->total_price = 0;
            $dataOrder = DB::table('order')
                ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id', 'order.bonus as bonus')
                ->join('shop', 'order.shop_id', '=', 'shop.id')
                ->where('order.customer_id', $v->customer_id)
                ->get();
            $data[$k]->quantity = 0;
            foreach ($dataOrder as $kOrder => $vOrder) {
                if($vOrder->collaborator_id == 1){
                    $data[$k]->dropship += 1;
                }
            }
            if ($v->point >= 0 && $v->point <= 1000) {
                $data[$k]->rank = 'Hạng đồng';
                $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
            } else if ($v->point >= 1000 && $v->point < 2000) {
                $data[$k]->rank = 'Hạng bạc';
                $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_silver.png';
            } else if ($v->point >= 2000 && $v->point < 3000) {
                $data[$k]->rank = 'Hạng vàng';
                $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
            } else if($v->point > 3000) {
                $data[$k]->rank = 'Hạng kim cương';
                $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/diamond1.png';
            }
        }
        $data_return = [
            'type' => $type,
            'status' => true,
            'title' => 'Quản lý cộng tác viên của nhà cung cấp: ' . $dataSupplier['name'],
            'data' => $data
        ];
        return view('supplier.collaborator.list.index', $data_return);
    }

    /**
     *  Hàng Dropship
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function dropship()
    {
        $type= 'dropship';
        $dataSupplier = session()->get('data_supplier');
        $data = DB::table('shop')
            ->select('order.customer_id as customer_id', 'order.customer_id as customer_id', 'customer.name as name_collaborator', 'customer.point as point',
                'shop.id as shop_id', 'customer.total_revenue as total_revenue')
            ->distinct('customer_id')
            ->join('order', 'shop.id', '=', 'order.shop_id')
            ->join('customer', 'order.customer_id', '=', 'customer.id')
            ->where('shop.customer_id', $dataSupplier->customer_id)
            ->where('order.collaborator_id', 1)
            ->get();

        foreach ($data as $k => $v) {
            $data[$k]->dropship = 0;
            $data[$k]->total_price = 0;
            $dataOrder = DB::table('order')
                ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id', 'shop.id as shop_id', 'order.bonus as bonus')
                ->join('shop', 'order.shop_id', '=', 'shop.id')
                ->where('order.customer_id', $v->customer_id)
                ->where('shop.customer_id', $dataSupplier->customer_id)
                ->where('order.collaborator_id', 1)
                ->get();
            $data[$k]->quantity = 0;
            foreach ($dataOrder as $kOrder => $vOrder) {
                if($vOrder->collaborator_id == 1){
                    $data[$k]->dropship += 1;
                }
                $data[$k]->code = $vOrder->code;
                $dataOrderDetail = OrderDetailModel::where('order_id', $vOrder->id_order)->get();
                foreach ($dataOrderDetail as $kOrDe => $vOrDetail) {
                    $data[$k]->order_id = $vOrDetail->order_id;
                    $data[$k]->quantity += $vOrDetail->quantity;
                }
            }
            if ($v->point >= 0 && $v->point <= 1000) {
                $data[$k]->rank = 'Hạng đồng';
                $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
            } else if ($v->point >= 1000 && $v->point < 2000) {
                $data[$k]->rank = 'Hạng bạc';
                $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_silver.png';
            } else if ($v->point >= 2000 && $v->point < 3000) {
                $data[$k]->rank = 'Hạng vàng';
                $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
            } else if($v->point > 3000) {
                $data[$k]->rank = 'Hạng kim cương';
                $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/diamond1.png';
            }
        }
        $dataReturn = [
            'title' => 'Hàng Dropship',
            'data' => $data,
            'type' => $type,
        ];
        return view('supplier.collaborator.dropship.index', $dataReturn);
    }

    /**
     *  Xác nhận giao hàng
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delivery(Request $request){
        $data = OrderModel::where('id', $request->id)->update(['status' => 2]);
        $dataReturn = [
            'status' => true,
            'msg' => 'Xác nhận giao hàng',
            'data' => $data
        ];
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     *  Giao hàng loạt
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deliveryAll(Request $request){
        $dataSupplier = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataSupplier->customer_id)->first();
        $data = OrderModel::where('shop_id', $dataShop->id)
            ->where('is_selected', 1)
            ->where('collaborator_id', 1)
            ->where('status', 0)
            ->orwhere('status', 1)
            ->get();
        if($data){
            foreach ($data as $k => $v){
                $dataOrderUpdate = OrderModel::find($v->id);
                $dataOrderUpdate->status = 2;
                $dataOrderUpdate->save();
            }
        }
        $dataReturn = [
            'status' => true,
            'msg' => 'Xác nhận giao hàng loạt',
            'data' => $data
        ];
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     *  Tìm kiếm
     * @param Request $request
     */
    public function search(Request $request)
    {
        $dataSupplier = session()->get('data_supplier');
        if ($request->ajax()) {
            $output = '';
            $query = $request->get('query');
            if ($query != '') {
                $data = DB::table('shop')
                    ->select('order.customer_id as customer_id', 'customer.name as name_collaborator', 'customer.point as point',
                        'shop.id as shop_id', 'customer.total_revenue as total_revenue', 'customer.total_quantity as total_quantity')
                    ->distinct('customer_id')
                    ->join('order', 'shop.id', '=', 'order.shop_id')
                    ->join('customer', 'order.customer_id', '=', 'customer.id')
                    ->where('shop.customer_id', $dataSupplier->customer_id)
                    ->where('customer.name', 'like', '%' . $query . '%')
                    ->get();

                foreach ($data as $k => $v) {
                    $data[$k]->dropship = 0;
                    $data[$k]->total_price = 0;
                    $dataOrder = DB::table('order')
                        ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id', 'shop.id as shop_id', 'order.bonus as bonus')
                        ->join('shop', 'order.shop_id', '=', 'shop.id')
                        ->where('order.customer_id', $v->customer_id)
                        ->get();
                    $data[$k]->quantity = 0;
                    $dataCheck =  ManageCTVModel::where('shop_id', $v->shop_id)->where('collaborator_id', $v->customer_id)->first();
                    $data[$k]->turn_on = $dataCheck->turn_on;
                    foreach ($dataOrder as $kOrder => $vOrder) {
                        if($vOrder->collaborator_id == 1){
                            $data[$k]->dropship += 1;
                        }
                    }
                    if ($v->point >= 0 && $v->point <= 1000) {
                        $data[$k]->rank = 'Hạng đồng';
                        $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                    } else if ($v->point >= 1000 && $v->point < 2000) {
                        $data[$k]->rank = 'Hạng bạc';
                        $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_silver.png';
                    } else if ($v->point >= 2000 && $v->point < 3000) {
                        $data[$k]->rank = 'Hạng vàng';
                        $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                    } else if($v->point > 3000) {
                        $data[$k]->rank = 'Hạng kim cương';
                        $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/diamond1.png';
                    }
                }
            } else {
                $data = DB::table('shop')
                    ->select('order.customer_id as customer_id', 'customer.name as name_collaborator', 'customer.point as point',
                        'shop.id as shop_id', 'customer.total_revenue as total_revenue', 'customer.total_quantity as total_quantity')
                    ->distinct('customer_id')
                    ->join('order', 'shop.id', '=', 'order.shop_id')
                    ->join('customer', 'order.customer_id', '=', 'customer.id')
                    ->where('shop.customer_id', $dataSupplier->customer_id)
                    ->get();

                foreach ($data as $k => $v) {
                    $data[$k]->dropship = 0;
                    $data[$k]->total_price = 0;
                    $dataOrder = DB::table('order')
                        ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id', 'shop.id as shop_id', 'order.bonus as bonus')
                        ->join('shop', 'order.shop_id', '=', 'shop.id')
                        ->where('order.customer_id', $v->customer_id)
                        ->get();
                    $data[$k]->quantity = 0;
                    $dataCheck =  ManageCTVModel::where('shop_id', $v->shop_id)->where('collaborator_id', $v->customer_id)->first();
                    $data[$k]->turn_on = $dataCheck->turn_on;
                    foreach ($dataOrder as $kOrder => $vOrder) {
                        if($vOrder->collaborator_id == 1){
                            $data[$k]->dropship += 1;
                        }
                    }

                    if ($v->point >= 0 && $v->point <= 1000) {
                        $data[$k]->rank = 'Hạng đồng';
                        $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                    } else if ($v->point >= 1000 && $v->point < 2000) {
                        $data[$k]->rank = 'Hạng bạc';
                        $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_silver.png';
                    } else if ($v->point >= 2000 && $v->point < 3000) {
                        $data[$k]->rank = 'Hạng vàng';
                        $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                    } else if($v->point > 3000) {
                        $data[$k]->rank = 'Hạng kim cương';
                        $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/diamond1.png';
                    }
                }
            }
            $total_row = $data->count();
            if ($total_row > 0) {
                foreach ($data as $k => $row) {
                    $data_url_detail = URL::to('admin/manage_ctv/list_order/' . $row->customer_id);
                    $data_url = url('admin/manage_ctv/turn_on');
                    $data_ctv = $row->customer_id;
                    $data_shop_id = $row->shop_id;
                    $turn_on = $row->turn_on === 1 ? 'checked' : '';
                    $output .= '
                               <tr class="text-center">
                                    <td>' . $row->name_collaborator . '</td>
                                    <td>
                                        <img width="28px" height="20px" class="image-preview" src="' . $row->img . '">
                                        <p>' . $row->rank . '</p>
                                    </td>
                                    <td>' . $row->dropship . '</td>
                                    <td>' . $row->total_quantity . '</td>
                                    <td>' . number_format($row->total_revenue) . ' đ ' . '</td>
                                    <td>
                                        <div class="align-items-center justify-content-center">
                                            <label class="switch">
                                                <input class="turn_on" type="checkbox" '.$turn_on.'
                                                data-url="'.$data_url.'"
                                                data-ctv="'.$data_ctv.'"
                                                data-shop-id="'.$data_shop_id.'"
                                                >
                                                <span class="slider green round"></span>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                ';
                }
            } else {
                $output = '
                           <tr>
                            <td colspan="5">Không có dữ liệu</td>
                           </tr>
                          ';
            }
            $data = array(
                'table_data' => $output,
                'total_data' => $total_row
            );
            echo json_encode($data);
        }
    }

    /**
     *  Danh sách đơn hàng
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function listOrder($id)
    {
        $dataFeeCTV24 = FeeCTV24HModel::first();
        $dataSupplier = session()->get('data_supplier');
        $data_shop = ShopModel::where('supplier_id', $dataSupplier->id)->first();
        $data_order = OrderModel::where('shop_id', $data_shop->id)->where('customer_id', $id)->where('is_selected', 1)->get();
        foreach ($data_order as $key => $value) {
            if($value->collaborator_id == 1){
                $data_order[$key]['type_order'] = 'Giao cho khách CTV';
            } else {
                $data_order[$key]['type_order'] = '';
            }
            if($value->payment_id == 0){
                $data_order[$key]['payment'] = 'Ví CTV24H';
            } else if($value->payment_id == 1) {
                $data_order[$key]['payment'] = 'Trả góp';
            } else if ($value->payment_id == 2) {
                $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
            } else {
                $data_order[$key]['payment'] = 'Thẻ tín dụng/ghi nợ';
            }

            if($value->shipping_id == 0){
                $data_order[$key]['shipping'] = 'Vận chuyển nhanh quốc tế';
            } else if($value->shipping_id == 1) {
                $data_order[$key]['shipping'] = 'Giao hàng tiết kiệm';
            } else if ($value->shipping_id == 2) {
                $data_order[$key]['shipping'] = 'Giao hàng nhanh';
            } else if ($value->shipping_id == 3) {
                $data_order[$key]['shipping'] = 'J&T Express';
            } else {
                $data_order[$key]['shipping'] = 'Viettel Post';
            }
            $data_order[$key]->total_price = ($value->total_price + 10000) +  ($value->total_price) * ($dataFeeCTV24->fee/100);
        }
        $data_return = [
            'title' => 'Đơn hàng đã bán',
            'data' => $data_order
        ];
        return view('supplier.collaborator.list.list_order', $data_return);
    }

//    /**
//     *  Attribute Product
//     * @param $value
//     * @return array
//     */
//    protected function getDataValueAttr($value)
//    {
//        $item = explode(',', $value);
//        $data = [];
//        if (isset($item)) {
//            foreach ($item as $v) {
//                $__dataAttrValue = AttributeValueModel::find($v);
//                if (isset($__dataAttrValue)) {
//                    $__dataAttrProduct = ProductAttributeModel::find($__dataAttrValue->attribute_id);
//                    $__data['type'] = isset($__dataAttrProduct) ? $__dataAttrProduct['name'] : '';
//                    $__data['value'] = isset($__dataAttrValue) ? $__dataAttrValue['value'] : '';
//                    array_push($data, $__data);
//                }
//            }
//        }
//        return $data;
//    }

    /**
     *  Chi tiết đơn hàng
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function orderDetail($id) {
        $fee = FeeCTV24HModel::first();
        $data_order = OrderModel::find($id);
        $data_customer = CustomerModel::find($data_order->customer_id);
        $data_address = CustomerAddressModel::where('customer_id', $data_customer->id)->where('default', 1)->first();
        if($data_address){
            $data_order['name_shipping'] = $data_address->name;
            $data_order['phone_shipping'] = $data_address->phone;
            $data_city = CityModel::find($data_address->city_id);
            $data_district = DistrictModel::find($data_address->district_id);
            $data_ward = WardModel::where('ward_id',$data_address->ward_id)->first();
            $data_order->address_shipping = $data_address->address . ', ' . $data_ward->name . ', ' . $data_district->name . ', ' . $data_city->name;
        }
        if ($data_order->shipping_id == 0) {
            $data_order->shipping = 'Vận chuyển nhanh quốc tế';
        } elseif ($data_order->shipping_id == 1) {
            $data_order->shipping = 'Giao hàng tiết kiệm';
        } elseif ($data_order->shipping_id == 2) {
            $data_order->shipping = 'Giao hàng nhanh';
        } elseif ($data_order->shipping_id == 3) {
            $data_order->shipping = 'J&T Express';
        } else {
            $data_order->shipping = 'Viettel Post';
        }
        if ($data_order->payment_id == 0) {
            $data_order->payment = 'Ví CTV24H';
        } elseif ($data_order->payment_id == 1) {
            $data_order->payment = 'Trả góp';
        } elseif ($data_order->payment_id == 2) {
            $data_order->payment = 'Thanh toán khi nhận hàng';
        } else {
            $data_order->payment = 'Thẻ tín dụng hoặc thẻ ghi nợ';
        }
        if ($data_order->status == 4 && $data_order->cancel_confirm == 0) {
            $data_order['is_status'] = 'Yêu cầu huỷ hàng';
            if ($data_order['cancel_user'] == 0) {
                $data_order['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                $data_order['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
            } elseif ($data_order['cancel_user'] == 1) {
                $data_order['user_cancel'] = 'Đã huỷ bởi người mua';
                if ($data_order['cancel_reason'] == 0) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                } elseif ($data_order['cancel_reason'] == 1) {
                    $data_order['reason_cancel'] = 'Muốn nhập mã Voucher';
                } elseif ($data_order['cancel_reason'] == 2) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                } elseif ($data_order['cancel_reason'] == 3) {
                    $data_order['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                } elseif ($data_order['cancel_reason'] == 4) {
                    $data_order['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                } else {
                    $data_order['reason_cancel'] = 'Lý do khác';
                }
            }
        } elseif ($data_order->status == 4 && $data_order->cancel_confirm == 1) {
            $data_order['is_status'] = 'Đã huỷ';
            if ($data_order['cancel_user'] == 0) {
                $data_order['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                $data_order['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
            } elseif ($data_order['cancel_user'] == 1) {
                $data_order['user_cancel'] = 'Đã huỷ bởi người mua';
                if ($data_order['cancel_reason'] == 0) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                } elseif ($data_order['cancel_reason'] == 1) {
                    $data_order['reason_cancel'] = 'Muốn nhập mã Voucher';
                } elseif ($data_order['cancel_reason'] == 2) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                } elseif ($data_order['cancel_reason'] == 3) {
                    $data_order['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                } elseif ($data_order['cancel_reason'] == 4) {
                    $data_order['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                } else {
                    $data_order['reason_cancel'] = 'Lý do khác';
                }
            }
        }
        $data_order_detail = OrderDetailModel::where('order_id', $data_order->id)->get();
        foreach ($data_order_detail as $key => $value) {
            $data_product = ProductModel::find($value['product_id']);
            $data_order_detail[$key]['product_name'] = $data_product['name'];
            $data_order_detail[$key]['product_image'] = $data_product['image'];
            $dataValue_Attribute = AttributeValueModel::find($value->type_product);
            $data_order_detail[$key]->value_attr = $dataValue_Attribute->value ?? '';
            $data_order_detail[$key]->size_of_value = $dataValue_Attribute->size_of_value ?? '';
        }
        $dataReturn = [
            'title' => 'Chi tiết đơn hàng: ' . $data_order['order_code'],
            'customer' => $data_customer,
            'order' => $data_order,
            'order_detail' => $data_order_detail,
            'fee' => $fee
        ];
        return view('supplier.collaborator.list.detail', $dataReturn);
    }

    /**
     *  Danh sách đơn hàng Dropship
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function listOrderDropship($id)
    {
        $dataFeeCTV24 = FeeCTV24HModel::first();
        $dataSupplier = session()->get('data_supplier');
        $data_shop = ShopModel::where('supplier_id', $dataSupplier->id)->first();
        $data_order = OrderModel::where('shop_id', $data_shop->id)->where('customer_id', $id)->where('is_selected', 1)->where('collaborator_id', 1)->get();
        foreach ($data_order as $key => $value) {
            if($value->collaborator_id == 1){
                $data_order[$key]['type_order'] = 'Giao cho khách CTV';
            } else {
                $data_order[$key]['type_order'] = '';
            }
            if($value->payment_id == 0){
                $data_order[$key]['payment'] = 'Ví CTV24H';
            } else if($value->payment_id == 1) {
                $data_order[$key]['payment'] = 'Trả góp';
            } else if ($value->payment_id == 2) {
                $data_order[$key]['payment'] = 'Thanh toán khi nhận hàng';
            } else {
                $data_order[$key]['payment'] = 'Thẻ tín dụng/ghi nợ';
            }

            if($value->shipping_id == 0){
                $data_order[$key]['shipping'] = 'Vận chuyển nhanh quốc tế';
            } else if($value->shipping_id == 1) {
                $data_order[$key]['shipping'] = 'Giao hàng tiết kiệm';
            } else if ($value->shipping_id == 2) {
                $data_order[$key]['shipping'] = 'Giao hàng nhanh';
            } else if ($value->shipping_id == 3) {
                $data_order[$key]['shipping'] = 'J&T Express';
            } else {
                $data_order[$key]['shipping'] = 'Viettel Post';
            }
            $data_order[$key]->total_price = ($value->total_price + 10000) +  ($value->total_price) * ($dataFeeCTV24->fee/100);
        }
        $data_return = [
            'title' => 'Đơn hàng đã bán',
            'data' => $data_order
        ];
        return view('supplier.collaborator.dropship.list_order', $data_return);
    }

    /**
     *  Chi tiết đơn hàng Dropship
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function dropshipDetail($id){
        $fee = FeeCTV24HModel::first();
        $data_order = OrderModel::find($id);
        $data_customer = CustomerModel::find($data_order->customer_id);
        $data_address = CustomerAddressModel::where('customer_id', $data_customer->id)->where('default', 1)->first();
        if($data_address){
            $data_order['name_shipping'] = $data_address->name;
            $data_order['phone_shipping'] = $data_address->phone;
            $data_city = CityModel::find($data_address->city_id);
            $data_district = DistrictModel::find($data_address->district_id);
            $data_ward = WardModel::where('ward_id',$data_address->ward_id)->first();
            $data_order->address_shipping = $data_address->address . ', ' . $data_ward->name . ', ' . $data_district->name . ', ' . $data_city->name;
        }
        if ($data_order->shipping_id == 0) {
            $data_order->shipping = 'Vận chuyển nhanh quốc tế';
        } elseif ($data_order->shipping_id == 1) {
            $data_order->shipping = 'Giao hàng tiết kiệm';
        } elseif ($data_order->shipping_id == 2) {
            $data_order->shipping = 'Giao hàng nhanh';
        } elseif ($data_order->shipping_id == 3) {
            $data_order->shipping = 'J&T Express';
        } else {
            $data_order->shipping = 'Viettel Post';
        }
        if ($data_order->payment_id == 0) {
            $data_order->payment = 'Ví CTV24H';
        } elseif ($data_order->payment_id == 1) {
            $data_order->payment = 'Trả góp';
        } elseif ($data_order->payment_id == 2) {
            $data_order->payment = 'Thanh toán khi nhận hàng';
        } else {
            $data_order->payment = 'Thẻ tín dụng hoặc thẻ ghi nợ';
        }
        if ($data_order->status == 4 && $data_order->cancel_confirm == 0) {
            $data_order['is_status'] = 'Yêu cầu huỷ hàng';
            if ($data_order['cancel_user'] == 0) {
                $data_order['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                $data_order['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
            } elseif ($data_order['cancel_user'] == 1) {
                $data_order['user_cancel'] = 'Đã huỷ bởi người mua';
                if ($data_order['cancel_reason'] == 0) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                } elseif ($data_order['cancel_reason'] == 1) {
                    $data_order['reason_cancel'] = 'Muốn nhập mã Voucher';
                } elseif ($data_order['cancel_reason'] == 2) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                } elseif ($data_order['cancel_reason'] == 3) {
                    $data_order['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                } elseif ($data_order['cancel_reason'] == 4) {
                    $data_order['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                } else {
                    $data_order['reason_cancel'] = 'Lý do khác';
                }
            }
        } elseif ($data_order->status == 4 && $data_order->cancel_confirm == 1) {
            $data_order['is_status'] = 'Đã huỷ';
            if ($data_order['cancel_user'] == 0) {
                $data_order['user_cancel'] = 'Đã huỷ tự động bởi hệ thống CTV24h';
                $data_order['reason_cancel'] = 'Người bán không xử lý đơn hàng đúng hạn';
            } elseif ($data_order['cancel_user'] == 1) {
                $data_order['user_cancel'] = 'Đã huỷ bởi người mua';
                if ($data_order['cancel_reason'] == 0) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi địa chỉ hàng';
                } elseif ($data_order['cancel_reason'] == 1) {
                    $data_order['reason_cancel'] = 'Muốn nhập mã Voucher';
                } elseif ($data_order['cancel_reason'] == 2) {
                    $data_order['reason_cancel'] = 'Muốn thay đổi sản phẩm trong đơn hàng';
                } elseif ($data_order['cancel_reason'] == 3) {
                    $data_order['reason_cancel'] = 'Thủ tục thanh toán quá rắc rối';
                } elseif ($data_order['cancel_reason'] == 4) {
                    $data_order['reason_cancel'] = 'Tìm thấy giá rẻ hơn ở chỗ khác';
                } else {
                    $data_order['reason_cancel'] = 'Lý do khác';
                }
            }
        }
        $data_order_detail = OrderDetailModel::where('order_id', $data_order->id)->get();
        foreach ($data_order_detail as $key => $value) {
            $data_product = ProductModel::find($value['product_id']);
            $data_order_detail[$key]['product_name'] = $data_product['name'];
            $data_order_detail[$key]['product_image'] = $data_product['image'];
            $dataValue_Attribute = AttributeValueModel::find($value->type_product);
            $data_order_detail[$key]->value_attr = $dataValue_Attribute->value ?? '';
            $data_order_detail[$key]->size_of_value = $dataValue_Attribute->size_of_value ?? '';
        }
        $dataReturn = [
            'title' => 'Chi tiết đơn hàng: ' . $data_order['order_code'],
            'customer' => $data_customer,
            'order' => $data_order,
            'order_detail' => $data_order_detail,
            'fee' => $fee
        ];
        return view('supplier.collaborator.dropship.detail', $dataReturn);
    }

    /**
     *
     *
     * @param Request $request
     */
    public function searchDropship(Request $request){
        $dataLogin = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataLogin->customer_id)->first();
        if ($request->ajax()) {
            $output = '';
            $query = $request->get('query');
            if ($query != '') {
                $data = ProductModel::where('name', 'like', '%' . $query . '%')->where('shop_id', $dataShop->id)
                    ->where('active', 1)->orderBy('id', 'desc')
                    ->get();
                $count_data = count($data);
            } else {
                $data = ProductModel::where('shop_id', $dataShop->id)
                    ->where('active', 1)
                    ->orderBy('id', 'desc')
                    ->get();
                $count_data = count($data);
            }
            $total_row = $data->count();
            if ($total_row > 0) {
                foreach ($data as $k => $row) {
                    if(!($row->sku_type)){
                        $data[$k]->sku_type = '-';
                    }else{
                        $data[$k]->sku_type = $row->sku_type;
                    }
                    $data[$k]->dropship = 'Hàng Dropship';
                    $attr_product = DB::table('attribute_value')
                        ->select('product_attribute.*', 'attribute_value.*')
                        ->join('product_attribute', 'attribute_value.attribute_id', '=', 'product_attribute.id')
                        ->where('attribute_value.product_id', $row->id)
                        ->orderBy('attribute_value.id', 'asc')
                        ->get();
                    $product_category = '';
                    $product_price = '';
                    $product_quantity = '';
                    $product_sold = '';
                    if (!$attr_product) {
                        $product_category .= '-';
                        $product_price .= '<div class="text-red text-bold-700 text-center">' . number_format($row->price) . ' đ</div>';
                        $product_quantity .= '<div class="text-bold-700 text-center">' . $row->warehouse . '</div>';
                        $product_sold .= '<div class="text-bold-700">' . $row->is_sell . '</div>';
                    }
                    foreach ($attr_product as $item) {
                        if ($item->quantity > 0) {
                            $quantity = $item->quantity;
                        } else {
                            $quantity = '<button class="disabled" disabled="disabled">hết hàng</button>';
                        }
                        $product_category .= '<div class="padding-10 color-gray font-size-14 text-left">' . $item->category_1 . ':' . $item->category_2. '/' . $item->size_of_value . '</div>';
                        $product_price .= '<div class="padding-10 color-red font-weight-bold text-center">' . number_format($item->price) . 'đ</div>';
                        $product_quantity .= '<div class="padding-10 m-0 text-center font-weight-bold font-size-14" >' . $quantity . '</div>';
                        $product_sold .= '<div class="padding-10 m-0 text-center font-weight-bold font-size-14" >' . $item->is_sell . '</div>';
                    }
                    $output .= '
                                <tr class="border_box mg-top-30" data-count="' . $count_data . '">
                                    <td class="align-items-lg-start justify-content-lg-center d-flex"><span class="padding-10 font-weight-bold align-items-lg-start color-blue">' . $row->dropship . '</span></td>
                                    <td class="position-relative" style="min-width: 200px"><div class="content-product position-absolute padding-10 d-flex align-items-lg-start font-weight-bold font-size-14">' . '<img width="60px" height="60px" class="border-radius-8 image-preview" src="' . $row->image . '"
                                             alt="">' . $row->name . '</div>
                                    </td>
                                    <td class="align-items-lg-start justify-content-lg-center d-flex"><span class="padding-10 font-weight-bold color-black align-items-lg-start">' . $row->sku_type . '</span></td>
                                    <td>' . $product_category . '</td>
                                    <td>' . $product_price . '</td>
                                    <td>' . $product_sold . '</td>
                                    <td>' . $product_quantity . '</td>
                                </tr>
                                ';
                }
            } else {
                $output = '
                           <tr>
                            <td colspan="5">Không có dữ liệu</td>
                           </tr>
                          ';
            }
            $data = array(
                'table_data' => $output,
                'total_data' => $total_row
            );
            echo json_encode($data);
        }
    }

    /**
     *  Xuất báo cáo Danh sách CTV
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportListCollaborator(){
        return Excel::download(new ListCollaboratorExport(), 'List_Collaborator.xlsx');
    }

    /**
     *  Xuất báo cáo Hàng Dropship
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportDropshipCollaborator(){
        return Excel::download(new DropshipCollaboratorExport(), 'Dropship_Collaborator.xlsx');
    }

    /**
     *  Bật/Tắt CTV
     * @param Request $request
     * @return array
     */
    public function turnOn(Request $request){
        $dataLogin = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataLogin->customer_id)->first();
        $check = $request->check;
        $data = ManageCTVModel::where('shop_id', $dataShop->id)
            ->where('collaborator_id', $request->customer_id)
            ->first();
        if ($check == 1) {
            $data->turn_on = 1;
            $data->save();
            $dataReturn = [
                'status' => true,
                'msg' => 'Bật CTV thành công',
                'url' => url('admin/manage_ctv')
            ];
            return $dataReturn;
        }
        if ($check == 0) {
            $data->turn_on = 0;
            $data->save();
            $dataReturn = [
                'status' => true,
                'msg' => 'Tắt CTV thành công!',
                'url' => url('admin/manage_ctv'),
            ];
            return $dataReturn;
        }
    }

    /**
     *  Lọc theo hạng
     * @param $rank
     */
    public function filterRank($rank){
        $dataSupplier = session()->get('data_supplier');
        $output = '';
        if($rank === 'bronze'){
            $data = DB::table('shop')
                ->select('order.customer_id as customer_id', 'customer.name as name_collaborator', 'customer.point as point',
                    'shop.id as shop_id', 'customer.total_revenue as total_revenue', 'customer.total_quantity as total_quantity')
                ->distinct('customer_id')
                ->join('order', 'shop.id', '=', 'order.shop_id')
                ->join('customer', 'order.customer_id', '=', 'customer.id')
                ->where('shop.customer_id', $dataSupplier->customer_id)
                ->where('customer.point', '<', 1000)
                ->get();
            foreach ($data as $k => $v) {
                $data[$k]->dropship = 0;
                $data[$k]->total_price = 0;
                $dataOrder = DB::table('order')
                    ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id', 'order.bonus as bonus', 'shop.id as shop_id')
                    ->join('shop', 'order.shop_id', '=', 'shop.id')
                    ->where('order.customer_id', $v->customer_id)
                    ->get();
                $data[$k]->quantity = 0;
                $dataCheck =  ManageCTVModel::where('shop_id', $v->shop_id)->where('collaborator_id', $v->customer_id)->first();
                $data[$k]->turn_on = $dataCheck->turn_on;
                foreach ($dataOrder as $kOrder => $vOrder) {
                    if($vOrder->collaborator_id == 1){
                        $data[$k]->dropship += 1;
                    }
                }
                if ($v->point >= 0 && $v->point <= 1000) {
                    $data[$k]->rank = 'Hạng đồng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if ($v->point >= 1000 && $v->point < 2000) {
                    $data[$k]->rank = 'Hạng bạc';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_silver.png';
                } else if ($v->point >= 2000 && $v->point < 3000) {
                    $data[$k]->rank = 'Hạng vàng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if($v->point > 3000) {
                    $data[$k]->rank = 'Hạng kim cương';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/diamond1.png';
                }
            }
        } elseif ($rank === 'silver') {
            $data = DB::table('shop')
                ->select('order.customer_id as customer_id', 'customer.name as name_collaborator', 'customer.point as point',
                    'shop.id as shop_id', 'customer.total_revenue as total_revenue', 'customer.total_quantity as total_quantity')
                ->distinct('customer_id')
                ->join('order', 'shop.id', '=', 'order.shop_id')
                ->join('customer', 'order.customer_id', '=', 'customer.id')
                ->where('shop.customer_id', $dataSupplier->customer_id)
                ->where('customer.point', '>=', 1000)
                ->where('customer.point', '<', 2000)
                ->get();
            foreach ($data as $k => $v) {
                $data[$k]->dropship = 0;
                $data[$k]->total_price = 0;
                $dataOrder = DB::table('order')
                    ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id', 'order.bonus as bonus', 'shop.id as shop_id')
                    ->join('shop', 'order.shop_id', '=', 'shop.id')
                    ->where('order.customer_id', $v->customer_id)
                    ->get();
                $data[$k]->quantity = 0;
                $dataCheck =  ManageCTVModel::where('shop_id', $v->shop_id)->where('collaborator_id', $v->customer_id)->first();
                $data[$k]->turn_on = $dataCheck->turn_on;
                foreach ($dataOrder as $kOrder => $vOrder) {
                    if($vOrder->collaborator_id == 1){
                        $data[$k]->dropship += 1;
                    }
                }
                if ($v->point >= 0 && $v->point <= 1000) {
                    $data[$k]->rank = 'Hạng đồng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if ($v->point >= 1000 && $v->point < 2000) {
                    $data[$k]->rank = 'Hạng bạc';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_silver.png';
                } else if ($v->point >= 2000 && $v->point < 3000) {
                    $data[$k]->rank = 'Hạng vàng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if($v->point > 3000) {
                    $data[$k]->rank = 'Hạng kim cương';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/diamond1.png';
                }
            }
        } elseif ($rank === 'gold'){
            $data = DB::table('shop')
                ->select('order.customer_id as customer_id', 'customer.name as name_collaborator', 'customer.point as point',
                    'shop.id as shop_id', 'customer.total_revenue as total_revenue', 'customer.total_quantity as total_quantity')
                ->distinct('customer_id')
                ->join('order', 'shop.id', '=', 'order.shop_id')
                ->join('customer', 'order.customer_id', '=', 'customer.id')
                ->where('shop.customer_id', $dataSupplier->customer_id)
                ->where('customer.point', '>=', 2000)
                ->where('customer.point', '<', 3000)
                ->get();
            foreach ($data as $k => $v) {
                $data[$k]->dropship = 0;
                $data[$k]->total_price = 0;
                $dataOrder = DB::table('order')
                    ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id', 'order.bonus as bonus', 'shop.id as shop_id')
                    ->join('shop', 'order.shop_id', '=', 'shop.id')
                    ->where('order.customer_id', $v->customer_id)
                    ->get();
                $data[$k]->quantity = 0;
                $dataCheck =  ManageCTVModel::where('shop_id', $v->shop_id)->where('collaborator_id', $v->customer_id)->first();
                $data[$k]->turn_on = $dataCheck->turn_on;
                foreach ($dataOrder as $kOrder => $vOrder) {
                    if($vOrder->collaborator_id == 1){
                        $data[$k]->dropship += 1;
                    }
                }
                if ($v->point >= 0 && $v->point <= 1000) {
                    $data[$k]->rank = 'Hạng đồng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if ($v->point >= 1000 && $v->point < 2000) {
                    $data[$k]->rank = 'Hạng bạc';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_silver.png';
                } else if ($v->point >= 2000 && $v->point < 3000) {
                    $data[$k]->rank = 'Hạng vàng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if($v->point > 3000) {
                    $data[$k]->rank = 'Hạng kim cương';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/diamond1.png';
                }
            }
        } else {
            $data = DB::table('shop')
                ->select('order.customer_id as customer_id', 'customer.name as name_collaborator', 'customer.point as point',
                    'shop.id as shop_id', 'customer.total_revenue as total_revenue', 'customer.total_quantity as total_quantity')
                ->distinct('customer_id')
                ->join('order', 'shop.id', '=', 'order.shop_id')
                ->join('customer', 'order.customer_id', '=', 'customer.id')
                ->where('shop.customer_id', $dataSupplier->customer_id)
                ->where('customer.point', '>', 3000)
                ->get();
            foreach ($data as $k => $v) {
                $data[$k]->dropship = 0;
                $data[$k]->total_price = 0;
                $dataOrder = DB::table('order')
                    ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id', 'order.bonus as bonus', 'shop.id as shop_id')
                    ->join('shop', 'order.shop_id', '=', 'shop.id')
                    ->where('order.customer_id', $v->customer_id)
                    ->get();
                $data[$k]->quantity = 0;
                $dataCheck =  ManageCTVModel::where('shop_id', $v->shop_id)->where('collaborator_id', $v->customer_id)->first();
                $data[$k]->turn_on = $dataCheck->turn_on;
                foreach ($dataOrder as $kOrder => $vOrder) {
                    if($vOrder->collaborator_id == 1){
                        $data[$k]->dropship += 1;
                    }
                }
                if ($v->point >= 0 && $v->point <= 1000) {
                    $data[$k]->rank = 'Hạng đồng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if ($v->point >= 1000 && $v->point < 2000) {
                    $data[$k]->rank = 'Hạng bạc';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_silver.png';
                } else if ($v->point >= 2000 && $v->point < 3000) {
                    $data[$k]->rank = 'Hạng vàng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if($v->point > 3000) {
                    $data[$k]->rank = 'Hạng kim cương';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/diamond1.png';
                }
            }
        }
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $data_url_detail = URL::to('admin/manage_ctv/list_order/' . $row->customer_id);
                $data_url = url('admin/manage_ctv/turn_on');
                $data_ctv = $row->customer_id;
                $data_shop_id = $row->shop_id;
                $turn_on = $row->turn_on === 1 ? 'checked' : '';
                $output .= '
                           <tr class="text-center">
                                <td>' . $row->name_collaborator . '</td>
                                <td>
                                    <img width="28px" height="20px" class="image-preview" src="' . $row->img . '">
                                    <p>' . $row->rank . '</p>
                                </td>
                                <td>' . $row->dropship . '</td>
                                <td>' . $row->total_quantity . '</td>
                                <td>' . number_format($row->total_revenue) . ' đ ' . '</td>
                                <td>
                                    <div class="align-items-center justify-content-center">
                                        <label class="switch">
                                            <input class="turn_on" type="checkbox" '.$turn_on.'
                                            data-url="'.$data_url.'"
                                            data-ctv="'.$data_ctv.'"
                                            data-shop-id="'.$data_shop_id.'"
                                            >
                                            <span class="slider green round"></span>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            ';
            }
        } else {
            $output = '
                       <tr>
                        <td colspan="5">Không có dữ liệu</td>
                       </tr>
                      ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *
     *  Lọc theo doanh thu từ cao đến thấp/ từ thấp đến cao
     * @param $sort
     */
    public function filterRevenue($sort){
        $dataSupplier = session()->get('data_supplier');
        $output = '';
        if($sort == 0){
            $data = DB::table('shop')
                ->select('order.customer_id as customer_id', 'customer.name as name_collaborator', 'customer.point as point',
                    'shop.id as shop_id', 'customer.total_revenue as total_revenue', 'customer.total_quantity as total_quantity')
                ->distinct('customer_id')
                ->join('order', 'shop.id', '=', 'order.shop_id')
                ->join('customer', 'order.customer_id', '=', 'customer.id')
                ->where('shop.customer_id', $dataSupplier->customer_id)
                ->orderBy('customer.total_revenue', 'desc')
                ->get();
            foreach ($data as $k => $v) {
                $data[$k]->dropship = 0;
                $data[$k]->total_price = 0;
                $dataOrder = DB::table('order')
                    ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id', 'order.bonus as bonus', 'shop.id as shop_id')
                    ->join('shop', 'order.shop_id', '=', 'shop.id')
                    ->where('order.customer_id', $v->customer_id)
                    ->get();
                $data[$k]->quantity = 0;
                $dataCheck =  ManageCTVModel::where('shop_id', $v->shop_id)->where('collaborator_id', $v->customer_id)->first();
                $data[$k]->turn_on = $dataCheck->turn_on;
                foreach ($dataOrder as $kOrder => $vOrder) {
                    if($vOrder->collaborator_id == 1){
                        $data[$k]->dropship += 1;
                    }
                }
                if ($v->point >= 0 && $v->point <= 1000) {
                    $data[$k]->rank = 'Hạng đồng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if ($v->point >= 1000 && $v->point < 2000) {
                    $data[$k]->rank = 'Hạng bạc';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_silver.png';
                } else if ($v->point >= 2000 && $v->point < 3000) {
                    $data[$k]->rank = 'Hạng vàng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if($v->point > 3000) {
                    $data[$k]->rank = 'Hạng kim cương';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/diamond1.png';
                }
            }
        } else {
            $data = DB::table('shop')
                ->select('order.customer_id as customer_id', 'customer.name as name_collaborator', 'customer.point as point',
                    'shop.id as shop_id', 'customer.total_revenue as total_revenue', 'customer.total_quantity as total_quantity')
                ->distinct('customer_id')
                ->join('order', 'shop.id', '=', 'order.shop_id')
                ->join('customer', 'order.customer_id', '=', 'customer.id')
                ->where('shop.customer_id', $dataSupplier->customer_id)
                ->orderBy('customer.total_revenue', 'asc')
                ->get();
            foreach ($data as $k => $v) {
                $data[$k]->dropship = 0;
                $data[$k]->total_price = 0;
                $dataOrder = DB::table('order')
                    ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id', 'order.bonus as bonus', 'shop.id as shop_id')
                    ->join('shop', 'order.shop_id', '=', 'shop.id')
                    ->where('order.customer_id', $v->customer_id)
                    ->get();
                $data[$k]->quantity = 0;
                $dataCheck =  ManageCTVModel::where('shop_id', $v->shop_id)->where('collaborator_id', $v->customer_id)->first();
                $data[$k]->turn_on = $dataCheck->turn_on;
                foreach ($dataOrder as $kOrder => $vOrder) {
                    if($vOrder->collaborator_id == 1){
                        $data[$k]->dropship += 1;
                    }
                }
                if ($v->point >= 0 && $v->point <= 1000) {
                    $data[$k]->rank = 'Hạng đồng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if ($v->point >= 1000 && $v->point < 2000) {
                    $data[$k]->rank = 'Hạng bạc';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_silver.png';
                } else if ($v->point >= 2000 && $v->point < 3000) {
                    $data[$k]->rank = 'Hạng vàng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if($v->point > 3000) {
                    $data[$k]->rank = 'Hạng kim cương';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/diamond1.png';
                }
            }
        }
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $data_url_detail = URL::to('admin/manage_ctv/list_order/' . $row->customer_id);
                $data_url = url('admin/manage_ctv/turn_on');
                $data_ctv = $row->customer_id;
                $data_shop_id = $row->shop_id;
                $turn_on = $row->turn_on === 1 ? 'checked' : '';
                $output .= '
                           <tr class="text-center">
                                <td>' . $row->name_collaborator . '</td>
                                <td>
                                    <img width="28px" height="20px" class="image-preview" src="' . $row->img . '">
                                    <p>' . $row->rank . '</p>
                                </td>
                                <td>' . $row->dropship . '</td>
                                <td>' . $row->total_quantity . '</td>
                                <td>' . number_format($row->total_revenue) . ' đ ' . '</td>
                                <td>
                                    <div class="align-items-center justify-content-center">
                                        <label class="switch">
                                            <input class="turn_on" type="checkbox" '.$turn_on.'
                                            data-url="'.$data_url.'"
                                            data-ctv="'.$data_ctv.'"
                                            data-shop-id="'.$data_shop_id.'"
                                            >
                                            <span class="slider green round"></span>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            ';
            }
        } else {
            $output = '
                       <tr>
                        <td colspan="5">Không có dữ liệu</td>
                       </tr>
                      ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }

    /**
     *  Lọc theo Tất cả/ CTV mới nhất/ CTV bán tốt nhất
     * @param $type
     */
    public function filterType($type){
        $dataSupplier = session()->get('data_supplier');
        $output = '';
        if($type === 'all'){
            $data = DB::table('shop')
                ->select('order.customer_id as customer_id', 'customer.name as name_collaborator', 'customer.point as point',
                    'shop.id as shop_id', 'customer.total_revenue as total_revenue', 'customer.total_quantity as total_quantity')
                ->distinct('customer_id')
                ->join('order', 'shop.id', '=', 'order.shop_id')
                ->join('customer', 'order.customer_id', '=', 'customer.id')
                ->where('shop.customer_id', $dataSupplier->customer_id)
                ->orderBy('customer.total_revenue', 'desc')
                ->get();
            foreach ($data as $k => $v) {
                $data[$k]->dropship = 0;
                $data[$k]->total_price = 0;
                $dataOrder = DB::table('order')
                    ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id', 'order.bonus as bonus', 'shop.id as shop_id')
                    ->join('shop', 'order.shop_id', '=', 'shop.id')
                    ->where('order.customer_id', $v->customer_id)
                    ->get();
                $data[$k]->quantity = 0;
                $dataCheck =  ManageCTVModel::where('shop_id', $v->shop_id)->where('collaborator_id', $v->customer_id)->first();
                $data[$k]->turn_on = $dataCheck->turn_on;
                foreach ($dataOrder as $kOrder => $vOrder) {
                    if($vOrder->collaborator_id == 1){
                        $data[$k]->dropship += 1;
                    }
                }
                if ($v->point >= 0 && $v->point <= 1000) {
                    $data[$k]->rank = 'Hạng đồng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if ($v->point >= 1000 && $v->point < 2000) {
                    $data[$k]->rank = 'Hạng bạc';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_silver.png';
                } else if ($v->point >= 2000 && $v->point < 3000) {
                    $data[$k]->rank = 'Hạng vàng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if($v->point > 3000) {
                    $data[$k]->rank = 'Hạng kim cương';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/diamond1.png';
                }
            }
        }  elseif ($type === 'new') {
            $data = DB::table('shop')
                ->select('order.customer_id as customer_id', 'customer.name as name_collaborator', 'customer.point as point',
                    'shop.id as shop_id', 'customer.total_revenue as total_revenue', 'customer.total_quantity as total_quantity', 'customer.created_at as created_at')
                ->distinct('customer_id')
                ->join('order', 'shop.id', '=', 'order.shop_id')
                ->join('customer', 'order.customer_id', '=', 'customer.id')
                ->where('shop.customer_id', $dataSupplier->customer_id)
                ->orderBy('customer.created_at', 'desc')
                ->get();
            foreach ($data as $k => $v) {
                $data[$k]->dropship = 0;
                $data[$k]->total_price = 0;
                $dataOrder = DB::table('order')
                    ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id', 'order.bonus as bonus', 'shop.id as shop_id')
                    ->join('shop', 'order.shop_id', '=', 'shop.id')
                    ->where('order.customer_id', $v->customer_id)
                    ->get();
                $data[$k]->quantity = 0;
                $dataCheck =  ManageCTVModel::where('shop_id', $v->shop_id)->where('collaborator_id', $v->customer_id)->first();
                $data[$k]->turn_on = $dataCheck->turn_on;
                foreach ($dataOrder as $kOrder => $vOrder) {
                    if($vOrder->collaborator_id == 1){
                        $data[$k]->dropship += 1;
                    }
                }
                if ($v->point >= 0 && $v->point <= 1000) {
                    $data[$k]->rank = 'Hạng đồng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if ($v->point >= 1000 && $v->point < 2000) {
                    $data[$k]->rank = 'Hạng bạc';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_silver.png';
                } else if ($v->point >= 2000 && $v->point < 3000) {
                    $data[$k]->rank = 'Hạng vàng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if($v->point > 3000) {
                    $data[$k]->rank = 'Hạng kim cương';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/diamond1.png';
                }
            }
        } elseif ($type === 'best') {
            $data = DB::table('shop')
                ->select('order.customer_id as customer_id', 'customer.name as name_collaborator', 'customer.point as point',
                    'shop.id as shop_id', 'customer.total_revenue as total_revenue', 'customer.total_quantity as total_quantity')
                ->distinct('customer_id')
                ->join('order', 'shop.id', '=', 'order.shop_id')
                ->join('customer', 'order.customer_id', '=', 'customer.id')
                ->where('shop.customer_id', $dataSupplier->customer_id)
                ->orderBy('customer.total_quantity', 'desc')
                ->get();
            foreach ($data as $k => $v) {
                $data[$k]->dropship = 0;
                $data[$k]->total_price = 0;
                $dataOrder = DB::table('order')
                    ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id', 'order.bonus as bonus', 'shop.id as shop_id')
                    ->join('shop', 'order.shop_id', '=', 'shop.id')
                    ->where('order.customer_id', $v->customer_id)
                    ->get();
                $data[$k]->quantity = 0;
                $dataCheck =  ManageCTVModel::where('shop_id', $v->shop_id)->where('collaborator_id', $v->customer_id)->first();
                $data[$k]->turn_on = $dataCheck->turn_on;
                foreach ($dataOrder as $kOrder => $vOrder) {
                    if($vOrder->collaborator_id == 1){
                        $data[$k]->dropship += 1;
                    }
                }
                if ($v->point >= 0 && $v->point <= 1000) {
                    $data[$k]->rank = 'Hạng đồng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if ($v->point >= 1000 && $v->point < 2000) {
                    $data[$k]->rank = 'Hạng bạc';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_silver.png';
                } else if ($v->point >= 2000 && $v->point < 3000) {
                    $data[$k]->rank = 'Hạng vàng';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/rank_gold.png';
                } else if($v->point > 3000) {
                    $data[$k]->rank = 'Hạng kim cương';
                    $data[$k]->img = '../../assets/admin/app-assets/images/ncc/manage_ctv/diamond1.png';
                }
            }
        }
        $total_row = $data->count();

        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                $data_url_detail = URL::to('admin/manage_ctv/list_order/' . $row->customer_id);
                $data_url = url('admin/manage_ctv/turn_on');
                $data_ctv = $row->customer_id;
                $data_shop_id = $row->shop_id;
                $turn_on = $row->turn_on === 1 ? 'checked' : '';
                $output .= '
                           <tr class="text-center">
                                <td>' . $row->name_collaborator . '</td>
                                <td>
                                    <img width="28px" height="20px" class="image-preview" src="' . $row->img . '">
                                    <p>' . $row->rank . '</p>
                                </td>
                                <td>' . $row->dropship . '</td>
                                <td>' . $row->total_quantity . '</td>
                                <td>' . number_format($row->total_revenue) . ' đ ' . '</td>
                                <td>
                                    <div class="align-items-center justify-content-center">
                                        <label class="switch">
                                            <input class="turn_on" type="checkbox" '.$turn_on.'
                                            data-url="'.$data_url.'"
                                            data-ctv="'.$data_ctv.'"
                                            data-shop-id="'.$data_shop_id.'"
                                            >
                                            <span class="slider green round"></span>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            ';
            }
        } else {
            $output = '
                       <tr>
                        <td colspan="5">Không có dữ liệu</td>
                       </tr>
                      ';
        }
        $data = array(
            'table_data' => $output,
            'total_data' => $total_row
        );
        echo json_encode($data);
    }
}
