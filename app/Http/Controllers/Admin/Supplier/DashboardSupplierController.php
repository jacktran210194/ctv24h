<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use App\Http\Controllers\SupplierBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class DashboardSupplierController extends Controller
{
    public function index(){
        $data_return = [
            'title' => 'Kênh người bán | CTV24H'
        ];
        return view('admin.dashboard.dashboard_supplier', $data_return);
    }

    public function notification(){
        $dataReturn = [
          'title' => 'Thông báo cập nhật',
        ];
        return view('supplier.dashboard.notification',$dataReturn);
    }
}
