<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use App\Models\AddressSupplierModel;
use App\Models\AttributeValueModel;
use App\Models\BrandModel;
use App\Models\BuyMorePercent;
use App\Models\CategoryProductChildModel;
use App\Models\CategoryProductModel;
use App\Models\CategoryShopModel;
use App\Models\CityModel;
use App\Models\CustomerAddressModel;
use App\Models\DistrictModel;
use App\Models\ProductAttributeModel;
use App\Models\ProductImageModel;
use App\Models\ProductModel;
use App\Models\ProductShippingModel;
use App\Models\ShippingModel;
use App\Models\ShippingSupplierModel;
use App\Models\ShopModel;
use App\Models\SubCategoryProductChildModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProductSupplierController extends Controller
{
    public function index()
    {
        $data_login = session()->get('data_supplier');
        if (isset($data_login)) {
            $data_shop = ShopModel::where('supplier_id', $data_login['id'])->first();
        }
        $data = ProductModel::orderBy('id', 'desc')->where('shop_id', $data_shop['id'])->where('dropship', 0)->count();
        $data_return = [
            'status' => true,
            'title' => 'Quản lý sản phẩm của nhà cung cấp',
            'data' => $data
        ];

        return view('supplier.product.index', $data_return);
    }

    public function add()
    {
        $data_login = session()->get('data_supplier');
        if ($data_login) {
            $data_shop = ShopModel::where('supplier_id', $data_login['id'])->first();
            $data_address = CustomerAddressModel::where('customer_id', $data_login->customer_id)->where('type_pick_up', 1)->get();
            $data_category_shop = CategoryProductModel::all();
            $data_brand = BrandModel::orderBy('id', 'desc')->get();
            $data_category = CategoryProductChildModel::all();
            $category_child = SubCategoryProductChildModel::all();
            $data_shipping = ShippingSupplierModel::where('supplier_id', $data_login->id)->get();
            foreach ($data_shipping as $key => $value) {
                $data_ship = ShippingModel::find($value->shipping_id);
                $data_shipping[$key]['shipping_name'] = $data_ship['name'];
                $data_shipping[$key]['max_weight'] = $data_ship['max_weight'];
            }
            $data_return = [
                'status' => true,
                'title' => 'Thêm mới sản phẩm',
                'category' => isset($data_category) ? $data_category : '',
                'brand' => isset($data_brand) ? $data_brand : '',
                'category_shop' => isset($data_category_shop) ? $data_category_shop : '',
                'shipping' => $data_shipping ?? '',
                'category_child' => $category_child,
                'data_address' => $data_address ?? ''
            ];
            return view('supplier.product.form', $data_return);
        } else {
            return redirect("/login");
        }

    }

    public function save(Request $request)
    {
        $dataReturn = $this->create($request);
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function create($request)
    {
        $rules = [
            'name' => 'required',
            'warehouse' => 'required',
            'place' => 'required',
            'category_2' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin !',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->getMessageBag();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $data_login = session()->get('data_supplier');
            if (isset($data_login)) {
                $data_shop = ShopModel::where('supplier_id', $data_login['id'])->first();
            }

            $data_product = new ProductModel([
                'name' => $request->name,
                'slug' => Str::slug($request->name),
                'price' => 0,
                'price_discount' => 0,
                'price_ctv' => 0,
                'sku_type' => isset($request->sku_type) ? $request->sku_type : null,
                'warehouse' => $request->warehouse,
                'desc' => $request->is_desc,
                'brand' => $request->brand,
                'dropship' => 0,
                'is_status' => 0,
                'category_id' => $request->category_id,
                'weight' => $request->weight,
                'height' => $request->height,
                'length' => $request->length,
                'width' => $request->width,
                'original' => $request->original,
                'material' => $request->material,
                'sort_desc' => $request->sort_desc,
                'shop_id' => $data_shop->id,
                'category_shop' => $request->category_1 ?? null,
                'place' => '',
                'place_district' => '',
                'status_product' => $request->status_product,
                'image' => '',
                'active' => $request->active,
                'category_sub_child' => $request->category_2 ?? null,
                'video' => $request->video ?? null,
                'category_product' => $request->category_product,
            ]);
            $data_product->save();
            $data_address = CustomerAddressModel::find($request->place);
            $data_city = CityModel::find($data_address->city_id);
            $data_district = DistrictModel::find($data_address->district_id);
            $data_product->place = $data_city->name;
            $data_product->place_district = $data_district->name;
            $data_product->save();
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/product/';
            $file->move($path, $fileName);
            $data_product->image = '/uploads/images/product/' . $fileName;
            $data_product->save();
            $get_image = $request->file('img');
            if (is_array($get_image) || is_object($get_image)) {
                foreach ($get_image as $file1) {
                    $fileName1 = Str::random(20) . '.' . $file1->getClientOriginalExtension();
                    $path1 = public_path() . '/uploads/images/product/' . $data_product['id'] . '/';
                    $file1->move($path1, $fileName1);
                    $data_image = new ProductImageModel([
                        'product_id' => $data_product['id'],
                        'image' => '/uploads/images/product/' . $data_product['id'] . '/' . $fileName1
                    ]);
                    $data_image->save();
                }
            }
            $percent = json_decode($request->buy_more_percent);
            if (isset($percent)){
                $this->addBuyMorePercent($data_product->id, $percent);
            }
            $attr = json_decode($request->attr);
            $__price_max = 0;
            $___price_min = 0;
            $item = json_decode($request->shipping);
            $shipping = explode(',', $item);
            if (is_array($shipping) || is_object($shipping)) {
                foreach ($shipping as $v1) {
                    $data_shipping = new ProductShippingModel([
                        'product_id' => $data_product->id,
                        'shipping_supplier_id' => $v1,
                        'shop_id' => $data_product->shop_id,
                    ]);
                    $data_shipping->save();
                }
            }

            //attr
            foreach ($attr as $value) {
                $product_attribute = new ProductAttributeModel();
                $product_attribute['product_id'] = $data_product->id;
                $product_attribute['category_1'] = $value->title;
                $product_attribute['category_2'] = $value->name;
                $product_attribute['name'] = ' ';
                $product_attribute['image_variant'] = $value->image_variant ?? null;
                $product_attribute->save();
                $attr_value = $value->value;
                foreach ($attr_value as $item) {

                    if ($___price_min == 0){
                        $___price_min = $item->price;
                    }elseif ($___price_min > $item->price ){
                        $___price_min = $item->price;
                    }

                    if ($__price_max == 0){
                        $__price_max = $item->price;
                    }elseif ( $__price_max < $item->price ){
                        $__price_max = $item->price;
                    }

                    $attribute_value = new AttributeValueModel();
                    $attribute_value['attribute_id'] = $product_attribute->id;
                    $attribute_value['value'] = $value->title_value;
                    $attribute_value['price'] = $item->price;
                    $attribute_value['price_ctv'] = $item->price_ctv;
                    $attribute_value['quantity'] = $item->quantity;
                    $attribute_value['size_of_value'] = $item->name;
                    $attribute_value['product_id'] = $data_product->id;
                    $attribute_value->save();
                }
            }
            $this->getPriceMaxAndMin($data_product->id, $__price_max, $___price_min);
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/product_supplier'),
            ];
        }
        return $dataReturn;
    }

    //add buy more percent
    protected function addBuyMorePercent($product_id, $array)
    {
        foreach ($array as $value){
            if (!empty($value->quantity_1) && !empty($value->quantity_2) && !empty($value->percent)){
                $percent = new BuyMorePercent([
                    'product_id' => $product_id,
                    'quantity_1' => $value->quantity_1,
                    'quantity_2' => $value->quantity_2,
                    'percent' => $value->percent
                ]);
                $percent->save();
            }
        }
        return true;
    }

    protected function getPriceMaxAndMin($product_id, $price_max, $price_min)
    {
        $product = ProductModel::find($product_id);
        $product->price = $price_max;
        $product->price_discount = $price_min;
        $product->save();
    }

    public function edit($id)
    {
        $data_login = session()->get('data_supplier');
        $data = ProductModel::find($id);
        $data_category_shop = CategoryProductModel::all();
        $data_category_product = CategoryProductChildModel::orderBy('id', 'desc')->where('category_id', $data->category_product)->get();
        $data_product_child = SubCategoryProductChildModel::orderBy('id', 'desc')->where('category_product_child_id', $data->category_id)->get();
        $data_image = ProductImageModel::where('product_id', $id)->get();
        $data_brand = BrandModel::orderBy('id', 'desc')->get();
        $attr = ProductAttributeModel::where('product_id', $data->id)->get();
        $data_address = CustomerAddressModel::where('customer_id', $data_login->customer_id)->where('type_pick_up', 1)->get();
        foreach ($attr as $key => $value){
            $product_value = AttributeValueModel::where('attribute_id', $value->id)->get();
            $attr[$key]->is_delete = 0;
            $attr[$key]->save();
            $attr[$key]->value = $product_value;
        }
        $attr_value = AttributeValueModel::where('product_id', $id)->get();
        foreach ($attr_value as $key => $value){
            $attr_value[$key]->is_delete = 0;
            $attr_value[$key]->save();
        }
        $data_shipping = ShippingSupplierModel::where('supplier_id', $data_login->id)->get();
        $product_shipping = ProductShippingModel::where('product_id', $id)->get();
        foreach ($data_shipping as $key => $value) {
            $data_shipping[$key]['check'] = false;
            foreach ($product_shipping as $v){
                if ($v->shipping_supplier_id == $value->shipping_id){
                    $data_shipping[$key]['check'] = true;
                }
            }
            $data_ship = ShippingModel::find($value->shipping_id);
            $data_shipping[$key]['shipping_name'] = $data_ship['name'];
            $data_shipping[$key]['max_weight'] = $data_ship['max_weight'];
        }
        $buy_more_percent = BuyMorePercent::where('product_id', $id)->get();
        foreach ($buy_more_percent as $key => $value){
            $buy_more_percent[$key]->is_delete = 0;
            $buy_more_percent[$key]->save();
        }
        $data_return = [
            'status' => true,
            'title' => 'Cập nhật sản phẩm',
            'brand' => $data_brand,
            'data' => $data,
            'data_image' => $data_image,
            'attr' => $attr,
            'category_shop' => $data_category_shop,
            'shipping' => $data_shipping,
            'data_category_product' => $data_category_product,
            'data_product_child' => $data_product_child,
            'data_address' => $data_address,
            'buy_more_percent' => $buy_more_percent
        ];
        return view('supplier.product.edit', $data_return);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'warehouse' => 'required',
            'place' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data_login = session()->get('data_supplier');
        $dataShop = ShopModel::where('supplier_id', $data_login['id'])->first();
        $data_product = ProductModel::find($id);
        if ($data_product) {
            $data_product->name = $request->name;
            $data_product->slug = Str::slug($request->name);
            $data_product->price = $request->price;
            $data_product->sku_type = $request->sku_type;
            $data_product->category_id = $request->category_id;
            $data_product->brand = $request->brand;
            $data_product->desc = $request->is_desc;
            $data_product->warehouse = $request->warehouse;
            $data_product->original = $request->original;
            $data_product->material = $request->material;
            $data_product->weight = $request->weight;
            $data_product->length = $request->length;
            $data_product->width = $request->width;
            $data_product->height = $request->height;
            $data_product->sort_desc = $request->sort_desc;
            $data_product->shop_id = $dataShop->id;
            $data_product->place = $request->place;
            $data_product->status_product = $request->status_product;
            $data_product->category_product = $request->category_product;
            $data_product->category_sub_child = $request->category_2;
            $data_product->price = 0;
            if (isset($request->video)){
                $data_product->video = $request->video;
            }
            $data_product->save();

        $this->updateVariantProduct($request->get('attr'), $id);
        $this->updateImgProduct($request->get('group_image'), $id);
        $this->updateBuyMorePercent($request->get('buy_more_percent'), $id);
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/product_supplier'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function updateVariantProduct ($attr, $id)
    {
        $attr_product = json_decode($attr);
        $__price_max = 0;
        $__price_min = 0;
        ProductAttributeModel::where('product_id', $id)->where('is_delete', 1)->delete();
        AttributeValueModel::where('product_id', $id)->where('is_delete', 1)->delete();
        foreach ($attr_product as $value){
            if (!empty($value->id)){
                $product_attr = ProductAttributeModel::find($value->id);
                $product_attr->category_1 = $value->title;
                $product_attr->category_2 = $value->name;
                $product_attr->image_variant = $value->image_variant;
                $product_attr->save();
                foreach ($value->value as $key){
                    if (!empty($key->id)){
                        $attr_value = AttributeValueModel::find($key->id);
                        $attr_value->value = $value->title_value;
                        $attr_value->price = $key->price;
                        $attr_value->price_ctv = $key->price_ctv;
                        $attr_value->quantity = $key->quantity;
                        $attr_value->size_of_value = $key->name;
                        $attr_value->save();
                    }else{
                        $attr_value = new AttributeValueModel([
                            'value' => $value->title_value,
                            'price' => $key->price,
                            'price_ctv' => $key->price_ctv,
                            'quantity' => $key->quantity,
                            'size_of_value' => $key->name,
                            'attribute_id' => $value->id,
                            'product_id' => $id
                        ]);
                        $attr_value->save();
                    }
                }
            }else{
                $product_attr = new ProductAttributeModel([
                    'category_1' => $value->title,
                    'category_2' => $value->name,
                    'name' => ' ',
                    'product_id' => $id
                ]);
                $product_attr->save();
                foreach ($value->value as $key){
                    $attr_value = new AttributeValueModel([
                        'value' => $value->title_value,
                        'price' => $key->price,
                        'price_ctv' => $key->price_ctv,
                        'quantity' => $key->quantity,
                        'size_of_value' => $key->name,
                        'attribute_id' => $product_attr->id,
                        'product_id' => $id,
                        'is_sell' => 0
                    ]);
                    $attr_value->save();
                }
            }
            foreach ($value->value as $key){
                if ($__price_max == 0){
                    $__price_max = $key->price;
                }
                if ($__price_max < $key->price){
                    $__price_max = $key->price;
                }
                if ($__price_min == 0){
                    $__price_min = $key->price;
                }
                if ($__price_min > $key->price){
                    $__price_min = $key->price;
                }
            }
        }
        $this->getPriceMaxAndMin($id, $__price_max, $__price_min);
        return true;
    }

    protected function updateBuyMorePercent($data_percent, $product_id)
    {
        $data_percent = json_decode($data_percent);
        BuyMorePercent::where('product_id', $product_id)->where('is_delete', 1)->delete();
        foreach ($data_percent as $value){
            if (!empty($value->quantity_1) && !empty($value->quantity_2) && !empty($value->percent)){
                if (!empty($value->id)){
                    $percent = BuyMorePercent::find($value->id);
                    $percent->quantity_1 = $value->quantity_1;
                    $percent->quantity_2 = $value->quantity_2;
                    $percent->percent = $value->percent;
                    $percent->save();
                }else{
                    $percent = new BuyMorePercent([
                        'quantity_1' => $value->quantity_1,
                        'quantity_2' => $value->quantity_2,
                        'percent' => $value->percent,
                        'product_id' => $product_id,
                    ]);
                    $percent->save();
                }
            }
        }
        return true;
    }

    public function updateImgProduct($data_image, $id)
    {
        $group_img = json_decode($data_image);
        foreach ($group_img as $value){
            if (!empty($value->upload_image)){
                if (!empty($value->id)){
                    $product_img = ProductImageModel::find($value->id);
                    $product_img->image = $value->upload_image;
                    $product_img->save();
                }else{
                    $product_img = new ProductImageModel([
                        'product_id' => $id,
                        'image' => $value->upload_image
                    ]);
                    $product_img->save();
                }
            }else{
                if (isset($value->id)){
                    $product_img = ProductImageModel::find($value->id);
                    $product_img->delete();
                }
            }
        }
        return true;
    }

    protected function uploadProductDropship($id)
    {
        $product = ProductModel::where('product_id', $id)->get();
        if (isset($product)){
            $product_parent = ProductModel::find($id);
            foreach ($product as $key => $value){
                $product[$key]->active = 0;
                $product[$key]->name = $product_parent->name;
                $product[$key]->slug = $product_parent->slug;
                $product[$key]->sku_type = $product_parent->sku_type;
                $product[$key]->category_id = $product_parent->category_id;
                $product[$key]->brand = $product_parent->brand;
                $product[$key]->desc = $product_parent->desc;
                $product[$key]->warehouse = $product_parent->warehouse;
                $product[$key]->original = $product_parent->original;
                $product[$key]->material = $product_parent->material;
                $product[$key]->weight = $product_parent->weight;
                $product[$key]->length = $product_parent->length;
                $product[$key]->width = $product_parent->width;
                $product[$key]->height = $product_parent->height;
                $product[$key]->sort_desc = $product_parent->sort_desc;
                $product[$key]->shop_id = $product_parent->shop_id;
                $product[$key]->place = $product_parent->place;
                $product[$key]->status_product = $product_parent->status_product;
                $product[$key]->category_product = $product_parent->category_product;
                $product[$key]->category_sub_child = $product_parent->category_2;
                $product[$key]->video = $product_parent->video;
                $product[$key]->save();
            }
//            $variant = ProductAttributeModel::where('product_id', $id)->get();
//            $this->uploadVariantProductDropShip($variant);
        }
    }

    public function deleteVariantAndValueProduct( Request $request)
    {
        try{
            switch ($request->get('type')){
                case 1:
                    $product_attr = ProductAttributeModel::find($request->get('id'));
                    $product_attr->is_delete = 1;
                    $product_attr->save();
                    break;
                case 2:
                    $attribute_value = AttributeValueModel::find($request->get('id'));
                    $attribute_value->is_delete = 1;
                    $attribute_value->save();
                    break;
                case 3:
                    $percent = BuyMorePercent::find($request->get('id'));
                    $percent->is_delete = 1;
                    $percent->save();
            }
            return true;
        }catch (\Exception $exception){
            $dataReturn = [
                'error' => $exception,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->getMessageBag();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    public function delete($id)
    {
        try {
            ProductModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    public function hideAndActive(Request $request, $id)
    {
        $product = ProductModel::find($id);
        $product->active = $request->hide;
        $product->save();
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    public function delete_image($id)
    {
        try {
            ProductImageModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    public function formAttr(Request $request)
    {
        $html_per = view('supplier.product.form_attribute');
        $data_return = [
            'html' => strval($html_per)
        ];
        return response()->json($data_return, Response::HTTP_OK);
    }

    public function formItem()
    {
        $html_per = view('supplier.product.form_item');
        $data_return = [
            'html' => strval($html_per)
        ];
        return response()->json($data_return, Response::HTTP_OK);
    }

    public function delete_item($id)
    {
        try {
            AttributeValueModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete_attr($id)
    {
        try {
            ProductAttributeModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    //search
    public function search(Request $request)
    {
        $data_login = session()->get('data_supplier');
        if ($data_login) {
            $data_shop = ShopModel::where('supplier_id', $data_login['id'])->first();
        }
        if ($request->ajax()) {
            $output = '';
            $query = $request->get('query');
            if ($query != '') {
                $data = ProductModel::where('name', 'like', '%' . $query . '%')->where('shop_id', $data_shop->id)
                    ->orderBy('id', 'desc')
                    ->get();
                if (count($data)) {
                    foreach ($data as $key => $value) {
                        $data[$key]['dropship'] = ($value['dropship'] == 1) ? 'Hàng Dropship' : 'Hàng Tại Kho';
                        $data_category = CategoryProductChildModel::find($value['category_id']);
                        $data[$key]['category'] = $data_category['name'] ?? '';
                        if ($value['is_status'] == 0) {
                            $data[$key]['status'] = 'Sản phẩm còn hàng';
                        } elseif ($value['is_status'] == 1) {
                            $data[$key]['status'] = 'Sản phẩm hết hàng';
                        } elseif ($value['is_status'] == 2) {
                            $data[$key]['status'] = 'Sản phẩm bị đánh vi phạm';
                        } else {
                            $data[$key]['status'] = 'Sản phẩm bị ẩn';
                        }
                    }
                }
            } else {
                $data = ProductModel::orderBy('id', 'desc')->where('shop_id', $data_shop->id)->get();
                if (count($data)) {
                    foreach ($data as $key => $value) {
                        $data[$key]['dropship'] = ($value['dropship'] == 1) ? 'Hàng Dropship' : 'Hàng Tại Kho';
                        $data_category = CategoryProductChildModel::find($value['category_id']);
                        $data[$key]['category'] = $data_category['name'] ?? '';
                        if ($value['is_status'] == 0) {
                            $data[$key]['status'] = 'Sản phẩm còn hàng';
                        } elseif ($value['is_status'] == 1) {
                            $data[$key]['status'] = 'Sản phẩm hết hàng';
                        } elseif ($value['is_status'] == 2) {
                            $data[$key]['status'] = 'Sản phẩm bị đánh vi phạm';
                        } else {
                            $data[$key]['status'] = 'Sản phẩm bị ẩn';
                        }
                    }
                }
            }
            $total_row = $data->count();
            if ($total_row > 0) {
                foreach ($data as $k => $row) {
                    $data_url_edit = URL::to('admin/product_supplier/edit/' . $row->id);
                    $data_url_delete = URL::to('admin/product_supplier/delete/' . $row->id);
                    $output .= '
                                <tr>
                                    <td>' . $row->name . '</td>
                                    <td><img width="40px" height="40px" class="image-preview" src="' . $row->image . '"
                                             alt=""></td>
                                    <td>' . $row->category . '</td>
                                    <td>' . $row->dropship . '</td>
                                    <td>' . $row->warehouse . '</td>
                                    <td>' . $row->is_sell . '</td>
                                    <td>' . $row->status . '</td>
                                    <td>
                                         <a href="' . $data_url_edit . '">
                                            <button class="btn btn-warning" title="Sửa"><i class="fa fa-pencil-square-o"
                                                                                           aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <button
                                            class="btn btn-delete btn-danger" title="Xoá"
                                            data-url="' . $data_url_delete . '"
                                        >
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                                ';
                }
            } else {
                $output = '
                           <tr>
                            <td colspan="5">Không có dữ liệu</td>
                           </tr>
                          ';
            }
            $data = array(
                'table_data' => $output,
                'total_data' => $total_row
            );
            echo json_encode($data);
        }
    }

    public function filter($status)
    {
        $data_login = session()->get('data_supplier');
        if ($data_login) {
            $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
        }
        $output = '';
        switch ($status) {
            case '3';
                $data = ProductModel::orderBy('id', 'desc')->where('shop_id', $data_shop->id)->where('active', 0)->where('dropship', 0)->get();
                $count_data = count($data);
                break;
            case '0';
                $data = ProductModel::orderBy('id', 'desc')->where('shop_id', $data_shop->id)->where('active', 1)->where('dropship', 0)->get();
                $count_data = count($data);
                break;
            case '1';
                $data = DB::table('product')->select('product.*')
                    ->join('attribute_value', 'product.id', '=', 'attribute_value.product_id')->where('product.shop_id', $data_shop->id)
                    ->where('attribute_value.quantity', 0)->where('product.dropship', 0)->get();
                $count_data = count($data);
                break;
            case '2';
                $data = ProductModel::orderBy('id', 'desc')->where('shop_id', $data_shop->id)->where('is_status', 2)->where('dropship', 0)->get();
                $count_data = count($data);
                break;
            default:
                $data = ProductModel::orderBy('id', 'desc')->where('shop_id', $data_shop->id)->where('dropship', 0)->get();
                $count_data = count($data);
        }
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $k => $row) {
                if (!($row->sku_type)) {
                    $data[$k]->sku_type = '-';
                } else {
                    $data[$k]->sku_type = $row->sku_type;
                }
                if ($data[$k]->dropship == 1){
                    $data[$k]->dropship = 'Hàng Dropship';
                }else{
                    $data[$k]->dropship = 'Hàng Tại Kho';
                }
                $attr_product = DB::table('attribute_value')
                    ->join('product_attribute', 'product_attribute.id', '=', 'attribute_value.attribute_id')
                    ->where('attribute_value.product_id', $row->id)
                    ->select('attribute_value.*', 'product_attribute.*')->get();
                $product_category = '';
                $product_price = '';
                $product_quantity = '';
                $product_sold = '';
                if (!$attr_product) {
                    $product_category .= '-';
                    $product_price .= '<div class="text-red text-bold-700 text-center">' . number_format($row->price) . ' đ</div>';
                    $product_quantity .= '<div class="text-bold-700 text-center">' . $row->warehouse . '</div>';
                    $product_sold .= '<div class="text-bold-700">' . $row->is_sell . '</div>';
                }
                if ($row->active == 1) {
                    $content = 'Ẩn';
                    $active = 0;
                } else {
                    $content = 'Hiện';
                    $active = 1;
                }
                foreach ($attr_product as $item) {
                    if ($item->quantity > 0) {
                        $quantity = $item->quantity;
                    } else {
                        $quantity = '<button class="disabled" disabled="disabled">Hết hàng</button>';
                    }
                    $product_category .= '<div class="padding-10 color-gray font-size-14 text-left">' . $item->category_1 . ':' . $item->category_2 . '/' . $item->size_of_value . '</div>';
                    $product_price .= '<div class="padding-10 color-red font-weight-bold text-center">' . number_format($item->price) . 'đ</div>';
                    $product_quantity .= '<div class="padding-10 m-0 text-center font-weight-bold font-size-14" >' . $quantity . '</div>';
                    $product_sold .= '<div class="padding-10 m-0 text-center font-weight-bold font-size-14" >' . $item->is_sell . '</div>';
                }
                $data_url_edit = URL::to('admin/product_supplier/edit/' . $row->id);
                $data_url_delete = URL::to('admin/product_supplier/delete/' . $row->id);
                $data_url_hide = URL::to('admin/product_supplier/hide-or-active/' . $row->id);
                $output .= '
                                <tr class="border_box mg-top-30" data-count="' . $count_data . '">
                                    <td class="color-blue d-flex align-items-lg-start"><span class="padding-10 font-weight-bold">' . $row->dropship . '</span></td>
                                    <td class="position-relative" style="min-width: 200px"><div class="content-product position-absolute padding-10 d-flex align-items-lg-start font-weight-bold font-size-14">' . '<img width="60px" height="60px" class="border-radius-8" src="' . $row->image . '"
                                             alt="">' . $row->name . '</div></td>
                                    <td class="d-flex align-items-lg-start"><span class="padding-10 font-weight-bold color-black">' . $row->sku_type . '</span></td>
                                    <td>' . $product_category . '</td>
                                    <td>' . $product_price . '</td>
                                    <td>' . $product_quantity . '</td>
                                    <td>' . $product_sold . '</td>
                                    <td class="d-flex justify-content-lg-center align-items-lg-start">
                                    <div class="d-flex align-items-center position-relative">
                                         <a href="' . $data_url_edit . '">
                                            <button class="btn d-flex align-items-center" title="Sửa"><img src="../../assets/admin/app-assets/images/icons/edit_1.png">
                                            <span class="color-blue font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Sửa</span>
                                            </button>
                                        </a>
                                        <div class="show-options" style="cursor: pointer"><img src="../../assets/admin/app-assets/images/icons/Polygon_17.png"></div>
                                         <div class="options-change border_box border-radius-8 position-absolute">
                                         <button
                                            class="btn btn-delete d-block" title="Xoá"
                                            data-url="' . $data_url_delete . '"
                                        >
                                            <img src="../../assets/admin/app-assets/images/icons/delete.png">
                                            <span class="color-black font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Xóa</span>
                                        </button>
                                         <button
                                            class="btn btn-hide d-block" title="' . $content . '"
                                            data-url="' . $data_url_hide . '"
                                            data-active="' . $active . '"
                                        >
                                            <img src="../../assets/admin/app-assets/images/icons/openeyes.png">
                                            <span class="color-black font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">' . $content . '</span>
                                        </button>
                                        </div>
                                    </div>
                                    </td>
                                </tr>
                                ';
            }
        } else {
            $output .= '
                        <tr data-count="0">
                            <td colspan="5">Không có dữ liệu</td>
                        </tr>
            ';
        }
        echo $output;
    }

    public function getCategoryChildProduct(Request $request)
    {
        $category_child_product = SubCategoryProductChildModel::where('category_product_child_id', $request->category_id)->get();
        if (isset($category_child_product) && count($category_child_product)) {
            $data['status'] = true;
            $data['category'] = $category_child_product;
        } else {
            $data['status'] = false;
        }
        return $data;
    }

    public function getCategoryProduct(Request $request)
    {
        $category_child_product = CategoryProductChildModel::orderBy('id', 'desc')->where('category_id', $request->category_id)->get();
        if (isset($category_child_product) && count($category_child_product)) {
            $data['status'] = true;
            $data['category'] = $category_child_product;
        } else {
            $data['status'] = false;
        }
        return $data;
    }
}
