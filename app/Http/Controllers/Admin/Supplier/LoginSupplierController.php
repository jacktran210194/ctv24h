<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use App\Models\CollaboratorModel;
use App\Models\CustomerModel;
use App\Models\SupplierModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class LoginSupplierController extends Controller
{
    protected $title = 'Đăng nhập';

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        if ($request->session()->has('data_supplier')) {
            Redirect::to('admin')->send();
        }
        return view('supplier.login.login', ['title' => 'Đăng nhập hệ thống']);
    }

    public function login(Request $request)
    {
        $phone = $request->phone ?? '';
        $password = $request->password ?? '';
        if (empty($phone) || empty($password)) {
            $dataReturn = [
                'status' => false,
                'msg' => 'Vui lòng điền đầy đủ thông tin'
            ];
        } else {
            $dataUser = SupplierModel::where('phone', $phone)->where('password', md5($password))->first();
            if ($dataUser) {
//                session()->flush();
                $data_customer = CustomerModel::find($dataUser['customer_id']);
                $request->session()->put('data_customer',$data_customer);
                session()->forget('data_collaborator');
                $request->session()->put('data_supplier', $dataUser);
                $dataUser['supplier_admin'] = true;
                $dataReturn = [
                    'status' => true,
                    'msg' => 'Đăng nhập thành công',
                    'url' => URL::to('admin/supplier_admin')
                ];
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Đăng nhập không thành công'
                ];
            }
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('admin/supplier_admin/login');
    }
}
