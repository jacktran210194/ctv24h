<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Admin\CollaboratorController;
use App\Http\Controllers\Controller;
use App\Models\AddressSupplierModel;
use App\Models\BankAccountModel;
use App\Models\CityModel;
use App\Models\CollaboratorModel;
use App\Models\CustomerAddressModel;
use App\Models\CustomerModel;
use App\Models\DistrictModel;
use App\Models\SupplierModel;
use App\Models\WardModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProfileSupplierController extends Controller
{
    //Thông tin cá nhân
    public function index()
    {
        $admin = session()->get('data_supplier');
        $data = CustomerModel::find($admin->customer_id);
        $supplier = SupplierModel::where('customer_id', $data->id)->first();
        $data_year = isset($supplier) ? date('Y', strtotime($supplier['birthday'])) : date('Y-m-d');
        $data_month = isset($supplier) ? date('m', strtotime($supplier['birthday'])) : date('Y-m-d');
        $data_day = isset($supplier) ? date('d', strtotime($supplier['birthday'])) : date('Y-m-d');
        $data_return = [
            'title' => 'Quản lý thông tin cá nhân nhà cung cấp',
            'data' => $data,
            'day' => $data_day,
            'month' => $data_month,
            'year' => $data_year,
            'supplier' => $supplier,
        ];
        return view('supplier.profile.index', $data_return);
    }

    //Cập nhật thông tin cá nhân
    public function update(Request $request)
    {
        $admin = session()->get('data_supplier');
        $rules = [
            'name1' => 'required|',
            'email1' => 'required|email',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'unique' => 'Email đã tồn tại !',
            'min' => 'Mật khẩu tối thiểu 8 ký tự cả chữ và số !',
            'email1.email' => 'Không đúng định dạng email'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data = SupplierModel::find($admin['id']);
        $dataCus = CustomerModel::find($data->customer_id);
        $dataCTV = CollaboratorModel::where('customer_id', $data->customer_id)->first();
        if ($data) {
            $today = Carbon::now()->format('Y-m-d');
            $birthday = date('Y-m-d', strtotime($request->birthday));
            if($birthday > $today){
                $dataReturn = [
                  'status' => false,
                  'msg' => 'Ngày sinh không được lớn hơn ngày hiện tại !'
                ];
                return $dataReturn;
            }
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/supplier/';
                $file->move($path, $fileName);
                $data->image = '/uploads/images/supplier/' . $fileName;
                $data->save();
            }

            $data->name = $request->name1;
            $data->email = $request->email1;
            $data->birthday = $request->birthday;
            $data->save();

            $dataCus->name = $request->name1;
            $dataCus->email = $request->email1;
            $dataCus->birthday = $request->birthday;
            $dataCus->save();

            if (isset($dataCTV)){
                $dataCTV->name = $request->name1;
                $dataCTV->email = $request->email1;
                $dataCTV->birthday = $request->birthday;
                $dataCTV->save();
            }

            $dataReturn = [
                'status' => true,
                'msg' => 'Thay đổi thông tin thành công !',
                'url' => URL::to('admin/profile'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function address()
    {
        $data_login = session()->get('data_supplier');
        $data_city = CityModel::all();
        $data_address = CustomerAddressModel::where('customer_id', $data_login->customer_id)->orderBy('id', 'desc')->get();
        $dataReturn = [
            'status' => true,
            'title' => 'Địa chỉ',
            'city' => $data_city,
            'data' => $data_address
        ];
        return view('supplier.profile.address', $dataReturn);
    }


    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /** Danh sách quận/huyện
     * @param Request $request
     */
    public function district(Request $request)
    {
        $data = $request->all();
        if ($data['action']) {
            $output = '';
            if ($data['action'] == 'city') {
                $city = DistrictModel::where('city_id', $data['id'])->orderBy('name', 'desc')->get();
                $output .= '
                    <option class="input_config">Quận/huyện</option>
                ';
                foreach ($city as $key => $district) {
                    $output .= '<option class="input_config" value="' . $district->id . '" data-name="' . $district->name . '">' . $district->name . '</option>';
                }
            }
        }
        echo $output;
    }

    /**
     *  Danh sách phường/xã
     * @param Request $request
     */
    public function ward(Request $request)
    {
        $data = $request->all();
        if ($data['action']) {
            $output = '';
            if ($data['action'] == 'district') {
                $district = WardModel::where('district_id', $data['id'])->get();
                $output .= '
                    <option class="input_config">Phường/xã</option>
                ';
                foreach ($district as $key => $ward) {
                    $output .= '<option class="input_config" value="' . $ward->ward_id . '" data-name="' . $ward->name . '">' . $ward->name . '</option>';
                }
            }
        }
        echo $output;
    }


    //thêm && sửa địa chỉ
    public function addAddress(Request $request)
    {
        $rules = [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        } else {
            if ($request->id) {
                $data_address = CustomerAddressModel::find($request->id);
            } else {
                $data_address = new CustomerAddressModel();
            }
            if ($request->type_default == '1') {
                $data_address_old = CustomerAddressModel::where('customer_id', $request->customer_id)->where('default', 1)->first();
                if ($data_address_old) {
                    $data_address_old->default = 0;
                    $data_address_old->save();
                }
            }
            if ($request->type_pick_up == '1') {
                $data_address_old = CustomerAddressModel::where('customer_id', $request->customer_id)->where('type_pick_up', 1)->first();
                if ($data_address_old) {
                    $data_address_old->type_pick_up = 0;
                    $data_address_old->save();
                }
            }
            if ($request->type_return == '1') {
                $data_address_old = CustomerAddressModel::where('customer_id', $request->customer_id)->where('type_return', 1)->first();
                if ($data_address_old) {
                    $data_address_old->type_return = 0;
                    $data_address_old->save();
                }
            }
            $data_address->name = $request->name;
            $data_address->phone = $request->phone;
            $data_address->city_id = $request->city_id;
            $data_address->district_id = $request->district_id;
            $data_address->ward_id = $request->ward_id;
            $data_address->customer_id = $request->customer_id;
            $data_address->default = $request->type_default;
            $data_address->type_pick_up = $request->type_pick_up;
            $data_address->type_return = $request->type_return;
            $data_address->address = '';
            $data_address->address_maps = '';
            $data_address->longitude = '';
            $data_address->latitude = '';
            $data_address->save();
            $data_city = CityModel::find($data_address->city_id);
            $data_district = DistrictModel::find($data_address->district_id);
            $data_ward = WardModel::where('ward_id', $data_address->ward_id)->first();
            $data_address->address = $request->address . ', ' . $data_ward->name . ', ' . $data_district->name . ', ' . $data_city->name;
            $data_address->save();
            $dataReturn = [
                'status' => true,
                'msg' => 'Thêm địa chỉ thành công !',
                'url' => url('admin/profile/address')
            ];
        }
        return $dataReturn;
    }

    public function editAddress(Request $request)
    {
        $data_address = CustomerAddressModel::find($request->id);
        $dataReturn = [
            'status' => true,
            'data_address' => $data_address
        ];
        return $dataReturn;
    }

    //Xoá địa chỉ
    public function delete($id)
    {
        try {
            CustomerAddressModel::destroy($id);
            $dataReturn = [
                'msg' => 'Xoá dữ liệu thành công !',
                'status' => true
            ];
            return \response()->json($dataReturn, 200);
        } catch (\Exception $e) {
            $dataReturn = [
                'msg' => 'Đã có lỗi xảy ra !',
                'status' => false
            ];
            return \response()->json($dataReturn, 500);
        }
    }

    //thiết lập shop
    public function setting(Request $request)
    {
        switch ($request->type) {
            case 'setting_private':
                $type = 'setting_private';
                break;
            case 'setting_chat':
                $type = 'setting_chat';
                break;
            case 'setting_notification':
                $type = 'setting_notification';
                break;
            default:
                $type = 'setting_basic';
                break;
        }
        $dataReturn = [
            'title' => 'THIẾT LẬP SHOP',
            'type' => $type
        ];
        return view('supplier.profile.setting_shop', $dataReturn);
    }

    public function editProfile(Request $request)
    {
        $data_customer = session()->get('data_customer');
        $customer = CustomerModel::where('id', $data_customer->id)->first();
        $pass = md5($request->pass);
        if ($pass == $customer->password) {
            if (isset($request->new_pass)) {
                $customer->password = md5($request->new_pass);
                $customer->save();
            }
            $data_return = [
                'status' => true,
                'msg' => 'Thay đổi mật khẩu thành công'
            ];
        } else {
            $data_return = [
                'status' => false,
                'msg' => 'sai mật khẩu',
            ];
        }
        return $data_return;
    }

    public function getModalAddress(Request $request)
    {
        try {
            $id_address = $request->id_address ?? '';
            $city = CityModel::all();
            $data['city'] = $city;
            if (empty($id_address)) {
                $html = view('supplier.profile.modal_address', $data);
            } else {
                $dataAddress = CustomerAddressModel::find($id_address);
                $city_id = $dataAddress->city_id ?? '';
                $district_id = $dataAddress->district_id ?? '';
                $district = DistrictModel::where('city_id', $city_id)->get();
                $ward = WardModel::where('district_id', $district_id)->get();
                $data['district'] = $district;
                $data['ward'] = $ward;
                $data['data'] = $dataAddress;
                $html = view('supplier.profile.modal_address', $data);
            }
            $dataReturn['html'] = strval($html);
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    public function filterAddress(Request $request)
    {
        try {
            $type = $request->type ?? '';
            $id = $request->id ?? '';
            if ($type === 'city') {
                $filter = DistrictModel::where('city_id', $id)->get();
                $data['title'] = 'Quận/Huyện';
                $data['ward'] = false;
            } else {
                $filter = WardModel::where('district_id', $id)->get();
                $data['title'] = 'Phường/Xã';
                $data['ward'] = true;
            }
            $data['data'] = $filter;
            $html = view('supplier.profile.address_filter', $data);
            $dataReturn['html'] = strval($html);
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     *  Cập nhật địa chỉ
     * @param Request $request
     */
    public function addressUpdate(Request $request)
    {
        $dataLogin = session()->get('data_supplier');
        $data = CustomerAddressModel::find($request->id);
        $data->name = $request->name;
        $data->phone = $request->phone;;
        $data->city_id = $request->city_id;
        $data->district_id = $request->district_id;
        $data->ward_id = $request->ward_id;
        $data->default = $request->type_default;
        $data->type_pick_up = $request->type_pick_up;
        $data->type_return = $request->type_return;
        $data->customer_id = $dataLogin->customer_id;
        $data->save();
        $dataReturn = [
            'status' => true,
            'msg' => 'Sửa địa chỉ thành công'
        ];
        return $dataReturn;
    }

    /**
     *  Kiểm tra mật khẩu cũ
     * @param Request $request
     * @return array
     */
    public function confirmPass(Request $request){
        $dataLogin = session()->get('data_supplier');
        $dataCustomer = CustomerModel::find($dataLogin->customer_id);
        if (md5($request->pass) == $dataCustomer->password){
            $dataReturn = [
                'status' => true,
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Mật khẩu không đúng. Vui lòng nhập lại'
            ];
        }
        return $dataReturn;
    }

    /**
     *  Đổi SDT
     * @param Request $request
     * @return array
     */
    public function changePhone(Request $request){
        $dataLogin = session()->get('data_supplier');
        $rules = [
            'phone' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $data = CustomerModel::where('phone', $request->get('phone'))->first();
            if (isset($data)){
                $dataReturn['status'] = false;
                $dataReturn['msg'] = 'Số điện thoại đã tồn tại';
            }else{
                $dataReturn = [
                    'status' => true,
                ];
            }
        }
        return $dataReturn;
    }

    /**
     *  Lưu SDT
     * @param Request $request
     * @return array
     */
    public function savePhone(Request $request){
        $dataLogin = session()->get('data_supplier');
        $dataCustomer = CustomerModel::find($dataLogin->customer_id);
        $dataCustomer->phone = $request->phone;
        $dataCustomer->save();

        $dataSupplier = SupplierModel::where('customer_id', $dataLogin->customer_id)->first();
        $dataSupplier->phone = $request->phone;
        $dataSupplier->save();

        if (isset($dataCollaborator)){
            $dataCollaborator = CollaboratorModel::where('customer_id', $dataLogin->customer_id)->first();
            $dataCollaborator->phone = $request->phone;
            $dataCollaborator->save();
        }
        $dataReturn = [
            'status' => true,
            'msg' => 'Đổi số điện thoại thành công',
            'url' => url('admin/profile'),
        ];
        return $dataReturn;
    }

}
