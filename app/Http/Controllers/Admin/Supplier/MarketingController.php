<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use App\Models\ProductModel;
use App\Models\ProductPromotionModel;
use App\Models\ProductVoucherModel;
use App\Models\PromotionModel;
use App\Models\ShopModel;
use App\Models\VoucherCategoryModel;
use App\Models\VoucherModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MarketingController extends Controller
{
    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /**
     *  Index
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){
        $dataReturn = [
            'title' => 'Kênh Marketing'
        ];
        return view('supplier.marketing.index', $dataReturn);
    }

    /**
     *  Chương trình của CTV24
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function ctv24Program(){
        $dataReturn = [
            'title' => 'Kênh Marketing | Chương trình của CTV24'
        ];
        return view('supplier.marketing.ctv24program.index', $dataReturn);
    }

    /**
     *  Đăng ký ngay
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function register(Request $request){
        switch ($request->type_time){
            case 'register':
                $type_time = 'register';
                break;
            case 'waiting':
                $type_time = 'waiting';
                break;
            default:
                $type_time = 'all';
                break;
        }

        switch ($request->type){
            case 'condition':
                $type = 'condition';
                break;
            default :
                $type = 'detail';
                break;
        }
        $dataReturn = [
            'type_time' => $type_time,
            'type' => $type,
            'title' => 'Kênh Marketing | Chương trình của CTV24'
        ];
        return view('supplier.marketing.ctv24program.register', $dataReturn);
    }

    /**
     *  Đăng kí sản phẩm
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function registerProduct(Request $request){
        switch ($request->type_register){
            case 'waiting_confirm':
                $type_register = 'waiting_confirm';
                break;
            case 'confirm':
                $type_register = 'confirm';
                break;
            default:
                $type_register = 'registered';
                break;
        }

        switch ($request->type){
            case 'condition':
                $type = 'condition';
                break;
            default :
                $type = 'detail';
                break;
        }
        $dataReturn = [
            'type_register' => $type_register,
            'type' => $type,
            'title' => 'Kênh Marketing | Chương trình của CTV24'
        ];
        return view('supplier.marketing.ctv24program.register_product', $dataReturn);
    }

    /**
     *  Thêm sản phẩm
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addProduct(Request $request){
        switch ($request->type){
            case 'add_list_product':
                $type = 'add_list_product';
                break;
            default:
                $type = 'choose';
                break;
        }

        $dataReturn = [
            'type' => $type,
            'title' => 'Kênh Marketing | Chương trình của CTV24'
        ];
        return view('supplier.marketing.ctv24program.add_product', $dataReturn);
    }

    /**
     *  Mã giảm giá của CTV24
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function ctv24Voucher(){
        $dataReturn = [
            'title' => 'Kênh Marketing | Mã giảm giá của CTV24'
        ];
        return view('supplier.marketing.ctv24_voucher.index', $dataReturn);
    }

    /**
     *  Chi tiết mã giảm giá
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function detailVoucherCTV24(Request $request){
        switch ($request->type){
            case 'condition':
                $type = 'condition';
                break;
            default :
                $type = 'detail';
                break;
        }
        $dataReturn = [
            'type' => $type,
            'title' => 'Kênh Marketing | Mã giảm giá của CTV24'
        ];
        return view('supplier.marketing.ctv24_voucher.detail', $dataReturn);
    }

    /**
     *  Đăng kí mã giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function registerVoucher(Request $request){
        switch ($request->type){
            case 'register':
                $type = 'register';
                break;
            default :
                $type = 'create';
                break;
        }
        $dataReturn = [
            'type' => $type,
            'title' => 'Kênh Marketing | Mã giảm giá của CTV24'
        ];
        return view('supplier.marketing.ctv24_voucher.register', $dataReturn);
    }

    /**
     *  Đăng ký những mã có sẵn
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function createCodeAvailable(Request $request){
        switch ($request->type){
            case 'create':
                $type = 'create';
                break;
            default :
                $type = 'register';
                break;
        }
        $dataReturn = [
            'type' => $type,
            'title' => 'Kênh Marketing | Mã giảm giá của CTV24'
        ];
        return view('supplier.marketing.ctv24_voucher.create_code_available', $dataReturn);
    }

    public function createNewCode(){
        $dataLogin = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataLogin->customer_id)->first();
        $dataProduct = ProductModel::where('shop_id', $dataShop->id)
            ->where('active', 1)
            ->get();
        $dataReturn = [
            'dataProduct' => $dataProduct,
            'title' => 'Kênh Marketing | Mã giảm giá của CTV24'
        ];
        return view('supplier.marketing.ctv24_voucher.create_new_code', $dataReturn);
    }

    public function saveNewCode(Request $request){
        if ($request->id) {
            $data_return = $this->updateNewCode($request);
        } else {
            $data_return = $this->addNewCode($request);
        }
        return response()->json($data_return, Response::HTTP_OK);
    }

    public function addNewCode($request){
        $rules = [
            'shop_or_product' => 'required',
            'time_start' => 'required|after:today',
            'name_voucher' => 'regex:/(^([a-zA-Z0-9]+)(\d+)?$)/u|required',
            'time_end' => 'required',
            'discount_or_refund' => 'required',
            'type_discount' => 'required',
            'value_discount' => 'required',
            'min_order_value' => 'required',
            'number_save' => 'required',
            'number_available' => 'required',
            'display_setting' => 'required',
            'product_id' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'time_start.after' => 'Ngày bắt đầu không được nhỏ hơn ngày hiện tại',
            'regex' => 'Mã giảm giá không được nhập ký tự đặc biệt',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            if ($request->time_end < $request->time_start){
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Ngày kết thúc phải lớn hơn ngày bắt đầu'
                ];
                return $dataReturn;
            } else {
                $data_supplier = session()->get('data_supplier');
                $customer_id = $data_supplier->customer_id;
                $shop_or_product = $request->shop_or_product;
                $title = $request->name_voucher;
                $time_start = $request->time_start;
                $time_end = $request->time_end;
                $discount_or_refund = $request->discount_or_refund;
                $type_discount = $request->type_discount;
                $min_order_value = $request->min_order_value;
                $number_save = $request->number_save;
                $number_available = $request->number_available;
                $value_discount = $request->value_discount;
                $discount_limit = $request->discount_limit ? : '';
                $max_value = $request->max_value ? : '';
                $display_setting = $request->display_setting ? : '';
                $product_id = $request->product_id ? : '';
                $data = PromotionModel::create([
                    'customer_id' => $customer_id,
                    'apply_for' => $shop_or_product,
                    'title' => $title,
                    'time_start' => $time_start,
                    'time_end' => $time_end,
                    'type' => $discount_or_refund,
                    'money_or_percent' => $type_discount,
                    'value_order_min' => $min_order_value,
                    'number_save' => $number_save,
                    'number_use' => $number_available,
                    'value_discount' => $value_discount,
                    'discount_limit' => 0,
                    'max_value' => 0,
                    'display_setting' => 0,
                    'product_id' => $product_id,
                    'code' => '',
                    'frame' => '',
                    'shop_id' => 0,
                    'image' => '',
                ]);
                $data->save();
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/promotion/';
                $file->move($path, $fileName);
                $data->image = '/uploads/images/promotion/' . $fileName;
                $data->save();
            }
            $dataReturn = [
                'status' => true,
                'msg' => 'Thêm mã giảm giá thành công',
                'url' => url('admin/marketing_supplier/ctv24_voucher'),
            ];
        }
        return $dataReturn;
    }

    public function updateNewCode(){

    }

    /**
     *  Mã giảm giá của tôi
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexMyVoucher(Request $request){
        switch ($request->type){
            case 'being':
                $type = 'being';
                break;
            case 'coming_up':
                $type = 'coming_up';
                break;
            case 'finish':
                $type = 'finish';
                break;
            default:
                $type = 'all';
                break;
        }
        $dataReturn = [
            'type' => $type,
            'title' => 'Kênh Marketing | Mã giảm giá của tôi',
        ];
        return view('supplier.marketing.my_voucher.index', $dataReturn);
    }

    /**
     *  Tạo mã giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addMyVoucher(){
        $dataLogin = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataLogin->customer_id)->first();
        $dataProduct = ProductModel::where('shop_id', $dataShop->id)
            ->where('active', 1)
            ->orderBy('id', 'desc')->get();
        $dataReturn = [
            'title' => 'Kênh Marketing | Mã giảm giá của tôi',
            'dataProduct' => $dataProduct
        ];
        return view('supplier.marketing.my_voucher.form', $dataReturn);
    }

    /**
     *  Lưu mã giảm giá
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveMyVoucher(Request $request){
        if ($request->id) {
            $data_return = $this->updateMyVoucher($request);
        } else {
            $data_return = $this->createMyVoucher($request);
        }
        return response()->json($data_return, Response::HTTP_OK);
    }

    /**
     *  Tạo mã giảm giá
     * @param $request
     * @return array
     */
    public function createMyVoucher($request){
        $data_product = json_decode($request->product_id);
        $time_start = date('Y-m-d H:i:s',strtotime($request->time_start));
        $time_10 =  Carbon::parse($time_start)->addMinute(10)->format('Y-m-d H:i:s');
        $rules = [
            'code' => 'required|regex:/(^([a-zA-Z0-9]+)(\d+)?$)/u',
            'shop_or_product' => 'required',
            'time_start' => 'required|after:today',
            'title' => 'regex:/(^([a-zA-Z0-9]+)(\d+)?$)/u|required',
            'time_end' => 'required',
            'discount_or_refund' => 'required',
            'type_discount' => 'required',
            'value_discount' => 'required',
            'min_order_value' => 'required',
            'number_save' => 'required',
            'number_available' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
            'time_start.after' => 'Ngày bắt đầu không được nhỏ hơn ngày hiện tại',
            'title.regex' => 'Tên chương trình giảm giá không chứa ký tự đặc biệt',
            'code.regex' => 'Mã giảm giá không chưa ký tự đặc biệt'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            if ($request->time_end < $request->time_start){
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Ngày kết thúc phải lớn hơn ngày bắt đầu'
                ];
                return $dataReturn;
            } else {
                $dataLogin = session()->get('data_supplier');
                $dataShop = ShopModel::find($dataLogin->customer_id);

                $dataSaveVoucherProduct = new VoucherModel();
                $dataSaveVoucherProduct->code = $request->code;
                $dataSaveVoucherProduct->customer_id = $dataLogin->customer_id;
                $dataSaveVoucherProduct->shop_id = $dataShop['id'];
                $dataSaveVoucherProduct->title = $request->title;
                $dataSaveVoucherProduct->time_start = $time_10;
                $dataSaveVoucherProduct->time_end = $request->time_end;
                $dataSaveVoucherProduct->discount_or_refund = $request->discount_or_refund;
                $dataSaveVoucherProduct->type_discount = $request->type_discount;
                $dataSaveVoucherProduct->value_discount = $request->value_discount;
                $dataSaveVoucherProduct->min_order_value = $request->min_order_value;
                $dataSaveVoucherProduct->number_save = $request->number_save;
                $dataSaveVoucherProduct->number_available = $request->number_available;
                $dataSaveVoucherProduct->discount_value_max = $request->discount_value_max;
                $dataSaveVoucherProduct->shop_or_product = $request->shop_or_product;
                if($request->discount_or_refund == 1){
                    $dataSaveVoucherProduct->type_discount = 1;
                    $dataSaveVoucherProduct->discount_value_max = $request->discount_value_max_refund;
                }
                $dataSaveVoucherProduct->save();
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/voucher/';
                $file->move($path, $fileName);
                $dataSaveVoucherProduct->image = '/uploads/images/voucher/' . $fileName;
                $dataSaveVoucherProduct->save();

                if($request->shop_or_product == 1){
                    if(is_array($data_product) || is_object($data_product)) {
                        foreach ($data_product as $k => $v) {
                            $dataProduct = new ProductVoucherModel([
                                'voucher_id' => $dataSaveVoucherProduct->id,
                                'product_id' => $v
                            ]);
                            $dataProduct->save();
                        }
                    }
                }
            }
            $dataReturn = [
                'status' => true,
                'msg' => 'Thêm mã giảm giá thành công',
                'url' => url('admin/marketing_supplier/my_voucher'),
            ];
        }
        return $dataReturn;
    }

    public function updateMyVoucher($request){

    }

    /**
     *  Tìm kiếm sản phẩm
     */
    public function searchProduct(Request $request){
        $dataLogin = session()->get('data_supplier');
        $dataShop = ShopModel::where('customer_id', $dataLogin->customer_id)->first();
        $key = $request->key ?? '';
        $product = ProductModel::where('shop_id', $dataShop->id)
            ->where('active', 1)
            ->where('name', 'like', '%' .$key. '%')
            ->orderBy('id', 'desc')
            ->get();
        $data = null;
        if(!empty($request->id)){
            $data = VoucherModel::find($request->id);
        }
        $data_r = [
            'product' => $product,
            'data' => $data,
            'product_active' => $request->product
        ];
        $view = view('supplier.marketing.my_voucher.product', $data_r);
        $dataReturn = [
            'status' => true,
            'html' => strval($view)
        ];
        return $dataReturn;
    }

    /**
     * Chi tiết Mã giảm giá của tôi
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function detailMyVoucher(Request $request, $id){
        $data = VoucherModel::find($request->id);
        $dataProductVC = ProductVoucherModel::where('voucher_id', $data->id)->get();
        $count = $dataProductVC->count();
        if($count == 0){
            $data->check = 0;
        } else {
            $data->check = 1;
        }
        if($dataProductVC){
            foreach ($dataProductVC as $k => $v){
                $dataProduct = ProductModel::find($v->product_id);
                $dataProductVC[$k]->name = $dataProduct->name ?? '';
            }
        }

        $dataReturn = [
            'title' => 'Kênh Marketing | Mã giảm giá của tôi',
            'voucher' => true,
            'data' => $data,
            'dataProductVC' => $dataProductVC
        ];
        return view('supplier.marketing.my_voucher.form', $dataReturn);
    }

    /**
     *  Mã giảm giá | Đơn hàng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function orderMyVoucher(){
        $dataReturn = [
            'title' => 'Kênh Marketing | Mã giảm giá của tôi'
        ];
        return view('supplier.marketing.my_voucher.order', $dataReturn);
    }

    /**
     *  Mã giảm giá của tôi | Tất cả
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getAllMyVoucher(){
        $dataLogin = session()->get('data_supplier');
        $data = VoucherModel::orderBy('id', 'desc')->where('customer_id', $dataLogin->customer_id)->get();
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        if($data){
            foreach ($data as $k => $v){
                if($v->shop_or_product == 0){
                    $data[$k]->shop_or_product = "Voucher toàn shop";
                } else {
                    $data[$k]->shop_or_product = "Voucher sản phẩm";
                }
                if($v->time_start <= $today && $v->time_end > $today){
                    $data[$k]->status_voucher = 'Đang diễn ra';
                    $data[$k]->color = 'bg-blue';
                } else if($v->time_start > $today && $v->time_end > $today) {
                    $data[$k]->status_voucher = 'Sắp diễn ra';
                    $data[$k]->color = 'bg-green';
                } else if($v->time_start < $today && $v->time_end < $today) {
                    $data[$k]->status_voucher = 'Đã kết thúc';
                    $data[$k]->color = 'bg-red';
                }
                if($v->type_discount == 0){
                    $data[$k]->type_discount = 'đ';
                } else {
                    $data[$k]->type_discount = '%';
                }
                if($v->time_start <= $today && $v->time_end > $today){
                    $data[$k]->check_end = 1;
                }
            }
        }
        $dataReturn = [
            'status' => true,
            'data' => $data
        ];
        return view('supplier.marketing.my_voucher.all', $dataReturn);
    }

    /**
     *  Mã giảm giá của tôi | Đang diễn ra
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getBeingMyVoucher(){
        $dataLogin = session()->get('data_supplier');
        $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d H:i:s');
        $data = VoucherModel::where('time_start', '<=', $today)
            ->where('time_end', '>', $today)
            ->where('customer_id', $dataLogin->customer_id)
            ->orderBy('id', 'desc')
            ->get();
        if($data){
            foreach ($data as $k => $v){
                if($v->shop_or_product == 0){
                    $data[$k]->shop_or_product = "Voucher toàn shop";
                } else {
                    $data[$k]->shop_or_product = "Voucher sản phẩm";
                }
                if($v->type_discount == 0){
                    $data[$k]->type_discount = 'đ';
                } else {
                    $data[$k]->type_discount = '%';
                }
                if($v->time_start <= $today && $v->time_end > $today){
                    $data[$k]->check_end = 1;
                }
            }
        }
        $dataReturn = [
            'status' => true,
            'data' => $data
        ];
        return view('supplier.marketing.my_voucher.being', $dataReturn);
    }

    /**
     *  Mã giảm giá của tôi | Sắp diễn ra
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getComingUpMyVoucher(){
        $dataLogin = session()->get('data_supplier');
        $today = Carbon::now('Asia/Ho_Chi_Minh');
        $data = VoucherModel::where('time_start', '>', $today)
            ->where('time_end', '>', $today)
            ->where('customer_id', $dataLogin->customer_id)
            ->orderBy('id', 'desc')
            ->get();
        if($data){
            foreach ($data as $k => $v){
                if($v->shop_or_product == 0){
                    $data[$k]->shop_or_product = "Voucher toàn shop";
                } else {
                    $data[$k]->shop_or_product = "Voucher sản phẩm";
                }
                if($v->type_discount == 0){
                    $data[$k]->type_discount = 'đ';
                } else {
                    $data[$k]->type_discount = '%';
                }
            }
        }
        $dataReturn = [
            'status' => true,
            'data' => $data
        ];
        return view('supplier.marketing.my_voucher.coming_up', $dataReturn);
    }

    /**
     *  Mã giảm giá của tôi | Đã kết thúc
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getFinishMyVoucher(){
        $dataLogin = session()->get('data_supplier');
        $today = Carbon::now('Asia/Ho_Chi_Minh');
        $data = VoucherModel::where('time_start', '<', $today)
            ->where('time_end', '<', $today)
            ->where('customer_id', $dataLogin->customer_id)
            ->orderBy('id', 'desc')
            ->get();
        if($data){
            foreach ($data as $k => $v){
                if($v->shop_or_product == 0){
                    $data[$k]->shop_or_product = "Voucher toàn shop";
                } else {
                    $data[$k]->shop_or_product = "Voucher sản phẩm";
                }
                if($v->type_discount == 0){
                    $data[$k]->type_discount = 'đ';
                } else {
                    $data[$k]->type_discount = '%';
                }
            }
        }
        $dataReturn = [
            'status' => true,
            'data' => $data
        ];
        return view('supplier.marketing.my_voucher.finish', $dataReturn);
    }

    /**
     *  Kết thúc mã giảm giá sớm
     * @param Request $request
     * @return array
     */
    public function finishMyVoucher(Request $request){
        $today = Carbon::now('Asia/Ho_Chi_Minh');
        VoucherModel::where('id', $request->id)->update(['time_end' => $today]);
        $dataReturn = [
            'title' => 'Mã giảm giá của tôi',
            'msg' => 'Đã kết thúc mã giảm giá'
        ];
        return $dataReturn;
    }

    /**
     *  Ưu đãi follower
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexOfferFollower(Request $request){
        switch ($request->type){
            case 'being':
                $type = 'being';
                break;
            case 'coming_up':
                $type = 'coming_up';
                break;
            case 'finish':
                $type = 'finish';
                break;
            default:
                $type = 'all';
                break;
        }
        $dataReturn = [
            'type' => $type,
            'title' => 'Kênh Marketing | Ưu đãi follower'
        ];
        return view('supplier.marketing.offer_follower.index', $dataReturn);
    }

    /**
     *  Tạo ưu đãi follower
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addOfferFollower(){
        $dataReturn = [
            'title' => 'Kênh Marketing | Mã giảm giá của tôi'
        ];
        return view('supplier.marketing.offer_follower.form', $dataReturn);
    }

    /**
     * Chi tiết ưu đãi follower
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function detailOfferFollower(){
        $dataReturn = [
            'title' => 'Kênh Marketing | Mã giảm giá của tôi'
        ];
        return view('supplier.marketing.offer_follower.form', $dataReturn);
    }

    /**
     *  Combo khuyến mại
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexPromotionCombo(Request $request){
        switch ($request->type){
            case 'being':
                $type = 'being';
                break;
            case 'coming_up':
                $type = 'coming_up';
                break;
            case 'finish':
                $type = 'finish';
                break;
            default:
                $type = 'all';
                break;
        }
        $dataReturn = [
            'type' => $type,
            'title' => 'Kênh Marketing | Combo khuyến mại'
        ];
        return view('supplier.marketing.promotion_combo.index', $dataReturn);
    }

    /**
     *  Tạo combo khuyến mại
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addPromotionCombo(){
        $dataReturn = [
            'title' => 'Kênh Marketing | Combo khuyến mại'
        ];
        return view('supplier.marketing.promotion_combo.form', $dataReturn);
    }

    /**
     * Combo khuyến mại | Đơn hàng và dữ liệu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function orderAndData(Request $request){
        switch ($request->type){
            case 'detail' :
                $type = 'detail';
                break;
            default:
                $type = 'all';
                break;
        }
        $dataReturn = [
            'type' => $type,
            'title' => 'Kênh Marketing | Combo khuyến mại'
        ];
        return view('supplier.marketing.promotion_combo.order_and_data', $dataReturn);
    }
    /**
     *  Mua kèm Deal sốc
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexDealShock(Request $request){
        switch ($request->type){
            case 'being':
                $type = 'being';
                break;
            case 'coming_up':
                $type = 'coming_up';
                break;
            case 'finish':
                $type = 'finish';
                break;
            default:
                $type = 'all';
                break;
        }
        $dataReturn = [
            'type' => $type,
            'title' => 'Kênh Marketing | Mua kèm Deal sốc'
        ];
        return view('supplier.marketing.deal_shock.index', $dataReturn);
    }

    /**
     *  Tạo mua kèm Deal sốc
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function createDealShock(){
        $dataReturn = [
            'title' => 'Kênh Marketing | Mua kèm Deal sốc'
        ];
        return view('supplier.marketing.deal_shock.form', $dataReturn);
    }

    /**
     *  Mua kèm Deal sốc | Thêm sản phẩm
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addProductDealShock(Request $request){
        switch ($request->type){
            case 'list' :
                $type = 'list';
                break;
            default:
                $type = 'choose';
                break;
        }
        $dataReturn = [
            'type' => $type,
            'title' => 'Kênh Marketing | Mua kèm Deal sốc'
        ];
        return view('supplier.marketing.deal_shock.add_product', $dataReturn);
    }

    /**
     *  Top sản phẩm bán chạy
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexTopBestSeller(){
        $dataReturn = [
            'title' => 'Kênh Marketing | Top sản phẩm bán chạy'
        ];
        return view('supplier.marketing.top_best_seller.index', $dataReturn);
    }

    /**
     *  Top sản phẩm bán chạy | Thêm danh mục
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function createTopBestSeller(){
        $dataReturn = [
            'title' => 'Kênh Marketing | Top sản phẩm bán chạy'
        ];
        return view('supplier.marketing.top_best_seller.form', $dataReturn);
    }

    /**
     *  Top sản phẩm bán chạy | Thêm sản phẩm
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addProductTopBestSeller(){
        $dataReturn = [
            'title' => 'Kênh Marketing | Top sản phẩm bán chạy'
        ];
        return view('supplier.marketing.top_best_seller.add_product', $dataReturn);
    }

    /**
     *  Hàng đồng giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexSamePrice(){
        $dataReturn = [
            'title' => 'Kênh Marketing | Hàng đồng giá'
        ];
        return view('supplier.marketing.same_price.index', $dataReturn);
    }

    /** Hàng đồng giá | Đăng ký
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function registerSamePrice(){
        $dataReturn = [
            'title' => 'Kênh Marketing | Hàng đồng giá'
        ];
        return view('supplier.marketing.same_price.register', $dataReturn);
    }

    /**
     *  Hàng đồng giá | Thêm sản phẩm
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addProductSamePrice(){
        $dataReturn = [
            'title' => 'Kênh Marketing | Hàng đồng giá'
        ];
        return view('supplier.marketing.same_price.add_product', $dataReturn);
    }
}
