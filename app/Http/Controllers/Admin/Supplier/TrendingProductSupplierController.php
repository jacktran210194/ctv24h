<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use App\Models\CategoryProductChildModel;
use App\Models\CategoryProductModel;
use App\Models\ProductModel;
use App\Models\ShopModel;
use App\Models\SponsoredProductModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class TrendingProductSupplierController extends Controller
{
    public function index()
    {
        $data_login = session()->get('data_supplier');
        if (isset($data_login)) {
            $data_shop = ShopModel::where('supplier_id', $data_login['id'])->first();
        }
        $data = ProductModel::where('shop_id', $data_shop->id)
            ->where('active', 1)
            ->where('is_trending', 1)
            ->orderBy('id', 'desc')
            ->get();
        if ($data) {
            foreach ($data as $k => $v) {
                $dataCategory = CategoryProductChildModel::find($v['category_id']);
                $data[$k]['category_name'] = $dataCategory['name'] ?? '';
                if ($v['is_trending'] == 1) {
                    $data[$k]['is_trending'] = 'Đang chờ duyệt';
                } elseif ($v['is_trending'] == 0) {
                    $data[$k]['is_trending'] = ' No Trending';
                } else {
                    $data[$k]['is_trending'] = 'Trending';
                }
            }
        }
        $dataReturn = [
            'data' => $data,
            'title' => 'Sản phẩm trend'
        ];
        return view('supplier.trending_product.index', $dataReturn);
    }

    public function add()
    {
        $data_login = session()->get('data_supplier');
        if (isset($data_login)) {
            $data_shop = ShopModel::where('supplier_id', $data_login['id'])->first();
        }
        $dataProduct = ProductModel::where('shop_id', $data_shop->id)
            ->where('active', 1)
            ->where('is_trending', 0)->orderBy('id', 'desc')->get();
        foreach ($dataProduct as $k => $v) {
            $data_category = CategoryProductChildModel::find($v['category_id']);
            $dataProduct[$k]['category_name'] = $data_category['name'];
        }

        $data_return = [
            'status' => true,
            'title' => 'Thêm sản phẩm trend',
            'data' => $dataProduct
        ];
        return view('supplier.trending_product.form', $data_return);
    }

    /**
     * @param $request
     * @param $rules
     * @param $customMessages
     * @return array
     */
    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /**
     *  Thêm sản phẩm trend
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function active($id)
    {
        $data_product = ProductModel::find($id);
        $data_product->update(['is_trending' => 1]);
        $dataReturn = [
            'status' => true,
            'msg' => 'Thêm sản phẩm trend thành công',
            'data' => $data_product,
            'url' => URL::to('admin/product_supplier/trending')
        ];
        return response()->json($dataReturn, Response::HTTP_OK);
    }
}
