<?php

namespace App\Http\Controllers\Admin\Supplier;

use App\Http\Controllers\Controller;
use App\Models\CategoryShopModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductCategoryShopModel;
use App\Models\ProductModel;
use App\Models\ReviewProductModel;
use App\Models\ShopImageModel;
use App\Models\ShopModel;
use App\Models\ShopRegisterModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;

class ShopSupplierController extends Controller
{
    public function index()
    {
        $data_login = session()->get('data_supplier');
        if (isset($data_login)) {
            $data_shop = ShopModel::where('supplier_id', $data_login['id'])->first();
            $count_review_1 = ReviewProductModel::where('shop_id', $data_shop->id)->avg('ratings') ?? 0;
            $count_review = round($count_review_1, 1) ?? 0;
            $data_shop['count_review'] = $count_review ?? 0;
            $count_category = CategoryShopModel::where('shop_id', $data_shop->id)->count();
            $data_shop['count_category'] = $count_category ?? 0;
        }
        $data_product = ProductModel::where('shop_id', $data_shop->id)->count();
        $data_image = ShopImageModel::where('shop_id', $data_shop->id)->get();
        $data_return = [
            'status' => true,
            'title' => 'Hồ sơ Shop',
            'data' => $data_shop,
            'data_image' => isset($data_image) ? $data_image : '',
            'count_product' => isset($data_product) ? $data_product : 0,
        ];
        return view('supplier.shop.form', $data_return);
    }

    public function save(Request $request)
    {
        if ($request->id) {
            $dataReturn = $this->update($request);
        } else {
            $dataReturn = $this->create($request);
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function edit(Request $request)
    {
        $data_shop = ShopModel::find($request->id);
        $data_image = ShopImageModel::where('shop_id', $request->id)->get();
        $data_category_shop = CategoryShopModel::orderBy('id', 'desc')->get();
        $data_return = [
            'title' => 'Cập nhật cửa hàng',
            'status' => true,
            'data' => $data_shop,
            'category_shop' => $data_category_shop,
            'data_image' => $data_image,
        ];
        return view('supplier.shop.form', $data_return);
    }

    public function update($request)
    {
        $rules = [
            'name' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data_shop = ShopModel::find($request->id);

        if ($data_shop) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_shop->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/shop/';
                $file->move($path, $fileName);
                $data_shop->image = '/uploads/images/shop/' . $fileName;
                $data_shop->save();
            }
            $data_shop->name = $request->name;
            $data_shop->video = $request->video;
            $data_shop->desc = $request->desc;
            $data_shop->supplier_id = $request->supplier_id;
            $data_shop->save();
            $get_image = $request->file('img');
            if (is_array($get_image) || is_object($get_image)) {
                foreach ($request->file('img') as $file1) {
                    $fileName1 = Str::random(20) . '.' . $file1->getClientOriginalExtension();
                    $path1 = public_path() . '/uploads/images/shop/' . $data_shop->id . '/';
                    $file1->move($path1, $fileName1);
                    $data_image = ShopImageModel::create([
                        'shop_id' => $data_shop->id,
                        'image' => '/uploads/images/shop/' . $data_shop->id . '/' . $fileName1
                    ]);
                    $data_image->save();
                }
            }
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/shop_supplier'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    public function delete_image($id)
    {
        try {
            ShopImageModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }


    //Đánh giá shop
    public function review()
    {
        $data_login = session()->get('data_supplier');
        $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
        $dataReturn = [
            'title' => 'Đánh giá Shop',
            'shop' => $data_shop
        ];
        return view('supplier.shop.review', $dataReturn);
    }

    //get data ajax

    //all star
    public function all_star()
    {
        $data_login = session()->get('data_supplier');
        $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
        $data_review = ReviewProductModel::where('shop_id', $data_shop->id)->orderBy('id', 'desc')->get();
        foreach ($data_review as $k => $v) {
            $data_product = ProductModel::find($v['product_id']);
            $data_review[$k]['product_name'] = $data_product['name'];
            $data_review[$k]['product_image'] = $data_product['image'];
            $data_order_detail = OrderDetailModel::find($v['order_detail_id']);
            $data_order = OrderModel::find($data_order_detail['order_id']);
            $data_review[$k]['order_code'] = $data_order['order_code'];
        }
        $dataReturn = [
            'title' => 'Đánh giá Shop',
            'data' => $data_review,
        ];
        return view('supplier.shop.all_star', $dataReturn);
    }

    //1 star
    public function one_star()
    {
        $data_login = session()->get('data_supplier');
        $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
        $data_review = ReviewProductModel::where('shop_id', $data_shop->id)->where('ratings', 1)->orderBy('id', 'desc')->get();
        foreach ($data_review as $k => $v) {
            $data_product = ProductModel::find($v['product_id']);
            $data_review[$k]['product_name'] = $data_product['name'];
            $data_review[$k]['product_image'] = $data_product['image'];
            $data_order_detail = OrderDetailModel::find($v['order_detail_id']);
            $data_order = OrderModel::find($data_order_detail['order_id']);
            $data_review[$k]['order_code'] = $data_order['order_code'];
        }
        $dataReturn = [
            'title' => 'Đánh giá Shop',
            'data' => $data_review,
        ];
        return view('supplier.shop.1_star', $dataReturn);
    }

    //2 star
    public function two_star()
    {
        $data_login = session()->get('data_supplier');
        $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
        $data_review = ReviewProductModel::where('shop_id', $data_shop->id)->where('ratings', 2)->orderBy('id', 'desc')->get();
        foreach ($data_review as $k => $v) {
            $data_product = ProductModel::find($v->product_id);
            $data_review[$k]['product_name'] = $data_product['name'];
            $data_review[$k]['product_image'] = $data_product['image'];
            $data_order_detail = OrderDetailModel::find($v['order_detail_id']);
            $data_order = OrderModel::find($data_order_detail['order_id']);
            $data_review[$k]['order_code'] = $data_order['order_code'];
        }
        $dataReturn = [
            'title' => 'Đánh giá Shop',
            'data' => $data_review,
        ];
        return view('supplier.shop.2_star', $dataReturn);
    }

    //3 star
    public function three_star()
    {
        $data_login = session()->get('data_supplier');
        $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
        $data_review = ReviewProductModel::where('shop_id', $data_shop->id)->where('ratings', 3)->orderBy('id', 'desc')->get();
        foreach ($data_review as $k => $v) {
            $data_product = ProductModel::find($v['product_id']);
            $data_review[$k]['product_name'] = $data_product['name'];
            $data_review[$k]['product_image'] = $data_product['image'];
            $data_order_detail = OrderDetailModel::find($v['order_detail_id']);
            $data_order = OrderModel::find($data_order_detail['order_id']);
            $data_review[$k]['order_code'] = $data_order['order_code'];
        }
        $dataReturn = [
            'title' => 'Đánh giá Shop',
            'data' => $data_review,
        ];
        return view('supplier.shop.3_star', $dataReturn);
    }

    //4 sao
    public function four_star()
    {
        $data_login = session()->get('data_supplier');
        $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
        $data_review = ReviewProductModel::where('shop_id', $data_shop->id)->where('ratings', 4)->orderBy('id', 'desc')->get();
        foreach ($data_review as $k => $v) {
            $data_product = ProductModel::find($v['product_id']);
            $data_review[$k]['product_name'] = $data_product['name'];
            $data_review[$k]['product_image'] = $data_product['image'];
            $data_order_detail = OrderDetailModel::find($v['order_detail_id']);
            $data_order = OrderModel::find($data_order_detail['order_id']);
            $data_review[$k]['order_code'] = $data_order['order_code'];
        }
        $dataReturn = [
            'title' => 'Đánh giá Shop',
            'data' => $data_review,
        ];
        return view('supplier.shop.4_star', $dataReturn);
    }

    //5 sao
    public function five_star()
    {
        $data_login = session()->get('data_supplier');
        $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
        $data_review = ReviewProductModel::where('shop_id', $data_shop->id)->where('ratings', 5)->orderBy('id', 'desc')->get();
        foreach ($data_review as $k => $v) {
            $data_product = ProductModel::find($v['product_id']);
            $data_review[$k]['product_name'] = $data_product['name'];
            $data_review[$k]['product_image'] = $data_product['image'];
            $data_order_detail = OrderDetailModel::find($v['order_detail_id']);
            $data_order = OrderModel::find($data_order_detail['order_id']);
            $data_review[$k]['order_code'] = $data_order['order_code'];
        }
        $dataReturn = [
            'title' => 'Đánh giá Shop',
            'data' => $data_review,
        ];
        return view('supplier.shop.5_star', $dataReturn);
    }

    //Trả lời đánh giá
    public function reply(Request $request)
    {
        $rules = [
            'reply' => 'required'
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin !',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $today = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d');
            $data_review = ReviewProductModel::find($request->id);
            $data_review->reply = $request->reply;
            $data_review->date_reply = $today;
            $data_review->status_reply = 1;
            $data_review->save();
            $dataReturn = [
                'status' => true,
            ];
        }
        return $dataReturn;
    }


    /**
     *
     * Category Shop
     *
     */

    public function indexCategory()
    {
        $data_login = session()->get('data_supplier');
        if (isset($data_login)) {
            $data_shop = ShopModel::where('supplier_id', $data_login['id'])->first();
        }
        $data_category_shop = CategoryShopModel::orderBy('id', 'desc')->where('shop_id', $data_shop->id)->get();
        foreach ($data_category_shop as $key => $value) {
            if ($value->created_by == 0) {
                $data_category_shop[$key]['user_created'] = 'Người bán';
            } else {
                $data_category_shop[$key]['user_created'] = 'Hệ thống';
            }
            $data_product_category = DB::table('product_category_shop')
                ->join('category_shop', 'category_shop.id', '=', 'product_category_shop.category_id')
                ->where('category_id', '=', $value->id)
                ->count();
            $data_category_shop[$key]['count_product'] = $data_product_category;
        }

        $data_return = [
            'status' => true,
            'title' => 'Quản lý danh mục cửa hàng',
            'data' => $data_category_shop
        ];
        return view('supplier.category_shop.index', $data_return);
    }

    public function displayCategory(Request $request)
    {
        $data = CategoryShopModel::find($request->id);
        if ($request->check == 1) {
            $data->status = 1;
            $data->save();
            $dataReturn = [
                'status' => true,
                'msg' => 'Bật danh mục thành công !',
                'url' => url('admin/shop_supplier/category')
            ];
        } else {
            $data->status = 0;
            $data->save();
            $dataReturn = [
                'status' => true,
                'msg' => 'Tắt danh mục thành công !',
                'url' => url('admin/shop_supplier/category')
            ];
        }
        return $dataReturn;
    }


    public function addCategory()
    {
        $data_return = [
            'status' => true,
            'title' => 'Thêm mới danh mục cửa hàng'
        ];

        return view('supplier.category_shop.form', $data_return);
    }

    public function saveCategory(Request $request)
    {
        if ($request->id) {
            $dataReturn = $this->updateCategory($request);
        } else {
            $dataReturn = $this->createCategory($request);
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function createCategory($request)
    {
        $rules = [
            'name' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $data_login = session()->get('data_supplier');
            if (isset($data_login)) {
                $data_shop = ShopModel::where('supplier_id', $data_login['id'])->first();
            }
            $data_category = CategoryShopModel::create([
                'name' => $request->name,
                'shop_id' => $data_shop->id,
                'created_by' => 0,
            ]);
            $data_category->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/shop_supplier/category'),
            ];
        }
        return $dataReturn;
    }

    public function editCategory(Request $request)
    {
        $data_category = CategoryShopModel::find($request->id);
        $data_return = [
            'title' => 'Cập nhật danh mục cửa hàng',
            'data' => $data_category
        ];
        return view('supplier.category_shop.form', $data_return);
    }

    public function updateCategory($request)
    {
        $rules = [
            'name' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data_login = session()->get('data_supplier');
        $data_category = CategoryShopModel::find($request->id);
        if ($data_category) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_category->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/category_shop/';
                $file->move($path, $fileName);
                $data_category->image = '/uploads/images/category_shop/' . $fileName;
                $data_category->save();
            }
            if (isset($data_login)) {
                $data_shop = ShopModel::where('supplier_id', $data_login['id'])->first();
            }
            $data_category->name = $request->name;
            $data_category->shop_id = $data_shop->id;
            $data_category->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/shop_supplier/category'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function deleteCategory($id)
    {
        try {
            CategoryShopModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    public function detail($id)
    {
        $data_category = CategoryShopModel::find($id);
        $data_product_category_shop = ProductCategoryShopModel::where('shop_id', $data_category->shop_id)->where('category_id', $data_category->id)->orderBy('id', 'desc')->get();
        $count_product = count($data_product_category_shop);
        if ($data_product_category_shop) {
            foreach ($data_product_category_shop as $key => $value) {
                $data_product = ProductModel::find($value->product_id);
                $data_product_category_shop[$key]['product_name'] = $data_product->name;
                $data_product_category_shop[$key]['product_image'] = $data_product->image;
                $data_product_category_shop[$key]['product_price'] = $data_product->price;
                $data_product_category_shop[$key]['product_warehouse'] = $data_product->warehouse;
            }
        }
        $data_product_shop = ProductModel::where('shop_id', $data_category->shop_id)->orderBy('id', 'desc')->get();
        $dataReturn = [
            'title' => 'Chi tiết danh mục ' . $data_category->name,
            'data' => $data_category,
            'data_product_category_shop' => $data_product_category_shop,
            'data_product' => $data_product_shop ?? null,
            'count_product' => $count_product
        ];
        return view('supplier.category_shop.detail', $dataReturn);
    }


    //Cập nhật thông tin để bán hàng
    public function update_info()
    {
        $data_login = session()->get('data_supplier');
        $data_shop = ShopModel::where('supplier_id', $data_login->id)->first();
        $dataReturn = [
            'status' => true,
            'title' => 'Cập nhật Shop',
            'data_shop' => $data_shop,
            'data_login' => $data_login
        ];
        return view('supplier.shop.update_shop', $dataReturn);
    }

    public function save_info(Request $request)
    {
        $rules = [
            'name_company' => 'required',
            'business' => 'required',
            'card_id' => 'required',
            'card_date' => 'required|before:today',
            'card_address' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'before' => 'Ngày cấp không được lớn hơn ngày hiện tại !'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data_shop = ShopModel::find($request->shop_id);
        $path = public_path() . '/uploads/images/shop_info_required/';
        if ($data_shop) {
            if ($request->hasFile('image_card_1')) {
                $file1 = $request->file('image_card_1');
                if ($request->file('image_card_1') == null) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                } else {
                    File::delete($data_shop->image_card_1);
                    $fileName1 = Str::random(20) . '.' . $file1->getClientOriginalExtension();
                    $file1->move($path, $fileName1);
                    $data_shop->image_card_1 = '/uploads/images/shop_info_required/' . $fileName1;
                    $data_shop->save();
                }
            }
            if ($request->hasFile('image_card_2')) {
                $file2 = $request->file('image_card_2');
                if (!$file2->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_shop->image_card_2);
                $fileName2 = Str::random(20) . '.' . $file2->getClientOriginalExtension();
                $file2->move($path, $fileName2);
                $data_shop->image_card_2 = '/uploads/images/shop_info_required/' . $fileName2;
                $data_shop->save();
            }
            if ($request->hasFile('business_license')) {
                $file3 = $request->file('business_license');
                if (!$file3->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_shop->business_license);
                $fileName3 = Str::random(20) . '.' . $file3->getClientOriginalExtension();
                $file3->move($path, $fileName3);
                $data_shop->business_license = '/uploads/images/shop_info_required/' . $fileName3;
                $data_shop->save();
            }
            if ($request->hasFile('related_document_1')) {
                $file4 = $request->file('related_document_1');
                if (!$file4->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_shop->related_document_1);
                $fileName4 = Str::random(20) . '.' . $file4->getClientOriginalExtension();
                $file4->move($path, $fileName4);
                $data_shop->related_document_1 = '/uploads/images/shop_info_required/' . $fileName4;
                $data_shop->save();
            }
            if ($request->hasFile('related_document_2')) {
                $file5 = $request->file('related_document_2');
                if (!$file5->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_shop->related_document_2);
                $fileName5 = Str::random(20) . '.' . $file5->getClientOriginalExtension();
                $file5->move($path, $fileName5);
                $data_shop->related_document_2 = '/uploads/images/shop_info_required/' . $fileName5;
                $data_shop->save();
            }
            $card_date = date('Y-m-d', strtotime($request->card_date));
            $data_shop->name_company = $request->name_company;
            $data_shop->business = $request->business;
            $data_shop->card_id = $request->card_id;
            $data_shop->card_date_create = $card_date;
            $data_shop->card_address = $request->card_address;
            $data_shop->active = 0;
            $data_shop->save();
            $dataReturn = [
                'status' => true,
            ];
            return $dataReturn;
        }
    }

    //Thêm sản phẩm vào danh mục shop
    public function add_product(Request $request)
    {
        $item = json_decode($request->product_id);
        $product = explode(',', $item);
        if (is_array($product) || is_object($product)) {
            foreach ($product as $v1) {
                $data_product_old = ProductCategoryShopModel::where('product_id', $v1)->where('category_id', $request->category_id)->first();
                if (!$data_product_old) {
                    $data_product = ProductModel::find($v1);
                    $data_product_category_shop = new ProductCategoryShopModel([
                        'product_id' => $v1,
                        'shop_id' => $request->shop_id,
                        'category_id' => $request->category_id,
                        'product_name' => $data_product->name
                    ]);
                    $data_product_category_shop->save();
                } else {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'Sản phẩm đã tồn tại !'
                    ];
                    return $dataReturn;
                }
            }
        }
        $dataReturn = [
            'status' => true,
            'msg' => 'Thêm thành công'
        ];
        return $dataReturn;
    }

    public function delete_product($id)
    {
        try {
            ProductCategoryShopModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    //search product
    public function search_product_category(Request $request)
    {
        $data_login = session()->get('data_supplier');
        if ($data_login) {
            $data_shop = ShopModel::where('supplier_id', $data_login['id'])->first();
        }
        if ($request->ajax()) {
            $output = '';
            $query = $request->get('query');
            if ($query != '') {
                $data = ProductCategoryShopModel::where('product_name', 'like', '%' . $query . '%')->where('shop_id', $data_shop->id)
                    ->orderBy('id', 'desc')
                    ->get();
                if (count($data)) {
                    foreach ($data as $key => $value) {
                        $data_product = ProductModel::find($value->product_id);
                        $data[$key]['product_image'] = $data_product['image'];
                        $data[$key]['product_price'] = $data_product['price'];
                        $data[$key]['product_warehouse'] = $data_product['warehouse'];
                    }
                }
            } else {
                $data = ProductCategoryShopModel::orderBy('id', 'desc')->where('shop_id', $data_shop->id)->get();
                if (count($data)) {
                    foreach ($data as $key => $value) {
                        $data_product = ProductModel::find($value->product_id);
                        $data[$key]['product_image'] = $data_product['image'];
                        $data[$key]['product_price'] = $data_product['price'];
                        $data[$key]['product_warehouse'] = $data_product['warehouse'];
                    }
                }
            }
            $total_row = $data->count();
            if ($total_row > 0) {
                foreach ($data as $k => $row) {
                    $data_url_delete = URL::to('admin/shop_supplier/category/delete_product/' . $row->id);
                    $output .= '
                                <tr class="text-bold-700">
                                    <td><input type="checkbox">
                                    </td>
                                    <td>
                                    <div class="d-flex align-items-center">
                                        <img width="71px" height="71px" class="image-preview mr-2 border_radius" src="' . $row->product_image . '"
                                             alt="">
                                             <p style="width: 200px!important;" class="m-0">' . $row->product_name . '</p>
                                    </div>
                                    </td>
                                    <td class="text-red">' . number_format($row->product_price) . 'đ</td>
                                    <td>' . $row->product_warehouse . '</td>
                                    <td>
                                         <a data-url="' . $data_url_delete . '" class="text-blue text-underline btn-delete">Xoá</a>
                                    </td>
                                </tr>
                                ';
                }
            } else {
                $output = '
                           <tr>
                            <td colspan="5">Không có dữ liệu</td>
                           </tr>
                          ';
            }
            $data = array(
                'table_data' => $output,
            );
            echo json_encode($data);
        }
    }

    public function filterProduct (Request $request)
    {
        try{
            $user = session()->get('data_customer');
            $shop = ShopModel::where('customer_id', $user->id)->first();
            $data_product = ProductModel::where('name', 'Like', '%'.$request->get('key_word').'%')
                ->where('shop_id', $shop->id)->get();
            $data['data_product'] = $data_product;
            $view = view('supplier.category_shop.data_product',$data)->render();
            return \response()->json(['prod'=> $view]);
        }catch (\Exception $exception){
            return $exception;
        }
    }
}
