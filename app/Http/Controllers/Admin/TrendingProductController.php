<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BankAccountModel;
use App\Models\CategoryProductChildModel;
use App\Models\CategoryProductModel;
use App\Models\ProductModel;
use App\Models\PromotionModel;
use App\Models\TrendingProductModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;

class TrendingProductController extends Controller
{
    /**
     *  Danh sách sản phẩm trend
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $data = DB::table('trending_product')
            ->select('product.*', 'category_product_child.name as category_product', 'shop.name as name_supplier', 'product.is_trending as is_trending')
            ->join('product', 'trending_product.product_id', '=', 'product.id')
            ->join('category_product_child', 'category_product_child.id', '=', 'product.category_id')
            ->join('shop', 'product.shop_id', 'shop.id')
            ->orderBy('trending_product.id', 'desc')
            ->get();
        $dataReturn = [
            'data' => $data,
            'title' => 'Sản phẩm trend'
        ];
        return view('admin.trending_product.index', $dataReturn);
    }

    /**
     *  Thêm sản phẩm trend
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $dataProduct = ProductModel::where('is_trending', 1)->orderBy('id', 'desc')->get();
        if ($dataProduct) {
            foreach ($dataProduct as $k => $v) {
                $dataCategory = CategoryProductChildModel::find($v['category_id']);
                $dataProduct[$k]['category_product'] = $dataCategory['name'] ?? '';
            }
        }
        $data_return = [
            'status' => true,
            'title' => 'Thêm sản phẩm trend',
            'data' => $dataProduct
        ];
        return view('admin.trending_product.form', $data_return);
    }

    /**
     * @param $request
     * @param $rules
     * @param $customMessages
     * @return array
     */
    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /**
     *  Thêm sản phẩm trend
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function active(Request $request)
    {
        $data = ProductModel::where('id', $request->id)->update(['is_trending' => 2]);
        $dataTrending = new TrendingProductModel();
        $dataTrending->product_id = $request->id;
        $dataTrending->save();
        $dataReturn = [
            'status' => true,
            'msg' => 'Thêm sản phẩm trend thành công',
            'data' => $data,
            'dataTrending' => $dataTrending,
            'url' => URL::to('admin/product/trending')
        ];
        return response()->json($dataReturn, Response::HTTP_OK);
    }
}
