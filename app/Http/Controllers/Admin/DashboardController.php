<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class DashboardController extends AdminBaseController
{
    public function index()
    {
        $dataReturn = [
            'title' => 'Quản trị | CTV24H'
        ];
        return view('admin.dashboard.dashboard',$dataReturn);
    }
}
