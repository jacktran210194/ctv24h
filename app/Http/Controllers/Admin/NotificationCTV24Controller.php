<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\NotificationCTV24Model;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class NotificationCTV24Controller extends Controller
{
    /**
     *  Dánh sách thông báo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){
        $data = NotificationCTV24Model::orderBy('created_at', 'desc')->get();
        $dataReturn = [
            'title' => 'Thông báo sàn CTV24',
            'data' => $data
        ];
        return view('admin.notification.index', $dataReturn);
    }

    /**
     *  Tạo thông báo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $data_return = [
            'title' => 'Tạo thông báo',
            'notification' => true
        ];
        return view('admin.notification.form', $data_return);
    }

    /**
     *  Lưu
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        if ($request->id) {
            $data_return = $this->update($request);
        } else {
            $data_return = $this->create($request);
        }
        return response()->json($data_return, Response::HTTP_OK);
    }

    /**
     * @param $request
     * @param $rules
     * @param $customMessages
     * @return array
     */
    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    /**
     *  Tạp thông báo
     * @param $request
     * @return array
     *
     */
    public function create($request){
        $rules = [
            'title' => 'required',
            'message' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $today = Carbon::now('Asia/Ho_Chi_Minh');
            $title = $request->title;
            $message = $request->message;
            $data = NotificationCTV24Model::create([
                'title' => $title,
                'message' => $message,
                'created_at' => date('d-m-Y H:i:s', strtotime($today)),
                'image' => ''
            ]);
            $data->save();
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/notification_ctv24/';
            $file->move($path, $fileName);
            $data->image = '/uploads/images/notification_ctv24/' . $fileName;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/notification'),
            ];
        }
        return $dataReturn;
    }

    /**
     * Cập nhật thông báo
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Request $request){
        $data = NotificationCTV24Model::find($request->id);
        $data_return = [
            'title' => 'Cập nhật thông báo',
            'notification' => true,
            'data' => $data,
        ];
        return view('admin.notification.form', $data_return);
    }

    /**
     *  Cập nhật thông báo
     * @param $request
     * @return array
     */
    public function update($request){
        $rules = [
            'title' => 'required',
            'message' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin',
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data = NotificationCTV24Model::find($request->id);

        if ($data) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/notification_ctv24/';
                $file->move($path, $fileName);
                $data->image = '/uploads/images/notification_ctv24/' . $fileName;
                $data->save();
            }
            $data->title = $request->title;
            $data->message = $request->message;
            $data->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/notification'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    /**
     *  Xóa thông báo
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            $data = NotificationCTV24Model::find($id);
            if ($data) {
                File::delete($data->image);
            }
            NotificationCTV24Model::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     *  Chi tiết thông báo
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function detail($id){
        $data = NotificationCTV24Model::find($id);
        $dataReturn = [
            'title' => 'Chi tiết thông báo',
            'data' => $data
        ];
        return view('admin.notification.detail', $dataReturn);
    }

}
