<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CardModel;
use App\Models\CategoryCardModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CardController extends Controller
{
    public function indexCategory()
    {
        $data = CategoryCardModel::orderBy('id', 'desc')->get();
        foreach ($data as $k => $v) {
            if ($v['active'] == 0) {
                $data[$k]['active'] = 'Bảo trì';
                $data[$k]['btn'] = 'danger';
            } else {
                $data[$k]['active'] = 'Hoạt động';
                $data[$k]['btn'] = 'success';
            }
            if ($v['type'] == 0) {
                $data[$k]['type'] = 'Thẻ điện thoại';
            } else {
                $data[$k]['type'] = 'Thẻ game';
            }
        }
        $dataReturn = [
            'title' => 'Quản lý nhà mạng',
            'data' => $data
        ];
        return view('admin.category_card.index', $dataReturn);
    }

    public function addCategory()
    {
        $data_return = [
            'title' => 'Thêm nhà mạng',
        ];
        return view('admin.category_card.form', $data_return);
    }

    public function saveCategory(Request $request)
    {
        if ($request->id) {
            $dataReturn = $this->updateCategory($request);
        } else {
            $dataReturn = $this->createCategory($request);
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function createCategory($request)
    {
        $rules = [
            'name' => 'required|unique:category_card,name',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'unique' => 'Tên nhà mạng đã tồn tại !'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $data_category = CategoryCardModel::create([
                'name' => $request->name,
                'type' => $request->type,
                'image' => ''
            ]);
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/category_card/';
            $file->move($path, $fileName);
            $data_category->image = '/uploads/images/category_card/' . $fileName;
            $data_category->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/card/category'),
            ];
        }
        return $dataReturn;
    }

    public function editCategory(Request $request)
    {
        $data_category = CategoryCardModel::find($request->id);
        $data_return = [
            'title' => 'Cập nhật nhà mạng',
            'data' => $data_category
        ];
        return view('admin.category_card.form', $data_return);
    }

    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    public function updateCategory($request)
    {
        $rules = [
            'name' => 'required|unique:brand,name,' . $request->id,
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'unique' => 'Tên nhà mạng đã tồn tại !'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data_category = CategoryCardModel::find($request->id);
        if ($data_category) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_category->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/category_card/';
                $file->move($path, $fileName);
                $data_category->image = '/uploads/images/category_card/' . $fileName;
                $data_category->save();
            }
            $data_category->name = $request->name;
            $data_category->type = $request->type;
            $data_category->active = $request->active;
            $data_category->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/card/category'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function deleteCategory($id)
    {
        try {
            CategoryCardModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    //danh sach the cao
    public function index()
    {
        $data = CardModel::orderBy('id', 'desc')->get();
        if (count($data)) {
            foreach ($data as $key => $value) {
                $data_category = CategoryCardModel::find($value['category_id']);
                $data[$key]['image'] = $data_category['image'];
                if ($value['status'] == 0) {
                    $data[$key]['status'] = 'Chưa sử dụng';
                    $data[$key]['class'] = 'success';
                } else {
                    $data[$key]['status'] = 'Đã sử dụng';
                    $data[$key]['class'] = 'danger';
                }
            }
        }
        $data_return = [
            'title' => 'Quản lý thẻ cào',
            'data' => $data
        ];
        return view('admin.card.index', $data_return);
    }

    public function add()
    {
        $category = CategoryCardModel::orderBy('type', 'asc')->get();
        $data_return = [
            'title' => 'Thêm thẻ cào',
            'category' => $category
        ];
        return view('admin.card.form', $data_return);
    }

    public function save(Request $request)
    {
        if ($request->id) {
            $dataReturn = $this->update($request);
        } else {
            $dataReturn = $this->create($request);
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function create($request)
    {
        $rules = [
            'seri' => 'required|unique:card,seri',
            'code' => 'required|unique:card,code',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin !',
            'seri.unique' => 'Số seri đã tồn tại !',
            'code.unique' => 'Mã thẻ đã tồn tại !'
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $data_category = CardModel::create([
                'seri' => $request->seri,
                'code' => $request->code,
                'category_id' => $request->category_id,
                'price' => $request->price,
                'token' => Str::random('20')
            ]);
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/card'),
            ];
        }
        return $dataReturn;
    }

    public function edit(Request $request)
    {
        $data_category = CardModel::find($request->id);
        $category = CategoryCardModel::orderBy('type', 'asc')->get();
        $data_return = [
            'title' => 'Cập nhật thẻ cào',
            'data' => $data_category,
            'category' => $category
        ];
        return view('admin.card.form', $data_return);
    }

    public function update($request)
    {
        $rules = [
            'seri' => 'required|unique:card,seri,' . $request->id,
            'code' => 'required|unique:card,code,' . $request->id,
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin !',
            'seri.unique' => 'Số seri đã tồn tại !',
            'code.unique' => 'Mã thẻ đã tồn tại !'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data_category = CardModel::find($request->id);
        if ($data_category) {
            $data_category->seri = $request->seri;
            $data_category->code = $request->code;
            $data_category->category_id = $request->category_id;
            $data_category->price = $request->price;
            $data_category->token = Str::random(20);
            $data_category->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/card'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function delete($id)
    {
        try {
            CardModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
