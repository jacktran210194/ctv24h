<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuctionController extends Controller
{
    /** Đấu giá
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        switch ($request->type) {
            case 'going':
                $type = 'going';
                break;
            case 'done':
                $type = 'done';
                break;
            case 'success':
                $type = 'success';
                break;
            default:
                $type = 'follow';
                break;
        }
        $data_return = [
            'type' => $type
        ];
        return view('frontend.auction.auction', $data_return);
    }
}
