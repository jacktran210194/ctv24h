<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CollaboratorModel;
use App\Models\CustomerModel;
use App\Models\OrderModel;
use App\Models\ProductAccessModel;
use App\Models\ProductModel;
use App\Models\ShopModel;
use App\Models\SupplierModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Response;
use App\Models\User;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use App\Models\AdminModel;

session_start();

class LoginController extends Controller
{
    public function index(Request $request)
    {
        if ($request->session()->has('data_user')) {
            Redirect::to('admin')->send();
        }
        return view('admin.login.login', ['title' => 'Đăng nhập hệ thống']);
    }

    /**
     * Login
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $username = $request->username ?? '';
        $password = $request->password ?? '';
        if (empty($username) || empty($password)) {
            $dataReturn = [
                'status' => false,
                'msg' => 'Vui lòng điền đầy đủ thông tin'
            ];
        } else {
            $dataUser = AdminModel::where('username', $username)->where('password', md5($password))->first();
            if ($dataUser) {
                session()->flush();
                $dataUser['super_admin'] = true;
                $request->session()->put('data_user', $dataUser);
                $dataReturn = [
                    'status' => true,
                    'msg' => 'Đăng nhập thành công',
                    'url' => URL::to('admin')
                ];
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Đăng nhập không thành công'
                ];
            }
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     * Change password
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function changePassword(Request $request)
    {
        if (!$request->session()->has('data_user')) {
            Redirect::to('admin')->send();
        }
        $id = $request->session()->get('data_user')->id;
        $data_return = [
            'id' => $id,
            'title' => 'Đăng nhập hệ thống'
        ];
        return view('admin.login.change_password', $data_return);
    }

    /**
     * Save change password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveChangePassword(Request $request)
    {
        try {
            $rules = [
                'password_old' => 'required',
                'password_new' => 'required',
                're_password_new' => 'required',
                'id' => 'required',
            ];
            $customMessages = [
                'required' => 'Vui lòng điền đầy đủ thông tin.',
            ];

            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $errors = $messages->all();
                $dataReturn['status'] = false;
                $dataReturn['msg'] = $errors[0];
            } else {
                if ($request->password_new !== $request->re_password_new) {
                    $dataReturn['status'] = false;
                    $dataReturn['msg'] = 'Mật khẩu không trùng nhau';
                } else {
                    $data_user = User::find($request->id);
                    if ($data_user) {
                        if ($data_user->password !== md5($request->password_old)) {
                            $dataReturn['status'] = false;
                            $dataReturn['msg'] = 'Mật khẩu cũ không đúng';
                        } else {
                            $data_user->password = md5($request->password_new);
                            $data_user->save();
                            $dataReturn['status'] = true;
                            $dataReturn['url'] = URL::to('admin');
                        }
                    } else {
                        $dataReturn['status'] = false;
                        $dataReturn['msg'] = 'Đã có lỗi xảy ra';
                    }
                }
            }

            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn['status'] = false;
            $dataReturn['msg'] = 'Đã có lỗi xảy ra';
            $dataReturn['error'] = $e;
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Logout
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(Request $request)
    {
        $data_collaborator = $request->session()->get('data_collaborator');
        $data_supplier = $request->session()->get('data_supplier');
        $data_user = $request->session()->get('data_user');
        if ($data_collaborator) {
            session()->flush();
            return redirect('admin/collaborator_admin/login');
        } else if ($data_supplier) {
            session()->flush();
            return redirect('admin/supplier_admin/login');
        } elseif ($data_user) {
            session()->flush();
            return redirect('admin/login');
        }
    }

    //login now

    public function login_ncc(Request $request)
    {
        session()->forget('data_collaborator');
        $data_customer = session()->get('data_customer');
        if ($data_customer) {
            $data_supplier = SupplierModel::where('customer_id', $data_customer->id)->first();
            if ($data_supplier) {
                $data_supplier['supplier'] = true;
                $request->session()->put('data_supplier', $data_supplier);

                //Today & before day
                $data_shop = ShopModel::where('supplier_id', $data_supplier->id)->first();
                $today_head = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 00:00:00');
                $today_end = Carbon::now('Asia/Ho_Chi_Minh')->format('Y-m-d 23:59:59');
                $today_head_before = Carbon::now('Asia/Ho_Chi_Minh')->subDay(1)->format('Y-m-d 00:00:00');
                $today_end_before = Carbon::now('Asia/Ho_Chi_Minh')->subDay(1)->format('Y-m-d 23:59:59');
                //end day
                $this->editStatusOrder($data_shop->id);
                $this->overdueForDelivery($data_shop->id);
                //Tổng số order
                $count_order_before = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)->where('date', '>=', $today_head_before)->where('date', '<=', $today_end_before)->count();
                $count_order_today = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)->where('date', '>=', $today_head)->where('date', '<=', $today_end)->count();
                if ($count_order_before == 0) {
                    $order_rate = 0;
                } else {
                    $order_rate = round($count_order_today / $count_order_before, 2);
                }
                if ($count_order_before <= $count_order_today && $count_order_before != 0) {
                    $tang_truong_order = 1; //1 đi lên
                    $order_rate_percent = round(($count_order_today - $count_order_before) / $count_order_before * 100, 2);
                } elseif ($count_order_before > $count_order_today && $count_order_before != 0) {
                    $tang_truong_order = -1; //-1 đi xuống
                    $order_rate_percent = 100 - ($order_rate * 100);
                } elseif ($count_order_before == 0 || $count_order_today == 0) {
                    $tang_truong_order = 0; //ko thay đổi
                    $order_rate_percent = 0;
                }
                $data_count_order = [
                    'count_order_today' => $count_order_today,
                    'order_rate_percent' => $order_rate_percent,
                    'tang_truong_order' => $tang_truong_order
                ];
                //end tổng số order

                //Lượt truy cập
                $count_access_today = ProductAccessModel::where('shop_id', $data_shop->id)->where('created_at', '>=', $today_head)->where('created_at', '<=', $today_end)->count();
                $count_access_before = ProductAccessModel::where('shop_id', $data_shop->id)->where('created_at', '>=', $today_head_before)->where('created_at', '<=', $today_end_before)->count();
                if ($count_access_today == 0 || $count_access_before == 0) {
                    $rate_access = 0;
                } else {
                    $rate_access = round($count_access_today / $count_access_before, 2);
                }
                if ($count_access_today >= $count_access_before && $count_access_before != 0) {
                    $percent_access = round(($count_access_today - $count_access_before) / $count_access_before * 100, 2);
                    $tang_truong_access = 1;
                } elseif ($count_access_today < $count_access_before && $count_access_before != 0) {
                    $percent_access = 100 - ($rate_access * 100);
                    $tang_truong_access = -1;
                } elseif ($count_access_today == 0 || $count_access_before == 0) {
                    $percent_access = 0;
                    $tang_truong_access = 0;
                }
                $data_product_access = [
                    'percent_access' => $percent_access,
                    'tang_truong_access' => $tang_truong_access,
                    'count_access' => $count_access_today
                ];
                //End lượt truy cập

                //Tỷ lệ chuyển đổi
                $count_order_confirm_today = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)
                    ->where('status', '>', 0)
                    ->where('date', '>=', $today_head)
                    ->where('date', '<=', $today_end)
                    ->count();
                $count_order_confirm_before = OrderModel::where('shop_id', $data_shop->id)->where('is_selected', 2)
                    ->where('status', '>', 0)
                    ->where('date', '>=', $today_head_before)
                    ->where('date', '<=', $today_end_before)
                    ->count();
                if ($count_order_confirm_today != 0 && $count_access_today != 0) {
                    $rate_convert_today = round(($count_order_confirm_today / $count_access_today) * 100, 2);
                } else {
                    $rate_convert_today = 0;
                }
                if ($count_order_confirm_before != 0 && $count_access_before != 0) {
                    $rate_convert_before = round(($count_order_confirm_before / $count_access_before) * 100, 2);
                } else {
                    $rate_convert_before = 0;
                }
                if ($rate_convert_today == 0 || $rate_convert_before == 0) {
                    $rate_convert = 0;
                } else {
                    $rate_convert = round($rate_convert_today / $rate_convert_before, 2);
                }
                if ($rate_convert_today == 0 || $rate_convert_before == 0) {
                    $tang_truong_convert = 0;
                    $percent_convert = 0;
                } elseif ($rate_convert_today >= $rate_convert_before && $rate_convert_before != 0) {
                    $tang_truong_convert = 1;
                    $percent_convert = round(($rate_convert_today - $rate_convert_before) / $rate_convert_before * 100, 2);
                } elseif ($rate_convert_today < $rate_convert_before && $rate_convert_before != 0) {
                    $tang_truong_convert = -1;
                    $percent_convert = 100 - ($rate_convert * 100);
                }
                $data_rate_convert = [
                    'rate_convert_today' => $rate_convert_today,
                    'tang_truong_convert' => $tang_truong_convert,
                    'percent_convert' => $percent_convert
                ];
                //End tỷ lệ chuyển đổi

                $order_waiting_confirm = OrderModel::where('shop_id', $data_shop->id)->where('status', 1)->where('shipping_confirm', 0)->count();
                $order_waiting_shipping = OrderModel::where('shop_id', $data_shop->id)->where('status', 1)->where('shipping_confirm', 1)->count();
                $order_shipped = OrderModel::where('shop_id', $data_shop->id)->where('status', 3)->where('shipping_confirm','=', 1)->count();
                $order_cancel = OrderModel::where('shop_id', $data_shop->id)->where('status', 4)->count();
                $order_return_money = OrderModel::where('shop_id', $data_shop->id)->where('status', 5)->count();
                $product_sold_out = DB::table('product')
                    ->join('attribute_value', 'attribute_value.attribute_id', '=', 'product.id')
                    ->where('attribute_value.quantity', '=', 0)
                    ->where('product.shop_id', $data_shop->id)
                    ->count();
                $product_lock = ProductModel::where('shop_id', $data_shop->id)->where('is_status', 2)->count();
                $dataReturn = [
                    'title' => 'Kênh người bán | CTV24H',
                    'status' => true,
                    'msg' => 'Đăng nhập thành công',
                    'url' => URL::to('admin/supplier_admin/'),
                    'order_waiting_confirm' => $order_waiting_confirm ?? 0,
                    'order_waiting_shipping' => $order_waiting_shipping ?? 0,
                    'order_shipped' => $order_shipped ?? 0,
                    'order_cancel' => $order_cancel ?? 0,
                    'order_return_money' => $order_return_money ?? 0,
                    'product_sold_out' => $product_sold_out ?? 0,
                    'product_lock' => $product_lock ?? 0,
                    'data_shop' => $data_shop,
                    'data_count_order' => $data_count_order,
                    'data_product_access' => $data_product_access,
                    'data_rate_convert' => $data_rate_convert
                ];
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Đăng nhập không thành công'
                ];
            }
            return view('supplier.dashboard.index', $dataReturn);
        } else {
            return redirect('admin/supplier_admin/login');
        }
    }

    public function login_ctv(Request $request)
    {
        session()->forget('data_supplier');
        $data_customer = session()->get('data_customer');
        if ($data_customer) {
            $data_collaborator = CollaboratorModel::where('customer_id', $data_customer->id)->first();
            if ($data_collaborator) {
                $data_collaborator['collaborator'] = true;
                $request->session()->put('data_collaborator', $data_collaborator);

                $dataReturn = [
                    'title' => 'Kênh CTV',
                    'status' => true,
                    'msg' => 'Đăng nhập thành công',
                    'url' => URL::to('admin/collaborator_admin/')
                ];
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Đăng nhập không thành công'
                ];
            }
//            return view('collaborator.dashboard.index', $dataReturn);
            return redirect('admin/order_collaborator/all');

        } else {
            return redirect('admin/collaborator_admin/login');
        }
    }
}
