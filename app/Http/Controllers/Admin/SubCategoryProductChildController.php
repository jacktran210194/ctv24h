<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryProductChildModel;
use App\Models\CategoryProductModel;
use App\Models\SubCategoryProductChildModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SubCategoryProductChildController extends Controller
{
    public function index()
    {
        $data = SubCategoryProductChildModel::orderBy('name')->get();
        foreach($data as $key => $value){
            $category_child = CategoryProductChildModel::find($value['category_product_child_id']);
            $category = CategoryProductModel::find($category_child->category_id);
            $data[$key]['category_name'] = isset($category_child) ? $category_child['name'] : '';
            $data[$key]['category'] = isset($category) ? $category['name'] : '';
        }
        $data_return = [
            'title' => 'Quản lý danh mục sản phẩm con',
            'data' => $data
        ];
        return view('admin.sub_category_product_child.index', $data_return);
    }

    public function add(){
        $category = CategoryProductModel::all();
        $category_1 = CategoryProductChildModel::all();
        $data_return = [
            'category' => $category,
            'category_1' => $category_1,
            'title' => 'Thêm danh mục sản phẩm con',
        ];
        return view('admin.sub_category_product_child.form',$data_return);
    }

    public function save(Request $request)
    {
        if ($request->id) {
            $dataReturn = $this->update($request);
        } else {
            $dataReturn = $this->create($request);
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function create($request){
        $rules = [
            'name' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $data_category = SubCategoryProductChildModel::create([
                'name' => $request->name,
                'category_product_child_id' => $request->category_id,
                'category_product_id' => $request->category_product_id,
                'image' => ''
            ]);
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/category_product_child/';
            $file->move($path, $fileName);
            $data_category->image = '/uploads/images/category_product_child/' . $fileName;
            $data_category->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/sub_category_product_child'),
            ];
        }
        return $dataReturn;
    }

    public function edit(Request $request){
        $data_category = SubCategoryProductChildModel::find($request->id);
        $category_1 = CategoryProductChildModel::all();
        $category = CategoryProductModel::all();
        $data_return = [
            'title' => 'Cập nhật danh mục sản phẩm',
            'data' => $data_category,
            'category' => $category,
            'category_1'=> $category_1
        ];
        return view('admin.sub_category_product_child.form',$data_return);
    }

    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    public function update($request){
        $rules = [
            'name' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data_category = SubCategoryProductChildModel::find($request->id);
        if ($data_category) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data_category->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/category_product_child/';
                $file->move($path, $fileName);
                $data_category->image = '/uploads/images/category_product_child/' . $fileName;
                $data_category->save();
            }

            $data_category->name = $request->name;
            $data_category->category_product_child_id = $request->category_id;
            $data_category->category_product_id = $request->category_product_id;
            $data_category->save();
            $dataReturn = [
                'status' => true,
                'url' => URL::to('admin/sub_category_product_child'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function delete($id){
        try {
            SubCategoryProductChildModel::destroy($id);
            $dataReturn = [
                'status' => true,
                'msg' => 'Xoá dữ liệu thành công',
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }
}
