<?php

namespace App\Http\Controllers;

use App\Models\CityModel;
use App\Models\CoinModel;
use App\Models\CustomerAddressModel;
use App\Models\DistrictModel;
use App\Models\ShopModel;
use App\Models\WardModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use App\Models\CustomerModel;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends BaseController
{
    //Profile
    public function getFile()
    {
        $dataCustomer = session()->get('data_customer');
        if (!$dataCustomer) {
            return redirect('login');
        } else {
            $customer = CustomerModel::find($dataCustomer['id']);
            $data_shop = ShopModel::where('customer_id', $customer['id'])->first();
        }

        $dataReturn = [
            'status' => true,
            'title' => 'Hồ sơ cá nhân',
            'data_customer' => $customer,
            'data_shop' => $data_shop
        ];
        return view('frontend.user.file', $dataReturn);
    }

    public function pointWallet(Request $request)
    {
        switch ($request->type) {
            case 'received':
                $type = 'received';
                break;
            case 'used':
                $type = 'used';
                break;
            default:
                $type = 'all-history';
                break;
        }
        $dataCustomer = session()->get('data_customer');
        if (!$dataCustomer) {
            return redirect('login');
        } else {
            $coin = 0;
            $dataCoin = CoinModel::where('customer_id', $dataCustomer->id)->get();
            foreach ($dataCoin as $k => $v) {
                $coin += $v['value'];
            }
            $customer = CustomerModel::find($dataCustomer['id']);
            $dataHistoryCoin = CoinModel::where('customer_id', $dataCustomer->id)->orderBy('created_at', 'desc')->get();
            foreach ($dataHistoryCoin as $key => $value) {
                if ($value->reason == 0) {
                    $dataHistoryCoin[$key]['status'] = 'Đăng nhập mỗi ngày';
                } else if ($value->reason == 1) {
                    $dataHistoryCoin[$key]['status'] = 'Xác thực tài khoản';
                } else {
                    $dataHistoryCoin[$key]['status'] = 'Mua hàng';
                }
            }

        }
        $dataReturn = [
            'status' => true,
            'type' => $type,
            'title' => 'Ví Point',
            'data_customer' => $customer,
            'coin' => $coin,
            'dataHistoryCoin' => $dataHistoryCoin
        ];
        return view('frontend.user.pointWallet', $dataReturn);
    }

    public function update(Request $request)
    {
        $data_customer = session()->get('data_customer');
        if (!$data_customer) {
            return redirect('login');
        }
        $rules = [
            'name' => 'required',
            'birthday' => 'required|date|before:today',
            'email' => 'required|email|unique:customer,email,' . $request->id,
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
            'before' => 'Vui lòng nhập ngày sinh trước ngày hiện tại !',
            'email' => 'Email không hợp lệ',
            'email.unique' => 'Email đã tồn tại !'
        ];
        $dataReturn = $this->checkRule($request, $rules, $customMessages);
        if (!$dataReturn['status']) {
            return $dataReturn;
        }
        $data = CustomerModel::find($data_customer['id']);
        $data_shop = ShopModel::where('customer_id', $data_customer['id'])->first();
        if ($data) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if (!$file->isValid()) {
                    $dataReturn = [
                        'status' => false,
                        'msg' => 'File tải lên không hợp lệ'
                    ];
                    return $dataReturn;
                }
                File::delete($data->image);
                $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads/images/customer/';
                $file->move($path, $fileName);
                $data->image = '/uploads/images/customer/' . $fileName;
                $data->save();
            }
            $data->name = $request->name;
            $data->email = $request->email;
            $data->gender = $request->gender;
            $data->birthday = $request->birthday;
            $data->save();
            $data_shop->name = $request->shop_name;
            $data_shop->save();
            $dataReturn = [
                'status' => true,
                'msg' => 'Thay đổi thông tin thành công !',
                'url' => URL::to('user/file'),
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra! Vui lòng thử lại.'
            ];
        }
        return $dataReturn;
    }

    public function checkRule($request, $rules, $customMessages)
    {
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn = [
                'status' => false,
                'msg' => $errors[0]
            ];
            return $dataReturn;
        }
        $dataReturn = [
            'status' => true,
        ];
        return $dataReturn;
    }

    public function getBank()
    {
        $dataCustomer = session()->get('data_customer');
        if (!$dataCustomer) {
            return redirect('/');
        }
        $dataReturn = [
            'title' => 'Tài khoản ngân hàng',
            'data' => isset($customer) ? $customer : null
        ];
        return view('frontend.user.bank', $dataReturn);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function address()
    {
        $dataCustomer = session()->get('data_customer');
        $address = CustomerAddressModel::where('customer_id', $dataCustomer->id)->orderBy('id', 'desc')->get();
        if (count($address)) {
            foreach ($address as $k => $v) {
//                $city = CityModel::find($v['city_id']);
//                $district = DistrictModel::find($v['district_id']);
//                $ward = WardModel::where('ward_id', $v['ward_id'])->first();
//                if ($ward) {
//                    $address[$k]['full_address'] = $v['address'] . ', ' . $ward->name . ', ' . $district->name . ', ' . $city->name . '.';
//                }
                $address[$k]['full_address'] = $v['address'];
            }
        }
        $data['address'] = $address;
        $data['title'] = 'Địa chỉ';
        return view('frontend.address.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getModalAddress(Request $request)
    {
        try {
            $id_address = $request->id_address ?? '';
            $city = CityModel::all();
            $data['city'] = $city;
            if (empty($id_address)) {
                $html = view('frontend.address.modal_form_address', $data);
            } else {
                $dataAddress = CustomerAddressModel::find($id_address);
                $city_id = $dataAddress->city_id ?? '';
                $district_id = $dataAddress->district_id ?? '';
                $district = DistrictModel::where('city_id', $city_id)->get();
                $ward = WardModel::where('district_id', $district_id)->get();
                $data['district'] = $district;
                $data['ward'] = $ward;
                $data['data'] = $dataAddress;
                $html = view('frontend.address.modal_form_address', $data);
            }
            $dataReturn['html'] = strval($html);
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterAddress(Request $request)
    {
        try {
            $type = $request->type ?? '';
            $id = $request->id ?? '';
            if ($type === 'city') {
                $filter = DistrictModel::where('city_id', $id)->get();
                $data['title'] = 'Quận/Huyện';
                $data['ward'] = false;
            } else {
                $filter = WardModel::where('district_id', $id)->get();
                $data['title'] = 'Phường/Xã';
                $data['ward'] = true;
            }
            $data['data'] = $filter;
            $html = view('frontend.address.address', $data);
            $dataReturn['html'] = strval($html);
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    public function change_password()
    {
        $dataCustomer = session()->get('data_customer');
        if (!$dataCustomer) {
            return redirect('/');
        } else {
            $customer = CustomerModel::find($dataCustomer['id']);
        }
        $dataReturn = [
            'title' => 'Đổi mật khẩu',
            'data' => isset($customer) ? $customer : null
        ];
        return view('frontend.user.change_password', $dataReturn);
    }

    public function savePassword(Request $request)
    {
        $dataCustomer = session()->get('data_customer');
        if (!$dataCustomer) {
            return redirect('/');
        }
        try {
            $rules = [
                'password_old' => 'required',
                'password_new' => 'required|different:password_old|max:50|min:8',
                're_password_new' => 'required',
            ];
            $customMessages = [
                'required' => 'Vui lòng điền đầy đủ thông tin.',
                'different' => 'Mật khẩu mới không được trùng với mật khẩu cũ !',
                'max' => 'Mật khẩu tối đa 50 ký tự !',
                'min' => 'Mật khẩu tối thiểu 8 ký tự !'
            ];
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $errors = $messages->all();
                $dataReturn['status'] = false;
                $dataReturn['msg'] = $errors[0];
            } else {
                if ($request->password_new != $request->re_password_new) {
                    $dataReturn['status'] = false;
                    $dataReturn['msg'] = 'Mật khẩu không trùng nhau';
                } else {
                    $data_user = CustomerModel::find($request->id);
                    if ($data_user) {
                        if ($data_user->password != md5($request->password_old)) {
                            $dataReturn['status'] = false;
                            $dataReturn['msg'] = 'Mật khẩu cũ không đúng';
                        } else {
                            $data_user->password = md5($request->password_new);
                            $data_user->save();
                            if (session()->has('data_customer')) {
                                session()->flush();
                            }
                            $dataReturn = [
                                'status' => true,
                                'msg' => 'Đổi mật khẩu thành công ! Vui lòng dùng mật khẩu mới để đăng nhập lại !',
                                'url' => URL::to('login')
                            ];
                        }
                    } else {
                        $dataReturn['status'] = false;
                        $dataReturn['msg'] = 'Đã có lỗi xảy ra';
                    }
                }
            }
            return $dataReturn;
        } catch (Exception $e) {
            $dataReturn['status'] = false;
            $dataReturn['msg'] = 'Đã có lỗi xảy ra';
            $dataReturn['error'] = $e;
            return $dataReturn;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse\
     */
    public function saveAddress(Request $request)
    {
        $rules = [
            'city_id' => 'required',
            'district_id' => 'required',
            'ward_id' => 'required',
            'name' => 'required',
            'phone' => 'required',
//            'address_maps' => 'required',
            'address' => 'required',
//            'longitude' => 'required',
//            'latitude' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];

        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $data_return['status'] = false;
            $data_return['msg'] = $errors[0];
        } else {
            $dataCustomer = session()->get('data_customer');
////            $data_save = $request->all();
//            $data_save->city_id = $request->city_id;
//            $data_save->district_id = $request->district_id;
//            $data_save->ward_id = $request->warrd_id;
//            $data_save->name = $request->name;
//            $data_save->phone = $request->phone;
//            $data_save->address = $request->address;
//            $data_save['customer_id'] = $dataCustomer->id;
            if ($request->id) {
                if ($request->default == 1) {
                    CustomerAddressModel::where('customer_id', $dataCustomer->id)
                        ->update(['default' => 0]);
                }
//                CustomerAddressModel::whereId($request->id)->update($data_save);
                $data_address_customer = CustomerAddressModel::find($request->id);
                $data_address_customer->city_id = $request->city_id;
                $data_address_customer->district_id = $request->district_id;
                $data_address_customer->ward_id = $request->ward_id;
                $data_address_customer->name = $request->name;
                $data_address_customer->phone = $request->phone;
                $data_address_customer->address = '';
                $data_address_customer->longitude = '';
                $data_address_customer->address_maps = '';
                $data_address_customer->latitude = '';
                $data_address_customer->customer_id = $dataCustomer->id;
                $data_address_customer->default = $request->default;
                $data_address_customer->save();
                $data_city = CityModel::find($data_address_customer->city_id);
                $data_district = DistrictModel::find($data_address_customer->district_id);
                $data_ward = WardModel::where('ward_id', $data_address_customer->ward_id)->first();
                $data_address_customer->address = $request->address . ', ' . $data_ward->name . ', ' . $data_district->name . ', ' . $data_city->name;
                $data_address_customer->save();
//                CustomerAddressModel::whereId($request->id)->update([
//                    'city_id' => $request->city_id,
//                    'district_id' => $request->district_id,
//                    'ward_id' => $request->ward_id,
//                    'name' => $request->name,
//                    'phone' => $request->phone,
//                    'address' => $request->address,
//                    'longitude' => '',
//                    'address_maps' => '',
//                    'latitude' => '',
//                    'customer_id' => $dataCustomer->id,
//                    'default' => $request->default
//                ]);

            } else {
                if ($request->default == 1) {
                    CustomerAddressModel::where('customer_id', $dataCustomer->id)
                        ->update(['default' => 0]);
                }
//                CustomerAddressModel::create($data_save);
//                CustomerAddressModel::create([
//                    'city_id' => $request->city_id,
//                    'district_id' => $request->district_id,
//                    'ward_id' => $request->ward_id,
//                    'name' => $request->name,
//                    'phone' => $request->phone,
//                    'address' => $request->address,
//                    'longitude' => '',
//                    'address_maps' => '',
//                    'latitude' => '',
//                    'customer_id' => $dataCustomer->id,
//                    'default' => $request->default
//                ]);
                $data_address_customer_new = new CustomerAddressModel();
                $data_address_customer_new->city_id = $request->city_id;
                $data_address_customer_new->district_id = $request->district_id;
                $data_address_customer_new->ward_id = $request->ward_id;
                $data_address_customer_new->name = $request->name;
                $data_address_customer_new->phone = $request->phone;
                $data_address_customer_new->address = '';
                $data_address_customer_new->longitude = '';
                $data_address_customer_new->address_maps = '';
                $data_address_customer_new->latitude = '';
                $data_address_customer_new->customer_id = $dataCustomer->id;
                $data_address_customer_new->default = $request->default;
                $data_address_customer_new->save();
                $data_city = CityModel::find($data_address_customer_new->city_id);
                $data_district = DistrictModel::find($data_address_customer_new->district_id);
                $data_ward = WardModel::where('ward_id', $data_address_customer_new->ward_id)->first();
                $data_address_customer_new->address = $request->address . ', ' . $data_ward->name . ', ' . $data_district->name . ', ' . $data_city->name;
                $data_address_customer_new->save();
            }
            $data_return = [
                'status' => true
            ];
        }
        return response()->json($data_return, Response::HTTP_OK);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAddress($id)
    {
        try {
            CustomerAddressModel::destroy($id);
            $dataReturn = [
                'status' => true,
            ];
            return response()->json($dataReturn, Response::HTTP_OK);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function defaultAddress(Request $request, $id)
    {
        $dataCustomer = session()->get('data_customer');
        CustomerAddressModel::where('customer_id', $dataCustomer->id)
            ->update(['default' => 0]);
        $customer_address = CustomerAddressModel::find($request->id);
        if (isset($customer_address)) {
            $customer_address->default = 1;
            $customer_address->save();
        }
        return redirect('user/address');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function defaultAjax(Request $request)
    {
        try {
            $dataCustomer = session()->get('data_customer');
            if ($request->get('type') == 1){
                CustomerAddressModel::where('customer_id', $dataCustomer->id)
                    ->update(['default' => 0]);
                $customer_address = CustomerAddressModel::find($request->get('id'));
                if (isset($customer_address)) {
                    $customer_address->default = 1;
                    $customer_address->save();
                }
                $dataReturn = [
                    'status' => true,
                    'customer_address' => $customer_address
                ];
            }else{
                $customer_address = CustomerAddressModel::where('customer_id', $dataCustomer->id)->where('default', 1)->first();
                $dataReturn = [
                    'status' => true,
                    'customer_address' => $customer_address
                ];
            }
            $views = view('frontend.AllAddressCustomer.address_default', $dataReturn)->render();
            return response()->json(['prod' => $views]);
        } catch (\Exception $e) {
            $dataReturn = [
                'error' => $e,
            ];
            return response()->json($dataReturn, Response::HTTP_BAD_REQUEST);
        }
    }

    //Đổi sđt
    public function change_phone()
    {
        $data_login = session()->get('data_customer');
        if (!$data_login) {
            return redirect('login');
        } else {
            $dataReturn = [
                'title' => 'Đổi số điện thoại',
                'data_login' => $data_login
            ];
            return view('frontend.user.change_phone', $dataReturn);
        }
    }

    public function check_pass(Request $request){
        $password = md5($request->password);
        $data_customer = CustomerModel::find($request->customer_id);
        if($password == $data_customer->password){
            $dataReturn = [
              'status' => true,
            ];
        }else{
            $dataReturn = [
              'status' => false,
              'msg' => 'Mật khẩu không chính xác !'
            ];
        }
        return $dataReturn;
    }

    public function save_phone(Request $request){
        $rules = [
            'phone_old' => 'required',
            'phone_new' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $phone_old = $request->phone_old;
            $phone_new = $request->phone_new;
            $data_customer = CustomerModel::find($request->customer_id);
            if($phone_old != $data_customer->phone){
                $dataReturn = [
                  'status' => false,
                  'msg' => 'Số điện thoại cũ không chính xác !'
                ];
            }else{
                $data_customer->phone = $phone_new;
                $data_customer->save();
                $request->session()->put('data_customer', $data_customer);
                $dataReturn['status'] = true;
            }
        }
        return $dataReturn;
    }
}
