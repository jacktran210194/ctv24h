<?php

namespace App\Http\Controllers;

use App\Models\CustomerModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class RechargeCardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {

        $data_login = session()->get('data_customer');

        switch ($request->type) {
            case 'currentPoint':
                $type = 'currentPoint';
                break;
            default:
                $type = 'recharge';
                break;
        }
        $data_return = [
            'type' => $type,
            'title' => "Thẻ nạp điểm",
            "data_login" => $data_login,
        ];
        return view('frontend.RechargeCard.index', $data_return);
    }

    public function updatePoint(Request $request){
        $data_login = session()->get('data_customer');
        $user = CustomerModel::find($data_login['id']);
        $point = $request->point + $user['point'];
        $user->update(['point' => $point ]);
        return true;
    }

}
