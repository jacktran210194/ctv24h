<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyAuction extends Controller
{
    public function index(Request $request){
        switch ($request->type) {
            case 'going':
                $type = 'going';
                break;
            case 'done':
                $type = 'done';
                break;
            case 'success':
                $type = 'success';
                break;
            default:
                $type = 'follow';
                break;
        }
        $data_return = [
            'type' => $type
        ];
        return view('frontend.my_auction.index', $data_return);
    }

    public function search(){
        return view('frontend.my_auction.search');
    }

    public function guideAuction(Request $request){
        switch ($request->type) {
            case 'going':
                $type = 'going';
                break;
            case 'done':
                $type = 'done';
                break;
            case 'success':
                $type = 'success';
                break;
            default:
                $type = 'follow';
                break;
        }
        $data_return = [
            'type' => $type
        ];
        return view('frontend.my_auction.guide',$data_return);
    }

    public function auctionDetails(){
        return view('frontend.my_auction.auction_details');
    }
}
