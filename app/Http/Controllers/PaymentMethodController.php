<?php

namespace App\Http\Controllers;

use App\Models\DeliveryGHTKModel;
use App\Models\MethodMomoModel;
use App\Models\MethodZaloPayModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use function PHPUnit\Framework\throwException;

class PaymentMethodController extends AdminBaseController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function zalo_pay()
    {
        $data_return['data'] = MethodZaloPayModel::first();
        return view('admin.payment_method.zalo_pay', $data_return);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function zalo_pay_save(Request $request)
    {
        try {
            $rules = [
                'user_id' => 'required',
                'access_token' => 'required',
            ];
            $customMessages = [
                'required' => 'Vui lòng điền đầy đủ thông tin.',
            ];
            $dataReturn = [];
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $errors = $messages->all();
                $dataReturn['status'] = false;
                $dataReturn['msg'] = $errors[0];
            } else {
                if (!empty($request->id)) {
                    $data = MethodZaloPayModel::find($request->id);
                } else {
                    $data = new MethodZaloPayModel();
                }
                $data->muid = $request->user_id;
                $data->maccesstoken = $request->access_token;
                $data->save();
                $dataReturn['status'] = true;
            }
            return response()->json($dataReturn, 200);
        } catch (\Exception $e) {
            throwException($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function zalo_pay_connect(Request $request)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://zalopay.com.vn/ummerchant/verifymerchantaccesstoken',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                    "muid":"' . $request->user_id . '",
                    "maccesstoken":"' . $request->access_token . '",
                    "systemlogin": "1"
                }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $dataReturn['response'] = $response;
        return response()->json($dataReturn, 200);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function momo()
    {
        $data_return['data'] = MethodMomoModel::first();
        return view('admin.payment_method.momo', $data_return);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function momo_save(Request $request)
    {
        try {
            $rules = [
                'partnerCode' => 'required',
                'accessKey' => 'required',
            ];
            $customMessages = [
                'required' => 'Vui lòng điền đầy đủ thông tin.',
            ];
            $dataReturn = [];
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $errors = $messages->all();
                $dataReturn['status'] = false;
                $dataReturn['msg'] = $errors[0];
            } else {
                if (!empty($request->id)) {
                    $data = MethodMomoModel::find($request->id);
                } else {
                    $data = new MethodMomoModel();
                }
                $data->partnerCode = $request->partnerCode;
                $data->accessKey = $request->accessKey;
                $data->save();
                $dataReturn['status'] = true;
            }
            return response()->json($dataReturn, 200);
        } catch (\Exception $e) {
            $dataReturn['e'] = $e;
            return response()->json($dataReturn, 500);
        }
    }
}
