<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CTV24hCartController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('frontend.CTV24hCart.index');
    }
}
