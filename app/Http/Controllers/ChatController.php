<?php

namespace App\Http\Controllers;

use App\Events\MyEvent;
use App\Models\CustomerModel;
use App\Models\GroupMessengerModel;
use App\Models\MessegeModel;
use App\Models\ShopModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class ChatController extends BaseController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $dataCustomer = session()->get('data_customer');
        $mes = GroupMessengerModel::where('user_id_1', $dataCustomer->id)
            ->orWhere('user_id_2', $dataCustomer->id)
            ->orderBy('date', 'DESC')
            ->get();
        $i = 0;
        $data['list_messenger'] = [];
        $data['group_id'] = '';
        $data['group_messenger_id'] = '';
        foreach ($mes as $k => $v) {
            $user_id = $dataCustomer->id === $v['user_id_1'] ? $v['user_id_2'] : $v['user_id_1'];
            $customer = CustomerModel::find($user_id);
            if (isset($customer)) {
                $shop_user = ShopModel::where('customer_id', $customer->id)->first();
                if (isset($shop_user)){
                    $mes[$k]['name_customer'] = $shop_user->name ;
                    $mes[$k]['avt_customer'] = isset($shop_user->image) ? $shop_user->image : 'Icon/chat/avarta.png';
                }else{
                    $mes[$k]['name_customer'] = isset($customer->name) ? $customer->name : $customer->phone ;
                    $mes[$k]['avt_customer'] = isset($customer->image) ? $customer->image : 'Icon/chat/avarta.png';
                }
                $messenger = MessegeModel::where('group_messenger_id', $v['id'])->orderBy('created_at', 'DESC')->first();
                $mes[$k]['messenger'] = isset($messenger) ? $messenger->messenger : '';
                $mes[$k]['active'] = $i === 0;
                if ($i === 0) {
                    $data_mes = MessegeModel::where('group_messenger_id', $v['id'])->get();
                    foreach ($data_mes as $key => $value) {
                        $data_mes[$key]['checked'] = 1;
                        $data_mes[$key]->save();
                        $data_mes[$key]['active'] = $value['user_send_id'] === $dataCustomer->id;
                    }
                    if (isset($shop_user)){
                        $data['name_customer_active'] = $shop_user->name ;
                        $data['avt_customer_active'] = isset($shop_user->image) ? $shop_user->image : 'Icon/chat/avarta.png';
                    }else{
                        $data['name_customer_active'] = isset($customer->name) ? $customer->name : $customer->phone ;
                        $data['avt_customer_active'] = isset($customer->image) ? $customer->image : 'Icon/chat/avarta.png';
                    }
                    $data['id_customer_active'] = $customer->id;
                    $data['group_messenger_id'] = $v['id'];
                    $data['list_messenger'] = $data_mes;
                    $data['group_id'] = $v['id'];
                }
                $i++;
            } else {
                unset($mes[$k]);
            }
        }
        $data['user_id'] = $dataCustomer->id;
        $data['list_mes'] = $mes;
        $data['my_avt'] = !empty($dataCustomer->image) ? $dataCustomer->image : 'Icon/chat/avarta.png';
        $data['title'] = 'Chat';
        return view('frontend.chat.index', $data);
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function sendMessage(Request $request)
    {
        $dataCustomer = session()->get('data_customer');
        $customer = ShopModel::find($request->user_id);
        $data_messenger = MessegeModel::where('user_receiver_id', $dataCustomer->id)->where('checked', 0)
            ->where('group_messenger_id', $request->get('group_messenger_id'))->get();
        foreach ($data_messenger as $key => $value){
            $data_messenger[$key]['checked'] = 1;
            $data_messenger[$key]->save();
        }
        $messenger_all = MessegeModel::where('user_receiver_id', $dataCustomer->id)->where('checked', 0)->count();
        $data['count'] = $messenger_all;
        $data['messenger'] = $request->messenger;
        $data['group_id'] = $request->group_messenger_id;
        $data['user_id'] = $request->user_id;
        $data['avt_customer'] = $customer->image ?? 'Icon/chat/avarta.png';
        $data['name_customer'] = $dataCustomer->name ?? '';
        $data['url_link'] = URL::to('chat/messenger/'.$request->group_messenger_id);

        event(new MyEvent($data));
        $messenger = new MessegeModel();
        $messenger->messenger = $request->messenger;
        $messenger->user_send_id = $dataCustomer->id;
        $messenger->group_messenger_id = $request->group_messenger_id;
        $messenger->user_receiver_id = $request->user_id;
        $messenger->user_send_avt = $dataCustomer->image ?? '';
        $messenger->user_receiver_avt = $customer->image ?? '';
        $messenger->checked = 0;
        $messenger->save();
        $group = GroupMessengerModel::find($request->group_messenger_id);
        $group->date = date('Y-m-d H:i:s');
        $group->save();
        return $data;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function messenger($id)
    {
        $dataCustomer = session()->get('data_customer');
        $mes = GroupMessengerModel::where('user_id_1', $dataCustomer->id)
            ->orWhere('user_id_2', $dataCustomer->id)
            ->orderBy('date', 'DESC')
            ->get();
        $data_messenger = MessegeModel::where('user_receiver_id', $dataCustomer->id)->where('checked', 0)->get();
        foreach ($data_messenger as $key => $value){
            $data_messenger[$key]['checked'] = 1;
            $data_messenger[$key]->save();
        }
        $data['list_messenger'] = [];
        $data['group_messenger_id'] = '';
        foreach ($mes as $k => $v) {
            if ($v['user_id_1'] === $dataCustomer->id){
                $user_id_2 = $v['user_id_2'];
            }else{
                $user_id_2 = $v['user_id_1'];
            }
            $data_user = CustomerModel::where('id', $user_id_2)->first();
            $ShopModel = ShopModel::where('customer_id', $user_id_2)->first();
            if (isset($ShopModel)){
                $mes[$k]['name_customer'] = $ShopModel->name;
                $mes[$k]['avt_customer'] = $ShopModel->image;
            }else{
                $mes[$k]['name_customer'] = $data_user->name;
                $mes[$k]['avt_customer'] = $data_user->image;
            }
            if (isset($data_user)) {
                $messenger = MessegeModel::where('group_messenger_id', $v['id'])->orderBy('created_at', 'DESC')->first();
                $mes[$k]['messenger'] = isset($messenger) ?  $messenger->messenger : '';
                $mes[$k]['active'] = (int)$v['id'] === (int)$id;
                if ((int)$v['id'] === (int)$id) {
                    $data_mes = MessegeModel::where('group_messenger_id', $v['id'])->get();
                    foreach ($data_mes as $key => $value) {
                        $data_mes[$key]['active'] = $value['user_send_id'] === $dataCustomer->id;
                    }

                    if($dataCustomer->id === $v['user_id_1']){
                        $id_customer_active = $v['user_id_2'];
                        $shop = ShopModel::where('customer_id',$id_customer_active)->first();
                        $data_customer = CustomerModel::where('id', $id_customer_active)->first();
                        if (isset($shop)){
                            $avt_customer_active = isset($shop->image) ? $shop->image : 'Icon/chat/avarta.png' ;
                            $name_customer_active = $shop->name;
                        }else{
                            $name_customer_active = $data_customer->name;
                            $avt_customer_active = isset($data_customer->image) ? $data_customer->image : 'Icon/chat/avarta.png' ;
                        }
                    }else{
                        $id_customer_active = $v['user_id_1'];
                        $shop = ShopModel::where('customer_id',$id_customer_active)->first();
                        $data_customer = CustomerModel::where('id', $id_customer_active)->first();
                        if (isset($shop)){
                            $avt_customer_active = isset($shop->image) ? $shop->image : 'Icon/chat/avarta.png' ;
                            $name_customer_active = $shop->name;
                        }else{
                            $name_customer_active = $data_customer->name;
                            $avt_customer_active = isset($data_customer->image) ? $data_customer->image : 'Icon/chat/avarta.png' ;
                        }
                    }
                    $data['name_customer_active'] = $name_customer_active;
                    $data['avt_customer_active'] = $avt_customer_active;
                    $data['id_customer_active'] = $id_customer_active;
                    $data['group_messenger_id'] = $v['id'];
                    $data['list_messenger'] = $data_mes;
                }
            } else {
                unset($mes[$k]);
            }
        }
        $data['group_id'] = $id;
        $data['user_id'] = $dataCustomer->id;
        $data['list_mes'] = $mes;
        $data['my_avt'] = !empty($dataCustomer->image) ? $dataCustomer->image : 'Icon/chat/avarta.png';
        return view('frontend.chat.index', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function messengerShop($id)
    {
        $dataCustomer = session()->get('data_customer');
        if ($id === $dataCustomer->id){
            return back();
        }
        if ($id === 0) {
            return redirect('');
        } else {
            $mes = GroupMessengerModel::where(function ($query) use ($dataCustomer, $id) {
                $query->where('user_id_1', $dataCustomer->id)
                    ->orWhere('user_id_2', $id);
            })->where(function ($query) use ($dataCustomer, $id) {
                $query->where('user_id_1', $id)
                    ->orWhere('user_id_2', $dataCustomer->id);
            })->first();
            if (!isset($mes)) {
                $mes = new GroupMessengerModel();
                $mes->user_id_1 = $dataCustomer->id;
                $mes->user_id_2 = $id;
                $mes->save();
            }
            return redirect('chat/messenger/'.$mes->id);
        }
    }
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function popupChatShop($id)
    {
        $dataCustomer = session()->get('data_customer');
        if ($id === 0) {
            return redirect('');
        } else {
            $mes = GroupMessengerModel::where(function ($query) use ($dataCustomer, $id) {
                $query->where('user_id_1', $dataCustomer->id)
                    ->where('user_id_2', $id);
            })->orWhere(function ($query) use ($dataCustomer, $id) {
                $query->where('user_id_1', $id)
                    ->where('user_id_2', $dataCustomer->id);
            })->first();
            if (empty($mes)) {
                $mes = new GroupMessengerModel();
                $mes->user_id_1 = $dataCustomer->id;
                $mes->user_id_2 = $id;
                $mes->save();
            }
            return redirect('chat/popup-chat/'.$mes->id);
        }
    }
    public function popupChat($id){
        $dataCustomer = session()->get('data_customer');
        $mes = GroupMessengerModel::where('user_id_1', $dataCustomer->id)
            ->orWhere('user_id_2', $dataCustomer->id)
            ->orderBy('date', 'DESC')
            ->get();
        $data['list_messenger'] = [];
        $data['group_messenger_id'] = '';
        foreach ($mes as $k => $v) {
            $data_shop_customer = ShopModel::where('customer_id', $dataCustomer->id)->first();
            if (isset($data_shop_customer)){
                $user_id = $dataCustomer->id;
            }else{
                $user_id = $dataCustomer->id === $v['user_id_1'] ? $v['user_id_2'] : $v['user_id_1'];
            }
            if ($v['user_id_1'] === $dataCustomer->id){
                $user_id_2 = $v['user_id_2'];
            }else{
                $user_id_2 = $v['user_id_1'];
            }
            $data_user = CustomerModel::where('id', $user_id_2)->first();
            $ShopModel = ShopModel::where('customer_id', $data_user->id)->first();
            if (isset($ShopModel)) {
                $mes[$k]['name_customer'] = $ShopModel->name;
                $mes[$k]['avt_customer'] = isset($ShopModel->image) ? $ShopModel->image : '/assets/frontend/Icon/products/shop_love.png';
                $messenger = MessegeModel::where('group_messenger_id', $v['id'])->orderBy('created_at', 'DESC')->first();
                $mes[$k]['messenger'] = isset($messenger) ?  $messenger->messenger : '';
                $mes[$k]['active'] = (int)$v['id'] === (int)$id;
                if ((int)$v['id'] === (int)$id) {
                    $data_mes = MessegeModel::where('group_messenger_id', $v['id'])->get();
                    foreach ($data_mes as $key => $value) {
                        $data_mes[$key]['active'] = $value['user_send_id'] === $dataCustomer->id;
                    }

                    if($dataCustomer->id === $v['user_id_1']){
                        $id_customer_active = $v['user_id_2'];
                        $data_customer = CustomerModel::where('id', $id_customer_active)->first();
                        $data_shop = ShopModel::where('customer_id', $id_customer_active)->first();
                        if (isset($data_shop)){
                            if (isset($data_shop->image)){
                                $avt_customer_active = $data_shop->image;
                            }else{
                                $avt_customer_active = 'Icon/chat/avarta.png';
                            }
                            $name_customer_active = $data_shop->name;
                        }else{
                            if (isset($data_customer->image)){
                                $avt_customer_active = $data_customer->image;
                            }else{
                                $avt_customer_active = 'Icon/chat/avarta.png';
                            }
                            $name_customer_active = $data_customer->name;
                        }
                    }else{
                        $id_customer_active = $v['user_id_1'];
                        $data_customer = CustomerModel::where('id', $id_customer_active)->first();
                        if (isset($data_customer->image)){
                            $avt_customer_active = $data_customer->image;
                        }else{
                            $avt_customer_active = 'Icon/chat/avarta.png';
                        }
                        $name_customer_active = $data_customer->name;
                    }
                    $data['name_customer_active'] = $name_customer_active;
                    $data['avt_customer_active'] = $avt_customer_active;
                    $data['id_customer_active'] = $id_customer_active;
                    $data['group_messenger_id'] = $v['id'];
                    $data['list_messenger'] = $data_mes;
                }
            } else {
                unset($mes[$k]);
                $mes[$k]['name_customer'] = $data_user->name;
            }
        }
        $data['group_id'] = $id;
        $data['user_id'] = $dataCustomer->id;
        $data['list_mes'] = $mes;
        $data['my_avt'] = !empty($dataCustomer->image) ? $dataCustomer->image : 'Icon/chat/avarta.png';
        return view('frontend.base.popup_chat', $data);
    }
}
