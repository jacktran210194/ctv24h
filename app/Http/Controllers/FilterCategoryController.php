<?php

namespace App\Http\Controllers;

use App\Models\ProductModel;
use App\Models\ReviewProductModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FilterCategoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $data_filter = ProductModel::query();
        foreach ($request->filter as $item => $value){
            if ($item == 'shipping_id'){
                $data_filter->join('product_shipping', 'product.id','=','product_shipping.product_id')
                    ->where('shipping_supplier_id', $value)->where('product.active', 1);
            }elseif ($item == 'ratings'){
                $data_filter->join('review_product','review_product.product_id','=','product.id')
                    ->where('ratings','>=', $value)->where('product.active', 1);
            }else{
                if ($item =='price_filter_1'){
                    $data_filter = $data_filter->where('price', '>=', $value)->where('active', 1);
                }elseif ($item == 'price_filter_2'){
                    $data_filter = $data_filter->where('price', '<=', $value)->where('active', 1);
                }else{
                    $data_filter = $data_filter->where($item, $value);
                }
            }
        }
        $data_filter = $data_filter->paginate(25);
        if (count($data_filter)) {
            foreach ($data_filter as $key => $value) {
                $total_rating = ReviewProductModel::where('product_id', $value->id)->avg('ratings') ?? 0;
                $avg_rating = round($total_rating,1);
                $data_filter[$key]->avg_rating = $avg_rating;
            }
        }
        $view = view('frontend.home.filter_category', ['dataSugesstionProduct' =>$data_filter])->render();
        return response()->json(['prod' => $view]);
    }
}
