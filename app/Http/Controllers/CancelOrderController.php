<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CancelOrderController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('frontend.CancelOrder.index');
    }
}
