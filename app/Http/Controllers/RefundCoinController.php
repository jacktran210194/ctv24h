<?php

namespace App\Http\Controllers;


use App\Models\VoucherRefundCoinModel;

class RefundCoinController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $data = VoucherRefundCoinModel::where('status', 1)->where('quantity','>',0)->get();
        $dataReturn = [
            'data' => $data,
            'title' => 'Hoàn Đơn Xu Bất Kỳ',
        ];
        return view('frontend.RefundCoin.index', $dataReturn);
    }
}
