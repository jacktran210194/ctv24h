<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class BaseController extends Controller
{
    public function __construct()
    {
        $dataCustomer = session()->get('data_customer');
        if (!isset($dataCustomer)) {
            Redirect::to('login')->send();
        }
    }
}
