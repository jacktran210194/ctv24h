<?php

namespace App\Http\Controllers;

use App\Models\ProductModel;
use App\Models\RatingProductModel;
use App\Models\SponsoredProductModel;
use App\Models\SupplierModel;
use Illuminate\Http\Request;
use DB;

class SponsoredProductController extends Controller
{
    public function index()
    {
        $data_supplier = SupplierModel::orderBy('id', 'desc')->get();
        $data_product = ProductModel::where('sponsored', 1)->get();
        $dataReturn = [
            'title' => 'Sản phẩm được tài trợ',
            'data_supplier' => $data_supplier,
            'data_product' => $data_product
        ];
        return view('frontend.SponsoredProduct.index', $dataReturn);
    }
}
