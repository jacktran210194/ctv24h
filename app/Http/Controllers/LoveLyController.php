<?php

namespace App\Http\Controllers;

use App\Models\FavouriteProductModel;
use App\Models\LovelyShopModel;
use App\Models\ProductModel;
use App\Models\ReviewProductModel;
use App\Models\ShopModel;
use Illuminate\Http\Request;
use DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class LoveLyController extends Controller
{
    public function __construct(Request $request)
    {
        if (!$request->session()->has('data_customer')) {
            Redirect::to('login')->send();
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $dataCustomer = session()->get('data_customer');
        $type = $request->type === 'product_lovely' ? 'product_lovely' : 'shop_lovely';

        $data_product_love = FavouriteProductModel::where('customer_id',$dataCustomer->id)->orderBy('id','desc')->paginate(5);
        if($data_product_love){
            foreach($data_product_love as $key => $value){
                $data_product = ProductModel::find($value->product_id);
                $data_product_love[$key]['product_image'] =  $data_product['image'];
                $data_product_love[$key]['product_price'] = $data_product['price'];
                $data_product_love[$key]['product_price_discount'] = $data_product['price_discount'];
                $data_product_love[$key]['product_name'] = $data_product['name'];
                $data_shop = ShopModel::find($data_product['shop_id']);
                $data_product_love[$key]['shop_name'] = $data_shop['name'];
                $data_product_love[$key]['shop_image'] = $data_shop['image'];
                $data_product_love[$key]['shop_id'] = $data_shop['id'];
            }
        }

        $data_love_shop = LovelyShopModel::where('customer_id', $dataCustomer->id)->orderBy('id', 'desc')->paginate(5);
        if($data_love_shop){
            foreach ($data_love_shop as $k => $v) {
                $data_shop = ShopModel::find($v->shop_id);
                $count_review_1 = ReviewProductModel::where('shop_id',$data_shop->id)->avg('ratings') ?? 0;
                $count_review = round($count_review_1,1) ?? 0;
                $data_love_shop[$k]['count_review'] = $count_review ?? 0;
                $data_love_shop[$k]['shop_image'] = $data_shop['image'];
                $data_love_shop[$k]['shop_name'] = $data_shop['name'];
                $data_product = ProductModel::where('shop_id', $v->shop_id)
                    ->where('active', 1)->count();
                $data_love_shop[$k]['count_product'] = $data_product;
            }
        }
        $data_return = [
            'type' => $type,
            'title' => 'Yêu thích',
            'dataLoveShop' => $data_love_shop,
            'dataFavouriteProduct' => $data_product_love,
        ];
        return view('frontend.love_ly.index', $data_return);
    }

    public function unlike_shop($id)
    {
        LovelyShopModel::destroy($id);
        return back();
    }

    /**
     *  Bỏ thích sản phẩm
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function dislike($id)
    {
        FavouriteProductModel::destroy($id);
        return back();
    }
}
