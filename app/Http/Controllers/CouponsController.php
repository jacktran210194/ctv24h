<?php

namespace App\Http\Controllers;

use App\Models\CategoryProductModel;
use App\Models\CollaboratorModel;
use App\Models\CustomerVoucherCtv24hModel;
use App\Models\SupplierModel;
use App\Models\VoucherCTV24hModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MongoDB\Driver\Monitoring\Subscriber;

class CouponsController extends Controller
{
    /**
     * Phiếu giảm giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $today = Carbon::now('Asia/Ho_Chi_Minh');
        $dataVoucherNCC = VoucherCTV24hModel::where('type', 0)->where('time_end', '>=', $today)->orderBy('id', 'desc')->limit(3)->get();
        $dataVoucherCTV = VoucherCTV24hModel::where('type', 1)->where('time_end', '>=', $today)->orderBy('id', 'desc')->limit(6)->get();
        if($dataVoucherCTV){
            $today = Carbon::now('Asia/Ho_Chi_Minh');
            foreach ($dataVoucherCTV as $k => $v){
                $dataCategory = CategoryProductModel::find($v->category_id);
                $dataVoucherCTV[$k]->category = $dataCategory->name ?? '';
                if($v->money_or_percent == 0){
                    $dataVoucherCTV[$k]->money_or_percent = 'đ';
                } else {
                    $dataVoucherCTV[$k]->money_or_percent = '%';
                }
            }
        }
        $dataVoucherCTVLeft = VoucherCTV24hModel::where('type', 1)->where('time_end', '>=', $today)->orderBy('time_end', 'desc')->first();
        $dataReturn = [
            'dataVoucherNCC' => $dataVoucherNCC,
            'dataVoucherCTV' => $dataVoucherCTV,
            'dataVoucherCTVLeft' => $dataVoucherCTVLeft,
            'title' => 'Phiếu giảm giá',
        ];
        return view('frontend.coupons.index', $dataReturn);
    }

    /**
     *  Lưu phiếu giảm giá
     * @param Request $request
     * @return array
     */
    public function saveCoupons(Request $request){
        $dataLogin = session()->get('data_customer');
        $dataSupplier = SupplierModel::where('customer_id', $dataLogin->id)->first();
        if(isset($dataSupplier)){
            $dataVoucherCus = CustomerVoucherCtv24hModel::where('customer_id', $dataLogin->id)
                ->where('voucher_id', $request->voucher_id)->first();
            if(!$dataVoucherCus){
                $data = CustomerVoucherCtv24hModel::create([
                    'customer_id' => $dataSupplier->customer_id,
                    'voucher_id' => $request->voucher_id
                ]);
                $dataReturn = [
                    'status' => true,
                    'msg' => 'Lưu mã thành công',
                    'url' => url('coupons')
                ];
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Mã đã được lưu',
                    'url' => url('coupons')
                ];
            }
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Vui lòng đăng nhập',
                'url' => url('login')
            ];
        }
        return $dataReturn;
    }

    public function saveCouponsCTVLeft(Request $request){
        $dataLogin = session()->get('data_customer');
        $dataCollaborator = CollaboratorModel::where('customer_id', $dataLogin->id)->first();
        if(isset($dataCollaborator)){
            $dataVoucherCus = CustomerVoucherCtv24hModel::where('customer_id', $dataLogin->id)
                ->where('voucher_id', $request->voucher_id)->first();
            if(!$dataVoucherCus){
                $data = CustomerVoucherCtv24hModel::create([
                    'customer_id' => $dataCollaborator->customer_id,
                    'voucher_id' => $request->voucher_id
                ]);
                $dataReturn = [
                    'status' => true,
                    'msg' => 'Lưu mã thành công',
                    'url' => url('coupons')
                ];
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Mã đã được lưu',
                    'url' => url('coupons')
                ];
            }
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => 'Vui lòng đăng nhập',
                'url' => url('login')
            ];
        }
        return $dataReturn;
    }
}
