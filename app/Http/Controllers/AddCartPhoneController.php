<?php

namespace App\Http\Controllers;

use App\Models\CardModel;
use App\Models\CategoryCardModel;
use App\Models\OrderCardModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class AddCartPhoneController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        switch ($request->type) {
            case 'buyCart':
                $type = 'buyCart';
                break;
            case 'buyCartGame':
                $type = 'buyCartGame';
                break;
            default:
                $type = 'upCard';
                break;
        }
        $data_category_card_phone = CategoryCardModel::where('type', 0)->get();
        $data_category_card_game = CategoryCardModel::where('type', 1)->get();
        $data_return = [
            'type' => $type,
            'category_card_phone' => $data_category_card_phone,
            'category_card_game' => $data_category_card_game,
            'title' => 'Nạp thẻ điện thoại, thanh toán cước'
        ];
        return view('frontend.AddCartPhone.index', $data_return);
    }

    public function filter($id)
    {
        $output = '';
        $data = DB::table('card')->select('card.price')->where('category_id', $id)->where('status', 0)->orderBy('price', 'asc')->distinct('price')->get();
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $row) {
                $output .= '
                          
                         <button id="btn-price" data-price="' . $row->price . '" class="price-denominations btn-price">
                                <p class="ct-denominations">' . number_format($row->price) . ' đ</p>
                                <p class="ct-price">Giá: ' . number_format($row->price) . ' đ</p>
                         </button>

                ';
            }
        } else {
            $output .= '
                        <h3>Không có dữ liệu</h3>
            ';
        }
        echo $output;
    }

    public function saveCardPhone(Request $request)
    {
        try {
            $rules = [
                'email' => 'required',
                'phone' => 'required',
            ];
            $customMessages = [
                'required' => 'Vui lòng điền đầy đủ thông tin.',
            ];
            $dataReturn = [];
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $messages = $validator->messages();
                $errors = $messages->all();
                $dataReturn['status'] = false;
                $dataReturn['msg'] = $errors[0];
            } else {
                $data_card = CardModel::where('price', $request->price)->where('category_id', $request->category)->where('status', 0)->first();
                $data_category_card = CategoryCardModel::find($data_card['category_id']);
                if (isset($data_category_card)) {
                    $name['category_name'] = $data_category_card['name'];
                }
                $data_category = new OrderCardModel();
                $data_category->customer_id = $request->customer_id;
                $data_category->email = $request->email;
                $data_category->phone = $request->phone;
                $data_category->payment = $request->payment;
                $data_category->card_id = $data_card['id'];
                $data_category->total_price = $data_card['price'];
                $data_category->save();
                $data_card->update(['status' => 1]);
                $email = $request->email;
                $data = [
                    'email' => $email,
                    'phone' => $request->phone,
                    'seri' => $data_card['seri'],
                    'code' => $data_card['code'],
                    'price' => $data_card['price'],
                    'category' => $name['category_name']
                ];
//            Mail::send('frontend.email_card_phone.index', $data, function ($message) use ($email) {
//                $message->from('ctv24h@gmail.com', 'CTV24h');
//                $message->to($email, 'Khách hàng');
//                $message->subject('Mua thẻ cào điện thoại - CTV24h');
//            });
                $dataReturn = [
                    'status' => true,
                    'msg' => 'Mua thẻ điện thoại thành công !',
                    'url' => URL::to('recharge-card-phone?type=buyCart'),
                ];
            }
            return $dataReturn;
        } catch (Exception $e) {
            $dataReturn = [
                'status' => false,
                'msg' => 'Đã có lỗi xảy ra !',
                'error' => $e
            ];
            return $dataReturn;
        }

    }

    //Mua thẻ game
    public function filter_game($id)
    {
        $output = '';
        $data = DB::table('card')->select('card.price')->where('category_id', $id)->where('status', 0)->orderBy('price', 'asc')->distinct('price')->get();
        $total_row = $data->count();
        if ($total_row > 0) {
            foreach ($data as $row) {
                $output .= '
                          
                         <button data-price_game="' . $row->price . '" class="price-denominations btn-price-game">
                                <p class="ct-denominations">' . number_format($row->price) . ' đ</p>
                                <p class="ct-price">Giá: ' . number_format($row->price) . ' đ</p>
                         </button>

                ';
            }
        } else {
            $output .= '
                        <h3>Không có dữ liệu</h3>
            ';
        }
        echo $output;
    }

    public function saveCardGame(Request $request)
    {
        $rules = [
            'email' => 'required',
            'phone' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            $data_card = CardModel::where('price', $request->price)->where('category_id', $request->category)->where('status', 0)->first();
            $data_category_card = CategoryCardModel::find($data_card['category_id']);
            if (isset($data_category_card)) {
                $name['category_name'] = $data_category_card['name'];
            }
            $data_category = new OrderCardModel();
            $data_category->customer_id = $request->customer_id;
            $data_category->email = $request->email;
            $data_category->phone = $request->phone;
            $data_category->card_id = $data_card['id'];
            $data_category->total_price = $data_card['price'];
            $data_category->payment = $request->payment;
            $data_category->save();
            $data_card->update(['status' => 1]);
            $email = $request->email;
            $data = [
                'email' => $email,
                'phone' => $request->phone,
                'seri' => $data_card['seri'],
                'code' => $data_card['code'],
                'price' => $data_card['price'],
                'category' => $name['category_name']
            ];
//            Mail::send('frontend.email_card_phone.index', $data, function ($message) use ($email) {
//                $message->from('ctv24h@gmail.com', 'CTV24h');
//                $message->to($email, 'Thẻ cào - CVT24h');
//                $message->subject('Mua thẻ cào điện thoại - CTV24h');
//            });
            $dataReturn = [
                'status' => true,
                'msg' => 'Mua thẻ game thành công !',
                'url' => URL::to('recharge-card-phone?type=buyCartGame'),
            ];
        }
        return $dataReturn;
    }
}
