<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AdminBaseController extends Controller
{
    public function __construct(Request $request)
    {
        if (!$request->session()->has('data_user')) {
            Redirect::to('admin/login')->send();
        }

        $session = $request->session()->get('data_user');
        if ($session->supplier_admin){
            Redirect::to('admin/supplier_admin/login')->send();
        }

        if ($session->collaborator_admin){
            Redirect::to('admin/collaborator_admin/login')->send();
        }
    }
}
