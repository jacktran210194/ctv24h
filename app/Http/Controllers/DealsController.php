<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DealsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $day = 28;
        $active = false;
        $date = [];
        $check = 0;
        $data_check = [];
        for ($i = 0; $i < 35; $i++) {
            if ($day + 1 > 32) {
                $day = 1;
                $active = true;
            }
            $data = [
                'active' => $active,
                'day' => $day < 10 ? '0' . $day : $day,
                'active_day' => $day === 25 ? true : false
            ];
            $data_check[] = $data;
            $check++;
            $day++;
            if ($check === 7) {
                $date[] = $data_check;
                $check = 0;
                $data_check = [];
            }
        }
        $data_return = [
          'date' => $date
        ];
        return view('frontend.deals.index', $data_return);
    }
}
