<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CollaboratorBaseController extends Controller
{
    public function __construct(Request $request)
    {
        if (!$request->session()->has('data_collaborator')) {
            Redirect::to('admin/collaborator_admin/login')->send();
        }
        $session = $request->session()->get('data_collaborator');
        if(!$session->collaborator_admin){
            Redirect::to('admin/login')->send();
        } else {
            $request->session()->put('data_collaborator', $session);
        }
    }
}
