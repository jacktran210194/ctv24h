<?php

namespace App\Http\Controllers;

use App\Models\CustomerModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class WelcomeCTV24H extends Controller
{

    //Profile
    public function index()
    {
        return view('frontend.WelcomeCTV24H.index');
    }
}
