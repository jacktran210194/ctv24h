<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Admin\Supplier\OrderController;
use App\Models\AttributeValueModel;
use App\Models\CategoryCardModel;
use App\Models\NotificationCTV24Model;
use App\Models\NotificationCustomerModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductAttributeModel;
use App\Models\ProductModel;
use App\Models\ReviewProductModel;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class NotificationController extends BaseController
{
    /**
     *  Thông báo cập nhật đơn hàng
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateOrder()
    {
        $data_login = session()->get('data_customer');
        if (!$data_login) {
            return redirect('login');
        } else {
            $dataOrder = OrderModel::where(['customer_id' => $data_login['id'],'is_selected' => 1])->orderBy('created_at','desc')->paginate(5);
            foreach ($dataOrder as $k => $v) {
                $data_order_detail = OrderDetailModel::where('order_id',$v['id'])->first();
                $data_product = ProductModel::find($data_order_detail['product_id']);
                $dataOrder[$k]['image_product'] = $data_product['image'];
                $dataOrder[$k]->created_at = date('H:i:s d-m-Y', strtotime($v->created_at));
                if ($v->status == 0) {
                    $dataOrder[$k]->content = 'Chờ xác nhận';
                } elseif ($v->status == 1) {
                    $dataOrder[$k]->content = 'Chờ lấy hàng';
                } elseif ($v->status == 2) {
                    $dataOrder[$k]->content = 'Đang giao hàng';
                } elseif ($v->status == 3) {
                    $dataOrder[$k]->content = 'Đã giao hàng';
                } elseif ($v->status == 4) {
                    $dataOrder[$k]->content = 'Đã huỷ';
                } else {
                    $dataOrder[$k]->content = 'Hoàn tiền';
                }

                $check_notification = NotificationCustomerModel::where(['customer_id' => $data_login['id'], 'order_id' => $v->id])->first();
                if($check_notification){
                    $dataOrder[$k]->is_read = true;
                }else{
                    $dataOrder[$k]->is_read = false;
                }
            }
            $dataReturn = [
                'title' => 'Thông báo cập nhật đơn hàng',
                'data' => $dataOrder
            ];
            return view('frontend.notification.update_order', $dataReturn);

        }
    }

    public function getPromotion()
    {
        return view('frontend.notification.promotion');
    }

    public function updateWallet()
    {
        return view('frontend.notification.update_wallet');
    }

    public function getOnline()
    {
        return view('frontend.notification.online');
    }

    /**
     *  Thông báo đánh giá
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *
     */
    public function updateReview()
    {
        $data_login = session()->get('data_customer');
        if (!$data_login) {
            return redirect('login');
        } else {
            $data_review = ReviewProductModel::where('customer_id', $data_login->id)->orderBy('created_at', 'desc')->get();
            foreach ($data_review as $k => $v) {
                $data_review[$k]['date'] = date('H:i:s d-m-Y', strtotime($v->created_at));
                $data_product = ProductModel::find($v->product_id);
                $data_review[$k]['product_image'] = $data_product->image ?? '';
                $data_review[$k]['product_name'] = $data_product->name ?? '';
            }
            $dataReturn = [
                'title' => 'Thông báo đánh giá',
                'data' => $data_review
            ];
            return view('frontend.notification.update_review', $dataReturn);
        }
    }

    /**
     *  Thông báo sàn CTV24
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function updateFloor()
    {
        $data = NotificationCTV24Model::orderBy('created_at', 'desc')->get();
        $dataReturn = [
            'data' => $data
        ];
        return view('frontend.notification.update_floor', $dataReturn);
    }

    //Xem lại đánh giá
    public function return_review_product($id)
    {
        $data = ReviewProductModel::find($id);
        $data_product = ProductModel::find($data->product_id);
        $data['product_image'] = $data_product->image;
        $data['product_name'] = $data_product->name;
        $dataReturn = [
            'status' => true,
            'data' => $data
        ];
        return view('frontend.notification.return_review_product', $dataReturn);
    }

    //cập nhật đánh giá
    public function save(Request $request)
    {
        $data_review = ReviewProductModel::find($request->id);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            File::delete($data_review->image_review);
            File::delete($data_review->video_review);
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/review_product/';
            if (strpos($fileName, ".mp4")) {
                $data_review->video_review = '/uploads/images/review_product/' . $fileName;
            } else {
                $data_review->image_review = '/uploads/images/review_product/' . $fileName;
            }
            $file->move($path, $fileName);
            $data_review->save();
        }
        $data_review->ratings = $request->ratings;
        $data_review->customer_comment = $request->customer_comment;
        $data_review->comment = $request->comment;
        $data_review->is_image = 1;
        $data_review->status = 1;
        $data_review->save();

        $data_product = ProductModel::find($data_review['product_id']);
        $data_product->is_review += 1;
        $data_product->save();
        $dataReturn = [
            'status' => true,
            'msg' => 'Cập nhật đánh giá thành công !',
            'url' => url('notification/update_review'),
        ];
        return $dataReturn;
    }
}
