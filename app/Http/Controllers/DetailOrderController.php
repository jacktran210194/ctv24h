<?php

namespace App\Http\Controllers;

use App\Models\AttributeValueModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductAttributeModel;
use App\Models\ProductModel;
use App\Models\ShopModel;
use Illuminate\Http\Request;
use DB;

class DetailOrderController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
//        $data_login = session()->get('data_customer');
//        if (!$data_login) {
//            return redirect('');
//        }
//        $data_order_check = session()->get('data_order_check');
//        if (isset($data_order_check)) {
//            $dataOrder = OrderModel::where('order_code', $data_order_check['order_code'])->first();
//            if ($dataOrder['status'] == 0) {
//                $dataOrder['is_status'] = 'Chờ xác nhận';
//            } elseif ($dataOrder['status'] == 1) {
//                $dataOrder['is_status'] = 'Chờ lấy hàng';
//            } elseif ($dataOrder['status'] == 2) {
//                $dataOrder['is_status'] = 'Đang giao hàng';
//            } elseif ($dataOrder['status'] == 3) {
//                $dataOrder['is_status'] = 'Đã giao hàng';
//            } elseif ($dataOrder['status'] == 4) {
//                $dataOrder['is_status'] = 'Đã huỷ';
//            } else {
//                $dataOrder['is_status'] = 'Hoàn tiền';
//            }
//            $dataOrder['customer_name'] = $data_login['name'];
//            $dataOrder['phone'] = $data_login['phone'];
//
//            $data_order_detail = OrderDetailModel::where('order_id',$dataOrder['id'])->get();
//            foreach($data_order_detail as $k => $v){
//                $data_shop = ShopModel::find($v['shop_id']);
//                $data_order_detail[$k]['shop_name'] = $data_shop['name'];
//                $data_product = ProductModel::find($v['product_id']);
//                $data_order_detail[$k]['product_name'] = $data_product['name'];
//                $data_order_detail[$k]['product_image'] = $data_product['image'];
//                $data_order_detail[$k]['price_product'] = $data_product['price_discount'];
//                $dataAttr = $this->getDataVlueAttr($v->type_product);
//                $data_order_detail[$k]['type_product'] = $dataAttr;
//            }
            $dataReturn = [
                'status' => true,
                'title' => 'Kiểm tra đơn hàng',
//                'data' => $dataOrder,
//                'data_order' => $data_order_detail
            ];
//        } else {
//            $dataReturn = [
//                'status' => false,
//                'msg' => 'Đã có lỗi xảy ra'
//            ];
//        }
        return view('frontend.DetailOrder.index', $dataReturn);
    }

//    protected function getDataVlueAttr($value)
//    {
//        $item = explode(',', $value);
//        $data = [];
//        if(isset($item)){
//            foreach ($item as $v) {
//                $__dataAttrValue = AttributeValueModel::find($v);
//                if (isset($__dataAttrValue)) {
//                    $__dataAttrProduct = ProductAttributeModel::find($__dataAttrValue->attribute_id);
//                    $__data['type'] = isset($__dataAttrProduct) ? $__dataAttrProduct['name'] : '';
//                    $__data['value'] = isset($__dataAttrValue) ? $__dataAttrValue['value'] : '';
//                    array_push($data, $__data);
//                }
//            }
//        }
//        return $data;
//    }
}
