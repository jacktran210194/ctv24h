<?php

namespace App\Http\Controllers;

use App\Models\CustomerModel;
use App\Models\OrderDetailModel;
use App\Models\ProductModel;
use App\Models\ReviewProductModel;
use App\Models\ShopModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ReviewProductController extends Controller
{

    //Profile
//    public function index()
//    {
//
//        return view('frontend.my_history.by_product');
//    }

    public function review(Request $request)
    {
        $data_order_detail = OrderDetailModel::find($request->order_detail_id);
        $data_login = session()->get('data_customer');
        $rules = [
            'comment' => 'required',
        ];
        $customMessages = [
            'required' => 'Vui lòng điền đầy đủ thông tin.',
        ];
        $dataReturn = [];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $errors = $messages->all();
            $dataReturn['status'] = false;
            $dataReturn['msg'] = $errors[0];
        } else {
            if (!$request->hasFile('image')) {
                $data_category = ReviewProductModel::create([
                    'customer_id' => $data_login['id'],
                    'product_id' => $request->product_id,
                    'ratings' => $request->ratings_star,
                    'comment' => $request->comment,
                    'image_review' => '',
                    'video_review' => '',
                    'name_customer' => $data_login['name'],
                    'avatar_customer' => $data_login['image'],
                    'order_detail_id' => $request->order_detail_id,
                    'is_image' => 0,
                    'customer_comment' => $request->customer_comment,
                    'category_product' => $request->category_product,
                    'status' => 0,
                    'shop_id' => $data_order_detail['shop_id'],
                    'order_id' => $request->order_id
                ]);
                if ($data_category->image_review != null || $data_category->video_review != null) {
                    $data_category->is_image = 1;
                    $data_category->save();
                }
                $dataReturn = [
                    'status' => true,
                    'msg' => 'Đánh giá thành công',
                    'url' => URL::to('my_history/by_product?status=shipped'),
                ];
                return $dataReturn;
            }
            $file = $request->file('image');
            if (!$file->isValid()) {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'File tải lên không hợp lệ'
                ];
                return $dataReturn;
            }
            $data_category = new ReviewProductModel([
                'customer_id' => $data_login['id'],
                'product_id' => $request->product_id,
                'ratings' => $request->ratings_star,
                'comment' => $request->comment,
                'image_review' => '',
                'video_review' => '',
                'name_customer' => $data_login['name'],
                'avatar_customer' => $data_login['image'],
                'order_detail_id' => $request->order_detail_id,
                'is_image' => 0,
                'customer_comment' => $request->customer_comment,
                'category_product' => $request->category_product,
                'status' => 0,
                'shop_id' => $data_order_detail['shop_id'],
                'order_id' => $request->order_id
            ]);
            $data_category->save();
            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/uploads/images/review_product/';
            if (strpos($fileName, ".mp4")) {
                $data_category->video_review = '/uploads/images/review_product/' . $fileName;
            } else {
                $data_category->image_review = '/uploads/images/review_product/' . $fileName;
            }
            $file->move($path, $fileName);
            $data_category->save();
//            $item = json_decode($request->image);
//            $content = json_encode($request->image);
//            if (isset($item)){
//                foreach($item as $value){
//                    $path = public_path() . '/uploads/images/review_product/';
//                    $fileName = Str::random(20) . '.' . $value->getClientOriginalExtension();
//                    $file->move($path, $fileName);
//                }
//            }
//            if (isset($content)) {
//                $data_category->image_review = $content;
//                $data_category->save();
            if ($data_category->image_review != null || $data_category->video_review != null) {
                $data_category->is_image = 1;
                $data_category->save();
            }
//            }

            $data_product = ProductModel::find($data_category['product_id']);
            $data_product->is_review += 1;
            $data_product->save();
            $dataReturn = [
                'status' => true,
                'msg' => 'Đánh giá thành công',
                'url' => URL::to('my_history/by_product'),
            ];
        }
        return $dataReturn;
    }
}
