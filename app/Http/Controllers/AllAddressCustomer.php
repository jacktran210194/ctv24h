<?php

namespace App\Http\Controllers;

use App\Models\CityModel;
use App\Models\CustomerAddressModel;
use App\Models\DistrictModel;
use App\Models\WardModel;
use Illuminate\Http\Request;

class AllAddressCustomer extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index( Request $request )
    {

        $dataCustomer = session()->get('data_customer');
        $address = CustomerAddressModel::where('customer_id', $dataCustomer->id)->get();
        if (count($address)) {
            foreach ($address as $k => $v) {
                $city = CityModel::find($v['city_id']);
                $district = DistrictModel::find($v['district_id']);
                $ward = WardModel::where('ward_id', $v['ward_id'])->first();
                $address[$k]['full_address'] = $v['address'];
            }
        }
        $data['address'] = $address;

        return view('frontend.AllAddressCustomer.index' ,$data);
    }
}
