<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FlashSaleController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('frontend.flashsale.index');
    }
}
