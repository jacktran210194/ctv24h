<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryGHTKModel extends Model
{
    use HasFactory;

    protected $table = 'delivery_gttk';

    protected $fillable = ['api_token_key'];
}
