<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeenProductModel extends Model
{
    use HasFactory;
    protected $table = 'seen_product';
    protected $guarded = [];
}
