<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MethodZaloPayModel extends Model
{
    use HasFactory;

    protected $table = 'method_zalo_pay';

    protected $fillable = ['muid', 'maccesstoken'];
}
