<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupMessengerModel extends Model
{
    use HasFactory;

    protected $table = 'group_messenger';

    protected $fillable = [
        'user_id_1', 'user_id_2', 'date'
    ];
}
