<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationCustomerModel extends Model
{
    use HasFactory;
    protected $table = 'notification_customer';
    protected $guarded = [];
}
