<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CollaboratorModel extends Model
{
    use HasFactory;
    protected $table = 'collaborator';
    protected $guarded = [];
}
