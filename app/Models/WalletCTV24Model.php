<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletCTV24Model extends Model
{
    use HasFactory;
    protected $table = 'wallet_ctv24h';
    protected $guarded = [];
}
