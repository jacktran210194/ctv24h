<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategoryProductChildModel extends Model
{
    use HasFactory;
    protected $table = 'category_product_sub_child';
    protected $guarded = [];
}
