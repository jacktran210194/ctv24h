<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryOrderPaymentCollaboratorModel extends Model
{
    use HasFactory;
    protected $table = 'history_order_payment_collaborator';
    protected $guarded = [];
}
