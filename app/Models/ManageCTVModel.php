<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManageCTVModel extends Model
{
    use HasFactory;
    protected $table = 'manage_ctv';
    protected $guarded = [];
}
