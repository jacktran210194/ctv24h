<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherCategoryModel extends Model
{
    use HasFactory;
    protected $table = 'voucher_category';
    protected $fillable = [
        'name',
        'image',
        'active',
        'supplier_id'
    ];
}
