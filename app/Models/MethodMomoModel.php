<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MethodMomoModel extends Model
{
    use HasFactory;

    protected $table = 'method_momo';

    protected $fillable = ['partnerCode', 'accessKey'];
}
