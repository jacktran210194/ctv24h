<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingSupplierModel extends Model
{
    use HasFactory;
    protected $table = 'shipping_supplier';
    protected $guarded = [];
}
