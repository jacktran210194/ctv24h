<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryDealCTV24 extends Model
{
    use HasFactory;
    protected $table = 'history_deal_ctv24h';
    protected $guarded = [];
}
