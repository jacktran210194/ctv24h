<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JAndTModel extends Model
{
    use HasFactory;

    protected $table = 'delivery_jandt';

    protected $fillable = ['api_token'];
}
