<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddressCustomerCTVModel extends Model
{
    use HasFactory;
    protected $table = 'address_customer_ctv';
    protected $guarded = [];
    public function province1(){
        return $this->belongsTo(CityModel::class,'city');
    }
    public function district1(){
        return $this->belongsTo(DistrictModel::class,'district');
    }
    public function ward1(){
        return $this->belongsTo(WardModel::class,'ward');
    }
}
