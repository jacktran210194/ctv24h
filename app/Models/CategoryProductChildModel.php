<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryProductChildModel extends Model
{
    use HasFactory;
    protected $table = 'category_product_child';
    protected $guarded = [];
}
