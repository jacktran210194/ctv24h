<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FavouriteProductModel extends Model
{
    use HasFactory;
    protected $table = 'favourite_product';
    protected $guarded = [];
}
