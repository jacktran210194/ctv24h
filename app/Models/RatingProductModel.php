<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RatingProductModel extends Model
{
    use HasFactory;
    protected $table = 'rating_product';
    protected $guarded = [];
}
