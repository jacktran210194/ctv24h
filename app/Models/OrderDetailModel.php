<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetailModel extends Model
{
    use HasFactory;
    protected $table = 'order_detail';
    protected $guarded = [];
    public function product(){
        return $this->belongsTo(ProductModel::class,'product_id');
    }
}
