<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderCTVModel extends Model
{
    use HasFactory;
    protected $table = 'order_ctv';
    protected $guarded = [];
}
