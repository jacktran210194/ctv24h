<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageFileModel extends Model
{
    use HasFactory;
    protected $table = 'file_image';
    protected $guarded = [];
}
