<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LovelyShopModel extends Model
{
    use HasFactory;
    protected $table = 'lovely_shop';
    protected $guarded = [];
}
