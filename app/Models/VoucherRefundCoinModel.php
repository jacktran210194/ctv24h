<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherRefundCoinModel extends Model
{
    use HasFactory;
    protected $table = 'voucher_refund_coin_table';
    protected $guarded = [];
}
