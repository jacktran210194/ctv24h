<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerVoucherCtv24hModel extends Model
{
    use HasFactory;
    protected $table = 'customer_voucher_ctv24h';
    protected $guarded = [];
}
