<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeeCTV24HModel extends Model
{
    use HasFactory;
    protected $table = 'fee_ctv24h';
    protected $guarded = [];
}
