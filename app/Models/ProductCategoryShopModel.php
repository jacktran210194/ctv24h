<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategoryShopModel extends Model
{
    use HasFactory;
    protected $table = 'product_category_shop';
    protected $guarded = [];
}
