<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrendingProductModel extends Model
{
    use HasFactory;
    protected $table = 'trending_product';
    protected $guarded = [];
}
