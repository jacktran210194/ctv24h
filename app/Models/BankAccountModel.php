<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankAccountModel extends Model
{
    use HasFactory;
    protected $table = 'bank_account';
    protected $guarded = [];
}
