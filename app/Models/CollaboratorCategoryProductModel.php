<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CollaboratorCategoryProductModel extends Model
{
    use HasFactory;
    protected $table = 'collaborator_category_products';
    protected $guarded = [];
}
