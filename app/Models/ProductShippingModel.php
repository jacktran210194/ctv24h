<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductShippingModel extends Model
{
    use HasFactory;
    protected $table = 'product_shipping';
    protected $guarded = [];
}
