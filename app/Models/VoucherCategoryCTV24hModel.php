<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherCategoryCTV24hModel extends Model
{
    use HasFactory;
    protected $table = 'voucher_category_ctv24h';
    protected $guarded = [];
}
