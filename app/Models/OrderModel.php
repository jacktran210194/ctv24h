<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{

    // status = 0 wait confirm
    // status = 1 wait take goods
    // status = 2 delivery took goods
    // status = 3 delivered
    // status = 4 and cancel_confirm =1 cancel order
    // status = 4 and cancel_confirm =0 request cancel order
    const CANCEL_STATUS = 4;
    const DELIVERED_STATUS = 3;
    const DELIVERING_STATUS = 2;
    const WAIT_DELIVERY_STATUS = 1;

    const  CANCEL_ORDER = -1;
    const  NOT_DELIVERY_ORDER = 49; //Shipper báo không giao được giao hàng
    const  DELAY_DELIVERY_ORDER = 410; //Shipper báo delay giao hàng
    const  DELIVERED_ORDER = 45; //Shipper báo đã giao hàng
    const  DELIVERING_ORDER = 123; // shiper đã lấy hàng
    const  RETURN_ORDER = 13; //Đơn hàng bồi hoàn

    use HasFactory;
    protected $table = 'order';
    protected $guarded = [];
    protected $fillable = [
        'order_code',
        'shop_id',
        'is_selected',
        'status',
        'code_delivery',
        'partner_id',
        'label',
        'tracking_id',
        'sorting_code',
        'customer_id',
        'total_price',
        'date',
        'address',
        'note',
        'collaborator_id',
        'supplier_id',
        'cod',
        'shipping_id',
        'delivery_method',
        'payment_id',
        'bonus',
        'cancel_confirm',
        'cancel_reason',
        'time_pick',
        'address_pick',
        'form_shipping',
        'name_address_pick',
        'phone_address_pick',
        'address_pick_id',
        'cancel_user',
        'status_confirm',
        'shipping_confirm',
        'total_price_product',
        'fee_ctv24h',
        'price_supplier',
        'address_ctv',
    ];
    public function shop()
    {
        return $this->belongsTo(ShopModel::class);
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetailModel::class, 'order_id');
    }

    public function address()
    {
        return $this->belongsTo(AddressCustomerCTVModel::class, 'address_ctv');
    }
}
