<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SponsoredProductModel extends Model
{
    use HasFactory;
    protected $table = 'sponsored_product';
    protected $guarded = [];
}
