<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerAddressModel extends Model
{
    use HasFactory;

    protected $table = 'customer_address';

    protected $fillable = [
        'city_id',
        'district_id',
        'ward_id',
        'name',
        'phone',
        'address_maps',
        'address',
        'longitude',
        'latitude',
        'default',
        'customer_id',
        'type_pick_up',
        'type_return',
        'supplier_id'
    ];
    protected $guarded = [];
    public function province(){
        return $this->belongsTo(CityModel::class,'city_id');
    }
    public function district(){
        return $this->belongsTo(DistrictModel::class,'district_id');
    }
    public function ward(){
        return $this->belongsTo(WardModel::class,'ward_id','ward_id');
    }
}
