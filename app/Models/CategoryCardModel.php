<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryCardModel extends Model
{
    use HasFactory;
    protected $table = 'category_card';
    protected $guarded = [];
}
