<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherCTV24hModel extends Model
{
    use HasFactory;
    protected $table = 'voucher_ctv24h';
    protected $guarded = [];
}
