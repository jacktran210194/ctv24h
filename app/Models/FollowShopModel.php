<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FollowShopModel extends Model
{
    use HasFactory;
    protected $table = 'follow_shop';
    protected $guarded = [];
}
