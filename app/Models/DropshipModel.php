<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DropshipModel extends Model
{
    use HasFactory;
    protected $table = 'dropship';
    protected $guarded = [];
}
