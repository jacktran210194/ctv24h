<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddressSupplierModel extends Model
{
    use HasFactory;
    protected $table = 'address_supplier';
    protected $guarded = [];
}
