<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessegeModel extends Model
{
    use HasFactory;

    protected $table = 'messenger';

    protected $fillable = ['messenger', 'user_send_id', 'group_messenger_id', 'user_receiver_id', 'user_send_avt', 'user_receiver_avt', 'checked'];
}
