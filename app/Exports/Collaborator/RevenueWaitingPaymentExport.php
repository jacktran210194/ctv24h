<?php

namespace App\Exports\Collaborator;

use App\Models\AddressCustomerCTVModel;
use App\Models\CustomerModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use DB;
class RevenueWaitingPaymentExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        $dataUser = session()->get('data_collaborator');
        return $dataReceived = OrderModel::where('status_confirm', 0)
            ->where('customer_id', $dataUser->customer_id)
            ->where('collaborator_id', 1)
            ->where('is_selected', 1)
            ->where('status', '<=', 3)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function headings(): array
    {
        return [
            'Loại doanh thu',
            'Đơn hàng',
            'Người mua',
            'Ngày thanh toán',
            'Trạng thái',
            'Số tiền'
        ];
    }

    public function map($value): array
    {
        $dataOrderDetail = OrderDetailModel::where('order_id', $value->id)->first();
        $dataProduct = ProductModel::find($dataOrderDetail->product_id);
        $value->name_product = $dataProduct->name ?? '';
        $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $value->id)->first();
        $value->customer_name = $dataCustomerCTV->name ?? '';

        return [
            'Hoa hồng',
            $value->name_product,
            $value->customer_name,
            $value->created_at,
            'Chờ người mua xác nhận đã nhận hàng',
            $value->bonus,
        ];
    }
}
