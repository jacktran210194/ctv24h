<?php

namespace App\Exports\Collaborator;

use App\Models\AddressCustomerCTVModel;
use App\Models\CustomerModel;
use App\Models\HistoryOrderPaymentCollaboratorModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\ProductModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use DB;
class RevenueDonePaymentExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        $dataUser = session()->get('data_collaborator');
        return $data = HistoryOrderPaymentCollaboratorModel::where('customer_id', $dataUser->customer_id)
            ->where('type', 1)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function headings(): array
    {
        return [
            'Loại doanh thu',
            'Đơn hàng',
            'Người mua',
            'Ngày thanh toán',
            'Trạng thái',
            'Số tiền'
        ];
    }

    public function map($value): array
    {
        $dataCustomerCTV = AddressCustomerCTVModel::where('order_id', $value->order_id)->first();
        $value->name_customer = $dataCustomerCTV->name ?? '';
        $dataOrder = OrderModel::find($value->order_id);
        $dataOrderDetail = OrderDetailModel::where('order_id', $dataOrder->id)->first();
        $dataProduct = ProductModel::find($dataOrderDetail->product_id);
        $value->name_product = $dataProduct->name ?? '';
        return [
            'Bán hàng',
            $value->name_product,
            $value->name_customer,
            $value->created_at,
            'Đã nhận được hàng',
            $value->price,
        ];
    }
}
