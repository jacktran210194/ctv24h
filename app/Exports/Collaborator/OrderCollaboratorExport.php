<?php

namespace App\Exports\Collaborator;

use App\Models\AddressCustomerCTVModel;
use App\Models\OrderModel;
use App\Models\ShippingModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class OrderCollaboratorExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function collection()
    {
        $dataLogin = session()->get('data_collaborator');
        if ($dataLogin) {
            return OrderModel::where('customer_id', $dataLogin->customer_id)
                ->where('is_selected', 1)
                ->where('collaborator_id', 1)
                ->orderBy('id', 'desc')
                ->get();
        }
    }

    public function headings(): array
    {
        return [
            'Mã vận đơn',
            'Họ tên khách hàng',
            'Ngày đặt hàng',
            'Tổng tiền',
            'Phương thức thanh toán',
            'Vận chuyển',
            'Trạng thái',
        ];
    }

    public function map($value): array
    {
        $dataAddressCustomer = AddressCustomerCTVModel::where('order_id', $value->id)->first();
        $value->name_customer = $dataAddressCustomer->name ?? '';
        if ($value['payment_id'] == 0) {
            $value['payment'] = 'Ví CTV24H';
        } elseif ($value['payment_id'] == 1) {
            $value['payment'] = 'Trả góp';
        } elseif ($value['payment_id'] == 2){
            $value['payment'] = 'Thanh toán khi nhận hàng';
        } elseif ($value['payment_id'] == 3) {
            $value['payment'] = 'Thẻ ATM nội địa/ Internet Banking';
        } elseif ($value['payment_id'] == 4) {
            $value['payment'] = 'CTV24H Pay';
        } else {
            $value['payment'] = 'Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB';
        }

        if ($value['status'] == 0) {
            $value['status'] = 'Chờ xác nhận';
        } elseif ($value['status'] == 1) {
            $value['status'] = 'Chờ lấy hàng';
        } elseif ($value['status'] == 2) {
            $value['status'] = 'Đang giao';
        } elseif ($value['status'] == 3) {
            $value['status'] = 'Đã giao';
        } elseif ($value['status'] == 4) {
            $value['status'] = 'Đã huỷ';
        } else {
            $value['status'] = 'Hoàn tiền';
        }

        $dataShipping = ShippingModel::find($value->shipping_id);
        $value->shipping = $dataShipping->name ?? '';
        $value['date'] = date('d-m-Y H:i:s', strtotime($value['date']));
        return [
            $value->order_code,
            $value->name_customer,
            $value->date,
            $value->total_price,
            $value->payment,
            $value->shipping,
            $value->status
        ];
    }
}
