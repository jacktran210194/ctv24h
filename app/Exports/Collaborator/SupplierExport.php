<?php

namespace App\Exports\Collaborator;

use App\Models\DropshipModel;
use App\Models\OrderDetailModel;
use App\Models\OrderModel;
use App\Models\SupplierModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use DB;
class SupplierExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        $data_user = session()->get('data_collaborator');
        return DB::table('shop')
            ->select('order.shop_id as shop_id', 'supplier.id as id_supplier', 'shop.name as name_shop')
            ->distinct('shop_id')
            ->join('order', 'shop.id', '=', 'order.shop_id')
            ->join('supplier', 'shop.supplier_id', 'supplier.id')
            ->where('order.customer_id', $data_user->customer_id)
            ->where('order.collaborator_id', 1)
            ->get();
    }

    public function headings(): array
    {
        return [
            'Tên shop NCC',
            'Số đơn đã Dropship',
            'Tổng doanh thu bán ra',
            'Lợi nhuận thu về',
        ];
    }

    public function map($value): array
    {
        foreach ($value as $k => $v)
        {
            $value->count = 0;
            $value->cod = 0;
            $dataOrder = OrderModel::where('shop_id', $value->shop_id)->where('collaborator_id', 1)->get();
            foreach ($dataOrder as $key => $val){
                $value->count += 1;
                $dataOrderDetail = OrderDetailModel::where('shop_id', $value->shop_id)->get();
                $value->total = 0;
                $value->cod += $val->cod;
                foreach ($dataOrderDetail as $kOrDe => $vOrDe){
                    $value->total += $vOrDe->product_price * $vOrDe->quantity;
                }
            }
        }
        return [
            $value->name_shop,
            $value->count,
            $value->total,
            $value->cod,
        ];
    }
}
