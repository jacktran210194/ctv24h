<?php

namespace App\Exports\Collaborator;

use App\Models\CustomerModel;
use App\Models\PointModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use DB;
class HistoryPaymentPointExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        $dataUser = session()->get('data_collaborator');
        return PointModel::where('customer_id', $dataUser->customer_id)
            ->where('type', 0)
            ->orderBy('date', 'desc')
            ->get();
    }

    public function headings(): array
    {
        return [
            'Họ và tên',
            'Nội dung',
            'Số điểm',
            'Thời gian'
        ];
    }

    public function map($value): array
    {
        $dataUser = CustomerModel::find($value->customer_id);
        $value->name = $dataUser['name'] ?? '';
        return [
            $value->name,
            $value->title,
            $value->point,
            $value->date,
        ];
    }
}
