<?php

namespace App\Exports\Supplier;

use App\Models\AttributeValueModel;
use App\Models\ProductModel;
use App\Models\ShopModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use DB;
class DropshipCollaboratorExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        $dataLogin = session()->get('data_supplier');
        if($dataLogin){
            return DB::table('attribute_value')
                ->select('product.name as name', 'product.sku_type as sku_type', 'attribute_value.*', 'product_attribute.category_2 as category_2')
                ->join('product_attribute', 'attribute_value.attribute_id', 'product_attribute.id')
                ->join('product', 'product_attribute.product_id', '=', 'product.id')
                ->join('shop', 'product.shop_id', 'shop.id')
                ->where('shop.customer_id', $dataLogin->customer_id)
                ->orderBy('attribute_value.id', 'desc')
                ->get();
        }
    }

    public function headings(): array
    {
        return [
            'Loại sản phẩm',
            'Sản phẩm',
            'SKU phân loại',
            'Phân loại hàng',
            'Giá',
            'SL đã bán của CTV',
            'Số lượng'
        ];
    }

    public function map($value): array
    {
        $attr = $value->category_2 . '/' . $value->size_of_value ?? '';
        $price = $value->price ?? '';
        $quantity = $value->quantity ?? '';
        $sold = $value->is_sell;
        return [
            'Hàng Dropship',
            $value->name ?? '',
            $value->sku_type ?? '',
            $attr ?? '',
            $price ?? '',
            $sold,
            $quantity ?? ''
        ];
    }
}
