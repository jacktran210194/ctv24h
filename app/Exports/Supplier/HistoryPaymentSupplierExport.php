<?php

namespace App\Exports\Supplier;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use DB;
class HistoryPaymentSupplierExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        $dataUser = session()->get('data_supplier');
        return DB::table('payment')
            ->select('payment.*', 'bank_account.*', 'payment.created_at as created_at', 'the_bank.name as name_bank', 'the_bank.short_name as short_name_bank', 'bank_account.bank_name as bank_name')
            ->join('bank_account', 'payment.account', '=', 'bank_account.id')
            ->join('the_bank', 'bank_account.bank_name', '=', 'the_bank.id')
            ->where('bank_account.customer_id', $dataUser->customer_id)
            ->where('payment.type', 0)
            ->orderBy('payment.id', 'desc')
            ->get();
    }

    public function headings(): array
    {
        return [
            'Chủ tài khoản',
            'Số tài khoản',
            'Ngân hàng',
            'Số tiền',
            'Phí',
            'Ngày giao dịch'
        ];
    }

    public function map($value): array
    {
        return [
            $value->user_name,
            $value->bank_account,
            $value->name_bank . ' - '. $value->short_name_bank,
            $value->price,
            11000,
            date('d-m-Y H:i:s', strtotime($value->created_at)),
        ];
    }
}
