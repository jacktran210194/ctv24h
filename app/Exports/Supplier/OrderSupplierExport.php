<?php

namespace App\Exports\Supplier;

use App\Models\CustomerModel;
use App\Models\OrderModel;
use App\Models\ShopModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use DB;
class OrderSupplierExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        $data_login = session()->get('data_supplier');
        if($data_login){
            $data_shop = ShopModel::where('supplier_id',$data_login->id)->first();
            return OrderModel::where('shop_id',$data_shop->id)->orderBy('id','desc')->get();
        }
    }

    public function headings(): array
    {
        return [
            'Mã vận đơn',
            'Tên khách hàng',
            'Ngày đặt',
            'Tổng tiền',
            'Ghi chú',
            'Hình thức thanh toán',
            'Trạng thái',
        ];
    }

    public function map($value): array
    {
        $data_customer = CustomerModel::find($value->customer_id);
        $customer_name = isset($data_customer) ? $data_customer->name : '';
        if($value['status'] == 0){
            $value['status'] = 'Chờ xác nhận';
        }elseif($value['status'] == 1){
            $value['status'] = 'Chờ lấy hàng';
        }elseif($value['status'] == 2){
            $value['status'] = 'Đang giao hàng';
        }elseif($value['status'] == 3){
            $value['status'] = 'Đã giao hàng';
        }elseif($value['status'] == 4){
            $value['status'] = 'Đã huỷ';
        }else{
            $value['status'] = 'Hoàn tiền';
        }
        if($value['payment_id'] == 0){
            $value['payment_id'] = 'Thanh toán bằng ví CTV24H';
        }elseif($value['payment_id'] == 1){
            $value['payment_id'] = 'Trả góp';
        }elseif($value['payment_id'] == 2){
            $value['payment_id'] = 'Thanh toán khi nhận hàng';
        }else{
            $value['payment_id'] = 'Thẻ tín dụng hoặc ghi nợ';
        }
        $value['date'] = date('d-m-Y H:i:s',strtotime($value['date']));
        return [
            "'".$value->order_code,
            $customer_name,
            $value->date,
            number_format($value->total_price) .'đ',
            $value->note,
            $value->payment_id,
            $value->status
        ];
    }
}
