<?php

namespace App\Exports\Supplier;

use App\Models\OrderDetailModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use DB;
class ListCollaboratorExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        $data_login = session()->get('data_supplier');
        if($data_login){
            return DB::table('shop')
                ->select('order.customer_id as customer_id', 'order.customer_id as customer_id', 'collaborator.name as name_collaborator', 'collaborator.point as point')
                ->distinct('customer_id')
                ->join('order', 'shop.id', '=', 'order.shop_id')
                ->join('collaborator', 'order.customer_id', '=', 'collaborator.customer_id')
                ->where('shop.customer_id', $data_login->customer_id)
                ->get();
        }
    }

    public function headings(): array
    {
        return [
            'Tên shop CTV',
            'Hạng shop',
            'Số lượng sản phẩm dropship',
            'Tổng sản phẩm bán ra',
            'Tổng doanh thu',
        ];
    }

    public function map($value): array
    {
        $data_login = session()->get('data_supplier');
        foreach ($value as $__key => $__val){
            $value->dropship = 0;
            $value->total_price = 0;
            $dataOrder = DB::table('order')
                ->select('order.total_price as total_price', 'order.id as id_order', 'order.order_code as code', 'order.collaborator_id as collaborator_id')
                ->join('shop', 'order.shop_id', '=', 'shop.id')
                ->where('order.customer_id', $value->customer_id)
                ->where('shop.customer_id', $data_login->customer_id)
                ->get();
            $value->quantity = 0;
            foreach ($dataOrder as $__k => $__v) {
                if($__v->collaborator_id == 1){
                    $value->dropship += 1;
                }
                $value->total_price += $__v->total_price;
                $value->code = $__v->code;
                $dataOrderDetail = OrderDetailModel::where('order_id', $__v->id_order)->get();
                foreach ($dataOrderDetail as $__keyDetail => $__valDetail) {
                    $value->order_id = $__valDetail->order_id;
                    $value->quantity += $__valDetail->quantity;
                }
            }
            if ($value->point >=0 && $value->point < 1000) {
                $value->rank = 'Hạng đồng';
            } else if ($value->point >= 1000 && $value->point < 2000) {
                $value->rank = 'Hạng bạc';
            } else if ($value->point >= 2000 && $value < 3000) {
                $value->rank = 'Hạng vàng';
            } else {
                $value->rank = 'Hạng kim cương';
            }
        }
        return [
            $value->name_collaborator,
            $value->rank,
            $value->dropship,
            $value->quantity,
            $value->total_price
        ];
    }
}
