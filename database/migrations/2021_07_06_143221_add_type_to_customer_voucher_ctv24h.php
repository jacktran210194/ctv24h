<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeToCustomerVoucherCtv24h extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_voucher_ctv24h', function (Blueprint $table) {
            $table->integer('type')->comment('1:phieu giam gia, 2:khuyen mai, 3:xu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_voucher_ctv24h', function (Blueprint $table) {
            //
        });
    }
}
