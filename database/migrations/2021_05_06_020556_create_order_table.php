<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->string('order_code');
            $table->integer('customer_id');
            $table->double('total_price');
            $table->integer('status')->default(0);
            $table->dateTime('date');
            $table->integer('city_id');
            $table->integer('district_id');
            $table->integer('ward_id');
            $table->string('address');
            $table->integer('payment_id')->comment('0: Thanh toan khi nhan hang, 1: The tin dung/ghi no, 2: thanh toan chuyen khoan');
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
