<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToProductShippingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_shipping', function (Blueprint $table) {
            $table->integer('shop_id')->nullable();
            $table->integer('shipping_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_shipping', function (Blueprint $table) {
            $table->dropColumn('shipping_name');
            $table->dropColumn('shop_id');
        });
    }
}
