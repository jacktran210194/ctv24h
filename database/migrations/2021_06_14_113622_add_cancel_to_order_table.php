<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCancelToOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->integer('cancel_confirm')->comment('0: cho xac nhan huy don, 1: da xac nhan huy don')->nullable();
            $table->integer('cancel_reason')->comment('0: muon thay doi dia chi hang, 1: Muon nhap ma voucher, 2: Muon thay doi sp trong don hang, 3: thu tuc thanh toan qua rac roi, 4: tim thay gia re hon cho khac, 5: Khong mua nua, 6: ly do khac')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->dropColumn('cancel_confirm');
            $table->dropColumn('cancel_reason');
        });
    }
}
