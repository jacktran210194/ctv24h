<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_product', function (Blueprint $table) {
            $table->id();
            $table->integer("customer_id");
            $table->integer("product_id");
            $table->integer("ratings");
            $table->string("comment", 255)->nullable();
            $table->string("image_review", 255)->nullable();
            $table->string("video_review", 255)->nullable();
            $table->string("name_customer", 255)->nullable();
            $table->string("avatar_customer", 255)->nullable();
            $table->integer("order_detail_id")->nullable()->nullable();
            $table->integer("is_image")->nullable();
            $table->integer("is_video")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_product');
    }
}
