<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderCtvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_ctv', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id')->nullable();
            $table->integer('shop_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->integer('profit')->nullable();
            $table->integer('fee_ctv24h')->nullable();
            $table->integer('real_profit')->nullable();
            $table->integer('active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_ctv');
    }
}
