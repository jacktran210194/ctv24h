<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDropshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dropship', function (Blueprint $table) {
            $table->id();
            $table->string('name', '255')->nullable();
            $table->integer('category_id')->nullable();
            $table->double('price')->nullable();
            $table->string('sku_type', '255')->nullable();
            $table->integer('product_type')->nullable();
            $table->string('warehouse')->nullable();
            $table->double('is_sell')->nullable();
            $table->string('image', '255')->nullable();
            $table->longText('description')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dropship');
    }
}
