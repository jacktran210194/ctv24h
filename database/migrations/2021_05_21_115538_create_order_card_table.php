<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_card', function (Blueprint $table) {
            $table->id();
            $table->text('order_code')->nullable();
            $table->integer('customer_id');
            $table->integer('card_id');
            $table->double('total_price');
            $table->integer('payment')->nullable();
            $table->integer('active')->default(1);
            $table->integer('status')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_card');
    }
}
