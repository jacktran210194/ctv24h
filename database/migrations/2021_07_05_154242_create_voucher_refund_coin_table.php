<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucherRefundCoinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_refund_coin_table', function (Blueprint $table) {
            $table->id();
            $table->string('code_voucher')->nullable();
            $table->string('content')->nullable();
            $table->dateTime('time_star')->nullable();
            $table->dateTime('time_stop')->nullable();
            $table->integer('min-price')->nullable();
            $table->integer('max_point')->nullable();
            $table->string('image')->nullable();
            $table->integer('type')->nullable();
            $table->integer('category_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_refund_coin_table');
    }
}
