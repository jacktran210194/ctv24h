<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemovePriceToProductDropShip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_drop_ship', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropColumn('ctv_category_id');
            $table->dropColumn('customer_id');
            $table->dropColumn('supplier_id');
            $table->integer('product_parent')->nullable();
            $table->integer('shop_parent')->nullable();
            $table->integer('profit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_drop_ship', function (Blueprint $table) {
            //
        });
    }
}
