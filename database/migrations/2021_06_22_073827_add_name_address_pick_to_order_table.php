<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNameAddressPickToOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->string('name_address_pick')->nullable()->comment('ho ten dia chi lay hang');
            $table->string('phone_address_pick')->nullable()->comment('sdt dia chi lay hang');
            $table->integer('address_pick_id')->nullable()->comment('id dia chi lay hang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->dropColumn('name_address_pick');
            $table->dropColumn('phone_address_pick');
            $table->dropColumn('address_pick_id');
        });
    }
}
