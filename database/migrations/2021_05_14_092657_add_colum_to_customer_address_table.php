<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumToCustomerAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_address', function (Blueprint $table) {
            $table->dropColumn('city_id');
            $table->dropColumn('district_id');
            $table->dropColumn('ward_id');
        });
        Schema::table('customer_address', function (Blueprint $table) {
            $table->string('city_id');
            $table->string('district_id');
            $table->string('ward_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_address', function (Blueprint $table) {
            //
        });
    }
}
