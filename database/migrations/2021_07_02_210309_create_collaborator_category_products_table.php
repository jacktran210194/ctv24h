<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollaboratorCategoryProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collaborator_category_products', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->integer('customer_id');
            $table->integer('NCC_id');
            $table->integer('shop_id')->nullable();
            $table->integer('CTV_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collaborator_category_products');
    }
}
