<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeDefaultToAddressSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('address_supplier', function (Blueprint $table) {
            $table->integer('type_default')->default(0);
            $table->integer('type_pick_up')->default(0);
            $table->integer('type_return')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('address_supplier', function (Blueprint $table) {
            //
        });
    }
}
