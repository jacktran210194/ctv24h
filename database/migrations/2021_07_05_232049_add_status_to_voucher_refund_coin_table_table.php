<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToVoucherRefundCoinTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voucher_refund_coin_table', function (Blueprint $table) {
            $table->integer('status')->default(1)
            ->comment('1:active, 2:expired, 3:hide');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucher_refund_coin_table', function (Blueprint $table) {
            //
        });
    }
}
