<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToPromotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promotion', function (Blueprint $table) {
            $table->integer('number_save')->nullable();
            $table->integer('number_use')->nullable();
            $table->integer('used')->default(0);
            $table->integer('apply_for')->nullable()->comment('0: Voucher toan shop, 1: voucher san pham');
            $table->integer('type')->nullable()->comment('0: Voucher khuyen mai, 1: voucher hoan xu');
            $table->integer('money_or_percent')->nullable()->comment('0: Theo so tien, 1: theo phan tram');
            $table->integer('value_discount')->nullable();
            $table->integer('value_order_min')->nullable();
            $table->integer('shop_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promotion', function (Blueprint $table) {
            //
        });
    }
}
