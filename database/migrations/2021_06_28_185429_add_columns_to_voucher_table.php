<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voucher', function (Blueprint $table) {
            $table->integer('shop_or_product')->nullable()->comment('0: Voucher toan shop, 1: Voucher san pham');
            $table->integer('discount_or_refund')->nullable()->comment('0: Voucher khuyen mai, 1: Voucher hoan xu');
            $table->integer('type_discount')->nullable()->comment('0: Theo so tien, 1: Theo phan tram');
            $table->integer('min_order_value')->nullable();
            $table->integer('number_save')->nullable();
            $table->integer('number_available')->nullable();
            $table->integer('customer_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucher', function (Blueprint $table) {
            //
        });
    }
}
