<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryOrderPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_order_payment', function (Blueprint $table) {
            $table->id();
            $table->string('order_code')->nullable();
            $table->integer('order_id')->nullable();
            $table->integer('status')->nullable();
            $table->integer('price')->nullable();
            $table->integer('type')->nullable()->comment('0: Doanh thu tu don hang, 1: Rut tien tu don hang, 2: Hoan tien tu don hang, 3: Hoa hong gioi thieu, 4: Tru phi quang cao, 5: Dieu chinh');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_order_payment');
    }
}
