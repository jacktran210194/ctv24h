<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialRevenueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_revenue', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id');
            $table->integer('status')->default(0)->comment('0: Dang xu ly, 1: Hoan thanh, 2: Da huy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_revenue');
    }
}
