<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCardToShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop', function (Blueprint $table) {
            $table->longText('image_card_1')->nullable()->comment('cmt mat truoc');
            $table->longText('image_card_2')->nullable()->comment('cmt mat sau');
            $table->longText('business_license')->nullable()->comment('giay phep kinh doanh');
            $table->longText('related_document_1')->nullable()->comment('giay to lien quan');
            $table->longText('related_document_2')->nullable()->comment('giay to lien quan');
            $table->string('name_company')->nullable()->comment('ten doanh nghiep');
            $table->string('business')->nullable()->comment('nganh nghe kinh doanh');
            $table->string('card_id')->nullable()->comment('cmnd');
            $table->date('card_date_create')->nullable()->comment('ngay cap');
            $table->string('card_address')->nullable()->comment('noi cap');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop', function (Blueprint $table) {
            //
        });
    }
}
