<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id');
            $table->string('image', '191')->nullable();
            $table->string('title', '191')->nullable();
            $table->string('content', '191')->nullable();
            $table->string('code', '191');
            $table->date('date_end');
            $table->time('time_end');
            $table->integer('number');
            $table->integer('number_used')->nullable();
            $table->integer('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher');
    }
}
