<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsDropshipToHistoryOrderPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_order_payment', function (Blueprint $table) {
            $table->integer('is_dropship')->nullable()->comment('0: Ban hang, 1: Hoa hong');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_order_payment', function (Blueprint $table) {
            //
        });
    }
}
