<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToVoucherCtv24hTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voucher_ctv24h', function (Blueprint $table) {
            $table->integer('discount_percent')->nullable();
            $table->integer('voucher_value')->nullable();
            $table->integer('money_or_percent')->nullable()->comment('0: Theo so tien, 1: Theo phan tram');
            $table->integer('discount_value')->nullable();
            $table->integer('money_max')->nullable();
            $table->integer('coin_max')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucher_ctv24h', function (Blueprint $table) {
            //
        });
    }
}
