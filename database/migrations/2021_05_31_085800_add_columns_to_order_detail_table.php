<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_detail', function (Blueprint $table) {
            $table->integer('status')->default(0)->comment('0: Cho xac nhan, 1: Cho thanh toan, 2: Dang giao hang, 3: Da giao hang, 4: Huy don hang, 5: Hoan tien');
            $table->double('total_price')->nullable();
            $table->string('address', '255')->nullable();
            $table->text('note')->nullable();
            $table->integer('cod')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_detail', function (Blueprint $table) {
            //
        });
    }
}
