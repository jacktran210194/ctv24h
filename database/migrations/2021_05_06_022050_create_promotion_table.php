<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion', function (Blueprint $table) {
            $table->id();
            $table->string('title', '191');
            $table->string('content', '191');
            $table->string('image', '191');
            $table->string('code', '191');
            $table->double('value');
            $table->integer('number')->nullable();
            $table->integer('number_used')->nullable();
            $table->integer('active')->default(1);
            $table->integer('supplier_id')->nullable();
            $table->dateTime('time_start');
            $table->dateTime('time_end');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion');
    }
}
