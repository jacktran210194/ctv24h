<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucherCtv24hTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_ctv24h', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id');
            $table->string('image', '191');
            $table->string('title', '191')->nullable();
            $table->string('content', '191')->nullable();
            $table->string('code', '191');
            $table->integer('number');
            $table->integer('number_used')->default(0);
            $table->integer('active')->default(1);
            $table->integer('type')->nullable()->comment('0: cho nha cung cap, 1: cho ctv, 2: cho khach hang');
            $table->dateTime('time_start');
            $table->dateTime('time_end');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_ctv24h');
    }
}
