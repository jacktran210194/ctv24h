<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('image');
            $table->string('video');
            $table->text('desc');
            $table->timestamp('date');
            $table->string('time_feedback');
            $table->integer('percent_order_false')->default(0);
            $table->integer('product')->default(0);
            $table->integer('percent_feedback')->default(0);
            $table->integer('review')->default(0);
            $table->integer('category_id');
            $table->string('report');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop');
    }
}
