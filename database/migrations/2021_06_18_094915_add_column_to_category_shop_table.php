<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToCategoryShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_shop', function (Blueprint $table) {
            $table->integer('created_by')->default(0)->comment('0: nguoi ban, 1: he thong');
            $table->integer('status')->default(1)->comment('0: tat, 1 bat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_shop', function (Blueprint $table) {
            //
        });
    }
}
