<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollaboratorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collaborator', function (Blueprint $table) {
            $table->id();
            $table->string('name','191');
            $table->string('email','191');
            $table->string('phone','191');
            $table->string('username','191');
            $table->string('password','191');
            $table->date('birthday');
            $table->integer('sex');
            $table->integer('status')->default(0);
            $table->integer('wallet')->default(0);
            $table->string('image','191');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collaborator');
    }
}
