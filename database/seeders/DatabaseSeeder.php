<?php

namespace Database\Seeders;

use App\Models\ShippingModel;
use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data_shipping = [
            'name' => 'Viettel Post',
            'image' => '',
            'phone' => '0338457433',
            'address' => 'Hà Nội',
            'max_weight' => '20000',
            'default' => 1
        ];
        DB::table('shipping')->insert($data_shipping);
        $data_admin = [
            'username' => 'admin',
            'password' => md5('admin'),
            'name' => 'Admin'
        ];
        DB::table('admin')->insert($data_admin);

        $dataFeeCTV24H = [
            'fee' => 3
        ];
        DB::table('fee_ctv24h')->insert($dataFeeCTV24H);
    }
}
