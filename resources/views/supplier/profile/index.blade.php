<?php
$page = 'profile';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<script src="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.js"></script>
<link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.css"/>
<meta name="csrf-token" content="{{ csrf_token() }}"/>

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <h3 class="text-tilte-red ml-2">TÀI KHOẢN</h3>
        <div class="row">
            <div class="col-lg-3">
                <div class="form_image">
                    <img width="200px" height="200px" id="avatar" class="thumbnail image-show-qr border-tron" style="object-fit: cover"
                         src="{{isset($supplier->image)  ? $supplier['image'] : '/uploads/images/avt.jpg'}}"
                    >
                    <input type='file' accept="image/*" id="image" class="input_file"
                           onchange="changeImg(this)" hidden>
                    <br>
                    <button class="btn btn-white text-bold-700 mt-1 btn-add-file border_radius"
                            style="color: #D1292F; width: 200px; height: auto">Thêm ảnh
                    </button>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="form_info card p-1 position-relative" style="max-width: 80%;" >
                    <div class="d-flex align-items-center justify-content-lg-start" style="margin-bottom: 20px">
                        <h3>Thông tin cá nhân</h3>
                        <img style="margin-left: 20px" width="20px" height="auto"
                             src="../../assets/admin/app-assets/images/icon_dashboard/edit.png"
                             alt="">
                    </div>
                    <div style="padding-left: 40px">
                        <div class="form-group d-flex align-items-center">
                            <label class="text-bold-700" style="width: 100px">Họ và tên:</label>
                            <input type="text" class="input_config" style="width: calc(100% - 100px);padding: 0.7rem 0.7rem;" id="name" value="{{isset($data) ? $data['name'] : ''}}">
                        </div>
                        <div class="form-group d-flex align-items-center">
                            <label class="text-bold-700" style="width: 100px">Ngày sinh:</label>
{{--                            <input type="date" class="input_config" style="padding: 0.7rem 0.7rem;" id="birthday" value="{{isset($data) ? date('Y-m-d', strtotime($data['birthday'])) : date('Y-m-d')}}">--}}
                            <div style="width: calc(100% - 100px)" class="d-flex justify-content-between align-items-center">
                                <div class="input_config" style="width: calc(100%/3 - 20px); padding: 10px">
                                    <select class="selected-day " style="outline: none"></select>
                                </div>
                                <div class="input_config" style="width: calc(100%/3 - 20px); padding: 10px">
                                    <select class="selected-month" style="outline: none"></select>
                                </div>
                                <div class="input_config" style="width: calc(100%/3 - 20px); padding: 10px">
                                    <select class="selected-year" style="outline: none"></select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex align-items-center justify-content-between form-group">
                        <div class="d-flex align-items-center" style="width: calc(50% - 5px);">
                            <label class="text-bold-700" style="width: 150px;">Số điện thoại: </label>
                            <div class="input_config form-control d-flex align-items-center justify-content-between">
                                <input style="height: 100%; padding: 0 10px; border: none; outline: none; width: calc(100% - 40px)"
                                       value="{{isset($data) ? $data['phone'] : ''}}" readonly
                                >
                                <img style="margin-left: 20px; cursor: pointer" width="20px" height="auto"
                                     src="../../assets/admin/app-assets/images/icon_dashboard/edit.png"
                                     class="btn-change-phone"
                                     alt="">
                            </div>
                        </div>
                        <div class="d-flex align-items-center" style="width: calc(50% - 5px)">
                            <label class="text-bold-700 mr-1" style="width: auto;">Email</label>
                            <input style="width: 125%;" class="form-control input_config" id="email" value="{{isset($data) ? $data['email'] : ''}}">
                        </div>
                    </div>
                    <div class="form-group d-flex align-items-center">
                        <label class="text-bold-700">Mật khẩu đăng nhập</label>
                        <div class="input_config form-control d-flex align-items-center justify-content-between">
                            <input type="password" disabled id="password" style="height: 100%; padding: 0 10px; border: none; outline: none; width: calc(100% - 40px)"
                                value="{{$data->password}}"
                            >
                            <img style="margin-left: 20px; cursor: pointer" width="20px" height="auto"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/edit.png"
                                 class="btn-change-pass"
                                 alt="">
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="position-absolute" style="right: 2%; bottom: 3%;">
                        <button class="btn btn_main text-bold-700 border_table mr-1" style="width: 140px;">Huỷ</button>
                        <button data-id="{{$data['id']}}" data-url="{{url('admin/profile/update')}}" class="btn btn_main btn-add-account text-bold-700 border_table" style="width: 140px;"><i class="fa fa-save"></i> Lưu</button>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<div class="popup-confirm-pass position-fixed w-100 h-100 d-flex justify-content-sm-center align-items-center">
    <div class="p-2 border_box bg-white border border-radius-16" style="width: 388px;">
        <h3 class="color-black font-size-14 text-center mg-top-20" style="line-height: 24px">Để đảm bảo tài khoản luôn an toàn. Bạn vui lòng nhập lại mật khẩu </h3>
        <div class="mg-top-20">
            <label class="font-size-14 color-black">Mật Khẩu</label>
            <input class="w-100 border border-radius-4" style="padding: 12px 15px" type="password">
        </div>
        <div class="d-flex justify-content-between align-items-center mg-top-20">
            <button class="font-weight-bold border border-radius-16 color-white bg-red btn-confirm-pass" style="padding: 12px 0; width: calc(50% - 10px)">Xác nhận</button>
            <button class="font-weight-bold border border-radius-16 color-red btn-cancel-pass" style="padding: 12px 0; width: calc(50% - 10px)">Hủy</button>
        </div>
    </div>
</div>
<div class="popup-confirm-pass-2 position-fixed w-100 h-100 d-flex justify-content-sm-center align-items-center">
    <div class="p-2 border_box bg-white border border-radius-16" style="width: 388px;">
        <h3 class="color-black font-size-14 text-center mg-top-20" style="line-height: 24px">Để đảm bảo tài khoản luôn an toàn. Bạn vui lòng nhập lại mật khẩu </h3>
        <div class="mg-top-20">
            <label class="font-size-14 color-black">Mật Khẩu</label>
            <input class="w-100 border border-radius-4 re-pass" style="padding: 10px 10px" type="password">
        </div>
        <div class="d-flex justify-content-between align-items-center mg-top-20">
            <button class="font-weight-bold border border-radius-16 color-white bg-red btn-confirm-pass-2" style="padding: 12px 0; width: calc(50% - 10px)">Xác nhận</button>
            <button class="font-weight-bold border border-radius-16 color-red btn-cancel-pass-2" style="padding: 12px 0; width: calc(50% - 10px)">Hủy</button>
        </div>
    </div>
</div>
<div class="popup-edit-pass position-fixed w-100 h-100 d-flex justify-content-sm-center align-items-center">
    <div class="p-2 border_box bg-white border border-radius-16" style="width: 388px;">
        <h3 class="color-black font-size-14 text-center mg-top-20" style="line-height: 24px">Vui lòng nhập mật khẩu mới </h3>
        <div class="mg-top-20">
            <label class="font-size-14 color-black">Mật Khẩu</label>
            <input class="w-100 border border-radius-4" style="padding: 12px 15px" type="password">
        </div>
        <div class="d-flex justify-content-between align-items-center mg-top-20">
            <button class="font-weight-bold border border-radius-16 color-white bg-red btn-edit-pass" style="padding: 12px 0; width: calc(50% - 10px)">Xác nhận</button>
            <button class="font-weight-bold border border-radius-16 color-red btn-cancel-pass" style="padding: 12px 0; width: calc(50% - 10px)">Hủy</button>
        </div>
    </div>
</div>
<div class="popup-change-phone position-fixed w-100 h-100 d-flex justify-content-sm-center align-items-center">
    <div class="p-2 border_box bg-white border border-radius-16" style="width: 388px;">
        <h3 class="color-black font-size-14 text-center mg-top-20" style="line-height: 24px">Để thay đổi số điện thoại. Bạn vui lòng nhập số điện thoại mới của bạn</h3>
        <div class="mg-top-20">
            <label class="font-size-14 color-black">Số điện thoại mới</label>
            <div class="d-flex align-items-center justify-content-lg-between">
                <input class="w-75 border border-radius-4" style="padding: 10px 10px;" type="text" id="phone">
                <button type="button" class="btn btn-send" data-url="{{url('admin/profile/change_phone')}}">Gửi mã</button>
            </div>
        </div>
        <div class="mg-top-20">
            <label class="font-size-14 color-black">Mã xác minh</label>
            <input class="w-100 border border-radius-4" style="padding: 10px 10px" type="number" id="otp">
        </div>
        <div id="recaptcha"></div>
        <div class="d-flex justify-content-between align-items-center mg-top-20">
            <button class="font-weight-bold border border-radius-16 color-white bg-red btn-confirm-otp" style="padding: 12px 0; width: calc(50% - 10px)" data-url="{{url('admin/profile/save_phone')}}">Xác nhận</button>
            <button class="font-weight-bold border border-radius-16 color-red btn-cancel-otp" style="padding: 12px 0; width: calc(50% - 10px)">Hủy</button>
        </div>
    </div>
</div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-firestore.js"></script>
<script src="{{url('assets/admin/js/otp/index.js')}}"></script>
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('click', '.btn-add-file', function () {
            $('.input_file').click();
        });
        $(document).on("click", ".btn-add-account", function () {
            let url = $(this).attr('data-url');
            let day = $('.selected-day').val();
            let month = $('.selected-month').val();
            let year = $('.selected-year').val();
            let datetime = year + '-' + month + '-' + day;
            let form_data = new FormData();
            let user_name = $('#name').val();
            let email = $('#email').val();
            let specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-=0123456789";
            let specialChars_email = "<>!#$%^&*()_+[]{}?:;|'\"\\,/~`-=";
            let check = function (string) {
                for (i = 0; i < specialChars.length; i++) {
                    if (string.indexOf(specialChars[i]) > -1) {
                        return true;
                    }
                }
                return false;
            };
            let checkEmail = function (string) {
                for (i = 0; i < specialChars_email.length; i++) {
                    if (string.indexOf(specialChars_email[i]) > -1) {
                        return true;
                    }
                }
                return false;
            };
            if(check(user_name)){
                swal('Thông báo', 'Họ và tên không bao gồm chữ số và kí tự đặc biệt', 'warning');
            } else if(checkEmail(email)){
                swal('Thông báo', 'Không đúng định dạng email', 'warning');
            } else {
                form_data.append('id', $(this).attr('data-id'));
                form_data.append('name1', $('#name').val());
                form_data.append('email1', $('#email').val());
                form_data.append('birthday', datetime);
                form_data.append('image', $('#image')[0].files[0]);
                $.ajax({
                    url: url,
                    type: "POST",
                    data: form_data,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        showLoading();
                    },
                    success: function (data) {
                        if (data.status) {
                            swal("Thành công", data.msg, "success");
                            window.location.href = data.url;
                        } else {
                            swal("Thất bại", data.msg, "error");
                        }
                    },
                    error: function () {
                        console.log('error')
                    },
                    complete: function () {
                        hideLoading();
                    }
                });
            }
        });
        //---------custom date time-------------//
        let day_time = {{$day}};
        let month_time = {{$month}};
        let year_time = {{ $year }};
        for (let i = 1; i <= 31; i++){
            if (i<10){
                let a = '0' + i;
                if(i == day_time){
                    let option = '<option selected>'+ a + '</option>';
                    $('.selected-day').append(option);
                }else {
                    let option = '<option>'+ a + '</option>';
                    $('.selected-day').append(option);
                }
            }else {
                if(i == day_time){
                    let option = '<option selected>'+ i + '</option>';
                    $('.selected-day').append(option);
                }else {
                    let option = '<option>'+ i + '</option>';
                    $('.selected-day').append(option);
                }
            }
        }
        for (let i = 1; i <= 12; i++){
            if ( i < 10 ){
                let a = '0' + i;
                if ( i == month_time){
                    let option = '<option selected>'+ a + '</option>';
                    $('.selected-month').append(option);
                }else {
                    let option = '<option>'+ a + '</option>';
                    $('.selected-month').append(option);
                }
            }else {
                if (i == month_time){
                    let option = '<option selected>'+ i + '</option>';
                    $('.selected-month').append(option);
                }else {
                    let option = '<option>'+ i + '</option>';
                    $('.selected-month').append(option);
                }
            }
        }
        let d = new Date();
        let year = d.getFullYear();
        for ( let i = 1960; i <= year; i++){
            if (i == year_time){
                let option = '<option selected>'+ i + '</option>';
                $('.selected-year').append(option);
            }else {
                let option = '<option>'+ i + '</option>';
                $('.selected-year').append(option);
            }
        }
        //-----------------------//
        let url_edit = window.location.href + '/address/edit/profile';
        let data={};
        $('.btn-change-pass').click(function () {
            $('.popup-confirm-pass').addClass("active");
        });
        $('.popup-edit-pass .btn-cancel-pass').click(function () {
            $('.popup-confirm-pass').addClass("active");
            $('.popup-edit-pass').removeClass("active");
        });
        $('.popup-confirm-pass .btn-cancel-pass').click(function () {
            $('.popup-confirm-pass').removeClass("active");
        });
        $('.btn-confirm-pass').click(function () {
            data['pass'] = $('.popup-confirm-pass input[type = "password"]').val();
            editProfile();
        });
        $('.btn-edit-pass').click(function () {
            let value = $('.popup-edit-pass input[type = "password"]').val();
            if (value.length < 8){
                alert('vui lòng điền thấp nhât 8 ký tự');
            }else {
                data['new_pass'] = value;
                editProfile();
            }
        });
        function editProfile() {
            if (data['new_pass'] == null){
                $.ajax({
                    url: url_edit,
                    type: 'POST',
                    data: data,
                    success:function (data) {
                        if (data.status){
                            $('.popup-edit-pass').addClass("active");
                            $('.popup-confirm-pass').removeClass("active");
                        }else {
                            swal("Thất bại", data.msg, "error");
                        }
                    }
                });
            }else {
                $.ajax({
                    url: url_edit,
                    type: 'POST',
                    data: data,
                    success:function (data) {
                        if (data.status){
                            swal("Thành công", data.msg, "success");
                            location.reload();
                        }else {
                            swal("Thất bại", data.msg, "error");
                        }
                    }
                });
            }
        }
        $(document).on('click', '.btn-change-phone', function () {
            $('.popup-confirm-pass-2').addClass("active");
        });
        $('.btn-cancel-pass-2').click(function () {
            $('.popup-confirm-pass-2').removeClass("active");
        });
        $('.btn-confirm-pass-2').click(function () {
            let pass = $('.re-pass').val();
            let url = 'admin/profile/confirm_pass';
            let data = {};
            data['pass'] = pass;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                success:function (data) {
                    if (data.status){
                        $('.popup-confirm-pass-2').removeClass("active");
                        $('.popup-change-phone').addClass("active");
                    }else {
                        swal("Thông báo", data.msg, "error");
                    }
                }
            });
        });
        $(document).on('click', '.btn-cancel-otp', function () {
            $('.popup-change-phone').removeClass('active');
        });
        //validate
        let specialChars = "<>@!#$%^&*_+[]{}?:;|'\"\\/~`-=()+-qwertyuiopasdfghjklzxcvbnm., ";
        let check = function (string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true;
                }
            }
            return false;
        };
        let verificationId;
        $(document).on('click', '.btn-send', function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            let phone = $('#phone').val();
            let check_phone = phone.slice(0, 1);
            let length_phone = phone.length;
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('phone', phone);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            if ($('#phone').val() == '') {
                swal('Thông báo', "Vui lòng nhập số điện thoại !", "error");
            } else if (check_phone !== '0') {
                swal('Thông báo', 'Số điện thoại bạn vừa nhập không hợp lệ !', 'error');
            } else if ((length_phone < 10) || (length_phone > 10)) {
                swal('Thông báo', 'Độ dài số điện thoại không hợp lệ !', 'error');
            }
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: async function (data) {
                    if (data.status) {
                        $('#recaptcha').show();
                        let applicationVerifier = new firebase.auth.RecaptchaVerifier(
                            'recaptcha',{
                                size: "invisible",
                                callback: function(response) {
                                    submitPhoneNumberAuth();
                                }
                            });
                        let fs = phone.slice(0, 3);
                        if (fs !== '+84') {
                            phone = phone.slice(1, phone.length);
                            phone = '+84' + phone;
                        }
                        let provider = new firebase.auth.PhoneAuthProvider();
                        try {
                            verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                            $('#recaptcha').hide();
                        } catch (e) {
                            console.log('e');
                            let click = 0;
                            $(document).on("click", ".swal-button--confirm", function () {
                                console.log(click++);
                            });
                        }
                    } else {
                        swal('Lỗi', data.msg, 'error');
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-confirm-otp", async function () {
            try {
                let verificationCode = $('#otp').val();
                let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
                try {
                    await firebase.auth().signInWithCredential(phoneCredential);
                    let url = $(this).attr('data-url');
                    let form_data = new FormData();
                    form_data.append('phone', $('#phone').val());
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: form_data,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status) {
                                $('.popup-change-phone').removeClass('active');
                                swal("Thông báo", data.msg, "success");
                                window.location.href = data.url;
                            } else {
                                swal("Thông báo", data.msg, "error");
                            }
                        },
                        error: function () {
                            console.log('error');
                        },
                    });
                } catch (e) {
                    swal("Thông báo", "Mã OTP không chính xác !", "error");
                    location.reload();
                }
            } catch (e) {
                swal("Thông báo", "Đã có lỗi xảy ra !", "error");
            }
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

