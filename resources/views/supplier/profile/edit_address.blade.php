<div class="popup-edit-payment popup-edit-address">
    <div class="container-popup-payment">
        <div class="header-popups-payment">
            <p class="text-bold-700 text-blue f-18">Thêm mới địa chỉ</p>
        </div>
        <div class="information-payment-unit">
            <div class="form-group">
                <input id="name" type="text" class="form-control input_config"
                       value="{{isset($data_address) ? $data_address['name'] : ''}}" placeholder="Họ và tên">
            </div>
            <div class="form-group">
                <input id="phone" type="number" class="form-control input_config"
                       value="{{isset($data_address) ? $data_address['phone'] : ''}}" placeholder="Số điện thoại">
            </div>
            <div class="form-group">
                <select id="city" class="form-control input_config choose_city city cityS"
                        data-url="{{url('admin/profile/select_district')}}">
                    <option class="input_config" value="">Tỉnh/thành phố</option>
                    @foreach($city as $value)
                        <option data-name="{{$value->name}}" class="input_config"
                                value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select id="district" class="form-control input_config choose_district district"
                        data-url="{{url('admin/profile/select_ward')}}">
                    <option class="input_config" value="">Quận/huyện</option>
                </select>
            </div>
            <div class="form-group">
                <select id="ward" class="form-control input_config ward">
                    <option class="input_config" value="">Phường/xã</option>
                </select>
            </div>
            <div class="form-group">
                <input id="address" type="text" class="form-control input_config"
                       value="{{isset($data_address) ? $data_address['address'] : ''}}" placeholder="Địa chỉ cụ thể">
            </div>
            <div class="form-group">
                <button style="width: 100%;" class="btn btn-default btn_border">
                    <div class="d-flex align-items-center">
                        <img src="../../assets/admin/app-assets/images/icon_dashboard/map.png" alt="">
                        <div style="margin: auto">
                            <span class="text-bold-700 f-12">Chọn vị trí trên bản đồ</span>
                            <p class="text-sliver m-0 f-12">Giúp đơn hàng được giao nhanh nhất</p>
                        </div>
                    </div>
                </button>
            </div>
            <div class="form-group">
                <input @isset($data_address) @if($data_address->type == 0) checked @endif @endisset id="type_address" name="type_address" type="radio" value="0"><span class="ml-1 text-bold-700">Đặt địa chỉ mặc định</span>
                <br>
                <input @isset($data_address) @if($data_address->type == 1) checked @endif @endisset id="type_address" name="type_address" type="radio" value="1"><span class="ml-1 text-bold-700">Đặt địa chỉ lấy hàng</span>
                <br>
                <input @isset($data_address) @if($data_address->type == 2) checked @endif @endisset id="type_address" name="type_address" type="radio" value="2"><span class="ml-1 text-bold-700">Đặt địa chỉ trả hàng</span>
            </div>
        </div>
        <div class="d-flex justify-content-lg-end align-items-center">
            <button class="btn btn_main btn-cancel text-bold-700">Trở lại</button>
            <button class="btn btn_main btn-confirm-address ml-2 text-bold-700"
                    data-supplier="{{isset($data_login) ? $data_login->id : ''}}"
                    data-url="{{url('admin/profile/address/add')}}"
                    data-id
            >Hoàn thành
            </button>
        </div>
    </div>
</div>
