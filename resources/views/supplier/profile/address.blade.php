<?php
$page = 'address_ncc';
$data_login = Session::get('data_supplier');


use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
{{--<link rel="stylesheet" type="text/css" href="css/address/style.css">--}}
<style>
    .popup-edit-payment {
        position: fixed;
        top: 0;
        left: 10%;
        width: 85%;
        height: 100%;
        z-index: 10;
        display: none;
    }

    .popup-edit-payment.show-popup {
        display: block;
        z-index: 20;
    }

    .popup-edit-payment .container-popup-payment {
        width: 40%;
        margin: 20px auto;
        background: #fff;
        border-radius: 16px;
        padding: 30px;
        border: 1px solid #005DB6;
    }

    .container-popup-payment .header-popups-payment {
        margin-bottom: 10px;
    }

    .title-header-payment {
        color: #005DB6;
        font-weight: bold;
        font-size: 24px;
    }
</style>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="title d-flex align-items-center position-relative p-2">
                <div>
                    <span class="text-bold-700 f-18">ĐỊA CHỈ</span>
                    <br>
                    <span class="text-sliver">Quản lý vận chuyển và giao hàng của bạn</span>
                </div>
                <div class="btn-add position-absolute" style="right: 1%;">
                    <button data-url="{{url('admin/profile/get_modal_address')}}"
                            class="btn btn_main text-bold-700 btn-add-new-address"><i class="fa fa-plus"></i> Thêm địa
                        chỉ mới
                    </button>
                </div>
            </div>
            <div class="border-bottom"></div>
            @if(count($data))
                @foreach($data as $value)
                    <div class="info-address p-2 d-flex align-items-center">
                        <img width="40px" height="40px"
                             src="../../assets/admin/app-assets/images/icon_dashboard/vitri.png"
                             alt="">
                        <div class="title-address ml-2">
                            <p class="text-sliver text-bold-700">Họ và tên</p>
                            <p class="text-sliver text-bold-700">Số điện thoại</p>
                            <p class="text-sliver text-bold-700 m-0">Địa chỉ</p>
                        </div>
                        <div class="detail-address ml-3">
                            <div class="d-flex align-items-center">
                                <p style="width: 183px;!important;" class="text-bold-700 f-16">{{$value->name}}
                                </p>
                                <div style="margin-left: 60px!important; margin-bottom: 1rem">
                                    @if($value['default'] == 1)
                                        <span class="badge bg-green"
                                              style="font-weight: 700!important; ">Địa chỉ mặc định</span>
                                    @endif
                                    @if($value['type_pick_up'] == 1)
                                        <span class="badge bg-pink text-red"
                                              style="font-weight: 700!important;margin-left: 20px;">Địa chỉ lấy hàng</span>
                                    @endif
                                    @if($value['type_return'] == 1)
                                        <span class="badge bg-yellow"
                                              style="font-weight: 700!important; margin-left: 20px;">Địa chỉ trả hàng</span>
                                    @endif
                                </div>
                            </div>
                            <p>{{$value->phone}}</p>
                            <p style="max-width: 500px;" class="m-0">{{$value->address}}</p>
                        </div>
                        <div class="position-absolute" style="right: 10%;">
                            <a class="btn_edit"
                               data-url="{{url('admin/profile/get_modal_address?id_address='.$value->id)}}"
                               id="{{$value->id}}"><img
                                    src="../../assets/admin/app-assets/images/icon_dashboard/edit.png"
                                    alt=""></a>
                            <a data-url="{{url('admin/profile/address/delete/'.$value->id)}}"
                               class="text-underline f-16 text-blue text-bold-700 ml-1 btn-delete">Xoá</a>
                        </div>
                    </div>
                    <div class="border-bottom"></div>
                @endforeach
            @else
                <h3 class="p-2 text-center">Không có dữ liệu !</h3>
            @endif
        </div>
    </div>
</div>

<div class="popup-edit-payment">
    <div class="container-popup-payment">
        <div class="header-popups-payment">
            <p class="text-bold-700 text-blue f-18">Thêm mới địa chỉ</p>
        </div>
        <div class="information-payment-unit">
            <div class="form-group">
                <input id="name" type="text" class="form-control input_config"
                       value="{{isset($data_address) ? $data_address['name'] : ''}}" placeholder="Họ và tên">
            </div>
            <div class="form-group">
                <input id="phone" type="number" class="form-control input_config"
                       value="{{isset($data_address) ? $data_address['phone'] : ''}}" placeholder="Số điện thoại">
            </div>
            <div class="form-group">
                <select id="city" class="form-control input_config choose_city city cityS"
                        data-url="{{url('admin/profile/select_district')}}">
                    <option class="input_config" value="">Tỉnh/thành phố</option>
                    @foreach($city as $value)
                        <option data-name="{{$value->name}}" class="input_config"
                                value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select id="district" class="form-control input_config choose_district district"
                        data-url="{{url('admin/profile/select_ward')}}">
                    <option class="input_config" value="">Quận/huyện</option>
                </select>
            </div>
            <div class="form-group">
                <select id="ward" class="form-control input_config ward">
                    <option class="input_config" value="">Phường/xã</option>
                </select>
            </div>
            <div class="form-group">
                <input id="address" type="text" class="form-control input_config"
                       value="{{isset($data_address) ? $data_address['address'] : ''}}" placeholder="Địa chỉ cụ thể">
            </div>
            <div class="form-group">
                <button style="width: 100%;" class="btn btn-default btn_border">
                    <div class="d-flex align-items-center">
                        <img src="../../assets/admin/app-assets/images/icon_dashboard/map.png" alt="">
                        <div style="margin: auto">
                            <span class="text-bold-700 f-12">Chọn vị trí trên bản đồ</span>
                            <p class="text-sliver m-0 f-12">Giúp đơn hàng được giao nhanh nhất</p>
                        </div>
                    </div>
                </button>
            </div>
            <div class="form-group">
                <input id="type_default" value="1" name="type_default" type="checkbox"><span class="ml-1 text-bold-700">Đặt địa chỉ mặc định</span>
                <br>
                <input id="type_pick_up" value="1" name="type_pick_up" type="checkbox"><span class="ml-1 text-bold-700">Đặt địa chỉ lấy hàng</span>
                <br>
                <input id="type_return" value="1" name="type_return" type="checkbox"><span class="ml-1 text-bold-700">Đặt địa chỉ trả hàng</span>
            </div>
        </div>
        <div class="d-flex justify-content-lg-end align-items-center">
            <button class="btn btn_main btn-cancel text-bold-700">Trở lại</button>
            <button class="btn btn_main btn-confirm-address ml-2 text-bold-700"
                    data-customer="{{isset($data_login) ? $data_login->customer_id : ''}}"
                    data-url="{{url('admin/profile/address/add')}}"
                    data-id
            >Hoàn thành
            </button>
        </div>
    </div>
</div>


<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<div id="modalAddress">
</div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
        $(document).on('click', '.btn-add-new-address', function () {
            $('.popup-edit-payment').addClass('show-popup');
        });
        $(document).on('click', '.btn-cancel', function () {
            $('.popup-edit-payment').removeClass('show-popup');
        });
        $(document).on('click', '.btn_edit', function () {
            let url = $(this).attr('data-url');
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    $('#modalAddress').html(data.html);
                    // myLatLng['lat'] = $('.btn-add-maps').attr('data-lat');
                    // myLatLng['lng'] = $('.btn-add-maps').attr('data-log');
                    $('#modalAddress').show();
                },
                error: function () {
                    console.log('error')
                }
            });
        });
        $(document).on('click', '.btn-confirm-address', function () {
            let url = $(this).attr('data-url');
            let id = $(this).attr('data-id');
            let type_default = $('#type_default').is(':checked') ? 1 : 0;
            let type_pick_up = $('#type_pick_up').is(':checked') ? 1 : 0;
            let type_return = $('#type_return').is(':checked') ? 1 : 0;
            let form_data = new FormData();
            let phone = $("#phone").val();
            let check_phone = phone.slice(0, 1);
            let name = $('#name').val();
            let specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-=0123456789";
            let check = function (string) {
                for (i = 0; i < specialChars.length; i++) {
                    if (string.indexOf(specialChars[i]) > -1) {
                        return true;
                    }
                }
                return false;
            };
            if (check(name)) {
                swal('Thông báo', 'Họ và tên không bao gồm chữ số và kí tự đặc biệt', 'error');
            } else if (phone == '') {
                swal('Thông báo', 'Vui lòng nhập số điện thoại', 'error');
            } else if (check_phone != '0') {
                swal('Thông báo', 'Số điện thoại không hợp lệ', 'error');
            } else if (phone.length < 10 || phone.length > 10) {
                swal('Thông báo', 'Độ dài số điện thoại không hợp lệ', 'error');
            } else {
                form_data.append('id', id);
                form_data.append('name', $('#name').val());
                form_data.append('phone', $('#phone').val());
                form_data.append('city_id', $('#city').val());
                form_data.append('district_id', $('#district').val());
                form_data.append('ward_id', $('#ward').val());
                form_data.append('address', $('#address').val());
                form_data.append('type_default', type_default);
                form_data.append('type_pick_up', type_pick_up);
                form_data.append('type_return', type_return);
                form_data.append('customer_id', $(this).attr('data-customer'));
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    type: "POST",
                    data: form_data,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.status) {
                            swal("Thành công", data.msg, "success");
                            window.location.href = data.url;
                        } else {
                            swal("Thất bại", data.msg, "error");
                        }
                    },
                    error: function (e) {
                        console.log('error' + e)
                    },
                });
            }
        });

        /**
         * Chọn quận/huyện
         */
        $('.choose_city').on('change', function () {
            let url = $(this).attr('data-url');
            let action = $(this).attr('id');
            let id = $(this).val();
            let result = '';
            if (action === 'city') {
                result = 'district';
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    action: action,
                    id: id,
                },
                success: function (data) {
                    $('#' + result).html(data);
                }
            });
        });

        /**
         *  Chọn phường/xã
         */
        $('.choose_district').on('change', function () {
            let url = $(this).attr('data-url');
            let action = $(this).attr('id');
            let id = $(this).val();
            let result = '';
            if (action === 'district') {
                result = 'ward';
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    action: action,
                    id: id,
                },
                success: function (data) {
                    $('#' + result).html(data);
                }
            });
        });

        $(document).on("click", ".btn-cancel", function () {
            $('#modalAddress').hide();
        });

        $(document).on('change', '.form-address', function () {
            let url = $(this).attr('data-url');
            let value = $(this).val();
            url += value;
            let val = $(this).attr('data-value');
            $(this).removeClass('border-red');
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    if (val === 'city') {
                        $('#districtSelect').html(data.html);
                    } else {
                        $('#wardSelect').html(data.html);
                    }
                },
                error: function () {
                    console.log('error')
                }
            });
        });
    })
    ;

    $(document).on("click", ".btn-update-address", function () {
        let url = $(this).attr('data-url');
        let id = $(this).attr('data-id');
        let phone = $("#phone_update").val();
        let check_phone = phone.slice(0, 1);
        let name = $('#name_update').val();
        let specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-=0123456789";
        let check = function (string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true;
                }
            }
            return false;
        };
        if (check(name)) {
            swal('Thông báo', 'Họ và tên không bao gồm chữ số và kí tự đặc biệt', 'error');
        } else {
            if (phone == '') {
                swal('Thông báo', 'Vui lòng nhập số điện thoại', 'error');
            } else if (check_phone != '0') {
                swal('Thông báo', 'Số điện thoại không hợp lệ', 'error');
            } else if (phone.length < 10 || phone.length > 10) {
                swal('Thông báo', 'Độ dài số điện thoại không hợp lệ', 'error');
            } else {
                let form_data = new FormData();
                let type_default = $('#type_default_update').is(':checked') ? 1 : 0;
                let type_pick_up = $('#type_pick_up_update').is(':checked') ? 1 : 0;
                let type_return = $('#type_return_update').is(':checked') ? 1 : 0;
                form_data.append('id', id);
                form_data.append('name', $("#name_update").val());
                form_data.append('phone', $("#phone_update").val());
                form_data.append('city_id', $("#citySelect").val());
                form_data.append('district_id', $("#districtSelect").val());
                form_data.append('ward_id', $("#wardSelect").val());
                form_data.append('address', $("#address_update").val());
                form_data.append('type_default', type_default);
                form_data.append('type_pick_up', type_pick_up);
                form_data.append('type_return', type_return);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    type: "POST",
                    data: form_data,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.status) {
                            swal("Thành công", data.msg, "success");
                            location.reload();
                        } else {
                            swal("Thất bại", data.msg, "error");
                        }
                    },
                    error: function () {
                        console.log('error')
                    },
                });
            }
        }
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->
</html>
