<?php
use Illuminate\Support\Facades\URL;
?>
<style>
    .popup-edit-payment-2 {
        width: 85%;
        height: 100%;
        display: block;
        z-index: 20;
    }

    .popup-edit-payment-2 .container-popup-payment-2 {
        width: 40%;
        margin: 20px auto;
        background: #fff;
        border-radius: 16px;
        padding: 30px;
        border: 1px solid #005DB6;
    }

    .container-popup-payment-2 .header-popups-payment-2 {
        margin-bottom: 10px;
    }

    .title-header-payment-2 {
        color: #005DB6;
        font-weight: bold;
        font-size: 24px;
    }
</style>
<div class="popup-edit-payment-2 position-absolute position-center">
    <div class="container-popup-payment-2">
        <div class="header-popups-payment-2">
            <p class="text-bold-700 text-blue f-18">Cập nhật địa chỉ</p>
        </div>
        <div class="information-payment-unit">
            <div class="form-group">
                <input id="name_update" type="text" class="form-control input_config"
                       value="<?= isset($data) ? $data->name : '' ?>" placeholder="Họ và tên">
            </div>
            <div class="form-group">
                <input id="phone_update" type="number" class="form-control input_config"
                       value="<?= isset($data) ? $data->phone : '' ?>" placeholder="Số điện thoại">
            </div>
            <div class="form-group">
                <select class="form-control input_config form-address" data-value="city" id="citySelect"
                        data-url="<?= URL::to('admin/profile/filter_address?type=city&&id=') ?>">
                    <option class="input_config" value="">Tỉnh/thành phố</option>
                    <?php foreach ($city as $value): ?>
                    <option
                        <?= isset($data) && $data->city_id == $value['id'] ? 'selected' : '' ?> value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control input_config form-address" data-value="district" id="districtSelect"
                        data-url="<?= URL::to('admin/profile/filter_address?type=district&&id=') ?>">
                    <option class="input_config" value="">Quận/huyện</option>
                    <?php if (isset($district)): ?>
                    <?php foreach ($district as $value): ?>
                    <option
                        <?= isset($data) && $data->district_id == $value['id'] ? 'selected' : '' ?> value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control input_config" id="wardSelect">
                    <option class="input_config" value="">Phường/xã</option>
                    <?php if (isset($ward)): ?>
                    <?php foreach ($ward as $val): ?>
                    <option
                        <?= isset($data) && $data->ward_id == $val['ward_id'] ? 'selected' : '' ?> value="<?= $val['ward_id'] ?>"><?= $val['name'] ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
            <div class="form-group">
                <input id="address_update" type="text" class="form-control input_config"
                       value="<?= isset($data) ? $data->address : '' ?>" placeholder="Địa chỉ cụ thể">
            </div>
            <div class="form-group">
                <button style="width: 100%;" class="btn btn-default btn_border">
                    <div class="d-flex align-items-center">
                        <img src="../../assets/admin/app-assets/images/icon_dashboard/map.png" alt="">
                        <div style="margin: auto">
                            <span class="text-bold-700 f-12">Chọn vị trí trên bản đồ</span>
                            <p class="text-sliver m-0 f-12">Giúp đơn hàng được giao nhanh nhất</p>
                        </div>
                    </div>
                </button>
            </div>
            <div class="form-group">
                <input id="type_default_update" value="1" name="type_default" type="checkbox" @if($data->default == 1) checked @endif ><span class="ml-1 text-bold-700">Đặt địa chỉ mặc định</span>
                <br>
                <input id="type_pick_up_update" value="1" name="type_pick_up" type="checkbox" @if($data->type_pick_up == 1) checked @endif><span class="ml-1 text-bold-700">Đặt địa chỉ lấy hàng</span>
                <br>
                <input id="type_return_update" value="1" name="type_return" type="checkbox" @if($data->type_return == 1) checked @endif><span class="ml-1 text-bold-700">Đặt địa chỉ trả hàng</span>
            </div>
        </div>
        <div class="d-flex justify-content-lg-end align-items-center">
            <button class="btn btn_main btn-cancel text-bold-700">Trở lại</button>
            <button class="btn btn_main btn-update-address ml-2 text-bold-700"
                    data-customer="{{isset($data_login) ? $data_login->customer_id : ''}}"
                    data-id="<?= isset($data) ? $data->id : '' ?>"
                    data-url="<?= URL::to('admin/profile/address/update') ?>">
            Hoàn thành
            </button>
        </div>
    </div>
</div>
