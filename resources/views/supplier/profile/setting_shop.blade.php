<?php
$page = 'shop_setting';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #47AA04;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>
<style>
</style>
<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card m-2">
            <div class="p-2">
                <h3 class="text-bold-700 f-18">{{$title}}</h3>
                <p class="text-sliver f-16">Thay đổi các cài đặt cho shop của bạn</p>
            </div>
            <div class="border-bottom"></div>
            <div class="p-2">
                <div class="tab-bar-page">
                    <ul class="tab-bar-content d-flex align-items-center justify-content-lg-start flex-wrap">
                        <li class="d-flex align-items-center justify-content-lg-center @if($type == 'setting_basic') active @endif setting_basic">
                            <a href="{{url('admin/profile/setting?type=setting_basic')}}" class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Thiết
                                lập cơ bản</a>
                        </li>
                        <li class="d-flex align-items-center justify-content-lg-center @if($type == 'setting_private') active @endif setting_private">
                            <a href="{{url('admin/profile/setting?type=setting_private')}}" class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Cài
                                đặt riêng tư</a>
                        </li>
                        <li class="d-flex align-items-center justify-content-lg-center @if($type == 'setting_chat') active @endif setting_chat">
                            <a href="{{url('admin/profile/setting?type=setting_chat')}}" class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Cài
                                đặt chat</a>
                        </li>
                        <li class="d-flex align-items-center justify-content-lg-center @if($type == 'setting_notification') active @endif setting_notification">
                            <a href="{{url('admin/profile/setting?type=setting_notification')}}" class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Cài
                                đặt thông báo</a>
                        </li>
                    </ul>
                </div>
                <!-- thiết lập cơ bản -->
                @if($type == 'setting_basic')
                    <div class="content_info">
                        <div class="d-flex align-items-center mt-4">
                            <img class="mr-3" width="44px" height="44px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/khien.png" alt="">
                            <div class="text_content">
                                <p class="text-bold-700 f-16">Bảo vệ tài khoản của bạn ngay bây giờ bằng cách xác minh
                                    hoạt
                                    động đáng ngờ</p>
                                <p class="text-sliver f-14 m-0">Trong trường hợp hoạt động có rủi ro cao, hãy xác minh
                                    hoạt
                                    động với OTP</p>
                            </div>
                            <label class="switch position-absolute" style="right: 2%;">
                                <input class="btn_check" type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="border-bottom mt-2"></div>
                        <div class="d-flex align-items-center mt-2">
                            <img class="mr-3" width="44px" height="44px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/mattrang.png" alt="">
                            <div class="text_content">
                                <p class="text-bold-700 f-16">Chế độ tạm nghỉ</p>
                                <p style="width: 80%;" class="text-sliver f-14 m-0">Kích hoạt chế độ tạm nghỉ để ngăn
                                    khách
                                    hàng đặt đơn hàng mới. Những đơn hàng đang tiến hành vẫn phải được xử lý</p>
                            </div>
                            <label class="switch position-absolute" style="right: 2%;">
                                <input class="btn_check" type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="border-bottom mt-2"></div>
                        <div class="d-flex align-items-center mt-2">
                            <img class="mr-3" width="44px" height="44px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/ngonngu.png" alt="">
                            <div class="text_content">
                                <p class="text-bold-700 f-16 m-0">Ngôn ngữ</p>
                            </div>
                            <div class="d-flex align-items-center position-absolute" style="right: 2%;">
                                <label class="container_radio m-0">
                                    <span class="text-bold-700">Vietnamese</span>
                                    <input checked type="radio" name="radio">
                                    <span class="radio_config"></span>
                                </label>
                                <div class="mr-2"></div>
                                <label class="container_radio m-0">
                                    <span class="text-bold-700">English</span>
                                    <input type="radio" name="radio">
                                    <span class="radio_config"></span>
                                </label>
                                <div class="mr-2"></div>
                                <label class="container_radio m-0">
                                    <span class="text-bold-700">Japan</span>
                                    <input type="radio" name="radio">
                                    <span class="radio_config"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                @elseif($type == 'setting_private')
                <!-- Cài đặt riêng tư -->
                    <div class="content_info">
                        <div class="d-flex align-items-center mt-4">
                            <img class="mr-3" width="44px" height="44px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/1_nguoi.png" alt="">
                            <div class="text_content">
                                <p class="text-bold-700 f-16">Hoạt động riêng tư</p>
                                <p style="width: 90%;" class="text-sliver f-14 m-0">Bật hoạt động riêng tư để ẩn hoạt
                                    động thích và bình luận của bạn khỏi những người đang theo dõi bạn.</p>
                            </div>
                            <label class="switch position-absolute" style="right: 2%;">
                                <input class="btn_check" type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="border-bottom mt-2"></div>
                        <div class="d-flex align-items-center mt-2">
                            <img class="mr-3" width="44px" height="44px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/traitim.png" alt="">
                            <div class="text_content">
                                <p class="text-bold-700 f-16">Ẩn thích của tôi</p>
                                <p style="width: 90%;" class="text-sliver f-14 m-0">Người khác sẽ không thấy lượt thích
                                    của bạn trong hồ sơ shop nếu kích hoạt.</p>
                            </div>
                            <label class="switch position-absolute" style="right: 2%;">
                                <input class="btn_check" type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                @elseif($type == 'setting_chat')
                <!-- Cài đặt chat -->
                    <div class="content_info">
                        <div class="d-flex align-items-center mt-4">
                            <img class="mr-3" width="44px" height="44px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/ngoisao.png" alt="">
                            <div class="text_content">
                                <p class="text-bold-700 f-16">Cho phép Trả giá</p>
                                <p class="text-sliver f-14 m-0">Nếu tắt chức năng này, Người mua sẽ không thể trả
                                    giá.</p>
                            </div>
                            <label class="switch position-absolute" style="right: 2%;">
                                <input class="btn_check" type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="border-bottom mt-2"></div>
                        <div class="d-flex align-items-center mt-2">
                            <img class="mr-3" width="44px" height="44px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/chat.png" alt="">
                            <div class="text_content">
                                <p class="text-bold-700 f-16">Cho phép Chat từ trang Hồ sơ</p>
                                <p class="text-sliver f-14 m-0">Bật để cho phép mọi người chat với bạn qua trang Hồ sơ.
                                    Khuyến khích với người mua.</p>
                            </div>
                            <label class="switch position-absolute" style="right: 2%;">
                                <input class="btn_check" type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="border-bottom mt-2"></div>
                        <div class="d-flex align-items-center mt-2">
                            <img class="mr-3" width="44px" height="44px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/nhomnguoi.png" alt="">
                            <div class="text_content">
                                <p class="text-bold-700 f-16">Người dùng đã bị chặn</p>
                                <p class="text-sliver f-14 m-0">Danh sách người dùng bạn đã chặn từ Chat.</p>
                            </div>
                            <select class="form-control input_config position-absolute" style="right: 2%; width: auto;"
                                    id="">
                                <option class="input_config" value="">Mở rộng</option>
                            </select>
                        </div>
                        <div class="row">
                            <div class="mt-2 col-lg-4">
                                <div class="block_out d-flex align-items-center p-1 border_xam border_radius mr-2">
                                    <div class="avatar_block position-relative mr-1">
                                        <img class="border-tron" width="60px" height="60px"
                                             src="https://i.pinimg.com/originals/7c/43/7b/7c437bc9adad0baf00366b73a4634dd8.jpg"
                                             alt="">
                                        <div class="icon_ban bg-white border-tron text-center position-absolute"
                                             style="width: 20px; height: 20px; bottom: 0; right: 0;">
                                            <i class="fa fa-ban fa-lg" style="color: #D1292F;"></i>
                                        </div>
                                    </div>
                                    <p style="width: 90%;" class="text-bold-700 m-0 f-14">Nguyễn Thế Quân</p>
                                    <div class="mr-1"></div>
                                    <button style="width: 100%;" class="btn btn_main">Bỏ chặn</button>
                                </div>
                            </div>
                            <div class="mt-2 col-lg-4">
                                <div class="block_out d-flex align-items-center p-1 border_xam border_radius mr-2">
                                    <div class="avatar_block position-relative mr-1">
                                        <img class="border-tron" width="60px" height="60px"
                                             src="https://i.pinimg.com/originals/7c/43/7b/7c437bc9adad0baf00366b73a4634dd8.jpg"
                                             alt="">
                                        <div class="icon_ban bg-white border-tron text-center position-absolute"
                                             style="width: 20px; height: 20px; bottom: 0; right: 0;">
                                            <i class="fa fa-ban fa-lg" style="color: #D1292F;"></i>
                                        </div>
                                    </div>
                                    <p style="width: 90%;" class="text-bold-700 m-0 f-14">Nguyễn Thế Quân</p>
                                    <div class="mr-1"></div>
                                    <button style="width: 100%;" class="btn btn_main">Bỏ chặn</button>
                                </div>
                            </div>
                            <div class="mt-2 col-lg-4">
                                <div class="block_out d-flex align-items-center p-1 border_xam border_radius mr-2">
                                    <div class="avatar_block position-relative mr-1">
                                        <img class="border-tron" width="60px" height="60px"
                                             src="https://i.pinimg.com/originals/7c/43/7b/7c437bc9adad0baf00366b73a4634dd8.jpg"
                                             alt="">
                                        <div class="icon_ban bg-white border-tron text-center position-absolute"
                                             style="width: 20px; height: 20px; bottom: 0; right: 0;">
                                            <i class="fa fa-ban fa-lg" style="color: #D1292F;"></i>
                                        </div>
                                    </div>
                                    <p style="width: 90%;" class="text-bold-700 m-0 f-14">Nguyễn Thế Quân</p>
                                    <div class="mr-1"></div>
                                    <button style="width: 100%;" class="btn btn_main">Bỏ chặn</button>
                                </div>
                            </div>
                            <div class="mt-2 col-lg-4">
                                <div class="block_out d-flex align-items-center p-1 border_xam border_radius mr-2">
                                    <div class="avatar_block position-relative mr-1">
                                        <img class="border-tron" width="60px" height="60px"
                                             src="https://i.pinimg.com/originals/7c/43/7b/7c437bc9adad0baf00366b73a4634dd8.jpg"
                                             alt="">
                                        <div class="icon_ban bg-white border-tron text-center position-absolute"
                                             style="width: 20px; height: 20px; bottom: 0; right: 0;">
                                            <i class="fa fa-ban fa-lg" style="color: #D1292F;"></i>
                                        </div>
                                    </div>
                                    <p style="width: 90%;" class="text-bold-700 m-0 f-14">Nguyễn Thế Quân</p>
                                    <div class="mr-1"></div>
                                    <button style="width: 100%;" class="btn btn_main">Bỏ chặn</button>
                                </div>
                            </div>
                            <div class="mt-2 col-lg-4">
                                <div class="block_out d-flex align-items-center p-1 border_xam border_radius mr-2">
                                    <div class="avatar_block position-relative mr-1">
                                        <img class="border-tron" width="60px" height="60px"
                                             src="https://i.pinimg.com/originals/7c/43/7b/7c437bc9adad0baf00366b73a4634dd8.jpg"
                                             alt="">
                                        <div class="icon_ban bg-white border-tron text-center position-absolute"
                                             style="width: 20px; height: 20px; bottom: 0; right: 0;">
                                            <i class="fa fa-ban fa-lg" style="color: #D1292F;"></i>
                                        </div>
                                    </div>
                                    <p style="width: 90%;" class="text-bold-700 m-0 f-14">Nguyễn Thế Quân</p>
                                    <div class="mr-1"></div>
                                    <button style="width: 100%;" class="btn btn_main">Bỏ chặn</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                <!-- Cài đặt thông báo -->
                    <div class="content_info">
                        <div class="d-flex align-items-center mt-4">
                            <div class="content-info">
                                <p class="text-bold-700 f-16">Thông báo email</p>
                                <p class="text-sliver f-14">Kiểm soát sàn gửi thông báo đến.</p>
                            </div>
                            <button class="btn btn_main position-absolute" style="right: 2%;">Bỏ email</button>
                        </div>
                        <div class="d-flex align-items-center mt-2">
                            <img class="mr-3" width="44px" height="44px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/ngoisao.png" alt="">
                            <div class="text_content">
                                <p class="text-bold-700 f-16 m-0">Cập nhật đơn hàng</p>
                            </div>
                            <label class="switch position-absolute" style="right: 2%;">
                                <input class="btn_check" type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="border-bottom mt-2"></div>
                        <div class="d-flex align-items-center mt-2">
                            <img class="mr-3" width="44px" height="44px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/mattrang.png" alt="">
                            <div class="text_content">
                                <p class="text-bold-700 f-16 m-0">Cập nhật sản phẩm</p>
                            </div>
                            <label class="switch position-absolute" style="right: 2%;">
                                <input class="btn_check" type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="border-bottom mt-2"></div>
                        <div class="d-flex align-items-center mt-2">
                            <img class="mr-3" width="44px" height="44px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/nhomnguoi.png" alt="">
                            <div class="text_content">
                                <p class="text-bold-700 f-16 m-0">Thông tin cá nhân</p>
                            </div>
                            <label class="switch position-absolute" style="right: 2%;">
                                <input class="btn_check" type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {

    })
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
