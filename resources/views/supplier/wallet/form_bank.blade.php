<?php
$page = 'wallet';
$data_login = Session::get('data_supplier');
use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="css/home/style.css">
<link rel="stylesheet" type="text/css" href="css/register/register_ncc.css">
<script src="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.js"></script>
<link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.css"/>
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<link href="css/mobiscroll/mobiscroll.jquery.min.css" rel="stylesheet"/>
<style>
    .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 35px;
        background: #ffe6e6;
        border-radius: 0.1875rem;
        outline: none;
        -webkit-transition: .2s;
        transition: opacity .2s;
        /*pointer-events: none;*/
    }

    .slider:hover {
        opacity: 1;
    }

    .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 45px;
        height: 40px;
        cursor: pointer;
        background-color: red;
        border-radius: 3px 0 0 3px !important;
    }

    .slider::-moz-range-thumb {
        width: 45px;
        height: 35px;
        background: #04AA6D;
        cursor: pointer;
    }
</style>

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label class="lb">Chủ tài khoản</label>
                                <input type="text" name="user_name" id="user_name" placeholder="Chủ tài khoản" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Ngân hàng</label>
                                <select name="bank_name" id="bank_name" class="form-control">
                                    <?php foreach ($dataBank as $value) :?>
                                    <option value="{{$value->id}}">{{$value->name.' ('.$value->short_name.')'}}</option>
                                    <?php endforeach; ?>
                                </select>
                                {{--                                <input type="text" name="bank_name" id="bank_name" placeholder="Ngân hàng" class="form-control">--}}
                            </div>
                            <div class="form-group">
                                <label class="lb">Số tài khoản</label>
                                <input type="number" name="bank_account" id="bank_account" placeholder="Số tài khoản" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Vui lòng kiểm tra số điện thoại dưới đây để nhận mã OTP</label>
                                <input readonly type="number" placeholder="Số tài khoản" class="form-control input-phone" value="<?= isset($data_login) ? $data_login->phone : '' ?>">
                            </div>
                            <div class="form-group">
                                <div class="position-relative">
                                    <input type="range" min="1" max="100" value="0" class="slider" id="myRange">
                                    <p class="text-otp position-absolute position-center" style="">Trượt để nhận mã OTP</p>
                                </div>
                            </div>
                            <div id="recaptcha-container"></div>
                            <div class="form-group">
                                <label class="lb-1" for="">Mã OTP</label>
                                <input type="text" class="form-control form-otp" placeholder="Nhập Mã OTP được gửi về Số Điện Thoại"
                                       disabled>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label class="lb">Chi nhánh</label>
                                <input type="text" name="branch" id="branch" placeholder="Chi nhánh" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Số CMND</label>
                                <input type="text" name="identify_card" id="identify_card" placeholder="Số CMND" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Ngày kích hoạt</label>
                                <input type="date" name="date_active" id="date_active" placeholder="Ngày kích hoạt" class="form-control">
                            </div>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/manage_wallet/add') ?>"><i class="fa fa-floppy-o"
                                    data-shop="<?= isset($dataUser) ? $dataUser->id : '' ?>"
                                                                                                                aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/manage_wallet')?>" class="btn btn-danger"
                               style="color: white"><i class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<!-- BEGIN: Footer-->
@include('admin.base.footer')
<!-- END: Footer-->
@include('admin.base.script')
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-firestore.js"></script>
<script src="js/otp/index.js"></script>
<script src="js/mobiscroll/mobiscroll.jquery.min.js"></script>
</body>
<script>
    $(document).ready(function () {
        // $(document).on("click", ".btn-add-account", function () {
        //     let url = $(this).attr('data-url');
        //     let form_data = new FormData();
        //     form_data.append('id', $(this).attr('data-id'));
        //     form_data.append('user_name', $('#user_name').val());
        //     form_data.append('bank_name', $('#bank_name').val());
        //     form_data.append('bank_account', $('#bank_account').val());
        //     form_data.append('date_active', $('#date_active').val());
        //     form_data.append('shop_id', $(this).attr('data-shop'));
        //     form_data.append('identify_card', $('#identify_card').val());
        //     form_data.append('branch', $('#branch').val());
        //     $.ajaxSetup({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         }
        //     });
        //     $.ajax({
        //         url: url,
        //         type: "POST",
        //         data: form_data,
        //         dataType: 'json',
        //         contentType: false,
        //         processData: false,
        //         beforeSend: function () {
        //             showLoading();
        //         },
        //         success: function (data) {
        //             if (data.status) {
        //                 window.location.href = data.url;
        //             } else {
        //                 alert(data.msg);
        //             }
        //         },
        //         error: function () {
        //             console.log('error')
        //         },
        //         complete: function () {
        //             hideLoading();
        //         }
        //     });
        // });

        let verificationId;
        $(document).on('change', '#myRange', async function () {
            let value = $(this).val();
            if (value == 100) {
                $('#recaptcha-container').show();
                let applicationVerifier = new firebase.auth.RecaptchaVerifier(
                    'recaptcha-container');
                let phone = $('.input-phone').val();
                let fs = phone.slice(0, 3);
                if (fs !== '+84') {
                    phone = phone.slice(1, phone.length);
                    phone = '+84' + phone;
                }
                let provider = new firebase.auth.PhoneAuthProvider();
                try {
                    verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                    $('#recaptcha-container').hide();
                    $('.form-otp').prop('disabled', false);
                } catch (e) {
                    console.log('e');
                    swal("Thông báo", "Số điện thoại vừa nhập không đúng !", "error");
                    // setTimeout();
                    // location.reload();
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                }
            }
        });

        $(document).on('click', '.btn-add-account', async function () {
            let data = [];
            data['user_name'] = $('#user_name').val();
            data['bank_name'] = $('#bank_name').val();
            data['bank_account'] = $('#bank_account').val();
            data['date_active'] = $('#date_active').val();
            data['identify_card'] = $('#identify_card').val();
            data['branch'] = $('#branch').val();
            if(!data['user_name'] || !data['bank_account'] || !data['identify_card'] || !data['branch'] || !data['date_active']){
                swal("Thông báo", "Vui lòng nhập đủ thông tin !", "error");
                return false;
            }
            try {
                let verificationCode = $('.form-otp').val();
                let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
                try {
                    await firebase.auth().signInWithCredential(phoneCredential);
                    let url = $(this).attr('data-url');
                    let form_data = new FormData();
                    form_data.append('user_name', $('#user_name').val());
                    form_data.append('bank_name', $('#bank_name').val());
                    form_data.append('bank_account', $('#bank_account').val());
                    form_data.append('date_active', $('#date_active').val());
                    form_data.append('identify_card', $('#identify_card').val());
                    form_data.append('branch', $('#branch').val());
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: form_data,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status) {
                                swal("Thông báo", data.msg, "success");
                                window.location.href = data.url;
                            } else {
                                swal("Thông báo", data.msg, "error");
                            }
                        },
                        error: function () {
                            console.log('error');
                        },
                    });
                } catch (e) {
                    swal("Thông báo", "Mã OTP không chính xác !", "error");
                }
            } catch (e) {
                swal("Thông báo", "Đã có lỗi xảy ra !", "error");
            }
        });
    });
</script>
<!-- END: Page JS-->


<!-- END: Body-->

</html>

