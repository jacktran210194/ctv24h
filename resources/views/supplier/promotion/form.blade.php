<?php
$page = 'promotion';
use Illuminate\Support\Facades\File;use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <h4>Tạo mã giảm giá</h4>
                            <div class="form-group">
                                <label class="lb">Loại mã</label>
                                <select id="apply_for" class="form-control apply_for">
                                    <option <?= isset($data) ? $data->apply_for == 1 : 'selected' ?> value="1">Voucher sản phẩm</option>
                                    <option <?= isset($data) ? $data->apply_for == 0 : 'selected' ?> value="0">Voucher toàn shop</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="product_id" class="form-control product">
                                    @if(isset($dataProduct))
                                        @foreach($dataProduct as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="lb">Tên chương trình giảm giá</label>
                                <input type="text" id="title" placeholder="Tên chương trình giảm giá"
                                       value="<?= isset($data) ? $data['title'] : ''?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Mã voucher</label>
                                <input type="text" id="code" placeholder="Mã voucher"
                                       value="<?= isset($data) ? $data['code'] : '' ?>"
                                       class="form-control">
                                <span style="color: #ff0000; font-style: italic">Mã Voucher gồm chữ và số, không chứa các kí tự đặc biệt</span>
                            </div>
                            <div class="form-group">
                                <label class="lb">Thời gian lưu mã giảm giá</label>
                                <div class="d-flex">
                                    <input type="datetime-local" id="time_start" placeholder="Thời gian bắt đầu"
                                           value="<?= isset($data) ? date('Y-m-d\TH:i:s', strtotime($data['time_start'])) : date('Y-m-d\TH:i:s') ?>"
                                           class="form-control mr-2">
                                    <input type="datetime-local" id="time_end" placeholder="Thời gian kết thúc"
                                           value="<?= isset($data) ? date('Y-m-d\TH:i:s', strtotime($data['time_end'])) : date('Y-m-d\TH:i:s') ?>"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="form-label-group">
                                    <input type='file' value="" accept="image/*" name="image" id="image" onchange="changeImg(this)">
                                    <br>
                                    <img width="120px" height="120px" id="avatar" class="thumbnail image-show-qr"
                                         src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/images/avt.jpg' ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <h4>Thiết lập mã giảm giá</h4>
                            <div class="form-group">
                                <label class="lb">Loại voucher</label>
                                <select id="type" class="form-control">
                                    <option <?= isset($data) ? $data->type == 0 : 'selected' ?> value="0">Voucher khuyến mại</option>
                                    <option <?= isset($data) ? $data->type == 1 : 'selected' ?> value="1">Voucher hoàn xu</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="lb">Loại giảm giá/mức giảm</label>
                                <div class="d-flex">
                                    <select id="money_or_percent" class="form-control mr-1">
                                        <option <?= isset($data) ? $data->money_or_percent == 1 : 'selected' ?> value="1">Theo %</option>
                                        <option <?= isset($data) ? $data->money_or_percent == 0 : 'selected' ?> value="0">Theo số tiền</option>
                                    </select>
                                    <input type="number" id="value_discount" placeholder="Giá trị giảm"
                                           value="<?= isset($data) ? $data['value_discount'] : '' ?>"
                                           class="form-control mr-1">
                                    <input type="number" id="maximum" placeholder="Giảm tối đa"
                                           value="<?= isset($data) ? $data['maximum'] : '' ?>"
                                           class="form-control maximum-discount">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="lb">Giá trị đơn hàng tối thiểu</label>
                                <input type="number" id="value_order_min" placeholder="Giá trị đơn hàng tối thiểu"
                                       value="<?= isset($data) ? $data['value_order_min'] : ''?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Số mã có thể lưu</label>
                                <input type="number" id="number_save" placeholder="Số mã có thể lưu"
                                       value="<?= isset($data) ? $data['number_save'] : '' ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Số mã có thể sử dụng</label>
                                <input type="number" id="number_use" placeholder="Số mã có thể sử dụng"
                                       value="<?= isset($data) ? $data['number_use'] : '' ?>"
                                       class="form-control">
                            </div>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/promotion/save') ?>"><i class="fa fa-floppy-o"
                                                                                            aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/promotion') ?>" class="btn btn-danger"
                               style="color: white"><i class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<!-- BEGIN: Footer-->
@include('admin.base.footer')
<!-- END: Footer-->
@include('admin.base.script')
<script type="text/javascript">
    $(document).ready(function () {

        $(".product").hide();
        $(".maximum-discount").hide();

        $('#code').on('keypress', function (event) {
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });

        $(document).on("change", ".apply_for", function () {
            if($(".apply_for").val() == 0){
                $(".product").hide();
            } else {
                $(".product").show();
            }
        });

        $(document).on("change", "#money_or_percent", function () {
           if($("#money_or_percent").val() == 1){
               $(".maximum-discount").show();
           } else {
               $(".maximum-discount").hide();
           }
        });
        if($(".maximum-discount").val() != ''){
            $(".maximum-discount").show();
        }
        $(document).on("click", ".btn-add-account", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('apply_for', $('#apply_for').val());
            form_data.append('title', $('#title').val());
            form_data.append('code', $('#code').val());
            form_data.append('time_start', $('#time_start').val());
            form_data.append('time_end', $('#time_end').val());
            form_data.append('type', $('#type').val());
            form_data.append('money_or_percent', $('#money_or_percent').val());
            form_data.append('value_discount', $('#value_discount').val());
            form_data.append('maximum', $('#maximum').val());
            form_data.append('value_order_min', $('#value_order_min').val());
            form_data.append('number_save', $('#number_save').val());
            form_data.append('number_use', $('#number_use').val());
            form_data.append('product_id', $('#product_id').val());
            form_data.append('image', $('#image')[0].files[0]);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });
</script>
<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>

