<?php
use Illuminate\Support\Facades\URL;
?>
@foreach($attr as $value)
    <div class="form-group form_attr">
        <div class="form-group d-flex">
            <label style="width: 8%;">Thuộc tính</label>
            <input value="{{$value->name}}" style="width: 35%;" data-id="{{$value->id}}" class="attribute_name form-control" type="text">
            <button data-url="{{url('admin/product_supplier/delete_attr/'.$value->id)}}"
                    class="btn-delete-attr btn btn-warning"><i class='fa fa-trash'></i></button>
        </div>
        <?php $i = 0; ?>
        @foreach ($value['item'] as $item)
            <div class="form-group d-flex">
                <label style="width: 8%;">Giá trị</label>
                <input style="width: 35%;" data-id="{{$item->id}}" value="{{$item['value']}}" class="attribute_value form-control" type="text">
                @if($i === 0)
                    <button data-url="<?= URL::to('admin/product_supplier/form_item') ?>"
                            class="btn-add-value btn btn-primary"><i class='fa fa-plus'></i></button>
                @else
                    <button class="btn-delete-value btn-delete-item btn btn-danger" data-url={{url('admin/product_supplier/delete_item/'.$item->id)}}><i class='fa fa-times'></i></button>
                @endif
                <?php $i++ ?>
            </div>
        @endforeach
        <div class="form-group">
            <div class="form-1"></div>
        </div>
    </div>
@endforeach
