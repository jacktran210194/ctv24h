<div class="form-group form_attr">
    <div class="form-group d-flex align-items-center">
        <label style="width: 11%;color:#0D7091;" class="text-bold-700">Nhóm phân loại</label>
        <input style="width: 20%;" class="attribute_name form-control" type="text">
    </div>
    <div class="form-group d-flex align-items-center form-item">
        <label style="width: 11%;color: #ff634a">Tên phân loại</label>
        <input style="width: 20%;" class="attribute_value form-control mr-1" type="text">
        <label style="color: #ff634a">Đơn giá</label>
        <input style="width: 20%" type="number" class="price_attr_value form-control mr-1 ml-1">
        <label style="color: #ff634a">Tồn kho</label>
        <input style="width: 10%" type="number" class="quantity_attr_value form-control mr-1 ml-1">
        <button data-url="<?= URL::to('admin/product_supplier/form_item') ?>"
                class="btn-add-value btn btn-primary"><i class='fa fa-plus'></i></button>
    </div>
    <div class="form-group">
        <div class="form-1"></div>
    </div>
</div>
