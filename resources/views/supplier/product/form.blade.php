<?php
$page = 'product_supplier_add';
$data_login = Session::get('data_supplier');
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
    .input_size::placeholder{
        font-size: 11px;
        color: #C7C7C7;
    }
    .label_percent{
        top: 50%;
        right: 5px;
        transform: translate(-50%, -50%);
        font-weight: bold;
        font-size: 16px;
    }
</style>

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    {{--                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>--}}
                    <h5>Trang chủ > sản phẩm > Thêm sản phẩm </h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="head_form">
            <div class="card">
                <div class="card-content">
                    <div class="m-1">
                        <div class="row">
                            <div class="w-100 p-2">
                                <h2 class="font-size-15 color-black text-uppercase" style="margin-bottom: 20px">Thêm sản
                                    phẩm mới </h2>
                                <div class="form-group d-flex align-items-center">
                                    <label class="font-weight-bold font-size-14 input_required" style="width: 150px">Tên
                                        sản
                                        phẩm</label>
                                    <input required type="text" name="name" id="name" placeholder="Tên sản phẩm"
                                           value="<?= isset($data) ? $data['name'] : '' ?>" class="form-control w-70">
                                </div>
                                <div class="form-group d-flex">
                                    <label class="font-weight-bold font-size-14 input_required" style="width: 150px">Danh
                                        mục</label>
                                    <ul class="p-1 content-category border_box border-radius-8">
                                        @foreach($category_shop as $value)
                                            <li data-value="{{$value->id}}" data-name="{{$value->name}}"
                                                class="@if(!empty($value)) before @endif @if(isset($data->category_product) && $data->category_product == $value->id ) active @endif">{{$value->name}}
                                            </li>
                                        @endforeach
                                    </ul>
                                    <ul class="p-1 content-category category-child-1 border-radius-8 border_box">
                                        @if(isset($category_product_child))
                                            @foreach($category_product_child as $item)
                                                <li>{{$item->name}}</li>
                                            @endforeach
                                        @endif
                                        @if(isset($data->category_product))
                                            @foreach($data_category_product as $k)
                                                <li data-name="{{$k->name}}" data-value="{{$k->id}}"
                                                    class="before @if(isset($data->category_id) && $data->category_id == $k->id) active @endif"
                                                >{{$k->name}}</li>
                                            @endforeach
                                        @endif
                                    </ul>
                                    <ul class="p-1 content-category category-child-2 border-radius-8 border_box">
                                        @if(isset($data->category_product))
                                            @foreach($data_product_child as $k)
                                                <li data-name="{{$k->name}}" data-value="{{$k->id}}"
                                                    class="@if(isset($data->category_sub_child) && $data->category_sub_child == $k->id) active @endif"
                                                >{{$k->name}}</li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                <span class="text-red text-center">(Vui lòng chọn đầy đủ 3 loại danh mục cần thiết cho sản phẩm của bạn)</span>
                            </div>
                        </div>
                        <p class="font-weight-bold font-size-14 color-black">Đã chọn: <span
                                class="color-red name-category"></span> <span class="color-red name-category-1"> </span>
                            <span class="color-red name-category-2"></span></p>
                        <button class="bg-red color-white border-radius-16 font-size-14 btn-next"
                                @if(isset($data->category_product)) @else disabled @endif>Tiếp theo
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="information-sales" style="display: none;">
            <div class="card mg-top-30">
                <div class="card-content">
                    <div class="row p-2 m-1">
                        <div class="w-100">
                            <h2 class="font-size-15 color-black text-uppercase" style="margin-bottom: 20px">Thông tin
                                bán hàng</h2>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px">Thương
                                    hiệu</label>
                                <select class="form-control w-70" id="brand">
                                    <option @isset($data) @if($data['brand']==0) selected
                                            @endif @endisset value="0">No
                                        Brand
                                    </option>
                                    @foreach($brand as $value)
                                        <option @isset($data) @if($data['brand'] == $value['id'])  selected
                                                @endif @endisset value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 150px">SKU phân
                                    loại</label>
                                <input type="number" name="sku_type" id="sku_type" placeholder="Phân loại"
                                       value="<?= isset($data) ? $data['sku_type'] : '' ?>"
                                       class="form-control w-70">
                            </div>
                            <div class="form-group d-flex">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px"
                                       for="">Bài đăng mẫu</label>
                                <textarea class="form-control ckeditor " name="" id="sort_desc" cols="30"
                                          rows="4">{{isset($data) ? $data['sort_desc'] : ''}}</textarea>
                            </div>
                            <div class="form-group d-flex">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px">Mô
                                    tả
                                    sản phẩm</label>
                                <textarea class="form-control ckeditor" name="desc" id="desc" cols="30"
                                          rows="3"><?= isset($data) ? $data['desc'] : '' ?></textarea>
                            </div>
                            <label class="font-weight-bold font-size-14 mb-1 input_required" for="">Phân loại hàng
                                hoá </label>
                            <div class="d-flex align-items-center">
                                <div style="width: 150px"></div>
                                <label class="font-weight-bold font-size-17 text-center" style="width: 300px">Nhóm phân loại</label>
                                <label class="font-weight-bold font-size-17 text-center" style="width: 300px">Phân loại</label>
                            </div>
                            <div class="d-flex align-items-center" style="margin-bottom: 30px">
                                <div style="width: 150px"></div>
                                <div class="w-75">
                                    <div data-category>
                                        <div list-category>
                                            <div class="d-flex align-items-center list-container-category" style="margin-bottom: 20px">
                                                <input class="form-control input_title" type="text"  placeholder="Nhập tên Nhóm phân loại hàng, ví dụ: màu sắc, kích thước v.v" style="max-width: 280px; margin-right: 10px;">
                                                <input class="form-control input_value" type="text"  placeholder="Nhập phân loại hàng, ví dụ: Trắng, Đỏ v.v" style="max-width: 280px; margin-right: 10px;"">
                                                <input type="hidden" name="value_image" value="">
                                                <button class="color-white font-weight-bold bg-red border-radius-4 btn-add-list" style="padding: 8px; border: none;"><span style="margin-right: 10px;">Thêm PL</span> <i class="fa fa-plus"></i></button>
                                            </div>
                                            <div data-content-category class="list-data-category">
                                                <div class="d-flex justify-content-between align-items-center" style="margin-bottom: 20px">
                                                    <div style="width: 300px;">
                                                        <input class="form-control input_title" style="margin-right: 5px;" type="text" placeholder="Nhập tên phân loại, ví dụ: Size, v.v" name="attr[0][category_1][sub][0][title]">
                                                    </div>
                                                    <div class="content-list" style="padding-left: 10px; margin-left: 10px; border-left: 1px solid #DDDDDD">
                                                        <div class="d-flex justify-content-between align-items-center" content_value>
                                                            <input class="form-control input_name" style="max-width: 300px; margin-right: 5px;" type="text" placeholder="Nhập phân loại, ví dụ: S, M, v.v" name="attr[0][category_1][sub][0][name]">
                                                            <input class="form-control input_price" style="max-width: 300px; margin-right: 5px;" type="text" placeholder="giá tham khảo" name="attr[0][category_1][sub][0][price]">
                                                            <input class="form-control input_price_ctv" style="max-width: 300px; margin-right: 5px;" type="text" placeholder="giá bán ctv" name="attr[0][category_1][sub][0][price_ctv]">
                                                            <input class="form-control input_quantity" style="max-width: 300px; margin-right: 5px;" type="number" placeholder="tồn kho" name="attr[0][category_1][sub][0][quantity]">
                                                            <div style="width: 220px"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="color-white font-weight-bold bg-red border-radius-4 btn-add-list-attr" style="padding: 8px; border: none;"><span style="margin-right: 10px;">Thêm Nhóm PL</span> <i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px" for="">Màu sắc</label>
                                <div class="w-70 d-flex align-items-center" list-image-variant>
                                    <input hidden class="input_image_2" type='file' value="" accept="image/*" id="input_image_2">
                                    <div class="content_button" style="width: 120px;margin-right: 15px;">
                                        <button
                                            class="btn w-100 btn-add-image position-relative border-radius-8 border_box d-flex justify-content-lg-center align-items-center"
                                            style="height: 120px; border: 1px solid #ECECEC;"
                                        >
                                            <img src="../../assets/admin/app-assets/images/icons/add_image.png">
                                        </button>
                                        <p class="font-weight-bold color-black font-size-17 text-center" style="margin-top: 10px; margin-bottom: 0">Phân loại hàng</p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px" for="">Tồn kho</label>
                                <input class="form-control w-70" type="number" disabled name="warehouse" id="warehouse"
                                       value="<?= isset($data) ? $data['warehouse'] : '' ?>">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px" for="">Tình
                                    trạng</label>
                                <select id="status_product" class="form-control w-70">
                                    <option value="1">Hàng mới</option>
                                    <option value="0">Đã qua sử dụng</option>
                                </select>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px" for="">Xuất
                                    xứ</label>
                                <input class="form-control w-70" type="text" name="original" id="original"
                                       value="<?= isset($data) ? $data['original'] : '' ?>">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px" for="">Chất
                                    liệu</label>
                                <input class="form-control w-70" type="text" name="material" id="material"
                                       value="<?= isset($data) ? $data['material'] : '' ?>">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px" for="">Gửi
                                    từ</label>
                                <select id="place" class="form-control w-70">
                                    @if(isset($data_address))
                                        @foreach($data_address as $value)
                                            <option value="{{$value->id}}">{{$value->address}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                {{--                                <input readonly class="form-control w-70" type="text" name="place" id="place"--}}
                                {{--                                       value="{{isset($data_login) ? $data_login->address : ''}}">--}}
                            </div>
                            <div class="form-group d-flex">
                                <label class="font-weight-bold font-size-14" style="width: 150px" for="">Mua nhiều giảm giá</label>
                                <div class="w-70">
                                    <div class="d-flex align-items-center">
                                        <p class="m-0 w-25 text-center font-weight-bold" style="margin-right: 10px">Từ (sản phẩm)</p>
                                        <p class="m-0 w-25 text-center font-weight-bold" style="margin-right: 10px">Đến (sản phẩm)</p>
                                        <p class="m-0 w-25 text-center font-weight-bold">Giảm giá (phần trăm)</p>
                                    </div>
                                    <div class="list-buy-percent" style="margin-top: 20px">
                                        <div class="content_list d-flex justify-content-between align-items-center">
                                            <input class="form-control w-25 input_quantity_1" type="number" style="margin-right: 10px">
                                            <input class="form-control w-25 input_quantity_2" type="number" style="margin-right: 10px">
                                            <div class="w-25 position-relative" style="margin-right: 10px">
                                                <input class="form-control input_percent" type="number">
                                                <label class="m-0 position-absolute label_percent">%</label>
                                            </div>
                                            <button class="form-control btn-add-percent bg-red w-25 color-white font-weight-bold" style="border: none">
                                                <img src="../../assets/admin/app-assets/images/icons/add.png">
                                                <span style="padding-left: 5px">Thêm giảm giá</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mg-top-30">
                <div class="card-content p-2 m-1">
                    <h2 class="font-size-15 color-black text-uppercase" style="margin-bottom: 20px">quản lý hình
                        ảnh </h2>
                    <div class="form-group d-flex align-items-center">
                        <input hidden class="input_image" type='file' value="" accept="image/*" id="image"
                               onchange="changeImg(this)">
                        <p class="font-size-14 font-weight-bold input_required"
                           style="margin-right: 15px;min-width: 120px;">Ảnh đại diện:</p>
                        <button
                            class="btn position-relative btn-image-1 border-radius-8 border_box d-flex justify-content-lg-center align-items-center">
                            <img src="../../assets/admin/app-assets/images/icons/add_image.png"></button>
                        <div>
                            <p class="m-0">Để có giao diện sản phẩm đẹp hơn trên sàn CTV24H bạn lên lựa chọn:</p>
                            <p class="m-0">1: Kích thước ảnh sản phẩm là hình vuông có kích thước 500 x 500 pixel. </p>
                            <p class="m-0">2: Với độ phân giải 72dpi.. </p>
                        </div>
                    </div>
                    <div class="d-flex">
                        <p class="font-size-14 font-weight-bold input_required"
                           style="margin-right: 15px;min-width: 120px;">Ảnh sản phẩm:</p>
                        <div class="form-group d-flex align-items-center flex-wrap group-image">
                            <input hidden class="form-control input_list_image" type='file' accept="image/*"
                                   id="imageQr"
                                   multiple/>
                            <button
                                class="btn position-relative btn-show-list border-radius-8 border_box d-flex justify-content-lg-center align-items-center">
                                <img src="../../assets/admin/app-assets/images/icons/add_image.png">
                            </button>
                            <button
                                class="btn position-relative btn-show-list border-radius-8 border_box d-flex justify-content-lg-center align-items-center">
                                <img src="../../assets/admin/app-assets/images/icons/add_image.png">
                            </button>
                            <button
                                class="btn position-relative btn-show-list border-radius-8 border_box d-flex justify-content-lg-center align-items-center">
                                <img src="../../assets/admin/app-assets/images/icons/add_image.png">
                            </button>
                            <button
                                class="btn position-relative btn-show-list border-radius-8 border_box d-flex justify-content-lg-center align-items-center">
                                <img src="../../assets/admin/app-assets/images/icons/add_image.png">
                            </button>
                            <button
                                class="btn position-relative btn-show-list border-radius-8 border_box d-flex justify-content-lg-center align-items-center">
                                <img src="../../assets/admin/app-assets/images/icons/add_image.png">
                            </button>
                            <button
                                class="btn position-relative btn-show-list border-radius-8 border_box d-flex justify-content-lg-center align-items-center">
                                <img src="../../assets/admin/app-assets/images/icons/add_image.png">
                            </button>
                            <button
                                class="btn position-relative btn-show-list border-radius-8 border_box d-flex justify-content-lg-center align-items-center">
                                <img src="../../assets/admin/app-assets/images/icons/add_image.png">
                            </button>
                            <button
                                class="btn position-relative btn-show-list border-radius-8 border_box d-flex justify-content-lg-center align-items-center">
                                <img src="../../assets/admin/app-assets/images/icons/add_image.png">
                            </button>
                            <br>
                            <div class="row image-show">
                                <?php if (isset($data_image) && count($data_image)): ?>
                                <?php foreach ($data_image as $value): ?>
                                <div class="pip col-lg-2 col-md-3" style="position: relative">
                                    <img class="mr-2 mt-2" style="width: 100px; height: 100px;"
                                         src="<?= $value['image'] ?>"
                                         title=""><br>
                                    <button type="button" style="position: absolute;top: 20px;left: 120px;"
                                            class="fa fa-times btn btn-danger btn-delete-image"
                                            data-url="<?= URL::to('admin/product_supplier/delete_image/' . $value['id']) ?>"></button>
                                </div>
                                <?php  endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex">
                        <p class="font-size-14 font-weight-bold" style="margin-right: 15px;min-width: 120px;">Video:</p>
                        <input type="text" placeholder="Thêm URL video youtube" class="border-radius-6 border-red p-1"
                               id="video_product">
                    </div>
                    <div class="video-product"></div>
                </div>
            </div>
            <div class="card mg-top-30">
                <div class="card-content p-2 m-1">
                    <h2 class="font-size-15 color-black text-uppercase" style="margin-bottom: 20px">Vận chuyển</h2>
                </div>
                <div class="col-lg-12">
                    <div class="mt-2">
                        <table>
                            <tr>
                                <td class="font-weight-bold font-size-14 input_required">Cân nặng sau đóng gói:</td>
                                <td>
                                    <div class="d-flex border_box border-radius-6 align-items-center group-kg">
                                        <input type="number" id="weight"
                                               class="border-radius-6 input-form font-weight-bold font-size-14"
                                               value="{{isset($data) ? $data['weight'] : ''}}">
                                        <label class="gram">Gram</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold font-size-14">Kích thước đóng gói:</td>
                                <td>
                                    <div class="d-flex border_box border-radius-6 align-items-center group-kg">
                                        <input type="text"
                                               placeholder="Chiều rộng"
                                               class="border-radius-6 input-form input_size font-weight-bold font-size-14"
                                               id="length"
                                               value="{{isset($data) ? $data['length'] : ''}}">
                                        <label class="gram">Cm</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex border_box border-radius-6 align-items-center group-kg">
                                        <input type="text"
                                               placeholder="Chiều dài"
                                               class="border-radius-6 input-form input_size font-weight-bold font-size-14"
                                               id="width"
                                               value="{{isset($data) ? $data['width'] : ''}}">
                                        <label class="gram">Cm</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex border_box border-radius-6 align-items-center group-kg">
                                        <input type="text"
                                               placeholder="Chiều cao"
                                               class="border-radius-6 input-form input_size font-weight-bold font-size-14"
                                               id="height"
                                               value="{{isset($data) ? $data['height'] : ''}}">
                                        <label class="gram">Cm</label>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <p class="mt-2 font-weight-bold font-size-14 input_required">Chọn đơn vị vận chuyển</p>
                    <div class="p-2 border_box border-radius-16"
                         style="border: 1px solid #CCD3DC; position: relative; margin-bottom: 40px">
                        @isset($shipping)
                            @foreach($shipping as $value)
                                <div class="d-flex align-items-center mb-2">
                                    <p class="font-size-15 m-0">{{$value->shipping_name}} (Tối đa {{$value->max_weight}}
                                        g)</p>
                                    <p style="position: absolute; right: 10%;" class="m-0">
                                        <label class="switch">
                                            <input data-id="{{$value->shipping_id}}" type="checkbox"
                                                   class="btn_add_shipping" name="shipping_supplier[]">
                                            <span class="slider round"></span>
                                        </label>
                                    </p>
                                </div>
                            @endforeach
                        @endisset
                    </div>
                </div>
                <div class="d-flex align-items-center justify-content-lg-end" style="padding-bottom: 30px">
                    <a href="<?= URL::to('admin/product_supplier') ?>" class="btn btn-form-edit">Hủy bỏ</a>
                    <button class="btn btn-back btn-form">Quay lại</button>
                    <button class="btn-form btn-save-hide bg-white"
                            data-id="<?= isset($data) ? $data['id'] : '' ?>"
                            data-supplier="{{isset($data_login) ? $data_login['id'] : ''}}"
                    >Lưu và Ẩn
                    </button>
                    <button type="button" class="btn btn-form btn-add-account bg-red"
                            data-id="<?= isset($data) ? $data['id'] : '' ?>"
                            data-supplier="{{isset($data_login) ? $data_login['id'] : ''}}"
                            data-url="<?= URL::to('admin/product_supplier/save') ?>">
                        Lưu và Hiện
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@if(empty($data_address) || count($data_address) == 0)
<div class="position-fixed w-100 h-100 d-flex justify-content-center align-items-center" style="background: #33333330; top: 0; left: 0; z-index: 100">
<div class="bg-white" style="width: 400px; border-radius: 16px; padding: 20px">
    <h2 class="text-center text-red font-size-17">Bạn chưa thiết lập địa chỉ lấy hàng</h2>
    <p class="text-center mg-top-20">Vui lòng thiết lập địa chỉ để tiếp  tục</p>
    <div class="d-flex justify-content-center" style="margin-top: 20px">
        <a href="{{url('/admin/profile/address')}}" class="font-weight-bold" style="padding: 10px 20px; background: #d10e0c; color: #FFFFFF; border-radius: 8px">Thiết lập</a>
    </div>
</div>
</div>
@endif
<!-- END: Content-->
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
@include('admin.base.script_product')

{{--<script src="dashboard/products/index.js"></script>--}}
<script src="dashboard/products/add-product.js"></script>
<script src="dashboard/products/add-category.js"></script>
<script src="dashboard/products/create_product.js"></script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
