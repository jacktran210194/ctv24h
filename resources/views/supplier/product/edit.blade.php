<?php
$page = 'product_supplier_add';
$data_login = Session::get('data_supplier');
use Illuminate\Support\Facades\URL;
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
    [content_value]:first-child{
        margin-top: 0 !important;
    }
    .btn-add-list-image, .show-list-image {
        overflow: initial;
        width: 120px;
        height: 120px;
        border: 1px solid #ECECEC;
        margin: 5px;
        margin-right: 15px;
    }
    .icon_update{
        top: -6px;
        right: -6px;
        z-index: 10;
        cursor: pointer;
        width: 20px;
        height: 20px;
        background: #ddd;
        border-radius: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 0;
    }
</style>

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    {{--                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>--}}
                    <h5>Trang chủ > sản phẩm > Thêm sản phẩm </h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="head_form">
            <div class="card">
                <div class="card-content">
                    <div class="m-1">
                        <div class="row">
                            <div class="w-100 p-2">
                                <h2 class="font-size-15 color-black text-uppercase" style="margin-bottom: 20px">Thêm sản
                                    phẩm mới </h2>
                                <div class="form-group d-flex align-items-center">
                                    <label class="font-weight-bold font-size-14 input_required" style="width: 150px">Tên
                                        sản
                                        phẩm</label>
                                    <input required type="text" name="name" id="name" placeholder="Tên sản phẩm"
                                           value="<?= isset($data) ? $data['name'] : '' ?>" class="form-control w-70">
                                </div>
                                <div class="form-group d-flex align-items-center">
                                    <label class="font-weight-bold font-size-14 input_required" style="width: 150px">Thương
                                        hiệu</label>
                                    <select class="form-control w-70" id="brand">
                                        <option @isset($data) @if($data['brand']==0) selected
                                        @endif @endisset value="0">No
                                        Brand
                                        </option>
                                        @foreach($brand as $value)
                                        <option @isset($data) @if($data['brand'] == $value['id'])  selected
                                        @endif @endisset value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group d-flex align-items-center">
                                    <label class="font-weight-bold font-size-14" style="width: 150px">SKU phân
                                        loại</label>
                                    <input type="number" name="sku_type" id="sku_type" placeholder="Phân loại"
                                           value="<?= isset($data) ? $data['sku_type'] : '' ?>"
                                           class="form-control w-70">
                                </div>
                                <div class="form-group d-flex">
                                    <label class="font-weight-bold font-size-14 input_required" style="width: 150px"
                                           for="">Mô
                                        tả
                                        ngắn</label>
                                    <textarea class="form-control ckeditor " name="" id="sort_desc" cols="30"
                                              rows="4">{{isset($data) ? $data['sort_desc'] : ''}}</textarea>
                                </div>
                                <div class="form-group d-flex">
                                    <label class="font-weight-bold font-size-14 input_required" style="width: 150px">Mô
                                        tả
                                        sản phẩm</label>
                                    <textarea class="form-control ckeditor" name="desc" id="desc" cols="30"
                                              rows="3"><?= isset($data) ? $data['desc'] : '' ?></textarea>
                                </div>
                                <div class="form-group d-flex">
                                    <label class="font-weight-bold font-size-14 input_required" style="width: 150px">Danh
                                        mục</label>
                                    <ul class="p-1 content-category category-product border_box border-radius-8">
                                        @foreach($category_shop as $value)
                                        <li data-value="{{$value->id}}" data-name="{{$value->name}}"
                                            class="@if(!empty($value)) before @endif @if(isset($data->category_product) && $data->category_product == $value->id ) active @endif">{{$value->name}}
                                        </li>
                                        @endforeach
                                    </ul>
                                    <ul class="p-1 content-category category-child-1 border-radius-8 border_box">
                                        @if(isset($data->category_product))
                                        @foreach($data_category_product as $k)
                                        <li data-name="{{$k->name}}" data-value="{{$k->id}}"
                                            class="before @if(isset($data->category_id) && $data->category_id == $k->id) active @endif"
                                        >{{$k->name}}</li>
                                        @endforeach
                                        @endif
                                    </ul>
                                    <ul class="p-1 content-category category-child-2 border-radius-8 border_box">
                                        @if(isset($data->category_product))
                                        @foreach($data_product_child as $k)
                                        <li data-name="{{$k->name}}" data-value="{{$k->id}}"
                                            class="@if(isset($data->category_sub_child) && $data->category_sub_child == $k->id) active @endif"
                                        >{{$k->name}}</li>
                                        @endforeach
                                        @endif
                                    </ul>
                                </div>
                                <span class="text-red text-center">(Vui lòng chọn đầy đủ 3 loại danh mục cần thiết cho sản phẩm của bạn)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="information-sales">
            <div class="card mg-top-30">
                <div class="card-content">
                    <div class="row p-2 m-1">
                        <div class="w-100">
                            <h2 class="font-size-15 color-black text-uppercase" style="margin-bottom: 20px">Thông tin
                                bán hàng</h2>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px" for="">Kho
                                    hàng</label>
                                <input class="form-control w-70" type="number" name="warehouse" id="warehouse"
                                       value="<?= isset($data) ? $data['warehouse'] : '' ?>">
                            </div>
                            <label class="font-weight-bold font-size-14 mb-1 input_required" for="">Phân loại hàng
                                hoá </label>
                            <div class="d-flex align-items-center">
                                <div style="width: 150px"></div>
                                <label class="font-weight-bold font-size-17 text-center" style="width: 300px">Nhóm phân loại</label>
                                <label class="font-weight-bold font-size-17 text-center" style="width: 300px">Phân loại</label>
                            </div>
                            <div class="d-flex align-items-center" style="margin-bottom: 30px">
                                <div style="width: 150px"></div>
                                <div class="w-75">
                                    <div data-category>
                                        @foreach($attr as $key => $value)
                                        <div list-category>
                                            @if($key == 0)
                                                <div class="d-flex align-items-center list-container-category" data-value="{{$value->id}}" style="margin-bottom: 20px">
                                                    <input class="form-control input_title" type="text" value="{{$value->category_1}}"  placeholder="Tên nhóm phân loại " style="max-width: 300px; margin-right: 10px;">
                                                    <input class="form-control input_value" type="text" value="{{$value->category_2}}"  placeholder="Phân loại hàng hoá" style="max-width: 300px; margin-right: 10px;"">
                                                    <input type="hidden" name="value_image" value="{{$value->image_variant}}">
                                                    <button class="color-white font-weight-bold bg-red border-radius-4 btn-add-list" style="padding: 8px; border: none;"><span style="margin-right: 10px;">Thêm PL</span> <i class="fa fa-plus"></i></button>
                                                </div>
                                                <div data-content-category class="list-data-category">
                                                    <div class="d-flex justify-content-between align-items-center" style="margin-bottom: 20px">
                                                        @foreach($value->value as $k => $v)
                                                            @if($k == 0)
                                                                <div style="width: 300px;">
                                                                    <input class="form-control input_title" style="margin-right: 5px;" type="text" value="{{$v->value}}" placeholder="Tên phân loại hàng">
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                        <div class="content-list" style="padding-left: 10px; margin-left: 10px; border-left: 1px solid #DDDDDD">
                                                            @foreach($value->value as $k => $v)
                                                            <div class="d-flex justify-content-between align-items-center" content_value data-value="{{$v->id}}" style="margin-top: 20px">
                                                                <input class="form-control input_name" style="max-width: 300px; margin-right: 5px;" type="text" value="{{$v->size_of_value}}" placeholder="Phân loại hàng">
                                                                <input class="form-control input_price" style="max-width: 300px; margin-right: 5px;" type="number" value="{{$v->price}}" placeholder="giá">
                                                                <input class="form-control input_price_ctv" style="max-width: 300px; margin-right: 5px;" type="number" value="{{$v->price_ctv}}" placeholder="giá ctv">
                                                                <input class="form-control input_quantity" style="max-width: 300px; margin-right: 5px;" type="number" value="{{$v->quantity}}" placeholder="tồn kho">
                                                                <button class="bg-red font-weight-bold font-size-17 color-white text-center border-radius-4 btn-delete-content" style="padding: 8px; border: none; width: 150px"><i class="fa fa-trash-o"></i></button>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                @else
                                                <div class="d-flex align-items-center list-container-category" data-value="{{$value->id}}" style="margin-bottom: 20px">
                                                    <input class="form-control input_title" disabled type="text" value="{{$value->category_1}}"  placeholder="Tên nhóm phân loại " style="max-width: 250px; margin-right: 10px;">
                                                    <input class="form-control input_value" type="text" value="{{$value->category_2}}"  placeholder="Phân loại hàng hoá" style="max-width: 250px; margin-right: 10px;"">
                                                    <input type="hidden" name="value_image" value="{{$value->image_variant}}">
                                                    <button class="color-white font-weight-bold bg-red border-radius-4 btn-add-list" style="padding: 8px; border: none; margin-right: 10px; width: 200px"><span style="margin-right: 10px;">Thêm PL</span> <i class="fa fa-plus"></i></button>
                                                    <button class="bg-red font-weight-bold font-size-17 color-white text-center border-radius-4 btn-delete-list" style="padding: 8px; border: none; width: 150px"><i class="fa fa-trash-o"></i></button>
                                                </div>
                                                <div data-content-category class="list-data-category">
                                                    <div class="d-flex justify-content-between align-items-center" style="margin-bottom: 20px">
                                                        @foreach($value->value as $k => $v)
                                                            @if($k == 0)
                                                                <div style="width: 300px;">
                                                                    <input class="form-control input_title" disabled style="margin-right: 5px;" type="text" value="{{$v->value}}" placeholder="Tên phân loại hàng">
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                        <div class="content-list" style="padding-left: 10px; margin-left: 10px; border-left: 1px solid #DDDDDD">
                                                            @foreach($value->value as $k => $v)
                                                                <div class="d-flex justify-content-between align-items-center" content_value data-value="{{$v->id}}" style="margin-top: 20px">
                                                                    <input class="form-control input_name" style="max-width: 300px; margin-right: 5px;" type="text" value="{{$v->size_of_value}}" placeholder="Phân loại hàng">
                                                                    <input class="form-control input_price" style="max-width: 300px; margin-right: 5px;" type="number" value="{{$v->price}}" placeholder="giá">
                                                                    <input class="form-control input_price_ctv" style="max-width: 300px; margin-right: 5px;" type="number" value="{{$v->price_ctv}}" placeholder="giá ctv">
                                                                    <input class="form-control input_quantity" style="max-width: 300px; margin-right: 5px;" type="number" value="{{$v->quantity}}" placeholder="tồn kho">
                                                                    <button class="bg-red font-weight-bold font-size-17 color-white text-center border-radius-4 btn-delete-content" style="padding: 8px; border: none; width: 150px"><i class="fa fa-trash-o"></i></button>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        @endforeach
                                    </div>
                                    <button class="color-white font-weight-bold bg-red border-radius-4 btn-add-list-attr" style="padding: 8px; border: none;"><span style="margin-right: 10px;">Thêm Nhóm PL</span> <i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px" for="">Màu sắc</label>
                                <div class="w-70 d-flex align-items-center" list-image-variant>
                                    <input hidden class="input_image_2" type='file' value="" accept="image/*" id="input_image_2">
                                    @foreach($attr as $value)
                                        <div class="content_button" style="width: 120px;margin-right: 15px;">
                                            <button
                                                class="btn w-100 btn-add-image position-relative border-radius-8 border_box d-flex justify-content-lg-center align-items-center"
                                                style="height: 120px; border: 1px solid #ECECEC;"
                                            >
                                                @if(isset($value->image_variant))
                                                    <img class="position-absolute border-radius-8" style="width: 100%; height: 100%; top: 0; left: 0; object-fit: cover" src="{{$value->image_variant}}">
                                                    @else
                                                    <img src="../../assets/admin/app-assets/images/icons/add_image.png">
                                                @endif
                                            </button>
                                            <p class="font-weight-bold color-black font-size-17 text-center" style="margin-top: 10px; margin-bottom: 0">{{$value->category_2}}</p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px" for="">Tình
                                    trạng</label>
                                <select id="status_product" class="form-control w-70">
                                    <option value="1">Hàng mới</option>
                                    <option value="0">Đã qua sử dụng</option>
                                </select>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px" for="">Xuất
                                    xứ</label>
                                <input class="form-control w-70" type="text" name="original" id="original"
                                       value="<?= isset($data) ? $data['original'] : '' ?>">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px" for="">Chất
                                    liệu</label>
                                <input class="form-control w-70" type="text" name="material" id="material"
                                       value="<?= isset($data) ? $data['material'] : '' ?>">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14 input_required" style="width: 150px" for="">Gửi
                                    từ</label>
                                <select id="place" class="form-control w-70">
                                    @if(isset($data_address))
                                    @foreach($data_address as $value)
                                    <option value="{{$value->id}}">{{$value->address}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group d-flex">
                                <label class="font-weight-bold font-size-14" style="width: 150px" for="">Mua nhiều giảm giá</label>
                                <div class="w-70">
                                    <div class="d-flex align-items-center">
                                        <p class="m-0 w-25 text-center font-weight-bold" style="margin-right: 10px">Từ (sản phẩm)</p>
                                        <p class="m-0 w-25 text-center font-weight-bold" style="margin-right: 10px">Đến (sản phẩm)</p>
                                        <p class="m-0 w-25 text-center font-weight-bold">Giảm giá (phần trăm)</p>
                                    </div>
                                    <div class="list-buy-percent" style="margin-top: 20px">
                                        @if(isset($buy_more_percent) && count($buy_more_percent))
                                                @foreach($buy_more_percent as $key => $value)
                                                    @if($key == 0)
                                                        <div class="content_list d-flex justify-content-between align-items-center" data-value="{{$value->id}}">
                                                            <input class="form-control w-25 input_quantity_1" value="{{$value->quantity_1}}" type="number" style="margin-right: 10px">
                                                            <input class="form-control w-25 input_quantity_2" value="{{$value->quantity_2}}" type="number" style="margin-right: 10px">
                                                            <input class="form-control w-25 input_percent" value="{{$value->percent}}" type="number" style="margin-right: 10px">
                                                            <button class="form-control btn-add-percent bg-red w-25 color-white font-weight-bold" style="border: none">
                                                                <img src="../../assets/admin/app-assets/images/icons/add.png">
                                                                <span style="padding-left: 5px">Thêm giảm giá</span>
                                                            </button>
                                                        </div>
                                                        @else
                                                        <div class="content_list d-flex justify-content-between align-items-center" data-value="{{$value->id}}" style="margin-top: 20px">
                                                            <input class="form-control w-25 input_quantity_1" disabled value="{{$value->quantity_1}}" type="number" style="margin-right: 10px">
                                                            <input class="form-control w-25 input_quantity_2" value="{{$value->quantity_2}}" type="number" style="margin-right: 10px">
                                                            <input class="form-control w-25 input_percent" value="{{$value->percent}}" type="number" style="margin-right: 10px">
                                                            <button class="form-control btn-delete-percent bg-red w-25 color-white font-weight-bold" style="border: none">
                                                                <i class="fa fa-trash-o"></i>
                                                                <span style="padding-left: 5px">Xóa</span>
                                                            </button>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @else
                                            <div class="content_list d-flex justify-content-between align-items-center">
                                                <input class="form-control w-25 input_quantity_1" type="number" style="margin-right: 10px">
                                                <input class="form-control w-25 input_quantity_2" type="number" style="margin-right: 10px">
                                                <input class="form-control w-25 input_percent" type="number" style="margin-right: 10px">
                                                <button class="form-control btn-add-percent bg-red w-25 color-white font-weight-bold" style="border: none">
                                                    <img src="../../assets/admin/app-assets/images/icons/add.png">
                                                    <span style="padding-left: 5px">Thêm giảm giá</span>
                                                </button>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mg-top-30">
                <div class="card-content p-2 m-1">
                    <h2 class="font-size-15 color-black text-uppercase" style="margin-bottom: 20px">quản lý hình
                        ảnh </h2>
                    <div class="form-group d-flex">
                        <input hidden type='file' value="" accept="image/*" id="image">
                        <p class="font-size-14 font-weight-bold input_required"
                           style="margin-right: 15px;min-width: 120px;">Ảnh đại diện:</p>
                        <button
                            class="btn position-relative btn-image-1 border-radius-8 border_box d-flex justify-content-lg-center align-items-center">
                            <img class="position-absolute border-radius-8" style="width: 100%; height: 100%; top: 0; left: 0; object-fit: cover"
                                 src="{{$data->image}}"
                            >
                        </button>
                        <input hidden value="{{$data->image}}" name="image_product">
                        <br>
                        <br>
                    </div>
                    <div class="d-flex">
                        <p class="font-size-14 font-weight-bold input_required"
                           style="margin-right: 15px;min-width: 120px;">Ảnh sản phẩm:</p>
                        <div class="form-group d-flex align-items-center flex-wrap group-image">
                            <input hidden value="{{$data->id}}" id="id_product">
                            <input hidden class="form-control input_upload_image" type='file' accept="image/*" />
                            <?php if (isset($data_image) && count($data_image)): ?>
                                @for($i = 0; $i <=8; $i++)
                                    @if(isset($data_image[$i]['image']))
                                        <div data-value="{{$data_image[$i]['id']}}"
                                            class="btn position-relative show-list-image border-radius-8 border_box d-flex justify-content-lg-center align-items-center">
                                            <img class="position-absolute border-radius-8 image-variant" style="width: 100%; height: 100%; top: 0; left: 0; object-fit: cover" src="<?= $data_image[$i]['image'] ?>">
                                            <label class="color-red position-absolute d-flex justify-content-center align-items-center icon_update">
                                                <i class="fa fa-times"></i>
                                            </label>
                                            <input type="hidden" class="upload_image" value="{{$data_image[$i]['image']}}">
                                        </div>
                                        @else
                                        <div
                                            class="btn show-list-image position-relative btn-add-list-image border-radius-8 border_box d-flex justify-content-lg-center align-items-center">
                                            <img src="../../assets/admin/app-assets/images/icons/add_image.png">
                                            <label class="color-red position-absolute d-flex justify-content-center align-items-center icon_update" style="opacity: 0; z-index: -100">
                                                <i class="fa fa-times"></i>
                                            </label>
                                            <input type="hidden" class="upload_image">
                                        </div>
                                    @endif
                                @endfor
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="d-flex">
                        <p class="font-size-14 font-weight-bold" style="margin-right: 15px;min-width: 120px;">Video:</p>
                        <input type="text" placeholder="Thêm URL video youtube" value="@if(str_contains($data->video, 'https://www.youtube.com/embed')) {{$data->video}} @endif" class="border-radius-6 border-red p-1"
                               id="video_product">
                    </div>
                    <div class="video-product">
                        @if(str_contains($data->video, 'https://www.youtube.com/embed'))
                            <iframe width="560" height="315" src="{{$data->video}}" title="W3Schools Free Online Web Tutorials"></iframe>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card mg-top-30">
                <div class="card-content p-2 m-1">
                    <h2 class="font-size-15 color-black text-uppercase" style="margin-bottom: 20px">Vận chuyển</h2>
                </div>
                <div class="col-lg-12">
                    <div class="mt-2">
                        <table>
                            <tr>
                                <td class="font-weight-bold font-size-14 input_required">Cân nặng sau đóng gói:</td>
                                <td>
                                    <div class="d-flex border_box border-radius-6 align-items-center group-kg">
                                        <input type="number" id="weight"
                                               class="border-radius-6 input-form font-weight-bold font-size-14"
                                               value="{{isset($data) ? $data['weight'] : ''}}">
                                        <label class="gram">Gram</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold font-size-14">Kích thước đóng gói:</td>
                                <td>
                                    <div class="d-flex border_box border-radius-6 align-items-center group-kg">
                                        <input type="text"
                                               class="border-radius-6 input-form font-weight-bold font-size-14"
                                               id="length"
                                               value="{{isset($data) ? $data['length'] : ''}}">
                                        <label class="gram">Cm</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex border_box border-radius-6 align-items-center group-kg">
                                        <input type="text"
                                               class="border-radius-6 input-form font-weight-bold font-size-14"
                                               id="width"
                                               value="{{isset($data) ? $data['width'] : ''}}">
                                        <label class="gram">Cm</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex border_box border-radius-6 align-items-center group-kg">
                                        <input type="text"
                                               class="border-radius-6 input-form font-weight-bold font-size-14"
                                               id="height"
                                               value="{{isset($data) ? $data['height'] : ''}}">
                                        <label class="gram">Cm</label>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <p class="mt-2 font-weight-bold font-size-14 input_required">Phí vận chuyển</p>
                    <div class="p-2 border_box border-radius-16"
                         style="border: 1px solid #CCD3DC; position: relative; margin-bottom: 40px">
                        @isset($shipping)
                        @foreach($shipping as $value)
                        <div class="d-flex align-items-center mb-2">
                            <p class="font-size-15 m-0">{{$value->shipping_name}} (Tối đa {{$value->max_weight}}
                                g)</p>
                            <p style="position: absolute; right: 10%;" class="m-0">
                                <label class="switch">
                                    <input data-id="{{$value->shipping_id}}" type="checkbox" @if($value->check) checked @endif
                                           class="btn_add_shipping" name="shipping_supplier[]">
                                    <span class="slider round"></span>
                                </label>
                            </p>
                        </div>
                        @endforeach
                        @endisset
                    </div>
                </div>
            </div>
            <div class="card mg-top-30">
                <div class="card-content p-2 m-1">
                    <div class="d-flex align-items-center justify-content-lg-end">
                        <a href="<?= URL::to('admin/product_supplier') ?>" class="btn btn-form-edit">Hủy bỏ</a>
                        <button type="button" class="btn btn-form btn-add-account bg-red"
                                data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                data-supplier="{{isset($data_login) ? $data_login['id'] : ''}}"
                                data-shipping="@if(isset($shipping)) @foreach($shipping as $key => $value) {{$value->shipping_id}}, @endforeach @endif"
                                data-url="<?= URL::to('admin/product_supplier/save') ?>">
                            Lưu
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
@include('admin.base.script_product')

{{--<script src="dashboard/products/index.js"></script>--}}
<script src="dashboard/products/add-product.js"></script>
<script src="dashboard/products/add-category.js"></script>
<script src="dashboard/products/upload-image.js"></script>
<script>

    $(document).ready(function () {
        let form_data = new FormData();
        let category = @if(isset($data->category_product)) {{$data->category_product}} @else null @endif;
        let category_1 = @if(isset($data->category_product)) {{$data->category_id}} @else null @endif;
        let category_2 = @if(isset($data->category_product)) {{$data->category_sub_child}} @else null @endif;
        let name_product, brand_product, sku_product, video = "{{$data->video}}";
        $(document).on('blur', '.input-price-ctv', function () {
            let price_product_attr = $(this).parents(".form_price_product").attr('data-price');
            let price_product_ctv_attr = $(this).val();
            let price_product_attr_new = parseInt(price_product_attr);
            let price_product_ctv_attr_new = parseInt(price_product_ctv_attr);
            if (price_product_ctv_attr_new > price_product_attr_new) {
                swal('Thông báo', 'Giá đại lý (CTV) phải nhỏ hơn giá sản phẩm nhập vào !', 'warning');
                $(this).val('');
            }
        });

        $(document).on('blur', '.input_price', function () {
            let parent = $(this).closest("[content_value]");
            let price = parent.find(".input_price_ctv").val();
            if ($(this).val() < price){
                swal('Thông báo', 'Giá sản phẩm không được nhỏ hơn giá ctv', 'warning');
                $(this).val('');
            }
        });

        $(document).on('click', '.btn_add_shipping', function () {
            let checked = [];
            $("[name='shipping_supplier[]']").each(function () {
                if ($(this).is(":checked")) {
                    checked.push($(this).attr('data-id'));
                }
            });
            $('.btn-add-account').attr('data-shipping', checked);
            $('.btn-save-hide').attr('data-shipping', checked);
        });
        $('.category-product li').click(function () {
            category = $(this).attr("data-value");
            $('.content-category li').removeClass("active");
            $(this).addClass("active");
            let url = window.location.origin + '/admin/product_supplier/category';
            let data = {};
            data['category_id'] = $(this).attr("data-value");
            $('.category-child-1').html("");
            $('.category-child-2').html("");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (data) {
                    if (data.status) {
                        for (const element of data.category) {
                            let li = '<li class="before" data-value=" ' + element.id + ' " data-name="' + element.name + '">' + element.name + '</li>';
                            $('.category-child-1').append(li);
                        }
                        $(".btn-next").prop("disabled", true);
                    }
                }
            });
        });
        $(document).on("click", ".category-child-1 li", function () {
            category_1 = $(this).attr("data-value");
            $('.category-child-1 li').removeClass("active");
            $(this).addClass("active");
            let url = window.location.origin + '/admin/product_supplier/category_child';
            let data = {};
            data['category_id'] = $(this).attr("data-value");
            $('.category-child-2').html("");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (data) {
                    if (data.status) {
                        for (const element of data.category) {
                            let li = '<li data-value=" ' + element.id + ' " data-name="' + element.name + '">' + element.name + '</li>';
                            $('.category-child-2').append(li);
                        }
                        $(".btn-next").prop("disabled", true);
                    } else {
                        $(".btn-next").prop("disabled", false);
                    }
                }
            });
        });
        $(document).on("blur", ".input_percent", function () {
            let value = parseInt($(this).val());
            if (value > 50 || isNaN(value)){
                swal('Thông báo', 'Giảm giá không được lớn hơn 50%', 'error');
                $(this).val("");
            }
        });
        $(document).on("click", ".category-child-2 li", function () {
            $('.category-child-2 li').removeClass("active");
            $(this).addClass("active");
            let text = $(this).attr("data-name");
            $(".name-category-2").text(text);
            $(".btn-next").prop("disabled", false);
            category_2 = $(this).attr("data-value");
        });
        $(document).on("click", ".btn-add-account", function () {
            if ($('#price_product').val() < 0) {
                swal('Thông báo', 'Giá sản phẩm không được nhỏ hơn 0', 'warning');
                return false;
            }
            if ($('#weight').val() == '') {
                swal('Thông báo', 'Cân nặng không được để trống !', 'warning');
                return false;
            }
            let attr_category = $('.title-group-catagory').val();
            let content_category = $('.content-group-catagory').val();
            let content_group_category = $('.content-group-catagory-2').val();
            let input_price = $('.input-price').val();
            let input_inventory = $('.input-inventory').val();
            if (attr_category == '' || content_category == '' || content_group_category == '' || input_price == '' || input_inventory == '') {
                swal('Thông báo', 'Vui lòng nhập đủ phân loại cho sản phẩm !', 'warning');
                return false;
            }
            if (input_price < 0) {
                swal('Thông báo', 'Giá sản phẩm không được nhỏ hơn 0', 'warning');
                return false;
            }
            let shipping = $(this).attr('data-shipping');
            if (shipping == undefined) {
                swal('Thông báo', 'Vui lòng chọn ít nhất 1 đơn vị vận chuyển', 'warning');
                return false;
            }
            let attribute_item = [];
            let check = true;
            $("[list-category]").each(function () {
                let content_list = $(this).find("[content_value]");
                let _arrcontent = [];
                if ( $(this).find(".input_title").val() == "" || $(this).find(".input_value").val() == ""
                    || $(this).find(".list-data-category .input_title").val() == "" || $(this).find('input[name="value_image"]').val() == ""
                ){
                    check = false;
                    return false;
                }
                content_list.each(function () {
                    if ( $(this).find(".input_name").val() == "" || $(this).find(".input_price").val() == "" || $(this).find(".input_price_ctv").val() == "" || $(this).find(".input_quantity").val() == "" ){
                        check = false;
                        return false;
                    }
                    let value = {
                        "id" : $(this).attr("data-value"),
                        "name": $(this).find(".input_name").val(),
                        "price" : $(this).find(".input_price").val(),
                        "price_ctv" : $(this).find(".input_price_ctv").val(),
                        "quantity" : $(this).find(".input_quantity").val()
                    };
                    _arrcontent.push(value);
                });
                let arr = {
                    'id' : $(this).find(".list-container-category").attr("data-value"),
                    'title': $(this).find(".input_title").val(),
                    'name': $(this).find(".input_value").val(),
                    'title_value' : $(this).find(".list-data-category .input_title").val(),
                    'image_variant': $(this).find('input[name="value_image"]').val(),
                    'value': _arrcontent
                };
                attribute_item.push(arr);
            });
            if (!check){
                swal('Thông báo', 'Vui lòng điền thông tin đầy đủ', 'error');
                return false;
            }
            let buy_more_percent = [];
            $(".list-buy-percent .content_list").each(function () {
                let buy_percent = {
                    'id' : $(this).attr("data-value"),
                    'quantity_1' : $(this).find(".input_quantity_1").val(),
                    'quantity_2' : $(this).find(".input_quantity_2").val(),
                    'percent' : $(this).find(".input_percent").val()
                };
                buy_more_percent.push(buy_percent);
            });
            let group_image = [];
            $(".group-image .show-list-image").each(function () {
                let img = $(this).find(".upload_image").val();
                let image_variant = {
                    'id' : $(this).attr("data-value"),
                    'upload_image' : img
                };
                group_image.push(image_variant);
            });
            form_data.append('buy_more_percent', JSON.stringify(buy_more_percent));
            form_data.append('group_image', JSON.stringify(group_image));
            form_data.append('name', $('#name').val());
            form_data.append('sku_type', $('#sku_type').val());
            form_data.append('video', video);
            form_data.append('category_id', category_1);
            form_data.append('category_2', category_2);
            form_data.append('brand', $('#brand').val());
            form_data.append('is_status', $('#is_status').val());
            form_data.append('warehouse', $('#warehouse').val());
            form_data.append('is_desc', CKEDITOR.instances['desc'].getData());
            form_data.append('weight', $('#weight').val());
            form_data.append('height', $('#height').val());
            form_data.append('length', $('#length').val());
            form_data.append('width', $('#width').val());
            form_data.append('original', $('#original').val());
            form_data.append('material', $('#material').val());
            form_data.append('image_product', $('input[name="image_product"]').val());
            form_data.append('attr', JSON.stringify(attribute_item));
            form_data.append('sort_desc', CKEDITOR.instances['sort_desc'].getData());
            form_data.append('place', $('#place').val());
            form_data.append('status_product', $('#status_product').val());
            form_data.append('shipping', JSON.stringify(shipping));
            form_data.append('category_product', category);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: window.location.href + '/update',
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        swal('Thông báo', data.msg, 'error');
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });

        $('.btn-next').click(function () {
            name_product = $('#name').val();
            brand_product = $('#brand').val();
            sku_product = $('#sku_type').val();
            
            let check = function (string) {
                for (i = 0; i < specialChars.length; i++) {
                    if (string.indexOf(specialChars[i]) > -1) {
                        return true;
                    }
                }
                return false;
            };
            if (name_product.length < 5 || name_product.length > 120) {
                swal('Thông báo', 'Tên sản phẩm nhập đủ từ 5 - 120 ký tự !', 'warning');
            } else if (name_product == '' || brand_product == '') {
                swal('Thông báo', "Vui lòng nhập đủ thông tin", 'warning');
            } else {
                $('.information-sales').css("display", "block");
                $('.head_form').hide();
            }
        });

        $(document).on('blur', '#video_product', function () {
            let value = $(this).val();
            value = value.replace('watch', 'embed');
            value = value.replace('?v=', '/');
            video = value;
            let html = '<iframe width="560" height="315" src="' + value + '" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
            $(".video-product").html(html);
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
