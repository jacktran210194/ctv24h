<?php
$page = 'product_supplier';
$data_login = Session::get('data_supplier');
if (isset($data_login)) {
    $data_shop = \App\Models\ShopModel::where('supplier_id', $data_login->id)->first();
}

use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <!-- BEGIN: Tab Bar-->
        <div class="tab-bar-page">
            <ul class="tab-bar-content d-flex align-items-center justify-content-lg-start flex-wrap">
                <li class="d-flex align-items-center justify-content-lg-center active">
                    <a href="{{url('/')}}" data-value="all"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Tất cả</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center">
                    <a href="{{url('/')}}" data-value="0"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đang hoạt
                        động</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center">
                    <a href="{{url('/')}}" data-value="1"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Hết hàng</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center">
                    <a href="{{url('/')}}" data-value="2"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Vi phạm</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center">
                    <a href="{{url('/')}}" data-value="3"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đã ẩn</a>
                </li>
            </ul>
        </div>
        <!-- END: Tab Bar-->
        <div class="bg-white p-1">
            <div
                class="form-label-group has-icon-left position-relative w-25 d-flex justify-content-sm-between align-items-center">
                <input type="text" id="search" class="form-control" name="search" placeholder="Nhập tên sản phẩm...">
                <div class="form-control-position">
                    <i class="feather icon-search font-weight-bold color-red font-size-17"></i>
                </div>
            </div>
            <div class="d-flex justify-content-sm-between align-items-center mg-top-30 w-100">
                <p class="color-red font-size-24 font-weight-bold m-0">
                    <span class="total-product">{{$data}}</span> Sản phẩm
                </p>
                <div class="d-flex align-items-center">
                    <a href="{{url('admin/product_supplier/add')}}"
                       class="btn color-black d-flex align-items-center font-weight-bold font-size-14 border_box border-radius-8 border"
                       style="margin-right: 20px; height: 36px">
                        <span style="margin-right: 10px">Đăng sản phẩm hàng loạt</span>
                        <img src="../../assets/admin/app-assets/images/icons/Polygon.png">
                    </a>
                    @if(($data_shop->name_company != null && $data_shop->business != null && $data_shop->card_id != null && $data_shop->card_date_create != null && $data_shop->card_address != null && $data_shop->image_card_1 != null && $data_shop->image_card_2 != null && $data_shop->business_license != null && $data_shop->related_document_1 != null && $data_shop->related_document_2 != null) && $data_shop->active == 1)
                        <a href="{{url('admin/product_supplier/add')}}"
                           class="btn d-flex align-items-center btn_main color-white font-weight-bold font-size-14">
                            <img src="../../assets/admin/app-assets/images/icons/add.png">
                            <span style="margin-left: 10px">Thêm sản phẩm</span>
                        </a>
                    @else
                        <a href="{{url('admin/shop_supplier/update_info')}}"
                           class="btn d-flex align-items-center btn_main color-white font-weight-bold font-size-14">
                            <img src="../../assets/admin/app-assets/images/icons/add.png">
                            <span style="margin-left: 10px">Thêm sản phẩm</span>
                        </a>
                    @endif
                </div>
            </div>
            <div class="clearfix mb-2"></div>
            <div class="card">
                <div class="card-content">
                    <!-- content -->
                    <div class="table-responsive table-responsive-lg">
                        <table class="table data-list-view table-sm">
                            <thead>
                            <tr>
                                <th class="border-top-left border-bottom-left color-white font-weight-bold bg-red">Loại
                                    sản phẩm
                                </th>
                                <th class="color-white font-weight-bold bg-red" scope="col">Tên sản phẩm</th>
                                <th class="color-white font-weight-bold bg-red" scope="col">SKU phân loại</th>
                                <th class="color-white font-weight-bold bg-red">Phân loại hàng</th>
                                <th class="color-white font-weight-bold bg-red text-center">Giá</th>
                                <th class="color-white font-weight-bold bg-red text-center">Kho hàng</th>
                                <th class="color-white font-weight-bold bg-red text-center">Đã bán</th>
                                <th class="border-top-right border-bottom-right color-white font-weight-bold bg-red"
                                    scope="col">Thao tác
                                </th>
                            </tr>
                            </thead>
                            <tbody id="data_product">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
<script src="dashboard/index.js"></script>
<script>
    $(document).ready(function () {
        // fetch_customer_data();
        getData();

        function fetch_customer_data(query = '') {
            $.ajax({
                url: "{{ route('search_product') }}",
                method: 'GET',
                data: {query: query},
                dataType: 'json',
                success: function (data) {
                    $('tbody').html(data.table_data);
                    $('.total-product').text(data.total_data);
                }
            })
        }

        $(document).on('keyup', '#search', function () {
            let query = $(this).val();
            fetch_customer_data(query);
        });

        function getData() {
            $.get('/admin/product_supplier/filter_order/all', function (data) {
                $('#data_product').html(data);
            })
        }

        $(document).on('click', '.tab-bar-content li a', function (ev) {
            ev.preventDefault();
            $('.tab-bar-content li').removeClass("active");
            $(this).parent().addClass("active");
            let status = $(this).attr("data-value");
            $.get('/admin/product_supplier/filter_order/' + status, function (data) {
                $('#data_product').html(data);
                let count = $('#data_product tr:first').attr('data-count');
                $('.total-product').text(count);
            })
        });

        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
        $(document).on("click", ".show-options", function () {
            $(this).next().toggle('fast');
        });
        $(document).on("click", ".btn-hide", function () {
            let url = $(this).attr('data-url');
            let active = $(this).attr('data-active');
            let text = $(this).attr("title");
            $('.status-action').text(text);
            $('.btn-edit').attr('data-url', url);
            $('.btn-edit').attr('data-active', active);
            $('#my-popup').modal('show');
        });
        $(document).on("click", ".btn-edit", function () {
            let url = $(this).attr('data-url');
            let edit = {};
            edit['hide'] = parseInt($(this).attr('data-active'));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: edit,
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        window.location.reload();
                    }
                }
            });
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
