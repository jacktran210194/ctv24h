<?php
$page = 'financial_wallet_supplier';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Conxtent-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="mb-1">
                    <span style="color: #ff0000; font-style: italic">Tính năng đang được nâng cấp</span>
                </div>
                <a class="btn btn-success mb-2"
                   href="{{url('admin/manage_financial/wallet')}}"><i
                        class="feather icon-file-text"></i> Xuất báo cáo</a>
                <div class="card">
                    <div class="card-content">
                        <!-- content -->
                        <div class="table-responsive table-responsive-lg">
                            <table class="table data-list-view table-sm">
                                <thead>
                                <tr>
                                    <th scope="col" >Họ và tên</th>
                                    <th scope="col" >Nội dung</th>
                                    <th scope="col" >Số xu</th>
                                    <th scope="col" >Thời gian</th>
                                </tr>
                                </thead>
                                <tbody>
{{--                                @if(count($data_plus))--}}
{{--                                    @foreach($data_plus as $value)--}}
{{--                                        <tr>--}}
{{--                                            <td>{{$value->name}}</td>--}}
{{--                                            <td>{{$value->title}}</td>--}}
{{--                                            <td>+ {{$value->point}}</td>--}}
{{--                                            <td>{{$value->date}}</td>--}}
{{--                                        </tr>--}}
{{--                                    @endforeach--}}
{{--                                @else--}}
{{--                                    <tr>--}}
{{--                                        <td>Không có dữ liệu</td>--}}
{{--                                    </tr>--}}
{{--                                @endif--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
<script src="dashboard/index.js"></script>

<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>

