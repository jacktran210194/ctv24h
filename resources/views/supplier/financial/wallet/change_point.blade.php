<?php
$page = 'financial_wallet_supplier';
$dataUser = Session::get('data_supplier');
use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group" >
                                <label class="lb">Họ và tên</label>
                                <input type="text" name="user_name" id="user_name" placeholder="Họ và tên" class="form-control" value="<?= $data ? $data->name : '' ?>" disabled>
                            </div>
                            <div class="form-group">
                                <label class="lb">Số điểm hiện tại</label>
                                <input type="text" placeholder="Số điểm hiện tại" class="form-control" value="<?= $data ? $data->point : '' ?> điểm" disabled>
                            </div>
                            <span style="color: #FF0000; font-style: italic; font-weight: bold" class="">Đổi 1 điểm = 1.000 đ</span>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label class="lb">Nhập số điểm</label>
                                <input type="number" name="point" id="point" placeholder="Nhập số điểm" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Nội dung</label>
                                <input type="text" name="title" id="title" placeholder="Nội dung" class="form-control">
                            </div>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/manage_financial/wallet/point/change/save') ?>"><i class="fa fa-floppy-o"
                                                                                                data-shop="<?= isset($dataUser) ? $dataUser->id : '' ?>"
                                                                                                aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/manage_financial/wallet/point')?>" class="btn btn-danger"
                               style="color: white"><i class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<!-- BEGIN: Footer-->
@include('admin.base.footer')
<!-- END: Footer-->
@include('admin.base.script')
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".btn-add-account", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('point', $('#point').val());
            form_data.append('title', $('#title').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });
</script>
<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>

