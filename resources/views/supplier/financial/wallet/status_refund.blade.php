<div class="card">
    <div class="card-content">
        <div class="m-2">
            <span class="title-payment-near">Các giao dịch gần đây</span>
        </div>
        <div class="d-flex table-head m-2">
            <div class="d-flex col-lg-6 align-items-center justify-content-lg-around">
                <div class="col-lg-3 ml-3">
                    <span>Chọn ngày</span>
                </div>
                <div>
                    <input type="date" class="form-control time-start" value="{{date('Y-m-d')}}">
                </div>
                <span class="ml-1 mr-1">đến</span>
                <div>
                    <input type="date" class="form-control time-end" value="{{date('Y-m-d')}}">
                </div>
                <button type="button" style="width: 8%; line-height: 20px;" class="btn border btn-filter-order-refund ml-1"
                        data-url="{{url('admin/manage_financial/wallet/status_refund/filter_date_refund')}}"
                ><i class="fa fa-search"></i>
                </button>
            </div>
        </div>
        <div class="main pl-2 pr-2">
            <div class="main-unit align-items-center">
                <div class="d-flex justify-content-lg-around">
                    <div
                        class="tab-menu align-items-center status-all">
                        <center>
                            <a data-url-all="{{url('admin/manage_financial/wallet/status_all')}}">Tất cả</a>
                        </center>
                    </div>
                    <div
                        class="tab-menu align-items-center status-revenue">
                        <center>
                            <a data-url-revenue="{{url('admin/manage_financial/wallet/status_revenue')}}">Doanh thu</a>
                        </center>
                    </div>
                    <div
                        class="tab-menu align-items-center status-withdrawal">
                        <center>
                            <a data-url-withdrawal="{{url('admin/manage_financial/wallet/status_withdrawal')}}">Rút tiền</a>
                        </center>
                    </div>
                    <div
                        class="tab-menu align-items-center checked status-refund">
                        <center>
                            <a class="popup_default">Hoàn tiền từ đơn hàng</a>
                        </center>
                    </div>
                    <div
                        class="tab-menu align-items-center status-bonus">
                        <center>
                            <a class="popup_default">Hoa hồng giới thiệu</a>
                        </center>
                    </div>
                    <div
                        class="tab-menu align-items-center status-advertisement">
                        <center>
                            <a class="popup_default">Trừ phí quảng cáo</a>
                        </center>
                    </div>
                    <div
                        class="tab-menu align-items-center status-adjusted">
                        <center>
                            <a class="popup_default">Điều chỉnh</a>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive table-responsive-lg p-1">
            <table class="table data-list-view table-sm">
                <thead class="">
                <tr>
                    <th class="text-center" scope="col">Ngày</th>
                    <th class="text-center" scope="col">Loại giao dịch | Mô tả</th>
                    <th>Tình trạng</th>
                    <th class="text-center" scope="col">Số tiền</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Không có dữ liệu</td>
                </tr>
{{--                <tr class="text-center">--}}
{{--                    <td>--}}
{{--                        15/03/2021--}}
{{--                    </td>--}}
{{--                    <td>--}}
{{--                                        <span class="title-dank">--}}
{{--                                            Hoàn tiền từ đơn hàng  #y53y5743r58--}}
{{--                                        </span>--}}
{{--                        <br>--}}
{{--                        <span>--}}
{{--                                            Combo 6 khăn giấy ướt baby care--}}
{{--                                        </span>--}}
{{--                    </td>--}}
{{--                    <td>--}}
{{--                        <div class="bg-orange bg-span">--}}
{{--                            Đang xử lý--}}
{{--                        </div>--}}
{{--                    </td>--}}
{{--                    <td class="title-dank">$75.67</td>--}}
{{--                </tr>--}}
                </tbody>
            </table>
        </div>
    </div>
</div>
