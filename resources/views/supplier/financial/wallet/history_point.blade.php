<?php
$page = 'financial_wallet_supplier';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Conxtent-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-content p-2">
                        <!-- content -->
                        {{--                        <a class="btn btn-success mb-2"--}}
                        {{--                           href="{{url('admin/manage_financial/wallet/payment/export')}}"><i--}}
                        {{--                                class="feather icon-file-text"></i> Xuất báo cáo</a>--}}
{{--                        --}}
                        <div class="table-responsive table-responsive-lg border_table">
                            <table class="table data-list-view table-sm">
                                <thead class="bg_table text-center">
                                <tr>
                                    <th scope="col">Ngày giao dịch</th>
                                    <th scope="col">Số điểm</th>
                                    <th scope="col">Nội dung</th>
                                </tr>
                                </thead>
                                <tbody class="text-center">
                                @if(count($dataHistoryPoint))
                                    @foreach($dataHistoryPoint as $value)
                                        <tr>
                                            <td>{{date('d-m-Y H:i:s', strtotime($value->date))}}</td>
                                            <td>{{$value->type.' '.$value->point}}</td>
                                            <td>{{$value->title}}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td>Không có dữ liệu</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
<script src="dashboard/index.js"></script>
<script>

    $(document).ready(function () {
        $(document).on("click", ".btn-active", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });

        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.btn-reject').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            $('#myModal').modal('show');
        });

        function filter_date() {
            let url = $('.btn-filter-order').attr('data-url');
            let data = {};
            data['time_start'] = $('.time-start').val();
            data['time_end'] = $('.time-end').val();
            if (data['time_start'] == '' || data['time_end'] == '' || (data['time_start'] > data['time_end'])) {
                swal("Thông báo", "Vui lòng chọn khoảng thời gian phù hợp", "error");
                return false;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: data,
                success: function (data) {
                    $('tbody').html(data.table_data);
                },
                error: function () {
                    console.log('error')
                },
            });
        }
        $(document).on('click', '.btn-filter-order', function () {
            filter_date();
        })
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>

