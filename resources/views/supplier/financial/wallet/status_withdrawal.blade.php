<?php
$page = 'financial_wallet_supplier';
$dataLogin = Session::get('data_supplier');
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/financial/style.css')}}">
<script src="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.js"></script>
<link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.css"/>
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<!-- END: Head-->
<style>
    .popup-add-bank-account {
        position: fixed;
        top: 0;
        left: 10%;
        width: 85%;
        height: 100%;
        z-index: 10;
        display: none;
    }

    .popup-add-bank-account .show-pop {
        display: block;
        z-index: 20;
    }

    .popup-add-bank-account .container-popup-add-bank-account {
        width: 40%;
        margin: 20px auto;
        background: #fff;
        border-radius: 16px;
        padding: 30px;
        border: 1px solid #005DB6;
    }

    .container-popup-add-bank-account .header-popup-add-bank-account {
        margin-bottom: 10px;
    }
</style>

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="d-flex">
            <div class="col-lg-9 col-md-12 p-0 pr-2">
                <div class="card">
                    <div class="card-content">
                        <div class="d-flex head-unit justify-content-lg-between mt-2">
                            <div class="box-content-f payment-unit-wait">
                                <div class="d-flex">
                                    <div class="col-lg-5 col-md-12">
                                        <p class="payment-wait mt-2">Ví CTV24</p>
                                        <p class="payment-total">Tổng cộng</p>
                                    </div>
                                    <div class="col-lg-7 col-md-12 pt-2">
                                        <p class="payment-price"><?= isset($data) ? number_format($data->wallet) : '' ?>đ</p>
                                    </div>
                                </div>
                                <div class="d-flex align-items-end justify-content-lg-between mb-1 p-1 bt">
                                    <div>
                                        <center>
                                            <a class="btn btn_main btn-withdrawal">Rút tiền</a>
                                        </center>
                                    </div>
                                    <div>
                                        <center>
                                            <a href="{{url('admin/manage_financial/wallet/payment')}}" class="btn btn_blue">Lịch sử rút tiền</a>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="box-content-f payment-unit-wait">
                                <div class="d-flex">
                                    <div class="col-lg-5 col-md-12">
                                        <p class="payment-wait mt-2">Ví Point</p>
                                    </div>
                                    <div class="col-lg-7 col-md-12 pt-2">
                                        <p class="payment-price"><?= isset($data) ? $data->point : '' ?> điểm</p>
                                    </div>
                                </div>
                                <div class="d-flex align-items-end justify-content-lg-between mb-1 p-1 bt">
                                    <div>
                                        <center>
                                            <a href="{{url('recharge-card')}}" class="btn btn_main">Nạp điểm</a>
                                        </center>
                                    </div>
                                    <div class="btn-point">
                                        <center>
                                            <a href="{{url('admin/manage_financial/wallet/point')}}" class="btn btn_blue">Lịch sử điểm</a>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="box-content-f payment-unit-wait">
                                <div class="d-flex">
                                    <div class="col-lg-5 col-md-12">
                                        <p class="payment-wait mt-2">Ví Xu</p>
                                    </div>
                                    <div class="col-lg-7 col-md-12 pt-2">
                                        <p class="payment-price"><?= isset($data) ? $data->coin : '' ?> xu</p>
                                    </div>
                                </div>
                                <div class="d-flex align-items-end justify-content-lg-end mb-1 p-1 bt">
                                    <div class="btn-point">
                                        <center>
                                            <a class="btn btn_blue popup_default">Lịch sử xu</a>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex bank-account ml-2 mt-5">
                            <div>
                                <img src="../../assets/admin/app-assets/images/ncc/financial/wallet.png">
                            </div>
                            <div class="ml-2">
                                <p class="title">Tài khoản ngân hàng</p>
                                <div class=" align-items-center justify-content-lg-around">
                                    @if(isset($dataBankAccount))
                                        @foreach($dataBankAccount as $valueBankAccount)
                                            <a href="{{url('admin/manage_financial/banking/add')}}">
                                                <div class="d-flex align-items-center just">
                                                    <p class="text-black font-size-17">{{$valueBankAccount->short_name}} - {{$valueBankAccount->bank_name}}
                                                        - {{'*** ' .$valueBankAccount->sub_bank_account}} @if($valueBankAccount->is_default == 1) <span class="bg-default-bank-acc ml-1">Mặc định</span> @endif
                                                    </p>
                                                </div>
                                            </a>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="body-unit">
                    <div class="card">
                        <div class="card-content">
                            <div class="m-2">
                                <span class="title-payment-near">Các giao dịch gần đây</span>
                            </div>
                            <div class="d-flex table-head m-2">
                                <div class="d-flex col-lg-6 align-items-center justify-content-lg-around">
                                    <div class="col-lg-3 ml-3">
                                        <span>Chọn ngày</span>
                                    </div>
                                    <div>
                                        <input type="date" class="form-control time-start" value="{{date('Y-m-d')}}">
                                    </div>
                                    <span class="ml-1 mr-1">đến</span>
                                    <div>
                                        <input type="date" class="form-control time-end" value="{{date('Y-m-d')}}">
                                    </div>
                                    <button type="button" style="width: 8%; line-height: 20px;" class="btn btn-filter-order-all ml-1"
                                            data-url="{{url('admin/manage_financial/wallet/status_all/filter_date_all')}}"
                                    ><i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="main pl-2 pr-2">
                                <div class="main-unit align-items-center">
                                    <div class="d-flex justify-content-lg-around">
                                        <div
                                            class="tab-menu align-items-center status-all">
                                            <center>
                                                <a href="{{url('admin/manage_financial/wallet/status_all')}}">Tất cả</a>
                                            </center>
                                        </div>
                                        <div
                                            class="tab-menu align-items-center status-revenue">
                                            <center>
                                                <a href="{{url('admin/manage_financial/wallet/status_revenue')}}">Doanh thu</a>
                                            </center>
                                        </div>
                                        <div
                                            class="tab-menu align-items-center checked status-withdrawal">
                                            <center>
                                                <a href="{{url('admin/manage_financial/wallet/status_withdrawal')}}">Rút
                                                    tiền</a>
                                            </center>
                                        </div>
                                        <div
                                            class="tab-menu align-items-center status-refund">
                                            <center>
                                                <a class="popup_default">Hoàn tiền từ đơn
                                                    hàng</a>
                                            </center>
                                        </div>
                                        <div
                                            class="tab-menu align-items-center status-bonus">
                                            <center>
                                                <a class="popup_default">Hoa hồng giới thiệu</a>
                                            </center>
                                        </div>
                                        <div
                                            class="tab-menu align-items-center status-advertisement">
                                            <center>
                                                <a class="popup_default">Trừ phí quảng cáo</a>
                                            </center>
                                        </div>
                                        <div
                                            class="tab-menu align-items-center status-adjusted">
                                            <center>
                                                <a class="popup_default">Điều chỉnh</a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive table-responsive-lg p-1">
                                <table class="table data-list-view table-sm">
                                    <thead class="">
                                    <tr>
                                        <th class="text-center" scope="col">Ngày</th>
                                        <th class="text-center" scope="col">Loại giao dịch | Mô tả</th>
                                        <th>Tình trạng</th>
                                        <th class="text-center" scope="col">Số tiền</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($data_history) && isset($data_history))
                                        @foreach($data_history as $value)
                                            <tr class="text-center">
                                                <td>{{$value->date}}</td>
                                                <td>
                                                    <span class="title-dank">{{$value->is_type}} từ đơn hàng  #{{$value->order_code}}</span>
                                                    <br>
                                                    <span>{{$value->name_product_order}}</span>
                                                </td>
                                                <td>
                                                    @if($value->status == 0)
                                                        <div class="bg-orange bg-span">Đang xử lý</div>
                                                    @elseif($value->status == 1)
                                                        <div class="bg-green bg-span">Hoàn thành</div>
                                                    @else
                                                        <div class="bg-red bg-span">Đã hủy</div>
                                                    @endif
                                                </td>
                                                <td class="title-dank">{{number_format($value->price)}} đ</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="1000">Không có dữ liệu !</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-12 bank-unit p-0">
                <div class="card">
                    <div class="card-content">
                        <div class="logo-card p-2">
                            <?php if(isset($dataBankAccountRight)): ?>
                            <img src="../../assets/admin/app-assets/images/ncc/financial/card.png">
                            <?php endif; ?>
                            <span class="title">Tài khoản ngân hàng</span>
                        </div>
                        <div class="align-items-center justify-content-center mb-2 card-right">
                            <a class="view-bank-account"
                            >
                                <div class="card-unit-mgl-zezo mb-3">
                                    <div class="card-bg">
                                        <img class=""
                                             src="../../assets/admin/app-assets/images/ncc/financial/card_red.png" style="width: 100%">
                                        <div class="card-text p-2">
                                            <p class="text-white text-bold-700"><?= isset($dataBankAccountRight) ? $dataBankAccountRight->bank_name . ' - ' . $dataBankAccountRight->short_name : '' ?></p>
                                            <p class="text-white text-bold-700 number-account"><?= isset($dataBankAccountRight) ? $dataBankAccountRight->first_num_acc . ' *** *** ' . $dataBankAccountRight->last_num_acc : '' ?></p>
                                            <p class="text-white text-bold-700 name-account"><?= isset($dataBankAccountRight) ? mb_strtoupper($dataBankAccountRight->user_name) : '' ?></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="add-card d-flex mb-2">
                            <div class="add-card-tool pt-1" style="width: 100%!important;">
                                <button class="btn btn-delete-bank"
                                        data-url="<?= isset($dataBankAccountRight) ? 'admin/manage_financial/wallet/delete_bank_account' : '' ?>"
                                        data-id="<?= isset($dataBankAccountRight) ? $dataBankAccountRight->id : ''?>"
                                >
                                    <div class="text-center">
                                        <img
                                            src="../../assets/admin/app-assets/images/ncc/financial/business_and_finance.png">
                                    </div>
                                    <p class="mt-1">Huỷ liên kết ngân hàng</p>
                                </button>
                            </div>
                            <div class="add-card-tool pt-1" style="width: 100%!important;">
                                <button class="btn btn-add-bank-account">
                                    <div class="text-center">
                                        <img src="../../assets/admin/app-assets/images/ncc/financial/plus.png">
                                    </div>
                                    <p class="mt-1">Thêm tài khoản ngân hàng</p>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pagination">
            {{$data_history->links()}}
        </div>
    </div>
    <!-- END: Content-->
</div>

<!-- START: Popup Thêm tài khoản ngân hàng-->
<div class="popup-add-bank-account" style="z-index: 1000">
    <div class="container-popup-add-bank-account">
        <div class="header-popup-add-bank-account mb-2">
            <p class="text-black text-bold-700 f-18">Thêm tài khoản ngân hàng</p>
        </div>
        <div class="body-bank-account">
            <div class="form-group">
                <label class="lb">Họ và tên</label>
                <input id="name" type="text" class="form-control input-new" placeholder="Họ và tên">
            </div>
            <div class="form-group">
                <label class="lb">Số CMND</label>
                <input id="identify_card" type="number" class="form-control input-new" placeholder="Số CMND">
            </div>
            <div class="form-group">
                <label class="lb">Tên chủ tài khoản</label>
                <input id="user_name" type="text" class="form-control input-new" placeholder="Tên chủ tài khoản">
            </div>
            <div class="form-group">
                <label class="lb">Tên ngân hàng</label>
                <select id="bank_name" class="form-control option-new">
                    @if(isset($dataBank))
                        @foreach($dataBank as $valueBank)
                            <option
                                value="{{$valueBank->id}}">{{$valueBank->name ." (" .$valueBank->short_name .")"}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label class="lb">Số tài khoản</label>
                <input id="bank_account" type="number" class="form-control input-new" placeholder="Số tài khoản">
            </div>
            <div class="form-group">
                <label class="lb">Khu vực</label>
                <select id="branch" class="form-control option-new">
                    @if(isset($dataCity))
                        @foreach($dataCity as $valueCity)
                            <option value="{{$valueCity->id}}">{{$valueCity->name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div id="recaptcha-container-bank"></div>
        <div class="footer-popup d-flex align-items-center justify-content-lg-end mt-2">
            <button class="btn btn-cancel-bank-account">
                Hủy
            </button>
            <button class="btn btn-confirm-bank-account"
                    data-url="{{url('admin/manage_financial/wallet/create_bank_account')}}"
                    data-phone="<?= $dataLogin ? $dataLogin->phone : '' ?>"
            >
                Tiếp theo
            </button>
        </div>
    </div>
</div>
<!-- END: Popup Thêm tài khoản ngân hàng-->

<!-- START: Popup Xác minh SĐT-->
<div class="popup-verification" style="z-index: 1000">
    <div class="container-popup-verification">
        <div class="header-popup-verification mb-2 align-items-center justify-content-center">
            <p class="text-black text-bold-700 text-center font-size-24">Xác minh số điện thoại</p>
            <p class="mt-4 text-center">Vui lòng xác minh danh tính của bạn bằng cách nhập mã OTP được gửi đến số tiện
                thoại của bạn</p>
        </div>
        <div class="body-verification mt-3">
            <div class="form-group">
                <label class="lb">Mã xác minh</label>
                <input id="otp_bank" type="text" class="form-control input-new otp_bank" placeholder="Mã xác minh">
            </div>
        </div>
        <div class="footer-popup d-flex align-items-center justify-content-lg-end mt-3">
            <button class="btn btn-confirm-verification"
                    data-url="{{url('admin/manage_financial/wallet/save_bank_account')}}"
                    data-pin-code="<?= isset($dataWallet) ? $dataWallet->pin_code : 0 ?>"
            >
                Xác nhận
            </button>
            <button class="btn btn-cancel-verification">
                Hủy
            </button>
        </div>
    </div>
</div>
<!-- END: Popup Xác minh SĐT-->

<!-- START: Popup Rút tiền-->
<div class="popup-withdrawal" style="z-index: 1000">
    <div class="container-popup-withdrawal">
        <div class="header-popup-withdrawal">
            <p class="text-black text-bold-700 font-size-24">Rút tiền</p>
        </div>
        <div class="body-withdrawal mt-2">
            <div class="form-group">
                <span class="text-grey">Tài khoản ngân hàng</span>
                <select name="" id="bank_id" class="form-control option-new">
                    @if(isset($dataBankAccount))
                        @foreach($dataBankAccount as $valueBankAcc)
                            <option value="{{$valueBankAcc->id}}">{{$valueBankAcc->short_name . ' - ' . $valueBankAcc->bank_name . ' - ' .$valueBankAcc->bank_account}}</option>
                        @endforeach
                    @endif
                </select>
                @if(!count($dataBankAccount))
                    <span class="text-red text-bold-700 text-italic">Thêm tài khoản ngân hàng và thiết lập mã Pin để rút tiền</span>
                @endif
                @if(isset($data) && $data->pin_code == null)
                    <span class="text-red text-bold-700 text-italic">Vui lòng thiết lập mã Pin để rút tiền</span>
                @endif
            </div>
            <div class="form-group">
                <span class="text-grey">Số tiền muốn rút</span>
                <input id="price" type="number" class="form-control input-new" placeholder="Số tiền muốn rút" <?= isset($data) && ($data->pin_code == null) ? 'disabled' : ''?>>
            </div>
            <div class="form-group">
                <p class="text-grey">Phí thanh toán</p>
                <span class="text-black text-bold-700">11.000đ</span>
                <span class="text-grey">(phí rút tiền chỉ áp dụng cho các giao dịch thành công)</span>
            </div>
            <div class="form-group">
                <span class="text-grey">Số tiền chuyển vào tài khoản</span>
                <p class="text-red text-bold-700 font-size-17 price-show"></p>
            </div>
        </div>
        <div class="footer-popup-withdrawal d-flex align-items-center justify-content-lg-end mt-3">
            <button class="btn btn-confirm-withdrawal" <?= isset($data) && ($data->pin_code == null) ? 'disabled' : '' ?>
            data-url="{{url('admin/manage_financial/wallet/withdrawal')}}"
            >
                Xác nhận
            </button>
            <button class="btn btn-cancel-withdrawal">
                Hủy
            </button>
        </div>
    </div>
</div>
<!-- END: Popup Rút tiền-->

<!-- START: Popup Nhập mã Pin-->
<div class="popup-verification-withdrawal" style="z-index: 1000">
    <div class="container-popup-verification-withdrawal">
        <div class="header-popup-verification-withdrawal mb-2 align-items-center justify-content-center">
            <p class="mt-3 text-center">Để đảm bảo luôn an toàn. Bạn vui lòng nhập mã pin</p>
        </div>
        <div class="body-verification-withdrawal mt-3">
            <div class="form-group">
                <label class="lb">Mã Pin</label>
                <input id="pin_code_input" type="password" class="form-control input-new">
            </div>
        </div>
        <div class="footer-popup-verification-withdrawal d-flex align-items-center justify-content-center mt-3">
            <button class="btn btn-confirm-verification-withdrawal"
                    data-url="{{url('admin/manage_financial/wallet/withdrawal/save')}}"
            >
                Xác nhận
            </button>
            <button class="btn btn-cancel-verification-withdrawal">
                Hủy
            </button>
        </div>
        <div class="mt-3">
            <p class="text-black text-bold-700 text-center text-underline forgot-pin-code">Quên mã Pin?</p>
        </div>
    </div>
</div>
<!-- END: Popup Nhập mã Pin-->

<!-- START: Popup Đổi mã Pin-->
<div class="popup-setting-pin-code" style="z-index: 1000">
    <div class="container-popup-setting-pin-code">
        <div class="header-setting-pin-code">
            <p class="text-bold-700 text-black font-size-24">Thiết lập mã pin</p>
            <p class="mt-3 mb-4">Vui lòng không sử dụng các thông tin như: Dãy số liên tiếp, dãy số lặp lại, ngày sinh nhật hoặc mật khẩu tài khoản ngân hàng làm mật khẩu của ví</p>
        </div>
        <div class="body-setting-pin-code">
            <div class="form-group">
                <label class="lb">Mã pin mới</label>
                <input pattern=" 0+\.[0-9]*[1-9][0-9]*$" name="itemConsumption"
                       onkeypress="return event.charCode >= 48 && event.charCode <= 57"
                       id="pin_code" type="password" class="form-control input-new" placeholder="Nhập mật khẩu">
            </div>
            <div class="form-group">
                <label class="lb">Xác nhận lại mã pin</label>
                <input pattern=" 0+\.[0-9]*[1-9][0-9]*$" name="itemConsumption"
                       onkeypress="return event.charCode >= 48 && event.charCode <= 57"
                       id="re_pin_code" type="password" class="form-control input-new" placeholder="Nhập lại mật khẩu">
            </div>
        </div>
        <div id="recaptcha-container"></div>
        <div class="footer-setting-pin-code d-flex align-items-center justify-content-lg-end mt-3">
            <button class="btn btn-cancel-setting-pin-code">
                Hủy
            </button>
            <button class="btn btn-confirm-setting-pin-code"
                    data-url="{{url('admin/manage_financial/payment_setting/create_pin_code')}}"
                    data-phone="<?= $dataLogin ? $dataLogin->phone : '' ?>"
            >
                Tiếp theo
            </button>
        </div>
    </div>
</div>
<!-- END: Popup Đổi mã Pin-->

<!-- START: Popup Xác thực mã Pin-->
<div class="popup-verification-pin-code" style="z-index: 1000">
    <div class="container-popup-verification-pin-code">
        <div class="header-popup-verification mb-2 align-items-center justify-content-center">
            <p class="text-black text-bold-700 text-center font-size-24">Xác minh số điện thoại</p>
            <p class="mt-4 text-center">Vui lòng xác minh danh tính của bạn bằng cách nhập mã OTP được gửi đến số tiện
                thoại của bạn</p>
        </div>
        <div class="body-verification-pin-code mt-3">
            <div class="form-group">
                <label class="lb">Mã xác minh</label>
                <input id="otp" type="text" class="form-control input-new otp" placeholder="Mã xác minh">
            </div>
        </div>
        <div class="footer-popup d-flex align-items-center justify-content-lg-end mt-3">
            <button class="btn btn-confirm-verification-pin-code"
                    data-url="{{url('admin/manage_financial/payment_setting/save_pin_code')}}"
            >
                Xác nhận
            </button>
            <button class="btn btn-cancel-verification-pin-code">
                Hủy
            </button>
        </div>
    </div>
</div>
<!-- END: Popup Xác thực mã Pin-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-firestore.js"></script>
<script src="{{url('assets/admin/js/otp/index.js')}}"></script>
<script src="{{url('assets/admin/js/financial/wallet.js')}}"></script>
</body>
<!-- END: Footer-->

<!-- END: Body-->
</html>

