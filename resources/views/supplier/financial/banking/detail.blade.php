<div class="popup-info-bank-account">
    <div class="container-popup-info-bank-account">
        <div class="header-popup-info-bank-account mb-3">
            <p class="text-black text-bold-700 font-size-24">Tài khoản ngân hàng</p>
        </div>
        <div class="body-bank-account">
            <div>
                <span class="text-grey text-bold-700">Tên chủ tài khoản </span>
                <p class="text-black text-bold-700 font-size-17"><?= isset($dataInfoBankAcc) ? $dataInfoBankAcc->user_name : '' ?></p>
            </div>
            <div>
                <span class="text-grey text-bold-700">Tên ngân hàng</span>
                <p class="text-black text-bold-700 font-size-17"><?= isset($dataInfoBankAcc) ? $dataInfoBankAcc->bank_name .' - ' .$dataInfoBankAcc->short_name : '' ?></p>
            </div>
            <div>
                <span class="text-grey text-bold-700">Số tài khoản</span>
                <p class="text-black text-bold-700 font-size-17"><?= isset($dataInfoBankAcc) ? '*** *** ' .$dataInfoBankAcc->last_num_acc : '' ?></p>
            </div>
            <div>
                <span class="text-grey text-bold-700">Khu vực</span>
                <p class="text-black text-bold-700 font-size-17"><?= isset($dataInfoBankAcc) ? $dataInfoBankAcc->branch : '' ?></p>
            </div>
            <div class="footer-popup-info-bank-account d-flex align-items-center justify-content-lg-end mt-3">
                <button class="btn btn-cancel-info-bank-account">
                    Quay lại
                </button>
            </div>
        </div>
    </div>
</div>
