<?php
$page = 'financial_wallet_supplier';
$data_login = Session::get('data_supplier');
use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/financial/style.css')}}">
<style>
    .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 35px;
        background: #ffe6e6;
        border-radius: 0.1875rem;
        outline: none;
        -webkit-transition: .2s;
        transition: opacity .2s;
        /*pointer-events: none;*/
    }

    .slider:hover {
        opacity: 1;
    }

    .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 45px;
        height: 40px;
        cursor: pointer;
        background-color: red;
        border-radius: 3px 0 0 3px !important;
    }

    .slider::-moz-range-thumb {
        width: 45px;
        height: 35px;
        background: #04AA6D;
        cursor: pointer;
    }
</style>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="d-flex bank-account ml-3 mt-2 mb-2">
                    <div>
                        <img src="../../assets/admin/app-assets/images/ncc/financial/wallet.png">
                    </div>
                    <div class="ml-2">
                        <p class="title">Tài khoản ngân hàng</p>
                    </div>
                </div>
                <div class="d-flex flex-wrap card-main">
                    @if(isset($dataBankAccount))
                        @foreach($dataBankAccount as $valueBankAccount)
                            <a class="view-bank-account"
                               data-url="{{url('admin/manage_financial/banking/add/info/'.$valueBankAccount->id)}}"
                            >
                                <div class="card-unit mb-2">
                                    <div class="card-bg">
                                        <img class=""
                                             src="../../assets/admin/app-assets/images/ncc/financial/card_red.png">
                                        <div class="card-text p-2">
                                            <p class="text-white text-bold-700">{{$valueBankAccount->bank_name . " - " . $valueBankAccount->short_name}}</p>
                                            <p class="text-white text-bold-700 number-account">{{$valueBankAccount->first_num_acc . " *** *** " .$valueBankAccount->last_num_acc}}</p>
                                            <p class="text-white text-bold-700 name-account">{{mb_strtoupper($valueBankAccount->user_name)}}</p>
                                        </div>
                                    </div>
                                </div>
                                <label class="container_radio text-black text-bold-700 ml-3 mb-3">Đặt tài khoản làm mặc định
                                    <input type="radio" name="radio" @if($valueBankAccount->is_default == 1) checked @endif class="check-r"
                                           data-id="{{$valueBankAccount->id}}"
                                           data-url-default="{{url('admin/manage_financial/banking/check_card')}}"
                                    >
                                    <span class="radio_config"></span>
                                </label>
                            </a>
                        @endforeach
                    @endif
                </div>
                <div class="col-lg-3 col-md-12 card-add mb-3 text-center">
                    <img src="../../assets/admin/app-assets/images/ncc/financial/plus.png">
                    <div class="mt-1">
                        <span class="title-card">Thêm tài khoản ngân hàng</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="show_popup">

</div>
<!-- END: Content-->
<!-- BEGIN: Footer-->
@include('admin.base.footer')
<!-- END: Footer-->
@include('admin.base.script')
</body>
<script>
    $(document).ready(function () {
        $(document).on("click", ".view-bank-account", function () {
            let url = $(this).attr('data-url');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.show_popup').addClass('active');
                    $('.show_popup').html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });
        $(document).on("click", ".btn-cancel-info-bank-account", function () {
            $('.show_popup').removeClass('active');
        });
        $(document).on("change", ".check-r", function () {
            let url = $('.check-r').attr('data-url-default');
            let data = {};
            data['bank_id'] = $(this).attr('data-id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                success: function (data) {
                    // if (data.status) {
                    //     $('.show_popup').addClass('active');
                    //     $('.show_popup').html(data);
                    // } else {
                    //     alert('Vui lòng thử lại');
                    // }
                },
                error: function () {
                    console.log('error')
                }
            });
        });
        let verificationId;
        $(document).on('change', '#myRange', async function () {
            let value = $(this).val();
            if (value == 100) {
                $('#recaptcha-container').show();
                let applicationVerifier = new firebase.auth.RecaptchaVerifier(
                    'recaptcha-container');
                let phone = $('.input-phone').val();
                let fs = phone.slice(0, 3);
                if (fs !== '+84') {
                    phone = phone.slice(1, phone.length);
                    phone = '+84' + phone;
                }
                let provider = new firebase.auth.PhoneAuthProvider();
                try {
                    verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                    $('#recaptcha-container').hide();
                    $('.form-otp').prop('disabled', false);
                } catch (e) {
                    console.log('e');
                    swal("Thông báo", "Số điện thoại vừa nhập không đúng !", "error");
                    // setTimeout();
                    // location.reload();
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                }
            }
        });

        $(document).on('click', '.btn-add-account', async function () {
            let data = [];
            data['user_name'] = $('#user_name').val();
            data['bank_name'] = $('#bank_name').val();
            data['bank_account'] = $('#bank_account').val();
            data['date_active'] = $('#date_active').val();
            data['identify_card'] = $('#identify_card').val();
            data['branch'] = $('#branch').val();
            if (!data['user_name'] || !data['bank_account'] || !data['identify_card'] || !data['branch'] || !data['date_active']) {
                swal("Thông báo", "Vui lòng nhập đủ thông tin !", "error");
                return false;
            }
            try {
                let verificationCode = $('.form-otp').val();
                let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
                try {
                    await firebase.auth().signInWithCredential(phoneCredential);
                    let url = $(this).attr('data-url');
                    let form_data = new FormData();
                    form_data.append('user_name', $('#user_name').val());
                    form_data.append('bank_name', $('#bank_name').val());
                    form_data.append('bank_account', $('#bank_account').val());
                    form_data.append('date_active', $('#date_active').val());
                    form_data.append('identify_card', $('#identify_card').val());
                    form_data.append('branch', $('#branch').val());
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: form_data,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status) {
                                swal("Thông báo", data.msg, "success");
                                window.location.href = data.url;
                            } else {
                                swal("Thông báo", data.msg, "error");
                            }
                        },
                        error: function () {
                            console.log('error');
                        },
                    });
                } catch (e) {
                    swal("Thông báo", "Mã OTP không chính xác !", "error");
                }
            } catch (e) {
                swal("Thông báo", "Đã có lỗi xảy ra !", "error");
            }
        });
    });
</script>
<!-- END: Page JS-->


<!-- END: Body-->

</html>

