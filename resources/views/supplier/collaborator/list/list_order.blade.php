<?php
$page = 'manage_ctv';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-1">
                <!-- content -->
                <div class="table-responsive table-responsive-lg border_table align-items-center">
                    <table class="table data-list-view table-sm">
                        <thead class="bg_table text-center">
                        <tr>
                            <th scope="col">Mã đơn hàng</th>
                            <th scope="col">Hình thức thanh toán</th>
                            <th scope="col">Đơn vị vận chuyển</th>
                            <th scope="col">Ngày đặt hàng</th>
                            <th scope="col">Tổng tiền</th>
                            <th scope="col">Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(count($data))
                                @foreach($data as $value)
                                    <tr class="text-center">
                                        <td>
                                            {{$value->order_code}}
                                            <div>
                                                <span style="color: #ff0000; font-style: italic">{{$value->type_order}}</span>
                                            </div>
                                        </td>
                                        <td>{{$value->payment}}</td>
                                        <td>{{$value->shipping}}</td>
                                        <td>{{$value->created_at}}</td>
                                        <td>{{number_format($value->total_price)}} đ</td>
                                        <td>
                                            <a class="text-blue text-underline text-bold-700" href="{{url('admin/manage_ctv/list_order/detail/'.$value->id)}}">Xem chi tiết</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>Không có dữ liệu</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')

<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

