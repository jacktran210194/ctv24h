<?php
$page = 'manage_ctv';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/manage_ctv/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="form-label-group position-relative has-icon-left">

        </div>
        {{--        <h5 id="count" style="color: #FF0000"></h5>--}}
        <div class="card p-1">
            <div class="card-content">
                <div class="main">
                    <div class="main-unit align-items-center">
                        <div class="col-lg-3 col-md-12 p-0 d-flex">
                            <div class="tab-menu col-lg-6 align-items-center <?= $type === 'list' ? 'checked' : '' ?>">
                                <center>
                                    <a href="<?= URL::to('admin/manage_ctv') ?>">Danh sách CTV</a>
                                </center>
                            </div>
                            <div class="tab-menu col-lg-6 align-items-center <?= $type === 'dropship' ? 'checked' : '' ?>">
                                <center>
                                    <a href="<?= URL::to('admin/manage_ctv/dropship') ?>">Hàng Dropship</a>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>

{{--                <?php if ($type === 'dropship'): ?>--}}
{{--                @include('supplier.collaborator.dropship.dropship')--}}
{{--                <?php else: ?>--}}
{{--                @include('supplier.collaborator.list.index')--}}
{{--                <?php endif; ?>--}}

                <div class="d-flex mt-2 mb-2 second">
                    <div class="col-lg-3 col-md-12 p-0 search">
                        <input type="text" id="search" class="form-control" name="search" placeholder="Tìm kiếm">
                        <div class="form-control-position">
                            <i class="feather icon-search search-icon"></i>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12">
                    </div>
                    <div class="d-flex col-lg-6 col-md-12 p-0 align-items-center justify-content-lg-end">
                        <span>Ngày gia nhập</span>
                        <div class="col-lg-4">
                            <input type="datetime-local" value="" class="form-control">
                        </div>
                        <div class="btn-export">
                            <center>
                                <a>Xuất báo cáo</a>
                            </center>
                        </div>
                        <img src="../../assets/admin/app-assets/images/ncc/manage_ctv/small_menu.png">
                    </div>
                </div>

                <div class="mb-1 count">
                    <span id="count"></span>
                </div>
                <!-- content -->
                <div class="table-responsive table-responsive-lg border_table">
                    <table class="table data-list-view table-sm">
                        <thead class="bg_table">
                        <tr>
                            <th scope="col">Tên shop CTV</th>
                            <th scope="col">Hạng shop</th>
                            <th scope="col">Số lượng sản phẩm dropship</th>
                            <th scope="col">Tổng sản phẩm bán ra</th>
                            <th scope="col">Tổng doanh thu</th>
{{--                            <th scope="col">Thao tác</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        {{--                        @if(count($data))--}}
                        {{--                            @foreach($data as $value)--}}
                        {{--                                <tr>--}}
                        {{--                                    <td>{{$value->name_collaborator}}</td>--}}
                        {{--                                    <td>{{$value->rank}}</td>--}}
                        {{--                                    <td>{{$value->dropship}}</td>--}}
                        {{--                                    <td>{{$value->quantity}}</td>--}}
                        {{--                                    <td>{{number_format($value->total_price)}} đ</td>--}}
                        {{--                                    <td>--}}
                        {{--                                        <a href="">Đơn hàng đã bán</a>--}}
                        {{--                                    </td>--}}
                        {{--                                </tr>--}}
                        {{--                            @endforeach--}}
                        {{--                        @else--}}
                        {{--                            <tr>--}}
                        {{--                                <td>Không có dữ liệu</td>--}}
                        {{--                            </tr>--}}
                        {{--                        @endif--}}
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        fetch_collaborator_data();
        $(document).on("click", ".btn-active", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            $('#myModal').modal('show');
        });

        function fetch_collaborator_data(query = '') {
            $.ajax({
                url: "{{ route('search') }}",
                method: 'GET',
                data: {query: query},
                dataType: 'json',
                success: function (data) {
                    $('tbody').html(data.table_data);
                    $('#count').html(data.total_data + ' CTV')
                }
            })
        }

        $(document).on('keyup', '#search', function () {
            let query = $(this).val();
            fetch_collaborator_data(query);
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

