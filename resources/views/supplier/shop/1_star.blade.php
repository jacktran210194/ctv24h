<div class="table-responsive table-responsive-lg border_table mt-3">
    <table class="table data-list-view table-sm">
        <thead>
        <tr class="bg_table">
            <th></th>
            <th>Sản phẩm</th>
            <th width="40%">Đánh giá của khách</th>
            <th width="20%" class="text-center">Trả lời đánh giá</th>
        </tr>
        </thead>
        <tbody>
        @if(count($data) && $data)
            @foreach($data as $value)
                <tr>
                    <td><input data-id="{{$value->id}}" @if($value['status_reply'] == 1) disabled @endif name="check_rep" class="check_rep" type="radio"></td>
                    <td>
                        <div class="d-flex align-items-center">
                            <img class="border_table mr-1" width="65px" height="63px"
                                 src="{{$value->product_image}}"
                                 alt="">
                            <div class="info_product">
                                <span class="text-bold-700">{{$value->product_name}}</span>
                                <br>
                                <span
                                    class="text-sliver text-bold-700 f-12">ID đơn hàng: {{$value->order_code}}</span>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <img class="mr-2" width="70px" height="auto"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/1sao.png"
                                 alt="">
                            <span class="m-0 f-12">{{date('H:i:s d-m-Y',strtotime($value->created_at))}}</span>
                        </div>
                        <div class="content_review">
                            <p class="text-sliver f-12">{{$value->customer_comment}}</p>
                        </div>
                        <div class="img_review d-flex">
                            @if($value['is_image'] == 1)
                                @if(strpos($value['video_review'], ".mp4") == true)
                                    <div class="relative" style="margin-right: 10px;">
                                        <video width="65px" height="63px" controls class="video-comment border_table mr-1">
                                            <source src="{{$value['video_review']}}">
                                        </video>
                                        {{--                                        <div class="icon-play-video d-flex align-items-center justify-content-lg-center"--}}
                                        {{--                                             data-src="{{$value['video_review']}}"--}}
                                        {{--                                        >--}}
                                        {{--                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
                                        {{--                                                <path d="M7.99998 0C3.58172 0 0 3.58172 0 7.99998C0 12.4183 3.58172 16 7.99998 16C12.4183 16 16 12.4183 16 7.99998C15.9953 3.58369 12.4163 0.00472098 7.99998 0ZM11.3691 8.25488C11.3137 8.36598 11.2237 8.45604 11.1125 8.51142V8.51427L6.54113 10.8C6.25881 10.9411 5.91562 10.8266 5.77453 10.5443C5.73442 10.464 5.71376 10.3754 5.71426 10.2857V5.71429C5.71413 5.39869 5.96983 5.14275 6.28543 5.14259C6.37419 5.14255 6.46175 5.16318 6.54113 5.20285L11.1125 7.48858C11.395 7.62934 11.5099 7.97243 11.3691 8.25488Z" fill="white"/>--}}
                                        {{--                                            </svg>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                @else
                                    <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                         src="{{$value->image_review}}"
                                         alt="">
                                @endif
                            @endif
                        </div>
                    </td>
                    <td class="text-center">
                        @if($value->status_reply == 0)
                            <button data-url="{{url('admin/shop_supplier/review/reply')}}" data-id="{{$value->id}}"
                                    class="btn btn_main btn_reply">Trả lời
                            </button>
                        @else
                            {{$value->reply}}
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr class="text-center text-bold-700">
                <td colspan="100">Không có dữ liệu !</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
