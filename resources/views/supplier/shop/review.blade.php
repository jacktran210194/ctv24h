<?php
$page = 'shop_review';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>
<style>
    .btn_sao {
        margin-right: 3px;
    }
</style>
<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-9">
                <div class="card p-1">
                    {{--                    <div class="form-label-group position-relative has-icon-right mb-3" style="width: 30%;">--}}
                    {{--                        <input type="text" id="search" class="form-control" name="search"--}}
                    {{--                               placeholder="Tìm kiếm sản phẩm...">--}}
                    {{--                        <div class="form-control-position">--}}
                    {{--                            <img src="../../assets/admin/app-assets/images/icon_dashboard/search.png" alt="">--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <div class="form_filter d-flex">
                        <div class="title">
                            <h3 class="text-bold-700 f-24 mr-2">Đánh giá Shop</h3>
                        </div>
                        <div class="list_filter">
                            <button class="btn border btn_main btn_all_star btn_sao text-bold-700">Tất cả</button>
                            <button class="btn border btn_1_star btn_sao text-bold-700">1 Sao <img
                                    src="../../assets/admin/app-assets/images/icon_dashboard/sao.png" alt=""></button>
                            <button class="btn border btn_2_star btn_sao text-bold-700">2 Sao <img
                                    src="../../assets/admin/app-assets/images/icon_dashboard/sao.png" alt=""></button>
                            <button class="btn border btn_3_star text-bold-700">3 Sao <img
                                    src="../../assets/admin/app-assets/images/icon_dashboard/sao.png" alt=""></button>
                            <button class="btn border btn_4_star btn_sao text-bold-700">4 Sao <img
                                    src="../../assets/admin/app-assets/images/icon_dashboard/sao.png" alt=""></button>
                            <button class="btn border btn_5_star btn_sao text-bold-700">5 Sao <img
                                    src="../../assets/admin/app-assets/images/icon_dashboard/sao.png" alt=""></button>
                        </div>
                        {{--                        <div class="position-absolute" style="right: 2%;">--}}
                        {{--                            <select id="status" class="form-control">--}}
                        {{--                                <option value="">Tất cả</option>--}}
                        {{--                                <option value="">Chưa xử lý</option>--}}
                        {{--                                <option value="">Đã xử lý</option>--}}
                        {{--                            </select>--}}
                        {{--                        </div>--}}
                    </div>
                    <div class="get_data_ajax">

                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card p-1">
                    <div class="info_shop text-center mt-1">
                        <img width="72px" height="72px" class="border-tron mb-1"
                             src="{{$shop->image}}"
                             alt="">
                        <h3 class=" text-bold-700">{{$shop->name}}</h3>
                    </div>
                    <div class="reply_review text-center mt-2 p-1">
                        <textarea class="form-control" id="content_reply" cols="30" rows="8"
                                  placeholder="Trả lời người mua"></textarea>
                        <button disabled data-url="{{url('admin/shop_supplier/review/reply')}}" class="btn btn_main btn_reply_1 mt-1">Gửi trả lời đánh giá</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        let url_filter = window.location.origin;
        let url = url_filter + '/admin/shop_supplier/review/all_star';
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $(".get_data_ajax").html(data);
            },
            error: function () {
                console.log('error')
            }
        });

        //tất cả sao
        $(document).on('click', '.btn_all_star', function () {
            let url = url_filter + '/admin/shop_supplier/review/all_star'
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.btn_all_star').addClass('btn_main');
                    $('.btn_1_star').removeClass('btn_main');
                    $('.btn_2_star').removeClass('btn_main');
                    $('.btn_3_star').removeClass('btn_main');
                    $('.btn_4_star').removeClass('btn_main');
                    $('.btn_5_star').removeClass('btn_main');
                    $(".get_data_ajax").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //1 sao
        $(document).on('click', '.btn_1_star', function () {
            let url = url_filter + '/admin/shop_supplier/review/1_star'
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.btn_all_star').removeClass('btn_main');
                    $('.btn_1_star').addClass('btn_main');
                    $('.btn_2_star').removeClass('btn_main');
                    $('.btn_3_star').removeClass('btn_main');
                    $('.btn_4_star').removeClass('btn_main');
                    $('.btn_5_star').removeClass('btn_main');
                    $(".get_data_ajax").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //2 sao
        $(document).on('click', '.btn_2_star', function () {
            let url = url_filter + '/admin/shop_supplier/review/2_star'
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.btn_all_star').removeClass('btn_main');
                    $('.btn_1_star').removeClass('btn_main');
                    $('.btn_2_star').addClass('btn_main');
                    $('.btn_3_star').removeClass('btn_main');
                    $('.btn_4_star').removeClass('btn_main');
                    $('.btn_5_star').removeClass('btn_main');
                    $(".get_data_ajax").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //3 sao
        $(document).on('click', '.btn_3_star', function () {
            let url = url_filter + '/admin/shop_supplier/review/3_star'
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.btn_all_star').removeClass('btn_main');
                    $('.btn_1_star').removeClass('btn_main');
                    $('.btn_2_star').removeClass('btn_main');
                    $('.btn_3_star').addClass('btn_main');
                    $('.btn_4_star').removeClass('btn_main');
                    $('.btn_5_star').removeClass('btn_main');
                    $(".get_data_ajax").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //4 sao
        $(document).on('click', '.btn_4_star', function () {
            let url = url_filter + '/admin/shop_supplier/review/4_star'
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.btn_all_star').removeClass('btn_main');
                    $('.btn_1_star').removeClass('btn_main');
                    $('.btn_2_star').removeClass('btn_main');
                    $('.btn_3_star').removeClass('btn_main');
                    $('.btn_4_star').addClass('btn_main');
                    $('.btn_5_star').removeClass('btn_main');
                    $(".get_data_ajax").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //5 sao
        $(document).on('click', '.btn_5_star', function () {
            let url = url_filter + '/admin/shop_supplier/review/5_star'
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.btn_all_star').removeClass('btn_main');
                    $('.btn_1_star').removeClass('btn_main');
                    $('.btn_2_star').removeClass('btn_main');
                    $('.btn_3_star').removeClass('btn_main');
                    $('.btn_4_star').removeClass('btn_main');
                    $('.btn_5_star').addClass('btn_main');
                    $(".get_data_ajax").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        $(document).on('click', '.btn_reply', function () {
            let url = $(this).attr('data-url');
            let id = $(this).attr('data-id');
            $('.btn-confirm-rep').attr('data-url', url);
            $('.btn-confirm-rep').attr('data-id', id);
            jQuery.noConflict();
            $('#myModal_rep').modal('show');
        });

        $(document).on('click','.btn-confirm-rep',function () {
            let url = $(this).attr('data-url');
            let id = $(this).attr('data-id');
            let form_data = new FormData();
            form_data.append('id',id);
            form_data.append('reply',$('#reply').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        swal('Thông báo','Trả lời thành công !','success');
                        location.reload();
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });

        $(document).on('click','.check_rep',function () {
            let id = $(this).attr('data-id');
            if($(this).is(':checked')){
                $('.btn_reply_1').attr('data-id',id);
                $('.btn_reply_1').prop('disabled',false);
            }else{
                alert('Lỗi');
            }
        });

        $(document).on('click','.btn_reply_1',function () {
            let url = $(this).attr('data-url');
            let id = $(this).attr('data-id');
            let form_data = new FormData();
            form_data.append('id',id);
            form_data.append('reply',$('#content_reply').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        swal('Thông báo','Trả lời thành công !','success');
                        location.reload();
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });

    })
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
