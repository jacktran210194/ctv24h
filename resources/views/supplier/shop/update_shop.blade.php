<?php
$page = 'shop_update';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>
<style>
</style>
<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content card-body">
                <h3 class="text-bold-700 f-24">Thông tin cập nhật shop</h3>
                @if($data_shop->active != 1)
                    <span class="f-12 text-bold-700"
                          style="color: red;">Cập nhập đầy đủ thông tin để bắt đầu bán hàng !</span>
                    <br>
                    <br>
                @endif
                <div class="content-info ml-5" style="width: 60%;">
                    <div class="form-group">
                        <label class="text-bold-700 input_required" for="">Tên doanh nghiệp</label>
                        <input id="name_company" type="text" class="form-control input_config"
                               value="{{isset($data_shop) ? $data_shop->name_company : ''}}">
                    </div>
                    <div class="form-group">
                        <label class="text-bold-700 input_required" for="">Ngành nghề kinh doanh</label>
                        <input id="business" type="text" class="form-control input_config"
                               value="{{isset($data_shop) ? $data_shop->business : ''}}">
                    </div>
                    <div class="form-group">
                        <label class="text-bold-700 input_required" for="">Họ và tên</label>
                        <input disabled id="name_supplier" type="text" class="form-control input_config"
                               value="{{isset($data_login) ? $data_login->name : ''}}">
                    </div>
                    <div class="form-group">
                        <label class="text-bold-700 input_required" for="">Ngày sinh</label>
                        <input disabled id="birthday_supplier" type="date" class="form-control input_config"
                               value="{{isset($data_login) ? date('Y-m-d',strtotime($data_login->birthday)) : ''}}">
                    </div>
                    <div class="form-group">
                        <label class="text-bold-700 input_required" for="">Số CMND/CCCD</label>
                        <input id="card_id" type="number" class="form-control input_config"
                               value="{{isset($data_shop) ? $data_shop->card_id : ''}}">
                    </div>
                    <div class="form-group">
                        <label class="text-bold-700 input_required" for="">Ngày cấp</label>
                        <input id="card_date" type="date" class="form-control input_config"
                               value="{{isset($data_shop) ? date('Y-m-d',strtotime($data_shop->card_date_create)) : ''}}">
                    </div>
                    <div class="form-group">
                        <label class="text-bold-700 input_required" for="">Nơi cấp</label>
                        <input id="card_address" type="text" class="form-control input_config"
                               value="{{isset($data_shop) ? $data_shop->card_address : ''}}">
                    </div>
                </div>
                <div class="content_image ml-5">
                    <div class="d-flex align-items-center">
                        <div class="card_before">
                            <span class="text-bold-700 input_required" for="">Căn Cước Công Dân (Mặt trước)</span><br>
                            <img class="border_shadow" id="img_1" width="300px" height="189px"
                                 src="{{isset($data_shop) && File::exists(substr($data_shop['image_card_1'], 1)) ? $data_shop['image_card_1'] : '../../assets/admin/app-assets/images/icon_dashboard/tai_file.png'}}"
                                 alt="">
                            <input required id="image_card_1" type="file" class="form-control hidden"
                                   onchange="show_img_1(this)">
                        </div>
                        <div class="card_after ml-3">
                            <span class="text-bold-700 input_required" for="">Căn Cước Công Dân (Mặt sau)</span><br>
                            <img class="border_shadow" id="img_2" width="300px" height="189px"
                                 src="{{isset($data_shop) && File::exists(substr($data_shop['image_card_2'], 1)) ? $data_shop['image_card_2'] : '../../assets/admin/app-assets/images/icon_dashboard/tai_file.png'}}"
                                 alt="">
                            <input required id="image_card_2" type="file" class="form-control hidden"
                                   onchange="show_img_2(this)">
                        </div>
                    </div>
                    <div class="d-flex align-items-center mt-2">
                        <div class="card_before">
                            <span class="text-bold-700 input_required" for="">Giấy phép đăng ký kinh doanh</span><br>
                            <img class="border_shadow" id="img_3" width="300px" height="189px"
                                 src="{{isset($data_shop) && File::exists(substr($data_shop['business_license'], 1)) ? $data_shop['business_license'] : '../../assets/admin/app-assets/images/icon_dashboard/tai_file.png'}}"
                                 alt="">
                            <input required id="business_license" type="file" class="form-control hidden"
                                   onchange="show_img_3(this)">
                        </div>
                        <div class="card_after ml-3">
                            <span class="text-bold-700 input_required" for="">Các giấy tờ liên quan</span><br>
                            <div class="d-flex align-items-center">
                                <img class="border_shadow" id="img_4" width="300px" height="189px"
                                     src="{{isset($data_shop) && File::exists(substr($data_shop['related_document_1'], 1)) ? $data_shop['related_document_1'] : '../../assets/admin/app-assets/images/icon_dashboard/tai_file.png'}}"
                                     alt="">
                                <input required id="related_document_1" type="file" class="form-control hidden"
                                       onchange="show_img_4(this)">
                                <img id="img_5" class="ml-2 border_shadow" width="300px" height="189px"
                                     src="{{isset($data_shop) && File::exists(substr($data_shop['related_document_2'], 1)) ? $data_shop['related_document_2'] : '../../assets/admin/app-assets/images/icon_dashboard/tai_file.png'}}"
                                     alt="">
                                <input required id="related_document_2" type="file" class="form-control hidden"
                                       onchange="show_img_5(this)">
                            </div>
                        </div>
                    </div>
                    <div class="d-flex align-items-center justify-content-end mr-4 mt-4">
                        <a href="{{url('admin/supplier_admin')}}" style="width: 100px!important;"
                           class="btn btn_sliver text-bold-700 mr-2">Huỷ</a>
                        <button data-url="{{url('admin/shop_supplier/update_info/save')}}"
                                data-shop="{{isset($data_shop) ? $data_shop->id : ''}}" style="width: 100px!important;"
                                class="btn btn_main text-bold-700 btn-save">Lưu
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="position-fixed position-center popup_confirm_shop" style="display: none; z-index: 200000000;">
    <img src="../../assets/admin/app-assets/images/icon_dashboard/confirm_shop.png" alt="">
    <button style="background: transparent;border: none;right: 5%; top:5%;" class="position-absolute btn_exit_confirm">
        <i class="fa fa-times fa-2x"></i>
    </button>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    //show img
    function show_img_1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img_1').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function show_img_2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img_2').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function show_img_3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img_3').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function show_img_4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img_4').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function show_img_5(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img_5').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function () {
        $('#img_1').click(function () {
            $('#image_card_1').click();
        });
        $('#img_2').click(function () {
            $('#image_card_2').click();
        });
        $('#img_3').click(function () {
            $('#business_license').click();
        });
        $('#img_4').click(function () {
            $('#related_document_1').click();
        });
        $('#img_5').click(function () {
            $('#related_document_2').click();
        });

        $(document).on('click', '.btn-save', function () {
            let name_company = $('#name_company').val();
            let business = $('#business').val();
            let card_id = $('#card_id').val();
            let specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-=0123456789";
            let check = function (string) {
                for (i = 0; i < specialChars.length; i++) {
                    if (string.indexOf(specialChars[i]) > -1) {
                        return true;
                    }
                }
                return false;
            };
            if ($('#name_company').val() == '') {
                swal('Thông báo', 'Tên doanh nghiệp không được để trống !', 'warning');
            } else if ($('#business').val() == '') {
                swal('Thông báo', 'Ngành nghề kinh doanh không được để trống !', 'warning');
            } else if ($('#card_id').val() == '') {
                swal('Thông báo', 'CMND/CCCD không được để trống', 'warning');
            } else if ($('#card_date').val() == '') {
                swal('Thông báo', 'Ngày cấp không được để trống', 'warning');
            } else if ($('#card_address').val() == '') {
                swal('Thông báo', 'Nơi cấp không được để trống', 'warning');
            } else if (check(name_company)) {
                swal('Thông báo', 'Tên doanh nghiệp không được phép nhập ký tự đặc biệt hoặc số !', 'warning');
            } else if (check(business)) {
                swal('Thông báo', 'Ngành nghề kinh doanh không được phép nhập ký tự đặc biệt hoặc số !', 'warning');
            } else if (card_id.length > 12 || card_id.length < 8 ) {
                swal('Thông báo', 'CMND / CCCD phải đủ 9-12 số !', 'warning');
            } else {
                let url = $(this).attr('data-url');
                let shop_id = $(this).attr('data-shop');
                let form_data = new FormData();
                let image_card_1 = $('#image_card_1')[0].files[0];
                let image_card_2 = $('#image_card_2')[0].files[0];
                let business_license = $('#business_license')[0].files[0];
                let related_document_1 = $('#related_document_1')[0].files[0];
                let related_document_2 = $('#related_document_2')[0].files[0];
                if (image_card_1 == undefined || image_card_2 == undefined || business_license == undefined || related_document_1 == undefined || related_document_2 == undefined) {
                    swal('Thông báo', 'Vui lòng thêm đầy đủ ảnh và giấy tờ', 'warning');
                } else {
                    form_data.append('shop_id', shop_id);
                    form_data.append('name_company', $('#name_company').val());
                    form_data.append('business', $('#business').val());
                    form_data.append('card_id', $('#card_id').val());
                    form_data.append('card_date', $('#card_date').val());
                    form_data.append('card_address', $('#card_address').val());
                    form_data.append('image_card_1', $('#image_card_1')[0].files[0]);
                    form_data.append('image_card_2', $('#image_card_2')[0].files[0]);
                    form_data.append('business_license', $('#business_license')[0].files[0]);
                    form_data.append('related_document_1', $('#related_document_1')[0].files[0]);
                    form_data.append('related_document_2', $('#related_document_2')[0].files[0]);
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: form_data,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status) {
                                $('.popup_confirm_shop').show();
                                $('#overlay1').addClass('open');
                            } else {
                                swal("Thất bại", data.msg, "error");
                            }
                        },
                        error: function () {
                            console.log('error')
                        },
                    });
                }
            }
        });

        $(document).on('click', '.btn_exit_confirm', function () {
            // $('.popup_confirm_shop').hide();
            // $('#overlay1').removeClass('open');
            location.reload();
        });
    })
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
