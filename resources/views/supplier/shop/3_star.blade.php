<div class="table-responsive table-responsive-lg border_table mt-3">
    <table class="table data-list-view table-sm">
        <thead>
        <tr class="bg_table">
            <th></th>
            <th>Sản phẩm</th>
            <th width="40%">Đánh giá của khách</th>
            <th width="20%" class="text-center">Trả lời đánh giá</th>
        </tr>
        </thead>
        <tbody>
        @if(count($data) && $data)
            @foreach($data as $value)
                <tr>
                    <td><input data-id="{{$value->id}}" @if($value['status_reply'] == 1) disabled @endif name="check_rep" class="check_rep" type="radio"></td>
                    <td>
                        <div class="d-flex align-items-center">
                            <img class="border_table mr-1" width="65px" height="63px"
                                 src="{{$value->product_image}}"
                                 alt="">
                            <div class="info_product">
                                <span class="text-bold-700">{{$value->product_name}}</span>
                                <br>
                                <span
                                    class="text-sliver text-bold-700 f-12">ID đơn hàng: {{$value->order_code}}</span>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <img class="mr-2" width="70px" height="auto"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/3sao.png"
                                 alt="">
                            <span class="m-0 f-12">{{date('H:i:s d-m-Y',strtotime($value->created_at))}}</span>
                        </div>
                        <div class="content_review">
                            <p class="text-sliver f-12">{{$value->customer_comment}}</p>
                        </div>
                        <div class="img_review d-flex">
                            <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                 src="{{$value->image_review}}"
                                 alt="">

                        </div>
                    </td>
                    <td class="text-center">
                        @if($value->status_reply == 0)
                            <button data-url="{{url('admin/shop_supplier/review/reply')}}" data-id="{{$value->id}}"
                                    class="btn btn_main btn_reply">Trả lời
                            </button>
                        @else
                            {{$value->reply}}
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr class="text-center text-bold-700">
                <td colspan="100">Không có dữ liệu !</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
