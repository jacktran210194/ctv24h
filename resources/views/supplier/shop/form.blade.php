<?php
$page = 'shop_supplier';
$data_login = Session::get('data_supplier');
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->
<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>
<style>
    .none_img {
        display: none;
    }
</style>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card p-2">
            <h3 class="text-bold-700 mb-2">Hồ Sơ Shop</h3>
            <div class="row">
                <div class="col-lg-5">
                    @if($data->image && $data_image)
                        <div id="carouselExampleControls" class="carousel slide mb-2" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img style="object-fit: contain" width="435px" height="310px" class="d-block w-100"
                                         src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '' ?>">
                                </div>
                                @if(isset($data_image) && count($data_image))
                                    @foreach($data_image as $value)
                                        <div class="carousel-item">
                                            <img style="object-fit: contain" width="435px" height="310px"
                                                 class="d-block w-100"
                                                 src="{{$value['image']}}">
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                               data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                               data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    @else
                        <img width="435px" height="310px" class="d-block w-100 mb-1"
                             src="../../assets/admin/app-assets/images/icon_dashboard/no_image.png">
                    @endif
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <div class="card_shadow text-center"
                             style="height: 180px; width: 29%;position: relative;">
                            <img src="../../assets/admin/app-assets/images/icon_dashboard/lich.png" alt="">
                            <p class="text-sliver f-12 mt-1 text-bold-700">Ngày tham gia</p>
                            <p class="pb-1 text-bold-700 position-absolute position-center" style="top: 90%!important;">
                                <?= isset($data) ? date('d-m-Y', strtotime($data['date'])) : date('d-m-Y') ?></p>
                        </div>
                        <div class="card_shadow text-center"
                             style="height: 180px; width: 29%;position: relative;">
                            <img src="../../assets/admin/app-assets/images/icon_dashboard/time.png" alt="">
                            <p class="text-sliver f-12 mt-1 text-bold-700">Thời gian phản hồi</p>
                            <p class="pb-1 text-bold-700 position-absolute position-center" style="top: 90%!important;">
                                {{isset($data) ? $data['time_feedback'] : ''}}</p>
                        </div>
                        <div class="card_shadow text-center"
                             style="height: 180px; width: 29%;position: relative;">
                            <img src="../../assets/admin/app-assets/images/icon_dashboard/order_fail.png" alt="">
                            <p class="text-sliver f-12 mt-1 text-bold-700">Tỷ lệ đơn hàng không thành công</p>
                            <p class="pb-1 text-bold-700 position-absolute position-center" style="top: 90%!important;">
                                {{isset($data) ? $data['percent_order_false'] : '00,00'}} %</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <div class="card_shadow text-center"
                             style="height: 180px; width: 29%;position: relative;">
                            <a href="{{url('admin/product_supplier')}}">

                                <img src="../../assets/admin/app-assets/images/icon_dashboard/product.png" alt="">
                                <p class="text-sliver f-12 mt-1 text-bold-700">Sản phẩm</p>
                                <p class="pb-1 text-bold-700 position-absolute position-center"
                                   style="top: 90%!important;">
                                    {{isset($count_product) ? $count_product : 0}}</p>
                            </a>
                        </div>
                        <div class="card_shadow text-center"
                             style="height: 180px; width: 29%;position: relative;">
                            <img src="../../assets/admin/app-assets/images/icon_dashboard/chat.png" alt="">
                            <p class="text-sliver f-12 mt-1 text-bold-700">Tỷ lệ phản hồi</p>
                            <p class="pb-1 text-bold-700 position-absolute position-center" style="top: 90%!important;">
                                {{$data['percent_feedback']}} %</p>
                        </div>
                        <div class="card_shadow text-center"
                             style="height: 180px; width: 29%;position: relative;">
                            <a href="{{url('admin/shop_supplier/review')}}">
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/review.png" alt="">
                                <p class="text-sliver f-12 mt-1 text-bold-700">Đánh giá</p>
                                <p class="pb-1 text-bold-700 position-absolute position-center"
                                   style="top: 90%!important;">
                                    {{$data['count_review']}}</p>
                            </a>
                        </div>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">

                        <div class="card_shadow text-center"
                             style="height: 180px; width: 29%;position: relative;">
                            <a href="{{url('admin/shop_supplier/category')}}">
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/category.png" alt="">
                                <p class="text-sliver f-12 mt-1 text-bold-700">Danh mục của shop</p>
                                <p class="pb-1 text-bold-700 position-absolute position-center"
                                   style="top: 90%!important;">{{$data['count_category']}}
                                </p>
                            </a>
                        </div>
                        <div class="card_shadow text-center"
                             style="height: 180px; width: 29%;position: relative;">
                            <img src="../../assets/admin/app-assets/images/icon_dashboard/report.png" alt="">
                            <p class="text-sliver f-12 mt-1 text-bold-700">Báo cáo của shop</p>
                            <p class="pb-1 text-bold-700 position-absolute position-center" style="top: 90%!important;">
                                1
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <label class="text-bold-700">Tên shop</label>
                    <div class="form-label-group position-relative has-icon-left">
                        <input type="text" name="name" id="name" class="form-control text-bold-700"
                               value="{{isset($data) ? $data['name'] : ''}}">
                        <div class="form-control-position" style="padding-left: 9px;">
                            <img width="18px" height="18px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/shop.png" alt="">
                        </div>
                    </div>
                    <label class="text-bold-700">Hình ảnh shop</label>
                    <div class="row">
                        <div class="col-lg-3">
                            <button class="btn btn_image btn-image-one border_gach p-2">
                                <img class="text-center" width="18px" height="18px"
                                     src="../../assets/admin/app-assets/images/icon_dashboard/image.png" alt="">
                                <br>
                                <div class="btn btn-sm border_xam mt-1">
                                    Tải hình ảnh
                                </div>
                            </button>
                            <input hidden class="input_image" type='file' value="" accept="image/*" id="image"
                                   onchange="changeImg_1(this)">
                        </div>
                        <div class="col-lg-3">
                            <button class="btn btn_image border_gach btn-show-img p-2">
                                <img class="text-center" width="18px" height="18px"
                                     src="../../assets/admin/app-assets/images/icon_dashboard/image.png" alt="">
                                <br>
                                <div class="btn btn-sm border_xam mt-1">
                                    Tải hình ảnh
                                </div>
                            </button>
                            <input hidden class="form-control input_list_image" type='file' accept="image/*"
                                   id="imageQr"
                                   multiple/>
                        </div>
                    </div>
                    <div class="row mt-1 image-show">
                        <div class="col-lg-3">
                            @if($data->image)
                                <img width="100px" height="100px" id="avatar" class="thumbnail image-show-qr"
                                     src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '' ?>">
                            @else
                                <img width="100px" height="100px" id="avatar" class="thumbnail image-show-qr none_img">
                            @endif
                        </div>
                        @if (isset($data_image) && count($data_image))
                            @foreach ($data_image as $value)
                                <div class="pip mb-2 col-lg-3" style="position: relative;">
                                    <img class="mr-2" style="width: 100px; height: 100px;"
                                         src="{{$value['image']}}"
                                         title=""><br>
                                    <button type="button" style="position: absolute; top:0;"
                                            class="fa fa-times btn-danger btn btn-sm btn-delete-image"
                                            data-url="{{url('admin/shop/delete_image/' . $value['id'])}}"></button>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <label class="text-bold-700 mt-1">Link youtube</label>
                    <div class="form-label-group position-relative has-icon-left">
                        <input type="text" id="video" class="form-control"
                               placeholder="" value="<?= isset($data) ? $data['video'] : '' ?>">
                        <div class="form-control-position">
                            <i class="fa fa-youtube"></i>
                        </div>
                    </div>
                    <div class="video_shop"></div>
                    <label class="text-bold-700">Mô tả shop</label>
                    <textarea class="form-control" id="desc" cols="30"
                              rows="7">{{isset($data) ? $data['desc'] : ''}}</textarea>
                    <div class="d-flex justify-content-end mt-2">
                        <button class="btn btn_sliver text-bold-700 mr-2" style="width: 120px">Huỷ</button>
                        <button type="button" class="btn btn_main text-bold-700 btn-add-account" style="width: 120px"
                                data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                data-supplier="{{isset($data_login) ? $data_login['id'] : ''}}"
                                data-url="<?= URL::to('admin/shop_supplier/save') ?>"
                        >Lưu
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    function changeImg_1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#avatar').removeClass('none_img');
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function () {
        $(document).on('blur','#video',function () {
            let value = $(this).val();
            value = value.replace('watch', 'embed');
            value = value.replace('?v=', '/');
            // let video = value;
            let html = '<iframe width="560" height="315" src="' + value + '" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
            $('.video_shop').html(html);
        });

        let form_data = new FormData();
        $(document).on("click", ".btn-show-img", function () {
            $('.input_list_image').click();
        });
        $(document).on("click", ".btn-image-one", function () {
            $('.input_image').click();
        });
        $(document).on("click", ".btn-delete-image", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            jQuery.noConflict();
            $('#myModal').modal('show');
        });

        $(document).on("click", ".btn-remove", function () {
            let index = $(".btn-remove").index(this);
            let values = form_data.getAll("img[]");
            values.splice(index, 1);
            form_data.delete("img[]");
            for (let i = 0; i < values.length; i++) {
                form_data.append("img[]", values[i]);
            }
            $(this).parent(".pip").remove();
        });
        $(document).on("change", "#imageQr", function () {
            readURL(this);
        });
        $(document).on("click", ".btn-upload-image-qr", function () {
            $('#imageQr').trigger('click');
        });

        // $(document).on("change", "#video", function () {
        //     let value = $(this).val();
        //     value = value.replace('watch', 'embed');
        //     value = value.replace('?v=', '/');
        //     $("#youtube").attr('src', value);
        // });

        $(document).on("click", ".btn-add-account", function () {
            let url = $(this).attr('data-url');
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('name', $('#name').val());
            form_data.append('video', $('#video').val());
            form_data.append('supplier_id', $(this).attr('data-supplier'));
            form_data.append('desc', $('#desc').val());
            form_data.append('image', $('#image')[0].files[0]);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });

        function readURL(input) {
            if (input.files) {
                let filesAmount = input.files.length;
                for (let i = 0; i < filesAmount; i++) {
                    let reader = new FileReader();

                    reader.onload = function (e) {
                        let file = e.target;
                        let html = "<div class=\"pip mb-2 col-lg-3\" style='position: relative;'>" +
                            "<img style=\"width: 100px; height: 100px;\"  src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                            "<br/><button style='position: absolute;top: 0;' class=\"fa fa-times btn btn-sm btn-danger btn-remove\"></button>" +
                            "</div>";
                        $('.image-show').append(html);
                    }
                    form_data.append('img[]', $('#imageQr')[0].files[i]);
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
