@if(count($data_product) && isset($data_product))
    @foreach($data_product as $value)
        <tr>
            <td>
                <label class="checkbox_config">
                    <input class="btn_click_product" name="product_id[]"
                           data-id="{{$value->id}}" type="checkbox">
                    <span class="checkbox_show"></span>
                </label>
            </td>
            <td>
                <div class="d-flex align-items-center">
                    <img width="71px" height="71px" class="border_radius mr-1 image-preview"
                         src="{{$value->image}}"
                         alt="">
                    <div>
                        <p style="width: 300px!important;"
                           class="f-14 text-bold-700 m-0">{{$value->name}}</p>
                        <div class="mb-2"></div>
                        <p class="text-sliver m-0">Mã: {{$value->sku_type}}</p>
                    </div>
                </div>
            </td>
            <td class="text-left text-bold-700">{{$value->is_sell}}</td>
            <td class="text-red text-bold-700">
                {{number_format($value->price)}} đ
            </td>
            <td class="text-red text-bold-700">
                {{$value->warehouse}}
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="1000">Không có dữ liệu !</td>
    </tr>
@endif
