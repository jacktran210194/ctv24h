<?php
$page = 'category_shop_supplier';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #47AA04;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card p-1">
            <div class="card-content">
                <div class="d-flex align-items-center p-1 position-relative mb-2">
                    <h3 class="text-bold-700">Danh mục Shop</h3>
                    <a href="{{url('admin/shop_supplier/category/add')}}"
                       class="btn btn_main p-1 text-bold-700 position-absolute" style="right: 2%;"><i
                            class="fa fa-plus"></i> Thêm danh mục</a>
                </div>
                <!-- content -->
                <div class="table-responsive table-responsive-lg border_table">
                    <table class="table data-list-view table-sm">
                        <thead class="bg_table">
                        <tr>
                            <th>Tên danh mục</th>
                            <th class="text-center">Tạo bởi</th>
                            <th class="text-center">Số lượng sản phẩm</th>
                            <th class="text-center">Hiển thị bật/tắt</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data))
                            @foreach($data as $value)
                                <tr>
                                    <td>{{$value->name}}</td>
                                    <td class="text-center">
                                        {{$value->user_created}}
                                    </td>
                                    <td class="text-center">
                                        {{$value->count_product}}
                                    </td>
                                    <td class="text-center">
                                        <label class="switch ml-3">
                                            <input
                                                class="btn_check_status" @if($value->status == 1) checked @endif type="checkbox"
                                                data-id="{{$value->id}}"
                                                data-url="{{url('admin/shop_supplier/category/display')}}"
                                            >
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{url('admin/shop_supplier/category/detail/'.$value->id)}}" class="text-blue text-underline text-bold-700">Thêm sản phẩm</a>
                                        <br>
                                        <a href="{{url('admin/shop_supplier/category/detail/'.$value->id)}}" class="text-blue text-underline text-bold-700">Chi tiết</a>
                                        <br>
                                        <a data-url="{{url('admin/shop_supplier/category/delete/'.$value->id)}}"
                                              class="text-blue text-underline text-bold-700 btn-delete">Xoá</a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">Không có dữ liệu</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            jQuery.noConflict();
            $('#myModal').modal('show');
        });

        $(document).on('click', '.btn_check_status', function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            let check = $(this).is(':checked') ? 1 : 0;
            form_data.append('check', check);
            form_data.append('id', $(this).attr('data-id'));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        alert('Lỗi');
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        })
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
