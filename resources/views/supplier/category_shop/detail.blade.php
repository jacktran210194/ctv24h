<?php
$page = 'category_shop_supplier';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #47AA04;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body d-flex align-items-center justify-content-between">
                <div class="title_info">
                    <h3 class="text-bold-700 f-24">{{isset($data) ? $data->name : ''}}</h3>
                    <p>Tạo bởi: người bán | {{$count_product}} sản phẩm</p>
                </div>
                <div>
                    <label class="switch">
                        <input
                            class="btn_check_status" @if($data->status == 1) checked @endif type="checkbox"
                            data-id="{{$data->id}}"
                            data-url="{{url('admin/shop_supplier/category/display')}}"
                        >
                        <span class="slider round"></span>
                    </label>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="form-label-group position-relative has-icon-right">
                        <input type="text" id="search" class="form-control" name="search"
                               placeholder="Tìm kiếm..." data-url="{{url('admin/shop_supplier/category/search_product_category')}}">
                        {{--                        <div class="form-control-position">--}}
                        {{--                            <img src="../../assets/admin/app-assets/images/icon_dashboard/search.png" alt="">--}}
                        {{--                        </div>--}}
                    </div>
                    <button class="btn btn_main text-bold-700 text-white btn_add_product">
                        <img src="../../assets/admin/app-assets/images/icon_dashboard/icon_add.png" alt="">
                        Thêm sản phẩm
                    </button>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h3 class="text-bold-700 f-24 mb-2">Danh sách sản phẩm</h3>
                <div class="table-responsive table-responsive-lg border_table">
                    <table class="table data-list-view table-sm">
                        <thead>
                        <tr class="bg_table">
                            <th><input type="checkbox"></th>
                            <th>Tên sản phẩm</th>
                            <th>Giá</th>
                            <th>Kho hàng</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>
                        <tbody id="get">
                        @if(count($data_product_category_shop) && isset($data_product_category_shop))
                            @foreach($data_product_category_shop as $value)
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <img width="71px" height="71px" class="border_radius mr-1 image-preview"
                                                 src="{{$value->product_image}}"
                                                 alt="">
                                            <div class="content_product">
                                                <p style="width: 200px!important;" class="f-14 text-bold-700 m-0">{{$value->product_name}}</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-bold-700 text-red">{{number_format($value->product_price)}}</td>
                                    <td class="text-bold-700 text-red">{{$value->product_warehouse}}</td>
                                    <td class="text-center">
                                        <a data-url="{{url('admin/shop_supplier/category/delete_product/'.$value->id)}}" class="text-bold-700 text-underline text-blue btn-delete">
                                            Xoá
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="10000">Không có dữ liệu !</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="position-fixed position-center popup_add_product p-4 w-100 h-100"
     style="display: none; overflow: auto; z-index: 600000; background: #33333330">
    <div class="card" style="max-width: 80%; margin: 0 auto">
        <div class="card-body">
            <h3 class="text-bold-700 mb-2">Chọn sản phẩm</h3>
            <div class="tab-bar-page">
                <ul class="tab-bar-content d-flex align-items-center justify-content-lg-start flex-wrap">
                    <li class="d-flex align-items-center justify-content-lg-center active product_pick">
                        <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Chọn</a>
                    </li>
                    <li class="d-flex align-items-center justify-content-lg-center product_import_ajax">
                        <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đăng
                            danh sách sản phẩm</a>
                    </li>
                </ul>
            </div>
            <!-- Chọn sản phẩm -->
            <div class="get_data_ajax">
                <div class="list_product">
                    <div class="mt-2 d-flex align-items-center">
                        <div class="form-group filter_category d-flex align-items-center">
                            <label style="width: 200px!important;" class="text-bold-700">Ngành hàng</label>
                            <select class="form-control" id="">
                                <option value="all">Tất cả</option>
                            </select>
                        </div>
                        <div class="form-group form_search d-flex align-items-center position-absolute"
                             style="right: 2%;">
                            <label style="width: 150px!important;" class="text-bold-700">Tìm kiếm</label>
                            <select style="width: auto!important;" class="form-control" id="">
                                <option value="">Tên sản phẩm</option>
                            </select>
                            <input type="text" name="key_search" class="form-control">
                        </div>
                    </div>
                    <div class="float-right">
                        <input type="checkbox">
                        <span class="text-bold-700">Xem sản phẩm có sẵn</span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="mt-1 table-responsive table-responsive-lg border_table">
                        <table class="table data-list-view table-sm">
                            <thead>
                            <tr class="bg_table">
                                <th><input type="checkbox"></th>
                                <th>Sản phẩm</th>
                                <th>Đã bán</th>
                                <th>Giá</th>
                                <th>Kho hàng</th>
                            </tr>
                            </thead>
                            <tbody id="get" class="list-product">
                            @include('supplier.category_shop.data_product')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Đăng danh sách sản phẩm -->
        </div>
        <div class="d-flex justify-content-end" style="padding: 10px 20px">
            <button class="btn border bg-white btn_exit_show_product text-bold-700 mr-1"
                    style="width: 150px!important; height: 40px!important;">Huỷ
            </button>
            <button data-shop="{{$data->shop_id}}" data-category="{{$data->id}}"
                    data-url="{{url('admin/shop_supplier/category/add_product')}}"
                    class="btn btn_main btn_save_product text-bold-700"
                    style="width: 150px!important; height: 40px!important;">Lưu
            </button>
        </div>
    </div>

</div>

<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('click', '.btn_add_product', function () {
            $('.popup_add_product').show();
        });

        $(document).on('click', '.btn_exit_show_product', function () {
            $('.popup_add_product').hide();
        });

        $(document).on('click', '.btn_click_product', function () {
            let product_id_click = [];
            $("[name='product_id[]']").each(function () {
                if ($(this).is(":checked")) {
                    product_id_click.push($(this).attr('data-id'));
                }
            });
            $('.btn_save_product').attr('data-product', product_id_click);
        });

        $(document).on('click', '.btn_save_product', function () {
            let list_product = $(this).attr('data-product');
            let shop_id = $(this).attr('data-shop');
            let category_shop = $(this).attr('data-category');
            let url = $(this).attr('data-url');
            if (list_product == undefined) {
                alert('Vui lòng chọn ít nhất 1 sản phẩm !');
            } else {
                let form_data = new FormData();
                form_data.append('product_id', JSON.stringify(list_product));
                form_data.append('category_id', category_shop);
                form_data.append('shop_id', shop_id);

                $.ajax({
                    url: url,
                    type: "POST",
                    data: form_data,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.status) {
                            window.location.reload();
                        } else {
                            alert(data.msg);
                        }
                    },
                    error: function (e) {
                        console.log(e)
                    },
                });
            }
        });

        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            jQuery.noConflict();
            $('#myModal').modal('show');
        });

        $(document).on('click', '.btn_check_status', function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            let check = $(this).is(':checked') ? 1 : 0;
            form_data.append('check', check);
            form_data.append('id', $(this).attr('data-id'));
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        window.location.reload();
                    } else {
                        swal("Thông báo", "Thất bại", "error");
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });

        function fetch_customer_data(query = '') {
            let url = $('#search').attr('data-url');
            $.ajax({
                url: url,
                method: 'GET',
                data: {query: query},
                dataType: 'json',
                success: function (data) {
                    $('tbody').html(data.table_data);
                }
            })
        }

        $(document).on('keyup', '#search', function () {
            let query = $(this).val();
            fetch_customer_data(query);
        });
        $('input[name="key_search"]').keyup(function () {
            let value = $(this).val();
            let data_2 = {};
            data_2['key_word'] = value;
            $.ajax({
                url: window.location.origin + '/admin/shop_supplier/category/filter_product',
                method: 'POST',
                data: data_2,
                dataType: 'json',
                success: function (data) {
                    $('.list-product').html(data.prod);
                }
            })
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
