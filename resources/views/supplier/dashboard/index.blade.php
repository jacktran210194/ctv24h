<?php
$page = 'home';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>
<style>
    .quantity_item {
        color: #005DB6;
        font-weight: 800;
        font-size: 24px;
        line-height: 33px;
    }

    .content_work {
        box-shadow: 0px 16px 24px rgba(0, 0, 0, 0.06), 0px 2px 6px rgba(0, 0, 0, 0.04), 0px 0px 1px rgba(0, 0, 0, 0.04);
        border-radius: 20px;
        width: 268px;
        height: 90px;
    }

    .border_tab {
        box-shadow: 0px 16px 24px rgba(0, 0, 0, 0.06), 0px 2px 6px rgba(0, 0, 0, 0.04), 0px 0px 1px rgba(0, 0, 0, 0.04);
        border-radius: 20px;
    }
</style>
<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <h3>Những việc cần làm</h3>
        <div class="list_work">
            <div class="d-flex align-items-center justify-content-start">
                <a href="{{url('admin/order/wait_confirm')}}">
                    <div class="bg-white d-flex align-items-center p-2 mr-1 content_work">
                        <div class="icon_work">
                            <img width="42px" height="42px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/choxacnhan.png" alt="">
                        </div>
                        <div class="ml-1">
                            <h4>Chờ xác nhận</h4>
                            <p class="quantity_item">{{$order_waiting_confirm}}</p>
                        </div>
                    </div>
                </a>
                <a href="{{url('admin/order/wait_shipping')}}">
                    <div class="bg-white d-flex align-items-center p-2 mr-1 content_work">
                        <div class="icon_work">
                            <img width="42px" height="42px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/cholayhang.png" alt="">
                        </div>
                        <div class="ml-1">
                            <h4>Chờ lấy hàng</h4>
                            <p class="quantity_item">{{$order_waiting_shipping}}</p>
                        </div>
                    </div>
                </a>
                <a href="{{url('admin/order/shipped')}}">
                    <div class="bg-white d-flex align-items-center p-2 mr-1 content_work">
                        <div class="icon_work">
                            <img width="42px" height="42px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/daxuly.png" alt="">
                        </div>
                        <div class="ml-1">
                            <h4>Đã xử lý</h4>
                            <p class="quantity_item">{{$order_shipped}}</p>
                        </div>
                    </div>
                </a>
                <a href="{{url('admin/order/cancel')}}">
                    <div class="bg-white d-flex align-items-center p-2 mr-1 content_work">
                        <div class="icon_work">
                            <img width="42px" height="42px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/dahuy.png" alt="">
                        </div>
                        <div class="ml-1">
                            <h4>Đã huỷ</h4>
                            <p class="quantity_item">{{$order_cancel}}</p>
                        </div>
                    </div>
                </a>
            </div>

            <div class="d-flex align-items-center justify-content-start mt-1">
                <a class="popup_default">
                    <div class="bg-white d-flex align-items-center p-2 mr-1 content_work">
                        <div class="icon_work">
                            <img width="42px" height="42px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/trahang.png" alt="">
                        </div>
                        <div class="ml-1">
                            <h4>Trả hàng/Hoàn tiền</h4>
                            <p class="quantity_item">{{$order_return_money}}</p>
                        </div>
                    </div>
                </a>
                <a href="{{url('admin/product_supplier')}}">
                    <div class="bg-white d-flex align-items-center p-2 mr-1 content_work">
                        <div class="icon_work">
                            <img width="42px" height="42px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/hethang.png" alt="">
                        </div>
                        <div class="ml-1">
                            <h4>Hết hàng</h4>
                            <p class="quantity_item">{{$product_sold_out}}</p>
                        </div>
                    </div>
                </a>
                <a href="{{url('admin/product_supplier')}}">
                    <div class="bg-white d-flex align-items-center p-2 mr-1 content_work">
                        <div class="icon_work">
                            <img width="42px" height="42px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/tamkhoa.png" alt="">
                        </div>
                        <div class="ml-1">
                            <h4>Đã tạm khoá</h4>
                            <p class="quantity_item">{{$product_lock}}</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-6 analysis_sale bg-white p-1">
                <div class="d-flex">
                    <h3 style="width: 100%">Phân tích bán hàng</h3>
                    <input type="datetime-local" class="form-control">
                </div>
                <div class="container">
                    <canvas id="myChart"></canvas>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="d-flex mb-2">
                    <h3 class="w-100">Các chỉ số phân tích</h3>
                    <a href="{{url('admin/data_sale')}}" class="w-100 text-blue text-underline text-bold-700">Xem
                        thêm</a>
                </div>
                <div class="tab_data d-flex align-items-center">
                    <div style="height: 250px!important; width: 150px!important;"
                         class="border_tab content_tab bg-white p-1 mr-1">
                        <h6 style="height: 20%!important;" class="text-bold-700">Lượt truy cập</h6>
                        <p>{{date('d/m/Y')}}</p>
                        <p class="quantity_item">{{$data_product_access['count_access'] ?? 0}}</p>
                        <div class="d-flex justify-content-end">
                            @if($data_product_access['tang_truong_access'] == 1)
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/tang_truong.png" alt="">
                            @elseif($data_product_access['tang_truong_access'] == -1)
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/tang_truong_am.png"
                                     alt="">
                            @elseif($data_product_access['tang_truong_access'] == 0)
                            @endif
                        </div>
                        <p class="mt-1 f-12 text-bold-700 text-sliver">Tăng
                            trưởng: {{$data_product_access['percent_access'] ?? 0}}%</p>
                    </div>
                    <div style="height: 250px!important; width: 150px!important;"
                         class="border_tab content_tab bg-white p-1 mr-1">
                        <h6 style="height: 20%!important;" class="text-bold-700">Lượt xem</h6>
                        <p>{{date('d/m/Y')}}</p>
                        <p class="quantity_item">{{round($data_product_access['count_access']*1.63,0) ?? 0}}</p>
                        <div class="d-flex justify-content-end">
                            @if($data_product_access['tang_truong_access'] == 1)
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/tang_truong.png" alt="">
                            @elseif($data_product_access['tang_truong_access'] == -1)
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/tang_truong_am.png"
                                     alt="">
                            @elseif($data_product_access['tang_truong_access'] == 0)
                            @endif
                        </div>
                        <p class="mt-1 f-12 text-bold-700 text-sliver">Tăng
                            trưởng: {{$data_product_access['percent_access'] ?? 0}}%</p>
                    </div>
                    <div style="height: 250px!important; width: 150px!important;"
                         class="border_tab content_tab bg-white p-1 mr-1">
                        <h6 style="height: 20%!important;" class="text-bold-700">Đơn hàng</h6>
                        <p>{{date('d/m/Y')}}</p>
                        <p class="quantity_item">{{$data_count_order['count_order_today'] ?? 0}}</p>
                        <div class="d-flex justify-content-end">
                            @if($data_count_order['tang_truong_order'] == 1)
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/tang_truong.png" alt="">
                            @elseif($data_count_order['tang_truong_order'] == -1)
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/tang_truong_am.png"
                                     alt="">
                            @else
                            @endif

                        </div>
                        <p class="mt-1 f-12 text-bold-700 text-sliver">Tăng
                            trưởng: {{$data_count_order['order_rate_percent'] ?? 0}}%</p>
                    </div>
                    <div style="height: 250px!important; width: 150px!important;    "
                         class="border_tab content_tab bg-white p-1 mr-1">
                        <h6 style="height: 20%!important;" class="text-bold-700">Tỷ lệ chuyển đổi</h6>
                        <p>{{date('d/m/Y')}}</p>
                        <p class="quantity_item">{{$data_rate_convert['rate_convert_today'] ?? 0}}%</p>
                        <div class="d-flex justify-content-end">
                            @if($data_rate_convert['tang_truong_convert'] == 1)
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/tang_truong.png" alt="">
                            @elseif($data_rate_convert['tang_truong_convert'] == -1)
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/tang_truong_am.png"
                                     alt="">
                            @else
                            @endif
                        </div>
                        <p class="mt-1 f-12 text-bold-700 text-sliver">Tăng
                            trưởng: {{$data_rate_convert['percent_convert']}}%</p>
                    </div>

                </div>
            </div>
        </div>
        <!--  chương trình nổi bật-->
        <div class="featured-program">
            <h2 class="title-featured font-size-24 font-weight-bold">Chương trình nổi bật</h2>
            <div class="d-flex justify-content-sm-between">
                <div class="w-50 p-1">
                    <div class="border_box padding-bottom-40">
                        <div class="header-box border_bottom">
                            <p class="date font-weight-bold font-size-14">Thứ 2, 24 tháng 11</p>
                            <div class="date-time slide-date-time d-flex ">
                                <div class="day d-flex justify-content-lg-center align-items-center w-100">
                                    <div class="sub-day">
                                        <p class="font-size-15 font-weight-bold">Sun</p>
                                        <p class="font-size-15 font-weight-bold">23</p>
                                    </div>
                                    <div class="sub-day">
                                        <p class="font-size-15 font-weight-bold">Mon</p>
                                        <p class="font-size-15 font-weight-bold">24</p>
                                    </div>
                                    <div class="sub-day">
                                        <p class="font-size-15 font-weight-bold">Tue</p>
                                        <p class="font-size-15 font-weight-bold">25</p>
                                    </div>
                                    <div class="sub-day">
                                        <p class="font-size-15 font-weight-bold">Wed</p>
                                        <p class="font-size-15 font-weight-bold">26</p>
                                    </div>
                                    <div class="sub-day">
                                        <p class="font-size-15 font-weight-bold">Thu</p>
                                        <p class="font-size-15 font-weight-bold">27</p>
                                    </div>
                                    <div class="sub-day">
                                        <p class="font-size-15 font-weight-bold">Fri</p>
                                        <p class="font-size-15 font-weight-bold">28</p>
                                    </div>
                                    <div class="sub-day">
                                        <p class="font-size-15 font-weight-bold">Sat</p>
                                        <p class="font-size-15 font-weight-bold">29</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-box">
                            <div class="content-1 border_box d-flex align-items-center">
                                <div class="day-time">
                                    <p class="font-weight-bold font-size-14">24</p>
                                    <p class="font-weight-bold font-size-11">tháng 11</p>
                                </div>
                                <div class="sale-of-day">
                                    <p class="font-weight-bold font-size-14">5/5 Sale Chào Hè</p>
                                    <div class="content-sale d-flex align-items-center">
                                        <div class="text-content">
                                            <p class="font-size-13 color-gray">Thời gian hoạt động: 05/05/2021 -
                                                05/05/2021</p>
                                            <p class="font-size-13 color-gray">Gì cũng rẻ, giá hạt rẻ cùng ngàn Deal
                                                hot</p>
                                        </div>
                                        <div class="button">
                                            <button class="popup_default see-more color-white font-weight-bold">Chi
                                                tiết
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content-1 border_box d-flex align-items-center">
                                <div class="day-time">
                                    <p class="font-weight-bold font-size-14">24</p>
                                    <p class="font-weight-bold font-size-11">tháng 11</p>
                                </div>
                                <div class="sale-of-day">
                                    <p class="font-weight-bold font-size-14">5/5 Sale Chào Hè</p>
                                    <div class="content-sale d-flex align-items-center">
                                        <div class="text-content">
                                            <p class="font-size-13 color-gray">Thời gian hoạt động: 05/05/2021 -
                                                05/05/2021</p>
                                            <p class="font-size-13 color-gray">Gì cũng rẻ, giá hạt rẻ cùng ngàn Deal
                                                hot</p>
                                        </div>
                                        <div class="button">
                                            <button class="popup_default see-more color-white font-weight-bold">Chi
                                                tiết
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content-1 border_box d-flex align-items-center">
                                <div class="day-time">
                                    <p class="font-weight-bold font-size-14">24</p>
                                    <p class="font-weight-bold font-size-11">tháng 11</p>
                                </div>
                                <div class="sale-of-day">
                                    <p class="font-weight-bold font-size-14">5/5 Sale Chào Hè</p>
                                    <div class="content-sale d-flex align-items-center">
                                        <div class="text-content">
                                            <p class="font-size-13 color-gray">Thời gian hoạt động: 05/05/2021 -
                                                05/05/2021</p>
                                            <p class="font-size-13 color-gray">Gì cũng rẻ, giá hạt rẻ cùng ngàn Deal
                                                hot</p>
                                        </div>
                                        <div class="button">
                                            <button class="popup_default see-more color-white font-weight-bold">Chi
                                                tiết
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-50 p-1">
                    <div class="border_box content_featured">
                        <p class="title_box font-weight-bold font-size-24 border_bottom color-blue">Chương trình giảm
                            giá của sản phẩm </p>
                        <div class="sale-product">
                            <p class="font-size-15 color-323C47 text-uppercase">THỜI TRANG NỮ VÀ TRẺ EM - DƯỚI 10K</p>
                            <div class="d-flex align-items-center justify-content-sm-between">
                                <div>
                                    <p class="font-size-13 color-gray">Thời gian diễn ra: 00:00 05/05/2021 - 23:59
                                        05/05/2021</p>
                                    <p class="font-size-13 color-gray">Thời hạn còn đăng ký: 4 ngày 20 giờ</p>
                                </div>
                                <div class="button">
                                    <button class="popup_default see-more color-white font-weight-bold">Đăng ký ngay
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="sale-product">
                            <p class="font-size-15 color-323C47 text-uppercase">THỜI TRANG NỮ VÀ TRẺ EM - DƯỚI 10K</p>
                            <div class="d-flex align-items-center justify-content-sm-between">
                                <div>
                                    <p class="font-size-13 color-gray">Thời gian diễn ra: 00:00 05/05/2021 - 23:59
                                        05/05/2021</p>
                                    <p class="font-size-13 color-gray">Thời hạn còn đăng ký: 4 ngày 20 giờ</p>
                                </div>
                                <div class="button">
                                    <button class="popup_default see-more color-white font-weight-bold">Đăng ký ngay
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="border_box content_featured">
                        <p class="title_box font-weight-bold font-size-24 border_bottom color-blue">Chương trình mã giảm
                            giá</p>
                        <div class="sale-product">
                            <p class="font-size-15 color-323C47 text-uppercase">THỜI TRANG NỮ VÀ TRẺ EM - DƯỚI 10K</p>
                            <div class="d-flex align-items-center justify-content-sm-between">
                                <div>
                                    <p class="font-size-13 color-gray">Thời gian diễn ra: 00:00 05/05/2021 - 23:59
                                        05/05/2021</p>
                                    <p class="font-size-13 color-gray">Thời hạn còn đăng ký: 4 ngày 20 giờ</p>
                                </div>
                                <div class="button">
                                    <button class="popup_default see-more color-white font-weight-bold">Đăng ký ngay
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="sale-product">
                            <p class="font-size-15 color-323C47 text-uppercase">THỜI TRANG NỮ VÀ TRẺ EM - DƯỚI 10K</p>
                            <div class="d-flex align-items-center justify-content-sm-between">
                                <div>
                                    <p class="font-size-13 color-gray">Thời gian diễn ra: 00:00 05/05/2021 - 23:59
                                        05/05/2021</p>
                                    <p class="font-size-13 color-gray">Thời hạn còn đăng ký: 4 ngày 20 giờ</p>
                                </div>
                                <div class="button">
                                    <button class="popup_default see-more color-white font-weight-bold">Đăng ký ngay
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: chương trình nổi bật-->

        <!--  Công cụ marketing -->
        <div id="marketing" class="border_box bg-white mg-top-30">
            <h2 class="title-featured font-size-24 font-weight-bold">Công cụ marketing </h2>
            <div class="d-flex align-items-center justify-content-sm-between w-100 flex-wrap">
                <div class="content-marketing border_box border-radius-16 position-relative">
                    <a style="color: #626262" href="{{url('admin/marketing_supplier/my_voucher')}}">
                        <div
                            class="d-flex align-items-center justify-content-lg-center position-absolute icon-marketing">
                            <img src="../../../../assets/admin/app-assets/images/icons/code_voucher.png">
                        </div>
                        <p class="font-size-14 font-weight-bold text-overflow">Mã giảm giá của tôi</p>
                        <p class="font-size-12 color-gray">Lorem Ipsum is simply dummy text of the printing and
                            typesetting
                            industry.</p>
                    </a>
                </div>
                <div class="content-marketing border_box border-radius-16 position-relative">
                    <a style="color: #626262" href="{{url('admin/marketing_supplier/promotion')}}">
                        <div
                            class="d-flex align-items-center justify-content-lg-center position-absolute icon-marketing">
                            <img src="../../../../assets/admin/app-assets/images/icons/discount.png">
                        </div>
                        <p class="font-size-14 font-weight-bold text-overflow">Chương trình giảm giá của tôi</p>
                        <p class="font-size-12 color-gray">Lorem Ipsum is simply dummy text of the printing and
                            typesetting
                            industry.</p>
                    </a>
                </div>
                <div class="content-marketing border_box border-radius-16 position-relative">
                    <a style="color: #626262" href="{{url('admin/marketing_supplier/promotion_combo')}}">
                        <div
                            class="d-flex align-items-center justify-content-lg-center position-absolute icon-marketing">
                            <img src="../../../../assets/admin/app-assets/images/icons/combo_sale.png">
                        </div>
                        <p class="font-size-14 font-weight-bold text-overflow">Combo khuyến mại</p>
                        <p class="font-size-12 color-gray">Lorem Ipsum is simply dummy text of the printing and
                            typesetting
                            industry.</p>
                    </a>
                </div>
                <div class="content-marketing border_box border-radius-16 position-relative">
                    <a style="color: #626262" href="{{url('admin/marketing_supplier/flash_sale')}}">
                        <div
                            class="d-flex align-items-center justify-content-lg-center position-absolute icon-marketing">
                            <img src="../../../../assets/admin/app-assets/images/icons/flash_sale.png">
                        </div>
                        <p class="font-size-14 font-weight-bold text-overflow">Flash sale</p>
                        <p class="font-size-12 color-gray">Lorem Ipsum is simply dummy text of the printing and
                            typesetting
                            industry.</p>
                    </a>
                </div>
                <div class="content-marketing border_box border-radius-16 position-relative">
                    <a style="color: #626262" href="{{url('admin/marketing_supplier/offer_follower')}}">
                        <div
                            class="d-flex align-items-center justify-content-lg-center position-absolute icon-marketing">
                            <img src="../../../../assets/admin/app-assets/images/icons/offers_follow.png">
                        </div>
                        <p class="font-size-14 font-weight-bold text-overflow">Ưu đãi follower</p>
                        <p class="font-size-12 color-gray">Lorem Ipsum is simply dummy text of the printing and
                            typesetting
                            industry.</p>
                    </a>
                </div>
            </div>
        </div>
        <!-- END: Công cụ marketing -->

        <!-- Hiệu quả hoạt động -->
        <div id="operational-efficiency" class="border_box bg-white mg-top-30">
            <h2 class="title-featured font-size-24 font-weight-bold">Hiệu quả hoạt động</h2>
            <p class="font-size-15 color-gray">Bảng Hiệu Quả Hoạt Động giúp Người Bán hiểu rõ hơn về hoạt động buôn bán
                của Shop mình dựa trên những chỉ tiêu sau:</p>
            <div class="table-efficiency d-flex flex-wrap">
                <ul class="border_box border-radius-16">
                    <li class="font-size-14 font-weight-bold text-overflow">Các sản phẩm vi phạm</li>
                    <li class="font-size-14 font-weight-bold text-overflow active">Quản lý đơn hàng</li>
                    <li class="font-size-14 font-weight-bold text-overflow">Chăm sóc khách hàng</li>
                    <li class="font-size-14 font-weight-bold text-overflow">Mức độ hài lòng của người mua</li>
                </ul>
                <table class="border_box border_radius">
                    <tr class="bg-red color-white font-weight-bold font-size-17 border-radius-top">
                        <th class="w-40 text-left border-top-left">Tiêu chí</th>
                        <th class="w-20">Shop của tôi</th>
                        <th class="w-40 border-top-right">Chỉ tiêu</th>
                    </tr>
                    <tr class="font-weight-bold font-size-17 border_bottom">
                        <td class="w-40 text-left">Tỷ lệ đơn hàng không thành công</td>
                        <td class="w-20 text-center">-</td>
                        <td class="w-40"> >10</td>
                    </tr>
                    <tr class="font-weight-bold font-size-17 border_bottom">
                        <td class="w-40 text-left">Tỷ lệ huỷ đơn hàng</td>
                        <td class="w-20 text-center">-</td>
                        <td class="w-40"> >5</td>
                    </tr>
                    <tr class="font-weight-bold font-size-17 border_bottom">
                        <td class="w-40 text-left">Tỷ lệ trả hàng/hoàn tiền</td>
                        <td class="w-20 text-center">-</td>
                        <td class="w-40"> >5</td>
                    </tr>
                    <tr class="font-weight-bold font-size-17">
                        <td class="w-40 text-left">Tỷ lệ giao hàng trễ</td>
                        <td class="w-20 text-center">-</td>
                        <td class="w-40"> >10</td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- END: Hiệu quả hoạt động -->

        <!--  Chat -->
        <div class="chat-footer d-flex justify-content-lg-end w-100">
            <button
                class="popup_default border-radius-8 out-line-none bg-red font-weight-bold color-white d-flex align-items-center"
                style="margin-right: 20px">
                <img src="../../assets/admin/app-assets/images/icons/live_chat.png">
                <a class="color-white" style="margin-left: 10px">Chat với CTV24</a>
            </button>
            <button
                class="popup_default border_box border-radius-8 out-line-none font-weight-bold color-gray d-flex align-items-center">
                <img src="../../assets/admin/app-assets/images/icons/chat-bubbles-with-ellipsis.png">
                <a class="color-white" style="margin-left: 10px">Chat với khách hàng</a>
            </button>
        </div>
        <!-- END: Chat -->

    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    let myChart = document.getElementById('myChart').getContext('2d');
    // Global Options
    Chart.defaults.global.defaultFontFamily = 'Lato';
    Chart.defaults.global.defaultFontSize = 18;
    Chart.defaults.global.defaultFontColor = '#777';

    let massPopChart = new Chart(myChart, {
        type: 'line',
        data: {
            labels: ['00:00', '06:00', '12:00', '18:00', '24:00',],
            datasets: [{
                label: '',
                data: [
                    5,
                    15,
                    20,
                    25,
                    10
                ],
                //backgroundColor:'green',
                backgroundColor: [
                    'rgba(255, 206, 86, 0.6)',
                    'rgba(75, 192, 192, 0.6)',
                    'rgba(153, 102, 255, 0.6)',
                    'rgba(255, 159, 64, 0.6)',
                    'rgba(255, 99, 132, 0.6)'
                ],
                borderWidth: 1,
                borderColor: '#007AFF',
                hoverBorderWidth: 3,
                hoverBorderColor: '#000'
            }]
        },
        options: {
            legend: {
                display: true,
                position: 'right',
                labels: {
                    fontColor: '#000'
                }
            },
            layout: {
                padding: {
                    left: 50,
                    right: 0,
                    bottom: 0,
                    top: 0
                }
            },
            tooltips: {
                enabled: true
            }
        }
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
