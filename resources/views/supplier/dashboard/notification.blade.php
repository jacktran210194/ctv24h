<?php
$page = 'home';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>
<style>
</style>
<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h2 class="text-bold-700 text-red mb-5">Vui lòng cập nhật đầy đủ thông tin để có thể đăng bán sản phẩm
                    !</h2>

                <div class="row mb-3">
                    <div class="col-lg-9">
                        <h3 class="f-24 text-blue text-bold-700">Cập nhật shop</h3>
                        <p class="text-sliver">Cập nhật đầy đủ thông tin của shop như tên doanh nghiệp, ngành nghề kinh
                            doanh, các giấy tờ liên quan</p>
                    </div>
                    <div class="col-lg-3">
                        <a target="_blank" href="{{url('admin/shop_supplier/update_info')}}">
                            <button style="width: 250px!important; height: 50px!important;"
                                    class="btn btn_main f-20 text-bold-700">Cập nhật
                            </button>
                        </a>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-9">
                        <h3 class="f-24 text-blue text-bold-700">Cài đặt đơn vị vận chuyển</h3>
                        <p class="text-sliver">Cài đặt các đơn vị vận chuyển cho shop của bạn! </p>
                    </div>
                    <div class="col-lg-3">
                        <a target="_blank" href="{{url('admin/shipping_supplier')}}">
                            <button style="width: 250px!important; height: 50px!important;"
                                    class="btn btn_main f-20 text-bold-700">Cài đặt
                            </button>
                        </a>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-9">
                        <h3 class="f-24 text-blue text-bold-700">Cài đặt địa chỉ lấy hàng</h3>
                        <p class="text-sliver">Cài đặt địa chỉ lấy hàng của bạn!</p>
                    </div>
                    <div class="col-lg-3">
                        <a target="_blank" href="{{url('admin/profile/address')}}">
                            <button style="width: 250px!important; height: 50px!important;"
                                    class="btn btn_main f-20 text-bold-700">Cài đặt
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
