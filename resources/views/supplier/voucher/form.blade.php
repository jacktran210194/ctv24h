<?php
$page = 'voucher';
use Illuminate\Support\Facades\File;use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label class="lb">Tiêu đề</label>
                                <input type="text" id="title" placeholder="Tiêu đề"
                                       value="<?= isset($data) ? $data['title'] : ''?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Nội dung</label>
                                <input type="text" id="content" placeholder="Nội dung"
                                       value="<?= isset($data) ? $data['content'] : ''?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Mã giảm giá</label>
                                <input type="text" id="code" placeholder="Mã giảm giá"
                                       value="<?= isset($data) ? $data['code'] : '' ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Thời gian bắt đầu</label>
                                <input type="datetime-local" id="time_start" placeholder="Thời gian bắt đầu"
                                       value="<?= isset($data) ? date('Y-m-d\TH:i:s', strtotime($data['time_start'])) : date('Y-m-d\TH:i:s') ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Thời gian kết thúc</label>
                                <input type="datetime-local" id="time_end" placeholder="Thời gian kết thúc"
                                       value="<?= isset($data) ? date('Y-m-d\TH:i:s', strtotime($data['time_end'])) : date('Y-m-d\TH:i:s') ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label class="lb">Số lượng mã giảm giá</label>
                                <input type="number" id="number" placeholder="Số lượng mã giảm giá"
                                       value="<?= isset($data)? $data['number'] : '' ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="first-name-floating-icon">Danh mục</label>
                                <select class="form-control" name="category_id" id="category_id">
                                    <?php foreach ($category as $value) :?>
                                        <option @isset($data) @if($value->id == $data->category_id) selected @endif @endisset value="{{$value->id}}">{{$value->name}}</option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
{{--                            <div class="form-group">--}}
{{--                                <label for="first-name-floating-icon">Trạng thái</label>--}}
{{--                                <select class="form-control" name="active" id="active">--}}
{{--                                    <option @if($data->active == 1) selected @endif value="1">Sắp diễn ra</option>--}}
{{--                                    <option @if($data->active == 0) selected @endif value="0">Đã diễn ra</option>--}}
{{--                                    <option @if($data->active == 2) selected @endif value="2">Đang diễn ra</option>--}}
{{--                                </select>--}}
{{--                                <div class="chip chip-{{$value->color}}">--}}
{{--                                    <div class="chip-body">--}}
{{--                                        <div class="chip-text">{{$value->status}}</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <div class="form-group">
                                <label>Ảnh</label>
                                <div class="form-label-group">
                                    <input type='file' value="" accept="image/*" name="image" id="image" onchange="changeImg(this)">
                                    <br>
                                    <img width="120px" height="120px" id="avatar" class="thumbnail image-show-qr"
                                         src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/images/avt.jpg' ?>">
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/my_voucher/save') ?>"><i class="fa fa-floppy-o"
                                                                                            aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/my_voucher') ?>" class="btn btn-danger"
                               style="color: white"><i class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<!-- BEGIN: Footer-->
@include('admin.base.footer')
<!-- END: Footer-->
@include('admin.base.script')
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".btn-add-account", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('category_id', $('#category_id').val());
            form_data.append('title', $('#title').val());
            form_data.append('content', $('#content').val());
            form_data.append('code', $('#code').val());
            form_data.append('time_start', $('#time_start').val());
            form_data.append('time_end', $('#time_end').val());
            form_data.append('number', $('#number').val());
            form_data.append('active', $('#active').val());
            form_data.append('image', $('#image')[0].files[0]);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });
</script>
<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>

