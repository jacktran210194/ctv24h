<?php
$page = 'voucher_category';

use Illuminate\Support\Facades\File;use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label class="lb">Tên danh mục</label>
                                <input type="text" id="name" placeholder="Tên danh mục"
                                       value="<?= isset($data) ? $data['name'] : ''?>"
                                       class="form-control">
                            </div>
                            <?php if(isset($data)) :?>
                            <div class="form-group">
                                <label class="lb">Trạng thái</label>
                                <select name="active" id="active" class="form-control">
                                    <option value="1" @isset($data) @if($data->active == 1) selected @endif @endisset >Kích hoạt</option>
                                    <option value="0" @isset($data) @if($data->active == 0) selected @endif @endisset  >Khóa</option>
                                </select>
                            </div>
                            <?php endif ;?>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Ảnh</label>
                                <div class="form-label-group">
                                    <input type='file' value="" accept="image/*" name="image" id="image" onchange="changeImg(this)">
                                    <br>
                                    <img width="120px" height="120px" id="avatar" class="thumbnail image-show-qr"
                                         src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/images/avt.jpg' ?>">
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/my_voucher/category/save') ?>"><i class="fa fa-floppy-o"
                                                                                            aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/my_voucher/category') ?>" class="btn btn-danger"
                               style="color: white"><i class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<!-- BEGIN: Footer-->
@include('admin.base.footer')
<!-- END: Footer-->
@include('admin.base.script')
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".btn-add-account", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('name', $('#name').val());
            form_data.append('active', $('#active').val());
            form_data.append('image', $('#image')[0].files[0]);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });
</script>
<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>

