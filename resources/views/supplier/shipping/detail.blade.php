<div class="detail_shipping card position-absolute position-center p-1" style="width: 650px;height: auto; z-index: 100000000000001;">
    <div class="card-body">
        <a><i class="fa fa-times fa-2x position-absolute icon_close text-black" style="right: 2%; top:3%;"></i></a>
        <div class="content_info text-center">
            <img width="179px" height="66px" src="{{$shipping->image}}" alt="">
            <h3 class="text-bold-700 f-24 mt-2">{{$shipping->name}}</h3>
        </div>
        <div class="info_shipping mt-2">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card text-center" style="border: 3px solid #D1292F; border-radius: 6px!important;">
                        <div class="card-body">
                            <h5 class="text-bold-700 text-black">Khối lượng tối đa</h5>
                            <h3 class="text-blue text-bold-700">100kg/đơn hàng</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card text-center" style="border: 3px solid #D1292F; border-radius: 6px!important;">
                        <div class="card-body">
                            <h5 class="text-bold-700 text-black">Kích thước tối đa</h5>
                            <h3 class="text-blue text-bold-700">Không quá 20cm</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card text-center" style="border: 3px solid #D1292F; border-radius: 6px!important;">
                        <div class="card-body">
                            <h5 class="text-bold-700 text-black">Giá trị đơn hàng tối đa</h5>
                            <h3 class="text-blue text-bold-700">20 triệu đồng</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
