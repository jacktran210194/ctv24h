<?php
$page = 'shipping_supplier';
$data_login = Session::get('data_supplier');

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #47AA04;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <div class="d-flex align-items-center">
            <a href="{{url('admin/order/shipping')}}" class="menu-tab-white mr-2 text-bold-700">Quản lý vận chuyển</a>
            <a class="menu-tab-white menu-tab-active text-bold-700">Cài đặt vận chuyển</a>
        </div>
        <h3 class="mt-3 mb-3">Cài đặt vận chuyển</h3>
        <div class="row">
            @if(isset($data_shipping))
                @foreach($data_shipping as $value)
                    @if(str_contains($value->name, 'Viettel Post'))
                        <div class="col-lg-3">
                            <div class="card p-2" style="border-radius: 20px">
                                <div class="d-flex align-items-center">
                                    <span class="w-100 text-bold-700">{{$value->name}}</span>
                                    <a data-id="{{$value->id}}"
                                       data-url="{{url('admin/shipping_supplier/detail/'.$value->id)}}"
                                       class="f-12 text-underline text-blue position-absolute text-bold-700 btn_detail"
                                       style="right: 5%;">Chi
                                        tiết</a>
                                </div>
                                <div class="d-flex align-items-center mt-2">
                                    <img class="image-preview" width="130px" height="40px"
                                         src="{{$value->image}}">
                                    <label class="switch ml-3 position-absolute" style="right: 5%;">
                                        <input class="btn_check" type="checkbox" checked disabled >
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        @else
                    <div class="col-lg-3">
                        <div class="card p-2" style="border-radius: 20px">
                            <div class="d-flex align-items-center">
                                <span class="w-100 text-bold-700">{{$value->name}}</span>
                                <a data-id="{{$value->id}}"
                                   data-url="{{url('admin/shipping_supplier/detail/'.$value->id)}}"
                                   class="f-12 text-underline text-blue position-absolute text-bold-700 btn_detail"
                                   style="right: 5%;">Chi
                                    tiết</a>
                            </div>
                            <div class="d-flex align-items-center mt-2">
                                <img class="image-preview" width="130px" height="40px"
                                     src="{{$value->image}}">
                                <label class="switch ml-3 position-absolute" style="right: 5%;">
                                    <input
                                        class="btn_check" @if(isset($value->shipping)) checked
                                        @endif type="checkbox"
                                        data-id="{{$value->id}}"
                                        data-url="{{url('admin/shipping_supplier/save')}}"
                                        data-supplier="{{isset($data_login) ? $data_login['id'] : ''}}"
                                        data-shipping="{{isset($value->shipping) ? $value->shipping : ''}}"
                                    >
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            @else
                <h3>Không có dữ liệu !</h3>
            @endif
        </div>

    </div>
</div>
<div class="show_detail">
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn_check", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            let check = $(this).is(':checked') ? 1 : 0;
            form_data.append('check', check);
            form_data.append('shipping_id', $(this).attr('data-id'));
            form_data.append('supplier_id', $(this).attr('data-supplier'));
            form_data.append('shipping', $(this).attr('data-shipping'));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        swal("Thông báo", "Thất bại", "error");
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });

        $(document).on('click', '.btn_detail', function () {
            let url = $(this).attr('data-url');
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $(".show_detail").html(data);
                    $('#overlay1').addClass('open');
                },
                error: function () {
                    console.log('error')
                }
            });
        });
        $(document).on('click','.icon_close',function () {
            $('.detail_shipping').hide();
            $('#overlay1').removeClass('open');

        })
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

