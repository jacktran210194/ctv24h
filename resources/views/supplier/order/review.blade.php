<div class="card position-fixed position-center border_radius list_review" style="z-index: 200000;">
    <h4 class="p-2 text-bold-700">Đánh giá của khách hàng</h4>
    <div class="card-body text-center text-bold-700">
        <table class="table-responsive">
            @php
            $i=1;
            @endphp
            @if($data_review && count($data_review))
                @foreach($data_review as $value)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>
                            <div class="d-flex align-items-center">
                                <img width="50px" height="50px" class="border_radius mr-1"
                                     src="{{$value->product_image}}"
                                     alt="">
                                <div class="text-bold-700 m-0">
                                    {{$value->product_name}}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div>
                                @if($value->ratings == 1)
                                    <img class="mr-2" width="70px" height="auto"
                                         src="../../assets/admin/app-assets/images/icon_dashboard/1sao.png"
                                         alt="">
                                @elseif($value->ratings == 2)
                                    <img class="mr-2" width="70px" height="auto"
                                         src="../../assets/admin/app-assets/images/icon_dashboard/2sao.png"
                                         alt="">
                                @elseif($value->ratings == 3)
                                    <img class="mr-2" width="70px" height="auto"
                                         src="../../assets/admin/app-assets/images/icon_dashboard/3sao.png"
                                         alt="">
                                @elseif($value->ratings == 4)
                                    <img class="mr-2" width="70px" height="auto"
                                         src="../../assets/admin/app-assets/images/icon_dashboard/4sao.png"
                                         alt="">
                                @else
                                    <img class="mr-2" width="70px" height="auto"
                                         src="../../assets/admin/app-assets/images/icon_dashboard/5sao.png"
                                         alt="">
                                @endif
                            </div>
                            <div>
                                <span class="f-12 text-sliver">{{$value->customer_comment}}</span>
                            </div>
                            <div class="mt-1">
                                @if($value->is_image == 1)
                                    @if(strpos($value->video_review, ".mp4") == true)
                                        <div class="relative" style="margin-right: 10px;">
                                            <video width="65px" height="63px" controls class="video-comment border_table mr-1">
                                                <source src="{{$value->video_review}}">
                                            </video>
                                        </div>
                                    @else
                                        <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                             src="{{$value->image_review}}"
                                             alt="">
                                    @endif
                                @endif
                            </div>
                        </td>
                        <td>{{date('d-m-Y H:i:s',strtotime($value->date_review))}}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10000">Không có đánh giá nào !</td>
                </tr>
            @endif
        </table>

    </div>
    <div class="d-flex justify-content-end p-1">
        <button class="btn btn_sliver btn_cancel_review text-bold-700">Đóng</button>
    </div>
</div>
