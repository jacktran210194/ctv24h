<div class="position-fixed position-center popup-confirm-order p-2" style="z-index: 500002">
    {{--<div class="position-fixed position-center popup_confirm_order p-2 border_radius bg-white" style="z-index: 500000; width: 35%!important; display: none; border: 1px solid #0D7091">--}}
    <div class="header-popups text-center">
        <img class="mb-2" width="60px" height="60px"
             src="../../assets/admin/app-assets/images/icon_dashboard/check_xanh.png" alt="">
        <h3 class="text-bold-700 text-blue text-bold-700 mb-2">Xác nhận đơn hàng</h3>
    </div>
    <div class="popup-content">
        <div class="form-group">
            <label class="text-bold-700">Địa chỉ kho lấy hàng</label>
            <select id="address_pick" class="form-control input_config">
                @isset($data_address)
                    <option class="input_config" value="{{$data_address->id}}">{{$data_address->address}}</option>
                @endisset
                @isset($address)
                    @foreach($address as $value)
                        <option class="input_config" value="{{$value->id}}">{{$value->address}}</option>
                    @endforeach
                @endisset
            </select>
        </div>
        <div class="form-group">
            <label class="text-bold-700">Thời gian dự kiến lấy hàng</label>
            {{--            <input disabled id="time_pick" type="date" class="form-control input_config" value="{{isset($data) ? date('Y-m-d', strtotime($data['time_layhang'])) : date('Y-m-d')}}">--}}
            <input type="date" id="time_pick" class="form-control input_config"
                   value="{{isset($data) ? date('Y-m-d', strtotime($data['created_at'])) : date('Y-m-d')}}">
        </div>
    </div>
    <div class="d-flex justify-content-lg-end align-items-center">
        <button class="btn btn_default border btn-cancel text-bold-700">Huỷ</button>
        <button class="btn btn_main btn-confirm-order ml-2 text-bold-700"
                data-url="{{url('admin/order/confirm_order')}}"
                data-id="{{isset($data) ? $data->id : ''}}"
        >Xác nhận
        </button>
    </div>
</div>
