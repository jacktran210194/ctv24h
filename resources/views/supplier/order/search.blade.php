<?php
$page = 'order_supplier';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<!-- END: Head-->

<!-- BEGIN: Body-->
<style>
    .popup-confirm-order {
        position: fixed;
        width: 35%;
        height: auto;
        z-index: 10;
        display: none;
        background: white;
        border: 1px solid blue;
        border-radius: 16px;
    }

    .popup-confirm-order.show-popup {
        display: block;
        z-index: 20;
    }

    .popup-wait-shipping {
        position: fixed;
        /*top: 20%;*/
        /*right: 30%;*/
        width: 30%;
        height: auto;
        z-index: 10;
        display: none;
        background: white;
        border: 1px solid blue;
        border-radius: 16px;
    }

    .popup-wait-shipping.show-popup {
        display: block;
        z-index: 20;
    }
</style>
<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <!-- BEGIN: Tab Bar-->
        <!-- END: Tab Bar-->
        <div id="show_list_order">
            <div class="card p-1">
                <div class="card-content">
                    <button style="display: none"
                            class="btn btn_main btn-confirm-order mb-2"
                            data-url="{{url('admin/order/confirm_order')}}"
                    ><i class="fa fa-check"></i> Xác nhận hàng loạt
                    </button>
                    <div class="d-flex align-items-center justify-content-between" style="position: relative;">
                        <h3>Kết quả tìm kiếm với mã đơn hàng: {{$order_code}}</h3>
                        <a href="{{url('admin/order/all')}}"><button class="btn btn-outline-danger text-bold-700">Huỷ tìm kiếm</button></a>
                    </div>

                    <h3 id="count_order" class="mb-3 text-bold-600 text-red">{{$total_data}} đơn hàng</h3>
                    </div>

                    <!-- content -->
                    <div class="table-responsive table-responsive-lg border_table">
                        <table class="table data-list-view table-sm">
                            <thead>
                            <tr class="bg_table text-center">
                                <th>Sản phẩm</th>
                                <th>Loại sản phẩm</th>
                                <th>Trạng thái</th>
                                <th>Vận chuyển</th>
                                <th>Tổng đơn hàng</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                            <tbody id="get">
                            @if($data_order && count($data_order))
                                @foreach($data_order as $value)
                                    <tr class="border_tr">
                                        <td colspan="10">
                                            <div class="d-flex align-items-center">
                                                <img style="border-radius: 50%" width="20px" height="20px"
                                                     src="{{$value->shop_image}}" alt="">
                                                <b style="margin-left: 10px;">{{$value->shop_name}}</b>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border_tr">
                                        <td>
                                            @if(count($data_order_detail) && $data_order_detail)
                                                @foreach($data_order_detail as $v)
                                                    @if($v->order_id == $value->id)
                                                        <div class="list_product d-flex align-items-center mb-1">
                                                            <div class="img_product">
                                                                <img class="border_radius" width="70px" height="70px"
                                                                     src="{{$v->product_image}}"
                                                                     alt="">
                                                            </div>
                                                            <div class="info_product ml-1">
                                                    <span class="text-bold-700">{{$v->product_name}} <span
                                                            class="text-default">(Số lượng: {{$v->quantity}})</span></span>
                                                                <br>
                                                                <span
                                                                    class="text-sliver f-12 m-0 text-bold-700">ID đơn hàng: {{$value->order_code}}</span>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                            <a href="{{url('admin/order/detail/'.$value->id)}}"
                                               class="text-underline text-bold-700">Xem thêm</a>
                                        </td>
                                        <td class="text-center text-blue text-bold-700">{{$value->order_dropship}}</td>
                                        <td class="text-center">
                                            {{--                                @if($value['status'] == 0)--}}
                                            {{--                                    @if($value['today'] >= $value['time_10_min'])--}}
                                            {{--                                                                        <img class="mb-1" width="210px" height="auto"--}}
                                            {{--                                                                             src="../../assets/admin/app-assets/images/icon_dashboard/noti_xac_nhan.png"--}}
                                            {{--                                                                             alt="">--}}
                                            {{--                                                                        <br>--}}
                                            {{--                                    @endif--}}
                                            {{--                                @endif--}}
                                            @if($value['status'] == 1 && ($value['shipping_confirm'] == 0))
                                                <img class="mb-1" width="210px" height="auto"
                                                     src="../../assets/admin/app-assets/images/icon_dashboard/noti_xac_nhan.png"
                                                     alt="">
                                                <br>
                                            @endif
                                            @if($value['status'] == 1 && $value->rui_ro == false && $value['shipping_confirm'] == 1)
                                                <p class="badge badge-pill bg-noti-success" style="font-weight: 700!important;">
                                                    <img src="../../assets/admin/app-assets/images/icon_dashboard/noti_success.png"
                                                         alt="">
                                                    Thời gian dự kiến lấy hàng {{date('d-m-Y',strtotime($value->time_pick))}}
                                                </p>
                                                <br>
                                            @endif
                                            @if(($value->rui_ro == true) && $value['status'] == 1)
                                                <img class="mb-1" width="150px" height="auto"
                                                     src="../../assets/admin/app-assets/images/icon_dashboard/noti_rui_ro.png"
                                                     alt="">
                                                <br>
                                            @endif
                                            <span class="text-bold-700">{{$value->is_status}}</span>
                                            <br>
                                            @if($value['status'] == 0 || $value['status'] == 1 || $value['status'] == 2)
                                                <span class="text-sliver f-12">Thanh toán COD</span>
                                            @endif
                                            @if($value['status'] == 2)
                                                <br>
                                                <img width="250px" height="auto"
                                                     src="../../assets/admin/app-assets/images/icon_dashboard/noti_shipping.png"
                                                     alt="">
                                            @endif
                                            @if($value['status'] == 3)
                                                <span class="text-sliver f-12">Hãy đánh giá người mua</span>
                                            @endif
                                            @if($value['status'] == 4)
                                                @if($value['cancel_confirm'] == 1)
                                                    <span class="text-sliver f-12 text-bold-700">{{$value->user_cancel}}</span>
                                                    <br>
                                                @endif
                                                <span class="text-sliver f-12 text-bold-700">{{$value->notification_status}}</span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            {{--                                <img width="46px" height="16px"--}}
                                            {{--                                     src="https://yt3.ggpht.com/ytc/AAUvwnhU0GWahEHuL6Wp-IKCAmkyejpu95fxhh7Jqg2xCg=s48-c-k-c0x00ffffff-no-rj-mo"--}}
                                            {{--                                     alt="">--}}
                                            <p class="text-bold-700">{{$value->shipping}}</p>
                                        </td>
                                        <td class="text-center">
                                <span
                                    class="text-red f-18 text-bold-700">{{number_format($value->total_price)}} đ</span>
                                            <p class="text-sliver">{{$value->payment}}</p>
                                        </td>
                                        <td class="text-center">
                                            @if($value['status'] == 0)
                                                {{--                                    @if($value['today'] < $value['time_10_min'])--}}
                                                <a href="{{url('admin/order/detail/'.$value->id)}}"
                                                   class="text-blue text-underline text-bold-700">Chi tiết</a>
                                                {{--                                    @elseif($value['today'] >= $value['time_10_min'])--}}
                                                {{--                                        <a data-id="{{$value->id}}"--}}
                                                {{--                                           data-url="{{url('admin/order/show_wait_shipping/'.$value->id)}}"--}}
                                                {{--                                           class="text-blue text-underline text-bold-700 btn-show-confirm">Xác nhận</a>--}}
                                                {{--                                    @endif--}}
                                            @elseif($value['status'] == 1)
                                                @if($value['shipping_confirm'] == 1)
                                                    <a href="{{url('admin/order/detail/'.$value->id)}}"
                                                       class="text-blue text-underline text-bold-700 btn-detail-order">Chi tiết</a>
                                                @elseif($value['shipping_confirm'] == 0)
                                                    <a data-id="{{$value->id}}"
                                                       data-url="{{url('admin/order/show_wait_shipping/'.$value->id)}}"
                                                       class="text-blue text-underline text-bold-700 btn-show-confirm">Xác nhận</a>
                                                @endif
                                            @elseif($value['status'] == 2)
                                                <a href="{{url('admin/order/detail/'.$value->id)}}"
                                                   class="text-blue text-underline text-bold-700 btn-detail-order">Chi tiết</a>
                                            @elseif($value['status'] == 3)
                                                <a href="{{url('admin/order/detail/'.$value->id)}}"
                                                   class="text-blue text-underline text-bold-700 btn-detail-order">Chi tiết</a>
                                                <br>
                                                <a class="text-blue text-underline text-bold-700 btn-review">Đánh giá người mua</a>
                                            @elseif($value['status'] == 4)
                                                @if($value['cancel_confirm'] == 1)
                                                    <a href="{{url('admin/order/detail/'.$value->id)}}"
                                                       class="text-blue text-underline text-bold-700 btn-detail-order">Chi tiết</a>
                                                @else
                                                    <a class="text-blue text-underline text-bold-700 btn_confirm_cancel"
                                                       data-id="{{$value->id}}">Phản hồi đơn huỷ</a>
                                                @endif
                                            @else
                                                <a data-id="{{$value->id}}"
                                                   data-url="{{url('admin/order/show_confirm/'.$value->id)}}"
                                                   class="text-blue text-underline text-bold-700 btn-feedback">Phản hồi</a>
                                                <br>
                                                <a class="text-blue text-underline text-bold-700 btn-detail-return-money">Chi tiết
                                                    trả hàng hoàn tiền</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center text-bold-700">
                                    <td colspan="1000">Không có đơn hàng nào !</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!--  Chat -->
        <div class="chat-footer d-flex justify-content-lg-end w-100">
            <button class="border-radius-8 out-line-none bg-red font-weight-bold color-white d-flex align-items-center"
                    style="margin-right: 20px">
                <img src="../../assets/admin/app-assets/images/icons/live_chat.png">
                <a class="color-white" style="margin-left: 10px">Chat với CTV24</a>
            </button>
            <button
                class="border_box border-radius-8 out-line-none font-weight-bold color-gray d-flex align-items-center">
                <img src="../../assets/admin/app-assets/images/icons/chat-bubbles-with-ellipsis.png">
                <a class="color-white" style="margin-left: 10px">Chat với khách hàng</a>
            </button>
        </div>
        <!-- END: Chat -->
    </div>
</div>
<div class="show-confirm"></div>
<div class="show-popup-form-shipping"></div>
<div class="position-fixed position-center popup-confirm-order p-2" style="z-index: 500002">
    {{--<div class="position-fixed position-center popup_confirm_order p-2 border_radius bg-white" style="z-index: 500000; width: 35%!important; display: none; border: 1px solid #0D7091">--}}
    <div class="header-popups text-center">
        <img class="mb-2" width="60px" height="60px"
             src="../../assets/admin/app-assets/images/icon_dashboard/check_xanh.png" alt="">
        <h3 class="text-bold-700 text-blue text-bold-700 mb-2">Xác nhận đơn hàng</h3>
    </div>
    <div class="popup-content">
        <div class="form-group">
            <label class="text-bold-700">Địa chỉ kho lấy hàng</label>
            <select id="address_pick" class="form-control input_config">
                @isset($data_address)
                    <option class="input_config" value="{{$data_address->id}}">{{$data_address->address}}</option>
                @endisset
                @isset($address)
                    @foreach($address as $value)
                        <option class="input_config" value="{{$value->id}}">{{$value->address}}</option>
                    @endforeach
                @endisset
            </select>
        </div>
        <div class="form-group">
            <label class="text-bold-700">Thời gian dự kiến lấy hàng</label>
            {{--            <input disabled id="time_pick" type="date" class="form-control input_config" value="{{isset($data) ? date('Y-m-d', strtotime($data['time_layhang'])) : date('Y-m-d')}}">--}}
            <input type="date" id="time_pick" class="form-control input_config"
                   value="{{isset($data) ? date('Y-m-d', strtotime($data['created_at'])) : date('Y-m-d')}}">
        </div>
    </div>
    <div class="d-flex justify-content-lg-end align-items-center">
        <button class="btn btn_default border btn-cancel text-bold-700">Huỷ</button>
        <button class="btn btn_main btn-confirm-order ml-2 text-bold-700"
                data-url="{{url('admin/order/confirm_order')}}"
                data-id="{{isset($data) ? $data->id : ''}}"
        >Xác nhận
        </button>
    </div>
</div>
<div class="show-popup-review" style="">
</div>

<div class="show-popup-confirm-cancel" style="display: none;">
    <div class="card position-fixed position-center" style="z-index: 200000;">
        <div class="card-body text-center text-bold-700">
            Bạn có muốn huỷ đơn hàng này không?
        </div>
        <div class="d-flex justify-content-end p-1">
            <button class="btn btn_main btn_ok_cancel mr-1 text-bold-700"
                    data-url="{{url('admin/order/confirm_cancel')}}">Đồng ý
            </button>
            <button class="btn btn_sliver btn_cancel_cancel text-bold-700">Huỷ</button>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        //show popup xác nhận đơn hàng // chọn hình thức giao hàng
        $(document).on('click', '.btn-show-confirm', function () {
            let url = $(this).attr('data-url');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.show-popup-form-shipping').html(data);
                    $('.popup-wait-shipping').addClass('show-popup');
                    $('#overlay1').addClass('open');
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //confirm Chọn hình thức lấy hàng (status: chờ lấy hàng)
        $(document).on('click', '.btn-confirm-wait-shipping', function () {
            let form_shipping = $('#form_shipping:checked').val();
            let order_id = $(this).attr('data-id');
            if(!form_shipping){
                alert('Vui lòng chọn hình thức phù hợp !');
                return false;
            }
            if(form_shipping === '0'){
                alert('Chức năng đang phát triển !');
            }else if(form_shipping === '1'){
                $('.btn-confirm-order').attr('data-form-shipping',form_shipping);
                $('.btn-confirm-order').attr('data-id',order_id);
                $('.popup-confirm-order').addClass('show-popup');
            }
        });

        //confirm trạng thái chờ xác nhận khi đã chọn hình thức lấy hàng và thời gian lấy hàng kho hàng
        $(document).on('click', '.btn-confirm-order', function () {
            let address_pick = $('#address_pick').val();
            let time_pick = $('#time_pick').val();
            let id = $(this).attr('data-id');
            let url = $(this).attr('data-url');
            let form_shipping = $(this).attr('data-form-shipping');
            let form_data = new FormData();
            form_data.append('id', id);
            form_data.append('address_pick', address_pick);
            form_data.append('time_pick', time_pick);
            form_data.append('form_shipping',form_shipping);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        alert(data.msg);
                        location.reload();
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });

        $(document).on('click', '.btn-cancel', function () {
            $('.popup-confirm-order').removeClass('show-popup');
        });

        $(document).on('click', '.btn-cancel-wait-shipping', function () {
            $('.popup-wait-shipping').removeClass('show-popup');
            $('#overlay1').removeClass('open');
        });

        //Xác nhận huỷ đơn cho khách
        $(document).on('click', '.btn_confirm_cancel', function () {
            let id = $(this).attr('data-id');
            $('.show-popup-confirm-cancel').show();
            $('#overlay1').addClass('open');
            $('.btn_ok_cancel').attr('data-id', id);
        });

        $(document).on('click', '.btn_ok_cancel', function () {
            let url = $(this).attr('data-url');
            let id = $(this).attr('data-id');
            let form_data = new FormData();
            form_data.append('id', id);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        alert(data.msg);
                        location.reload();
                    } else {
                        alert(data.msg);
                    }
                },
                error: function (e) {
                    console.log(e)
                },
            });

        });

        $(document).on('click', '.btn_cancel_cancel', function () {
            $('.show-popup-confirm-cancel').hide();
            $('#overlay1').removeClass('open');
        });

        //xem đánh giá
        $(document).on('click', '.btn-review', function () {
            let url = $(this).attr('data-url');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $(".show-popup-review").html(data);
                    $('.list_review').show();
                    $('#overlay1').addClass('open');
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        $(document).on('click', '.btn_cancel_review', function () {
            $('.list_review').hide();
            $('#overlay1').removeClass('open');
        });

    });

</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
