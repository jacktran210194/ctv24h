<?php
$page = 'order_supplier';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>
<style>
    .text_text {
        position: absolute;
        right: 2%;
    }

    .text_title {
        position: absolute;
        right: 20%;
    }
</style>
<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        @if($order->status == 2)
            <button data-url="{{url('admin/order/update_status')}}" data-id="{{$order->id}}"
                    class="btn btn_main mb-1 text-bold-700 btn_shipped"
                    style="width: 200px!important; height: 50px!important;"><i class="fa fa-check mr-1"></i>Đã giao hàng
            </button>
        @endif
        @if($order->status == 1)
            <button data-url="{{url('admin/order/update_shipping')}}" data-id="{{$order->id}}"
                    class="btn btn-success mb-1 text-bold-700 btn_shipping"
                    style="height: 50px!important;"><i class="fa fa-check mr-1"></i>Lấy hàng thành công
            </button>
        @endif
        <div class="card">
            <h3 class="text-bold-700 p-2">Chi tiết đơn hàng</h3>
            <div class="card-content p-3">
                <div class="status_order d-flex align-items-center">
                    @if($order->status == 0)
                        <h3 class="text-red m-0 f-18 text-bold-700">CHỜ XÁC NHẬN</h3>
                    @elseif($order->status == 1)
                        <h3 class="text-red m-0 f-18 text-bold-700">CHỜ LẤY HÀNG</h3>
                    @elseif($order->status == 2)
                        <img class="mr-1" width="26px" height="auto"
                             src="../../assets/admin/app-assets/images/icon_dashboard/shipping.png" alt="">
                        <h3 class="text-red m-0 f-18 text-bold-700">ĐANG GIAO</h3>
                    @elseif($order->status == 3)
                        <h3 class="text-red m-0 f-18 text-bold-700">ĐÃ GIAO</h3>
                    @elseif($order->status == 4)
                        <img class="mr-1" width="26px" height="26px"
                             src="../../assets/admin/app-assets/images/icon_dashboard/cancel.png" alt="">
                        <h3 class="text-red m-0 f-18 text-bold-700">{{$order->is_status}}</h3>
                    @else
                        <h3 class="text-red m-0 f-18 text-bold-700">HOÀN TIỀN</h3>
                    @endif


                </div>
                <div class="note_status mt-1">
                    @if($order->status == 4)
                        <span class="text-sliver">{{$order->user_cancel}}</span>
                        <br>
                        <span class="text-sliver">Lý do huỷ: {{$order->reason_cancel}}</span>
                    @endif
                </div>
                <h3 class="text-bold-700">Ngày đặt: {{$order['date']}}</h3>
                <div class="history_customer d-flex align-items-center justify-content-between mt-5">
                    <div style="width: 380px;" class="mr-5">
                        <h3 class="text-bold-700 mb-1 f-18">Lịch sử mua hàng của người mua</h3>
                        <span class="text-sliver">Với người mua có tỷ lệ giao hàng thành công thấp, hãy liên hệ xác nhận đơn hàng với họ trước khi gửi hàng.</span>
                    </div>
                    <div class="d-flex mr-5">
                        <h3 class="text-bold-700 mr-2" style="color: red; font-size: 24px!important;">98 %</h3>
                        <p class="text-sliver">Đơn giao thành công</p>
                    </div>
                    <div class="d-flex">
                        <h3 class="text-bold-700 mr-2" style="color: red;font-size: 24px!important;">5/5</h3>
                        <p class="text-sliver">Đánh giá người mua</p>
                    </div>
                </div>
            </div>
            <div class="border-bottom"></div>
            <div class="card-content p-3">
                <div class="d-flex justify-content-between">
                    <div class="card_shadow p-2" style="width: 32%!important;">
                        <h3 class="text-bold-700 f-18 mb-1">ID đơn hàng</h3>
                        <p class="badge bg_sliver">{{$order->order_code}}</p>
                        <p>{{$order->payment}}</p>
                    </div>
                    <div class="card_shadow p-2" style="width: 32%!important;">
                        <h3 class="text-bold-700 f-18 mb-1">Địa chỉ nhận hàng</h3>
                        <p class="text-sliver text-bold-700">{{$order->name_shipping}}
                            <span>{{$order->phone_shipping}}</span></p>
                        <p class="text-sliver">{{$order->address_shipping}}</p>
                    </div>
                    <div class="card_shadow p-2" style="width: 32%!important;">
                        <h3 class="text-bold-700 f-18 mb-1">Thông tin vận chuyển</h3>
                        <p class="text-sliver text-bold-700">{{$order->shipping}}</p>
                        <span class="badge bg_sliver">#312321032103</span>
                    </div>
                </div>
                <div class="info_customer d-flex align-items-center mt-2">
                    <div class="d-flex align-items-center justify-content-start">
                        <img class="border_radius mr-1" width="20px" height="20px"
                             src="{{$customer->image}}"
                             alt="">
                        <p class="text-sliver text-bold-700 m-0">{{$customer->name}}</p>
                    </div>
                    <div class="btn_info d-flex align-items-center position-absolute" style="right: 2%;">
                        <a target="_blank" href="{{url('chat')}}"><button class="btn border p-3">Chat ngay</button></a>
                    </div>
                </div>
            </div>
            <div class="border-bottom"></div>
            <div class="card-content p-3">
                <h4 class="text-bold-700">Thông tin thanh toán</h4>
                <div class="table-responsive table-responsive-lg">
                    <table class="table data-list-view table-sm">
                        <thead>
                        <tr class="text-sliver" style="font-size: 20px!important;">
                            <th class="text-center">STT</th>
                            <th>Sản phẩm</th>
                            <th>Lời nhắn của khách</th>
                            <th class="text-center">Đơn giá</th>
                            <th class="text-center">Số lượng</th>
                            <th class="text-center">Thành tiền</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($order_detail as $value)
                            <tr>
                                <td class="text-center">{{$i++}}</td>
                                <td>
                                    <div class="d-flex align-items-center" style="width: 60%!important;">
                                        <img class="mr-1 image-preview" width="71px" height="71px"
                                             style="border-radius: 16px;"
                                             src="{{$value->product_image}}"
                                             alt="">
                                        <div class="info_product">
                                            <span class="text-bold-700">{{$value->product_name}}</span>
                                            <br>
                                            <span
                                                class="text-sliver text-bold-500">{{$value->value_attr}} - {{$value->size_of_value}}</span>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <span class="text-bold-700">{{ isset($order->note) ? $order->note : 'Không có lời nhắn nào của khách' }}</span>
                                </td>
                                <td class="text-center">
                                    <h3 class="text-bold-700 f-18">{{number_format($value->product_price)}} đ</h3>
                                </td>
                                <td class="text-center">
                                    <span class="text-bold-700">X {{$value->quantity}}</span>
                                </td>
                                <td class="text-center">
                                    <h3 class="text-bold-700 f-18">{{number_format($value->total_price)}} đ</h3>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    <a class="text-sliver">Xem chi tiết doanh thu <img
                            src="../../assets/admin/app-assets/images/icon_dashboard/mui_ten.png" alt=""></a>
                </div>
                <div class="clearfix"></div>
                <div class="total_order float-right mt-2">
                    <table>
                        <thead>
                        <tr>
                            <td>
                                <span class="text-sliver text-bold-700 text_title">Tổng tiền thanh toán</span>
                                <span
                                    class="f-18 text-bold-700 text_text">{{number_format($order->total_price)}} đ</span>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <span class="text-sliver text-bold-700 text_title">Tổng tiền sản phẩm</span>
                                <span
                                    class="f-18 text-bold-700 text_text">{{number_format($order->total_price_product)}} đ</span>
                            </td>

                        </tr>
                        <tr>
                            @foreach($order_detail as $value)
                                @php $product = \App\Models\ProductModel::find($value->product_id); @endphp
                                @if($product->dropship == 1)
                                    @php
                                        $product_dropship = \App\Models\ProductDropShipModel::where('product_id', $product->id)->first();
                                        $product_dropship_2 = \App\Models\ProductModel::find($product_dropship->product_parent);
                                        $type_product = \App\Models\AttributeValueModel::find($value->type_product);
                                        $attr_product = \App\Models\AttributeValueModel::where('product_id', $product_dropship_2->id)->where('size_of_value', $type_product->size_of_value)->first();
                                    @endphp
                                    <td>
                                        <span class="text-sliver text-bold-700 text_title">Tiền sản phẩm thực lãnh</span>
                                        <span
                                            class="f-18 text-bold-700 text_text">{{number_format($attr_product->price_ctv)}} đ</span>
                                    </td>
                                @endif
                            @endforeach
                        </tr>
                        <tr>
                            <td>
                                <span class="text-sliver text-bold-700 text_title">Phí vận chuyển (không trợ giá)</span>
                                <span class="f-18 text-bold-700 text_text">10.000 đ</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="text-sliver text-bold-700 text_title">Phí giao dịch</span>
                                <span class="f-18 text-bold-700 text_text">({{$fee->fee}}%) {{number_format($order->total_price_product * ($fee->fee/100))}} đ</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="text-sliver text-bold-700 text_title">Doanh thu</span>
                                <span
                                    class="f-18 text-bold-700 text_text">{{number_format($order->price_supplier)}} đ</span>
                            </td>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
            <div class="border-bottom"></div>
            {{-- <div class="card-content p-3">
                <div class="total_price_all d-flex align-items-center justify-content-between">
                    <h5 class="text-bold-700">Thanh toán của người mua</h5>
                    <h3 class="text-bold-700">{{number_format($order->total_price)}}
                        đ
                        <a><img
                                src="../../assets/admin/app-assets/images/icon_dashboard/mui_ten.png" alt=""></a></h3>

                </div>
            </div> --}}
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-add-account", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('status', $('#status').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });

        $(document).on("click", ".btn_shipped", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('order_id', $(this).attr('data-id'));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        swal("Thông báo", "Cập nhật thành công !", "success");
                        window.location.href = data.url;
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });

        $(document).on("click", ".btn_shipping", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('order_id', $(this).attr('data-id'));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        swal("Thông báo", "Cập nhật thành công !", "success");
                        window.location.href = data.url;
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });
    })
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
