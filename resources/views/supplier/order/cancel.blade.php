<?php
$page = 'order_supplier_cancel';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<!-- END: Head-->

<!-- BEGIN: Body-->
<style>
    .popup-confirm-order {
        position: fixed;
        width: 35%;
        height: auto;
        z-index: 10;
        display: none;
        background: white;
        border: 1px solid blue;
        border-radius: 16px;
    }

    .popup-confirm-order.show-popup {
        display: block;
        z-index: 20;
    }

    .popup-wait-shipping {
        position: fixed;
        /*top: 20%;*/
        /*right: 30%;*/
        width: 30%;
        height: auto;
        z-index: 10;
        display: none;
        background: white;
        border: 1px solid blue;
        border-radius: 16px;
    }

    .popup-wait-shipping.show-popup {
        display: block;
        z-index: 20;
    }
</style>
<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <!-- BEGIN: Tab Bar-->
        <div class="tab-bar-page">
            <ul class="tab-bar-content d-flex align-items-center justify-content-lg-start flex-wrap">
                <li class="d-flex align-items-center justify-content-lg-center filter-all"
                    data-url-all="{{url('admin/order/all')}}">
                    <a href="{{url('admin/order/all')}}"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Tất cả</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-wait-confirm"
                    data-url-waitn-confirm="{{url('admin/order/wait_confirm')}}">
                    <a href="{{url('admin/order/wait_confirm')}}"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Chờ xác
                        nhận</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-wait-shipping"
                    data-url-wait-shipping="{{url('admin/order/wait_shipping')}}">
                    <a href="{{url('admin/order/wait_shipping')}}"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Chờ lấy
                        hàng</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-shipping"
                    data-url-shipping="{{url('admin/order/shipping')}}">
                    <a href="{{url('admin/order/shipping')}}"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đang giao</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-shipped"
                    data-url-shipped="{{url('admin/order/shipped')}}">
                    <a href="{{url('admin/order/shipped')}}"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đã giao</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center active filter-cancel"
                    data-url-cancel="{{url('admin/order/cancel')}}">
                    <a href="{{url('admin/order/cancel')}}"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đã Huỷ</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-return popup_default"
                    data-url-return="{{url('admin/order/return_money')}}">
                    <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Hoàn tiền</a>
                </li>
            </ul>
        </div>
        <!-- END: Tab Bar-->
        <div id="show_list_order">
            <div class="card p-1">
                <div class="card-content">
                    <div class="form_filter d-flex">
                        <form action="{{url('admin/order/search')}}" method="GET">
                            @csrf
                            <div class="form-label-group position-relative has-icon-right">
                                <input required type="text" id="search" class="form-control" name="order_code"
                                       placeholder="Nhập mã vận đơn...">
                                <div class="form-control-position">
                                    <img src="../../assets/admin/app-assets/images/icon_dashboard/search.png" alt="">
                                </div>
                            </div>
                        </form>

                        <div class="form-group d-flex form-time align-items-center"
                             style="width: 75%; position: absolute; right: 1%">
                            <span style="width: 100%;">Ngày đặt hàng</span>
                            <input style="width: 30%" class="filter_time form-control time-start"
                                   type="datetime-local" value="{{date('Y-m-d\TH:i:s')}}">
                            <input style="width: 30%" class="filter_time form-control time-stop"
                                   type="datetime-local" value="{{date('Y-m-d\TH:i:s')}}">
                            <button type="button" style="width: 20%;" class="btn border btn-filter-order ml-1"
                                    data-url="{{url('admin/order/filter_order')}}"
                            ><i class="fa fa-search"></i>
                            </button>
                            <a class="btn border ml-1" style="width: 100%;" href="{{url('admin/order/export')}}">Xuất
                                báo
                                cáo</a>
                            <a style="display: none;" class="ml-1"><img src="../../assets/admin/app-assets/images/icon_dashboard/icon_menu.png"
                                                 alt=""></a>
                        </div>
                    </div>
                    <button style="display: none"
                            class="btn btn_main btn-confirm-order mb-2"
                            data-url="{{url('admin/order/confirm_order')}}"
                    ><i class="fa fa-check"></i> Xác nhận hàng loạt
                    </button>
                    <div class="d-flex align-items-center" style="position: relative;">
                        <h3 id="count_order" class="mb-3 text-bold-600 text-red">{{$total_data}} đơn hàng</h3>
                        <button style="right: 0%;" class="btn btn_main position-absolute mb-1 popup_default"><img
                                src="../../assets/admin/app-assets/images/menu/vanchuyen_trang.png" alt=""> Giao hàng
                            loạt
                        </button>
                    </div>

                    <!-- content -->
                    <div class="table-responsive table-responsive-lg border_table">
                        <table class="table data-list-view table-sm">
                            <thead>
                            <tr class="bg_table text-center">
                                <th>Sản phẩm</th>
                                <th>Tên NCC</th>
                                <th>Loại sản phẩm</th>
                                <th>Trạng thái</th>
                                <th>Vận chuyển</th>
                                <th>Tổng đơn hàng</th>
                                <th>Lợi nhuận</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                            <tbody id="get">
                            @if(count($data_order) && $data_order)
                                @foreach($data_order as $value)
                                    <tr class="border_tr">
                                        <td colspan="10">
                                            <div class="d-flex align-items-center">
                                                <img style="border-radius: 50%" width="20px" height="20px"
                                                     src="{{$value->shop_image}}" alt="">
                                                <b style="margin-left: 10px;">{{$value->shop_name}}</b>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border_tr">
                                        <td>
                                            @if(count($data_order_detail))
                                                @foreach($data_order_detail as $v)
                                                    @if($v->order_id == $value->id)
                                                        <div class="list_product d-flex align-items-center mb-1">
                                                            <div class="img_product">
                                                                <img class="border_radius" width="70px" height="70px"
                                                                     src="{{$v->product_image}}"
                                                                     alt="">
                                                            </div>
                                                            <div class="info_product ml-1">
                                                    <span class="text-bold-700">{{$v->product_name}} <span
                                                            class="text-default">(Số lượng: {{$v->quantity}})</span></span>
                                                                <br>
                                                                <span
                                                                    class="text-sliver f-12 m-0 text-bold-700">ID đơn hàng: {{$value->order_code}}</span>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                            <a href="{{url('admin/order/detail/'.$value->id)}}"
                                               class="text-underline text-bold-700">Xem thêm</a>
                                        </td>
                                        <td class="text-center text-bold-700">{{ $value->shop->name }}</td>
                                        <td class="text-center text-blue text-bold-700">{{$value->order_dropship}}</td>
                                        <td class="text-center">
                                            {{--                                @if($value['status'] == 0)--}}
                                            {{--                                    @if($value['today'] >= $value['time_10_min'])--}}
                                            {{--                                                                        <img class="mb-1" width="210px" height="auto"--}}
                                            {{--                                                                             src="../../assets/admin/app-assets/images/icon_dashboard/noti_xac_nhan.png"--}}
                                            {{--                                                                             alt="">--}}
                                            {{--                                                                        <br>--}}
                                            {{--                                    @endif--}}
                                            {{--                                @endif--}}
                                            @if($value['status'] == 1 && ($value['shipping_confirm'] == 0))
                                                <img class="mb-1" width="210px" height="auto"
                                                     src="../../assets/admin/app-assets/images/icon_dashboard/noti_xac_nhan.png"
                                                     alt="">
                                                <br>
                                            @endif
                                            @if($value['status'] == 1 && $value->rui_ro == false && $value['shipping_confirm'] == 1)
                                                <p class="badge badge-pill bg-noti-success"
                                                   style="font-weight: 700!important;">
                                                    <img
                                                        src="../../assets/admin/app-assets/images/icon_dashboard/noti_success.png"
                                                        alt="">
                                                    Thời gian dự kiến lấy
                                                    hàng {{date('d-m-Y',strtotime($value->time_pick))}}
                                                </p>
                                                <br>
                                            @endif
                                            @if(($value->rui_ro == true) && $value['status'] == 1)
                                                <img class="mb-1" width="150px" height="auto"
                                                     src="../../assets/admin/app-assets/images/icon_dashboard/noti_rui_ro.png"
                                                     alt="">
                                                <br>
                                            @endif
                                            <span class="text-bold-700">{{$value->is_status}}</span>
                                            <br>
                                            @if($value['status'] == 0 || $value['status'] == 1 || $value['status'] == 2)
                                                <span class="text-sliver f-12">Thanh toán COD</span>
                                            @endif
                                            @if($value['status'] == 2)
                                                <br>
                                                <img width="250px" height="auto"
                                                     src="../../assets/admin/app-assets/images/icon_dashboard/noti_shipping.png"
                                                     alt="">
                                            @endif
                                            @if($value['status'] == 3)
                                                <span class="text-sliver f-12">Hãy đánh giá người mua</span>
                                            @endif
                                            @if($value['status'] == 4)
                                                @if($value['cancel_confirm'] == 1)
                                                    <span
                                                        class="text-sliver f-12 text-bold-700">{{$value->user_cancel}}</span>
                                                    <br>
                                                @endif
                                                <span
                                                    class="text-sliver f-12 text-bold-700">{{$value->notification_status}}</span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            {{--                                <img width="46px" height="16px"--}}
                                            {{--                                     src="https://yt3.ggpht.com/ytc/AAUvwnhU0GWahEHuL6Wp-IKCAmkyejpu95fxhh7Jqg2xCg=s48-c-k-c0x00ffffff-no-rj-mo"--}}
                                            {{--                                     alt="">--}}
                                            <p class="text-bold-700">{{$value->shipping}}</p>
                                        </td>
                                        <td class="text-center">
                                <span
                                    class="text-red f-18 text-bold-700">{{number_format($value->total_price)}} đ</span>
                                            <p class="text-sliver">{{$value->payment}}</p>
                                        </td>
                                        <td class="text-center text-bold-700">---</td>
                                        <td width="10%" class="text-center">
                                            @if($value['status'] == 0)
                                                {{--                                    @if($value['today'] < $value['time_10_min'])--}}
                                                <a href="{{url('admin/order/detail/'.$value->id)}}"
                                                   class="text-blue text-underline text-bold-700">Chi tiết</a>
                                                {{--                                    @elseif($value['today'] >= $value['time_10_min'])--}}
                                                {{--                                        <a data-id="{{$value->id}}"--}}
                                                {{--                                           data-url="{{url('admin/order/show_wait_shipping/'.$value->id)}}"--}}
                                                {{--                                           class="text-blue text-underline text-bold-700 btn-show-confirm">Xác nhận</a>--}}
                                                {{--                                    @endif--}}
                                            @elseif($value['status'] == 1)
                                                @if($value['shipping_confirm'] == 1)
                                                    <a href="{{url('admin/order/detail/'.$value->id)}}"
                                                       class="text-blue text-underline text-bold-700 btn-detail-order">Chi
                                                        tiết</a>
                                                @elseif($value['shipping_confirm'] == 0)
                                                    <a data-id="{{$value->id}}"
                                                       data-url="{{url('admin/order/show_wait_shipping/'.$value->id)}}"
                                                       class="text-blue text-underline text-bold-700 btn-show-confirm">Xác
                                                        nhận</a>
                                                @endif
                                            @elseif($value['status'] == 2)
                                                <a href="{{url('admin/order/detail/'.$value->id)}}"
                                                   class="text-blue text-underline text-bold-700 btn-detail-order">Chi
                                                    tiết</a>
                                            @elseif($value['status'] == 3)
                                                <a href="{{url('admin/order/detail/'.$value->id)}}"
                                                   class="text-blue text-underline text-bold-700 btn-detail-order">Chi
                                                    tiết</a>
                                                <br>
                                                <a class="text-blue text-underline text-bold-700 btn-review">Đánh giá
                                                    người mua</a>
                                            @elseif($value['status'] == 4)
                                                @if($value['cancel_confirm'] == 1)
                                                    <a href="{{url('admin/order/detail/'.$value->id)}}"
                                                       class="text-blue text-underline text-bold-700 btn-detail-order">Chi
                                                        tiết</a>
                                                @else
                                                    <a class="text-blue text-underline text-bold-700 btn_confirm_cancel"
                                                       data-id="{{$value->id}}">Phản hồi đơn huỷ</a>
                                                @endif
                                            @else
                                                <a data-id="{{$value->id}}"
                                                   data-url="{{url('admin/order/show_confirm/'.$value->id)}}"
                                                   class="text-blue text-underline text-bold-700 btn-feedback">Phản
                                                    hồi</a>
                                                <br>
                                                <a class="text-blue text-underline text-bold-700 btn-detail-return-money">Chi
                                                    tiết
                                                    trả hàng hoàn tiền</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center text-bold-700">
                                    <td colspan="1000">Không có đơn hàng nào !</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="pagination">
            {{$data_order->links('pagination.index')}}
        </div>
        <!--  Chat -->
        <div class="chat-footer d-flex justify-content-lg-end w-100">
            <button class="border-radius-8 out-line-none bg-red font-weight-bold color-white d-flex align-items-center"
                    style="margin-right: 20px; border-color: inherit">
                <img src="../../assets/admin/app-assets/images/icons/live_chat.png">
                <a class="color-white" style="margin-left: 10px">Chat với CTV24</a>
            </button>
            <button
                class="border_box border-radius-8 out-line-none font-weight-bold color-gray d-flex align-items-center" style="border-color: inherit">
                <img src="../../assets/admin/app-assets/images/icons/chat-bubbles-with-ellipsis.png">
                <a class="color-white" style="margin-left: 10px">Chat với khách hàng</a>
            </button>
        </div>
        <!-- END: Chat -->
    </div>
</div>
<div class="show-confirm"></div>
<div class="show-popup-form-shipping"></div>

{{--<div class="position-fixed position-center popup-wait-shipping p-2" style="z-index: 500001">--}}
{{--    <div class="header-popups">--}}
{{--        <div class="d-flex align-items-center">--}}
{{--            <h3 class="text-bold-700 text-blue text-bold-700">Giao đơn hàng</h3>--}}
{{--            <img class="ml-5" width="105px" height="35px"--}}
{{--                 src="https://tuyencongnhan.vn/uploads/images/20-10-17-09/image_2020_10_15T12_43_10_022Z-64.png" alt="">--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="popup-content">--}}
{{--        <div class="d-flex align-items-center mt-2">--}}
{{--            <label class="container_radio">--}}
{{--                <input type="radio" name="form_shipping" id="form_shipping" value="0">--}}
{{--                <span class="radio_config"></span>--}}
{{--            </label>--}}
{{--            <img class="mr-2" width="60px" height="60px"--}}
{{--                 src="../../assets/admin/app-assets/images/icon_dashboard/giaohang_1.png" alt="">--}}
{{--            <div>--}}
{{--                <h4 class="text-bold-700">Tôi sẽ tự mang đến bưu cục</h4>--}}
{{--                <p class="m-0">Bạn có thể gửi tới bất cứ bưu cục giao hàng nhanh nào thuộc cùng tỉnh hoặc thành phố</p>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="d-flex align-items-center mt-2">--}}
{{--            <label class="container_radio">--}}
{{--                <input type="radio" name="form_shipping" id="form_shipping" value="1">--}}
{{--                <span class="radio_config"></span>--}}
{{--            </label>--}}
{{--            <img class="mr-2" width="60px" height="60px"--}}
{{--                 src="../../assets/admin/app-assets/images/icon_dashboard/giaohang_2.png" alt="">--}}
{{--            <div>--}}
{{--                <h4 class="text-bold-700">Đơn vị vận chuyển tới lấy hàng</h4>--}}
{{--                <p class="m-0">Giao hàng nhanh sẽ đến lấy hàng theo địa chỉ nhận hàng mà bạn đã xác nhận.</p>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="d-flex justify-content-lg-end align-items-center mt-2">--}}
{{--        <button class="btn btn_default border btn-cancel-wait-shipping text-bold-700">Huỷ</button>--}}
{{--        <button class="btn btn_main btn-confirm-wait-shipping ml-2 text-bold-700"--}}
{{--                data-url="{{url('admin/order/confirm_wait_shipping')}}"--}}
{{--        >Xác nhận--}}
{{--        </button>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="position-fixed position-center popup-confirm-order p-2" style="z-index: 500002">
    {{--<div class="position-fixed position-center popup_confirm_order p-2 border_radius bg-white" style="z-index: 500000; width: 35%!important; display: none; border: 1px solid #0D7091">--}}
    <div class="header-popups text-center">
        <img class="mb-2" width="60px" height="60px"
             src="../../assets/admin/app-assets/images/icon_dashboard/check_xanh.png" alt="">
        <h3 class="text-bold-700 text-blue text-bold-700 mb-2">Xác nhận đơn hàng</h3>
    </div>
    <div class="popup-content">
        <div class="form-group">
            <label class="text-bold-700">Địa chỉ kho lấy hàng</label>
            <select id="address_pick" class="form-control input_config">
                @isset($data_address)
                    <option class="input_config" value="{{$data_address->id}}">{{$data_address->address}}</option>
                @endisset
                @isset($address)
                    @foreach($address as $value)
                        <option class="input_config" value="{{$value->id}}">{{$value->address}}</option>
                    @endforeach
                @endisset
            </select>
        </div>
        <div class="form-group">
            <label class="text-bold-700">Thời gian dự kiến lấy hàng</label>
            {{--            <input disabled id="time_pick" type="date" class="form-control input_config" value="{{isset($data) ? date('Y-m-d', strtotime($data['time_layhang'])) : date('Y-m-d')}}">--}}
            <input type="date" id="time_pick" class="form-control input_config"
                   value="{{isset($data) ? date('Y-m-d', strtotime($data['created_at'])) : date('Y-m-d')}}">
        </div>
    </div>
    <div class="d-flex justify-content-lg-end align-items-center">
        <button class="btn btn_default border btn-cancel text-bold-700">Huỷ</button>
        <button class="btn btn_main btn-confirm-order ml-2 text-bold-700"
                data-url="{{url('admin/order/confirm_order')}}"
                data-id="{{isset($data) ? $data->id : ''}}"
        >Xác nhận
        </button>
    </div>
</div>


<div class="show-popup-confirm-cancel" style="display: none;">
    <div class="card position-fixed position-center" style="z-index: 200000;">
        <div class="card-body text-center text-bold-700">
            Bạn có muốn huỷ đơn hàng này không?
        </div>
        <div class="d-flex justify-content-end p-1">
            <button class="btn btn_main btn_ok_cancel mr-1 text-bold-700"
                    data-url="{{url('admin/order/confirm_cancel')}}">Đồng ý
            </button>
            <button class="btn btn_sliver btn_cancel_cancel text-bold-700">Huỷ</button>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        let url_filter = window.location.origin;

        // getAll();
        // function filter_all() {
        //     let url = url_filter + '/admin/order/all';
        //     $.ajax({
        //         url: url,
        //         type: "GET",
        //         dataType: 'html',
        //         success: function (data) {
        //             $('.filter-all').addClass('active');
        //             $('.filter-wait-confirm').removeClass('active');
        //             $('.filter-wait-shipping').removeClass('active');
        //             $('.filter-shipping').removeClass('active');
        //             $('.filter-shipped').removeClass('active');
        //             $('.filter-cancel').removeClass('active');
        //             $('.filter-return').removeClass('active');
        //             $("#show_list_order").html(data);
        //         },
        //         error: function (e) {
        //             console.log('error: ' + e);
        //         }
        //     });
        // }

        // filter_all();


        // function filter_wait_confirm() {
        //     let url = url_filter + '/admin/order/wait_confirm';
        //     $.ajax({
        //         url: url,
        //         type: "GET",
        //         dataType: 'html',
        //         success: function (data) {
        //             $('.filter-wait-confirm').addClass('active');
        //             $('.filter-all').removeClass('active');
        //             $('.filter-wait-shipping').removeClass('active');
        //             $('.filter-shipping').removeClass('active');
        //             $('.filter-shipped').removeClass('active');
        //             $('.filter-cancel').removeClass('active');
        //             $('.filter-return').removeClass('active');
        //             $("#show_list_order").html(data);
        //         },
        //         error: function (e) {
        //             console.log('error: ' + e);
        //         }
        //     });
        // }
        //
        // function filter_wait_shipping() {
        //     let url = url_filter + '/admin/order/wait_shipping';
        //     $.ajax({
        //         url: url,
        //         type: "GET",
        //         dataType: 'html',
        //         success: function (data) {
        //             $('.filter-wait-shipping').addClass('active');
        //             $('.filter-wait-confirm').removeClass('active');
        //             $('.filter-all').removeClass('active');
        //             $('.filter-shipping').removeClass('active');
        //             $('.filter-shipped').removeClass('active');
        //             $('.filter-cancel').removeClass('active');
        //             $('.filter-return').removeClass('active');
        //             $("#show_list_order").html(data);
        //         },
        //         error: function (e) {
        //             console.log('error: ' + e);
        //         }
        //     });
        // }
        //
        // function filter_shipping() {
        //     let url = url_filter + '/admin/order/shipping';
        //     $.ajax({
        //         url: url,
        //         type: "GET",
        //         dataType: 'html',
        //         success: function (data) {
        //             $('.filter-shipping').addClass('active');
        //             $('.filter-wait-confirm').removeClass('active');
        //             $('.filter-wait-shipping').removeClass('active');
        //             $('.filter-all').removeClass('active');
        //             $('.filter-shipped').removeClass('active');
        //             $('.filter-cancel').removeClass('active');
        //             $('.filter-return').removeClass('active');
        //             $("#show_list_order").html(data);
        //         },
        //         error: function (e) {
        //             console.log('error: ' + e);
        //         }
        //     });
        // }
        //
        // function filter_shipped() {
        //     let url = url_filter + '/admin/order/shipped'
        //     $.ajax({
        //         url: url,
        //         type: "GET",
        //         dataType: 'html',
        //         success: function (data) {
        //             $('.filter-shipped').addClass('active');
        //             $('.filter-wait-confirm').removeClass('active');
        //             $('.filter-wait-shipping').removeClass('active');
        //             $('.filter-shipping').removeClass('active');
        //             $('.filter-all').removeClass('active');
        //             $('.filter-cancel').removeClass('active');
        //             $('.filter-return').removeClass('active');
        //             $("#show_list_order").html(data);
        //         },
        //         error: function (e) {
        //             console.log('error: ' + e);
        //         }
        //     });
        // }
        //
        // function filter_cancel() {
        //     let url = url_filter + '/admin/order/cancel'
        //     $.ajax({
        //         url: url,
        //         type: "GET",
        //         dataType: 'html',
        //         success: function (data) {
        //             $('.filter-cancel').addClass('active');
        //             $('.filter-wait-confirm').removeClass('active');
        //             $('.filter-wait-shipping').removeClass('active');
        //             $('.filter-shipping').removeClass('active');
        //             $('.filter-shipped').removeClass('active');
        //             $('.filter-all').removeClass('active');
        //             $('.filter-return').removeClass('active');
        //             $("#show_list_order").html(data);
        //         },
        //         error: function (e) {
        //             console.log('error: ' + e);
        //         }
        //     });
        // }
        //
        // function filter_return_money() {
        //     let url = url_filter + '/admin/order/return_money'
        //     $.ajax({
        //         url: url,
        //         type: "GET",
        //         dataType: 'html',
        //         success: function (data) {
        //             $('.filter-return').addClass('active');
        //             $('.filter-wait-confirm').removeClass('active');
        //             $('.filter-wait-shipping').removeClass('active');
        //             $('.filter-shipping').removeClass('active');
        //             $('.filter-shipped').removeClass('active');
        //             $('.filter-cancel').removeClass('active');
        //             $('.filter-all').removeClass('active');
        //             $("#show_list_order").html(data);
        //         },
        //         error: function (e) {
        //             console.log('error: ' + e);
        //         }
        //     });
        // }

        function getAll() {
            let url = url_filter + '/admin/order/all';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $("#show_list_order").html(data);
                },
                error: function (e) {
                    console.log('error: ' + e);
                }
            });
        }

        {{--function fetch_customer_data(query = '') {--}}
        {{--    $.ajax({--}}
        {{--        url: "{{ route('search_order') }}",--}}
        {{--        method: 'GET',--}}
        {{--        data: {query: query},--}}
        {{--        dataType: 'json',--}}
        {{--        success: function (data) {--}}
        {{--            $('tbody').html(data.table_data);--}}
        {{--            $('#count_order').html(data.total_data + ' đơn hàng')--}}
        {{--        }--}}
        {{--    });--}}
        {{--}--}}

        {{--$(document).on('keyup', '#search', function () {--}}
        {{--    let query = $(this).val();--}}
        {{--    fetch_customer_data(query);--}}
        {{--});--}}

        // function getData() {
        //     let status = $('#status').val();
        //     // $.get('/admin/order/filter/' + status, function (data) {
        //     //     $('#get').html(data);
        //     // })
        //     $.ajax({
        //         url: "/admin/order/filter/" + status,
        //         method: 'GET',
        //         data: {status: status},
        //         dataType: 'json',
        //         success: function (data) {
        //             $('tbody').html(data.table_data);
        //             $('#count_order').html(data.total_data + ' đơn hàng')
        //         }
        //     })
        // }
        //
        // $("#status").on('change', function () {
        //     getData();
        //     let st = $(this).val();
        //     if (st == 0) {
        //         $('.btn-confirm-order').show();
        //     } else {
        //         $('.btn-confirm-order').hide();
        //     }
        // });

        // $(document).on('click', '.btn-confirm-order', function () {
        //     let url = $(this).attr('data-url');
        //     let data = {};
        //     $.ajaxSetup({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         }
        //     });
        //     $.ajax({
        //         url: url,
        //         type: "POST",
        //         data: data,
        //         dataType: 'json',
        //         contentType: false,
        //         processData: false,
        //         success: function (data) {
        //             if (data.status) {
        //                 swal("Thông báo", data.msg, "success");
        //                 window.location.href = data.url;
        //             } else {
        //                 alert(data.msg);
        //             }
        //         },
        //         error: function () {
        //             console.log('error')
        //         },
        //     });
        // });

        function filter() {
            let url = $('.btn-filter-order').attr('data-url');
            let data = {};
            data['time_start'] = $('.time-start').val();
            data['time_stop'] = $('.time-stop').val();
            if (data['time_start'] == '' || data['time_stop'] == '' || (data['time_start'] > data['time_stop'])) {
                swal("Thông báo", "Vui lòng chọn khoảng thời gian phù hợp", "error");
                return false;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: data,
                success: function (data) {
                    $('tbody').html(data.table_data);
                    $('#count_order').html(data.total_data + ' đơn hàng')
                },
                error: function () {
                    console.log('error')
                },
            });
        }

        $(document).on('click', '.btn-filter-order', function () {
            filter();
        });

        //filter status

        //all
        // $(document).on('click', '.filter-all', function () {
        //     filter_all();
        // });
        //
        // //chờ xác nhận
        // $(document).on('click', '.filter-wait-confirm', function () {
        //     filter_wait_confirm();
        // });
        //
        // //chờ lấy hàng
        // $(document).on('click', '.filter-wait-shipping', function () {
        //     filter_wait_shipping();
        // });
        //
        // //đang giao hàng
        // $(document).on('click', '.filter-shipping', function () {
        //     filter_shipping();
        // });
        //
        // //đã giao hàng
        // $(document).on('click', '.filter-shipped', function () {
        //     filter_shipped();
        // });
        //
        // //đã huỷ
        // $(document).on('click', '.filter-cancel', function () {
        //     filter_cancel();
        // });
        //
        // //Hoàn tiền
        // $(document).on('click', '.filter-return', function () {
        //     filter_return_money();
        // });

        //show popup xác nhận đơn hàng // chọn hình thức giao hàng
        $(document).on('click', '.btn-show-confirm', function () {
            let url = $(this).attr('data-url');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.show-popup-form-shipping').html(data);
                    $('.popup-wait-shipping').addClass('show-popup');
                    $('#overlay1').addClass('open');
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //confirm Chọn hình thức lấy hàng (status: chờ lấy hàng)
        $(document).on('click', '.btn-confirm-wait-shipping', function () {
            let form_shipping = $('#form_shipping:checked').val();
            let order_id = $(this).attr('data-id');
            if (!form_shipping) {
                alert('Vui lòng chọn hình thức phù hợp !');
                return false;
            }
            if (form_shipping === '0') {
                alert('Chức năng đang phát triển !');
            } else if (form_shipping === '1') {
                $('.btn-confirm-order').attr('data-form-shipping', form_shipping);
                $('.btn-confirm-order').attr('data-id', order_id);
                $('.popup-confirm-order').addClass('show-popup');
            }
        });

        //confirm trạng thái chờ xác nhận khi đã chọn hình thức lấy hàng và thời gian lấy hàng kho hàng
        $(document).on('click', '.btn-confirm-order', function () {
            let address_pick = $('#address_pick').val();
            let time_pick = $('#time_pick').val();
            let id = $(this).attr('data-id');
            let url = $(this).attr('data-url');
            let form_shipping = $(this).attr('data-form-shipping');
            let form_data = new FormData();
            form_data.append('id', id);
            form_data.append('address_pick', address_pick);
            form_data.append('time_pick', time_pick);
            form_data.append('form_shipping', form_shipping);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        alert(data.msg);
                        location.reload();
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });

        $(document).on('click', '.btn-cancel', function () {
            $('.popup-confirm-order').removeClass('show-popup');
        });

        $(document).on('click', '.btn-cancel-wait-shipping', function () {
            $('.popup-wait-shipping').removeClass('show-popup');
            $('#overlay1').removeClass('open');
        });

        //Xác nhận huỷ đơn cho khách
        $(document).on('click', '.btn_confirm_cancel', function () {
            let id = $(this).attr('data-id');
            $('.show-popup-confirm-cancel').show();
            $('#overlay1').addClass('open');
            $('.btn_ok_cancel').attr('data-id', id);
        });

        $(document).on('click', '.btn_ok_cancel', function () {
            let url = $(this).attr('data-url');
            let id = $(this).attr('data-id');
            let form_data = new FormData();
            form_data.append('id', id);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        $('.show-popup-confirm-cancel').hide();
                        $('#overlay1').removeClass('open');
                        swal("Thông báo", data.msg, "success");
                        location.reload();
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
            });

        });

        $(document).on('click', '.btn_cancel_cancel', function () {
            $('.show-popup-confirm-cancel').hide();
            $('#overlay1').removeClass('open');
        });

    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
