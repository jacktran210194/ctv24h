<div class="position-fixed position-center popup-wait-shipping p-2" style="z-index: 500001">
    <div class="header-popups">
        <div class="d-flex align-items-center">
            <h3 class="text-bold-700 text-blue text-bold-700">Giao đơn hàng</h3>
            <img class="ml-5" width="105px" height="35px" src="https://tuyencongnhan.vn/uploads/images/20-10-17-09/image_2020_10_15T12_43_10_022Z-64.png" alt="">
        </div>
    </div>
    <div class="popup-content">
        <div class="d-flex align-items-center mt-2">
            <label class="container_radio">
                <input type="radio" name="form_shipping" id="form_shipping" value="0">
                <span class="radio_config"></span>
            </label>
            <img class="mr-2" width="60px" height="60px" src="../../assets/admin/app-assets/images/icon_dashboard/giaohang_1.png" alt="">
            <div>
                <h4 class="text-bold-700">Tôi sẽ tự mang đến bưu cục</h4>
                <p class="m-0">Bạn có thể gửi tới bất cứ bưu cục giao hàng nhanh nào thuộc cùng tỉnh hoặc thành phố</p>
            </div>
        </div>
        <div class="d-flex align-items-center mt-2">
            <label class="container_radio">
                <input type="radio" name="form_shipping" id="form_shipping" value="1">
                <span class="radio_config"></span>
            </label>
            <img class="mr-2" width="60px" height="60px" src="../../assets/admin/app-assets/images/icon_dashboard/giaohang_2.png" alt="">
            <div>
                <h4 class="text-bold-700">Đơn vị vận chuyển tới lấy hàng</h4>
                <p class="m-0">Giao hàng nhanh sẽ đến lấy hàng theo địa chỉ nhận hàng mà bạn đã xác nhận.</p>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-lg-end align-items-center mt-2">
        <button class="btn btn_default border btn-cancel-wait-shipping text-bold-700">Huỷ</button>
        <button class="btn btn_main btn-confirm-wait-shipping ml-2 text-bold-700"
                data-id="{{$data->id}}"
                data-url="{{url('admin/order/confirm_wait_shipping')}}"
        >Xác nhận
        </button>
    </div>
</div>
