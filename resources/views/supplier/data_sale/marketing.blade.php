<div class="mg-top-30 p-2 bg-white">
    <ul class="d-flex border_box border-radius-4 p-0 content-overview">
        <li class="text-center overview font-weight-bold font-size-14 d-flex align-items-center justify-content-lg-center active" style="height: 40px; padding: 0 20px">
           <a href="{{url('/admin/data_sale/marketing')}}">Chương trình khuyến mãi</a>
        </li>
        <li class="text-center overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="height: 40px; padding: 0 20px">
            <a href="{{url('/admin/data_sale/combo-sale')}}">Combo khuyến mãi</a>
        </li>
        <li class="text-center overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="height: 40px; padding: 0 20px">
            <a href="{{url('/admin/data_sale/follower-offers')}}"> Ưu đãi follower</a>
        </li>
        <li class="text-center overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="height: 40px; padding: 0 20px">
            <a href="{{url('/admin/data_sale/voucher')}}">Voucher</a>
        </li>
        <li class="text-center overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="height: 40px; padding: 0 20px">
            <a href="{{url('/admin/data_sale/flash-sale')}}">Flash Sale của Shop</a>
        </li>
        <li class="text-center overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="height: 40px; padding: 0 20px">
            <a href="{{url('admin/data_sale/deal-shock')}}">Mua kèm deal sốc</a>
        </li>
    </ul>
    <h3 class="font-weight-bold font-size-17 color-black mg-top-30">Chỉ số quan trọng</h3>
    <div class="d-flex justify-content-sm-between mg-top-30" style="margin-right: -20px">
        <div class="border-radius-16 bg-1 box-data dv-ct">
            <div class="d-flex title-box">
                <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Doanh thu</h3>
                <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
            </div>
            <div>
                <p class=" font-weight-bold font-size-36 color-white mg-top-20">200.000đ</p>
                <div class="d-flex justify-content-sm-between align-items-center mg-top-20">
                    <p class="font-size-12 color-white" style="margin: 0;">Tăng trưởng so với hôm qua</p>
                    <div>
                        <img src="../../assets/admin/app-assets/images/icons/trendin-up.png" style="max-width: 25px;margin-bottom: 5px">
                        <p class="font-weight-bold font-size-14 color-white" style="margin: 0;">83.33%</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-radius-16 bg-2 box-data dv-ct">
            <div class="d-flex title-box">
                <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Đã bán</h3>
                <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
            </div>
            <div>
                <p class=" font-weight-bold font-size-36 color-white mg-top-20">2</p>
                <div class="d-flex justify-content-sm-between align-items-center mg-top-20">
                    <p class="font-size-12 color-white" style="margin: 0;">Tăng trưởng so với hôm qua</p>
                    <div>
                        <img src="../../assets/admin/app-assets/images/icons/trendin-up.png" style="max-width: 25px;margin-bottom: 5px">
                        <p class="font-weight-bold font-size-14 color-white" style="margin: 0;">83.33%</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-radius-16 bg-3 box-data dv-ct">
            <div class="d-flex title-box">
                <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Đơn hàng</h3>
                <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
            </div>
            <div>
                <p class=" font-weight-bold font-size-36 color-white mg-top-20">2</p>
                <div class="d-flex justify-content-sm-between align-items-center mg-top-20">
                    <p class="font-size-12 color-white" style="margin: 0;">Tăng trưởng so với hôm qua</p>
                    <div>
                        <img src="../../assets/admin/app-assets/images/icons/trendin-up.png" style="max-width: 25px;margin-bottom: 5px">
                        <p class="font-weight-bold font-size-14 color-white" style="margin: 0;">83.33%</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-radius-16 bg-4 box-data dv-ct">
            <div class="d-flex title-box">
                <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Người mua</h3>
                <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
            </div>
            <div>
                <p class=" font-weight-bold font-size-36 color-white mg-top-20">2</p>
                <div class="d-flex justify-content-sm-between align-items-center mg-top-20">
                    <p class="font-size-12 color-white" style="margin: 0;">Tăng trưởng so với hôm qua</p>
                    <div>
                        <img src="../../assets/admin/app-assets/images/icons/trendin-up.png" style="max-width: 25px;margin-bottom: 5px">
                        <p class="font-weight-bold font-size-14 color-white" style="margin: 0;">83.33%</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-radius-16 bg-5 box-data dv-ct">
            <div class="d-flex title-box">
                <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Doanh thu /Khách</h3>
                <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
            </div>
            <div>
                <p class=" font-weight-bold font-size-36 color-white mg-top-20">20.000đ</p>
                <div class="d-flex justify-content-sm-between align-items-center mg-top-20">
                    <p class="font-size-12 color-white" style="margin: 0;">Tăng trưởng so với hôm qua</p>
                    <div>
                        <img src="../../assets/admin/app-assets/images/icons/trendin-up.png" style="max-width: 25px;margin-bottom: 5px">
                        <p class="font-weight-bold font-size-14 color-white" style="margin: 0;">83.33%</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mg-top-30">
        <img src="../../assets/admin/app-assets/images/icons/graph.png" style="width: 100%">
    </div>
</div>
<div class="mg-top-30 bg-white p-2">
    <div class="d-flex justify-content-sm-between">
        <h3 class="font-weight-bold font-size-17 color-black">Tổng quan</h3>
        <div class="d-flex align-items-center">
            <button class="d-flex align-items-center border-radius-8 bg-white group-kg" style="padding: 10px 20px; margin-right: 20px">
                <p class="m-0">Tất cả</p>
                <img src="../../assets/admin/app-assets/images/icons/Polygon.png" style="margin-left: 10px">
            </button>
            <div class="d-flex align-items-center border-radius-8 bg-white group-kg" style="padding: 10px 20px; margin-right: 20px">
                <input type="text" style="width: 150px; height: 24px" class="border-0 out-line-none" placeholder="Tìm kiếm">
                <label class="m-0"><img src="../../assets/admin/app-assets/images/icons/search.png" style="margin-left: 10px"></label>
            </div>
        </div>
    </div>
    <div class="mg-top-30 border-radius-8 bg-red color-white font-weight-bold d-flex">
        <p class="m-0 w-25 d-flex align-items-center " style="padding-left: 15px">  Tên chương trình</p>
        <div class="m-0 w-20 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Số lượng sản phẩm đã bán</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="m-0 w-10 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Đơn hàng</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="m-0 w-10 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Người mua</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="m-0 w-10 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Doanh thu</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="m-0 w-20 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Doanh thu/ mỗi người mua</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="w-5"></div>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 d-flex align-items-center w-25 font-weight-bold font-size-14" style="padding-left: 15px">Phiếu ưu đãi Grab Express </p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >10.000</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >5</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >20</p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >100.000 đ</p>
        <p class="m-0 w-5 font-size-14 text-center font-weight-bold color-blue text-decoration" style="padding-right: 10px" >Chi tiết</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 d-flex align-items-center w-25 font-weight-bold font-size-14" style="padding-left: 15px">Phiếu ưu đãi Grab Express </p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >10.000</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >5</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >20</p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >100.000 đ</p>
        <p class="m-0 w-5 font-size-14 text-center font-weight-bold color-blue text-decoration" style="padding-right: 10px" >Chi tiết</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 d-flex align-items-center w-25 font-weight-bold font-size-14" style="padding-left: 15px">Phiếu ưu đãi Grab Express </p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >10.000</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >5</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >20</p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >100.000 đ</p>
        <p class="m-0 w-5 font-size-14 text-center font-weight-bold color-blue text-decoration" style="padding-right: 10px" >Chi tiết</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 d-flex align-items-center w-25 font-weight-bold font-size-14" style="padding-left: 15px">Phiếu ưu đãi Grab Express </p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >10.000</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >5</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >20</p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >100.000 đ</p>
        <p class="m-0 w-5 font-size-14 text-center font-weight-bold color-blue text-decoration" style="padding-right: 10px" >Chi tiết</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 d-flex align-items-center w-25 font-weight-bold font-size-14" style="padding-left: 15px">Phiếu ưu đãi Grab Express </p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >10.000</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >5</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >20</p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >100.000 đ</p>
        <p class="m-0 w-5 font-size-14 text-center font-weight-bold color-blue text-decoration" style="padding-right: 10px" >Chi tiết</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 d-flex align-items-center w-25 font-weight-bold font-size-14" style="padding-left: 15px">Phiếu ưu đãi Grab Express </p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >10.000</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >5</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >20</p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >100.000 đ</p>
        <p class="m-0 w-5 font-size-14 text-center font-weight-bold color-blue text-decoration" style="padding-right: 10px" >Chi tiết</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 d-flex align-items-center w-25 font-weight-bold font-size-14" style="padding-left: 15px">Phiếu ưu đãi Grab Express </p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >10.000</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >5</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >20</p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >100.000 đ</p>
        <p class="m-0 w-5 font-size-14 text-center font-weight-bold color-blue text-decoration" style="padding-right: 10px" >Chi tiết</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 d-flex align-items-center w-25 font-weight-bold font-size-14" style="padding-left: 15px">Phiếu ưu đãi Grab Express </p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >10.000</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >5</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >20</p>
        <p class="m-0 w-20 font-size-14 text-center font-weight-bold" >100.000 đ</p>
        <p class="m-0 w-5 font-size-14 text-center font-weight-bold color-blue text-decoration" style="padding-right: 10px" >Chi tiết</p>
    </div>
    <div class="d-flex align-items-center justify-content-lg-center w-100 mg-top-30">
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
            <img src="../../assets/admin/app-assets/images/icons/prev.png">
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            1
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13 active">
            2
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            3
        </button>
        <p class="font-weight-bold font-size-13" style="margin-right: 20px;color: #A2A6B0; margin-bottom: 0">
            ...
        </p>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            15
        </button>

        <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
            <img src="../../assets/admin/app-assets/images/icons/next.png">
        </button>
    </div>
</div>
