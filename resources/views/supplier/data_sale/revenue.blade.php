<div class="mg-top-30 p-2 bg-white">
    <h3 class="font-weight-bold font-size-24 color-black mg-top-30">Tổng quan về doanh thu</h3>
    <div class="d-flex bg-white pr-container">
        <div class="mg-top-40 dv-revenue position-relative">
            <div class="border-top ct-revenue mg-top-20" >
                <div class="d-flex mg-top-20 align-items-center">
                    <p class="m-0 font-size-17 color-black" style="padding-right: 20px">Số lượt truy cập</p>
                    <img src="../../assets/admin/app-assets/images/icons/icon_danger.png" width="25px" height="25px">
                </div>
                <p class="m-0 font-weight-bold font-size-36 color-black" style="padding: 10px 0">2</p>
                <div class="mg-top-10 d-flex align-items-center" style="margin-bottom: 40px">
                    <p class="m-0 font-size-14 color-gray">
                        <span>so với 00:00-15:00 hôm qua</span>
                        <span class="font-weight-bold color-black" style="margin-left: 20px">83.33%</span>
                    </p>
                    <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                </div>
            </div>
            <div class="border-top ct-revenue d-flex align-items-center" >
                <div>
                    <div class="d-flex mg-top-20 align-items-center">
                        <p class="m-0 font-size-17 color-black" style="padding-right: 20px">Người mua</p>
                        <img src="../../assets/admin/app-assets/images/icons/icon_danger.png" width="25px" height="25px">
                    </div>
                    <p class="m-0 font-weight-bold font-size-36 color-black" style="padding: 10px 0">2</p>
                    <div class="mg-top-10 d-flex align-items-center" style="margin-bottom: 40px">
                        <p class="m-0 font-size-14 color-gray">
                            <span>so với 00:00-15:00 hôm qua</span>
                            <span class="font-weight-bold color-black" style="margin-left: 20px">83.33%</span>
                        </p>
                        <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                    </div>
                </div>
                <div style="padding-left: 30px">
                    <div class="d-flex mg-top-20 align-items-center">
                        <p class="m-0 font-size-17 color-black" style="padding-right: 20px">Doanh thu</p>
                        <img src="../../assets/admin/app-assets/images/icons/icon_danger.png" width="25px" height="25px">
                    </div>
                    <p class="m-0 font-weight-bold font-size-36 color-black" style="padding: 10px 0">2.000.000đ</p>
                    <div class="mg-top-10 d-flex align-items-center" style="margin-bottom: 40px">
                        <p class="m-0 font-size-14 color-gray">
                            <span>so với 00:00-15:00 hôm qua</span>
                            <span class="font-weight-bold color-black" style="margin-left: 20px">83.33%</span>
                        </p>
                        <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                    </div>
                </div>
            </div>
            <div class="border-top ct-revenue d-flex align-items-center border-bottom">
                <div>
                    <div class="d-flex mg-top-20 align-items-center">
                        <p class="m-0 font-size-17 color-black" style="padding-right: 20px">Người mua</p>
                        <img src="../../assets/admin/app-assets/images/icons/icon_danger.png" width="25px" height="25px">
                    </div>
                    <p class="m-0 font-weight-bold font-size-36 color-black" style="padding: 10px 0">2</p>
                    <div class="mg-top-10 d-flex align-items-center" style="margin-bottom: 40px">
                        <p class="m-0 font-size-14 color-gray">
                            <span>so với 00:00-15:00 hôm qua</span>
                            <span class="font-weight-bold color-black" style="margin-left: 20px">83.33%</span>
                        </p>
                        <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                    </div>
                </div>
                <div style="padding-left: 30px">
                    <div class="d-flex mg-top-20 align-items-center">
                        <p class="m-0 font-size-17 color-black" style="padding-right: 20px">Doanh thu</p>
                        <img src="../../assets/admin/app-assets/images/icons/icon_danger.png" width="25px" height="25px">
                    </div>
                    <p class="m-0 font-weight-bold font-size-36 color-black" style="padding: 10px 0">2.000.000đ</p>
                    <div class="mg-top-10 d-flex align-items-center" style="margin-bottom: 40px">
                        <p class="m-0 font-size-14 color-gray">
                            <span>so với 00:00-15:00 hôm qua</span>
                            <span class="font-weight-bold color-black" style="margin-left: 20px">83.33%</span>
                        </p>
                        <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                    </div>
                </div>
                <div style="padding-left: 30px">
                    <div class="d-flex mg-top-20 align-items-center">
                        <p class="m-0 font-size-17 color-black" style="padding-right: 20px">Doanh thu trên mỗi người</p>
                        <img src="../../assets/admin/app-assets/images/icons/icon_danger.png" width="25px" height="25px">
                    </div>
                    <p class="m-0 font-weight-bold font-size-36 color-black" style="padding: 10px 0">200.000đ</p>
                    <div class="mg-top-10 d-flex align-items-center" style="margin-bottom: 40px">
                        <p class="m-0 font-size-14 color-gray">
                            <span>so với 00:00-15:00 hôm qua</span>
                            <span class="font-weight-bold color-black" style="margin-left: 20px">83.33%</span>
                        </p>
                        <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                    </div>
                </div>
            </div>
            <div class="d-flex bg-white position-absolute h-100 pr-3d" style="right: 310px;">
                <div class="ct-3d d-flex flex-column">
                    <div class="ct-dv-1 h-3 position-relative bg-ct-1 d-flex justify-content-lg-center align-items-center w-100">
                        <div>
                            <div class="d-flex justify-content-lg-center w-100">
                                <img src="../../assets/admin/app-assets/images/icons/Group_16690.png">
                            </div>
                            <p class="font-weight-bold mg-top-20 font-size-17 color-white text-center">Lượt truy cập</p>
                        </div>
                    </div>
                    <div class="ct-dv-1 h-3 position-relative bg-ct-2 d-flex justify-content-lg-center align-items-center">
                        <div>
                            <div class="d-flex justify-content-lg-center w-100">
                                <img src="../../assets/admin/app-assets/images/icons/report.png">
                            </div>
                            <p class="font-weight-bold mg-top-20 font-size-17 color-white text-center">Tất cả đơn</p>
                        </div>
                    </div>
                    <div class="ct-dv-1 h-3 position-relative bg-ct-3 d-flex justify-content-lg-center align-items-center">
                        <div>
                            <div class="d-flex justify-content-lg-center w-100">
                                <img src="../../assets/admin/app-assets/images/icons/wallet-90.png">
                            </div>
                            <p class="font-weight-bold mg-top-20 font-size-17 color-white text-center">Đã xác nhận</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-2 d-flex position-absolute ts-container bg-white" style="width: 320px">
                <div class="ts-c">
                    <div class="bd-content">
                        <div class="d-flex align-items-center h-50">
                            <div class="sub-bda arrow-1"></div>
                            <div class="sub-ct p-1">
                                <p class="m-0 font-size-14 color-gray">Tỉ lệ chuyển đổi( Lượt truy cập - Đơn đã xác nhận)
                                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M9.0013 1.91797C5.09547 1.91797 1.91797 5.09547 1.91797 9.0013C1.91797 12.9071 5.09547 16.0846 9.0013 16.0846C12.9071 16.0846 16.0846 12.9071 16.0846 9.0013C16.0846 5.09547 12.9071 1.91797 9.0013 1.91797ZM9.0013 17.3346C4.4063 17.3346 0.667969 13.5963 0.667969 9.0013C0.667969 4.4063 4.4063 0.667969 9.0013 0.667969C13.5963 0.667969 17.3346 4.4063 17.3346 9.0013C17.3346 13.5963 13.5963 17.3346 9.0013 17.3346Z" fill="#8C8C8C"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.99609 10.1473C8.65109 10.1473 8.37109 9.86734 8.37109 9.52234V5.83984C8.37109 5.49484 8.65109 5.21484 8.99609 5.21484C9.34109 5.21484 9.62109 5.49484 9.62109 5.83984V9.52234C9.62109 9.86734 9.34109 10.1473 8.99609 10.1473Z" fill="#8C8C8C"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M9.00547 12.9987C8.54464 12.9987 8.16797 12.6262 8.16797 12.1654C8.16797 11.7045 8.53714 11.332 8.99714 11.332H9.00547C9.4663 11.332 9.8388 11.7045 9.8388 12.1654C9.8388 12.6262 9.4663 12.9987 9.00547 12.9987Z" fill="#8C8C8C"/>
                                    </svg>
                                </p>
                                <p class="m-0 font-weight-bold font-size-17 color-blue" style="padding-top: 15px">0.00%</p>
                            </div>
                        </div>
                        <div class="d-flex align-items-center h-50">
                            <div class="sub-bda arrow-2"></div>
                            <div class="sub-ct p-1">
                                <p class="m-0 font-size-14 color-gray">Tỉ lệ chuyển đổi( Lượt truy cập - Đơn đã xác nhận)
                                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M9.0013 1.91797C5.09547 1.91797 1.91797 5.09547 1.91797 9.0013C1.91797 12.9071 5.09547 16.0846 9.0013 16.0846C12.9071 16.0846 16.0846 12.9071 16.0846 9.0013C16.0846 5.09547 12.9071 1.91797 9.0013 1.91797ZM9.0013 17.3346C4.4063 17.3346 0.667969 13.5963 0.667969 9.0013C0.667969 4.4063 4.4063 0.667969 9.0013 0.667969C13.5963 0.667969 17.3346 4.4063 17.3346 9.0013C17.3346 13.5963 13.5963 17.3346 9.0013 17.3346Z" fill="#8C8C8C"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.99609 10.1473C8.65109 10.1473 8.37109 9.86734 8.37109 9.52234V5.83984C8.37109 5.49484 8.65109 5.21484 8.99609 5.21484C9.34109 5.21484 9.62109 5.49484 9.62109 5.83984V9.52234C9.62109 9.86734 9.34109 10.1473 8.99609 10.1473Z" fill="#8C8C8C"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M9.00547 12.9987C8.54464 12.9987 8.16797 12.6262 8.16797 12.1654C8.16797 11.7045 8.53714 11.332 8.99714 11.332H9.00547C9.4663 11.332 9.8388 11.7045 9.8388 12.1654C9.8388 12.6262 9.4663 12.9987 9.00547 12.9987Z" fill="#8C8C8C"/>
                                    </svg>
                                </p>
                                <p class="m-0 font-weight-bold font-size-17 color-blue" style="padding-top: 15px">0.00%</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <p class="m-0 font-size-14 color-gray">Tỉ lệ chuyển đổi( Lượt truy cập - Đơn đã xác nhận)
                            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.0013 1.91797C5.09547 1.91797 1.91797 5.09547 1.91797 9.0013C1.91797 12.9071 5.09547 16.0846 9.0013 16.0846C12.9071 16.0846 16.0846 12.9071 16.0846 9.0013C16.0846 5.09547 12.9071 1.91797 9.0013 1.91797ZM9.0013 17.3346C4.4063 17.3346 0.667969 13.5963 0.667969 9.0013C0.667969 4.4063 4.4063 0.667969 9.0013 0.667969C13.5963 0.667969 17.3346 4.4063 17.3346 9.0013C17.3346 13.5963 13.5963 17.3346 9.0013 17.3346Z" fill="#8C8C8C"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M8.99609 10.1473C8.65109 10.1473 8.37109 9.86734 8.37109 9.52234V5.83984C8.37109 5.49484 8.65109 5.21484 8.99609 5.21484C9.34109 5.21484 9.62109 5.49484 9.62109 5.83984V9.52234C9.62109 9.86734 9.34109 10.1473 8.99609 10.1473Z" fill="#8C8C8C"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.00547 12.9987C8.54464 12.9987 8.16797 12.6262 8.16797 12.1654C8.16797 11.7045 8.53714 11.332 8.99714 11.332H9.00547C9.4663 11.332 9.8388 11.7045 9.8388 12.1654C9.8388 12.6262 9.4663 12.9987 9.00547 12.9987Z" fill="#8C8C8C"/>
                            </svg>
                        </p>
                        <p class="font-weight-bold font-size-17 color-red">0.00%</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mg-top-30 p-2 bg-white">
    <div class="d-flex justify-content-sm-between align-items-center">
        <h2 class="font-size-24 font-weight-bold">Xu hướng số liệu</h2>
        <p class="m-0 font-size-13">Đã chọn <span class="color-red">2</span>/4</p>
    </div>
    <div class="mg-top-40">
        <div class="d-flex align-items-center">
            <div class="w-20">
                <p class="font-weight-bold font-size-17 m-0" style="padding:20px 0">Lượt truy cập: </p>
            </div>
            <div class="w-80 d-flex flex-wrap">
                <div class="d-flex align-items-center" style="padding: 10px">
                    <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Lượt truy cập:</p>
                    <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                </div>
            </div>
        </div>
        <div class="d-flex align-items-lg-start mg-top-20">
            <div class="w-20">
                <p class="font-weight-bold font-size-17 m-0" style="padding:20px 0">Lượt truy cập: </p>
            </div>
            <div class="w-80 d-flex">
                <div class="d-flex align-items-center w-100 flex-wrap">
                    <div class="w-25 d-flex align-items-center" style="padding: 10px">
                        <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Người mua</p>
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                    </div>
                    <div class="w-25 d-flex align-items-center" style="padding: 10px">
                        <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Sản phẩm</p>
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                    </div>
                    <div class="w-25 d-flex align-items-center" style="padding: 10px">
                        <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Đơn hàng</p>
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                    </div>
                    <div class="w-25 d-flex align-items-center" style="padding: 10px">
                        <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Doanh thu</p>
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                    </div>
                    <div class="w-25 d-flex align-items-center" style="padding: 10px">
                        <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Tỉ lệ chuyển đổi ( Lượt truy cập - Đơn đã xác nhận)</p>
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex align-items-lg-start mg-top-20">
            <div class="w-20">
                <p class="font-weight-bold font-size-17 m-0" style="padding:20px 0">Lượt truy cập: </p>
            </div>
            <div class="w-80 d-flex">
                <div class="d-flex align-items-center w-100 flex-wrap">
                    <div class="w-25 d-flex align-items-center" style="padding: 10px">
                        <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Người mua</p>
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                    </div>
                    <div class="w-25 d-flex align-items-center" style="padding: 10px">
                        <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Sản phẩm</p>
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                    </div>
                    <div class="w-25 d-flex align-items-center" style="padding: 10px">
                        <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Đơn hàng</p>
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                    </div>
                    <div class="w-25 d-flex align-items-center" style="padding: 10px">
                        <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Doanh thu</p>
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                    </div>
                    <div class="w-25 d-flex align-items-center" style="padding: 10px">
                        <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Doanh thu trên mỗi người mua</p>
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                    </div>
                    <div class="w-25 d-flex align-items-center" style="padding: 10px">
                        <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Tỉ lệ chuyển đổi ( Lượt truy cập - Đơn đã xác nhận)</p>
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                    </div>
                    <div class="w-25 d-flex align-items-center" style="padding: 10px">
                        <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Tỉ lê chuyển đổi ( Đơn được đặt - Đơn đã xác nhận)</p>
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                    </div>
                    <div class="w-25 d-flex align-items-center" style="padding: 10px">
                        <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Lượt truy cập:</p>
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mg-top-30">
        <img src="../../assets/admin/app-assets/images/icons/graph.png" style="width: 100%">
    </div>
</div>
