<h3 class="color-black font-weight-bold font-size-24 mg-top-30">
    <span class="color-red">1</span> sản phẩm nên được bổ sung thông tin ở phần mô tả sản phẩm để người mua hiểu rõ hơn về sản phẩm của Shop.

</h3>
<div class="mg-top-30 border-radius-8 bg-red color-white font-weight-bold d-flex">
    <p class="m-0 w-30 d-flex align-items-center position-relative bf-input" style="padding-left: 50px"> Sản phẩm</p>
    <div class="m-0 w-20 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
        <p class="m-0">Lần cuối cập nhật lúc</p>
        <div class="d-flex align-items-center" style="padding-left: 5px">
            <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
            <div class="d-flex flex-column">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
    </div>
    <div class="m-0 w-35 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
        <p class="m-0">Độ dài hiện tại/Đề xuất</p>
        <div class="d-flex flex-column" style="padding-left: 5px">
            <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
            <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
        </div>
    </div>
    <div class="m-0 w-15 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
        <p class="m-0">Thao tác</p>
    </div>
</div>
<div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 15px 0">
    <div class="d-flex align-items-center w-30 bf-input position-relative bf-gray" style="padding-left: 50px">
        <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
        <div>
            <div class="d-flex align-items-center justify-content-sm-between border-radius-8" style="background: rgba(255, 195, 3, 0.1); padding: 5px 10px; margin-bottom: 8px">
                <img src="../../assets/admin/app-assets/images/icons/Danger_Circle_1.png">
                <p class="m-0 font-size-12" style="color: #FFC303; padding-left: 15px">Những mục cần cải thiện</p>
            </div>
            <p class="m-0 font-weight-bold color-black">Giỏ đựng đồ rửa bát</p>
            <p class="m-0 font-weight-bold color-red">DQG3BE</p>
        </div>
    </div>
    <div class="m-0 w-20 d-flex text-center font-weight-bold justify-content-lg-center align-items-center"><p class="m-0">17:32 <br> 31-05-2021</p></div>
    <div class="m-0 w-35 d-flex align-items-center justify-content-sm-between font-weight-bold">
        <p class="m-0 w-90 d-flex align-items-center border-radius-8 position-relative bf-dv w-6" style="background: #C7C7C7; height: 9px"></p>
        <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">19/30</p>
    </div>
    <div class="d-flex justify-content-lg-center align-items-center w-15">
        <div>
            <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                <img src="../../assets/admin/app-assets/images/icons/edit.png">
                <span style="margin-left: 5px">Sửa</span>
            </div>
            <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                <img src="../../assets/admin/app-assets/images/icons/del.png">
                <span>Bỏ qua</span>
            </div>
        </div>
    </div>
</div>
<div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 15px 0">
    <div class="d-flex align-items-center w-30 bf-input position-relative bf-gray" style="padding-left: 50px">
        <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
        <div>
            <div class="d-flex align-items-center justify-content-sm-between border-radius-8" style="background: rgba(255, 195, 3, 0.1); padding: 5px 10px; margin-bottom: 8px">
                <img src="../../assets/admin/app-assets/images/icons/Danger_Circle_1.png">
                <p class="m-0 font-size-12" style="color: #FFC303; padding-left: 15px">Những mục cần cải thiện</p>
            </div>
            <p class="m-0 font-weight-bold color-black">Giỏ đựng đồ rửa bát</p>
            <p class="m-0 font-weight-bold color-red">DQG3BE</p>
        </div>
    </div>
    <div class="m-0 w-20 d-flex text-center font-weight-bold justify-content-lg-center align-items-center"><p class="m-0">17:32 <br> 31-05-2021</p></div>
    <div class="m-0 w-35 d-flex align-items-center justify-content-sm-between font-weight-bold">
        <p class="m-0 w-90 d-flex align-items-center border-radius-8 position-relative bf-dv w-6" style="background: #C7C7C7; height: 9px"></p>
        <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">19/30</p>
    </div>
    <div class="d-flex justify-content-lg-center align-items-center w-15">
        <div>
            <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                <img src="../../assets/admin/app-assets/images/icons/edit.png">
                <span style="margin-left: 5px">Sửa</span>
            </div>
            <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                <img src="../../assets/admin/app-assets/images/icons/del.png">
                <span>Bỏ qua</span>
            </div>
        </div>
    </div>
</div>
<div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 15px 0">
    <div class="d-flex align-items-center w-30 bf-input position-relative bf-gray" style="padding-left: 50px">
        <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
        <div>
            <div class="d-flex align-items-center justify-content-sm-between border-radius-8" style="background: rgba(255, 195, 3, 0.1); padding: 5px 10px; margin-bottom: 8px">
                <img src="../../assets/admin/app-assets/images/icons/Danger_Circle_1.png">
                <p class="m-0 font-size-12" style="color: #FFC303; padding-left: 15px">Những mục cần cải thiện</p>
            </div>
            <p class="m-0 font-weight-bold color-black">Giỏ đựng đồ rửa bát</p>
            <p class="m-0 font-weight-bold color-red">DQG3BE</p>
        </div>
    </div>
    <div class="m-0 w-20 d-flex text-center font-weight-bold justify-content-lg-center align-items-center"><p class="m-0">17:32 <br> 31-05-2021</p></div>
    <div class="m-0 w-35 d-flex align-items-center justify-content-sm-between font-weight-bold">
        <p class="m-0 w-90 d-flex align-items-center border-radius-8 position-relative bf-dv w-6" style="background: #C7C7C7; height: 9px"></p>
        <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">19/30</p>
    </div>
    <div class="d-flex justify-content-lg-center align-items-center w-15">
        <div>
            <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                <img src="../../assets/admin/app-assets/images/icons/edit.png">
                <span style="margin-left: 5px">Sửa</span>
            </div>
            <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                <img src="../../assets/admin/app-assets/images/icons/del.png">
                <span>Bỏ qua</span>
            </div>
        </div>
    </div>
</div>
<div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 15px 0">
    <div class="d-flex align-items-center w-30 bf-input position-relative bf-gray" style="padding-left: 50px">
        <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
        <div>
            <div class="d-flex align-items-center justify-content-sm-between border-radius-8" style="background: rgba(255, 195, 3, 0.1); padding: 5px 10px; margin-bottom: 8px">
                <img src="../../assets/admin/app-assets/images/icons/Danger_Circle_1.png">
                <p class="m-0 font-size-12" style="color: #FFC303; padding-left: 15px">Những mục cần cải thiện</p>
            </div>
            <p class="m-0 font-weight-bold color-black">Giỏ đựng đồ rửa bát</p>
            <p class="m-0 font-weight-bold color-red">DQG3BE</p>
        </div>
    </div>
    <div class="m-0 w-20 d-flex text-center font-weight-bold justify-content-lg-center align-items-center"><p class="m-0">17:32 <br> 31-05-2021</p></div>
    <div class="m-0 w-35 d-flex align-items-center justify-content-sm-between font-weight-bold">
        <p class="m-0 w-90 d-flex align-items-center border-radius-8 position-relative bf-dv w-6" style="background: #C7C7C7; height: 9px"></p>
        <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">19/30</p>
    </div>
    <div class="d-flex justify-content-lg-center align-items-center w-15">
        <div>
            <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                <img src="../../assets/admin/app-assets/images/icons/edit.png">
                <span style="margin-left: 5px">Sửa</span>
            </div>
            <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                <img src="../../assets/admin/app-assets/images/icons/del.png">
                <span>Bỏ qua</span>
            </div>
        </div>
    </div>
</div>
<div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 15px 0">
    <div class="d-flex align-items-center w-30 bf-input position-relative bf-gray" style="padding-left: 50px">
        <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
        <div>
            <div class="d-flex align-items-center justify-content-sm-between border-radius-8" style="background: rgba(255, 195, 3, 0.1); padding: 5px 10px; margin-bottom: 8px">
                <img src="../../assets/admin/app-assets/images/icons/Danger_Circle_1.png">
                <p class="m-0 font-size-12" style="color: #FFC303; padding-left: 15px">Những mục cần cải thiện</p>
            </div>
            <p class="m-0 font-weight-bold color-black">Giỏ đựng đồ rửa bát</p>
            <p class="m-0 font-weight-bold color-red">DQG3BE</p>
        </div>
    </div>
    <div class="m-0 w-20 d-flex text-center font-weight-bold justify-content-lg-center align-items-center"><p class="m-0">17:32 <br> 31-05-2021</p></div>
    <div class="m-0 w-35 d-flex align-items-center justify-content-sm-between font-weight-bold">
        <p class="m-0 w-90 d-flex align-items-center border-radius-8 position-relative bf-dv w-6" style="background: #C7C7C7; height: 9px"></p>
        <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">19/30</p>
    </div>
    <div class="d-flex justify-content-lg-center align-items-center w-15">
        <div>
            <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                <img src="../../assets/admin/app-assets/images/icons/edit.png">
                <span style="margin-left: 5px">Sửa</span>
            </div>
            <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                <img src="../../assets/admin/app-assets/images/icons/del.png">
                <span>Bỏ qua</span>
            </div>
        </div>
    </div>
</div>
