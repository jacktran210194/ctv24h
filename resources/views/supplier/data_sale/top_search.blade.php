<div class="mg-top-30 bg-white p-2">
    <ul class="d-flex border_box border-radius-4 p-0 content-overview">
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="padding: 10px 20px">
            <a href="{{url('/admin/data_sale/sale-tactician')}}">Sản phẩm theo xu hướng</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="padding: 10px 20px">
            <a href="{{url('/admin/data_sale/sale-tactician/competitive-product')}}">Sản phẩm cạnh tranh</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center active" style="padding: 10px 20px">
            <a href="{{url('/admin/data_sale/sale-tactician/top-search')}}">Top từ khoá</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="padding: 10px 20px">
            <a href="{{url('/admin/data_sale/sale-tactician/optimize-product')}}">Tối ưu sản phẩm</a>
        </li>
    </ul>
    <h3 class="mg-top-30 font-size-24 font-weight-bold">Top từ khoá</h3>
    <p class="font-size-14 color-black">Chỉ có 3 ngành hàng chiếm phần lớn trong danh sách sản phẩm của Shop bạn được hiển thị. Các từ khóa hàng đầu được tìm kiếm cho mỗi danh mục từ 11-06-2021 - 17-06-2021 sẽ được hiển thị.
    </p>
    <div class="mg-top-30 border-top d-flex" style="padding-top: 30px;">
        <button class="w-100 border border-radius-8 bg-white d-flex justify-content-sm-between align-items-center" style="padding: 10px 20px; max-width: 240px">
            <p class="m-0 font-weight-bold font-size-14 color-gray">Home Appliances</p>
            <img src="../../assets/admin/app-assets/images/icons/Polygon.png">
        </button>
    </div>
    <div class="mg-top-30 border-radius-8 bg-red color-white font-weight-bold d-flex">
        <p class="m-0 w-30" style="padding: 10px 0 10px 30px;">Thứ</p>
        <p class="m-0 w-20 d-flex align-items-center ">Từ khóa</p>
        <p class="m-0 w-50 d-flex align-items-center text-center">Mục lục tìm kiếm</p>
    </div>
    <div class="mg-top-30 border-radius-16 d-flex align-items-center border" style="padding: 30px">
        <div class="w-30 d-flex justify-content-sm-between align-items-center" style="padding-right: 34px">
           <p class=" m-0 font-weight-bold color-red">1</p>
            <p class="m-0 font-weight-bold color-black">Nồi chiên không dầu</p>
        </div>
        <p class="m-0 w-60 d-flex align-items-center border-radius-8 position-relative bf-dv w-8" style="background: #C7C7C7; height: 9px"></p>
        <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">80</p>
    </div>
    <div class="mg-top-30 border-radius-16 d-flex align-items-center border" style="padding: 30px">
        <div class="w-30 d-flex justify-content-sm-between align-items-center" style="padding-right: 34px">
            <p class=" m-0 font-weight-bold color-red">2</p>
            <p class="m-0 font-weight-bold color-black">Nồi chiên không dầu</p>
        </div>
        <p class="m-0 w-60 d-flex align-items-center border-radius-8 position-relative bf-dv w-6" style="background: #C7C7C7; height: 9px"></p>
        <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">60</p>
    </div>
    <div class="mg-top-30 border-radius-16 d-flex align-items-center border" style="padding: 30px">
        <div class="w-30 d-flex justify-content-sm-between align-items-center" style="padding-right: 34px">
            <p class=" m-0 font-weight-bold color-red">3</p>
            <p class="m-0 font-weight-bold color-black">Nồi chiên không dầu</p>
        </div>
        <p class="m-0 w-60 d-flex align-items-center border-radius-8 position-relative bf-dv w-5-0" style="background: #C7C7C7; height: 9px"></p>
        <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">50</p>
    </div>
    <div class="mg-top-30 border-radius-16 d-flex align-items-center border" style="padding: 30px">
        <div class="w-30 d-flex justify-content-sm-between align-items-center" style="padding-right: 34px">
            <p class=" m-0 font-weight-bold color-red">4</p>
            <p class="m-0 font-weight-bold color-black">Nồi chiên không dầu</p>
        </div>
        <p class="m-0 w-60 d-flex align-items-center border-radius-8 position-relative bf-dv w-3" style="background: #C7C7C7; height: 9px"></p>
        <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">30</p>
    </div>
    <div class="mg-top-30 border-radius-16 d-flex align-items-center border" style="padding: 30px">
        <div class="w-30 d-flex justify-content-sm-between align-items-center" style="padding-right: 34px">
            <p class=" m-0 font-weight-bold color-red">5</p>
            <p class="m-0 font-weight-bold color-black">Nồi chiên không dầu</p>
        </div>
        <p class="m-0 w-60 d-flex align-items-center border-radius-8 position-relative bf-dv w-2-5" style="background: #C7C7C7; height: 9px"></p>
        <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">25</p>
    </div>
    <div class="mg-top-30 border-radius-16 d-flex align-items-center border" style="padding: 30px">
        <div class="w-30 d-flex justify-content-sm-between align-items-center" style="padding-right: 34px">
            <p class=" m-0 font-weight-bold color-red">6</p>
            <p class="m-0 font-weight-bold color-black">Nồi chiên không dầu</p>
        </div>
        <p class="m-0 w-60 d-flex align-items-center border-radius-8 position-relative bf-dv w-2" style="background: #C7C7C7; height: 9px"></p>
        <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">20</p>
    </div>
    <div class="mg-top-30 border-radius-16 d-flex align-items-center border" style="padding: 30px">
        <div class="w-30 d-flex justify-content-sm-between align-items-center" style="padding-right: 34px">
            <p class=" m-0 font-weight-bold color-red">7</p>
            <p class="m-0 font-weight-bold color-black">Nồi chiên không dầu</p>
        </div>
        <p class="m-0 w-60 d-flex align-items-center border-radius-8 position-relative bf-dv w-1-8" style="background: #C7C7C7; height: 9px"></p>
        <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">18</p>
    </div>
    <div class="mg-top-30 border-radius-16 d-flex align-items-center border" style="padding: 30px">
        <div class="w-30 d-flex justify-content-sm-between align-items-center" style="padding-right: 34px">
            <p class=" m-0 font-weight-bold color-red">8</p>
            <p class="m-0 font-weight-bold color-black">Nồi chiên không dầu</p>
        </div>
        <p class="m-0 w-60 d-flex align-items-center border-radius-8 position-relative bf-dv" style="background: #C7C7C7; height: 9px"></p>
        <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">0</p>
    </div>
</div>
