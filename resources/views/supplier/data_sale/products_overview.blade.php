<div class="mg-top-30 p-2 bg-white">
    <ul class="d-flex border_box border-radius-4 p-0 content-overview">
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="width: 140px; height: 40px">
            <a href="{{url('/admin/data_sale/products')}}">Tổng quan</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center active" style="width: 140px; height: 40px">
            <a href="{{url('/admin/data_sale/products-overview')}}">Hiệu quả</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="width: 140px; height: 40px">
            <a href="{{url('/admin/data_sale/need-improve')}}">Cần cải thiện</a>
        </li>
    </ul>
    <h3 class="font-weight-bold font-size-24 mg-top-30">Thứ hạng sản phẩm</h3>
    <div class="d-flex align-items-center mg-top-30">
        <button class="d-flex align-items-center border-radius-8 bg-white group-kg" style="padding: 10px 20px; margin-right: 20px">
            <p class="m-0">Xem theo ngành hàng</p>
            <img src="../../assets/admin/app-assets/images/icons/Polygon.png" style="margin-left: 10px">
        </button>
        <button class="d-flex align-items-center border-radius-8 bg-white group-kg" style="padding: 10px 20px; margin-right: 20px">
            <p class="m-0">Tất cả ngành hàng</p>
            <img src="../../assets/admin/app-assets/images/icons/Polygon.png" style="margin-left: 10px">
        </button>
        <div class="d-flex align-items-center border-radius-8 bg-white group-kg" style="padding: 10px 20px; margin-right: 20px">
            <input type="text" style="width: 150px; height: 24px" class="border-0 out-line-none" placeholder="Tìm kiếm">
            <label class="m-0"><img src="../../assets/admin/app-assets/images/icons/search.png" style="margin-left: 10px"></label>
        </div>
    </div>
    <div class="mg-top-30 border-radius-8 bg-red color-white font-weight-bold d-flex">
        <p class="m-0 w-30 d-flex align-items-center " style="padding-left: 15px">Thông tin sản phẩm</p>
        <div class="m-0 w-18 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Lượt xem sản phẩm</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="m-0 w-18 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Sản phẩm( thêm vào giỏ hàng)</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="m-0 w-18 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Sản phẩm( đã xác nhận)</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="m-0 w-18 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Doanh thu( đã xác nhận)</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-30" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">100</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-30" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">100</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-30" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">100</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-30" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">100</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-30" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">100</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-30" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">100</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-30" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">100</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-30" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">100</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-30" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">1</p>
        <p class="m-0 w-25 d-flex align-items-center justify-content-lg-center font-weight-bold">100</p>
    </div>
    <div class="d-flex align-items-center justify-content-lg-center w-100 mg-top-30">
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
            <img src="../../assets/admin/app-assets/images/icons/prev.png">
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            1
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13 active">
            2
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            3
        </button>
        <p class="font-weight-bold font-size-13" style="margin-right: 20px;color: #A2A6B0; margin-bottom: 0">
            ...
        </p>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            15
        </button>

        <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
            <img src="../../assets/admin/app-assets/images/icons/next.png">
        </button>
    </div>
</div>
