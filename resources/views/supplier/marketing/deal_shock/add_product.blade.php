<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-3">
                <div class="head-unit-first align-items-center justify-content-lg-between">
                    <div class="head-title">
                        <span>Chọn sản phẩm</span>
                    </div>
                </div>
                <div class="tab-menu d-flex align-items-center justify-content-between p-0 mt-2">
                    <div class="d-flex tab-menu-f col-lg-4 col-md-12 p-0">
                        <div class="menu col-lg-4 col-md-12 <?= $type === 'choose' ? 'checked'  :'' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/deal_shock/add_product?type=choose') ?>">Chọn</a>
                            </center>
                        </div>
                        <div class="menu col-lg-8 col-md-12 <?= $type === 'list' ? 'checked'  :'' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/deal_shock/add_product?type=list') ?>">Đăng danh sách sản phẩm </a>
                            </center>
                        </div>
                    </div>
                </div>
                <?php if($type === 'choose'): ?>
                <div class="d-flex filter-unit align-items-center justify-content-lg-between">
                    <div class="d-flex col-lg-4 col-md-12 align-items-center">
                        <div class="mt-2">
                            <span class="text-black text-bold-700 mr-2">Ngành hàng</span>
                        </div>
                        <div class="head-right mt-2">
                            <span class="mr-5 text-center">Tất cả</span>
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/down.png" class="mr-2">
                        </div>
                    </div>
                    <div class="d-flex col-lg-8 col-md-12 align-items-center justify-content-lg-end p-0">
                        <div class="mt-2">
                            <span class="text-black text-bold-700 mr-2">Tìm kiếm</span>
                        </div>
                        <div class="head-right mt-2 position">
                            <select name="" id="" class="form-control">
                                <option value="">Tên sản phẩm</option>
                                <option value="">Mã sản phẩm</option>
                            </select>
                        </div>
                        <input type="text" class="form-control mt-2 w-50">
                    </div>
                </div>
                <div class="d-flex align-items-center justify-content-lg-end mt-2 checkbox-unit">
                    <div class="mr-2">
                        <input type="checkbox" class="checkbox-view-prd">
                    </div>
                    <div>
                        <span>Xem sản phẩm có sẵn</span>
                    </div>
                </div>
                <div class="body-unit align-items-center justify-content-center mt-3">
                    <div class="table-responsive table-responsive-lg border_table">
                        <table class="table data-list-view table-sm">
                            <thead class="bg_table text-center">
                            <tr>
                                <th scope="col"><input type="checkbox"></th>
                                <th scope="col">Sản phẩm</th>
                                <th scope="col">Đã bán</th>
                                <th scope="col">Giá</th>
                                <th scope="col">Kho hàng</th>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i = 0; $i < 4; $i++)
                            <tr>
                                <td class="text-center"><input type="checkbox"></td>
                                <td>
                                    <div class="d-flex align-items-center justify-content-center">
                                        <img class="image"
                                             src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/product.png">
                                        <div class="product-info">
                                            <p class="name-product">Áo khoác bông dài quá gối </p>
                                            <p class="code-product">Mã: 3241519686</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <span class="text-black text-bold-700">1</span>
                                </td>
                                <td class="text-center">
                                    <span class="text-red text-bold-700 f-18">3.720.000đ</span>
                                </td>
                                <td class="text-center">
                                    <span class="text-black text-bold-700">10</span>
                                </td>
                            </tr>
                            @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php else: ?>

                <div class="post-list-product mt-2">
                    <div class="d-flex head-list">
                        <div class="col-lg-10 col-md-12 p-0">
                            <div class="text-guide">
                                <p class="text-black text-bold-700">Đăng tải danh sách sản phẩm của bạn bằng cách sử dụng bảng Excel mẫu.</p>
                                <p class="text-grey text-bold-700">Tìm hiểu cách thêm/chọn sản phẩm hàng hoạt cho các chương trình Marketing</p>
                                <p class="text-grey text-bold-700">Tìm Mã sản phẩm từ danh sách dưới đây </p>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-12 p-0">
                            <div class="btn-download">
                                <center>
                                    <img src="../../assets/admin/app-assets/images/ncc/marketing/muakemdealsoc/download.png" class="mr-1">
                                    <span>Tải về bản mẫu</span>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="deal-unit-upload-file align-items-center justify-content-center mt-3 mb-4">
                    <div class="align-items-center justify-content-center content-deal-unit">
                        <div class="text-center mb-2">
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/magiamgiacuactv24/cloud.png">
                        </div>
                        <div class="text-center mb-3">
                            <span class="text-black text-bold-700 font-size-24">Chọn hoặc kéo thả file excel vào đây </span>
                            <p class="text-grey text-bold-700 mt-1">Tối đa 20 sản phẩm được chọn</p>
                        </div>
                        <div class="text-center btn-upload-file">
                            <center>
                                <a >Tải tập tin</a>
                            </center>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <!-- content -->
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

