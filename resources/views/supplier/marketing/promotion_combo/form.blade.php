<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="head-new-code mb-2">
                    <div class="title-new-code">
                        <span>TẠO COMBO KHUYẾN MẠI MỚI</span>
                    </div>
                </div>
                <div class="d-flex content-new-code">
                    <div class="d-flex col-lg-8 col-md-12 p-0">
                        <div class="w-100">
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Tên combo khuyến mại:</label>
                                <div class="d-flex w-100">
                                        <input required type="text" name="name" id="name"
                                               value="" class="form-control input-new mr-1">
                                        <input required type="text" name="name" id="name"
                                               value="" class="form-control input-new ml-1">
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Thời gian chạy combo:</label>
                                <div class="d-flex w-100">
                                    <input required type="datetime-local" name="name" id="name"
                                           value="" class="form-control input-new mr-1">
                                    <input required type="datetime-local" name="name" id="name"
                                           value="" class="form-control input-new ml-1">
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Loại combo:</label>
                                <div class="d-flex w-100">
                                    <input required type="number" name="name" id="name"
                                           value="" class="form-control input-new mr-1" placeholder="Giảm giá theo %">
                                    <input required type="number" name="name" id="name"
                                           value="" class="form-control input-new ml-1" placeholder="Giảm giá theo số tiền">
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 1050px"></label>
                                <label class="font-weight-bold font-size-14 pr-1" style="width: 100px">Mua</label>
                                <input required type="number" name="name" id="name"
                                       value="" class="form-control input-new">
                                <label class="font-weight-bold font-size-14 ml-1 mr-1" style="width: 950px">Sản phẩm để được tham gia</label>
                                <input required type="number" name="name" id="name"
                                       value="" class="form-control input-new" placeholder="% giảm giá">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Giới hạn đặt hàng:</label>
                                <input required type="number" name="name" id="name"
                                       value="" class="form-control input-new" placeholder="Số lượng combo khuyến mãi tối đa mà một người mua có thể đặt">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-content m-2">
                <div class="d-flex head-new-code mb-2 align-items-center justify-content-between">
                    <div class="title-new-code">
                        <span>SẢN PHẨM CỦA COMBO KHUYẾN MẠI</span>
                        <p class="text-grey">Vui lòng thêm sản phẩm vào combo khuyến mãi</p>
                    </div>
                    <div class="btn-add-product">
                        <center>
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/add.png" class="pr-1">
                            <span>Thêm sản phẩm</span>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none" class="card down-unit pt-1">
            <div class="card-content m-2">
                <div class="head-new-combo mb-2 align-items-center justify-content-between">
                    <div class="title-new-combo">
                        <p class="text-black text-bold-700">Thiết lập nhóm sản phẩm</p>
                        <span class="text-grey">Đã chọn</span>
                        <br>
                        <span class="text-black">0</span>
                        <span class="text-grey">phân loại hàng</span>
                    </div>
                    <div class="d-flex tool-unit">
                        <div class="col-lg-7">
                        </div>
                        <div class="d-flex col-lg-5 p-0 btn-group align-items-center justify-content-between">
                            <div class="btn-unit">
                                <center>
                                    <a href="">Vô hiệu hóa hàng loạt</a>
                                </center>
                            </div>
                            <div class="btn-unit">
                                <center>
                                    <a href="">Kích hoạt hàng loại</a>
                                </center>
                            </div>
                            <div class="btn-unit">
                                <center>
                                    <a href="">Xóa hàng loạt</a>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-unit-combo">
                    <div class="body-unit mt-3 align-items-center justify-content-center">
                        <div class="table-responsive table-responsive-lg border_table">
                            <table class="table data-list-view table-sm">
                                <thead class="bg_table text-center">
                                <tr>
                                    <th scope="col"><input type="checkbox"></th>
                                    <th scope="col">Sản phẩm</th>
                                    <th scope="col">Giá</th>
                                    <th scope="col">Kho hàng</th>
                                    <th scope="col">Thông tin vận chuyển</th>
                                    <th scope="col">Kích hoạt/Bỏ kích hoạt</th>
                                    <th scope="col">Hoạt động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @for($i = 0; $i < 2; $i++)
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td>
                                        <div class="d-flex align-items-center justify-content-center">
                                            <img class="image"
                                                 src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/product.png">
                                            <div class="product-info">
                                                <p class="name-product">Áo khoác bông dài quá gối </p>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">700.000đ</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">10</span>
                                    </td>
                                    <td class="text-center">
                                        <img src="../../assets/admin/app-assets/images/ncc/marketing/magiamgiacuactv24/ghn.png">
                                        <p class="text-black text-bold-700">Giao hàng nhanh</p>
                                    </td>
                                    <td class="text-center">
                                        <div class="align-items-center justify-content-center">
                                            <label class="switch">
                                                <input type="checkbox">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <a>
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/magiamgiacuactv24/bin.png">
                                        </a>
                                    </td>
                                </tr>
                                @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="d-flex footer-combo">
                    <div class="col-lg-8 col-md-12 p-0">

                    </div>
                    <div class="d-flex col-lg-4 col-md-12 p-0 mb-3 mt-3 align-items-lg-end justify-content-lg-end">
                        <div class="btn-footer mr-2">
                            <center>
                                <a href="">Hủy</a>
                            </center>
                        </div>
                        <div class="btn-footer">
                            <center>
                                <a href="">Xác nhận</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-active", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            $('#myModal').modal('show');
        });

        $(document).on("click", ".btn-add-product", function () {
           $(".down-unit").css('display','block');
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

