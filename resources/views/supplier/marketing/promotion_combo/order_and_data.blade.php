<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="head-unit-first align-items-center justify-content-lg-between">
                    <div class="head-title">
                        <span>Đơn hàng và dữ liệu</span>
                    </div>
                    <div class="order-info-unit p-2 mt-2">
                        <div class="mb-1">
                            <span class="info-text">Thông tin cơ bản</span>
                        </div>
                        <div class="d-flex order-info">
                            <div class="col-lg-5 col-md-12 p-0">
                                <div>
                                    <span class="head-order-title">Loại combo:</span>
                                    <span class="footer-order-title">Mua 2 sản phẩm để được giảm 10.000đ</span>
                                </div>
                                <div>
                                    <span class="head-order-title">Giảm giá:</span>
                                    <span class="footer-order-title">5.000đ</span>
                                </div>
                                <div>
                                    <span class="head-order-title">Gới hạn đặt hàng:</span>
                                    <span class="footer-order-title">Khách hàng được mua tối đa 20 combo khuyến mại</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-12">
                                <div>
                                    <span class="head-order-title">Tên combo:</span>
                                    <span>Mua 2 giảm 10k</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div>
                                    <span class="head-order-title">Thời gian chạy combo: </span>
                                    <span>2021/06/17 00:00-2021/07/04 12:00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-menu d-flex align-items-center justify-content-between p-0 mt-2">
                    <div class="d-flex tab-menu-f col-lg-4 col-md-12 p-0">
                        <div class="menu col-lg-4 col-md-12 <?= $type === 'all' ? 'checked'  :'' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/promotion_combo/order_and_data?type=all') ?>">Tổng quan</a>
                            </center>
                        </div>
                        <div class="menu col-lg-8 col-md-12 <?= $type === 'detail' ? 'checked'  :'' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/promotion_combo/order_and_data?type=detail') ?>">Chi tiết dữ liệu combo khuyến mại</a>
                            </center>
                        </div>
                    </div>
                </div>
                <?php if($type === 'all') :?>
                <div class="body-unit align-items-center justify-content-center mt-3">
                    <div class="table-responsive bor table-responsive-lg border_table">
                        <table class="table data-list-view table-sm">
                            <thead class="bg_table text-center">
                            <tr>
                                <th scope="col">Sản phẩm</th>
                                <th scope="col">Đơn giá </th>
                                <th scope="col">Số lượng</th>
                                <th scope="col">Tổng tiền</th>
                                <th scope="col">Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr class="head-content-tb">
                                    <td class="text-center">
                                        <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/product.png">
                                        <span class="text-black text-bold-700">Áo khoác bông dài quá gối</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700">720.000đ</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">20</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700">3.720.000đ</span>
                                    </td>
                                    <td>
                                        <div class="bg-blue bg-span text-center align-items-center justify-content-center btn-status-combo-order">
                                            Đang diễn ra
                                        </div>
                                    </td>
                                </tr>
                                <tr class="head-content-tb">
                                    <td class="text-center">
                                        <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/product.png">
                                        <span class="text-black text-bold-700">Áo khoác bông dài quá gối</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700">720.000đ</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">20</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700">3.720.000đ</span>
                                    </td>
                                    <td>
                                        <div class="bg-red bg-span text-center align-items-center justify-content-center btn-status-combo-order">
                                            Đã kết thúc
                                        </div>
                                    </td>
                                </tr>
                                <tr class="head-content-tb">
                                    <td class="text-center">
                                        <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/product.png">
                                        <span class="text-black text-bold-700">Áo khoác bông dài quá gối</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700">720.000đ</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">20</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700">3.720.000đ</span>
                                    </td>
                                    <td>
                                        <div class="bg-blue bg-span text-center align-items-center justify-content-center btn-status-combo-order">
                                            Đang diễn ra
                                        </div>
                                    </td>
                                </tr>
                                <tr class="head-content-tb">
                                    <td class="text-center">
                                        <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/product.png">
                                        <span class="text-black text-bold-700">Áo khoác bông dài quá gối</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700">720.000đ</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">20</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700">3.720.000đ</span>
                                    </td>
                                    <td>
                                        <div class="bg-red bg-span text-center align-items-center justify-content-center btn-status-combo-order">
                                            Đã kết thúc
                                        </div>
                                    </td>
                                </tr>
                                <tr class="head-content-tb">
                                    <td class="text-center">
                                        <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/product.png">
                                        <span class="text-black text-bold-700">Áo khoác bông dài quá gối</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700">720.000đ</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">20</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700">3.720.000đ</span>
                                    </td>
                                    <td>
                                        <div class="bg-blue bg-span text-center align-items-center justify-content-center btn-status-combo-order">
                                            Đang diễn ra
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php else: ?>
                <div class="d-flex statistic p-0 mt-3">
                    <div class="box-unit">
                        <div class="mt-1 ml-1">
                            <span class="">Doanh thu</span>
                            <br>
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/combo_km/danger_circle.png">
                            <br>
                            <div class="align-content-lg-end justify-content-lg-end">
                                <span class="text-blue font-weight-bold font-size-36">0</span>
                                <span class="text-blue font-size-14">đ</span>
                            </div>
                        </div>
                    </div>
                    <div class="box-unit">
                        <div class="mt-1 ml-1">
                            <span class="">Đơn hàng</span>
                            <br>
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/combo_km/danger_circle.png">
                            <br>
                            <span class="text-blue font-weight-bold font-size-36">0</span>
                        </div>
                    </div>
                    <div class="box-unit">
                        <div class="mt-1 ml-1">
                            <span class="">Số lượng combo đã bán</span><img src="../../assets/admin/app-assets/images/ncc/marketing/combo_km/danger_circle.png" class="pl-1">
                            <br>
                            <span class="text-blue font-weight-bold font-size-36">0</span>
                        </div>
                    </div>
                    <div class="box-unit">
                        <div class="mt-1 ml-1">
                            <span class="">Số sản phẩm đã bán</span><img src="../../assets/admin/app-assets/images/ncc/marketing/combo_km/danger_circle.png" class="pl-1">
                            <br>
                            <span class="text-blue font-weight-bold font-size-36">0</span>
                        </div>
                    </div>
                    <div class="box-unit">
                        <div class="mt-1 ml-1">
                            <span class="">Người mua</span>
                            <br>
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/combo_km/danger_circle.png">
                            <br>
                            <span class="text-blue font-weight-bold font-size-36">0</span>
                        </div>
                    </div>
                    <div class="box-unit">
                        <div class="mt-1 ml-1">
                            <span class="">Doanh thu/người mua</span><img src="../../assets/admin/app-assets/images/ncc/marketing/combo_km/danger_circle.png" class="pl-1">
                            <br>
                            <span class="text-blue font-weight-bold font-size-36">0</span>
                        </div>
                    </div>
                </div>
                <div class="body-unit align-items-center justify-content-center mt-3">
                    <div class="table-responsive bor table-responsive-lg border_table">
                        <table class="table data-list-view table-sm">
                            <thead class="bg_table text-center">
                            <tr>
                                <th scope="col">Ngày</th>
                                <th scope="col">Doanh thu</th>
                                <th scope="col">Đơn hàng</th>
                                <th scope="col">Số lượng combo khuyến mãi đã bán</th>
                                <th scope="col">Số lượng sản phẩm đã bán</th>
                                <th scope="col">Người mua</th>
                                <th scope="col">Doanh thu trên mỗi người mua</th>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i = 0; $i < 6; $i++)
                                <tr class="head-content-tb">
                                    <td class="text-center">
                                        <span class="text-red text-bold-700">14-01-2021</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">0đ</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">0</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">0</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">0</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">0</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">0đ</span>
                                    </td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <!-- content -->
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

