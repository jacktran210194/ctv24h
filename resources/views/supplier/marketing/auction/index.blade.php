<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">
<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <div class="d-flex align-items-center mb-2">
            <a href="{{url('admin/marketing_supplier/auction')}}" style="width: 200px!important;"
               class="mr-1 btn border btn_main btn text-bold-700">Chương trình đấu giá
            </a>
            <a href="{{url('admin/marketing_supplier/auction/auction_my')}}" style="width: 200px!important;" class="btn border bg-white text-bold-700">
                Đấu giá của tôi
            </a>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="tab-bar-page mb-2" style="width: 50%!important;">
                    <ul class="tab-bar-content d-flex align-items-center justify-content-lg-start flex-wrap">
                        <li class="d-flex align-items-center justify-content-lg-center active">
                            <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Tất
                                cả</a>
                        </li>
                        <li class="d-flex align-items-center justify-content-lg-center">
                            <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đã
                                đăng ký</a>
                        </li>
                        <li class="d-flex align-items-center justify-content-lg-center">
                            <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Chờ
                                xác nhận</a>
                        </li>
                    </ul>
                </div>
                <div class="border_xam border_radius mb-1 p-1 d-flex align-items-center">
                    <img width="190px" height="150px" class="border_radius"
                         src="https://luathuythanh.vn/assets/uploads/images/trinh-tu-dau-gia-bang-bo-phieu-truc-tiep-tai-cuoc-dau-gia.jpg"
                         alt="">
                    <div class="content_info ml-2">
                        <div class="bg-green text-bold-700 mb-1" style="width: 100px!important;">Sắp diễn ra</div>
                        <h3 class="text-bold-700 f-18">Chương trình đấu giá [0h 21.06.2021 - 23h59 21.06.2021]</h3>
                        <p class="text-sliver f-14">Thời gian chương trình: 2021/07/09 23:00:00 đến 2021/07/10
                            22:59:59</p>
                        <p class="text-sliver f-14 m-0">Đăng ký: có 3 khung giờ bạn có thể đăng ký.</p>
                    </div>
                    <a href="{{url('admin/marketing_supplier/auction/register')}}" class="border_radius btn btn_main position-absolute text-bold-700" style="right: 3%;">Đăng ký ngay</a>
                </div>
                <div class="border_xam border_radius mb-1 p-1 d-flex align-items-center">
                    <img width="190px" height="150px" class="border_radius"
                         src="https://luathuythanh.vn/assets/uploads/images/trinh-tu-dau-gia-bang-bo-phieu-truc-tiep-tai-cuoc-dau-gia.jpg"
                         alt="">
                    <div class="content_info ml-2">
                        <div class="bg-green text-bold-700 mb-1" style="width: 100px!important;">Sắp diễn ra</div>
                        <h3 class="text-bold-700 f-18">Chương trình đấu giá [0h 21.06.2021 - 23h59 21.06.2021]</h3>
                        <p class="text-sliver f-14">Thời gian chương trình: 2021/07/09 23:00:00 đến 2021/07/10
                            22:59:59</p>
                        <p class="text-sliver f-14 m-0">Đăng ký: có 3 khung giờ bạn có thể đăng ký.</p>
                    </div>
                    <a href="{{url('admin/marketing_supplier/auction/register')}}" class="border_radius btn btn_main position-absolute text-bold-700" style="right: 3%;">Đăng ký ngay</a>
                </div>
                <div class="border_xam border_radius mb-1 p-1 d-flex align-items-center">
                    <img width="190px" height="150px" class="border_radius"
                         src="https://luathuythanh.vn/assets/uploads/images/trinh-tu-dau-gia-bang-bo-phieu-truc-tiep-tai-cuoc-dau-gia.jpg"
                         alt="">
                    <div class="content_info ml-2">
                        <div class="bg-green text-bold-700 mb-1" style="width: 100px!important;">Sắp diễn ra</div>
                        <h3 class="text-bold-700 f-18">Chương trình đấu giá [0h 21.06.2021 - 23h59 21.06.2021]</h3>
                        <p class="text-sliver f-14">Thời gian chương trình: 2021/07/09 23:00:00 đến 2021/07/10
                            22:59:59</p>
                        <p class="text-sliver f-14 m-0">Đăng ký: có 3 khung giờ bạn có thể đăng ký.</p>
                    </div>
                    <a href="{{url('admin/marketing_supplier/auction/register')}}" class="border_radius btn btn_main position-absolute text-bold-700" style="right: 3%;">Đăng ký ngay</a>
                </div>
                <div class="border_xam border_radius mb-1 p-1 d-flex align-items-center">
                    <img width="190px" height="150px" class="border_radius"
                         src="https://luathuythanh.vn/assets/uploads/images/trinh-tu-dau-gia-bang-bo-phieu-truc-tiep-tai-cuoc-dau-gia.jpg"
                         alt="">
                    <div class="content_info ml-2">
                        <div class="bg-green text-bold-700 mb-1" style="width: 100px!important;">Sắp diễn ra</div>
                        <h3 class="text-bold-700 f-18">Chương trình đấu giá [0h 21.06.2021 - 23h59 21.06.2021]</h3>
                        <p class="text-sliver f-14">Thời gian chương trình: 2021/07/09 23:00:00 đến 2021/07/10
                            22:59:59</p>
                        <p class="text-sliver f-14 m-0">Đăng ký: có 3 khung giờ bạn có thể đăng ký.</p>
                    </div>
                    <a href="{{url('admin/marketing_supplier/auction/register')}}" class="border_radius btn btn_main position-absolute text-bold-700" style="right: 3%;">Đăng ký ngay</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

