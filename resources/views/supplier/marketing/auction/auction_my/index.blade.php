<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">
<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <div class="d-flex align-items-center mb-2">
            <a href="{{url('admin/marketing_supplier/auction')}}" style="width: 200px!important;"
                    class="mr-1 btn border bg-white btn text-bold-700">Chương trình đấu giá
            </a>
            <a href="{{url('admin/marketing_supplier/auction/auction_my')}}" style="width: 200px!important;" class="btn btn_main border bg-white text-bold-700">
                Đấu giá của tôi
            </a>
        </div>
        <div class="card">
            <div class="card-body">
                <h3 class="text-bold-700 f-24 mb-2">Đấu giá của tôi</h3>
                <div class="tab-bar-page mb-2">
                    <ul class="tab-bar-content d-flex align-items-center justify-content-lg-start flex-wrap">
                        <li class="d-flex align-items-center justify-content-lg-center active">
                            <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Tất
                                cả</a>
                        </li>
                        <li class="d-flex align-items-center justify-content-lg-center">
                            <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đang diễn ra</a>
                        </li>
                        <li class="d-flex align-items-center justify-content-lg-center">
                            <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Sắp diễn ra</a>
                        </li>
                        <li class="d-flex align-items-center justify-content-lg-center">
                            <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đã kết thúc</a>
                        </li>
                    </ul>
                </div>
                <div class="table-responsive table-responsive-lg border_table">
                    <table class="table data-list-view table-sm">
                        <thead>
                        <tr class="bg_table text-center">
                            <th style="text-align: left!important;">Loại sản phẩm</th>
                            <th>Lượt đấu giá</th>
                            <th>Giá hiện tại</th>
                            <th>Bước giá</th>
                            <th>Giá khởi điểm</th>
                            <th>Thời gian</th>
                            <th>Giá mua</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody id="get">
                            <tr class="text-center text-bold-700">
                                <td style="text-align: left!important;">
                                    <div class="d-flex align-items-center">
                                        <img class="border_radius mr-1" width="73px" height="71px" src="https://margram.vn/files/chup-anh-san-pham-8.jpg" alt="">
                                        <div class="content_info">
                                            <p class="text-red f-14">[Hàng có sẵn]</p>
                                            <p class="f-14 m-0">Phiếu ưu đãi Grab Express </p>
                                        </div>
                                    </div>
                                </td>
                                <td>50</td>
                                <td>100.000 đ</td>
                                <td>2.000 đ</td>
                                <td>10.000 đ</td>
                                <td>
                                    <div class="bg-blue mb-1" style="width: 150px!important; margin: 0 auto;">Đang diễn ra</div>
                                    <p class="text-sliver">09:24:41 đến 11:00:59</p>
                                </td>
                                <td class="text-red f-24">
                                    156.000 đ
                                </td>
                                <td class="text-underline" style="color: #005DB6; text-align: right!important;">
                                    <a><i class="fa fa-edit fa-lg"></i> Sửa</a>
                                    <br>
                                    <a>Đơn hàng</a>
                                </td>
                            </tr>
                            <tr class="text-center text-bold-700">
                                <td style="text-align: left!important;">
                                    <div class="d-flex align-items-center">
                                        <img class="border_radius mr-1" width="73px" height="71px" src="https://margram.vn/files/chup-anh-san-pham-8.jpg" alt="">
                                        <div class="content_info">
                                            <p class="text-red f-14">[Hàng có sẵn]</p>
                                            <p class="f-14 m-0">Phiếu ưu đãi Grab Express </p>
                                        </div>
                                    </div>
                                </td>
                                <td>50</td>
                                <td>100.000 đ</td>
                                <td>2.000 đ</td>
                                <td>10.000 đ</td>
                                <td>
                                    <div class="bg-green mb-1" style="width: 150px!important; margin: 0 auto;">Sắp diễn ra</div>
                                    <p class="text-sliver">09:24:41 đến 11:00:59</p>
                                </td>
                                <td class="text-red f-24">
                                    156.000 đ
                                </td>
                                <td class="text-underline" style="color: #005DB6; text-align: right!important;">
                                    <a><i class="fa fa-edit fa-lg"></i> Sửa</a>
                                    <br>
                                    <a>Đơn hàng</a>
                                </td>
                            </tr>
                            <tr class="text-center text-bold-700">
                                <td style="text-align: left!important;">
                                    <div class="d-flex align-items-center">
                                        <img class="border_radius mr-1" width="73px" height="71px" src="https://margram.vn/files/chup-anh-san-pham-8.jpg" alt="">
                                        <div class="content_info">
                                            <p class="text-red f-14">[Hàng dropship]</p>
                                            <p class="f-14 m-0">Phiếu ưu đãi Grab Express </p>
                                        </div>
                                    </div>
                                </td>
                                <td>50</td>
                                <td>100.000 đ</td>
                                <td>2.000 đ</td>
                                <td>10.000 đ</td>
                                <td>
                                    <div class="bg-red mb-1" style="width: 150px!important; margin: 0 auto;">Đã diễn ra</div>
                                    <p class="text-sliver">09:24:41 đến 11:00:59</p>
                                </td>
                                <td class="text-red f-24">
                                    156.000 đ
                                </td>
                                <td class="text-underline" style="color: #005DB6; text-align: right!important;">
                                    <a><i class="fa fa-edit fa-lg"></i> Sửa</a>
                                    <br>
                                    <a>Đơn hàng</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

