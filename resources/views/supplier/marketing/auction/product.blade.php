<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">
<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <div class="card_title d-flex align-items-center justify-content-between">
                    <h3 class="text-bold-700 f-24">ĐĂNG KÝ SẢN PHẨM ĐẤU GIÁ</h3>
                    <a class="btn btn_main text-white text-bold-700">
                        <img src="../../assets/admin/app-assets/images/icon_dashboard/icon_add.png" alt="">
                        Thêm sản phẩm đấu giá
                    </a>
                </div>
                <div class="mt-2 table-responsive table-responsive-lg border_table">
                    <table class="table data-list-view table-hover">
                        <thead class="bg_table">
                        <tr class="text-center text-bold-700">
                            <th><input type="checkbox"></th>
                            <th class="text-left">Tên sản phẩm</th>
                            <th>Giá gốc</th>
                            <th width="12%">Giá khởi điểm (đ)</th>
                            <th width="12%">Bước giá (đ)</th>
                            <th>Giá mua</th>
                            <th>Thời gian</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr class="text-center text-bold-700 f-16">
                                <td><input type="checkbox"></td>
                                <td class="text-left">
                                    <div class="d-flex align-items-center">
                                        <img class="border_radius mr-1" width="77px" height="77px"
                                             src="https://yt3.ggpht.com/Vy6FzAeEurOMlZXglEuf2Q7LzLfvIu5YlV0_8zWL14KtYnFHj18ZHzvVW6dn8p2h37Jk-OGq=s48-c-k-c0x00ffffff-no-nd-rj"
                                             alt="">
                                        <p class="m-0" style="width: 150px!important;">Áo khoác bông dài quá gối </p>
                                    </div>
                                </td>
                                <td>700.000 đ</td>
                                <td class="text-left"><input class="form-control" type="number"></td>
                                <td class="text-left"><input class="form-control" type="number"></td>
                                <td class="text-red f-18">500.000 đ</td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="bg-red" style="width: 40px!important;">09</div>
                                         :
                                        <div class="bg-red" style="width: 40px!important;">24</div>
                                         :
                                        <div class="bg-red" style="width: 40px!important;">41</div>
                                    </div>
                                </td>
                                <td class="text-blue text-right text-underline">
                                    <a>Lưu và gửi</a>
                                    <br>
                                    <a><i class="fa fa-trash fa-lg"></i>  Xoá</a>
                                </td>
                            </tr>
                            <tr class="text-center text-bold-700 f-16">
                                <td><input type="checkbox"></td>
                                <td class="text-left">
                                    <div class="d-flex align-items-center">
                                        <img class="border_radius mr-1" width="77px" height="77px"
                                             src="https://yt3.ggpht.com/Vy6FzAeEurOMlZXglEuf2Q7LzLfvIu5YlV0_8zWL14KtYnFHj18ZHzvVW6dn8p2h37Jk-OGq=s48-c-k-c0x00ffffff-no-nd-rj"
                                             alt="">
                                        <p class="m-0" style="width: 150px!important;">Áo khoác bông dài quá gối </p>
                                    </div>
                                </td>
                                <td>700.000 đ</td>
                                <td class="text-left"><input class="form-control" type="number"></td>
                                <td class="text-left"><input class="form-control" type="number"></td>
                                <td class="text-red f-18">500.000 đ</td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="bg-red" style="width: 40px!important;">09</div>
                                        :
                                        <div class="bg-red" style="width: 40px!important;">24</div>
                                        :
                                        <div class="bg-red" style="width: 40px!important;">41</div>
                                    </div>
                                </td>
                                <td class="text-blue text-right text-underline">
                                    <a>Lưu và gửi</a>
                                    <br>
                                    <a><i class="fa fa-trash fa-lg"></i>  Xoá</a>
                                </td>
                            </tr>
                            <tr class="text-center text-bold-700 f-16">
                                <td><input type="checkbox"></td>
                                <td class="text-left">
                                    <div class="d-flex align-items-center">
                                        <img class="border_radius mr-1" width="77px" height="77px"
                                             src="https://yt3.ggpht.com/Vy6FzAeEurOMlZXglEuf2Q7LzLfvIu5YlV0_8zWL14KtYnFHj18ZHzvVW6dn8p2h37Jk-OGq=s48-c-k-c0x00ffffff-no-nd-rj"
                                             alt="">
                                        <p class="m-0" style="width: 150px!important;">Áo khoác bông dài quá gối </p>
                                    </div>
                                </td>
                                <td>700.000 đ</td>
                                <td class="text-left"><input class="form-control" type="number"></td>
                                <td class="text-left"><input class="form-control" type="number"></td>
                                <td class="text-red f-18">500.000 đ</td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="bg-red" style="width: 40px!important;">09</div>
                                        :
                                        <div class="bg-red" style="width: 40px!important;">24</div>
                                        :
                                        <div class="bg-red" style="width: 40px!important;">41</div>
                                    </div>
                                </td>
                                <td class="text-blue text-right text-underline">
                                    <a>Lưu và gửi</a>
                                    <br>
                                    <a><i class="fa fa-trash fa-lg"></i>  Xoá</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

