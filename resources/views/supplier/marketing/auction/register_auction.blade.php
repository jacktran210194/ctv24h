<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">
<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <div class="d-flex align-items-center mb-2">
            <a href="{{url('admin/marketing_supplier/auction')}}" style="width: 200px!important;"
               class="mr-1 btn border btn_main btn text-bold-700">Chương trình đấu giá
            </a>
            <a href="{{url('admin/marketing_supplier/auction/auction_my')}}" style="width: 200px!important;" class="btn border bg-white text-bold-700">
                Đấu giá của tôi
            </a>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="text-bold-700 border_xam border_radius d-flex align-items-center p-2">
                    <img class="border_radius mr-1" width="195px" height="152px" src="https://file4.batdongsan.com.vn/2020/08/21/tEfrXoEj/20200821105955-7ab7.jpg" alt="">
                    <div class="content_info" style="width: 300px!important;">
                        <div class="bg-green mb-1">Sắp diễn ra</div>
                        <p>Chương trình đấu giá [0h 21.06.2021 - 23h59 21.06.2021]</p>
                        <button class="btn btn_main m-0">Đăng ký ngay</button>
                    </div>
                    <div class="border_doc ml-5">
                        <img src="../../assets/admin/app-assets/images/icon_dashboard/thanh_doc.png" alt="">
                    </div>
                    <div class="ml-4" style="width: 300px!important;">
                        <p class="f-18">Thời gian đăng ký</p>
                        <br>
                        <p class="text-sliver m-0">2021/06/17 00:00-2021/07/04 12:00
                            Bắt đầu sau 13 giờ 0 phút</p>
                    </div>
                    <div class="border_doc ml-5">
                        <img src="../../assets/admin/app-assets/images/icon_dashboard/thanh_doc.png" alt="">
                    </div>
                    <div class="ml-4" style="width: 250px!important;">
                        <p class="f-18">Thời gian diễn ra</p>
                        <br>
                        <p class="text-sliver m-0">2021/07/07 00:00-2021/07/07 23:59
                            Bắt đầu sau 20 ngày 13 giờ
                        </p>
                    </div>
                </div>
                <div class="content_bettwen mt-3">
                    <div class="tab-bar-page mb-2" style="width: 50%!important;">
                        <ul class="tab-bar-content d-flex align-items-center justify-content-lg-start flex-wrap">
                            <li style="width: auto!important;" class="p-1 d-flex align-items-center justify-content-lg-center active btn_detail_program">
                                <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Chi tiết chương trình</a>
                            </li>
                            <li style="width: auto!important;" class="p-1 d-flex align-items-center justify-content-lg-center btn_condition_program">
                                <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Điều kiện tham gia</a>
                            </li>
                        </ul>
                    </div>
                    <div class="detail_program ml-1">
                        <p>Quy định chung:</p>
                        <p>- Chương trình không áp dụng cho các sản phẩm thay đổi nội dung và / hoặc tăng giá trước khi đăng ký & trong suốt thời gian tham gia chương trình.</p>
                        <p>- Sản phẩm tham gia chương trình phải là hàng có sẵn. Shop có trách nhiệm cập nhật đúng tồn kho trước giờ mở bán, tránh để tồn kho ảo dẫn đến việc hủy đơn và có thể bị tính điểm Sao Quả Tạ / bị khóa Shop ít nhất 1 tuần.</p>
                        <p>- Tính năng Tạm nghỉ sẽ bị tắt trong suốt thời gian chương trình diễn ra.</p>
                        <p>- Điều kiện và điều khoản chương trình có thể được thay đổi mà không cần thông báo trước.</p>
                        <p>- Nếu xảy ra tranh chấp, quyết định cuối cùng thuộc về Shopee.</p>
                    </div>
                    <div style="display: none;" class="codition_program ml-1">
                        <p>Điều kiện tham gia</p>
                        <p>Điều kiện tham gia</p>
                        <p>Điều kiện tham gia</p>
                        <p>Điều kiện tham gia</p>
                        <p>Điều kiện tham gia</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h3 class="text-bold-700 f-24 mb-2">KHUNG GIỜ</h3>
                <div class="tab-bar-page mb-2" style="width: 50%!important;">
                    <ul class="tab-bar-content d-flex align-items-center justify-content-lg-start flex-wrap">
                        <li class="p-1 d-flex align-items-center justify-content-lg-center active">
                            <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Tất cả</a>
                        </li>
                        <li class="p-1 d-flex align-items-center justify-content-lg-center">
                            <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đăng ký</a>
                        </li>
                        <li class="p-1 d-flex align-items-center justify-content-lg-center   ">
                            <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Chờ xác nhận</a>
                        </li>
                    </ul>
                </div>
                <div class="text-bold-700 border_xam border_radius d-flex align-items-center p-2 mb-1">
                    <img class="border_radius mr-1" width="195px" height="152px" src="https://file4.batdongsan.com.vn/2020/08/21/tEfrXoEj/20200821105955-7ab7.jpg" alt="">
                    <div class="content_info">
                        <div class="bg-green mb-1">Sắp diễn ra</div>
                        <p>Chương trình đấu giá [0h 21.06.2021 - 23h59 21.06.2021]</p>
                        <div class="text-blue d-flex align-items-center m-0">
                            <i class="fa fa-check-circle fa-2x mr-1"></i><span class="m-0"> Shop của bạn có thể tham gia cho khung giờ này.</span>
                        </div>
                    </div>
                    <a href="{{url('admin/marketing_supplier/auction/register/product')}}" class="btn btn_main position-absolute" style="right: 3%;">Đăng ký sản phẩm</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(document).on('click','.btn_detail_program',function () {
            $('.detail_program').show();
            $('.codition_program').hide();
            $('.btn_detail_program').addClass('active');
            $('.btn_condition_program').removeClass('active');
        });
        $(document).on('click','.btn_condition_program',function () {
            $('.detail_program').hide();
            $('.codition_program').show();
            $('.btn_condition_program').addClass('active');
            $('.btn_detail_program').removeClass('active');
        })
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

