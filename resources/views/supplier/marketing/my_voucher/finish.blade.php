<div class="body-unit align-items-center justify-content-center mt-3">
    <div class="table-responsive bor table-responsive-lg border_table">
        <table class="table data-list-view table-sm">
            <thead class="bg_table text-center">
            <tr>
                <th scope="col" class="pt-2">Mã voucher | Tên</th>
                <th scope="col" class="pt-2">Loại mã</th>
                <th scope="col" class="pt-2">Giảm giá</th>
                <th scope="col">
                    <span>Tổng số mã giảm giá</span>
                    <br>
                    <span>có thể sử dụng</span>
                </th>
                <th scope="col" class="pt-2">Đã lưu</th>
                <th scope="col" class="pt-2">Đã dùng</th>
                <th scope="col">
                    <span>Trạng thái</span>
                    <br>
                    <span> Thời gian lưu Mã giảm giá</span>
                </th>
                <th scope="col" class="pt-2">Thao tác</th>
            </tr>
            </thead>
            <tbody>
            @if($data)
                @foreach($data as $value)
                    <tr class="head-content-tb">
                        <td>
                            <div class="d-flex">
                                <img class="image image-preview"
                                     src="{{$value->image}}">
                                <div class="product-info">
                                    <span class="text-black text-bold-700">{{$value->title}}</span>
                                    <p class="text-grey">Giảm {{number_format($value->value_discount)}}{{$value->type_discount}}</p>
                                </div>
                            </div>
                        </td>
                        <td class="text-center">
                            <span class="text-black text-bold-700">{{$value->shop_or_product}}</span>
                            <br>
                            {{--                        <span class="text-grey">(Tất cả sản phẩm)</span>--}}
                        </td>
                        <td class="text-center">
                            <p class="text-grey">Giảm {{number_format($value->value_discount)}}{{$value->type_discount}}</p>
                        </td>
                        <td class="text-center">
                            <span class="text-black text-bold-700">{{$value->number_available}}</span>
                        </td>
                        <td class="text-center">
                            <span class="text-black text-bold-700">{{$value->number_save ? : 0}}</span>
                        </td>
                        <td class="text-center">
                            <span class="text-black text-bold-700">{{$value->number_used ? : 0}}</span>
                        </td>
                        <td>
                            <div class="bg-red bg-span text-center align-items-center justify-content-center btn-status">
                                Đã kết thúc
                            </div>
                            <div class="text-center">
                                <span class="text-black text-bold-700">{{date('d-m-Y', strtotime($value->time_start))}} đến {{date('d-m-Y', strtotime($value->time_end))}}</span>
                            </div>
                        </td>
                        <td class="text-center">
                            <a href="{{url('admin/marketing_supplier/my_voucher/detail/'.$value->id)}}" class="text-blue text-underline text-bold-700">Chi tiết</a>
                            <br>
                            <a href="{{url('admin/marketing_supplier/my_voucher/order')}}" class="text-blue text-underline text-bold-700">Đơn hàng</a>
                            <br>
                            <a class="text-blue text-underline text-bold-700">Sao chép</a>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
