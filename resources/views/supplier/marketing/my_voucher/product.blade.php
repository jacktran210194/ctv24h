<?php
if (!empty($product_active)) {
    $data_js = json_decode($product_active, true);
    $json_last_error = json_last_error();
}
?>
<?php foreach ($product as $value): ?>
<div class="w-100 <?= !empty($product_active) && !$json_last_error && in_array($value['id'], $data_js) ? 'active-product' : '' ?>">
    <div class="btn btn-product list-product" data-id="<?= $value['id'] ?>" data-name="<?= $value['name'] ?>"><?= $value['name'] ?></div>
</div>
<?php endforeach; ?>
