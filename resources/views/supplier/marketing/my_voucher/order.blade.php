<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="head-unit-first align-items-center justify-content-lg-between mb-2">
                    <div class="head-title">
                        <span>ĐƠN HÀNG</span>
                    </div>
                </div>
                <div class="order-info-unit p-2">
                    <div class="mb-1">
                        <span class="info-text">Thông tin cơ bản</span>
                    </div>
                    <div class="d-flex order-info">
                        <div class="col-lg-4 col-md-12 p-0">
                            <div>
                                <span class="head-order-title">Tên chương trình giảm giá:</span>
                                <span class="footer-order-title">Follow ngay</span>
                            </div>
                            <div>
                                <span class="head-order-title">Giảm giá:</span>
                                <span class="footer-order-title">5.000đ</span>
                            </div>
                            <div>
                                <span class="head-order-title">Loại mã:</span>
                                <span class="footer-order-title">Mã giảm giá toàn shop</span>
                            </div>
                            <div>
                                <span class="head-order-title">Số lượng:</span>
                                <span class="footer-order-title">300</span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div>
                                <span class="head-order-title">Loại voucher:</span>
                                <span>Khuyến mại</span>
                            </div>
                            <div>
                                <span class="head-order-title">Thời gian lưu mã giảm giá:</span>
                                <span>10:09 28/10/2020 đến 10:10 07/01/2021</span>
                            </div>
                            <div>
                                <span class="head-order-title">Sản phẩm được áp dụng: </span>
                                <span>Tất cả sản phẩm</span>
                            </div>
                            <div>
                                <span class="head-order-title">Đã lưu:</span>
                                <span>115</span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div>
                                <span class="head-order-title">Giá trị đơn hàng tối thiếu: </span>
                                <span>290000đ</span>
                            </div>
                            <div>
                                <span class="head-order-title">Mã voucher: </span>
                                <span>SFP - 62312737</span>
                            </div>
                            <div>
                                <span class="head-order-title">Chế độ hiển thị voucher: </span>
                                <span>Trang chủ shop</span>
                            </div>
                            <div>
                                <span class="head-order-title">Đã dùng:</span>
                                <span>19</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body-unit align-items-center justify-content-center mt-2">
                    <div class="table-responsive bor table-responsive-lg border_table">
                        <table class="table data-list-view table-sm">
                            <thead class="bg_table text-center">
                            <tr>
                                <th scope="col" class="pt-2">Mã đơn hàng</th>
                                <th scope="col" class="pt-2">Sản phẩm</th>
                                <th scope="col" class="pt-2">Mức giảm</th>
                                <th scope="col" class="pt-2">Tổng số tiền</th>
                                <th scope="col" class="pt-2">Ngày đặt hàng</th>
                                <th scope="col" class="pt-2">Trạng thái đơn hàng</th>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i = 0; $i < 6; $i++)
                                <tr class="head-content-tb">
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">201111PBVPBJMQ</span>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center justify-content-center">
                                            <img class="image" src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/product.png">
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">5.000đ</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">233.700đ</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">11/11/2020</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">Đã giao</span>
                                    </td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- content -->
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

