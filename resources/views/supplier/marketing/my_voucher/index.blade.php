<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="head-unit-first align-items-center justify-content-lg-between">
                    <div class="head-title">
                        <span>DANH SÁCH MÃ GIẢM GIÁ</span>
                        <p class="mt-2">Tạo Mã giảm giá toàn shop hoặc Mã giảm giá sản phẩm ngay bây giờ để thu hút người mua.</p>
                    </div>
                </div>
                <div class="tab-menu d-flex align-items-center justify-content-between p-0 mt-2">
                    <div class="d-flex tab-menu-f col-lg-6 col-md-12 p-0">
                        <div class="menu col-lg-3 col-md-12 checked voucher-status-all">
                            <center>
                                <a data-url-all="{{url('admin/marketing_supplier/my_voucher/all')}}">Tất cả</a>
                            </center>
                        </div>
                        <div class="menu col-lg-3 col-md-12 voucher-status-being">
                            <center>
                                <a data-url-being="{{url('admin/marketing_supplier/my_voucher/being')}}">Đang diễn ra</a>
                            </center>
                        </div>
                        <div class="menu col-lg-3 col-md-12 voucher-status-coming-up">
                            <center>
                                <a data-url-coming_up="{{url('admin/marketing_supplier/my_voucher/coming_up')}}">Sắp diễn ra</a>
                            </center>
                        </div>
                        <div class="menu col-lg-3 col-md-12 voucher-status-finish">
                            <center>
                                <a data-url-finish="{{url('admin/marketing_supplier/my_voucher/finish')}}">Đã kết thúc</a>
                            </center>
                        </div>
                    </div>
                    <div class="align-items-center justify-content-around">
                        <a href="">
                            <div class="btn-create">
                                <a href="{{url('admin/marketing_supplier/my_voucher/add')}}">
                                    <img class="pl-2 pr-1" src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/add.png">
                                    <span class="pr-2">Tạo mới</span>
                                </a>
                            </div>
                        </a>
                    </div>
                </div>
                <div id="show_content">

                </div>
            </div>
            <!-- content -->
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        let url_filter = window.location.origin;
        let url = url_filter + '/admin/marketing_supplier/my_voucher/all';
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $("#show_content").html(data);
            },
            error: function () {
                console.log('error')
            }
        });

        //Tất cả
        $(document).on("click", ".voucher-status-all", function () {
            let url = url_filter + '/admin/marketing_supplier/my_voucher/all';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.voucher-status-all').addClass('checked');
                    $('.voucher-status-being').removeClass('checked');
                    $('.voucher-status-coming-up').removeClass('checked');
                    $('.voucher-status-finish').removeClass('checked');
                    $("#show_content").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Đang diễn ra
        $(document).on("click", ".voucher-status-being", function () {
            let url = url_filter + '/admin/marketing_supplier/my_voucher/being';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.voucher-status-all').removeClass('checked');
                    $('.voucher-status-being').addClass('checked');
                    $('.voucher-status-coming-up').removeClass('checked');
                    $('.voucher-status-finish').removeClass('checked');
                    $("#show_content").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Sắp diễn ra
        $(document).on("click", ".voucher-status-coming-up", function () {
            let url = url_filter + '/admin/marketing_supplier/my_voucher/coming_up';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.voucher-status-all').removeClass('checked');
                    $('.voucher-status-being').removeClass('checked');
                    $('.voucher-status-coming-up').addClass('checked');
                    $('.voucher-status-finish').removeClass('checked');
                    $("#show_content").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Đã kết thúc
        $(document).on("click", ".voucher-status-finish", function () {
            let url = url_filter + '/admin/marketing_supplier/my_voucher/finish';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.voucher-status-all').removeClass('checked');
                    $('.voucher-status-being').removeClass('checked');
                    $('.voucher-status-coming-up').removeClass('checked');
                    $('.voucher-status-finish').addClass('checked');
                    $("#show_content").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        $(document).on("click", ".btn-active", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
        $(document).on("click", ".ending-soon", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    swal('Thông báo', data.msg, 'success');
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

