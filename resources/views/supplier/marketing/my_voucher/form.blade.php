<?php
$page = 'marketing';
use Illuminate\Support\Facades\File;use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">
<style>
    .active-product {
        background-color: #a9a9a9;
    }
</style>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="head-new-code mb-2">
                    <div class="title-new-code">
                        <span>TẠO MÃ GIẢM GIÁ</span>
                    </div>
                </div>
                <div class="d-flex content-new-code">
                    <div class="d-flex col-lg-8 col-md-12 p-0">
                        <div class="w-100">
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Loại mã:</label>
                                <div class="d-flex w-100 align-items-center justify-content-around">
                                    <div
                                        class="d-flex shop-or-product-unit align-items-center justify-content-lg-center">
                                        <label class="container_radio text-black text-bold-700">Voucher toàn shop
                                            <input value="0" type="radio" name="shop_or_product" id="shop_or_product"
                                                   class="check-r" <?= isset($data) && ($data->shop_or_product == 0) ? 'checked' : ''?>>
                                            <span class="radio_config"></span>
                                        </label>
                                    </div>
                                    <div
                                        class="d-flex d-flex shop-or-product-unit align-items-center justify-content-lg-center">
                                        <label class="container_radio text-black text-bold-700">Voucher sản phẩm
                                            <input value="1" type="radio" name="shop_or_product" id="shop_or_product"
                                                   class="check-r" <?= isset($data) && ($data->shop_or_product == 1) ? 'checked' : ''?>>
                                            <span class="radio_config"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <?php if(!isset($data)): ?>
                            <div class="form-group required product-voucher">
                                <label class="text-black text-bold-700 font-size-15">Chọn sản phẩm</label>
                                <div class="product-selected m-0 mb-1 row"
                                     style="border: 1px solid #D9D9D9; border-radius: 5px; padding: 2px">
                                    <?php if (isset($data_product)): ?>
                                    <?php if (count($data_product)): ?>
                                    <?php foreach ($data_product as $value): ?>
                                    <span class="btn btn-secondary mr-1 mb-1 btn-remove-product"
                                          data-id="<?= $value['id'] ?>"><?= $value['name'] ?><i class="fa fa-times"
                                                                                                aria-hidden="true"></i></span>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                                <input class="form-control mb-1" id="searchProduct" placeholder="Tìm kiếm sản phẩm"
                                       data-url="<?= URL::to('admin/marketing_supplier/my_voucher/search_product') ?>">
                                <?php
                                if (isset($data)) {
                                    $data_js = json_decode($data->product, true);
                                    $json_last_error = json_last_error();
                                }
                                ?>
                                <div class="form-control" id="product" style="height: 20vh; overflow-y: scroll">
                                    <?php foreach ($dataProduct as $value): ?>
                                    <div
                                        class="w-100 <?= isset($data) && !$json_last_error && in_array($value['id'], $data_js) ? 'active-product' : '' ?>">
                                        <div class="btn btn-product list-product" data-id="<?= $value['id'] ?>"
                                             data-name="<?= $value['name'] ?>"><?= $value['name'] ?></div>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <?php endif; ?>

                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Tên chương trình giảm
                                    giá:</label>
                                <input required type="text" name="title" id="title"
                                       value="<?= isset($data) ? $data->title : '' ?>" class="form-control input-new"
                                       placeholder="Tên chương trình giảm giá không chứa các ký tự đặc biệt">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Mã voucher:</label>
                                <input required type="text" name="code" id="code"
                                       value="<?= isset($data) ? $data->code : '' ?>" class="form-control input-new"
                                       placeholder="Mã voucher không chứa ký tự đặc biệt">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Thời gian lưu mã giảm
                                    giá:</label>
                                <div class="d-flex w-100">
                                    <input required type="datetime-local" name="name" id="time_start"
                                           value="<?= isset($data) ? date('Y-m-d\TH:i:s', strtotime($data['time_start'])) : date('Y-m-d\TH:i:s') ?>"
                                           class="form-control input-new mr-1">
                                    <input required type="datetime-local" name="name" id="time_end"
                                           value="<?= isset($data) ? date('Y-m-d\TH:i:s', strtotime($data['time_end'])) : date('Y-m-d\TH:i:s') ?>"
                                           class="form-control input-new ml-1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 ml-3">
                        <?php if(isset($data) && ($data->check == 1)): ?>
                        <div class="table-responsive bor table-responsive-lg border_table" style="width: 90%;">
                            <table class="table data-list-view table-sm">
                                <thead class="bg_table text-center">
                                <tr>
                                    <td>Tên sản phẩm</td>
                                </tr>
                                </thead>
                                <tbody class="text-center">
                                @if(isset($dataProductVC))
                                    @foreach($dataProductVC as $val)
                                        <tr>
                                            <td>{{$val->name}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="head-new-code mb-2 mt-3">
                    <div class="title-new-code">
                        <span>THIẾT LẬP MÃ GIẢM GIÁ</span>
                    </div>
                </div>
                <div class="d-flex content-new-code mb-3">
                    <div class="d-flex col-lg-8 col-md-12 p-0 mb-5">
                        <div class="w-100">
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Loại voucher:</label>
                                <div class="d-flex w-100 align-items-center justify-content-around">
                                    <div
                                        class="d-flex shop-or-product-unit align-items-center justify-content-lg-center">
                                        <label class="container_radio text-black text-bold-700">Voucher khuyến mại
                                            <input value="0" type="radio" name="discount_or_refund"
                                                   id="discount_or_refund"
                                                   class="check-r" <?= isset($data) && ($data->discount_or_refund == 0) ? 'checked' : ''?>>
                                            <span class="radio_config"></span>
                                        </label>
                                    </div>
                                    <div
                                        class="d-flex d-flex shop-or-product-unit align-items-center justify-content-lg-center">
                                        <label class="container_radio text-black text-bold-700">Voucher Hoàn xu
                                            <input value="1" type="radio" name="discount_or_refund"
                                                   id="discount_or_refund"
                                                   class="check-r" <?= isset($data) && ($data->discount_or_refund == 1) ? 'checked' : ''?>>
                                            <span class="radio_config"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Loại giảm giá / mức
                                    giảm:</label>
                                <div class="d-flex w-100">
                                    <select id="type_discount" class="form-control option-new mr-1 type_all">
                                        <option
                                            value="0" <?= isset($data) && $data->type_discount == 0 ? 'selected' : ''?>>
                                            Theo số tiền
                                        </option>
                                        <option
                                            value="1" <?= isset($data) && $data->type_discount == 1 ? 'selected' : ''?>>
                                            Theo phần trăm
                                        </option>
                                    </select>
                                    <select id="type_discount" class="form-control option-new mr-1 type_percent"
                                            disabled>
                                        <option
                                            value="1" <?= isset($data) && $data->type_discount == 1 ? 'selected' : ''?>>
                                            Theo phần trăm
                                        </option>
                                    </select>
                                    <input required type="number" id="value_discount"
                                           value="<?= isset($data) ? $data->value_discount : '' ?>"
                                           class="form-control input-new ml-1">
                                </div>
                            </div>
                            <div class="value_max">
                                <div class="form-group d-flex align-items-center">
                                    <label class="font-weight-bold font-size-14" style="width: 300px">Mức giảm tối
                                        đa:</label>
                                    <input required type="number" id="discount_value_max"
                                           value="<?= isset($data) ? $data->discount_value_max : '' ?>"
                                           class="form-control input-new" placeholder="Mức giảm tối đa">
                                </div>
                            </div>
                            <div class="value_max_refund">
                                <div class="form-group d-flex align-items-center">
                                    <label class="font-weight-bold font-size-14" style="width: 300px">Mức giảm tối đa
                                        (xu):</label>
                                    <input required type="number" id="discount_value_max_refund"
                                           value="<?= isset($data) && ($data->discount_or_refund == 1) ? $data->discount_value_max : '' ?>"
                                           class="form-control input-new" placeholder="Mức giảm xu tối đa">
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Giá trị đơn hàng tối
                                    thiểu:</label>
                                <input required type="number" id="min_order_value"
                                       value="<?= isset($data) ? $data->min_order_value : '' ?>"
                                       class="form-control input-new">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Số mã có thể
                                    lưu:</label>
                                <input required type="number" id="number_save"
                                       value="<?= isset($data) ? $data->number_save : '' ?>"
                                       class="form-control input-new" placeholder="Tổng số mã giảm giá có thể lưu ">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Số mã có thể sử
                                    dụng:</label>
                                <input required type="number" id="number_available"
                                       value="<?= isset($data) ? $data->number_available : '' ?>"
                                       class="form-control input-new" placeholder="Tổng số mã giảm giá có thể sử dụng">
                            </div>
                            <div class="form-group d-flex">
                                <label class="font-weight-bold font-size-14" style="width: 250px">Hình ảnh:</label>
                                <div class="form-label-group">
                                    <img width="120px" height="120px" id="avatar" class="thumbnail image-show-qr"
                                         src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/images/avt.jpg' ?>">
                                    <br>
                                    <input type='file' value="" accept="image/*" name="image" id="image"
                                           onchange="changeImg(this)">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex col-lg-4 col-md-12 align-items-lg-end justify-content-center btn-unit">
                        <div class="button-new-code mr-1">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/my_voucher') ?>">Hủy</a>
                            </center>
                        </div>
                        <?php if(!isset($data)) :?>
                        <div class="">
                            <button class="btn btn-main-round btn-add-voucher"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/marketing_supplier/my_voucher/save') ?>"
                            >Xác nhận
                            </button>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(".value_max").hide();
        $(".value_max_refund").hide();
        $('.product-voucher').hide();
        $('.type_percent').hide();
        $(document).on("change", "#type_discount", function () {
            if ($(this).val() == '1') {
                $(".value_max").show();
            } else {
                $(".value_max").hide();
            }
        });
        $(document).on('click', '#discount_or_refund', function () {
            if ($(this).val() == 1) {
                $('.type_all').hide();
                $('.type_percent').show();
                $('.value_max_refund').show();
                $('.value_max').hide();
            } else {
                $('.type_percent').hide();
                $('.type_all').show();
                $('.value_max_refund').hide();
            }
        });
        $(document).on("click", "#shop_or_product", function () {
            if ($(this).val() == '1') {
                $('.product-voucher').show();
            } else {
                $('.product-voucher').hide();
            }
        });
        $(document).on("click", ".btn-add-voucher", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            let number_save = $('#number_save').val();
            let number_available = ($('#number_available')).val();
            let product = [];
            $(".btn-remove-product").each(function () {
                let product_id = $(this).attr('data-id');
                product.push(product_id);
            });
            if (number_available >= number_save) {
                swal('Thông báo', 'Số mã có thể sử dụng phải nhỏ hơn hoặc bằng số mã có thể lưu', 'error');
            } else {
                form_data.append('id', $(this).attr('data-id'));
                form_data.append('title', $('#title').val());
                form_data.append('code', $('#code').val());
                form_data.append('time_start', $('#time_start').val());
                form_data.append('time_end', $('#time_end').val());
                form_data.append('type_discount', $('#type_discount').val());
                form_data.append('value_discount', $('#value_discount').val());
                form_data.append('min_order_value', $('#min_order_value').val());
                form_data.append('number_save', $('#number_save').val());
                form_data.append('number_available', $('#number_available').val());
                form_data.append('discount_value_max', $('#discount_value_max').val());
                form_data.append('discount_value_max_refund', $('#discount_value_max_refund').val());
                form_data.append('image', $('#image')[0].files[0]);
                form_data.append('product_id', JSON.stringify(product));

                let shop_or_product = $('input[name="shop_or_product"]:checked').val();
                let discount_or_refund = $('input[name="discount_or_refund"]:checked').val();
                form_data.append('shop_or_product', shop_or_product);
                form_data.append('discount_or_refund', discount_or_refund);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    type: "POST",
                    data: form_data,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        showLoading();
                    },
                    success: function (data) {
                        if (data.status) {
                            swal('Thành công', data.msg, 'success');
                            window.location.href = data.url;
                        } else {
                            swal('Lỗi', data.msg, 'error');
                        }
                    },
                    error: function () {
                        console.log('error')
                    },
                    complete: function () {
                        hideLoading();
                    }
                });
            }
        });
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
        $(document).on('click', '.btn-product', function () {
            let product_id = $(this).attr('data-id');
            let name = $(this).attr('data-name');
            let hasClass = $(this).parent().hasClass('active-product');
            if (hasClass) {
                $(".btn-remove-product").each(function () {
                    let pr_id = $(this).attr('data-id');
                    if (pr_id === product_id) {
                        $(this).remove();
                        return false;
                    }
                });
            } else {
                let html = '<span class="btn btn-secondary mr-1 mb-1 btn-remove-product" data-id="' + product_id + '">' + name + '<i class="fa fa-times" aria-hidden="true"></i></span>';
                $('.product-selected').append(html);
            }
            $(this).parent().toggleClass("active-product");
        });
        $(document).on("click", ".btn-remove-product", function () {
            let product_id = $(this).attr('data-id');
            $(".active-product").each(function () {
                let pr_id = $(this).find('.list-product:first').attr('data-id');
                if (pr_id === product_id) {
                    $(this).removeClass('active-product');
                    return false;
                }
            });
            $(this).remove();
        });
        $(document).on("keyup", "#searchProduct", function () {
            let url = $(this).attr('data-url');
            let keyword = $(this).val();
            let data = {};
            let product = [];
            $(".btn-remove-product").each(function () {
                let pr_id = $(this).attr('data-id');
                product.push(pr_id);
            });
            data['key'] = keyword;
            data['product'] = JSON.stringify(product);
            data['id'] = $('.btn-save').attr('data-id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                success: function (data) {
                    $('#product').html(data.html);
                },
                error: function () {
                    console.log('error')
                }
            });
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

