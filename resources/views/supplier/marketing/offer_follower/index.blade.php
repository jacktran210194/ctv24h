<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="head-unit-first align-items-center justify-content-lg-between">
                    <div class="head-title">
                        <span>ƯU ĐÃI FOLLOWER</span>
                    </div>
                </div>
                <div class="tab-menu d-flex align-items-center justify-content-between p-0 mt-2">
                    <div class="d-flex tab-menu-f col-lg-6 col-md-12 p-0">
                        <div class="menu col-lg-3 col-md-12 <?= $type === 'all' ? 'checked'  :'' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/offer_follower?type=all') ?>">Tất cả</a>
                            </center>
                        </div>
                        <div class="menu col-lg-3 col-md-12 <?= $type === 'being' ? 'checked'  :'' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/offer_follower?type=being') ?>">Đang diễn ra</a>
                            </center>
                        </div>
                        <div class="menu col-lg-3 col-md-12 <?= $type === 'coming_up' ? 'checked'  :'' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/offer_follower?type=coming_up') ?>">Sắp diễn ra</a>
                            </center>
                        </div>
                        <div class="menu col-lg-3 col-md-12 <?= $type === 'finish' ? 'checked'  :'' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/offer_follower?type=finish') ?>">Đã kết thúc</a>
                            </center>
                        </div>
                    </div>
                    <div class="align-items-center justify-content-around">
                        <a href="">
                            <div class="btn-create">
                                <a href="{{url('admin/marketing_supplier/offer_follower/add')}}">
                                    <img class="pl-2 pr-1" src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/add.png">
                                    <span class="pr-2">Tạo ưu đãi follower</span>
                                </a>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="body-unit align-items-center justify-content-center mt-3">
                    <div class="table-responsive bor table-responsive-lg border_table">
                        <table class="table data-list-view table-sm">
                            <thead class="bg_table text-center">
                            <tr>
                                <th scope="col">Tên chương trình khuyến mãi</th>
                                <th scope="col">Lượt sử dụng tối đa</th>
                                <th scope="col">Người theo dõi mới</th>
                                <th scope="col">Trạng thái</th>
                                <th scope="col">Thời gian</th>
                                <th scope="col">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i = 0; $i < 6; $i ++)
                                <tr class="head-content-tb">
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">Follow Phiếu ưu đãi Grab Express </span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700">300</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700">15</span>
                                    </td>
                                    <td>
                                        <div class="bg-blue bg-span text-center align-items-center justify-content-center btn-status">
                                            Đang diễn ra
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">18-08-2021 đến 26-0802021</span>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{url('admin/marketing_supplier/offer_follower/detail')}}" class="text-blue text-underline text-bold-700">Chi tiết</a>
                                    </td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- content -->
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

