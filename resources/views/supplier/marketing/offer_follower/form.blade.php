<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="head-new-code mb-2">
                    <div class="title-new-code">
                        <span>TẠO ƯU ĐÃI FOLLOWER MỚI</span>
                    </div>
                </div>
                <div class="d-flex content-new-code">
                    <div class="d-flex col-lg-8 col-md-12 p-0">
                        <div class="w-100">
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Tên ưu đãi:</label>
                                <div class="d-flex w-100">
                                        <input required type="text" name="name" id="name"
                                               value="" class="form-control input-new mr-1">
                                        <input required type="text" name="name" id="name"
                                               value="" class="form-control input-new ml-1">
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Thời gian lưu ưu đãi follower:</label>
                                <div class="d-flex w-100">
                                    <input required type="datetime-local" name="name" id="name"
                                           value="" class="form-control input-new mr-1">
                                    <input required type="datetime-local" name="name" id="name"
                                           value="" class="form-control input-new ml-1">
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Ngày hết hạn:</label>
                                <input required type="text" name="name" id="name"
                                       value="" class="form-control input-new" placeholder="Thời gian nhận voucher 7 ngày" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="head-new-code mb-2 mt-3">
                    <div class="title-new-code">
                        <span>THIẾT LẬP ƯU ĐÃI FOLLOWER</span>
                    </div>
                </div>
                <div class="d-flex content-new-code mb-3">
                    <div class="d-flex col-lg-8 col-md-12 p-0 mb-5">
                        <div class="w-100">
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Loại ưu đãi:</label>
                                <div class="d-flex w-100">
                                    <input required type="text" name="name" id="name"
                                           value="" class="form-control input-new mr-1" placeholder="Voucher" disabled>
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Hình thức ưu đãi:</label>
                                <div class="d-flex w-100">
                                    <input required type="text" name="name" id="name"
                                           value="" class="form-control input-new mr-1">
                                    <input required type="text" name="name" id="name"
                                           value="" class="form-control input-new ml-1">
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Loại khuyến mãi:</label>
                                <div class="d-flex w-100">
                                    <select name="" id="" class="form-control option-new mr-1">
                                        <option value="">Theo số tiền</option>
                                        <option value="">Theo phần trăm</option>
                                    </select>
                                    <input required type="text" name="name" id="name"
                                           value="" class="form-control input-new ml-1" placeholder="Nhập...">
                                </div>

                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Giá trị đơn hàng tối thiểu:</label>
                                <input required type="number" name="name" id="name"
                                       value="" class="form-control input-new" placeholder="Nhập...">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Lượt sử dụng tối đa:</label>
                                <input required type="text" name="name" id="name"
                                       value="" class="form-control input-new" placeholder="Tổng số lượt sử dụng mã tối đa">
                            </div>
                        </div>
                    </div>
                    <div class="d-flex col-lg-4 col-md-12 align-items-lg-end justify-content-center btn-unit">
                        <div class="button-new-code mr-1">
                            <center>
                                <a href="">Hủy</a>
                            </center>
                        </div>
                        <div class="button-new-code">
                            <center>
                                <a href="">Xác nhận</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

