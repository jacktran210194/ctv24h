<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">
<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h3 class="text-bold-700 f-24 mb-3">{{$title}}</h3>
                <div class="tab-menu d-flex align-items-center justify-content-between p-0 mt-2 mb-3">
                    <div class="d-flex tab-menu-f col-lg-6 col-md-12 p-0">
                        <div class="menu col-lg-3 col-md-12 checked status-all">
                            <center>
                                <a data-url-all="{{url('admin/marketing_supplier/promotion/all')}}">Tất cả</a>
                            </center>
                        </div>
                        <div class="menu col-lg-3 col-md-12 status-being">
                            <center>
                                <a data-url-being="{{url('admin/marketing_supplier/promotion/being')}}">Đang diễn ra</a>
                            </center>
                        </div>
                        <div class="menu col-lg-3 col-md-12 status-coming-up">
                            <center>
                                <a data-url-coming_up="{{url('admin/marketing_supplier/promotion/coming_up')}}">Sắp diễn ra</a>
                            </center>
                        </div>
                        <div class="menu col-lg-3 col-md-12 status-finish">
                            <center>
                                <a data-url-finish="{{url('admin/marketing_supplier/promotion/finish')}}">Đã kết thúc</a>
                            </center>
                        </div>
                    </div>
                    <div class="align-items-center justify-content-around">
                        <a href="">
                            <div class="btn-create">
                                <a href="{{url('admin/marketing_supplier/promotion/add')}}">
                                    <img class="pl-2 pr-1" src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/add.png">
                                    <span class="pr-2">Tạo chương trình của tôi</span>
                                </a>
                            </div>
                        </a>
                    </div>
                </div>
                <div id="show_content">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            $('#myModal').modal('show');
        });

        let url_filter = window.location.origin;
        let url = url_filter + '/admin/marketing_supplier/promotion/all';
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $("#show_content").html(data);
            },
            error: function () {
                console.log('error')
            }
        });

        //Tất cả
        $(document).on("click", ".status-all", function () {
            let url = url_filter + '/admin/marketing_supplier/promotion/all';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.status-all').addClass('checked');
                    $('.status-being').removeClass('checked');
                    $('.status-coming-up').removeClass('checked');
                    $('.status-finish').removeClass('checked');
                    $("#show_content").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Đang diễn ra
        $(document).on("click", ".status-being", function () {
            let url = url_filter + '/admin/marketing_supplier/promotion/being';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.status-all').removeClass('checked');
                    $('.status-being').addClass('checked');
                    $('.status-coming-up').removeClass('checked');
                    $('.status-finish').removeClass('checked');
                    $("#show_content").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Sắp diễn ra
        $(document).on("click", ".status-coming-up", function () {
            let url = url_filter + '/admin/marketing_supplier/promotion/coming_up';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.status-all').removeClass('checked');
                    $('.status-being').removeClass('checked');
                    $('.status-coming-up').addClass('checked');
                    $('.status-finish').removeClass('checked');
                    $("#show_content").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Đã kết thúc
        $(document).on("click", ".status-finish", function () {
            let url = url_filter + '/admin/marketing_supplier/promotion/finish';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.status-all').removeClass('checked');
                    $('.status-being').removeClass('checked');
                    $('.status-coming-up').removeClass('checked');
                    $('.status-finish').addClass('checked');
                    $("#show_content").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });
    });
    $(document).on('click', '.popup_default', function () {
       swal('Thông báo', 'Tính năng đang phát triển', 'warning');
    });

</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

