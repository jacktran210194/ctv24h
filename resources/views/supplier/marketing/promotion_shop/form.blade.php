<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">
<!-- BEGIN: Header-->
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background: #D1292F;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h3 class="text-bold-700 f-24 mb-5">{{$title}}</h3>
                <div class="form_content mt-2">
                    <div class="form-group form-row">
                        <div class="col-2">
                            <label class="text-bold-700">Tên chương trình giảm giá</label>
                        </div>
                        <div class="col-6">
                            <input type="text" class="form-control input_config" id="name">
                            <p class="mt-1 text-sliver text-bold-700">Tên chương trình khuyến mãi không hiển thị với
                                người
                                mua</p>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <div class="col-2">
                            <label class="text-bold-700">Thời gian khuyến mãi</label>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="datetime-local" class="form-control input_config" value="{{date('Y-m-d\TH:i:s')}}" id="time_start">
                                </div>
                                <div class="col-lg-6">
                                    <input type="datetime-local" class="form-control input_config" value="{{date('Y-m-d\TH:i:s')}}" id="time_stop">
                                </div>
                            </div>
                            <p class="mt-1 text-sliver text-bold-700">Thời gian của chương trình không được quá 180 ngày
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-between">
                    <div>
                        <h3 class="text-bold-700 f-24">SẢN PHẨM KHUYẾN MÃI</h3>
                        <p class="text-sliver">Tổng cộng có 2 sản phẩm</p>
                    </div>
                    <div>
                        <button class="btn btn_main btn_add_product">
                            <img src="../../assets/admin/app-assets/images/icon_dashboard/icon_add.png" alt=""> Thêm sản
                            phẩm
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card fix-unit">
            <div class="card-body">
                <div class="content_info">
                    <h3 class="text-bold-700 f-16">Chỉnh sửa hàng loạt</h3>
                    <p>Đã chọn 0 phân loại hàng</p>
                </div>
                <div class="row mb-2">
                    <div class="col-3">
                        <label class="text-bold-700">Khuyến mãi (%)</label>
                        <input type="number" class="form-control">
                    </div>
                    <div class="col-3">
                        <label class="text-bold-700">SL khuyến mãi</label>
                        <select class="form-control" id="">
                            <option value="">Không giới hạn</option>
                        </select>
                    </div>
                    <div class="col-3">
                        <label class="text-bold-700">Giới hạn đặt hàng</label>
                        <select class="form-control" id="">
                            <option value="">Không giới hạn</option>
                        </select>
                    </div>
                    <div class="col-3 position-absolute" style="right: 0; bottom: 25%;">
                        <button class="btn btn_main mr-1">Cập nhật hàng loạt</button>
                        <button class="btn border">Xoá</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card table-product">
            <div class="card-body">
                <div class="table table-responsive table-responsive-lg border_table">
                    <table class="table-hover table">
                        <thead class="bg_table">
                        <tr class="text-center text-bold-700">
                            <th><input type="checkbox" class="popup_default"></th>
                            <th class="text-left">Tên sản phẩm</th>
                            <th>Giá gốc</th>
                            <th width="12%">Giá sau giảm</th>
                            <th>Giảm giá</th>
                            <th>Kho hàng <i class="fa fa-info-circle"></i></th>
                            <th>SL SP KM <i class="fa fa-info-circle"></i></th>
                            <th>Bật/Tắt</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(isset($dataPromotion))
                                @foreach($dataPromotion as $__val)
                                <tr class="text-center text-bold-700">
                                    <td>
                                        <input type="checkbox" class="check-pr" name="productID[]" value="{{$__val->product_id}}" data-product-id="<?= isset($__val) ? $__val->product_id : '' ?>">
                                    </td>
                                    <td class="text-left">
                                        <div class="d-flex align-items-center">
                                            <img width="60px" height="60px" class="border_radius mr-1"
                                                 src="{{$__val->image}}">
                                            <p style="width: 120px!important;" class="m-0">{{$__val->name_product}}</p>
                                        </div>
                                    </td>
                                    <td>{{number_format($__val->price)}}đ</td>
                                    <td><input type="number" class="form-control" value="600000"></td>
                                    <td><input type="number" class="form-control" min="1" max="3" value="10"></td>
                                    <td>{{$__val->quantity}}</td>
                                    <td>
                                        <select class="form-control" id="">
                                            <option value="">Không giới hạn</option>
                                        </select>
                                    </td>
                                    <td>
                                        <label class="switch">
                                            <input class="btn_check" checked type="checkbox">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <button class="btn btn-delete"
                                                data-url="{{url('admin/marketing_supplier/promotion/delete_product/'.$__val->id)}}"
                                                data-msg="Bạn có muốn xóa sản phẩm này không?"
                                        ><i class="fa fa-trash fa-2x text-red"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>Không có sản phẩm</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="float-right">
            <button style="width: 120px!important; height: 37px!important;" class="btn btn_grey_border_20 mr-1">Huỷ</button>
            <button style="width: 120px!important; height: 37px!important;" class="btn btn_main_border_20 btn-create-promotion"
                    data-url="{{url('admin/marketing_supplier/promotion/save_promotion')}}"
            >Xác nhận</button>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="popup-add-product position-fixed position-center">
    <div class="container-popup-add-product">
        <div class="header-popup-add-product mb-2">
            <p class="text-black text-bold-700 f-18">Chọn sản phẩm</p>
        </div>
        <div class="tab-menu d-flex align-items-center justify-content-start p-0 mt-2">
            <div class="d-flex tab-menu-f col-lg-5 col-md-12 p-0">
                <div class="menu col-lg-6 col-md-12 <?= $type_up && $type_up == 'choose' ? 'checked-menu-tab' : '' ?>">
                    <center>
                        <a class="check-white"
                           href="<?= URL::to('admin/marketing_supplier/promotion/add?type_up=choose') ?>">Chọn</a>
                    </center>
                </div>
                <div
                    class="menu col-lg-6 col-md-12 <?= $type_up && $type_up == 'list_product' ? 'checked-menu-tab' : '' ?>">
                    <center>
                        <a class="check-white"
                           href="<?= URL::to('admin/marketing_supplier/promotion/add?type_up=list_product') ?>">Danh
                            sách sản phẩm</a>
                    </center>
                </div>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-lg-between mt-2">
            <div class="col-lg-4 col-md-12 p-0 d-flex form-group align-items-center justify-content-lg-between">
                <div class="col-lg-4 p-0">
                    <span class="text-black text-bold-700">Ngành hàng</span>
                </div>
                <div class="col-lg-8 p-0">
                    <select name="" id="" class="form-control">
                        <option value="">Tất cả</option>
                        @if(isset($dataCategoryProduct))
                            @foreach($dataCategoryProduct as $val)
                                <option value="{{$val->id}}">{{$val->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-lg-5 col-md-12 p-0 d-flex form-group align-items-center justify-content-lg-between">
                <div class="col-lg-3">
                    <span class="text-black text-bold-700">Tìm kiếm</span>
                </div>
                <div class="col-lg-4 p-0">
                    <select name="" id="" class="form-control">
                        <option>Tên sản phẩm</option>
                        <option>Mã SKU</option>
                    </select>
                </div>
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="tab-menu d-flex align-items-center justify-content-between p-0 mt-2">
            <div class="d-flex tab-menu-f col-lg-3 col-md-12 p-0">
                <div
                    class="menu col-lg-6 col-md-12 <?= $type_down && $type_down == 'search' ? 'checked-menu-tab' : '' ?>">
                    <center>
                        <a class="check-white"
                           href="<?= URL::to('admin/marketing_supplier/promotion/add?type_down=search') ?>">Tìm</a>
                    </center>
                </div>
                <div
                    class="menu col-lg-6 col-md-12 <?= $type_down && $type_down == 'reset' ? 'checked-menu-tab' : '' ?>">
                    <center>
                        <a class="check-white"
                           href="<?= URL::to('admin/marketing_supplier/promotion/add?type_down=reset') ?>">Nhập lại</a>
                    </center>
                </div>
            </div>
            <div class="d-flex align-items-center justify-content-center">
                <input type="checkbox" class="mr-1">
                <span>Xem sản phẩm có sẵn</span>
            </div>
        </div>
        <div class="body-unit align-items-center justify-content-center mt-3">
            <div class="table-responsive bor table-responsive-lg border_table">
                <table class="table data-list-view table-sm">
                    <thead class="bg_table text-center get-all">
                    <tr>
                        <th scope="col">
                            <input type="checkbox" class="check-all">
                        </th>
                        <th scope="col">Sản phẩm</th>
                        <th scope="col">Loại sản phẩm</th>
                        <th scope="col">Đã bán</th>
                        <th scope="col">Giá</th>
                        <th scope="col">Kho hàng</th>
                    </tr>
                    </thead>
                    <tbody class="text-center get-product">
                    @if(count($dataProduct))
                        @foreach($dataProduct as $value)
                            <tr>
                                <td>
                                    <input type="checkbox" class="check-product" name="product[]" value="{{$value->id}}"
                                           data-id="{{$value->id}}"
                                    >
                                </td>
                                <td>
                                    <div class="d-flex align-items-center justify-content-start m-0" style="width: 400px!important;">
                                        <img style="width: 75px; height: 75px; border-radius: 14px;" class="image"
                                             src="{{$value->image}}">
                                        <div class="product-info align-items-center justify-content-start">
                                            <p class="name-product">{{$value->name}} </p>
                                            <p class="code-product">Mã: {{$value->sku_type}}</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <span class="text-blue text-bold-700">{{$value->type_product}}</span>
                                </td>
                                <td class="text-center">
                                    <span class="text-bold text-bold-700">{{$value->is_sell}}</span>
                                </td>
                                <td class="text-center">
                                    <span class="text-red text-bold-700"
                                          data-price="{{$value->price}}" name="price[]"
                                    >{{number_format($value->price)}}đ</span>
                                </td>
                                <td class="text-center">
                                    <span class="text-red text-bold-700">{{$value->warehouse}}</span>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="footer-popup d-flex align-items-center justify-content-lg-end mt-2 position-fixed"
             style="right: 5%; top: 45%;">
            <button class="btn btn-cancel-add-product mr-1">
                Hủy
            </button>
            <button class="btn btn-confirm-add-product"
                    data-url="{{url('admin/marketing_supplier/promotion/add_product')}}"
                    data-price-before="{{$value->price}}"
            >
                Xác nhận
            </button>
        </div>
    </div>
</div>
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        // $('.table-product').hide();
        // $('.fix-unit').hide();
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            jQuery.noConflict();
            $('#myModal').modal('show');
        });

        $(document).on("click", ".btn_add_product", function () {
            $('.popup-add-product').addClass('show-popup');
        });
        $(document).on('click', '.btn-cancel-add-product', function () {
            $('.popup-add-product').removeClass('show-popup');
        });

        $(document).on('click', '.btn-confirm-add-product', function () {
            let product = [];
            $("[name='product[]']").each(function () {
                if ($(this).is(":checked")) {
                    product.push($(this).attr('data-id'));
                }
            });

            if(product.length == 0){
                swal('Thông báo', 'Vui lòng chọn ít nhất 1 sản phẩm', 'warning');
                return false;
            }
            let url = $(this).attr('data-url');
            let data = {};
            data['product'] = product;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        $('.popup-add-product').removeClass('show-popup');
                        swal("Thành công", data.msg, "success");
                        window.location.reload();
                        // window.location.href = data.url;
                        // $('.table-product').show();
                        // $('.fix-unit').show();
                    } else {
                        swal("Lỗi", data.msg, "warning");
                    }
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        $(document).on('click', '.btn-create-promotion', function () {
            let product_id = [];
            $("[name='productID[]']").each(function () {
                if ($(this).is(":checked")) {
                    product_id.push($(this).attr('data-product-id'));
                }
            });
            if(product_id.length == 0){
                swal('Thông báo', 'Vui lòng chọn ít nhất 1 sản phẩm', 'warning');
                return false;
            }
            let url = $(this).attr('data-url');
            let data = {};
            data['name'] = $('#name').val();
            data['time_start'] = $('#time_start').val();
            data['time_stop'] = $('#time_stop').val();
            data['product_id'] = product_id;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        swal("Thành công", data.msg, "success");
                        window.location.href = data.url;
                    } else {
                        swal("Lỗi", data.msg, "warning");
                    }
                },
                error: function () {
                    console.log('error')
                }
            });
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

