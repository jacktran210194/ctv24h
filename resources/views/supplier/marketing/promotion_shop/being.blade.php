<div class="d-flex align-items-center justify-content-start mb-2">
    <div class="d-flex align-items-center mr-2">
        <label style="width: 150px!important;" class="text-bold-700">Thời gian khuyến mãi</label>
        <input type="date" style="width: 35%!important;" class="form-control" value="{{date('Y-m-d')}}">
        <input type="date" style="width: 35%!important;" class="form-control" value="{{date('Y-m-d')}}">
    </div>
    <div class="d-flex align-items-center mr-1">
        <label style="width: 100px!important;" class="text-bold-700">Tìm kiếm</label>
        <input type="text" class="form-control">
    </div>
    <div class="d-flex align-items-center">
        <button class="btn btn_main mr-1 popup_default">Tìm kiếm</button>
        <button class="btn btn-outline-danger popup_default">Nhập lại</button>
    </div>
</div>
<div class="table table-responsive table-lg table-hover border_table">
    <table class="table">
        <thead class="bg_table">
        <tr class="text-center text-bold-700">
            <th class="text-left">Tên chương trình</th>
            <th class="text-left">Sản phẩm</th>
            <th>Tình trạng</th>
            <th>Thời gian</th>
            <th width="10%">Thao tác</th>
        </tr>
        </thead>
        <tbody>
        @if(count($data))
            @foreach($data as $value)
                <tr class="text-center text-bold-700">
                    <td class="text-left">{{$value->name}}</td>
                    <td class="text-left" style="width: 300px!important;">
                        <div class="d-flex align-items-center">
                            @if(isset($product))
                                @foreach($product as $val)
                                    @if($value->id == $val->promotion_id)
                                        <img width="60" height="60" class="border_radius mr-1" src="{{$val->image}}" alt="">
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </td>
                    <td>
                        <div style="margin: 0 auto" class="bg-blue">Đang diễn ra</div>
                    </td>
                    <td>
                        {{date('d-m-Y', strtotime($value->time_start))}} đến {{date('d-m-Y', strtotime($value->time_stop))}}
                    </td>
                    <td class="text-blue text-underline text-center">
                        <a class="popup_default">Chi tiết</a>
                        <br>
                        <a class="popup_default">Sao chép</a>
                        <br>
                        <a class="popup_default">Dữ liệu</a>
                        <br>
                        <a class="popup_default">Thêm</a>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td>Không có dữ liệu</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
