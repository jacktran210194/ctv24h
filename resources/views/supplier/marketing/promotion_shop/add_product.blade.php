<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">
<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h3 class="text-bold-700 mb-2">{{$title}}</h3>
                <div class="tab-bar-page">
                    <ul class="tab-bar-content d-flex align-items-center justify-content-lg-start flex-wrap">
                        <li class="d-flex align-items-center justify-content-lg-center active product_pick">
                            <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Chọn</a>
                        </li>
                        <li class="d-flex align-items-center justify-content-lg-center product_import_ajax">
                            <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Danh sách sản phẩm</a>
                        </li>
                    </ul>
                </div>
                <!-- Chọn sản phẩm -->
                <div class="get_data_ajax"></div>
                <!-- Đăng danh sách sản phẩm -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        let url_filter = window.location.origin;
        let url = url_filter + '/admin/marketing_supplier/flash_sale/get_list_product';
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $(".get_data_ajax").html(data);
            },
            error: function () {
                console.log('error')
            }
        });
        $(document).on('click', '.btn_import', function () {
            $('#input_import').click();
        });

        //ajax get data
        $(document).on('click', '.product_pick', function () {
            let url = url_filter + '/admin/marketing_supplier/flash_sale/get_list_product';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.product_pick').addClass('active');
                    $('.product_import_ajax').removeClass('active');
                    $(".get_data_ajax").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        $(document).on('click', '.product_import_ajax', function () {
            let url = url_filter + '/admin/marketing_supplier/flash_sale/get_import_product';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.product_import_ajax').addClass('active');
                    $('.product_pick').removeClass('active');
                    $(".get_data_ajax").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->

</html>

