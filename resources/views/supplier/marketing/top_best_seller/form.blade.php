<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="head-unit-first align-items-center justify-content-lg-between">
                    <div class="head-title">
                        <span>THÊM DANH MỤC</span>
                    </div>
                </div>
                <div class="d-flex content-new-code mt-3">
                    <div class="d-flex col-lg-8 col-md-12 p-0">
                        <div class="w-100">
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Tên danh sách:</label>
                                <input required type="number" name="name" id="name"
                                       value="" class="form-control input-new" placeholder="Tên bộ sưu tập không hiển thị với người mua ">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content -->
        </div>
        <div class="card">
            <div class="card-content m-2">
                <div class="d-flex head-unit-first align-items-center justify-content-lg-between">
                    <div class="head-title">
                        <span>SẢN PHẨM</span>
                        <p class="text-grey text-bold-700 mt-1">Vui lòng chọn 4 - 8 sản phẩm cho mỗi danh mục</p>
                    </div>
                    <div class="btn-create">
                        <a href="{{url('admin/marketing_supplier/top_best_seller/add_product')}}">
                            <img class="pl-2 pr-1" src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/add.png">
                            <span class="pr-2">Thêm sản phẩm</span>
                        </a>
                    </div>
                </div>
                <div class="d-flex body-unit justify-content-lg-between mt-3">
                    <div class="col-lg-8 col-md-12 p-0">
                        <div class="table-responsive table-responsive-lg border_table">
                            <table class="table data-list-view table-sm">
                                <thead class="bg_table text-center">
                                <tr>
                                    <th scope="col">Sản phẩm</th>
                                    <th scope="col">Giá gốc</th>
                                    <th scope="col">Đã bán</th>
                                    <th scope="col">Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                @for($i = 0; $i < 4; $i++)
                                    <tr class="head-content-tb">
                                        <td class="text-center">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_4.png">
                                        </td>
                                        <td class="text-center">
                                           <span class="text-red text-bold-700">725.000đ</span>
                                        </td>
                                        <td class="text-center">
                                            <span class="text-red text-bold-700">20</span>
                                        </td>
                                        <td class="text-center">
                                            <div class="d-flex align-items-center justify-content-center position-relative">
                                                <a href="">
                                                    <button class="btn d-flex align-items-center" title="Sửa"><img src="../../assets/admin/app-assets/images/ncc/marketing/magiamgiacuactv24/delete.png">
                                                        <span class="color-blue font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Xóa</span>
                                                    </button>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 p-0 align-items-center justify-content-center">
                        <div class="text-center">
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/iphone.png">
                        </div>
                    </div>
                </div>
            </div>
            <!-- content -->
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-active", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            $('#myModal').modal('show');
        });

        $(document).on("click", ".show-options", function () {
            $(this).next().toggle('fast');
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

