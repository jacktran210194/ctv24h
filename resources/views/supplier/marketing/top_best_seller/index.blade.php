<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="head-unit-first align-items-center justify-content-lg-between">
                    <div class="head-title">
                        <span>TOP SẢN PHẨM BÁN CHẠY</span>
                    </div>
                </div>
                <div class="d-flex head-unit-second align-items-center justify-content-lg-between mt-2">
                    <div>
                        <span class="text-red text-bold-700 f-18">60 Đơn hàng</span>
                    </div>
                    <div class="btn-create">
                        <a href="{{url('admin/marketing_supplier/top_best_seller/create')}}">
                            <img class="pl-2 pr-1" src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/add.png">
                            <span class="pr-2">Thêm danh mục</span>
                        </a>
                    </div>
                </div>
                <div class="d-flex body-unit justify-content-lg-between mt-3">
                    <div class="col-lg-8 col-md-12 p-0">
                        <div class="table-responsive table-responsive-lg border_table">
                            <table class="table data-list-view table-sm">
                                <thead class="bg_table text-center">
                                <tr>
                                    <th scope="col">Tên bộ sưu tập</th>
                                    <th scope="col">Sản phẩm</th>
                                    <th scope="col">Tắt/bật trạng thái</th>
                                    <th scope="col">Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr class="head-content-tb">
                                        <td class="text-center">
                                            <p class="text-black text-bold-700">Sản phẩm bán chạy</p>
                                        </td>
                                        <td class="text-center">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_1.png">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_2.png">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_3.png">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_3.png">                                        </td>
                                        <td class="text-center">
                                            <div class="align-items-center justify-content-center">
                                                <label class="switch">
                                                    <input type="checkbox">
                                                    <span class="slider round green"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="d-flex align-items-center position-relative">
                                                <a href="">
                                                    <button class="btn d-flex align-items-center" title="Sửa"><img src="../../assets/admin/app-assets/images/icons/edit_1.png">
                                                        <span class="color-blue font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Sửa</span>
                                                    </button>
                                                </a>
                                                <div class="show-options" style="cursor: pointer"><img src="../../assets/admin/app-assets/images/icons/Polygon_17.png"></div>
                                                <div class="options-change border_box border-radius-8 position-absolute">
                                                    <button
                                                        class="btn btn-hide d-block" title="'.$content.'"
                                                        data-url="' . $data_url_hide . '"
                                                        data-active="'. $active .'"
                                                    >
                                                        <img src="../../assets/admin/app-assets/images/icons/openeyes.png">
                                                        <span class="color-black font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Xem trước</span>
                                                    </button>
                                                    <button
                                                        class="btn btn-delete d-block" title="Xoá"
                                                        data-url="' . $data_url_delete . '"
                                                    >
                                                        <img src="../../assets/admin/app-assets/images/icons/delete.png">
                                                        <span class="color-black font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Xóa</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="head-content-tb">
                                        <td class="text-center">
                                            <p class="text-black text-bold-700">Bộ sưu tập hè thu</p>
                                        </td>
                                        <td class="text-center">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_1.png">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_2.png">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_3.png">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_3.png">                                        </td>
                                        <td class="text-center">
                                            <div class="align-items-center justify-content-center">
                                                <label class="switch">
                                                    <input type="checkbox">
                                                    <span class="slider round green"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="d-flex align-items-center position-relative">
                                                <a href="">
                                                    <button class="btn d-flex align-items-center" title="Sửa"><img src="../../assets/admin/app-assets/images/icons/edit_1.png">
                                                        <span class="color-blue font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Sửa</span>
                                                    </button>
                                                </a>
                                                <div class="show-options" style="cursor: pointer"><img src="../../assets/admin/app-assets/images/icons/Polygon_17.png"></div>
                                                <div class="options-change border_box border-radius-8 position-absolute">
                                                    <button
                                                        class="btn btn-hide d-block" title="'.$content.'"
                                                        data-url="' . $data_url_hide . '"
                                                        data-active="'. $active .'"
                                                    >
                                                        <img src="../../assets/admin/app-assets/images/icons/openeyes.png">
                                                        <span class="color-black font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Xem trước</span>
                                                    </button>
                                                    <button
                                                        class="btn btn-delete d-block" title="Xoá"
                                                        data-url="' . $data_url_delete . '"
                                                    >
                                                        <img src="../../assets/admin/app-assets/images/icons/delete.png">
                                                        <span class="color-black font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Xóa</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="head-content-tb">
                                        <td class="text-center">
                                            <p class="text-black text-bold-700">Váy hoa nhí</p>
                                        </td>
                                        <td class="text-center">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_1.png">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_2.png">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_3.png">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_3.png">                                        </td>
                                        <td class="text-center">
                                            <div class="align-items-center justify-content-center">
                                                <label class="switch">
                                                    <input type="checkbox">
                                                    <span class="slider round green"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="d-flex align-items-center position-relative">
                                                <a href="">
                                                    <button class="btn d-flex align-items-center" title="Sửa"><img src="../../assets/admin/app-assets/images/icons/edit_1.png">
                                                        <span class="color-blue font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Sửa</span>
                                                    </button>
                                                </a>
                                                <div class="show-options" style="cursor: pointer"><img src="../../assets/admin/app-assets/images/icons/Polygon_17.png"></div>
                                                <div class="options-change border_box border-radius-8 position-absolute">
                                                    <button
                                                        class="btn btn-hide d-block" title="'.$content.'"
                                                        data-url="' . $data_url_hide . '"
                                                        data-active="'. $active .'"
                                                    >
                                                        <img src="../../assets/admin/app-assets/images/icons/openeyes.png">
                                                        <span class="color-black font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Xem trước</span>
                                                    </button>
                                                    <button
                                                        class="btn btn-delete d-block" title="Xoá"
                                                        data-url="' . $data_url_delete . '"
                                                    >
                                                        <img src="../../assets/admin/app-assets/images/icons/delete.png">
                                                        <span class="color-black font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Xóa</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="head-content-tb">
                                        <td class="text-center">
                                            <p class="text-black text-bold-700">Áo thun</p>
                                        </td>
                                        <td class="text-center">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_1.png">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_2.png">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_3.png">
                                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/product_3.png">                                        </td>
                                        <td class="text-center">
                                            <div class="align-items-center justify-content-center">
                                                <label class="switch">
                                                    <input type="checkbox">
                                                    <span class="slider round green"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="d-flex align-items-center position-relative">
                                                <a href="">
                                                    <button class="btn d-flex align-items-center" title="Sửa"><img src="../../assets/admin/app-assets/images/icons/edit_1.png">
                                                        <span class="color-blue font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Sửa</span>
                                                    </button>
                                                </a>
                                                <div class="show-options" style="cursor: pointer"><img src="../../assets/admin/app-assets/images/icons/Polygon_17.png"></div>
                                                <div class="options-change border_box border-radius-8 position-absolute">
                                                    <button
                                                        class="btn btn-hide d-block" title="'.$content.'"
                                                        data-url="' . $data_url_hide . '"
                                                        data-active="'. $active .'"
                                                    >
                                                        <img src="../../assets/admin/app-assets/images/icons/openeyes.png">
                                                        <span class="color-black font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Xem trước</span>
                                                    </button>
                                                    <button
                                                        class="btn btn-delete d-block" title="Xoá"
                                                        data-url="' . $data_url_delete . '"
                                                    >
                                                        <img src="../../assets/admin/app-assets/images/icons/delete.png">
                                                        <span class="color-black font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Xóa</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 p-0 align-items-center justify-content-center">
                        <div class="text-center">
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay/iphone.png">
                        </div>
                    </div>
                </div>
            </div>
            <!-- content -->
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-active", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            $('#myModal').modal('show');
        });

        $(document).on("click", ".show-options", function () {
            $(this).next().toggle('fast');
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

