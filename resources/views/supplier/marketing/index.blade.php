<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-3">
                <div class="box-unit">
                    <div class="head-unit-f mb-3">
                        <span>Chương trình của CTV24</span>
                    </div>
                    <div class="d-flex content-unit-f flex-wrap">
                        <div class="main-content mr-3">
                            <a href="{{url('admin/marketing_supplier/ctv24_programs')}}">
                                <div class="box-content pl-1 pt-2">
                                    <p class="title-box-f">Chương trình của CTV24</p>
                                    <span class="title-box-s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                                </div>
                                <div class="img-content">
                                    <img
                                        src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24.png">
                                </div>
                            </a>
                        </div>
                        <div class="main-content">
                            <a href="{{url('admin/marketing_supplier/ctv24_voucher')}}">
                                <div class="box-content pl-1 pt-2">
                                    <p class="title-box-f">Mã giảm giá của CTV24</p>
                                    <span class="title-box-s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                                </div>
                                <div class="img-content">
                                    <img src="../../assets/admin/app-assets/images/ncc/marketing/magiamgiacuactv24.png">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="box-unit mt-2">
                    <div class="head-unit-f mb-3">
                        <span>Chương trình của tôi</span>
                    </div>
                    <div class="d-flex content-unit-f flex-wrap">
                        <div class="main-content mr-3">
                            <a href="{{url('admin/marketing_supplier/my_voucher')}}">
                                <div class="box-content pl-1 pt-2">
                                    <p class="title-box-f">Mã giảm giá của tôi</p>
                                    <span class="title-box-s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                                </div>
                                <div class="img-content">
                                    <img src="../../assets/admin/app-assets/images/ncc/marketing/magiamgiacuatoi.png">
                                </div>
                            </a>
                        </div>
                        <div class="main-content mr-3">
                            <a href="{{url('admin/marketing_supplier/offer_follower')}}">
                                <div class="box-content pl-1 pt-2">
                                    <p class="title-box-f">Ưu đãi follower</p>
                                    <span class="title-box-s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                                </div>
                                <div class="img-content">
                                    <img src="../../assets/admin/app-assets/images/ncc/marketing/uudaifollow.png">
                                </div>
                            </a>
                        </div>
                        <div class="main-content mr-3">
                            <a href="{{url('admin/marketing_supplier/deal_shock')}}">
                                <div class="box-content pl-1 pt-2">
                                    <p class="title-box-f">Mua kèm deal sốc</p>
                                    <span class="title-box-s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                                </div>
                                <div class="img-content">
                                    <img src="../../assets/admin/app-assets/images/ncc/marketing/muakemdealsoc.png">
                                </div>
                            </a>
                        </div>
                        <div class="main-content mr-3">
                            <a href="{{url('admin/marketing_supplier/top_best_seller')}}">
                                <div class="box-content pl-1 pt-2">
                                    <p class="title-box-f">Top sản phẩm bán chạy</p>
                                    <span class="title-box-s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                                </div>
                                <div class="img-content">
                                    <img src="../../assets/admin/app-assets/images/ncc/marketing/topsanphambanchay.png">
                                </div>
                            </a>
                        </div>
                        <div class="main-content mr-3">
                            <a href="{{url('admin/marketing_supplier/promotion_combo')}}">
                                <div class="box-content pl-1 pt-2">
                                    <p class="title-box-f">Combo khuyến mại</p>
                                    <span class="title-box-s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                                </div>
                                <div class="img-content">
                                    <img src="../../assets/admin/app-assets/images/ncc/marketing/combokhuyenmai.png">
                                </div>
                            </a>
                        </div>
                        <div class="main-content mr-3">
                            <a href="{{url('admin/marketing_supplier/promotion')}}">
                                <div class="box-content pl-1 pt-2">
                                    <p class="title-box-f">Chương trình giảm giá của tôi</p>
                                    <span class="title-box-s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                                </div>
                                <div class="img-content">
                                    <img
                                        src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhgiamgiacuatoi.png">
                                </div>
                            </a>
                        </div>
                        <div class="main-content mr-3">
                            <a href="{{url('admin/marketing_supplier/auction')}}">
                                <div class="box-content pl-1 pt-2">
                                    <p class="title-box-f">Đấu giá</p>
                                    <span class="title-box-s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                                </div>
                                <div class="img-content">
                                    <img src="../../assets/admin/app-assets/images/ncc/marketing/daugia.png">
                                </div>
                            </a>
                        </div>
                        <div class="main-content mr-3">
                            <a href="{{url('admin/marketing_supplier/flash_sale')}}">
                                <div class="box-content pl-1 pt-2">
                                    <p class="title-box-f">Flash sale</p>
                                    <span class="title-box-s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                                </div>
                                <div class="img-content">
                                    <img src="../../assets/admin/app-assets/images/ncc/marketing/flashsale.png">
                                </div>
                            </a>
                        </div>
                        <div class="main-content mr-3">
                            <a href="{{url('admin/marketing_supplier/same_price')}}">
                                <div class="box-content pl-1 pt-2">
                                    <p class="title-box-f">Hàng đồng giá</p>
                                    <span class="title-box-s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                                </div>
                                <div class="img-content">
                                    <img src="../../assets/admin/app-assets/images/ncc/marketing/hangdonggia.png">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- content -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

