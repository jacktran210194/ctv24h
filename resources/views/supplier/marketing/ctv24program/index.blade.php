<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="d-flex head-unit align-items-center justify-content-lg-between">
                    <div class="head-left">
                        <span>Chương trình của CTV24</span>
                    </div>
                    <div class="head-right p-1">
                        <span class="mr-1">Chương trình sắp kết thúc hạn đăng ký</span>
                        <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/down.png">
                    </div>
                </div>
                <div class="body-unit mt-2 align-items-center justify-content-center">
                    <div class="body-content d-flex p-2 align-items-center mb-1">
                        <div class="image mb-2">
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/image1.png">
                        </div>
                        <div class="main ml-3">
                            <p class="noti bg-green text-center">Sắp diễn ra</p>
                            <p class="text">[Fashion] - Khuyến mãi Deal đồng giá [0h 21.06.2021 - 23h59 21.06.2021]</p>
                            <p class="time">Thời gian chương trình: 2021/07/09 23:00:00 đến 2021/07/10 22:59:59</p>
                        </div>
                        <div class="btn-register">
                            <center>
                                <a href="{{url('admin/marketing_supplier/ctv24_programs/register')}}">Đăng ký ngay</a>
                            </center>
                        </div>
                    </div>
                    <div class="body-content d-flex p-2 align-items-center mb-1">
                        <div class="image mb-2">
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/image2.png">
                        </div>
                        <div class="main ml-3">
                            <p class="noti bg-green text-center">Sắp diễn ra</p>
                            <p class="text">Chương trình tham gia hàng đồng giá - [Ngành hàng tiêu dùng] - [Duy nhất 07.07]</p>
                            <p class="time">Thời gian chương trình: 2021/07/09 23:00:00 đến 2021/07/10 22:59:59</p>
                        </div>
                        <div class="btn-register">
                            <center>
                                <a href="{{url('admin/marketing_supplier/ctv24_programs/register')}}">Đăng ký ngay</a>
                            </center>
                        </div>
                    </div>
                    <div class="body-content d-flex p-2 align-items-center mb-1">
                        <div class="image mb-2">
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/image1.png">
                        </div>
                        <div class="main ml-3">
                            <p class="noti bg-green text-center">Sắp diễn ra</p>
                            <p class="text">Chương trình tham gia hàng đồng giá - [Ngành hàng tiêu dùng] - [Duy nhất 07.07]</p>
                            <p class="time">Thời gian chương trình: 2021/07/09 23:00:00 đến 2021/07/10 22:59:59</p>
                        </div>

                        <div class="btn-register">
                            <center>
                                <a href="{{url('admin/marketing_supplier/ctv24_programs/register')}}">Đăng ký ngay</a>
                            </center>
                        </div>
                    </div>
                    <div class="body-content d-flex p-2 align-items-center mb-1">
                        <div class="image mb-2">
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/image3.png">
                        </div>
                        <div class="main ml-3">
                            <p class="noti bg-green text-center">Sắp diễn ra</p>
                            <p class="text">Chương trình tham gia hàng đồng giá - [Ngành hàng tiêu dùng] - [Duy nhất 07.07]</p>
                            <p class="time">Thời gian chương trình: 2021/07/09 23:00:00 đến 2021/07/10 22:59:59</p>
                        </div>
                        <div class="btn-register">
                            <center>
                                <a href="{{url('admin/marketing_supplier/ctv24_programs/register')}}">Đăng ký ngay</a>
                            </center>
                        </div>
                    </div>
                </div>
                <!-- content -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

