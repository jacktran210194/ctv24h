<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="head-unit-first align-items-center justify-content-lg-between">
                    <div class="head-left-first">
                        <span>Chương trình của CTV24</span>
                    </div>
                    <div class="body-unit-first mt-2 align-items-center justify-content-center">
                        <div class="body-content-first d-flex p-2 align-items-center mb-1">
                            <div class="image mb-2">
                                <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/image1.png">
                            </div>
                            <div class="main-register-first ml-2">
                                <p class="noti bg-green text-center">Sắp diễn ra</p>
                                <p class="text mr-3">Chương trình tham gia hàng đồng giá - [Ngành hàng tiêu dùng] - [Duy nhất 07.07]</p>
                            </div>
                            <div class="image-line">
                                <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/line.png">
                            </div>
                            <div class="time-unit-first pl-3 pr-3">
                                <p class="text">Thời gian đăng ký</p>
                                <p class="time">2021/06/17 00:00-2021/07/04 12:00 Bắt đầu sau 13 giờ 0 phút</p>
                            </div>
                            <div class="image-line">
                                <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/line.png">
                            </div>
                            <div class="time-unit-first pl-3 pr-3">
                                <p class="text">Thời gian diễn ra</p>
                                <p class="time">2021/07/07 00:00-2021/07/07 23:59 Bắt đầu sau 20 ngày 13 giờ</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-menu-first mt-3">
                    <div class="d-flex col-lg-4 col-md-12 p-0 align-items-center justify-content-between">
                        <div class="col-lg-6 col-md-12 <?= $type === 'detail' ? 'checked-first' : ''?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/ctv24_programs/register?type=detail') ?>">Chi tiết chương trình</a>
                            </center>
                        </div>
                        <div class="col-lg-6 col-md-12 <?= $type === 'condition' ? 'checked-first' : ''?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/ctv24_programs/register?type=condition') ?>">Điều kiện tham gia</a>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="footer-unit mt-2">
                    <div class="footer-body m-2">
                        <p>Quy định chung:</p>
                        <p> - Chương trình không áp dụng cho các sản phẩm thay đổi nội dung và / hoặc tăng giá trước khi đăng ký & trong suốt thời gian tham gia chương trình.</p>
                        <p>- Sản phẩm tham gia chương trình phải là hàng có sẵn. Shop có trách nhiệm cập nhật đúng tồn kho trước giờ mở bán, tránh để tồn kho ảo dẫn đến việc hủy đơn và có thể bị tính điểm Sao Quả Tạ / bị khóa Shop ít nhất 1 tuần.</p>
                        <p>- Tính năng Tạm nghỉ sẽ bị tắt trong suốt thời gian chương trình diễn ra.</p>
                        <p>- Điều kiện và điều khoản chương trình có thể được thay đổi mà không cần thông báo trước.</p>
                        <p>- Nếu xảy ra tranh chấp, quyết định cuối cùng thuộc về Shopee.</p>
                    </div>
                </div>
            </div>
            <!-- content -->
        </div>

        <div class="card">
            <div class="card-content m-2">
                <div class="head-unit align-items-center justify-content-lg-between">
                    <div class="head-left">
                        <span>Khung giờ</span>
                    </div>
                    <div class="tab-menu d-flex col-lg-4 col-md-12 align-items-center justify-content-around p-0 mt-2">
                        <div class="menu col-lg-4 col-md-12 <?= $type_time === 'all' ? 'checked' : ''?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/ctv24_programs/register?type_time=all') ?>">Tất
                                    cả</a>
                            </center>
                        </div>
                        <div class="menu col-lg-4 col-md-12 <?= $type_time === 'register' ? 'checked' : '' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/ctv24_programs/register?type_time=register') ?>">Đăng
                                    ký</a>
                            </center>
                        </div>
                        <div class="menu col-lg-4 col-md-12 <?= $type_time === 'waiting' ? 'checked' : '' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/ctv24_programs/register?type_time=waiting') ?>">Chờ
                                    xác nhận</a>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="body-unit mt-2 align-items-center justify-content-center">
                    <div class="body-content d-flex p-2 align-items-center mb-1">
                        <div class="image mb-2">
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/image1.png">
                        </div>
                        <div class="main-register ml-3">
                            <p class="noti bg-green text-center">Sắp diễn ra</p>
                            <p class="text">[Fashion] - Khuyến mãi Deal đồng giá [0h 21.06.2021 - 23h59 21.06.2021]</p>
                            <p class="time">Thời gian chương trình: 2021/07/09 23:00:00 đến 2021/07/10 22:59:59</p>
                            <img
                                src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/icon_check.png">
                            <span class="description">Shop của bạn có thể tham gia cho khung giờ này.</span>
                        </div>
                        <div class="btn-register-product">
                            <center>
                                <a href="{{url('admin/marketing_supplier/ctv24_programs/register/product')}}">Đăng ký sản phẩm</a>
                            </center>
                        </div>
                    </div>
                    <div class="body-content d-flex p-2 align-items-center mb-1">
                        <div class="image mb-2">
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/image2.png">
                        </div>
                        <div class="main-register ml-3">
                            <p class="noti bg-green text-center">Sắp diễn ra</p>
                            <p class="text">Chương trình tham gia hàng đồng giá - [Ngành hàng tiêu dùng] - [Duy nhất
                                07.07]</p>
                            <p class="time">Thời gian chương trình: 2021/07/09 23:00:00 đến 2021/07/10 22:59:59</p>
                            <img
                                src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/icon_check.png">
                            <span class="description">Shop của bạn có thể tham gia cho khung giờ này.</span>
                        </div>
                        <div class="btn-register-product">
                            <center>
                                <a href="{{url('admin/marketing_supplier/ctv24_programs/register/product')}}">Đăng ký sản phẩm</a>
                            </center>
                        </div>
                    </div>
                    <div class="body-content d-flex p-2 align-items-center mb-1">
                        <div class="image mb-2">
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/image3.png">
                        </div>
                        <div class="main-register ml-3">
                            <p class="noti bg-green text-center">Sắp diễn ra</p>
                            <p class="text">Chương trình tham gia hàng đồng giá - [Ngành hàng tiêu dùng] - [Duy nhất
                                07.07]</p>
                            <p class="time">Thời gian chương trình: 2021/07/09 23:00:00 đến 2021/07/10 22:59:59</p>
                            <img
                                src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/icon_check.png">
                            <span class="description">Shop của bạn có thể tham gia cho khung giờ này.</span>
                        </div>
                        <div class="btn-register-product">
                            <center>
                                <a href="{{url('admin/marketing_supplier/ctv24_programs/register/product')}}">Đăng ký sản phẩm</a>
                            </center>
                        </div>
                    </div>
                </div>
                <!-- content -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

