<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="head-unit-product align-items-center justify-content-lg-between">
                    <div class="d-flex head-left-product align-items-center justify-content-lg-between">
                        <div class="title-register">
                            <span>Chọn sản phẩm</span>
                        </div>
                    </div>
                    <div class="tab-menu-product d-flex col-lg-3 col-md-12 align-items-center justify-content-around p-0 mt-2">
                        <div class="col-lg-4 menu <?= $type === 'choose' ? 'checked' : '' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/ctv24_programs/register/product/add?type=choose') ?>">Chọn</a>
                            </center>
                        </div>
                        <div class="col-lg-8 menu <?= $type === 'add_list_product' ? 'checked' : '' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/ctv24_programs/register/product/add?type=add_list_product') ?>">Đăng danh sách sản phẩm </a>
                            </center>
                        </div>
                    </div>
                    <div class="head-unit-second d-flex mt-2">
                        <div class="col-lg-3 col-md-12 d-flex p-0 align-items-center justify-content-center">
                            <div class="col-lg-5 pl-1 align-items-center justify-content-center">
                                <span class="title-branch">Ngành hàng</span>
                            </div>
                            <div class="d-flex col-lg-7 drop-show align-items-center justify-content-around">
                                <div>
                                    <span class="title-all">Tất cả</span>
                                </div>
                                <div class="ml-5">
                                    <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/down.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="body-unit mt-2 align-items-center justify-content-center">
                    <div class="table-responsive table-responsive-lg border_table">
                        <table class="table data-list-view table-sm">
                            <thead class="bg_table text-center">
                            <tr>
                                <th scope="col"><input type="checkbox"></th>
                                <th scope="col">Sản phẩm</th>
                                <th scope="col">Đã bán</th>
                                <th scope="col">Giá</th>
                                <th scope="col">Kho hàng</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center"><input type="checkbox"></td>
                                <td>
                                    <div class="d-flex align-items-center justify-content-center">
                                        <img class="image"
                                             src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/product.png">
                                        <div class="product-info">
                                            <p class="name-product">Áo khoác bông dài quá gối </p>
                                            <p class="code-product">Mã: 3241519686</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <span class="text-black text-bold-700">1</span>
                                </td>
                                <td class="text-center">
                                    <span class="text-red text-bold-700 f-18">3.720.000đ</span>
                                </td>
                                <td class="text-center">
                                    <span class="text-red text-bold-700 f-18">10</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center"><input type="checkbox"></td>
                                <td>
                                    <div class="d-flex align-items-center justify-content-center">
                                        <img class="image"
                                             src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/product.png">
                                        <div class="product-info">
                                            <p class="name-product">Áo khoác bông dài quá gối </p>
                                            <p class="code-product">Mã: 3241519686</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <span class="text-black text-bold-700">1</span>
                                </td>
                                <td class="text-center">
                                    <span class="text-red text-bold-700 f-18">3.720.000đ</span>
                                </td>
                                <td class="text-center">
                                    <span class="text-red text-bold-700 f-18">10</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center"><input type="checkbox"></td>
                                <td>
                                    <div class="d-flex align-items-center justify-content-center">
                                        <img class="image"
                                             src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/product.png">
                                        <div class="product-info">
                                            <p class="name-product">Áo khoác bông dài quá gối </p>
                                            <p class="code-product">Mã: 3241519686</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <span class="text-black text-bold-700">1</span>
                                </td>
                                <td class="text-center">
                                    <span class="text-red text-bold-700 f-18">3.720.000đ</span>
                                </td>
                                <td class="text-center">
                                    <span class="text-red text-bold-700 f-18">10</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center"><input type="checkbox"></td>
                                <td>
                                    <div class="d-flex align-items-center justify-content-center">
                                        <img class="image"
                                             src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/product.png">
                                        <div class="product-info">
                                            <p class="name-product">Áo khoác bông dài quá gối </p>
                                            <p class="code-product">Mã: 3241519686</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <span class="text-black text-bold-700">1</span>
                                </td>
                                <td class="text-center">
                                    <span class="text-red text-bold-700 f-18">3.720.000đ</span>
                                </td>
                                <td class="text-center">
                                    <span class="text-red text-bold-700 f-18">10</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- content -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

