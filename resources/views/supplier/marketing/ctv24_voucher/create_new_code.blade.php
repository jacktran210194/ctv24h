<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="head-new-code mb-2">
                    <div class="title-new-code">
                        <span>TẠO CHƯƠNG TRÌNH MÃ GIẢM GIÁ</span>
                    </div>
                </div>
                <div class="d-flex content-new-code">
                    <div class="d-flex col-lg-8 col-md-12 p-0">
                        <div class="w-100">
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Loại mã:</label>
                                <div class="d-flex w-100 align-items-center justify-content-around">
                                    <div class="d-flex shop-or-product-unit align-items-center justify-content-lg-center">
                                        <label class="container_radio text-black text-bold-700">Voucher toàn shop
                                            <input value="0" type="radio" name="shop_or_product" id="shop_or_product" class="check-r">
                                            <span class="radio_config"></span>
                                        </label>
                                    </div>
                                    <div class="d-flex d-flex shop-or-product-unit align-items-center justify-content-lg-center">
                                        <label class="container_radio text-black text-bold-700">Voucher sản phẩm
                                            <input value="1" type="radio" name="shop_or_product" id="shop_or_product" class="check-r">
                                            <span class="radio_config"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Tên chương trình giảm giá:</label>
                                <input required type="text" name="name" id="name_voucher"
                                       value="" class="form-control input-new" placeholder="Tên chương trình giảm giá không chứa các ký tự đặc biệt">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Thời gian lưu mã giảm giá:</label>
                                <div class="d-flex w-100">
                                    <input required type="datetime-local" name="name" id="time_start"
                                           value="<?= isset($data) ? date('Y-m-d\TH:i:s', strtotime($data['time_start'])) : date('Y-m-d\TH:i:s') ?>" class="form-control input-new mr-1">
                                    <input required type="datetime-local" name="name" id="time_end"
                                           value="<?= isset($data) ? date('Y-m-d\TH:i:s', strtotime($data['time_end'])) : date('Y-m-d\TH:i:s') ?>" class="form-control input-new ml-1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="head-new-code mb-2 mt-5">
                    <div class="title-new-code">
                        <span>THIẾT LẬP MÃ GIẢM GIÁ</span>
                    </div>
                </div>
                <div class="d-flex content-new-code">
                    <div class="d-flex col-lg-8 col-md-12 p-0 mb-3">
                        <div class="w-100">
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Loại voucher:</label>
                                <div class="d-flex w-100 align-items-center justify-content-around">
                                    <div class="d-flex shop-or-product-unit align-items-center justify-content-lg-center">
                                        <label class="container_radio text-black text-bold-700">Voucher khuyến mại
                                            <input value="0" type="radio" name="discount_or_refund" id="discount_or_refund" class="check-r">
                                            <span class="radio_config"></span>
                                        </label>
                                    </div>
                                    <div class="d-flex d-flex shop-or-product-unit align-items-center justify-content-lg-center">
                                        <label class="container_radio text-black text-bold-700">Voucher Hoàn xu
                                            <input value="1" type="radio" name="discount_or_refund" id="discount_or_refund" class="check-r">
                                            <span class="radio_config"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Loại giảm giá / mức giảm:</label>
                                <div class="d-flex w-100">
                                    <select id="type_discount" class="form-control option-new mr-1">
                                        <option value="0">Theo số tiền</option>
                                        <option value="1">Theo phần trăm</option>
                                    </select>
                                    <input required type="number" id="value_discount"
                                           value="" class="form-control input-new ml-1">
                                </div>
                            </div>
                            <div class="drop-percent">
                                <div class="d-flex form-group align-items-center">
                                    <label class="font-weight-bold font-size-14" style="width: 230px"></label>
                                    <label class="font-weight-bold font-size-14" style="width: 200px">Mức giảm tối đa</label>
                                </div>
                                <div class="form-group d-flex align-items-center">
                                    <label class="font-weight-bold font-size-14" style="width: 230px"></label>
                                    <div class="d-flex align-items-center justify-content-start">
                                        <div class="d-flex shop-or-product-unit align-items-center justify-content-lg-center mr-3">
                                            <label class="container_radio text-black text-bold-700">Giới hạn
                                                <input value="0" type="radio" name="discount_limit" id="discount_limit" class="check-r">
                                                <span class="radio_config"></span>
                                            </label>
                                        </div>
                                        <div class="d-flex d-flex shop-or-product-unit align-items-center justify-content-lg-center">
                                            <label class="container_radio text-black text-bold-700">Không giới hạn
                                                <input value="1" type="radio" name="discount_limit" id="discount_limit" class="check-r">
                                                <span class="radio_config"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group d-flex align-items-center">
                                    <label class="font-weight-bold font-size-14" style="width: 300px"></label>
                                    <input required type="number" id="max_value"
                                           value="" class="form-control input-new" placeholder="đ">
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Giá trị đơn hàng tối thiểu:</label>
                                <input required type="number" id="min_order_value"
                                       value="" class="form-control input-new">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Số mã có thể lưu:</label>
                                <input required type="number" id="number_save"
                                       value=">" class="form-control input-new" placeholder="Tổng số mã giảm giá có thể lưu ">
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Số mã có thể sử dụng:</label>
                                <input required type="number" id="number_available"
                                       value="" class="form-control input-new" placeholder="Tổng số mã giảm giá có thể sử dụng">
                            </div>
                            <div class="form-group d-flex">
                                <label class="font-weight-bold font-size-14" style="width: 250px">Hình ảnh:</label>
                                <div class="form-label-group">
                                    <img width="120px" height="120px" id="avatar" class="thumbnail image-show-qr"
                                         src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/images/avt.jpg' ?>">
                                    <br>
                                    <input type='file' value="" accept="image/*" name="image" id="image" onchange="changeImg(this)">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="head-new-code mb-2">
                    <div class="title-new-code">
                        <span>HIỂN THỊ MÃ GIẢM GIÁ VÀ CÁC SẢN PHẨM ÁP DỤNG</span>
                    </div>
                </div>
                <div class="d-flex content-new-code mb-3">
                    <div class="d-flex col-lg-8 col-md-12 p-0">
                        <div class="w-100">
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 230px">Thiết lập hiển thị:</label>
                                <div class="align-items-center justify-content-around">
                                    <div class="d-flex shop-or-product-unit align-items-center justify-content-lg-start mb-1">
                                        <label class="container_radio text-black text-bold-700">Voucher khuyến mại
                                            <input value="0" type="radio" name="display_setting" id="display_setting" class="check-r">
                                            <span class="radio_config"></span>
                                        </label>
                                    </div>
                                    <div class="d-flex d-flex shop-or-product-unit align-items-center justify-content-lg-start mb-1">
                                        <label class="container_radio text-black text-bold-700">Chia sẻ thông qua mã voucher
                                            <input value="1" type="radio" name="display_setting" id="display_setting" class="check-r">
                                            <span class="radio_config"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <label class="font-weight-bold font-size-14" style="width: 300px">Sản phẩm được áp dụng:</label>
                                <select name="" id="product_id" class="form-control">
                                    <option value="0">Tất cả</option>
                                    @if(isset($dataProduct))
                                        @foreach($dataProduct as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex col-lg-4 col-md-12 align-items-lg-end justify-content-center btn-unit">
                        <div class="button-new-code mr-1">
                            <center>
                                <a href="{{url('admin/marketing_supplier/ctv24_voucher')}}">Hủy</a>
                            </center>
                        </div>
                        <div class="">
                            <button class="btn btn-main-round btn-add-voucher"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/marketing_supplier/ctv24_voucher/register/create_new_code/save_new_code') ?>"
                            >Đăng ký</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $('.drop-percent').hide();
        $(document).on("click", ".btn-add-voucher", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('name_voucher', $('#name_voucher').val());
            form_data.append('time_start', $('#time_start').val());
            form_data.append('time_end', $('#time_end').val());
            form_data.append('type_discount', $('#type_discount').val());
            form_data.append('value_discount', $('#value_discount').val());
            form_data.append('min_order_value', $('#min_order_value').val());
            form_data.append('number_save', $('#number_save').val());
            form_data.append('number_available', $('#number_available').val());
            form_data.append('max_value', $('#max_value').val());
            form_data.append('product_id', $('#product_id').val());
            form_data.append('image', $('#image')[0].files[0]);

            let shop_or_product = $('input[name="shop_or_product"]:checked').val();
            let discount_or_refund = $('input[name="discount_or_refund"]:checked').val();
            let discount_limit = $('input[name="discount_limit"]:checked').val();
            let display_setting = $('input[name="display_setting"]:checked').val();
            form_data.append('shop_or_product', shop_or_product);
            form_data.append('discount_or_refund', discount_or_refund);
            form_data.append('discount_limit', discount_limit);
            form_data.append('display_setting', display_setting);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    if (data.status) {
                        swal('Thành công', data.msg, 'success');
                        window.location.href = data.url;
                    } else {
                        swal('Lỗi', data.msg, 'error');
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
        $(document).on("change", "#type_discount", function () {
            if($(this).val() == '1'){
                $(".drop-percent").show();
            } else {
                $(".drop-percent").hide();
            }
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

