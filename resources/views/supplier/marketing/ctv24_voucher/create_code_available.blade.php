<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-3">
                <div class="head-unit">
                    <div class="head-left">
                        <span>Đăng ký mã giảm giá</span>
                    </div>
                    <div class="tab-menu d-flex col-lg-6 col-md-12 align-items-center justify-content-around p-0 mt-2">
                        <div class="menu col-lg-6 col-md-12 <?= $type === 'create' ? 'checked' : '' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/ctv24_voucher/register?type=create') ?>">Tạo mã giảm giá mới cho chương trình</a>
                            </center>
                        </div>
                        <div class="menu col-lg-6 col-md-12 <?= $type === 'register' ? 'checked' : '' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/ctv24_voucher/register?type=register') ?>">Đăng ký những mã có sẵn</a>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="body-unit-voucher align-items-center justify-content-center mt-3 mb-4">
                    <div class="body-unit mt-2 align-items-center justify-content-center m-2">
                        <div class="table-responsive bor table-responsive-lg border_table">
                            <table class="table data-list-view table-sm">
                                <thead class="bg_table text-center">
                                <tr>
                                    <th scope="col"><input type="checkbox"></th>
                                    <th scope="col">Mã | Tên chương trình giảm giá </th>
                                    <th scope="col">Loại mã</th>
                                    <th scope="col">Giảm giá</th>
                                    <th scope="col">Số lượng</th>
                                    <th scope="col">Tình trạng | Thời gian</th>
                                    <th scope="col">Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                @for($i = 0; $i < 4; $i++)
                                <tr class="head-content-tb">
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td>
                                        <div class="d-flex align-items-center justify-content-center">
                                            <div class="product-info">
                                                <span class="text-red font-weight-bold">[SFP - 128958]</span>
                                                <p class="name-product">Phiếu ưu đãi Grab Express </p>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">Mã giảm giá toàn shop</span>
                                        <br>
                                        <span class="all-product">(Tất cả sản phẩm)</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="discount-value text-bold-700">Giảm 5%</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700 f-18">10</span>
                                    </td>
                                    <td>
                                        <div class="bg-blue bg-span text-center align-items-center justify-content-center btn-status-register">
                                            Sắp diễn ra
                                        </div>
                                        <div class="text-center">
                                            <span class="text-black text-bold-700">18-08-2021 đến 26-0802021</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <img src="../../assets/admin/app-assets/images/ncc/marketing/magiamgiacuactv24/edit.png">
                                        <span class="text-blue text-bold-700">Sửa</span>
                                    </td>
                                </tr>
                                @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- content -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

