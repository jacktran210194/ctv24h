<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-3">
                <div class="head-unit">
                    <div class="head-left">
                        <span>Đăng ký mã giảm giá</span>
                    </div>
                    <div class="tab-menu d-flex col-lg-6 col-md-12 align-items-center justify-content-around p-0 mt-2">
                        <div class="menu col-lg-6 col-md-12 <?= $type === 'create' ? 'checked' : '' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/ctv24_voucher/register?type=create') ?>">Tạo mã giảm giá mới cho chương trình</a>
                            </center>
                        </div>
                        <div class="menu col-lg-6 col-md-12 <?= $type === 'register' ? 'checked' : '' ?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/ctv24_voucher/register?type=register') ?>">Đăng ký những mã có sẵn</a>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="body-unit-voucher align-items-center justify-content-center mt-3 mb-4">
                    <div class="align-items-center justify-content-center content-unit-voucher">
                        <div class="text-center mb-2">
                            <img src="../../assets/admin/app-assets/images/ncc/marketing/magiamgiacuactv24/cloud.png">
                        </div>
                        <div class="text-center mb-3">
                            <span class="title">Mã giảm giá của bạn sẽ được tạm lưu trong khi chờ Sàn duyệt.</span>
                        </div>
                        <div class="text-center btn-create-voucher">
                            <center>
                                <?php if($type === 'create') :?>
                                    <a href="{{url('admin/marketing_supplier/ctv24_voucher/register/create_new_code')}}">Tạo chương trình mã giảm giá</a>
                                <?php else: ?>
                                    <a href="{{url('admin/marketing_supplier/ctv24_voucher/register/create_code_available')}}">Tạo chương trình mã giảm giá</a>
                                <?php endif; ?>
                            </center>
                        </div>
                    </div>
                </div>
                <!-- content -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

