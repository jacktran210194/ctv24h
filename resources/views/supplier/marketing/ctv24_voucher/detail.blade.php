<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <div class="head-unit-first align-items-center justify-content-lg-between">
                    <div class="head-left-first">
                        <span>Mã giảm giá của CTV24</span>
                    </div>
                    <div class="body-unit-first mt-2 align-items-center justify-content-center">
                        <div class="body-content-first d-flex p-2 align-items-center mb-1">
                            <div class="image-voucher">
                                <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/image1.png">
                            </div>
                            <div class="main-register-first ml-2">
                                <p class="noti bg-green text-center">Sắp diễn ra</p>
                                <p class="text mr-3">Chương trình tham gia hàng đồng giá - [Ngành hàng tiêu dùng] - [Duy
                                    nhất 07.07]</p>
                                <div class="btn-register-voucher col-lg-4 align-items-center justify-content-center">
                                    <center>
                                        <a href="{{url('admin/marketing_supplier/ctv24_voucher/register')}}">Đăng ký ngay</a>
                                    </center>
                                </div>
                            </div>
                            <div class="image-line">
                                <img
                                    src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/line.png">
                            </div>
                            <div class="time-unit-first pl-3 pr-3">
                                <p class="text">Thời gian đăng ký</p>
                                <p class="time">2021/06/17 00:00-2021/07/04 12:00 Bắt đầu sau 13 giờ 0 phút</p>
                            </div>
                            <div class="image-line">
                                <img
                                    src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/line.png">
                            </div>
                            <div class="time-unit-first pl-3 pr-3">
                                <p class="text">Thời gian diễn ra</p>
                                <p class="time">2021/07/07 00:00-2021/07/07 23:59 Bắt đầu sau 20 ngày 13 giờ</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-menu-first mt-3">
                    <div class="d-flex col-lg-4 col-md-12 p-0 align-items-center justify-content-between">
                        <div class="col-lg-6 col-md-12 <?= $type === 'detail' ? 'checked-first' : ''?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/ctv24_voucher/detail?type=detail') ?>">Chi
                                    tiết chương trình</a>
                            </center>
                        </div>
                        <div class="col-lg-6 col-md-12 <?= $type === 'condition' ? 'checked-first' : ''?>">
                            <center>
                                <a href="<?= URL::to('admin/marketing_supplier/ctv24_voucher/detail?type=condition') ?>">Điều
                                    kiện tham gia</a>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="footer-unit mt-2">
                    <div class="footer-body m-2">
                        <p>Quy định chung:</p>
                        <p> - Chương trình không áp dụng cho các sản phẩm thay đổi nội dung và / hoặc tăng giá trước khi
                            đăng ký & trong suốt thời gian tham gia chương trình.</p>
                        <p>- Sản phẩm tham gia chương trình phải là hàng có sẵn. Shop có trách nhiệm cập nhật đúng tồn
                            kho trước giờ mở bán, tránh để tồn kho ảo dẫn đến việc hủy đơn và có thể bị tính điểm Sao
                            Quả Tạ / bị khóa Shop ít nhất 1 tuần.</p>
                        <p>- Tính năng Tạm nghỉ sẽ bị tắt trong suốt thời gian chương trình diễn ra.</p>
                        <p>- Điều kiện và điều khoản chương trình có thể được thay đổi mà không cần thông báo trước.</p>
                        <p>- Nếu xảy ra tranh chấp, quyết định cuối cùng thuộc về Shopee.</p>
                    </div>
                </div>
            </div>
            <!-- content -->
        </div>

        <div class="card">
            <div class="card-content m-2">
                <div class="head-unit-product align-items-center justify-content-lg-between">
                    <div class="d-flex head-left-product align-items-center justify-content-lg-between">
                        <div class="title-register">
                            <span>Danh sách mã giảm giá đăng ký</span>
                        </div>
                        <div class="btn-add-product">
                            <a href="{{url('admin/marketing_supplier/ctv24_voucher/register')}}">
                                <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/add.png">
                                <span>Đăng ký mã giảm giá</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="body-unit mt-2 align-items-center justify-content-center">
                    <div class="table-responsive table-responsive-lg border_table">
                        <table class="table data-list-view table-sm">
                            <thead class="bg_table text-center">
                            <tr>
                                <th scope="col">Mã | Tên giảm giá</th>
                                <th scope="col">Loại mã</th>
                                <th scope="col">Giảm giá</th>
                                <th scope="col">Số lượng</th>
                                <th scope="col">Giá trị đơn tối thiểu</th>
                                <th scope="col">Trạng thái | Thời gian lưu mã</th>
                                <th scope="col">Trạng thái đăng ký</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center justify-content-center">
                                            <div class="product-info">
                                                <span class="text-red font-weight-bold">[SFP - 128958]</span>
                                                <p class="name-product">Phiếu ưu đãi Grab Express </p>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">Mã giảm giá toàn shop</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">Giảm 5%</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700 f-18">10</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700 f-18">150.000đ</span>
                                    </td>
                                    <td>
                                        <div class="bg-blue bg-span btn-status">
                                            Đang diễn ra
                                        </div>
                                        <div class="text-center">
                                            <span class="text-black text-bold-700">2021/06/17 00:00-2021/07/04 12:00</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="status">
                                            <p class="text-red">Đã đăng ký</p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center justify-content-center">
                                            <div class="product-info">
                                                <span class="text-red font-weight-bold">[SFP - 128958]</span>
                                                <p class="name-product">Phiếu ưu đãi Grab Express </p>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">Mã giảm giá toàn shop</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-black text-bold-700">Giảm 5%</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700 f-18">10</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-red text-bold-700 f-18">150.000đ</span>
                                    </td>
                                    <td>
                                        <div class="bg-green bg-span text-center align-items-center justify-content-center btn-status">
                                            Sắp diễn ra
                                        </div>
                                        <div class="text-center">
                                            <span class="text-black text-bold-700">2021/06/17 00:00-2021/07/04 12:00</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="status-red">
                                            <p class="text-red">Chưa đăng ký</p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- content -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

