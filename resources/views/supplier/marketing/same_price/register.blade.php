<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-3">
                <div class="head-unit-first align-items-center justify-content-lg-between">
                    <div class="head-left-first">
                        <span>HÀNG ĐỒNG GIÁ</span>
                    </div>
                    <div class="body-unit-first mt-2 align-items-center justify-content-center">
                        <div class="body-content-first d-flex p-2 align-items-center mb-1">
                            <div class="image-voucher">
                                <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/image1.png">
                            </div>
                            <div class="main-register-first ml-2 pr-4">
                                <p class="noti bg-blue text-center">Đang diễn ra</p>
                                <p class="text mr-3 pr-5">Chương trình tham gia hàng đồng giá - [Ngành hàng tiêu dùng] - [Duy
                                    nhất 07.07]</p>
                                <div class="btn-register-voucher col-lg-4 align-items-center justify-content-center">
                                    <center>
                                        <a href="">Đăng ký ngay</a>
                                    </center>
                                </div>
                            </div>
                            <div class="image-line">
                                <img
                                    src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/line.png">
                            </div>
                            <div class="time-unit-first pl-3">
                                <p class="text">Đăng ký</p>
                                <p class="time">2021/06/17 00:00-2021/07/04 12:00 Bắt đầu sau 13 giờ 0 phút</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-unit mt-2">
                    <div class="d-flex align-items-center justify-content-start">
                        <img src="../../assets/admin/app-assets/images/ncc/marketing/hangdonggia/ticket_star.png">
                        <span class="text-blue text-bold-700 f-18 pl-1">Điều kiện tham gia chương trình</span>
                    </div>
                    <div class="footer-body mt-2">
                        <div class="mb-1">
                            <span class="text-black text-bold-700">Quốc tế:</span>
                            <span class="text-black text-bold-700">Không giới hạn</span>
                        </div>
                        <div class="mb-1">
                            <span class="text-black text-bold-700"> Tình trạng Shop:</span>
                            <span class="text-black text-bold-700"> Không giới hạn</span>
                        </div>
                        <div class="mb-1">
                            <span class="text-black text-bold-700"> Loại shop:</span>
                            <span class="text-black text-bold-700"> Không giới hạn</span>
                        </div>
                        <div class="mb-1">
                            <span class="text-black text-bold-700"> Đánh giá shop:</span>
                            <span class="text-black text-bold-700"> Không giới hạn</span>
                        </div>
                        <div class="mb-1">
                            <span class="text-black text-bold-700"> Thời gian chuẩn bị hàng trung bình:</span>
                            <span class="text-black text-bold-700"> Không giới hạn</span>
                        </div>
                        <div>
                            <span class="text-black text-bold-700"> Điểm Sao Quả Tạ:</span>
                            <span class="text-black text-bold-700"> Không giới hạn</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content -->
        </div>

        <div class="card">
            <div class="card-content m-3">
                <div class="head-unit-product align-items-center justify-content-lg-between">
                    <div class="d-flex head-left-product align-items-center justify-content-lg-between">
                        <div class="title-register">
                            <span>Danh sách sản phầm tham gia</span>
                        </div>
                        <div class="btn-add-product">
                            <a href="{{url('admin/marketing_supplier/same_price/add_product')}}">
                                <img src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/add.png">
                                <span>Thêm sản phẩm</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="body-unit mt-2 align-items-center justify-content-center">
                    <div class="table-responsive table-responsive-lg border_table">
                        <table class="table data-list-view table-sm">
                            <thead class="bg_table text-center">
                            <tr>
                                <th scope="col"><input type="checkbox"></th>
                                <th scope="col">Sản phẩm</th>
                                <th scope="col">SKU phân loại</th>
                                <th scope="col">Phân loại hàng</th>
                                <th scope="col">Giá</th>
                                <th scope="col">Đồng giá</th>
                                <th scope="col">Kho hàng</th>
                                <th scope="col">Đã bán</th>
                                <th scope="col">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td>
                                        <div class="d-flex align-items-center justify-content-center">
                                            <img class="image"
                                                 src="../../assets/admin/app-assets/images/ncc/marketing/chuongtrinhcuactv24/product.png">
                                            <div class="product-info">
                                                <p class="name-product">Áo khoác bông dài quá gối </p>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-black text-bold-700">-</p>
                                        <br>
                                        <p class="text-black text-bold-700">-</p>
                                        <br>
                                        <p class="text-black text-bold-700">-</p>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-grey ">Màu: Đỏ, M</p>
                                        <br>
                                        <p class="text-grey ">Màu: Đỏ, L</p>
                                        <br>
                                        <p class="text-grey ">Màu: Đỏ, S</p>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-red text-bold-700 f-18">3.720.000đ</p>
                                        <br>
                                        <p class="text-red text-bold-700 f-18">3.720.000đ</p>
                                        <br>
                                        <p class="text-red text-bold-700 f-18">3.720.000đ</p>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-black text-bold-700 ">50.000đ</p>
                                        <br>
                                        <p class="text-black text-bold-700">50.000đ</p>
                                        <br>
                                        <p class="text-black text-bold-700 ">50.000đ</p>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-black text-bold-700">5</p>
                                        <br>
                                        <div style="margin-left: 4rem" class="bg-pink bg-span text-center align-items-center justify-content-center btn-status-combo">
                                            Hết hàng
                                        </div>
                                        <br>
                                        <p class="text-black text-bold-700 pt-1">5</p>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-black text-bold-700">20</p>
                                        <br>
                                        <p class="text-black text-bold-700">20</p>
                                        <br>
                                        <p class="text-black text-bold-700">20</p>
                                    </td>
                                    <td class="text-center">
                                        <div class="d-flex align-items-center position-relative">
                                            <center>
                                                <a href="">
                                                    <button class="btn d-flex align-items-center" title="Sửa"><img src="../../assets/admin/app-assets/images/icons/edit_1.png">
                                                        <span class="color-blue font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Sửa</span>
                                                    </button>
                                                </a>
                                            </center>
                                            <div class="show-options" style="cursor: pointer"><img src="../../assets/admin/app-assets/images/icons/Polygon_17.png"></div>
                                            <div class="options-change border_box border-radius-8 position-absolute">
                                                <button
                                                    class="btn btn-delete d-block" title="Xoá"
                                                    data-url="' . $data_url_delete . '"
                                                >
                                                    <img src="../../assets/admin/app-assets/images/icons/delete.png">
                                                    <span class="color-black font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Xóa</span>
                                                </button>
                                                <button
                                                    class="btn btn-hide d-block" title="'.$content.'"
                                                    data-url="' . $data_url_hide . '"
                                                    data-active="'. $active .'"
                                                >
                                                    <img src="../../assets/admin/app-assets/images/icons/openeyes.png">
                                                    <span class="color-black font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Ẩn</span>
                                                </button>
                                                <button
                                                    class="btn btn-hide d-block" title="'.$content.'"
                                                    data-url="' . $data_url_hide . '"
                                                    data-active="'. $active .'"
                                                >
                                                    <img src="../../assets/admin/app-assets/images/icons/openeyes.png">
                                                    <span class="color-black font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Hiển thị</span>
                                                </button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- content -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-active", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            $('#myModal').modal('show');
        });

        $(document).on("click", ".show-options", function () {
            $(this).next().toggle('fast');
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

