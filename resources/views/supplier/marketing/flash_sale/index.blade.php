<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background: #D1292F;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h3 class="text-bold-700 f-24 mb-2">{{$title}}</h3>
                <div class="d-flex align-items-center">
                    <div class="tab-bar-page" style="width: 50%!important;">
                        <ul class="tab-bar-content d-flex align-items-center justify-content-lg-start flex-wrap">
                            <li class="d-flex align-items-center justify-content-lg-center active">
                                <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Tất
                                    cả</a>
                            </li>
                            <li class="d-flex align-items-center justify-content-lg-center">
                                <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đang
                                    diễn ra</a>
                            </li>
                            <li class="d-flex align-items-center justify-content-lg-center">
                                <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Sắp
                                    diễn ra</a>
                            </li>
                            <li class="d-flex align-items-center justify-content-lg-center">
                                <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đã
                                    kết thúc</a>
                            </li>
                        </ul>
                    </div>
                    <div class="position-absolute" style="right: 2%;">
                        <a href="{{url('admin/marketing_supplier/flash_sale/add_flash_sale')}}" class="btn btn_main text-bold-700 m-0 text-white" style="width: 150px!important;">
                            <img src="../../assets/admin/app-assets/images/icon_dashboard/icon_add.png" alt="">
                            Tạo flash sale
                        </a>
                    </div>
                </div>
                <div class="content_table mt-3">
                    <div class="table-responsive table-responsive-lg border_table">
                        <table class="table data-list-view table-sm">
                            <thead>
                            <tr class="bg_table">
                                <th width="15%">Khung giờ</th>
                                <th>Sản phẩm</th>
                                <th width="12%" class="text-center">Lượt người mua đặt nhắc nhở</th>
                                <th width="10%" class="text-center">Lượt nhấp chuột/xem</th>
                                <th>Trạng thái</th>
                                <th class="text-center">Bật tắt</th>
                                <th class="text-center">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody id="get">
                            <tr>
                                <td>
                                    <span class="text-red f-12 text-bold-700">18:00 - 21:00</span>
                                    <p class="f-14 text-bold-700">24-06-2021</p>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <img width="71px" height="71px" class="border_radius mr-1 image-preview"
                                             src="https://www.bigc.vn/files/blog/chuong-trinh-tet/banner-resize-3-07.png"
                                             alt="">
                                        <div class="content_product">
                                            <span class="text-red f-12 text-bold-700">[SFP - 128958]</span>
                                            <p class="f-14 text-bold-700 m-0">Phiếu ưu đãi Grab Express </p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center text-bold-700">-</td>
                                <td class="text-center text-bold-700">-</td>
                                <td>
                                    <div style="width: 120px!important;" class="bg-blue text-bold-700 text-center">Đang
                                        diễn ra
                                    </div>
                                </td>
                                <td>
                                    <label class="switch ml-3">
                                        <input class="btn_check" checked type="checkbox">
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                                <td class="text-center">
                                    <a class="text-bold-700 text-underline" style="color: #005DB6;">
                                        <i class="fa fa-edit"></i> Sửa
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="text-red f-12 text-bold-700">18:00 - 21:00</span>
                                    <p class="f-14 text-bold-700">24-06-2021</p>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <img width="71px" height="71px" class="border_radius mr-1 image-preview"
                                             src="https://baodanang.vn/dataimages/202008/original/images1574712_a1.jpg"
                                             alt="">
                                        <div class="content_product">
                                            <span class="text-red f-12 text-bold-700">[SFP - 128958]</span>
                                            <p class="f-14 text-bold-700 m-0">Phiếu ưu đãi Grab Express </p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center text-bold-700">-</td>
                                <td class="text-center text-bold-700">-</td>
                                <td>
                                    <div style="width: 120px!important;" class="bg-green text-bold-700 text-center">
                                        Sắp diễn ra
                                    </div>
                                </td>
                                <td>
                                    <label class="switch ml-3">
                                        <input class="btn_check" checked type="checkbox">
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                                <td class="text-center">
                                    <a class="text-bold-700 text-underline" style="color: #005DB6;">
                                        <i class="fa fa-edit"></i> Sửa
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span class="text-red f-12 text-bold-700">18:00 - 21:00</span>
                                    <p class="f-14 text-bold-700">24-06-2021</p>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <img width="71px" height="71px" class="border_radius mr-1 image-preview"
                                             src="https://cdn.scb.com.vn/picture/grab_2_.webp"
                                             alt="">
                                        <div class="content_product">
                                            <span class="text-red f-12 text-bold-700">[SFP - 128958]</span>
                                            <p class="f-14 text-bold-700 m-0">Phiếu ưu đãi Grab Express </p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center text-bold-700">-</td>
                                <td class="text-center text-bold-700">-</td>
                                <td>
                                    <div style="width: 120px!important;" class="bg-red text-bold-700 text-center">
                                        Đã kết thúc
                                    </div>
                                </td>
                                <td>
                                    <label class="switch ml-3">
                                        <input class="btn_check" checked type="checkbox">
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                                <td class="text-center">
                                    <a class="text-bold-700 text-underline" style="color: #005DB6;">
                                        <i class="fa fa-edit"></i> Sửa
                                    </a>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

