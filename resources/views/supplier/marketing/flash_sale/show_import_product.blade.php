<div class="import_product mt-4">
    <div class="d-flex align-items-center">
        <div class="content_info">
            <h3 class="text-bold-700">Đăng tải danh sách sản phẩm của bạn bằng cách sử dụng bảng Excel
                mẫu.</h3>
            <p class="text-sliver text-bold-700">Tìm hiểu cách thêm/chọn sản phẩm hàng hoạt cho các
                chương
                trình Marketing</p>
            <p class="text-sliver text-bold-700">Tìm Mã sản phẩm từ danh sách dưới đây </p>
        </div>
        <button class="btn btn_main text-bold-700 position-absolute" style="right: 2%;"><i class="fa fa-download"></i> Tải về bản mẫu</button>
    </div>
    <div class="content_import">
        <div class="border_gach_red position-relative" style="height: 530px!important; background: #F3F3F3">
            <div class="form_import text-center position-absolute position-center">
                <img width="95px" height="87px" src="../../assets/admin/app-assets/images/icon_dashboard/icon_import.png" alt="">
                <h3 class="text-bold-700 f-24">Chọn hoặc kéo thả file excel vào đây </h3>
                <p class="text-sliver text-bold-700">Tối đa 20 sản phẩm được chọn</p>
                <button class="btn btn_main text-bold-700 btn_import" style="width: 150px!important;">Tải tệp tin</button>
                <input multiple hidden id="input_import" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
            </div>
        </div>
    </div>
</div>
