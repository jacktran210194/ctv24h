<?php
$page = 'marketing';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/marketing/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">
<!-- BEGIN: Header-->
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background: #D1292F;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h3 class="text-bold-700 f-24 mb-2">{{$title}}</h3>
                <div class="form-group" style="width: 70%;">
                    <label class="text-bold-700">Khung thời gian</label>
                    <div class="d-flex align-items-center">
                        <input type="date" class="form-control input_config">
                        <select class="form-control input_config" id="">
                            <option value="">--Chọn khung giờ--</option>
                            <option data-time-1="{{date('H:i:s',strtotime('12:00:00'))}}"
                                    data-time-2="{{date('H:i:s',strtotime('15:00:00'))}}">12:00:00 - 15:00:00
                            </option>
                            <option data-time-1="{{date('H:i:s',strtotime('12:00:00'))}}"
                                    data-time-2="{{date('H:i:s',strtotime('15:00:00'))}}">12:00:00 - 15:00:00
                            </option>
                            <option data-time-1="{{date('H:i:s',strtotime('12:00:00'))}}"
                                    data-time-2="{{date('H:i:s',strtotime('15:00:00'))}}">12:00:00 - 15:00:00
                            </option>
                            <option data-time-1="{{date('H:i:s',strtotime('12:00:00'))}}"
                                    data-time-2="{{date('H:i:s',strtotime('15:00:00'))}}">12:00:00 - 15:00:00
                            </option>
                        </select>
                        <input style="width: 60%;" type="number" placeholder="Số sản phẩm tham gia"
                               class="form-control input_config">
                    </div>
                </div>
                <div class="form-group" style="width: 70%;">
                    <label class="text-bold-700">Tiêu chí sản phẩm</label>
                    <div class="card_shadow">
                        <button class="btn border_red mt-1" style="width: 100px!important;">Tất cả</button>
                        <p class="text-sliver text-bold-700 mt-2">Tất cả tiêu chí sản phẩm</p>
                        <div class="d-flex mt-1">
                            <ul style="width: 60%;">
                                <li>Tồn kho cho phép: 1~1000</li>
                                <li>Giá sau khuyến mãi là giá thấp nhất trong 7 ngày qua (không tính giá chạy Flash Sale
                                    của Shopee)
                                </li>
                                <li>Lượt thích: Không giới hạn</li>
                                <li>Số đơn hàng bán được trong 30 ngày qua: Không giới hạn</li>
                                <li>Thời gian tham gia chương trình tiếp theo: >= 1 ngày (Cùng một sản phẩm không thể
                                    đăng ký Flash Sale trong 1 ngày liên tiếp
                                </li>
                            </ul>
                            <ul>
                                <li>Khoảng giảm giá cho phép: 5% ~ 90%</li>
                                <li>Đánh giá: Không giới hạn</li>
                                <li>Hàng đặt trước: Không chấp nhận hàng đặt trước</li>
                                <li>Giao hàng trong: Không giới hạn ngày</li>
                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="content_info">
                        <h3 class="text-bold-700 f-24">Sản phẩm chạy Flash Sale của Shop</h3>
                        <p class="text-bold-700 text-sliver f-14">Vui lòng kiểm tra tiêu chí sản phẩm trước khi thêm sản
                            phẩm vào chương trình khuyến mãi của bạn.</p>
                    </div>
                    <div class="position-absolute" style="right: 2%;">
                        <a href="{{url('admin/marketing_supplier/flash_sale/add_product_flash_sale')}}"
                           class="btn btn_main text-bold-700 m-0 text-white" style="width: 200px!important;">
                            <img src="../../assets/admin/app-assets/images/icon_dashboard/icon_add.png" alt="">
                            Thêm sản phẩm
                        </a>
                    </div>
                </div>

            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="title_info mr-2" style="width: 15%!important;">
                        <h3 class="text-bold-700 f-16">Chỉnh sửa hàng loạt</h3>
                        <p class="text-sliver">Đã chọn <span style="color: black!important;"
                                                             class="text-bold-700">0</span> phân loại hàng</p>
                    </div>
                    <div class="update_all">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="text-bold-700">Khuyến mãi (%)</label>
                                    <input type="number" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label style="width: 100%;" class="text-bold-700">SL SP khuyến mãi</label>
                                    <input type="number" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="text-bold-700">Giới hạn đặt hàng</label>
                                    <input type="number" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="text-bold-700">Bật / Tắt</label>
                                    <br>
                                    <label class="switch">
                                        <input class="btn_check" checked type="checkbox">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn_list">
                        <button class="btn btn_main text-bold-700 mr-2">Cập nhật hàng loạt</button>
                        <button class="btn btn_sliver text-bold-700">Xoá</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="table-responsive table-responsive-lg border_table">
                <table class="table data-list-view table-sm">
                    <thead>
                    <tr class="bg_table">
                        <th><input type="checkbox"></th>
                        <th>Giá gốc</th>
                        <th>Giá đã giảm</th>
                        <th>Khuyến mãi (%)</th>
                        <th width="7%">SL</th>
                        <th>Kho hàng</th>
                        <th>Giới hạn đặt hàng</th>
                        <th class="text-center">Bật/Tắt</th>
                        <th>Thao tác</th>
                    </tr>
                    </thead>
                    <tbody id="get">
                    <tr>
                        <td>
                            <input type="checkbox">
                        </td>
                        <td>
                            <div class="d-flex align-items-center">
                                <img width="71px" height="71px" class="border_radius mr-1 image-preview"
                                     src="https://www.bigc.vn/files/blog/chuong-trinh-tet/banner-resize-3-07.png"
                                     alt="">
                                <p class="f-14 text-bold-700 m-0">Phiếu ưu đãi Grab Express </p>
                            </div>
                        </td>
                        <td class="text-left text-bold-700">700.000 đ</td>
                        <td><input type="number" class="form-control"></td>
                        <td><input type="number" class="form-control"></td>
                        <td class="text-bold-700">20</td>
                        <td><input type="number" placeholder="Giới hạn đặt hàng" class="form-control"></td>
                        <td>
                            <label class="switch ml-3">
                                <input class="btn_check" checked type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </td>
                        <td class="text-center">
                            <a class="text-bold-700 text-underline" style="color: #D1292F;">
                                <i class="fa fa-trash fa-2x"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox">
                        </td>
                        <td>
                            <div class="d-flex align-items-center">
                                <img width="71px" height="71px" class="border_radius mr-1 image-preview"
                                     src="https://www.bigc.vn/files/blog/chuong-trinh-tet/banner-resize-3-07.png"
                                     alt="">
                                <p class="f-14 text-bold-700 m-0">Phiếu ưu đãi Grab Express </p>
                            </div>
                        </td>
                        <td class="text-left text-bold-700">700.000 đ</td>
                        <td><input type="number" class="form-control"></td>
                        <td><input type="number" class="form-control"></td>
                        <td class="text-bold-700">20</td>
                        <td><input type="number" placeholder="Giới hạn đặt hàng" class="form-control"></td>
                        <td>
                            <label class="switch ml-3">
                                <input class="btn_check" checked type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </td>
                        <td class="text-center">
                            <a class="text-bold-700 text-underline" style="color: #D1292F;">
                                <i class="fa fa-trash fa-2x"></i>
                            </a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-end">
            <button class="btn border bg-white text-bold-700 mr-1" style="width: 100px!important;">Huỷ</button>
            <button class="btn btn_main text-bold-700" style="width: 100px!important;">Xác nhận</button>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

