<div class="list_product">
    <div class="mt-2 d-flex align-items-center">
        <div class="form-group filter_category d-flex align-items-center">
            <label style="width: 200px!important;" class="text-bold-700">Ngành hàng</label>
            <select class="form-control" id="">
                <option value="all">Tất cả</option>
            </select>
        </div>
        <div class="form-group form_search d-flex align-items-center position-absolute"
             style="right: 2%;">
            <label style="width: 150px!important;" class="text-bold-700">Tìm kiếm</label>
            <select style="width: auto!important;" class="form-control" id="">
                <option value="">Tên sản phẩm</option>
            </select>
            <input type="text" class="form-control">
        </div>
    </div>
    <div class="float-right">
        <input type="checkbox">
        <span class="text-bold-700">Xem sản phẩm có sẵn</span>
    </div>
    <div class="clearfix"></div>
    <div class="mt-1 table-responsive table-responsive-lg border_table">
        <table class="table data-list-view table-sm">
            <thead>
            <tr class="bg_table">
                <th><input type="checkbox"></th>
                <th>Sản phẩm</th>
                <th>Đã bán</th>
                <th>Giá</th>
                <th>Kho hàng</th>
            </tr>
            </thead>
            <tbody id="get">
            <tr>
                <td>
                    <input type="checkbox">
                </td>
                <td>
                    <div class="d-flex align-items-center">
                        <img width="71px" height="71px" class="border_radius mr-1 image-preview"
                             src="https://www.bigc.vn/files/blog/chuong-trinh-tet/banner-resize-3-07.png"
                             alt="">
                        <div>
                            <p class="f-14 text-bold-700 m-0">Áo khoác bông dài quá gối</p>
                            <div class="mb-2"></div>
                            <p class="text-sliver m-0">Mã: 231321321321</p>
                        </div>
                    </div>
                </td>
                <td class="text-left text-bold-700">100</td>
                <td class="text-red text-bold-700">
                    700.000 đ
                </td>
                <td class="text-red text-bold-700">
                    10
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox">
                </td>
                <td>
                    <div class="d-flex align-items-center">
                        <img width="71px" height="71px" class="border_radius mr-1 image-preview"
                             src="https://www.bigc.vn/files/blog/chuong-trinh-tet/banner-resize-3-07.png"
                             alt="">
                        <div>
                            <p class="f-14 text-bold-700 m-0">Áo khoác bông dài quá gối</p>
                            <div class="mb-2"></div>
                            <p class="text-sliver m-0">Mã: 231321321321</p>
                        </div>
                    </div>
                </td>
                <td class="text-left text-bold-700">100</td>
                <td class="text-red text-bold-700">
                    700.000 đ
                </td>
                <td class="text-red text-bold-700">
                    10
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox">
                </td>
                <td>
                    <div class="d-flex align-items-center">
                        <img width="71px" height="71px" class="border_radius mr-1 image-preview"
                             src="https://www.bigc.vn/files/blog/chuong-trinh-tet/banner-resize-3-07.png"
                             alt="">
                        <div>
                            <p class="f-14 text-bold-700 m-0">Áo khoác bông dài quá gối</p>
                            <div class="mb-2"></div>
                            <p class="text-sliver m-0">Mã: 231321321321</p>
                        </div>
                    </div>
                </td>
                <td class="text-left text-bold-700">100</td>
                <td class="text-red text-bold-700">
                    700.000 đ
                </td>
                <td class="text-red text-bold-700">
                    10
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
