<?php
$page = 'introduction_member';
use Illuminate\Support\Facades\URL;
$dataLogin = Session::get('data_supplier');
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/financial/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    {{--                    <a href="{{url('admin/manage_supplier/add')}}" class="btn btn-success"><i class="fa fa-plus"--}}
                    {{--                                                                                               aria-hidden="true"></i> Thêm</a>--}}
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_supplier')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content">
                <div class="d-flex align-items-end justify-content-lg-center mb-5">
                    <div class="col-lg-6 col-md-12 ml-3 mt-3">
                        <p class="text-black text-bold-700 font-size-24 mb-3">GIỚI THIỆU THÀNH VIÊN</p>
                        <span class="font-size-17 text-grey text-justify">Theo chính sách bán hàng của Sàn, giá bán trên Shop là giá bán cho CTV, CTV có, hoa hồng nhận được sẽ có 2 loại như sau:</span>
                        <br>
                        <span class="font-size-17 text-grey text-justify">Loại 1: Giá NCC yêu cầu bán</span>
                        <br>
                        <span class="font-size-17 text-grey text-justify">Đây là giá mà Shop áp dụng cho bán lẻ, CTV chỉ có thể thu tiền từ khách lẻ giá trị đúng với giá NCC đã đặt ra. Phần chênh lệch giữa giá bán và giá thu tiền từ khách hàng là Hoa Hồng CTV.</span>
                        <br>
                        <span class="font-size-17 text-grey text-justify">Ví dụ: Giá bán sỉ cho CTV  SP A là 100k, Giá bán CTV bán cho khách là 150k. Vậy sau khi CTV đặt hàng từ NCC thì NCC sẽ gửi đơn cho KH và thu tiền giá đơn hàng là 150k thu từ khách hàng. Hoa hồng CTV sẽ là 50k.</span>
                        <br>
                        <span class="font-size-17 text-grey text-justify">Loại 2: Giá CTV tự đặt ra.</span>
                        <br>
                        <span class="font-size-17 text-grey text-justify">Đây là giá CTV tự đề ra và được  người mua chấp nhận trong giao dịch giữa CTV và khách hàng. Sau khi đơn hàng hoàn thành, phần chênh lệch giữa Giá bán của Shop và giá CTV bán cho khách hàng là Hoa Hồng CTV.</span>
                        <br>
                        <span class="font-size-17 text-grey text-justify">Đối với những mặt hàng NCC không yêu cầu áp giá bán lẻ cho KH, CTV có thể bán theo giá tùy ý thì sẽ có mục nhập giá bán cho mỗi sản phẩm khi CTV đặt đơn với NCC.</span>
                        <br>
                        <span class="font-size-17 text-grey text-justify">Ví dụ: Giá bán sỉ NCC đưa ra cho SP A là 100k, và NCC không áp giá bán lẻ. CTV bán SPA cho khách 180k, CTV có thể nhập giá bán đã thỏa thuận với KH là 180k vào mục Giá KH để lên đơn và thu tiền từ khách là 180k. Hoa hồng CTV sẽ làm 80k.</span>
                        <p class="font-size-17 text-grey text-justify mt-5">Như vậy về chức năng tính HH trên sản phẩm bán ra sẽ thiết lập trên giá bán 2 cơ chế đặt giá trong phần thiết lập Shop, nhập thông tin giá bán, giá trị HH ngay khi đăng sản phẩm trên gian hàng. NCC có thể chọn chế độ bán theo giá NCC yêu cầu hoặc bán theo giá tùy ý CTV.</p>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="link-unit-red ml-5 mr-5 mb-2">
                            <p class="text-center text-black text-bold-700 font-size-17">Mã giới thiệu</p>
                            <p class="text-center text-bold-700 text-red font-size-17">038597321</p>
                        </div>
                        <div class="link-unit ml-5 mr-5">
                            <p class="text-center text-black text-bold-700 font-size-17">Link giới thiệu</p>
                            <p class="text-center text-bold-700 text-blue font-size-17">https://www.figma.com/file</p>
                        </div>
                        <div class="ml-5 mr-5">
                            <img src="../../assets/frontend/Icon/gioithieu/image.png" style="width: 100%!important;" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>
