<?php
use App\Models\ProductModel;
$path_1 = "assets/frontend/Icon/products";
?>
<div class="header-search flex">
    <div class="container-nav flex container">
        <div class="title-nav">
            <p class="search-relate">
                Tìm kiếm liên quan
            </p>
        </div>
        <div class="container-navigation relative">
            <div class="next-arrow button-arrow">
                <object data="icon/search/icon_next.svg" width="8px"></object>
            </div>
            <ul class="navigation flex">
                <li class="flex bg-nav-1 content-nav">Phụ kiện âm thanh</li>
                <li class="flex bg-nav-2 content-nav">Phụ kiện âm thanh</li>
                <li class="flex bg-nav-3 content-nav">Phụ kiện âm thanh</li>
                <li class="flex bg-nav-4 content-nav">Phụ kiện âm thanh</li>
                <li class="flex bg-nav-5 content-nav">Phụ kiện âm thanh</li>
                <li class="flex bg-nav-1 content-nav">Phụ kiện âm thanh</li>
            </ul>
            <div class="prev-arrow button-arrow">
                <object data="icon/search/icon_preview.svg" width="8px"></object>
            </div>
        </div>
    </div>
</div>
<div class="body-seaech flex container">
    <?php
    $path = "assets/frontend/Icon/products";
    ?>
    <div class="fillter">
        <div class="title-fillter flex">
            <div class="icon_fillter">
                <object data="icon/search/icon_fillter.svg" width="12px"></object>
            </div>
            <div class="title-text">BỘ LỌC TÌM KIẾM</div>
        </div>
        <div class="filter-catagory">
            <p class="title">Theo danh mục</p>
            @if(isset($category_product))
                @foreach($display_category as $items)
                    @if(!empty($items))
                        <div class="sub-filter">
                            <p class="title">{{$items->name}}</p>
                            <div class="dropdown-filter" style="display: none">
                                @foreach($category_product as $value)
                                    @if($items->id == $value->category_product_id)
                                        <?php
                                        $count_category = ProductModel::where('category_sub_child', $value->id)->count();
                                        ?>
                                        @if($count_category >0)
                                            <div class="flex content content-filter">
                                                <input type="checkbox" id="{{$value->id}}">
                                                <p class="text">{{$value->name}}<span class="number">({{$count_category}})</span>
                                                </p>
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
            {{--                <p class="load-more">Xem Thêm ></p>--}}
        </div>
        <div class="filter-catagory">
            <p class="title">Nơi bán</p>

            <div class="flex content filter-place">
                <input type="checkbox" value="Hà Nội">
                <p class="text">Hà Nội</p>
            </div>
            <div class="flex content filter-place">
                <input type="checkbox" value="TP.Hồ Chí Minh">
                <p class="text">TP.Hồ Chí Minh</p>
            </div>
            <div class="flex content filter-place">
                <input type="checkbox" value="Thái Nguyên">
                <p class="text">Thái Nguyên</p>
            </div>
            <div class="flex content filter-place">
                <input type="checkbox" value="Vĩnh Phúc">
                <p class="text">Vĩnh Phúc</p>
            </div>
            <div class="flex content filter-place">
                <input type="checkbox" value="Hải Phòng">
                <p class="text">Hải Phòng </p>
            </div>
            <p class="load-more">Xem Thêm ></p>
        </div>
        <div class="filter-catagory">
            <p class="title">Đơn vị vận chuyển</p>
            @if(isset($shipping))
                @foreach($shipping as $value)
                    <div class="flex content filter-shipping">
                        <input type="checkbox" value="{{$value->id}}">
                        <p class="text">{{$value ->name }}</p>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="filter-catagory form-fillter">
            <p class="title">Khoảng giá</p>
            <div class="form-fillter-price flex">
                <input type="number" placeholder="TỪ" class="price-filter-1">
                <span class="icon-space">-</span>
                <input type="number" placeholder="ĐẾN" class="price-filter-2">
            </div>
            <button class="btn btn-filter">Áp dụng</button>
        </div>
        <div class="filter-catagory">
            <p class="title">Loại shop</p>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Shopee Mall</p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Shop yêu thích</p>
            </div>
        </div>
        <div class="filter-catagory">
            <p class="title">Tình trạng</p>
            <div class="flex content filter-status">
                <input type="checkbox" value="1">
                <p class="text">Sản phẩm mới </p>
            </div>
            <div class="flex content filter-status">
                <input type="checkbox" value="0">
                <p class="text">Sản phẩm đã từng </p>
            </div>
        </div>
        <div class="filter-catagory">
            <p class="title">Đánh giá </p>
            <div class="flex content">
                <button class="filter-ratings" value="5">
                    <object data="icon/search/icon_star_five.svg"></object>
                </button>
            </div>
            <div class="flex content">
                <button class="d-flex align-items-center w-100 filter-ratings" value="4">
                    <object data="icon/search/icon_star_for.svg"></object>
                    <p class="text">trở lên </p>
                </button>
            </div>
            <div class="flex content">
                <button class="d-flex align-items-center w-100 filter-ratings" value="3">
                    <object data="icon/search/icon_star_three.svg"></object>
                    <p class="text">trở lên </p>
                </button>
            </div>
            <div class="flex content">
                <button class="d-flex align-items-center w-100 filter-ratings" value="2">
                    <object data="icon/search/icon_star_tow.svg"></object>
                    <p class="text">trở lên </p>
                </button>
            </div>
            <div class="flex content">
                <button class="d-flex align-items-center w-100 filter-ratings" value="1">
                    <object data="icon/search/icon_star_one.svg"></object>
                    <p class="text">trở lên </p>
                </button>
            </div>
        </div>
        <div class="filter-catagory">
            <p class="title">Dịch vụ và khuyến mãi</p>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Freeship Xtra </p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Hoàn xu Xtra </p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Đang giảm giá </p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Miễn phí vận chuyển </p>
            </div>
            <p class="load-more">Thêm
                <object width="7px" data="icon/search/icon_arrow_down.svg"></object>
            </p>
        </div>
        <button class="btn clear-all">Xóa tất cả</button>
    </div>
    <div class="container-left">
        @if($data_shop)
            <div class="flex shop-love">
                <div class="box-1 flex div-box">
                    <div class="image-button">
                        <a href="{{url('seeshop/'.$data_shop->id)}}"><div class="iamge-shop"><img @if($data_shop->image) src="{{$data_shop->image}}" @else src="<?php echo asset("$path/shop_love.png")?>" @endif></div></a>
                        <p class="shop">Shop yêu thích</p>
                    </div>
                    <div class="infor-shop">
                        <a href="{{url('seeshop/'.$data_shop->id)}}"><h2 class="tite-shop">{{$data_shop->name}}</h2></a>
                        <div class="link-button flex">
                            <p class="number-follow">
                                <span class="number">{{$data_shop->follow}} </span>
                                Người Theo Dõi
                            </p>
                            <p class="number-follow">
                                <span class="number">{{$data_shop->following}} </span>
                                Đang Theo Dõi
                            </p>
                        </div>
                    </div>
                </div>
                <div class="box-2 div-box">
                    <p class="price-buy">
                        <object data="icon/products/t-shirt.svg" width="20px"></object>
                        {{$data_shop->count_product}}
                        <span> Sản Phẩm </span>
                    </p>
                </div>
                <div class="box-3 div-box">
                    <p class="price-buy">
                        <object data="icon/products/favorite_2.svg" width="20px"></object>
                        {{$data_shop->rating}}
                        <span>  Đánh Giá </span>
                    </p>
                </div>
                <div class="box-4 div-box">
                    <p class="price-buy">
                        <object data="icon/products/chat_bubbles_with_ellipsis.svg" width="20px"></object>
                        {{$data_shop->percent_feedback}}%
                        <span> Tỉ Lệ Phản Hồi </span>
                    </p>
                </div>
                <div class="box-5 div-box">
                    <p class="price-buy">
                        <object data="icon/products/clock.svg" width="20px"></object>
                        {{$data_shop->time_feedback}}
                        <span> Thời Gian Phản Hồi </span>
                    </p>
                </div>
            </div>
        @else
            <h3 class="text-center">Không tìm thấy shop hợp lệ !</h3>
        @endif

        @if($data && count($data))
            <div class="sort-container flex">
                <div class="content-sort flex">
                    <p class="title-sort">Sắp Xếp Theo</p>
                    <a href="{{url('search?keyword='.$keyword.'&type=relate')}}">
                        <button class="btn-sort @if($type=='relate') active @endif">Liên Quan</button>
                    </a>
                    <a href="{{url('search?keyword='.$keyword.'&type=new')}}">
                        <button class="btn-sort @if($type=='new') active @endif">Mới Nhất</button>
                    </a>
                    <a href="{{url('search?keyword='.$keyword.'&type=best_seller')}}">
                        <button class="btn-sort @if($type=='best_seller') active @endif ">Bán Chạy</button>
                    </a>
                    <button class="btn-sort btn-option relative">Giá
                        <object data="icon/search/btn_arrow_down.svg"></object>
                    </button>
                </div>
                <div class="flex paginate">
                    <div class="content-paginate">
                        <span class="in-paginate">5</span>/<span class="all-paginate">10</span>
                    </div>
                    <button class="btn-sort next-paginate relative">
                        <object data="icon/search/polygon_left.svg"></object>
                    </button>
                    <button class="btn-sort prev-paginate relative">
                        <object data="icon/search/polygon_right.svg"></object>
                    </button>
                </div>
            </div>
        @else
            <h3 class="p-3 text-center">Không tìm thấy sản phẩm nào phù hợp !</h3>
        @endif
        <div class="product-search">
            <?php
            $pathSlide = "assets/frontend/Icon/home/Danh_muc";
            ?>
            <div class="container-section-product">

                @if($type=='relate')
                    @foreach($data as $value)
                        @php
                            $product_value = \App\Models\AttributeValueModel::where('product_id', $value->id)->get();
                            $price_ctv_min = \App\Models\AttributeValueModel::where('product_id', $value->id)->min('price_ctv');
                            $price_ctv_max = \App\Models\AttributeValueModel::where('product_id', $value->id)->max('price_ctv');
                        @endphp
                        <?php
                        $min = 0;
                        $max = 0;
                        foreach ($product_value as $item){
                            $ctv = $item->price - $item->price_ctv;
                            if ($min == 0){
                                $min = $ctv;
                            }elseif ($min > $ctv){
                                $min = $ctv;
                            }
                            if ($max == 0){
                                $max = $ctv;
                            }elseif ($max < $ctv){
                                $max = $ctv;
                            }
                        }
                        ?>
                        <div class="content border-box relative sp-product-content">
                            <a href="{{url('details_product/'.$value->id)}}">
                                <div class="img-product">
                                    <img src="{{$value->image}}" class="lazy">
                                </div>
                                <div class="item-content-product d-flex flex-column justify-content-between">
                                    <div class="title-product-n relative">
                                        <p class="card-title">{{$value->name}}</p>
                                    </div>
                                    <div class="product-price">
                                        @if($value->price_discount == $value->price)
                                            <p class="price"><span>{{number_format($value->price_discount)}} đ </p>
                                        @else
                                            <p class="price"><span>{{number_format($value->price_discount)}} đ</span> -
                                                <span class="price-sale">{{number_format($value->price)}} đ</span></p>
                                        @endif
                                        @if($price_ctv_min == $price_ctv_max)
                                                <div class="d-flex justify-content-between">
                                                    <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ</p>
                                                </div>
                                            @else
                                                <div class="d-flex justify-content-between">
                                                    <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ - {{number_format($price_ctv_max)}}đ</p>
                                                </div>
                                            @endif
                                            <div class="d-flex justify-content-between" style="margin-top: 10px">
                                                @if($min == $max)
                                                        <p>Lời: <span class="text-red">{{number_format($min)}}đ</span></p>
                                                    @else
                                                        <p>Lời: <span class="text-red">{{number_format($min)}}đ - {{number_format($max)}}đ</span></p>
                                                    @endif
                                            </div>
                                    </div>
                                    <div class="review">
                                        @if($value->avg_rating == 0)
                                            <img src="<?php echo asset("$path_1/0_sao.png")?>">
                                        @else
                                            @if($value->avg_rating == 1)
                                                <img src="<?php echo asset("$path_1/1_sao.png")?>">
                                            @elseif($value->avg_rating == 2)
                                                <img src="<?php echo asset("$path_1/2_sao.png")?>">
                                            @elseif($value->avg_rating == 3)
                                                <img src="<?php echo asset("$path_1/3_sao.png")?>">
                                            @elseif($value->avg_rating == 4)
                                                <img src="<?php echo asset("$path_1/4_sao.png")?>">
                                            @elseif($value->avg_rating == 5)
                                                <img src="<?php echo asset("$path_1/5_sao.png")?>">
                                            @elseif($value->avg_rating > 1 && $value->avg_rating < 2)
                                                <img src="<?php echo asset("$path_1/1_5_sao.png")?>">
                                            @elseif($value->avg_rating > 2 && $value->avg_rating < 3)
                                                <img src="<?php echo asset("$path_1/2_5_sao.png")?>">
                                            @elseif($value->avg_rating > 3 && $value->avg_rating < 4)
                                                <img src="<?php echo asset("$path_1/3_5_sao.png")?>">
                                            @elseif($value->avg_rating > 4 && $value->avg_rating < 5)
                                                <img src="<?php echo asset("$path_1/4_5_sao.png")?>">
                                            @endif
                                        @endif
                                        <p>Đã bán {{$value->is_sell}}</p>
                                    </div>
                                    <div class="review">
                                        <img style="width: 20px;"
                                             src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                        <p>{{$value->place}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @elseif($type=='new')
                    @foreach($data as $value)
                        @php
                            $product_value = \App\Models\AttributeValueModel::where('product_id', $value->id)->get();
                            $price_ctv_min = \App\Models\AttributeValueModel::where('product_id', $value->id)->min('price_ctv');
                            $price_ctv_max = \App\Models\AttributeValueModel::where('product_id', $value->id)->max('price_ctv');
                        @endphp
                        <?php
                        $min = 0;
                        $max = 0;
                        foreach ($product_value as $item){
                            $ctv = $item->price - $item->price_ctv;
                            if ($min == 0){
                                $min = $ctv;
                            }elseif ($min > $ctv){
                                $min = $ctv;
                            }
                            if ($max == 0){
                                $max = $ctv;
                            }elseif ($max < $ctv){
                                $max = $ctv;
                            }
                        }
                        ?>
                        <div class="content border-box relative sp-product-content">
                            <a href="{{url('details_product/'.$value->id)}}">
                                <div class="img-product">
                                    <img src="{{$value->image}}" class="lazy">
                                </div>
                                <div class="item-content-product d-flex flex-column justify-content-between">
                                    <div class="title-product-n relative">
                                        <p class="card-title">{{$value->name}}</p>
                                    </div>
                                    <div class="product-price">
                                        @if($value->price_discount == $value->price)
                                            <p class="price"><span>{{number_format($value->price_discount)}} đ </p>
                                        @else
                                            <p class="price"><span>{{number_format($value->price_discount)}} đ</span> -
                                                <span class="price-sale">{{number_format($value->price)}} đ</span></p>
                                        @endif
                                        @if($price_ctv_min == $price_ctv_max)
                                            <div class="d-flex justify-content-between">
                                                <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ</p>
                                            </div>
                                        @else
                                            <div class="d-flex justify-content-between">
                                                <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ - {{number_format($price_ctv_max)}}đ</p>
                                            </div>
                                        @endif
                                        <div class="d-flex justify-content-between" style="margin-top: 10px">
                                            @if($min == $max)
                                                <p>Lời: <span class="text-red">{{number_format($min)}}đ</span></p>
                                            @else
                                                <p>Lời: <span class="text-red">{{number_format($min)}}đ - {{number_format($max)}}đ</span></p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="review">
                                        @if($value->avg_rating == 0)
                                            <img src="<?php echo asset("$path_1/0_sao.png")?>">
                                        @else
                                            @if($value->avg_rating == 1)
                                                <img src="<?php echo asset("$path_1/1_sao.png")?>">
                                            @elseif($value->avg_rating == 2)
                                                <img src="<?php echo asset("$path_1/2_sao.png")?>">
                                            @elseif($value->avg_rating == 3)
                                                <img src="<?php echo asset("$path_1/3_sao.png")?>">
                                            @elseif($value->avg_rating == 4)
                                                <img src="<?php echo asset("$path_1/4_sao.png")?>">
                                            @elseif($value->avg_rating == 5)
                                                <img src="<?php echo asset("$path_1/5_sao.png")?>">
                                            @elseif($value->avg_rating > 1 && $value->avg_rating < 2)
                                                <img src="<?php echo asset("$path_1/1_5_sao.png")?>">
                                            @elseif($value->avg_rating > 2 && $value->avg_rating < 3)
                                                <img src="<?php echo asset("$path_1/2_5_sao.png")?>">
                                            @elseif($value->avg_rating > 3 && $value->avg_rating < 4)
                                                <img src="<?php echo asset("$path_1/3_5_sao.png")?>">
                                            @elseif($value->avg_rating > 4 && $value->avg_rating < 5)
                                                <img src="<?php echo asset("$path_1/4_5_sao.png")?>">
                                            @endif
                                        @endif
                                        <p>Đã bán {{$value->is_sell}}</p>
                                    </div>
                                    <div class="review">
                                        <img style="width: 20px;"
                                             src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                        <p>{{$value->place}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @elseif($type=='best_seller')
                    @foreach($data as $value)
                        @php
                            $product_value = \App\Models\AttributeValueModel::where('product_id', $value->id)->get();
                            $price_ctv_min = \App\Models\AttributeValueModel::where('product_id', $value->id)->min('price_ctv');
                            $price_ctv_max = \App\Models\AttributeValueModel::where('product_id', $value->id)->max('price_ctv');
                        @endphp
                        <?php
                        $min = 0;
                        $max = 0;
                        foreach ($product_value as $item){
                            $ctv = $item->price - $item->price_ctv;
                            if ($min == 0){
                                $min = $ctv;
                            }elseif ($min > $ctv){
                                $min = $ctv;
                            }
                            if ($max == 0){
                                $max = $ctv;
                            }elseif ($max < $ctv){
                                $max = $ctv;
                            }
                        }
                        ?>
                        <div class="content border-box relative sp-product-content">
                            <a href="{{url('details_product/'.$value->id)}}">
                                <div class="img-product">
                                    <img src="{{$value->image}}" class="lazy">
                                </div>
                                <div class="item-content-product d-flex flex-column justify-content-between">
                                    <div class="title-product-n relative">
                                        <p class="card-title">{{$value->name}}</p>
                                    </div>
                                    <div class="product-price">
                                        @if($value->price_discount == $value->price)
                                            <p class="price"><span>{{number_format($value->price_discount)}} đ </p>
                                        @else
                                            <p class="price"><span>{{number_format($value->price_discount)}} đ</span> -
                                                <span class="price-sale">{{number_format($value->price)}} đ</span></p>
                                        @endif
                                        @if($price_ctv_min == $price_ctv_max)
                                            <div class="d-flex justify-content-between">
                                                <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ</p>
                                            </div>
                                        @else
                                            <div class="d-flex justify-content-between">
                                                <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ - {{number_format($price_ctv_max)}}đ</p>
                                            </div>
                                        @endif
                                        <div class="d-flex justify-content-between" style="margin-top: 10px">
                                            @if($min == $max)
                                                <p>Lời: <span class="text-red">{{number_format($min)}}đ</span></p>
                                            @else
                                                <p>Lời: <span class="text-red">{{number_format($min)}}đ - {{number_format($max)}}đ</span></p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="review">
                                        @if($value->avg_rating == 0)
                                            <img src="<?php echo asset("$path_1/0_sao.png")?>">
                                        @else
                                            @if($value->avg_rating == 1)
                                                <img src="<?php echo asset("$path_1/1_sao.png")?>">
                                            @elseif($value->avg_rating == 2)
                                                <img src="<?php echo asset("$path_1/2_sao.png")?>">
                                            @elseif($value->avg_rating == 3)
                                                <img src="<?php echo asset("$path_1/3_sao.png")?>">
                                            @elseif($value->avg_rating == 4)
                                                <img src="<?php echo asset("$path_1/4_sao.png")?>">
                                            @elseif($value->avg_rating == 5)
                                                <img src="<?php echo asset("$path_1/5_sao.png")?>">
                                            @elseif($value->avg_rating > 1 && $value->avg_rating < 2)
                                                <img src="<?php echo asset("$path_1/1_5_sao.png")?>">
                                            @elseif($value->avg_rating > 2 && $value->avg_rating < 3)
                                                <img src="<?php echo asset("$path_1/2_5_sao.png")?>">
                                            @elseif($value->avg_rating > 3 && $value->avg_rating < 4)
                                                <img src="<?php echo asset("$path_1/3_5_sao.png")?>">
                                            @elseif($value->avg_rating > 4 && $value->avg_rating < 5)
                                                <img src="<?php echo asset("$path_1/4_5_sao.png")?>">
                                            @endif
                                        @endif
                                        <p>Đã bán {{$value->is_sell}}</p>
                                    </div>
                                    <div class="review">
                                        <img style="width: 20px;"
                                             src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                        <p>{{$value->place}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @else
                    Phân loại theo giá
                @endif
            </div>
        </div>
    </div>
</div>


