<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/address/style.css">
<link rel="stylesheet" type="text/css" href="css/home/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="w-100 bg-white p-3">
        <!-- End: checkout-->
        <div class="container">
            <div class="d-flex">
                <div class="col-lg-6 d-flex align-items-center ">
                    <object data="Icon/address/vector.svg" style="width: 20px">
                    </object>
                    <h3 class="ml-1" style="margin-bottom: 0; margin-left: 10px !important;">Địa chỉ của tôi</h3>
                </div>
                <div class="col-lg-6 d-flex justify-content-end">
                    <div class="btn btn-main text-white btn-add-new-address"
                         data-url="<?= URL::to('get_modal_address') ?>">
                        + Thêm Địa chỉ mới
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container p-0 bg-white mt-3 mb-3">
        <?php foreach ($address as $value): ?>
        <div class="border-bottom-view">
            <div class="pb-3 pt-3 pr-2 pl-2">
                <div class="d-flex">
                    <div class="col-lg-10">
                        <div class="form-check">
                            <label class="form-check-label pl-3 font-size-12" for="exampleRadios1">
                                <strong><?= $value['name'] . ' ' . $value['phone'] ?></strong>
                                <span class="pl-3"><?= $value['full_address'] ?></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-2 d-flex justify-content-end p-0">
                        <div class="btn btn-add-new-address"
                             data-url="<?= URL::to('get_modal_address?id_address=' . $value['id']) ?>">
                            <object data="Icon/address/edit.svg"></object>
                        </div>
                        <div class="btn btn-delete" data-url="<?= URL::to('address/delete/' . $value['id']) ?>"
                             style="text-decoration: underline">Xoá
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <a href="<?= URL::to('address/default/'.$value['id']) ?>"
                       class="btn font-large-1 btn-default-address"
                       style="color: #000 !important; text-decoration: underline">Để địa chỉ
                        làm mặc định
                    </a>
                    <label class="switch">
                        <input type="checkbox" class="check-default"
                              <?= $value['default'] == 1 ? 'checked' : '' ?>
                              data-id="{{$value->id}}"
                               data-url="{{url('address/default/'.$value['id'])}}"
                        >
                        <span class="slider round"></span>
                    </label>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>

    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
    <div class="w-100 modal-fixed" id="modalAddress">
    </div>
@include('frontend.address.modal_maps')
@include('frontend.address.modal_success')
@include('frontend.address.modal_error')
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
@include('frontend.address.modal_delete')

<script src="js/address/index.js"></script>
<script src="js/address/maps.js"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYutTAaM_NN7-HgRsJb5mPky4ANnuesaU&callback=initMap"
        type="text/javascript"></script>
</body>
<!-- END: Body-->
</html>
