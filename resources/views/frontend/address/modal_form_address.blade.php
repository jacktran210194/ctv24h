<?php
use Illuminate\Support\Facades\URL;
?>
<div class="position-absolute position-center">
    <div class="p-5 bg-white" style="border-radius: 18px;">
        <h3 style="color: #005DB6">THÊM ĐỊA CHỈ MỚI</h3>
        <div class="mt-4" style="width: 25vw">
            <div class="form-group">
                <input class="form-control" id="phone" placeholder="Số điện thoại"
                       value="<?= isset($data) ? $data->phone : '' ?>">
            </div>
            <div class="form-group">
                <input class="form-control" id="name" placeholder="Họ và tên"
                       value="<?= isset($data) ? $data->name : '' ?>">
            </div>
            <div class="form-group">
                <select class="form-control form-address" data-value="city" id="citySelect"
                        data-url="<?= URL::to('filter_address?type=city&&id=') ?>">
                    <option value="">Tỉnh/Thành Phố</option>
                    <?php foreach ($city as $value): ?>
                    <option
                        <?= isset($data) && $data->city_id == $value['id'] ? 'selected' : '' ?> value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control form-address" data-value="district" id="districtSelect"
                        data-url="<?= URL::to('filter_address?type=district&&id=') ?>">
                    <option value="">Quận/Huyện</option>
                    <?php if (isset($district)): ?>
                    <?php foreach ($district as $value): ?>
                    <option
                        <?= isset($data) && $data->district_id == $value['id'] ? 'selected' : '' ?> value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" id="wardSelect">
                    <option value="">Phường/Xã</option>
                    <?php if (isset($ward)): ?>
                    <?php foreach ($ward as $val): ?>
                    <option
                        <?= isset($data) && $data->ward_id == $val['ward_id'] ? 'selected' : '' ?> value="<?= $val['ward_id'] ?>"><?= $val['name'] ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
            <div class="form-group">
                <input class="form-control" id="address" placeholder="Toà nhà, tên đường..."
                       value="<?= isset($data) ? $data->address : '' ?>">
            </div>
            <div class="form-group btn-show-maps">
                <div class="form-control d-flex p-2 form-maps" style="height: auto !important;">
                    <div class="p-2 d-flex">
                        <object data="Icon/address/maps.svg"></object>
                        <input id="addressMaps" value=""
                               style="display:none;">
{{--                        <input id="addressMaps" value="<?= isset($data) ? $data->address_maps : '' ?>"--}}
{{--                               style="display:none;">--}}

                        <div class="ml-3 position-address">
{{--                            <?php if (isset($data)) : ?>--}}
{{--                            <?= $data->address_maps ?>--}}
{{--                            <?php else: ?>--}}
                            <strong>Chọn vị trí trên bản đồ</strong>
                            <br/>
                            <span>Giúp đơn hàng được giao nhanh nhất</span>
<!--                            --><?php //endif; ?>
                        </div>
                    </div>
                    <div>
                        <object data="Icon/address/next.svg"></object>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <p class="mb-2">Đặt làm mặc định</p>
                <label class="switch">
                    <input id="default" type="checkbox" <?= isset($data) && $data->default === 1 ? 'checked' : '' ?>>
                    <span class="slider round"></span>
                </label>
            </div>
            <div class="form-group d-flex justify-content-end">
                <div type="button" class="btn icon-back-modal btn-back-modal-address">Trở lại</div>
                <div type="button" class="btn btn-main ml-2 btn-add-maps"
                     data-lat="<?= isset($data) ? $data->latitude : '' ?>"
                     data-log="<?= isset($data) ? $data->longitude : '' ?>"
                     data-id="<?= isset($data) ? $data->id : '' ?>"
                     data-url="<?= URL::to('address/save') ?>">
                    Hoàn thành
                </div>
            </div>
        </div>
    </div>
</div>
