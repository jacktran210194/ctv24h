<div class="w-100 modal-fixed" id="modalMaps">
    <div class="position-absolute position-center">
        <div class="p-5 bg-white" style="border-radius: 18px;">
            <h3 style="color: #005DB6">VỊ TRÍ TRÊN BẢN ĐỒ</h3>
            <div class="mt-4" style="width: 40vw">
                <div class="form-group">
                    <div id="map"></div>
                </div>
                <div class="form-group d-flex justify-content-end">
                    <div type="button" class="btn icon-back-modal btn-back-modal-maps">Trở lại</div>
                    <div type="button" class="btn btn-main ml-2 btn-complete-maps">Hoàn thành</div>
                </div>
            </div>
        </div>
    </div>
</div>
