
<div class="w-100 modal-fixed" id="modalError" style="display: none">
    <div class="position-absolute position-center">
        <div class="p-5 bg-white" style="border-radius: 18px;">
            <section class="c-container">
                <div class="o-circle c-container__circle o-circle__sign--failure">
                    <div class="o-circle__sign"></div>
                </div>
            </section>
            <h3 class="text-danger text-mes-error">Lưu không thành công</h3>
        </div>
    </div>
</div>
