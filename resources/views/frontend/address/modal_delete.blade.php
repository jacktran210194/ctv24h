<div class="w-100 modal-fixed" id="modalDelete" style="display: none">
    <div class="position-absolute position-center" style="top: 15vh">
        <div class="p-2 bg-white" style="border-radius: 18px;">
            <div class="modal-body">
                <p class="message-modal">Bạn có muốn xoá ?</p>
            </div>
            <div class="modal-footer" style="width: 40vw;">
                <button type="button" class="btn btn-success btn-confirm-delete" data-url="" data-dismiss="modal">Đồng ý</button>
                <button type="button" class="btn btn-danger btn-close-modal-delete" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

