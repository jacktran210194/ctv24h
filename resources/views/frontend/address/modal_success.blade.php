<div class="w-100 modal-fixed" id="modalSuccess" style="display: none">
    <div class="position-absolute position-center">
        <div class="p-5 bg-white" style="border-radius: 18px;">
            <section class="c-container">
                <div class="o-circle c-container__circle o-circle__sign--success">
                    <div class="o-circle__sign"></div>
                </div>
            </section>
            <h3 class="text-success text-mes-success">Lưu thành công</h3>
        </div>
    </div>
</div>
