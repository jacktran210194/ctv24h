<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/DetailOrder/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->
    <?php
    $path = "assets/frontend/Icon/DetailOrder";
    ?>
    <div class="container body-page">
        @if(isset($data))
            <div class="d-flex nav-page">
                <a href="{{url("/my_history/by_product")}}" class="come-back">
                    TRỞ LẠI
                </a>
                <div class="title-details-order d-flex">
                    <p class="title-order">Chi Tiết Đơn Hàng: {{isset($data) ? $data['order_code'] : ''}}</p>
                    <p class="delivered">{{isset($data) ? $data['is_status'] : ''}}</p>
                </div>
            </div>
            @if($data->status<4)
                <div class="detail-deliver">
                    <div class="d-flex steps-order">
                        @if($data['status'] >= 1)
                            <div class="box-steps orders-placed active">
                                <div class="d-flex image-icon">
                                    <div class="icon-steps">
                                        <img src="<?php echo asset("$path/Group-16715.png") ?>">
                                    </div>
                                </div>
                                <div class="text-steps">
                                    <p class="title-steps">Đơn hàng đã đặt </p>
                                    {{--                            <p class="times">14:31 12-01-2021</p>--}}
                                </div>
                            </div>
                        @else
                            <div class="box-steps orders-placed" style="opacity: 50%;">
                                <div class="d-flex image-icon">
                                    <div class="icon-steps">
                                        <img src="<?php echo asset("$path/Group-16715.png") ?>">
                                    </div>
                                </div>
                                <div class="text-steps">
                                    <p class="title-steps">Đơn hàng đã đặt </p>
                                    {{--                            <p class="times">14:31 12-01-2021</p>--}}
                                </div>
                            </div>
                        @endif
                        @if($data['status'] >= 2)
                            <div class="box-steps orders-placed active">
                                <div class="d-flex image-icon">
                                    <div class="icon-steps">
                                        <img src="<?php echo asset("$path/Group-16208.png") ?>">
                                    </div>
                                </div>
                                <div class="text-steps">
                                    <p class="title-steps">Đã xác nhận thanh toán</p>
                                    {{--                            <p class="times">14:31 12-01-2021</p>--}}
                                </div>
                            </div>
                        @else
                            <div class="box-steps orders-placed" style="opacity: 50%;">
                                <div class="d-flex image-icon">
                                    <div class="icon-steps">
                                        <img src="<?php echo asset("$path/Group-16208.png") ?>">
                                    </div>
                                </div>
                                <div class="text-steps">
                                    <p class="title-steps">Đã xác nhận thanh toán</p>
                                    {{--                            <p class="times">14:31 12-01-2021</p>--}}
                                </div>
                            </div>
                        @endif
                        @if($data['status'] >= 2)
                            <div class="box-steps orders-placed active">
                                <div class="d-flex image-icon">
                                    <div class="icon-steps">
                                        <img src="<?php echo asset("$path/fast-delivery.png") ?>">
                                    </div>
                                </div>
                                <div class="text-steps">
                                    <p class="title-steps">Đã giao cho ĐVVC</p>
                                    {{--                            <p class="times">14:31 12-01-2021</p>--}}
                                </div>
                            </div>
                        @else
                            <div class="box-steps orders-placed" style="opacity: 50%;">
                                <div class="d-flex image-icon">
                                    <div class="icon-steps">
                                        <img src="<?php echo asset("$path/fast-delivery.png") ?>">
                                    </div>
                                </div>
                                <div class="text-steps">
                                    <p class="title-steps">Đã giao cho ĐVVC</p>
                                    {{--                            <p class="times">14:31 12-01-2021</p>--}}
                                </div>
                            </div>
                        @endif
                        @if($data['status'] == 3)
                            <div class="box-steps orders-placed active">
                                <div class="d-flex image-icon">
                                    <div class="icon-steps">
                                        <img src="<?php echo asset("$path/delivery-box.png") ?>">
                                    </div>
                                </div>
                                <div class="text-steps">
                                    <p class="title-steps">Đơn hàng đã nhận</p>
                                    {{--                            <p class="times">14:31 12-01-2021</p>--}}
                                </div>
                            </div>
                        @else
                            <div class="box-steps orders-placed" style="opacity: 50%">
                                <div class="d-flex image-icon">
                                    <div class="icon-steps">
                                        <img src="<?php echo asset("$path/delivery-box.png") ?>">
                                    </div>
                                </div>
                                <div class="text-steps">
                                    <p class="title-steps">Đơn hàng đã nhận</p>
                                    {{--                            <p class="times">14:31 12-01-2021</p>--}}
                                </div>
                            </div>
                        @endif
                        @if($data['status'] == 3)
                            <div class="box-steps orders-placed active">
                                <div class="d-flex image-icon">
                                    <div class="icon-steps">
                                        <img src="<?php echo asset("$path/cancel.png") ?>">
                                    </div>
                                </div>
                                <div class="text-steps">
                                    <p class="title-steps">Đơn hàng đã giao</p>
                                    {{--                            <p class="times">14:31 12-01-2021</p>--}}
                                </div>
                            </div>
                        @else
                            <div class="box-steps orders-placed" style="opacity: 50%">
                                <div class="d-flex image-icon">
                                    <div class="icon-steps">
                                        <img src="<?php echo asset("$path/cancel.png") ?>">
                                    </div>
                                </div>
                                <div class="text-steps">
                                    <p class="title-steps">Đơn hàng đã giao</p>
                                    {{--                            <p class="times">14:31 12-01-2021</p>--}}
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="d-flex information-order">
                        <div class="box-information">
                            <p class="title-information">Địa chỉ người nhận </p>
                            @if($data->collaborator_id == 1)
                                <p class="name">{{isset($data) ? $data->name_customer_ctv : ''}} {{isset($data) ? $data->phone_customer_ctv : ''}}</p>
                                <p class="address">{{isset($data) ? $data->address_customer_ctv : ''}}</p>
                            @else
                                <p class="name">{{isset($data) ? $data->customer_name : ''}} {{isset($data) ? $data->customer_phone : ''}}</p>
                                <p class="address">{{isset($data) ? $data->customer_address : ''}}</p>
                            @endif
                        </div>
                        <div class="box-information">
                            <p class="title-information">Tình trạng vận chuyển</p>
                            <p class="transporters">Nhà vận chuyển: <span
                                    class="name-transporters">{{$data->shipping}}</span></p>
                            <p class="status">Tình trạng: <span
                                    class="review-status">{{isset($data) ? $data['is_status'] : ''}}</span></p>
                        </div>
                        <div class="box-information">
                            <p class="title-information">Hình thức thanh toán</p>
                            <p class="name">{{$data->payment}} </p>
                        </div>
                    </div>
                </div>
            @else
                <h3 style="color: red;" class="text-center text-justify">{{$data->is_status}}</h3>
                <div class="border-bottom"></div>
            @endif
            <div class="item-content">
                <div class="d-flex item-shop">
                    <div class="d-flex name-shop">
                        <object data="Icon/cart/shop_name.svg" style="width: 23px; height: 23px;"></object>
                        <p class="name">{{$data->shop_name}}</p>
                    </div>
                    <button class="btn-like">Yêu thích</button>
                    <a href="{{url("seeshop/".$data->shop_id)}}">
                        <button class="btn-seeshop">Xem shop</button>
                    </a>
                    <a class="btn-chat-shop preventdefault" href="{{url('/chat')}}">
                        <img src="<?php echo asset("$path/chat.png") ?>">
                        <p>Chat</p>
                    </a>
                </div>
                @if(isset($data_order))
                    @foreach($data_order as $value)
                        <div class="d-flex item-product">
                            <div class="d-flex image-product">
                                <img width="113px" height="117px" src="{{$value->product_image}}">
                            </div>
                            <div class="infor-product">
                                <a href="{{url('details_product', $value->product_id)}}" class="title-product">{{$value->product_name}}</a>
                                <p class="brank"> {{$value->value_attr}} - {{$value->size_of_value}}</p>
                                <p class="quantity">x{{$value->quantity}}</p>
                            </div>
                            <div class="price-product">
                                {{--                                    <p class="price">1.400.000đ</p>--}}
                                <p class="discount-price">{{number_format($value->total_price)}}đ</p>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="bottom d-flex">
                <div class="total-price">
                    @if(isset($data->bonus))
                        <div class="d-flex content">
                            <p class="text-content">Tổng tiền sản phẩm: </p>
                            <p class="text-right">{{isset($data) ? number_format($data['total_price'] - $data->bonus - 10000) : ''}}
                                đ</p>
                        </div>
                        @else
                        <div class="d-flex content">
                            <p class="text-content">Tổng tiền sản phẩm: </p>
                            <p class="text-right">{{isset($data) ? number_format($data['total_price'] - 10000) : ''}}
                                đ</p>
                        </div>
                    @endif
                    <div class="d-flex content">
                        <p class="text-content">Phí vận chuyển: </p>
                        <p class="text-right">10.000đ</p>
                    </div>
                    @if(isset($data->bonus))
                        <div class="d-flex content">
                            <p class="text-content">Lợi nhuận: </p>
                            <p class="text-right">{{number_format($data->bonus)}}đ</p>
                        </div>
                     @endif
                    <div class="d-flex content all-price">
                        <p class="text-content">Tổng số tiền:</p>
                        <p class="text-right">{{isset($data) ? number_format($data['total_price']) : ''}} đ</p>
                    </div>
                </div>
            </div>
            <div class="button d-flex">
                <button class="btn-buy-again preventdefault">Mua lần nữa</button>
            </div>
        @else
            <h3 class="text-justify text-center">Mã vận đơn không tồn tại !</h3>
        @endif
    </div>
    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
