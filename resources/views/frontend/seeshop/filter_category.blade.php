<!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/seeshop/style.css">
<link rel="stylesheet" type="text/css" href="css/search/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->
    <div class="body-seeshop">
        <input type="hidden" class="id_shop" value="{{$dataInfoShop->id}}">
        <div class="body-seaech flex">
    <?php
    $data_login = \Illuminate\Support\Facades\Session::get('data_customer');
    $data_shop = \App\Models\ShopModel::where('customer_id', $data_login->id)->first();
    $path = "assets/frontend/Icon/products";
    $page = 'category_shop';
    ?>
    <div class="fillter">
        <div class="title-fillter flex">
            <div class="icon_fillter">
                <object data="icon/search/icon_fillter.svg" width="12px"></object>
            </div>
            <div class="title-text">BỘ LỌC TÌM KIẾM</div>
        </div>
        <div class="filter-catagory">
            <p class="title">Theo danh mục</p>
            @if(isset($categoryAll))
                @foreach($categoryAll as $val)
                        <div class="sub-filter">
                            <p class="title">{{$val->name}}</p>
                        </div>
                @endforeach
            @endif
            <p class="load-more">Xem Thêm ></p>
        </div>
        <div class="filter-catagory">
            <p class="title">Nơi bán</p>
            <p class="load-more btn_load_more" style="cursor: pointer;">Xem Thêm ></p>
            <p class="load-more btn_hide_more" style="cursor: pointer; display: none;">Ẩn bớt ></p>
        </div>
        <div class="filter-catagory">
            <p class="title">Đơn vị vận chuyển</p>
        </div>
        <div class="filter-catagory form-fillter">
            <p class="title">Khoảng giá</p>
            <div class="form-fillter-price flex">
                <input type="number" placeholder="TỪ" class="price-filter-1 check_price_1">
                <span class="icon-space">-</span>
                <input type="number" placeholder="ĐẾN" class="price-filter-2 check_price_2">
            </div>
            <button class="btn btn-filter">Áp dụng</button>
        </div>
        <div class="filter-catagory">
            <p class="title">Loại shop</p>
            <div class="flex content">
                <input class="check_type_shop" type="checkbox">
                <p class="text">Shopee Mall</p>
            </div>
            <div class="flex content">
                <input class="check_type_shop" type="checkbox">
                <p class="text">Shop yêu thích</p>
            </div>
        </div>
        <div class="filter-catagory">
            <p class="title">Tình trạng</p>
            <div class="flex content filter-status">
                <input class="check_type_product" type="checkbox" value="1">
                <p class="text">Sản phẩm mới </p>
            </div>
            <div class="flex content filter-status">
                <input class="check_type_product" type="checkbox" value="0">
                <p class="text">Sản phẩm đã từng </p>
            </div>
        </div>
        <div class="filter-catagory">
            <p class="title">Đánh giá </p>
            <div class="flex content">
                <button class="filter-ratings check_rating" value="5">
                    <object data="icon/search/icon_star_five.svg"></object>
                </button>
            </div>
            <div class="flex content">
                <button class="d-flex align-items-center w-100 filter-ratings check_rating" value="4">
                    <object data="icon/search/icon_star_for.svg"></object>
                    <p class="text">trở lên </p>
                </button>
            </div>
            <div class="flex content">
                <button class="d-flex align-items-center w-100 filter-ratings check_rating" value="3">
                    <object data="icon/search/icon_star_three.svg"></object>
                    <p class="text">trở lên </p>
                </button>
            </div>
            <div class="flex content">
                <button class="d-flex align-items-center w-100 filter-ratings check_rating" value="2">
                    <object data="icon/search/icon_star_tow.svg"></object>
                    <p class="text">trở lên </p>
                </button>
            </div>
            <div class="flex content">
                <button class="d-flex align-items-center w-100 filter-ratings check_rating" value="1">
                    <object data="icon/search/icon_star_one.svg"></object>
                    <p class="text">trở lên </p>
                </button>
            </div>
        </div>
        <div class="filter-catagory">
            <p class="title">Dịch vụ và khuyến mãi</p>
            <div class="flex content">
                <input class="check_service" type="checkbox">
                <p class="text">Freeship Xtra </p>
            </div>
            <div class="flex content">
                <input class="check_service" type="checkbox">
                <p class="text">Hoàn xu Xtra </p>
            </div>
            <div class="flex content">
                <input class="check_service" type="checkbox">
                <p class="text">Đang giảm giá </p>
            </div>
            <div class="flex content">
                <input class="check_service" type="checkbox">
                <p class="text">Miễn phí vận chuyển </p>
            </div>
            <p class="load-more">Thêm
                <object width="7px" data="icon/search/icon_arrow_down.svg"></object>
            </p>
        </div>
        <button class="btn clear-all-shop">Xóa tất cả</button>
    </div>
    <div class="container-left">
        <div class="sort-container flex">
            <div class="content-sort flex">
                <p class="title-sort">Sắp Xếp Theo</p>
                <button class="btn-fillter btn_filter_default active"
                        data-url="sort_related"
                >
                    Liên Quan
                </button>
                <button class="btn-fillter"
                        data-url="sort_new"
                >
                    Mới Nhất
                </button>
                <button class="btn-fillter" data-url="best_seller">
                    Bán Chạy
                </button>
                <button class=" btn-fillter btn-option relative" data-url="price_asc">
                    Giá
                </button>
            </div>
            <div class="flex paginate"></div>
        </div>
        <div class="product-search">
            <div class="container-section-product">
                <?php $path_1 = "assets/frontend/Icon/products"; ?>
                @if(count($dataProduct) && $dataProduct)
                    @foreach ($dataProduct as $value)
                            @php
                            if (isset($value->product_id)){
                                $id = $value->product_id;
                            }else{
                                $id = $value->id;
                            }
                                $product_value = \App\Models\AttributeValueModel::where('product_id', $id)->get();
                                $price_ctv_min = \App\Models\AttributeValueModel::where('product_id', $id)->min('price_ctv');
                                $price_ctv_max = \App\Models\AttributeValueModel::where('product_id', $id)->max('price_ctv');
                            @endphp
                            <?php
                            $min = 0;
                            $max = 0;
                            foreach ($product_value as $item){
                                $ctv = $item->price - $item->price_ctv;
                                if ($min == 0){
                                    $min = $ctv;
                                }elseif ($min > $ctv){
                                    $min = $ctv;
                                }
                                if ($max == 0){
                                    $max = $ctv;
                                }elseif ($max < $ctv){
                                    $max = $ctv;
                                }
                            }
                            ?>
                        <div class="content border-box relative sp-product-content">
                            <a href="{{url('details_product/'.(isset($value->product_id)?$value->product_id:$value->id))}}">
                                <div class="img-product">
                                    <img src="{{$value->image}}" class="lazy">
                                </div>
                                <div class="item-content-product d-flex justify-content-between flex-column">
                                    <div class="title-product-n relative">
                                        <p class="card-title">{{$value->name}}</p>
                                    </div>
                                    <div class="product-price">
                                        <p class="price">{{number_format((int)$value->price_discount)}}đ
                                            - {{number_format((int)$value->price)}}đ</p>
                                        <div class="d-flex justify-content-between">
                                            <p class="pass">Giá CTV: {{number_format((int)$price_ctv_min)}}đ - {{number_format((int)$price_ctv_max)}}đ</p>
                                        </div>
                                        <div class="d-flex justify-content-between" style="margin-top: 10px">
                                            <p>Lời: <span class="text-red">{{number_format($min)}}đ - {{number_format($max)}}đ</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="review">
                                        @if($value->avg_rating == 0)
                                            <img src="<?php echo asset("$path_1/0_sao.png")?>">
                                        @else
                                            @if($value->avg_rating == 1)
                                                <img src="<?php echo asset("$path_1/1_sao.png")?>">
                                            @elseif($value->avg_rating == 2)
                                                <img src="<?php echo asset("$path_1/2_sao.png")?>">
                                            @elseif($value->avg_rating == 3)
                                                <img src="<?php echo asset("$path_1/3_sao.png")?>">
                                            @elseif($value->avg_rating == 4)
                                                <img src="<?php echo asset("$path_1/4_sao.png")?>">
                                            @elseif($value->avg_rating == 5)
                                                <img src="<?php echo asset("$path_1/5_sao.png")?>">
                                            @elseif($value->avg_rating > 1 && $value->avg_rating < 2)
                                                <img src="<?php echo asset("$path_1/1_5_sao.png")?>">
                                            @elseif($value->avg_rating > 2 && $value->avg_rating < 3)
                                                <img src="<?php echo asset("$path_1/2_5_sao.png")?>">
                                            @elseif($value->avg_rating > 3 && $value->avg_rating < 4)
                                                <img src="<?php echo asset("$path_1/3_5_sao.png")?>">
                                            @elseif($value->avg_rating > 4 && $value->avg_rating < 5)
                                                <img src="<?php echo asset("$path_1/4_5_sao.png")?>">
                                            @endif
                                        @endif
                                        <p>Đã bán {{$value->is_sell}}</p>
                                    </div>
                                    <div class="review">
                                        <img style="width: 20px;"
                                             src="Icon/home/Danh_muc/icon-like.png">
                                        <p>{{$value->place}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @else
                    <h3 class="p-4 text-red">Không có sản phẩm nào phù hợp !</h3>
                @endif
            </div>
        </div>
    </div>
</div>
    </div>
@include('frontend.base.footer')
@include('frontend.base.modal')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
<script src="js/seeshop/index.js"></script>
<script src="js/seeshop/filter.js"></script>
</body>


<script>
    $(document).ready(function () {
        $(document).on('click', '.btn_load_more', function () {
            $('.load_more_city').show();
            $(this).hide();
            $('.btn_hide_more').show();
        });

        $(document).on('click', '.btn_hide_more', function () {
            $('.load_more_city').hide();
            $(this).hide();
            $('.btn_load_more').show();
        });
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            data['customer_id'] = $(this).attr(' data-customer');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            data['customer_id'] = $(this).attr(' data-customer');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "GET",
                data: data,
                dataType: 'json',
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });

    $(document).ready(function () {
        $(document).on("click", ".btn-follow", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            data['customer_id'] = $(this).attr(' data-customer');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });

    $(document).ready(function () {
        $(document).on("click", ".btn-follow", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            data['customer_id'] = $(this).attr(' data-customer');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "GET",
                data: data,
                dataType: 'json',
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });
</script>
<!-- END: Body-->
</html>


