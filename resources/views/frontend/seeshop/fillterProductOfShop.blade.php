<div class="body-seaech flex">
    <?php
    $data_login = \Illuminate\Support\Facades\Session::get('data_customer');
    $data_shop = \App\Models\ShopModel::where('customer_id', $data_login->id)->first();
    $path = "assets/frontend/Icon/products";
    ?>
    <div class="fillter">
        <div class="title-fillter flex">
            <div class="icon_fillter">
                <object data="icon/search/icon_fillter.svg" width="12px"></object>
            </div>
            <div class="title-text">BỘ LỌC TÌM KIẾM</div>
        </div>
        <div class="filter-catagory">
            <p class="title">Theo danh mục</p>
            @if(count($dataCategoryShop) && $dataCategoryShop)
                @foreach($dataCategoryShop as $value)
                    <div class="flex content content-filter">
                        <input class="check_category" type="checkbox" id="{{$value->cate_id}}">
                        <p class="text">{{$value->name}}</p>
                    </div>
                @endforeach
            @else
                <p>Không có danh mục</p>
            @endif
        </div>
        <div class="filter-catagory">
            <p class="title">Nơi bán</p>
            @if(count($list_city) && $list_city)
                @php $i=0; @endphp
                @foreach($list_city as $value_city)
                    @if($i++ <= 4)
                        <div class="flex content filter-place">
                            <input class="check_place" type="checkbox" value="{{$value_city->name}}">
                            <p class="text">{{$value_city->name}}</p>
                        </div>
                    @endif
                @endforeach
            @endif
            @if(count($list_city) && $list_city)
                @php $y=0; @endphp
                @foreach($list_city as $value_city)
                    @if($y++ >= 5)
                        <div style="display: none" class="flex content load_more_city filter-place">
                            <input class="check_place" type="checkbox" value="{{$value_city->name}}">
                            <p class="text">{{$value_city->name}}</p>
                        </div>
                    @endif
                @endforeach
            @endif
            <p class="load-more btn_load_more" style="cursor: pointer;">Xem Thêm ></p>
            <p class="load-more btn_hide_more" style="cursor: pointer; display: none;">Ẩn bớt ></p>
        </div>
        <div class="filter-catagory">
            <p class="title">Đơn vị vận chuyển</p>
            @if(isset($shipping))
                @foreach($shipping as $value)
                    <div class="flex content filter-shipping">
                        <input class="check_shipping" type="checkbox" value="{{$value->id}}">
                        <p class="text">{{$value->name }}</p>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="filter-catagory form-fillter">
            <p class="title">Khoảng giá</p>
            <div class="form-fillter-price flex">
                <input type="number" placeholder="TỪ" class="price-filter-1 check_price_1">
                <span class="icon-space">-</span>
                <input type="number" placeholder="ĐẾN" class="price-filter-2 check_price_2">
            </div>
            <button class="btn btn-filter">Áp dụng</button>
        </div>
        <div class="filter-catagory">
            <p class="title">Loại shop</p>
            <div class="flex content">
                <input class="check_type_shop preventdefault" type="checkbox">
                <p class="text">Shopee Mall</p>
            </div>
            <div class="flex content">
                <input class="check_type_shop preventdefault" type="checkbox">
                <p class="text">Shop yêu thích</p>
            </div>
        </div>
        <div class="filter-catagory">
            <p class="title">Tình trạng</p>
            <div class="flex content filter-status">
                <input class="check_type_product" type="checkbox" value="1">
                <p class="text">Sản phẩm mới </p>
            </div>
            <div class="flex content filter-status">
                <input class="check_type_product" type="checkbox" value="0">
                <p class="text">Sản phẩm đã từng </p>
            </div>
        </div>
        <div class="filter-catagory">
            <p class="title">Đánh giá </p>
            <div class="flex content">
                <button class="filter-ratings check_rating" value="5">
                    <object data="icon/search/icon_star_five.svg"></object>
                </button>
            </div>
            <div class="flex content">
                <button class="d-flex align-items-center w-100 filter-ratings check_rating" value="4">
                    <object data="icon/search/icon_star_for.svg"></object>
                    <p class="text">trở lên </p>
                </button>
            </div>
            <div class="flex content">
                <button class="d-flex align-items-center w-100 filter-ratings check_rating" value="3">
                    <object data="icon/search/icon_star_three.svg"></object>
                    <p class="text">trở lên </p>
                </button>
            </div>
            <div class="flex content">
                <button class="d-flex align-items-center w-100 filter-ratings check_rating" value="2">
                    <object data="icon/search/icon_star_tow.svg"></object>
                    <p class="text">trở lên </p>
                </button>
            </div>
            <div class="flex content">
                <button class="d-flex align-items-center w-100 filter-ratings check_rating" value="1">
                    <object data="icon/search/icon_star_one.svg"></object>
                    <p class="text">trở lên </p>
                </button>
            </div>
        </div>
        <div class="filter-catagory">
            <p class="title">Dịch vụ và khuyến mãi</p>
            <div class="flex content">
                <input class="check_service preventdefault" type="checkbox">
                <p class="text">Freeship Xtra </p>
            </div>
            <div class="flex content">
                <input class="check_service preventdefault" type="checkbox">
                <p class="text">Hoàn xu Xtra </p>
            </div>
            <div class="flex content">
                <input class="check_service preventdefault" type="checkbox">
                <p class="text">Đang giảm giá </p>
            </div>
            <div class="flex content">
                <input class="check_service preventdefault" type="checkbox">
                <p class="text">Miễn phí vận chuyển </p>
            </div>
            <p class="load-more">Thêm
                <object width="7px" data="icon/search/icon_arrow_down.svg"></object>
            </p>
        </div>
        <button class="btn clear-all-filter" style="margin-left: -4px">Xóa tất cả</button>
    </div>
    <div class="container-left">
        <div class="sort-container flex">
            <div class="content-sort flex">
                <p class="title-sort">Sắp Xếp Theo</p>
                <button class="btn-fillter btn_filter_default active"
                        data-url="sort_related"
                >
                    Liên Quan
                </button>
                <button class="btn-fillter"
                        data-url="sort_new"
                >
                    Mới Nhất
                </button>
                <button class="btn-fillter" data-url="best_seller">
                    Bán Chạy
                </button>
                <button class=" btn-fillter btn-option relative" data-url="price_asc">
                    Giá
                </button>
            </div>
            <div class="flex paginate"></div>
        </div>
        <div class="product-search">
            <div class="container-section-product">

            </div>
        </div>
    </div>
</div>
