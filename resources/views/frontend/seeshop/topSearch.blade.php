<div class="suggestion section-san-pham-trend top-search">
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    $pathTow = "assets/frontend/Icon/seeshop";
    ?>
    <h3 class="title-section">tìm kiếm hàng đầu</h3>
    <div class="product-section relative">
        <div class="container-section-product slide-top-search">
            <div class="content border-box relative sp-product-content preventdefault">
                <div class="img-product">
                    <img src="<?php echo asset("$pathTow/top_search.png")?>">
                </div>
                <div class="order-search">
                    <div class="relative">
                        <img src="<?php echo asset("$pathTow/Vector.png")?>">
                        <p class="number-order number-order-first">1</p>
                    </div>
                </div>
                <div style="background: #ffffff; padding:12px 0; margin-top: 1px;border-bottom-left-radius: 4px;border-bottom-right-radius: 4px;">
                    <div class="title-product-n relative">
                        <span>Quần jean ống rộng Kèm Hình Thật...</span>
                    </div>
                    <div class="product-price">
                        <p class="price">270.000đ - 370.000đ</p>
                        <p class="pass">Giá CTV: ********</p>
                        <div class="form btn-sign-up relative">
                            <a href="#">Đăng ký CTV để xem giá</a>
                        </div>
                    </div>
                    <div class="review">
                        <img src="<?php echo asset("$path/icon-review.png")?>">
                        <p>Đã bán 10</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                        <p>Hà Nội</p>
                    </div>
                </div>
            </div>
            <div class="content border-box relative sp-product-content preventdefault">
                <div class="img-product">
                    <img src="<?php echo asset("$pathTow/top_search_1.png")?>">
                </div>
                <div class="order-search">
                    <div class="relative">
                        <img src="<?php echo asset("$pathTow/Vector_1.png")?>">
                        <p class="number-order">2</p>
                    </div>
                </div>
                <div style="background: #ffffff; padding:12px 0; margin-top: 1px;border-bottom-left-radius: 4px;border-bottom-right-radius: 4px;">
                    <div class="title-product-n relative">
                        <span>Quần jean ống rộng Kèm Hình Thật...</span>
                    </div>
                    <div class="product-price">
                        <p class="price">270.000đ - 370.000đ</p>
                        <p class="pass">Giá CTV: ********</p>
                        <div class="form btn-sign-up relative">
                            <a href="#">Đăng ký CTV để xem giá</a>
                        </div>
                    </div>
                    <div class="review">
                        <img src="<?php echo asset("$path/icon-review.png")?>">
                        <p>Đã bán 10</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                        <p>Hà Nội</p>
                    </div>
                </div>
            </div>
            <div class="content border-box relative sp-product-content preventdefault">
                <div class="img-product">
                    <img src="<?php echo asset("$pathTow/top_search_2.png")?>">
                </div>
                <div class="order-search">
                    <div class="relative">
                        <img src="<?php echo asset("$pathTow/Vector_2.png")?>">
                        <p class="number-order">3</p>
                    </div>
                </div>
                <div style="background: #ffffff; padding:12px 0; margin-top: 1px;border-bottom-left-radius: 4px;border-bottom-right-radius: 4px;">
                    <div class="title-product-n relative">
                        <span>Giày sandal nữ cao gót Kèm Hình Thật...</span>
                    </div>
                    <div class="product-price">
                        <p class="price">270.000đ - 370.000đ</p>
                        <p class="pass">Giá CTV: ********</p>
                        <div class="form btn-sign-up relative">
                            <a href="#">Đăng ký CTV để xem giá</a>
                        </div>
                    </div>
                    <div class="review">
                        <img src="<?php echo asset("$path/icon-review.png")?>">
                        <p>Đã bán 10</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                        <p>Hà Nội</p>
                    </div>
                </div>
            </div>
            <div class="content border-box relative sp-product-content preventdefault">
                <div class="img-product">
                    <img src="<?php echo asset("$pathTow/top_search_3.png")?>">
                </div>
                <div class="order-search">
                    <div class="relative">
                        <img src="<?php echo asset("$pathTow/Vector_2.png")?>">
                        <p class="number-order">4</p>
                    </div>
                </div>
                <div style="background: #ffffff; padding:12px 0; margin-top: 1px;border-bottom-left-radius: 4px;border-bottom-right-radius: 4px;">
                    <div class="title-product-n relative">
                        <span>Quần jean ống rộng Kèm Hình Thật...</span>
                    </div>
                    <div class="product-price">
                        <p class="price">270.000đ - 370.000đ</p>
                        <p class="pass">Giá CTV: ********</p>
                        <div class="form btn-sign-up relative">
                            <a href="#">Đăng ký CTV để xem giá</a>
                        </div>
                    </div>
                    <div class="review">
                        <img src="<?php echo asset("$path/icon-review.png")?>">
                        <p>Đã bán 10</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                        <p>Hà Nội</p>
                    </div>
                </div>
            </div>
            <div class="content border-box relative sp-product-content preventdefault">
                <div class="img-product">
                    <img src="<?php echo asset("$pathTow/top_search.png")?>">
                </div>
                <div class="order-search">
                    <div class="relative">
                        <img src="<?php echo asset("$pathTow/Vector_2.png")?>">
                        <p class="number-order">5</p>
                    </div>
                </div>
                <div style="background: #ffffff; padding:12px 0; margin-top: 1px;border-bottom-left-radius: 4px;border-bottom-right-radius: 4px;">
                    <div class="title-product-n relative">
                        <span>Quần jean ống rộng Kèm Hình Thật...</span>
                    </div>
                    <div class="product-price">
                        <p class="price">270.000đ - 370.000đ</p>
                        <p class="pass">Giá CTV: ********</p>
                        <div class="form btn-sign-up relative">
                            <a href="#">Đăng ký CTV để xem giá</a>
                        </div>
                    </div>
                    <div class="review">
                        <img src="<?php echo asset("$path/icon-review.png")?>">
                        <p>Đã bán 10</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                        <p>Hà Nội</p>
                    </div>
                </div>
            </div>
            <div class="content border-box relative sp-product-content preventdefault">
                <div class="img-product">
                    <img src="<?php echo asset("$pathTow/top_search_4.png")?>">
                </div>
                <div class="order-search">
                    <div class="relative">
                        <img src="<?php echo asset("$pathTow/Vector_2.png")?>">
                        <p class="number-order">6</p>
                    </div>
                </div>
                <div style="background: #ffffff; padding:12px 0; margin-top: 1px;border-bottom-left-radius: 4px;border-bottom-right-radius: 4px;">
                    <div class="title-product-n relative">
                        <span>Áo phao nữ, mũ có lông Kèm Hình Thật...</span>
                    </div>
                    <div class="product-price">
                        <p class="price">270.000đ - 370.000đ</p>
                        <p class="pass">Giá CTV: ********</p>
                        <div class="form btn-sign-up relative">
                            <a href="#">Đăng ký CTV để xem giá</a>
                        </div>
                    </div>
                    <div class="review">
                        <img src="<?php echo asset("$path/icon-review.png")?>">
                        <p>Đã bán 10</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                        <p>Hà Nội</p>
                    </div>
                </div>
            </div>
            <div class="content border-box relative sp-product-content preventdefault">
                <div class="img-product">
                    <img src="<?php echo asset("$pathTow/top_search_2.png")?>">
                </div>
                <div class="order-search">
                    <div class="relative">
                        <img src="<?php echo asset("$pathTow/Vector_2.png")?>">
                        <p class="number-order">7</p>
                    </div>
                </div>
                <div style="background: #ffffff; padding:12px 0; margin-top: 1px;border-bottom-left-radius: 4px;border-bottom-right-radius: 4px;">
                    <div class="title-product-n relative">
                        <span>Quần jean ống rộng Kèm Hình Thật...</span>
                    </div>
                    <div class="product-price">
                        <p class="price">270.000đ - 370.000đ</p>
                        <p class="pass">Giá CTV: ********</p>
                        <div class="form btn-sign-up relative">
                            <a href="#">Đăng ký CTV để xem giá</a>
                        </div>
                    </div>
                    <div class="review">
                        <img src="<?php echo asset("$path/icon-review.png")?>">
                        <p>Đã bán 10</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                        <p>Hà Nội</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

