<div class="infor-shop">
    <div class="d-flex justify-content-between">
        <h2 class="title-infor">THÔNG TIN SHOP</h2>
        <h2 class="title-infor">Ngày tham gia: {{date('d-m-Y',strtotime($dataInfoShop->date))}}</h2>
    </div>
    <div class="container-infor-shop flex">
        <div class="container-image-shop relative">
            <div class="slick-image flex">
                <img class="image" style="width: 595px; height: 285px; object-fit: contain;"
                     src="{{$dataInfoShop->image}}" alt="">
                <?php if(isset($dataShopImage)) :?>
                <?php foreach($dataShopImage as $value): ?>
                <img class="image" style="width: 595px; height: 285px; object-fit: contain;" src="{{$value->image}}">
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="content-infor-shop">
            <h3 class="name-shop"><?= isset($dataInfoShop) ? $dataInfoShop->name : '' ?> </h3>
            <ul class="text-shop">
                <li><?= isset($dataInfoShop) ? $dataInfoShop->desc : '' ?> </li>
            </ul>
        </div>
    </div>
</div>
