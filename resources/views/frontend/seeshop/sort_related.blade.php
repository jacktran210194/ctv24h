<?php $path_1 = "assets/frontend/Icon/products"; ?>
    @if(isset($dataRelated))
        <?php foreach($dataRelated as $value): ?>
        @php
            $product_value = \App\Models\AttributeValueModel::where('product_id', $value->id)->get();
            $price_ctv_min = \App\Models\AttributeValueModel::where('product_id', $value->id)->min('price_ctv');
            $price_ctv_max = \App\Models\AttributeValueModel::where('product_id', $value->id)->max('price_ctv');
        @endphp
        <?php
        $min = 0;
        $max = 0;
        foreach ($product_value as $item){
            $ctv = $item->price - $item->price_ctv;
            if ($min == 0){
                $min = $ctv;
            }elseif ($min > $ctv){
                $min = $ctv;
            }
            if ($max == 0){
                $max = $ctv;
            }elseif ($max < $ctv){
                $max = $ctv;
            }
        }
        ?>
        <a class="content border-box" href="{{url('details_product/'.$value->id)}}">
            <div class="img-product">
                <img style="height: 180px" src="{{$value->image}}?>">
            </div>
            <div class="item-content-product">
                <div class="title-product-n relative">
                    <span>{{$value->name}}</span>
                </div>
                <div class="container-price justify-content-between d-flex flex-column">
                    <div class="product-price">
                        <p class="price">{{number_format($value->price_discount)}}đ
                            - {{number_format($value->price)}}đ</p>
                        <div class="d-flex justify-content-between">
                            <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ - {{number_format($price_ctv_max)}}đ</p>
                        </div>
                        <div class="d-flex justify-content-between" style="margin-top: 10px">
                            <p>Lời: <span class="text-red">{{number_format($min)}}đ - {{number_format($max)}}đ</span>
                            </p>
                        </div>
                    </div>
                    <div class="review">
                        @if($value->avg_rating == 0)
                            <img src="<?php echo asset("$path_1/0_sao.png")?>">
                        @else
                            @if($value->avg_rating == 1)
                                <img src="<?php echo asset("$path_1/1_sao.png")?>">
                            @elseif($value->avg_rating == 2)
                                <img src="<?php echo asset("$path_1/2_sao.png")?>">
                            @elseif($value->avg_rating == 3)
                                <img src="<?php echo asset("$path_1/3_sao.png")?>">
                            @elseif($value->avg_rating == 4)
                                <img src="<?php echo asset("$path_1/4_sao.png")?>">
                            @elseif($value->avg_rating == 5)
                                <img src="<?php echo asset("$path_1/5_sao.png")?>">
                            @elseif($value->avg_rating > 1 && $value->avg_rating < 2)
                                <img src="<?php echo asset("$path_1/1_5_sao.png")?>">
                            @elseif($value->avg_rating > 2 && $value->avg_rating < 3)
                                <img src="<?php echo asset("$path_1/2_5_sao.png")?>">
                            @elseif($value->avg_rating > 3 && $value->avg_rating < 4)
                                <img src="<?php echo asset("$path_1/3_5_sao.png")?>">
                            @elseif($value->avg_rating > 4 && $value->avg_rating < 5)
                                <img src="<?php echo asset("$path_1/4_5_sao.png")?>">
                            @endif
                        @endif
                        <p>Đã bán {{$value->is_sell}}</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;" src="Icon/home/Danh_muc/icon-like.png">
                        <p>{{$value->place}}</p>
                    </div>
                </div>
            </div>
        </a>
        <?php endforeach; ?>
    @endif

<div class="paginate-product d-flex align-items-center">
    <div class="content-paginate">
        <span class="in-paginate">{{$dataRelated->currentPage()}}</span>/<span class="all-paginate">{{$dataRelated->lastPage()}}</span>
    </div>
    <button class="btn-sort next-paginate relative">
        <a class="d-flex w-100 h-100" href="{{$dataRelated->previousPageUrl()}}">
            <object data="icon/search/polygon_left.svg"></object>
        </a>
    </button>
    <button class="btn-sort prev-paginate relative">
        <a class="d-flex w-100 h-100" href="{{$dataRelated->nextPageUrl()}}">
            <object data="icon/search/polygon_right.svg"></object>
        </a>
    </button>
</div>
