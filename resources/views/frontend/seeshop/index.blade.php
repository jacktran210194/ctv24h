<?php $page = 'all_category'; ?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/seeshop/style.css">
<link rel="stylesheet" type="text/css" href="css/search/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->
    <div class="body-seeshop">
        <input type="hidden" class="id_shop" value="{{$dataInfoShop->id}}">
        @include('frontend.seeshop.loveShop')
        @include('frontend.seeshop.inforShop')
        @include('frontend.seeshop.suggestions')
        @include('frontend.seeshop.topSearch')
        @include('frontend.seeshop.fillterProductOfShop')
    </div>
@include('frontend.base.footer')
@include('frontend.base.modal')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
<script src="js/seeshop/index.js"></script>
<script src="js/seeshop/filter.js"></script>
</body>


<script>
    $(document).ready(function () {
        $(document).on('click', '.btn_load_more', function () {
            $('.load_more_city').show();
            $(this).hide();
            $('.btn_hide_more').show();
        });

        $(document).on('click', '.btn_hide_more', function () {
            $('.load_more_city').hide();
            $(this).hide();
            $('.btn_load_more').show();
        });
    });
</script>
<!-- END: Body-->
</html>

