<?php
$path = "assets/frontend/Icon/products";
$dataLogin = Session::get('data_customer');
use Carbon\Carbon;?>
<div class="container-shop-love">
    <div id="shop-love" class="flex">
        <div class="box-1 flex div-box">
            <div class="image-button">
                <div class="iamge-shop"><img @if($dataInfoShop->image) src="{{$dataInfoShop->image}}"
                                             @else src="<?php echo asset("$path/shop_love.png")?>" @endif></div>
                <p class="shop">Shop yêu thích</p>
            </div>
            <div class="infor-shop">
                <h2 class="tite-shop"><?= isset($dataInfoShop) ? $dataInfoShop->name : '' ?> </h2>
                <div class="link-button flex">
                    <p class="number-follow follow_shop">
                        <span class="number">{{$count_follow}} </span>
                        Người Theo Dõi
                    </p>
                    <p class="number-like like_shop">
                        <span class="number">{{$count_love}} </span>
                        Yêu thích
                    </p>
                </div>
                <div class="link-button">
                    <?php
                        if(isset($dataLogin)){
                            $dataFollow = \App\Models\FollowShopModel::where('customer_id', $dataLogin->id)->where('shop_id', $dataInfoShop->id)->first();
                            $dataLike = \App\Models\LovelyShopModel::where('customer_id', $dataLogin->id)->where('shop_id', $dataInfoShop->id)->first();
                        }
                    ?>
                    <?php if(isset($dataFollow)):?>
                    <div class="follow-shop">
                        <button style="width: 120px!important;" class="button-unfollow btn-shop-followed button"
                                data-id="{{$dataInfoShop->id}}"
                                data-url="{{url('seeshop/unfollow/'.$dataInfoShop->id)}}"
                        >
                            <object data="Icon/products/gold_shop.svg" width="12px"></object>
                            Bỏ Theo Dõi
                        </button>
                    </div>

                    <?php else :?>
                        <div class="follow-shop">
                            <button style="width: 120px!important;" class="button-follow btn-shop button"
                                    data-id="{{$dataInfoShop->id}}"
                                    data-url="{{url('seeshop/follow')}}"
                            >
                                <object data="Icon/products/gold_shop.svg" width="12px"></object>
                                Theo Dõi
                            </button>
                        </div>
                    <?php endif; ?>

                    <button class="button" style="width: 120px!important;">
                        <a style="color: white!important;"
                           href="<?= URL::to('chat/messenger/shop/' . $dataInfoShop->id) ?>">
                            <object data="Icon/products/icon_chat.svg" width="12px"></object>
                            Chat Ngay
                        </a>
                    </button>

                    <?php if(isset($dataLike)): ?>
                        <div class="like-shop">
                            <button style="width: 120px!important;"
                                class="button btn-shop-followed button-dislike"
                                data-id="{{$dataInfoShop->id}}"
                                data-url="{{url('seeshop/dislike/'.$dataInfoShop->id)}}"
                            >
                                <object data="Icon/products/gold_like.svg" width="12px"></object>
                                Bỏ Thích
                            </button>
                        </div>
                    <?php else: ?>
                        <div class="like-shop">
                            <button style="width: 120px!important;"
                                class="button btn-shop button-like"
                                data-id="{{$dataInfoShop->id}}"
                                data-url="{{url('seeshop/like')}}"
                            >
                                <object data="Icon/products/gold_like.svg" width="12px"></object>
                                Yêu Thích
                            </button>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
        <div class="box-2 div-box">
            <p class="price-buy">
                <object data="Icon/products/t-shirt.svg"
                        width="20px"></object> <?= isset($dataCountProduct) ? $dataCountProduct : '0' ?>
                <span> Sản Phẩm </span>
            </p>
        </div>
        <div class="box-3 div-box">
            <p class="price-buy">
                <object data="Icon/products/favorite_2.svg" width="20px"></object>
                {{$avg_review}}
                <span>  Đánh Giá </span>
            </p>
        </div>
        <div class="box-4 div-box">
            <p class="price-buy">
                <object data="Icon/products/chat_bubbles_with_ellipsis.svg" width="20px"></object>
                {{$dataInfoShop->percent_feedback}}%
                <span> Tỉ Lệ Phản Hồi </span>
            </p>
        </div>
        <div class="box-5 div-box">
            <p class="price-buy">
                <object data="Icon/products/clock.svg" width="20px"></object>
                {{$dataInfoShop->time_feedback}}
                <span> Thời Gian Phản Hồi </span>
            </p>
        </div>
    </div>
</div>
<div class="container-fillter">
    <div class="content relative">
        <ul class="nav-fillter flex">
            <a href="{{url('seeshop/'.$dataInfoShop->id)}}">
                <li class="@if(isset($page) && $page == 'all_category') all-product @endif sub-nav-fillter">Gợi Ý Của Tôi </li>
            </a>
            @if(isset($dataCategoryShop))
                @foreach($dataCategoryShop as $value)
                    <a href="{{url('seeshop/'.$dataInfoShop->id.'/category/'.$value->cate_id)}}">
                        <li class="@if(isset($page) && $page == 'category_shop') all-product @endif sub-nav-fillter">{{$value->name}}</li>
                    </a>
                @endforeach
            @endif
        </ul>
    </div>
</div>
