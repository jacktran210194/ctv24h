<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/my_history/style.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="container">
        <div class="main-history mt-5 mb-5 pb-5">
            <h3 class="p-3 pt-5 pb-5">Lịch Sử Của Tôi</h3>
            <div class="d-flex header">
                <div class="pt-3 pb-3 pl-5 pr-5 active">
                    <h6>Tất cả lịch sử</h6>
                </div>
                <div class="pt-3 pb-3 pl-5 pr-5 inactive">
                    <a href="<?= URL::to('my_history/by_product') ?>"><h6>Mua hàng</h6></a>
                </div>
                <div class="pt-3 pb-3 pl-5 pr-5 inactive">
                    <a href="<?= URL::to('love_ly?type=product_lovely') ?>"><h6>Sản phẩm yêu thích</h6></a>
                </div>
                <div class="pt-3 pb-3 pl-5 pr-5 inactive">
                    <h6>Sản phẩm quan tâm</h6>
                </div>
                <div class="pt-3 pb-3 pl-5 pr-5 inactive">
                    <a href="<?= URL::to('love_ly?type=shop_lovely') ?>"><h6>NCC đã thích</h6></a>
                </div>
            </div>
            <div class="list-product pl-3 pt-5">
                <div class="d-flex ml-3 mb-5">
                    <div class="col-lg-2">
                        <img src="Icon/cart/image_product.png" class="image-product">
                    </div>
                    <div class=" d-flex col-lg-10 p-0 pr-3">
                        <div class="col-lg-7 p-0 flex-column justify-content-around d-flex">
                            <div class="name-product">Áo khoác nữ lót lông hottren 2020, chuẩn hàng quảng châu loại 1 sẵn DN</div>
                            <div class="text-type">Phân loại: Xanh - M</div>
                            <div class="text-type">x1</div>
                        </div>
                        <div class="col-lg-5 p-0 d-flex flex-column align-items-end justify-content-between">
                            <a class="btn btn-default">Xem Chi Tiết Đơn hàng</a>
                            <div class="d-flex">
                                <h4>Tổng số tiền: </h4>
                                <h4 class="pl-5 text-red">710.000đ</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex ml-3 mb-5">
                    <div class="col-lg-2">
                        <img src="Icon/cart/image_product.png" class="image-product">
                    </div>
                    <div class=" d-flex col-lg-10 p-0 pr-3">
                        <div class="col-lg-7 p-0">
                            <div class="name-product">Áo khoác nữ lót lông hottren 2020, chuẩn hàng quảng châu loại 1 sẵn DN</div>
                            <div class="d-flex align-items-center pt-3">
                                <object data="Icon/cart/shop_name.svg" width="20" height="20"></object>
                                <b class="ml-3">wear.vn</b>
                                <img src="Icon/lovely/star.png">
                            </div>
                        </div>
                        <div class="col-lg-5 p-0 d-flex flex-column align-items-end justify-content-between">
                            <div class="d-flex align-items-end">
                                <object data="Icon/history/like.svg" width="20" height="20"></object>
                                <span class="text-like pl-3">Đã Thích(13.1k)</span>
                            </div>
                            <a class="btn btn-default">Xem Chi Tiết sản phẩm</a>
                        </div>
                    </div>
                </div>
                <div class="d-flex ml-3 mb-5">
                    <div class="col-lg-2">
                        <img src="Icon/cart/image_product.png" class="image-product">
                    </div>
                    <div class=" d-flex col-lg-10 p-0 pr-3">
                        <div class="col-lg-7 p-0 flex-column justify-content-around d-flex">
                            <div class="name-product">Áo khoác nữ lót lông hottren 2020, chuẩn hàng quảng châu loại 1 sẵn DN</div>
                            <div class="text-type">Phân loại: Xanh - M</div>
                            <div class="text-type">x1</div>
                        </div>
                        <div class="col-lg-5 p-0 d-flex flex-column align-items-end justify-content-between">
                            <a class="btn btn-default">Xem Chi Tiết Đơn hàng</a>
                            <div class="d-flex">
                                <h4>Tổng số tiền: </h4>
                                <h4 class="pl-5 text-red">710.000đ</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
