<?php

use Illuminate\Support\Facades\URL;

$data_login = Session::get('data_customer');

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/my_history/style.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div id="mainByProduct" class="mb-3">
        <div class="container">
            <ul class="menu p-0">
                <li class=""><a href="{{url('my_history/by_product?status=all')}}">Tất
                        cả</a></li>
                <li class=""><a
                        href="{{url('my_history/by_product/wait_confirm')}}">Chờ xác nhận</a></li>
                <li class=""><a
                        href="{{url('my_history/by_product/wait_shipping')}}">Chờ lấy hàng</a></li>
                <li class=""><a
                        href="{{url('my_history/by_product/shipping')}}">Đang giao</a></li>
                <li class=""><a
                        href="{{url('my_history/by_product/shipped')}}">Đã giao</a></li>
                <li class="active"><a
                        href="{{url('my_history/by_product/cancel_order')}}">Đã huỷ</a></li>
            </ul>
        </div>
    </div>
    <div id="main2" class="mb-5">
        <div class="container">
            {{--            @if($status=='all')--}}
            @if(count($data_order))
                @foreach($data_order as $value)
                    <br>
                    <br>
                    <div class="d-flex">
                        <h3 class="mr-3">Đơn hàng: {{$value->order_code}}</h3>
                        <h3>@if($value->status == 0) Chờ xác nhận @elseif($value->status == 1) Chờ lấy
                            hàng @elseif($value->status == 2) Đang giao hàng @elseif($value->status == 3) Đã giao
                            hàng @elseif($value->status == 4) Đã huỷ @else Hoàn tiền @endif</h3>
                    </div>
                    <div class="my-list mt-3 p-3 w-100">
                        <div class="header-list w-100 p-3 mb-5">
                            <div class="float-left bookmark-wrapper d-flex align-items-center">
                                <object data="Icon/cart/shop_name.svg" width="20" height="20"></object>
                                <b class="ml-3">{{$value->shop_name}}</b>
                                <object data="Icon/cart/love.svg" class="ml-3" width="70"
                                        height="auto"></object>
                                <a href="{{url('seeshop/'.$value->shop_id)}}" class="btn btn-default ml-3">
                                    <span class="text-bold-600">Xem shop</span>
                                </a>
                                <a href="{{url('chat/messenger/shop/'.$value->shop_id)}}"
                                   class="btn btn-default ml-3 d-flex">
                                    <object data="Icon/cart/chat.svg" width="20" height="20"></object>
                                    <span class="text-bold-600 ml-2">Chat</span>
                                </a>
                            </div>
                        </div>

                        @foreach($data_order_detail as $v)
                            @if($value->id==$v->order_id)
                                <div class="d-flex mb-3 w-100">
                                    <div class="col-lg-2">
                                        <img width="115px" height="115px" src="{{$v->product_image}}"
                                             class="image-product">
                                    </div>
                                    <div class=" d-flex col-lg-10 p-0 pr-3">
                                        <div class="col-lg-7 p-0 flex-column justify-content-around d-flex">
                                            <div class="name-product">{{$v->product_name}}</div>
                                            <div class="text-type">{{$v->value_1}}
                                                - {{$v->value_2}}</div>
                                            <div class="text-type">x {{$v->quantity}}</div>
                                        </div>
                                        <div
                                            class="col-lg-5 p-0 d-flex flex-column align-items-end justify-content-center">
                                            <span class="price-old">{{number_format($v->product_price)}} đ</span>
                                            <span
                                                class="text-red pt-3 text-bold-800 price-new font-size-16">{{number_format($v->total_price)}} đ</span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        <div class="info_order d-flex w-100 justify-content-end">
                            <h3>Tổng thanh toán: <span class="text-red">{{number_format($value->total_price)}} đ</span>
                            </h3>
                        </div>
                        <div class="border-bottom"></div>
                        <div class="d-flex w-100 justify-content-end container-btn mt-3">
                            @if($value->status == 0)
                                <button class="btn btn-main preventdefault" disabled>Chờ Xác Nhận
                                </button>
                                <a class="btn btn-default ml-3"
                                   href="{{url('search_order?order_code='.$value->order_code)}}">Xem Chi
                                    Tiết Đơn hàng</a>
                                <a class="btn btn-default ml-3 btn_cancel_order"
                                   data-order="{{$value->id}}" data-url="{{url('by_product/cancel')}}"
                                >Hủy
                                    Đơn hàng</a>
                            @elseif($value->status == 1)
                                <button class="btn btn-main preventdefault" disabled>Chờ Lấy Hàng
                                </button>
                                <a class="btn btn-default ml-3" data-order="{{$v->id}}"
                                   href="{{url('search_order?order_code='.$value->order_code)}}">Xem Chi
                                    Tiết Đơn hàng</a>
                                <a data-order="{{$value->id}}" data-url="{{url('by_product/cancel')}}"
                                   onclick="return confirm('Bạn có muốn huỷ đơn hàng này không?')"
                                   class="btn btn-default btn_cancel_order ml-3">Hủy Đơn hàng</a>
                            @elseif($value->status == 2)
                                <button class="btn btn-main preventdefault" disabled>Đang giao hàng
                                </button>
                            @elseif($value->status==3)
                                @if($value->status_confirm == 0)
                                    <button class="btn btn-main btn_check_product"
                                            data-order-detail-id="{{ $value->id }}"
                                            data-url="{{url('my_history/by_product/check_product')}}">Đã
                                        nhận được hàng
                                    </button>
                                    <button
                                        class="btn btn-default preventdefault btn_return_product ml-3">
                                        Trả hàng hoàn tiền
                                    </button>
                                @else
                                    @php
                                        $check_review = \App\Models\ReviewProductModel::where('customer_id',$data_login['id'])->where('order_id',$value->id)->count() ?? 0;
                                        $check_order_detail = \App\Models\OrderDetailModel::where('order_id',$value->id)->count();
                                    @endphp
                                    @if($check_review < $check_order_detail)
                                        <button class="btn btn-review-product ml-3"
                                                data-url="{{url('my_history/by_product/list_order/'.$value->id)}}"
                                        >Đánh giá
                                        </button>
                                        <div class="show_list_order"></div>
                                    @else
                                        <button class="btn btn-main preventdefault">Mua lần nữa</button>
                                    @endif
                                @endif
                                <a class="btn btn-default ml-3"
                                   href="{{url('search_order?order_code='.$value->order_code)}}">Xem chi
                                    tiết đơn hàng</a>
                            @else
                                <button class="btn btn-main preventdefault">Mua lần nữa</button>
                                <a class="btn btn-default ml-3"
                                   href="{{url('search_order?order_code='.$value->order_code)}}">Xem chi
                                    tiết đơn huỷ</a>
                            @endif
                        </div>
                    </div>
                @endforeach
            @else
                <h3 class="text-center">Không có đơn hàng nào</h3>
        @endif
        <!-- END: Cart-->
            <div class="popup-edit-payment">
                <div class="container-popup-payment">
                    <div class="header-popups-payment">
                        <p class="title-header-payment ml-2">Chọn lý do huỷ đơn</p>
                        <p>Vui lòng chọn lý do hủy. Với lý do này, bạn sẽ hủy tất cả sản phẩm trong đơn hàng và
                            không
                            thể thay đổi sau đó </p>
                    </div>
                    <div class="information-payment-unit">
                        <div class="d-flex justify-content-between sub-payment-unit">
                            <div class="container-payment">
                                <div class="name-payment-unit d-flex align-items-center">
                                    <div class="selected name_payment"
                                         data-id="0"
                                    ></div>
                                    <p class="name">Muốn thay đổi địa chỉ giao hàng</p>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between sub-payment-unit">
                            <div class="container-payment">
                                <div class="name-payment-unit d-flex align-items-center">
                                    <div class="selected name_payment"
                                         data-id="1"
                                    ></div>
                                    <p class="name">Muốn nhập/thay đổi mã voucher</p>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between sub-payment-unit">
                            <div class="container-payment">
                                <div class="name-payment-unit d-flex align-items-center">
                                    <div class="selected name_payment"
                                         data-id="2"
                                    ></div>
                                    <p class="name">Muốn thay đổi sản phẩm trong đơn hàng</p>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between sub-payment-unit">
                            <div class="container-payment">
                                <div class="name-payment-unit d-flex align-items-center">
                                    <div class="selected name_payment"
                                         data-id="3"
                                    ></div>
                                    <p class="name">Thủ tục thanh toán quá rắc rối</p>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between sub-payment-unit">
                            <div class="container-payment">
                                <div class="name-payment-unit d-flex align-items-center">
                                    <div class="selected name_payment"
                                         data-id="4"
                                    ></div>
                                    <p class="name">Tìm thấy giá rẻ hơn ở chỗ khác</p>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between sub-payment-unit">
                            <div class="container-payment">
                                <div class="name-payment-unit d-flex align-items-center">
                                    <div class="selected name_payment"
                                         data-id="5"
                                    ></div>
                                    <p class="name">Đổi ý không muốn mua nữa</p>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between sub-payment-unit">
                            <div class="container-payment">
                                <div class="name-payment-unit d-flex align-items-center">
                                    <div class="selected name_payment"
                                         data-id="6"
                                    ></div>
                                    <p class="name">Lý do khác</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-lg-end align-items-center">
                        <button class="btn btn-cancel">Trở lại</button>
                        <button data-url="{{url('my_history/by_product/cancel')}}"
                                class="btn btn-confirm-payment btn-confirm ml-2"
                        >Huỷ đơn hàng
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div
        class="container-popup-review d-flex justify-content-lg-center align-items-center position-fixed position-center">
        <div class="popup-review">
            <h3 class="title">Đánh giá sản phẩm </h3>
            <div class="product-review d-flex align-items-center">
                <div
                    class="image d-flex justify-content-lg-center align-items-center"
                    style="max-width: 117px">
                    <img class="review_product_image" style="width: 100%!important;"
                         src="">
                </div>
                <div class="title-product">
                    <h4 class="review_product_name"></h4>
                    <br>
                    <span class="attr-product review_value_1"></span> - <span
                        class="attr-product review_value_2"></span>
                </div>
            </div>
            <div
                class="d-flex justify-content-lg-center align-items-center ratings">
                <button class="btn btn-star-rating" data-ratings="1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="28"
                         height="27" viewBox="0 0 28 27" fill="none">
                        <path
                            d="M27.6992 10.3578C27.5224 9.8111 27.0375 9.42279 26.4638 9.37108L18.6708 8.66348L15.5893 1.45084C15.3621 0.922242 14.8446 0.580078 14.2697 0.580078C13.6947 0.580078 13.1773 0.922242 12.95 1.45207L9.8685 8.66348L2.07433 9.37108C1.50165 9.42402 1.01796 9.8111 0.840185 10.3578C0.662408 10.9045 0.82659 11.5042 1.25981 11.8822L7.15034 17.0483L5.41336 24.6997C5.28626 25.2623 5.50462 25.8438 5.97141 26.1812C6.22232 26.3625 6.51587 26.4548 6.81189 26.4548C7.06712 26.4548 7.32029 26.386 7.54751 26.25L14.2697 22.2324L20.9894 26.25C21.4811 26.5458 22.1009 26.5188 22.5667 26.1812C23.0337 25.8428 23.2518 25.261 23.1247 24.6997L21.3878 17.0483L27.2783 11.8832C27.7115 11.5042 27.8769 10.9056 27.6992 10.3578Z"
                            fill="#ccc"/>
                    </svg>
                </button>
                <button class="btn btn-star-rating" data-ratings="2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="28"
                         height="27" viewBox="0 0 28 27" fill="none">
                        <path
                            d="M27.6992 10.3578C27.5224 9.8111 27.0375 9.42279 26.4638 9.37108L18.6708 8.66348L15.5893 1.45084C15.3621 0.922242 14.8446 0.580078 14.2697 0.580078C13.6947 0.580078 13.1773 0.922242 12.95 1.45207L9.8685 8.66348L2.07433 9.37108C1.50165 9.42402 1.01796 9.8111 0.840185 10.3578C0.662408 10.9045 0.82659 11.5042 1.25981 11.8822L7.15034 17.0483L5.41336 24.6997C5.28626 25.2623 5.50462 25.8438 5.97141 26.1812C6.22232 26.3625 6.51587 26.4548 6.81189 26.4548C7.06712 26.4548 7.32029 26.386 7.54751 26.25L14.2697 22.2324L20.9894 26.25C21.4811 26.5458 22.1009 26.5188 22.5667 26.1812C23.0337 25.8428 23.2518 25.261 23.1247 24.6997L21.3878 17.0483L27.2783 11.8832C27.7115 11.5042 27.8769 10.9056 27.6992 10.3578Z"
                            fill="#ccc"/>
                    </svg>
                </button>
                <button class="btn btn-star-rating" data-ratings="3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="28"
                         height="27" viewBox="0 0 28 27" fill="none">
                        <path
                            d="M27.6992 10.3578C27.5224 9.8111 27.0375 9.42279 26.4638 9.37108L18.6708 8.66348L15.5893 1.45084C15.3621 0.922242 14.8446 0.580078 14.2697 0.580078C13.6947 0.580078 13.1773 0.922242 12.95 1.45207L9.8685 8.66348L2.07433 9.37108C1.50165 9.42402 1.01796 9.8111 0.840185 10.3578C0.662408 10.9045 0.82659 11.5042 1.25981 11.8822L7.15034 17.0483L5.41336 24.6997C5.28626 25.2623 5.50462 25.8438 5.97141 26.1812C6.22232 26.3625 6.51587 26.4548 6.81189 26.4548C7.06712 26.4548 7.32029 26.386 7.54751 26.25L14.2697 22.2324L20.9894 26.25C21.4811 26.5458 22.1009 26.5188 22.5667 26.1812C23.0337 25.8428 23.2518 25.261 23.1247 24.6997L21.3878 17.0483L27.2783 11.8832C27.7115 11.5042 27.8769 10.9056 27.6992 10.3578Z"
                            fill="#ccc"/>
                    </svg>
                </button>
                <button class="btn btn-star-rating" data-ratings="4">
                    <svg xmlns="http://www.w3.org/2000/svg" width="28"
                         height="27" viewBox="0 0 28 27" fill="none">
                        <path
                            d="M27.6992 10.3578C27.5224 9.8111 27.0375 9.42279 26.4638 9.37108L18.6708 8.66348L15.5893 1.45084C15.3621 0.922242 14.8446 0.580078 14.2697 0.580078C13.6947 0.580078 13.1773 0.922242 12.95 1.45207L9.8685 8.66348L2.07433 9.37108C1.50165 9.42402 1.01796 9.8111 0.840185 10.3578C0.662408 10.9045 0.82659 11.5042 1.25981 11.8822L7.15034 17.0483L5.41336 24.6997C5.28626 25.2623 5.50462 25.8438 5.97141 26.1812C6.22232 26.3625 6.51587 26.4548 6.81189 26.4548C7.06712 26.4548 7.32029 26.386 7.54751 26.25L14.2697 22.2324L20.9894 26.25C21.4811 26.5458 22.1009 26.5188 22.5667 26.1812C23.0337 25.8428 23.2518 25.261 23.1247 24.6997L21.3878 17.0483L27.2783 11.8832C27.7115 11.5042 27.8769 10.9056 27.6992 10.3578Z"
                            fill="#ccc"/>
                    </svg>
                </button>
                <button class="btn btn-star-rating" data-ratings="5">
                    <svg xmlns="http://www.w3.org/2000/svg" width="28"
                         height="27" viewBox="0 0 28 27" fill="none">
                        <path
                            d="M27.6992 10.3578C27.5224 9.8111 27.0375 9.42279 26.4638 9.37108L18.6708 8.66348L15.5893 1.45084C15.3621 0.922242 14.8446 0.580078 14.2697 0.580078C13.6947 0.580078 13.1773 0.922242 12.95 1.45207L9.8685 8.66348L2.07433 9.37108C1.50165 9.42402 1.01796 9.8111 0.840185 10.3578C0.662408 10.9045 0.82659 11.5042 1.25981 11.8822L7.15034 17.0483L5.41336 24.6997C5.28626 25.2623 5.50462 25.8438 5.97141 26.1812C6.22232 26.3625 6.51587 26.4548 6.81189 26.4548C7.06712 26.4548 7.32029 26.386 7.54751 26.25L14.2697 22.2324L20.9894 26.25C21.4811 26.5458 22.1009 26.5188 22.5667 26.1812C23.0337 25.8428 23.2518 25.261 23.1247 24.6997L21.3878 17.0483L27.2783 11.8832C27.7115 11.5042 27.8769 10.9056 27.6992 10.3578Z"
                            fill="#ccc"/>
                    </svg>
                </button>
            </div>
            <div class="content-comment">
                <div
                    class="comment d-flex flex-wrap justify-content-lg-center align-items-center">
                    <button class="btn btn-comment">Chất lượng sản phẩm
                        tuyệt vời
                    </button>
                    <button class="btn btn-comment">Đóng gói sản phẩm
                        rất
                        đẹp và chắc chắn
                    </button>
                    <button class="btn btn-comment">Shop phục vụ rất tốt
                    </button>
                    <button class="btn btn-comment">Rất đáng tiền
                    </button>
                    <button class="btn btn-comment">Thời gian giao hàng
                        rất
                        nhanh
                    </button>
                </div>
            </div>
            <div
                class="note-review d-flex justify-content-lg-between align-items-center">
                <div id="textarea">
                    <label for="textarea">Hãy chia sẻ những điều bạn
                        thích
                        về sản phẩm này</label>
                    <textarea></textarea>
                </div>
                <div
                    class="form-upload d-flex justify-content-center align-content-center">
                    <input type="file" id="myFile" name="filename"
                           accept="video/*,image/*"
                           onchange="readURL(this);">
                    <div
                        class="upload-files d-flex justify-content-lg-center align-items-center">
                        <div class="btn icon-upload">
                            <img src="Icon/chat/upload.png">
                            <p>Tải ảnh lên</p>
                        </div>
                        <img id="blah" src="Icon/chat/upload.png"/>
                        <video controls autoplay muted id="video-tag">
                            <source id="video-source" src="splashVideo">
                        </video>
                    </div>
                </div>
            </div>
            <div class="content-img-video-review mb-4"></div>
            <div
                class="d-flex justify-content-lg-end align-items-center">
                <button class="btn btn-bottom btn-back">Trở lại</button>
                <button class="btn btn-bottom btn-up-review"
                        data-url="{{url("my_history/review-product")}}"
                        name="uploadclick">Đánh giá
                </button>
            </div>
        </div>
    </div>
    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</body>
<!-- END: Body-->
@include('frontend.base.script')
<script src="js/myHistory/index.js"></script>
</html>

