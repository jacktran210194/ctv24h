<div class="card position-fixed position-center list_order p-2" style="display: none; z-index: 10">
    <div class="card-body">
        <h3 class="mb-3">Chọn sản phẩm để đánh giá</h3>
        <div class="row">
            @foreach($data_order_detail as $value)
                @php
                    $check_review_product = \App\Models\ReviewProductModel::where('order_detail_id',$value->id)->first();
                @endphp
                <div class="col-lg-12 col-md-12">
                    <div class="d-flex align-items-center mb-3">
                        <input @if($check_review_product) disabled @endif class="mr-3 check_order_detail" data-product-id="{{$value->product_id}}" data-id="{{$value->id}}" data-order="{{$value->order_id}}" data-name="{{$value->product_name}}" data-image="{{$value->product_image}}" data-value_1="{{$value->value_1}}"  data-value_2={{$value->value_2}} value="1" type="radio" id="check_order_detail" name="check_order_detail">
                        <img style="border-radius: 8px;" width="80px" height="80px"
                             src="{{$value->product_image}}"
                             alt="{{$value->product_name}}">
                        <div class="info_product ml-4">
                            <p class="text-bold-700">{{$value->product_name}}</p>
                            <span class="text-sliver">{{$value->value_1}} - {{$value->value_2}}</span>
                            <br>
                            <span class="text-sliver m-0">x{{$value->quantity}}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="form-btn d-flex justify-content-end mt-3">
            <button style="width: 100px" class="btn border mr-3 btn_back_review">Trở về</button>
            <button style="width: 100px" class="btn btn-main btn_click_review" disabled>Đánh giá</button>
        </div>
    </div>
</div>
