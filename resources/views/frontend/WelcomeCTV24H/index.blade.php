<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/WelcomeCTV24H/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->
<div class="container bodypage">
    <?php
    $path = "assets/frontend/Icon/WelcomeCTV24H";
    ?>
    <div class="banner-image">
        <img src="<?php echo asset("$path/banner.png") ?>">
    </div>
    <div class="body-content">
        <div class="image-banner">
            <img src="<?php echo asset("$path/image.png") ?>">
        </div>
        <div class="content-page">
            <p class="title">Cùng phát triển 100.000 CTV bán hàng online trong vòng 12 tháng tới.</p>
            <p class="text">Sau khi hoàn tất giai đoạn 1, giai đoạn thử nghiệm, lựa chọn mô hình phù hợp và hoàn thiện công thức phát triển bền vững, Cotavi muốn mời các Nhà cung cấp, Nhà bán sỉ/bán buôn hoặc các cá nhân/tổ chức quan tâm mô hình, đồng hành phát triển. Bằng cách HỢP TÁC CHIẾN LƯỢC giai đoạn 2, giai đoạn tăng tốc mở rộng số lượng CTV lên tối thiểu 100.000 CTV trong vòng 12 tháng tới.</p>
            <p class="text">Đối tác đầu tư hoặc nhà cung cấp muốn hợp tác phân phối sản phẩm với Cotavi,
                Nhà cung cấp cần tư vấn thêm, vui lòng bấm vào biểu tượng chat messenger góc phải màn hình để được hỗ trợ</p>
        </div>
        <div class="level-container">
{{--            <img class="bg_container" src="<?php echo asset("$path/bg_image.png") ?>">--}}
            <div class="content">
            <h2 class="title-container">Các cấp độ của NCC Và CTV </h2>
            <div class="content-level">
                <div class="level container-box">
                    <div class="title-level">
                        <img src="<?php echo asset("$path/bg_level.png") ?>">
                        <p class="title-box-content">Cấp Độ</p>
                    </div>
                    <div class="ct-level">
                        <ul>
                            <li>Mức chiết khấu trực tiếp trên sản phẩm </li>
                            <li>Logo biểu tượng </li>
                            <li>nhận quà sinh nhật, lễ tết, sự kiện</li>
                            <li>nhận biểu tượng vIP</li>
                            <li>Đổi quà trên trang vIP</li>
                        </ul>
                    </div>
                </div>
                <div class="member container-box border-top">
                    <div class="title-member">
                        <img src="<?php echo asset("$path/bg_customer.png") ?>">
                        <p class="title-box-content">Thành viên </p>
                    </div>
                    <div class="ct-level ct-member">
                        <ul>
                            <li>0%</li>
                            <li><img src="<?php echo asset("$path/icon_true.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/icon_false.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/icon_false.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/icon_false.png") ?>"></li>
                        </ul>
                    </div>
                </div>
                <div class="sliver container-box border-top border-bottom">
                    <div class="title-sliver">
                        <img src="<?php echo asset("$path/bg_customer.png") ?>">
                        <p class="title-box-content">sliver</p>
                    </div>
                    <div class="ct-level ct-sliver">
                        <ul>
                            <li>1.5%</li>
                            <li><img src="<?php echo asset("$path/icon_true.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/icon_true.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/icon_true.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/icon_false.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/silver_diamond.png") ?>"></li>
                        </ul>
                    </div>
                </div>
                <div class="gold container-box border-top border-bottom">
                    <div class="title-gold">
                        <img src="<?php echo asset("$path/bg_customer.png") ?>">
                        <p class="title-box-content">Gold</p>
                    </div>
                    <div class="ct-level ct-gold">
                        <ul>
                            <li>3%</li>
                            <li><img src="<?php echo asset("$path/icon_true.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/icon_true.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/icon_true.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/icon_false.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/yellow_diamond.png") ?>"></li>
                        </ul>
                    </div>
                </div>
                <div class="platinum container-box border-top border-bottom">
                    <div class="title-platinum">
                        <img src="<?php echo asset("$path/bg_customer.png") ?>">
                        <p class="title-box-content">Platinum</p>
                    </div>
                    <div class="ct-level ct-platinum">
                        <ul>
                            <li>4%</li>
                            <li><img src="<?php echo asset("$path/icon_true.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/icon_true.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/icon_true.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/icon_false.png") ?>"></li>
                            <li><img src="<?php echo asset("$path/red_diamond.png") ?>"></li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="btn-register">
            <a class="btn btn-ctv" href="{{url('register/ctv')}}">Đăng Ký CTV</a>
            <a class="btn btn-ncc" href="{{url('register/ncc')}}">Đăng Ký NCC</a>
        </div>
    </div>
</div>
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>

