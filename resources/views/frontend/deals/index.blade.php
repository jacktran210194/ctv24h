<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/deals/style.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="bg-oregen p-0">
        <img src="Icon/deals/banner.png" width="100%">
        <div class="d-flex justify-content-center m-5">
            <object data="Icon/deals/lichct.svg"></object>
        </div>
        <div class="container pb-5">
            <div class="d-flex mb-5 position-relative">
                <div class="position-absolute triangle"></div>
                <div class="col-lg-3 shadow-offset">
                    <img src="Icon/deals/content-1.png" width="100%">
                </div>
                <div class="col-lg-9 calendar d-flex flex-column justify-content-around p-3">
                    <div class="d-flex justify-content-around">
                        <div class="menu-inactive align-items-center w-7-percent d-flex justify-content-center">Thứ 2
                        </div>
                        <div class="menu-inactive align-items-center w-7-percent d-flex justify-content-center">Thứ 3
                        </div>
                        <div class="menu-inactive align-items-center w-7-percent d-flex justify-content-center">Thứ 4
                        </div>
                        <div class="menu-active align-items-center w-7-percent d-flex justify-content-center">Thứ 5
                        </div>
                        <div class="menu-inactive align-items-center w-7-percent d-flex justify-content-center">Thứ 6
                        </div>
                        <div class="menu-inactive align-items-center w-7-percent d-flex justify-content-center">Thứ 7
                        </div>
                        <div class="menu-inactive align-items-center w-7-percent d-flex justify-content-center">Chủ
                            nhật
                        </div>
                    </div>
                    <?php if (count($date)): ?>
                    <?php $i = 0; ?>
                    <?php foreach ($date as $value): ?>
                    <div class="d-flex justify-content-around">
                        <?php foreach ($value as $item): ?>
                        <div
                            class="d-flex flex-column justify-content-center w-7-percent <?= $item['active'] ? 'active' : 'inactive' ?> align-items-center">
                               <span class="<?= $item['active_day'] ? 'active_day p-3' : '' ?>">
                                   <?= $item['day'] ?>
                               </span>
                            <?php if ($item['active_day']): ?>
                            <span class="title pt-2">Tết linh đình</span>
                            <?php endif; ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="d-flex mb-5 justify-content-center content-title"></div>
            <div class="row">
                <?php for ($i = 0; $i < 24; $i++): ?>
                <div class="col-lg-2 mb-3 p-1">
                    <img src="Icon/deals/product.png" class="w-100">
                    <div class="p-3 bg-white d-flex flex-column">
                        <div class="text-bold-600 number-line-2">
                            Quần jean ống rộng Kèm Hình Thật...
                        </div>
                        <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                        <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                        <div class="d-flex mt-2">
                            <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                Giá CTV: ****
                            </div>
                            <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                Lời: 50.000
                            </div>
                        </div>
                        <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                        <div class="d-flex mt-2">
                            <div class="d-flex col-lg-6 p-0">
                                <img src="Icon/lovely/star.png" width="100%">
                            </div>
                            <div class="d-flex justify-content-end col-lg-6 p-0">
                                Đã bán 10
                            </div>
                        </div>
                        <div class="d-flex mt-2 align-items-center">
                            <div class="d-flex col-lg-6 p-0 align-items-center">
                                <object data="Icon/deals/like.svg"></object>
                            </div>
                            <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                Hà Nội
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++ ?>
                <?php endfor; ?>
            </div>
        </div>
    </div>
    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
