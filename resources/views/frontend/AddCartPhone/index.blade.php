<?php
use Illuminate\Support\Facades\URL;
$data_customer = Session::get('data_customer');
?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/AddCartPhone/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->
    <?php
    $path = "assets/frontend/Icon/AddCartPhone";
    ?>
    <div class="container body-page">
        <div class="banner">
            <img src="<?php echo asset("$path/banner.png")?>">
        </div>
        <div class="nav-page">
            <p class="title-nav">NẠP THẺ ĐIỆN THOẠI</p>
            <ul class="nav-catagory">
                <li class=" <?= $type === 'upCard' ? 'active' : '' ?>">
                    <a href="<?= URL::to('recharge-card-phone?type=upCard') ?>">Nạp thẻ điện thoại trực tiếp</a>
                </li>
                <li class="<?= $type === 'buyCart' ? 'active' : '' ?>">
                    <a href="<?= URL::to('recharge-card-phone?type=buyCart') ?>"> Mua Thẻ Điện Thoại</a>
                </li>
                <li class="<?= $type === 'buyCartGame' ? 'active' : '' ?>">
                    <a href="<?= URL::to('recharge-card-phone?type=buyCartGame') ?>"> Mua Thẻ Game</a>
                </li>
            </ul>
        </div>
    <?php if($type === 'upCard')  :?>
    <!--        Nạp tiền điện thoại trực tiếp-->
        <div class="content-cart-phone">
            <div class="carts-phone">
                @foreach($category_card_phone as $v1)
                    <button data-id="{{$v1->id}}" class="type-cart-phone btn-buy-card">
                        <img style="object-fit: contain" width="227px" height="80px" src="{{$v1->image}}">
                    </button>
                @endforeach
            </div>
            <div class="price-cart-phone">
                <div class="form-sellect">
                    <div class="telephone box-content">
                        <label>Nạp vào số điện thoại</label>
                        <input type="text">
                    </div>
                    <div class="email-confirm box-content">
                        <label>Email nhận mã thẻ</label>
                        <input type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
                    </div>
                </div>
                <div class="denominations">
                    <p class="title-denominations">Chọn mệnh giá</p>
                    <div class="price" id="price_card_phone">
                        {{--                        <button class="price-denominations">--}}
                        {{--                            <p class="ct-denominations">20.000 đ</p>--}}
                        {{--                            <p class="ct-price">Giá: 20.000 đ</p>--}}
                        {{--                        </button>--}}
                        {{--                        <button class="price-denominations">--}}
                        {{--                            <p class="ct-denominations">50.000 đ</p>--}}
                        {{--                            <p class="ct-price">Giá: 50.000 đ</p>--}}
                        {{--                        </button>--}}
                        {{--                        <button class="price-denominations">--}}
                        {{--                            <p class="ct-denominations">100.000 đ</p>--}}
                        {{--                            <p class="ct-price">Giá: 100.000 đ</p>--}}
                        {{--                        </button>--}}
                        {{--                        <button class="price-denominations">--}}
                        {{--                            <p class="ct-denominations">200.000 đ</p>--}}
                        {{--                            <p class="ct-price">Giá: 200.000 đ</p>--}}
                        {{--                        </button>--}}
                        {{--                        <button class="price-denominations">--}}
                        {{--                            <p class="ct-denominations">300.000 đ</p>--}}
                        {{--                            <p class="ct-price">Giá: 300.000 đ</p>--}}
                        {{--                        </button>--}}
                        {{--                        <button class="price-denominations">--}}
                        {{--                            <p class="ct-denominations">500.000 đ</p>--}}
                        {{--                            <p class="ct-price">Giá: 500.000 đ</p>--}}
                        {{--                        </button>--}}
                    </div>
                </div>
                <div class="form-checkout">
                    <div class="flex-box">
                        <p class="supplier">Nhà mạng cung cấp:</p>
                        <div class="logo-supplier">
                            <img src="">
                        </div>
                    </div>
                    <div class="flex-box">
                        <p class="supplier">Thành tiền:</p>
                        <p class="price-supplier">
                            0 đ
                        </p>
                    </div>
                    <div class="button">
                        <button class="btn-checked preventdefault">Chọn Mua</button>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif($type === 'buyCart') :?>
    <!--        Mua thẻ điện thoại-->
        <div class="content-cart-phone buyCart">
            <div class="carts-phone">
                @foreach($category_card_phone as $v1)
                    <button class="type-cart-phone btn-buy-card" id="type-cart-phone" data-id="{{$v1['id']}}"
                            data-img="{{$v1->image}}">
                        <img class="img-category" style="object-fit: contain" width="227px" height="80px"
                             src="{{$v1->image}}">
                    </button>
                @endforeach
            </div>
            <div class="price-cart-phone">
                <div class="denominations">
                    <p class="title-denominations">Chọn mệnh giá</p>
                    <div class="price price_card_phone" id="price_card_phone">
                        {{--                        MỆNH GIÁ--}}
                    </div>
                </div>
                <div class="quantity-cart">
                    <p class="text-quantity">Số lượng:</p>
                    <div class="quantity">
                        <button class="btn-button btn-plus btn-plus-phone"><img
                                src="<?php echo asset("$path/icon_plus.png")?>">
                        </button>
                        <p class="number-quantity number_card_phone"><input style="width: auto;" min="1" max="99" step="1" value="1" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" type="number" name="number_phone" id="number_phone"></p>
                        <button class="btn-button btn-devie btn-devie-phone"><img
                                src="<?php echo asset("$path/icon_devie.png")?>">
                        </button>
                    </div>
                </div>
                <div class="form-sellect">
                    <div class="telephone box-content">
                        <label>Nạp vào số điện thoại</label>
                        <input type="text" id="phone_phone">
                    </div>
                    <div class="email-confirm box-content">
                        <label>Email nhận mã thẻ</label>
                        <input type="email" id="email_phone" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
                    </div>
                </div>
                <div class="form-checkout">
                    <div class="flex-box">
                        <p class="supplier">Nhà mạng cung cấp:</p>
                        <div class="logo-supplier">
                            <img src="">
<!--                            --><?php //echo asset("$path/viettel.png")?>
                        </div>
                    </div>
                    <div class="flex-box">
                        <p class="supplier">Thành tiền:</p>
                        <p class="price-supplier">
                            0 đ
                        </p>
                    </div>
                    <div class="button">
                        <button class="btn-checked btn-checkout-phone"
                                data-category=""
                                data-price=""
                                data-customer="{{isset($data_customer) ? $data_customer['id'] : ''}}"
                        >Chọn Mua
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <?php else :?>
    <!--        mua thẻ game-->
        <div class="content-cart-phone">
            <div class="carts-phone">
                @foreach($category_card_game as $v2)
                    <button class="type-cart-phone type-cart-game btn-buy-game" data-img="{{$v2->image}}" data-id="{{$v2['id']}}">
                        <img style="object-fit: contain" width="227px" height="80px" src="{{$v2->image}}">
                    </button>
                @endforeach
            </div>
            <div class="price-cart-phone">
                <div class="form-sellect">
                    <div class="telephone box-content">
                        <label>Nạp vào số điện thoại</label>
                        <input id="phone_game" type="text">
                    </div>
                    <div class="email-confirm box-content">
                        <label>Email nhận mã thẻ</label>
                        <input id="email_game" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
                    </div>
                </div>
                <div class="denominations">
                    <p class="title-denominations">Chọn mệnh giá</p>
                    <div class="price" id="price_card_game">
                        {{--                        Mệnh giá thẻ game --}}
                    </div>
                </div>
                <div class="quantity-cart">
                    <p class="text-quantity">Số lượng:</p>
                    <div class="quantity">
                        <button class="btn-button btn-plus btn-plus-game"><img
                                src="<?php echo asset("$path/icon_plus.png")?>">
                        </button>
                        <p class="number-quantity number_card_game"><input style="width: auto;" min="1" max="99" step="1" value="1" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" type="number" name="number_game" id="number_game"></p>
                        <button class="btn-button btn-devie btn-devie-game"><img
                                src="<?php echo asset("$path/icon_devie.png")?>">
                        </button>
                    </div>
                </div>
                <div class="form-checkout">
                    <div class="flex-box">
                        <p class="supplier">Nhà mạng cung cấp:</p>
                        <div class="logo-supplier">
                            <img src="">
                        </div>
                    </div>
                    <div class="flex-box">
                        <p class="supplier">Thành tiền:</p>
                        <p class="price-supplier">
                            0 đ
                        </p>
                    </div>
                    <div class="button">
                        <button data-url="{{url('recharge-card-phone/save_card_game')}}"
                                data-customer="{{isset($data_customer) ? $data_customer['id'] : ''}}"
                                class="btn-checked btn-checkout-game">Chọn Mua
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <!-- BEGIN: Footer-->
    <div class="popup-order-information">
        <p class="title-popup">Thông tin đơn hàng</p>
        <div class="d-flex align-items-center">
            <p class="denominations order-price">Mệnh giá: </p>
            <img class="order-category pl-5" id="order-category" style="object-fit:contain; width:227px; height:63px; ">
        </div>
        <div class="select-payment">
            <p class="title-popup">Chọn hình thức thanh toán</p>
            <div class="d-flex align-items-center payment" data-payment="0">
                <div class="check-box"></div>
                <div class="d-flex align-items-center justify-content-between" style="width: 90%">
                    <p class="text">Thanh toán bằng thẻ quốc tế Visa, Master, JCB</p>
                    <div class="image"><img src="Icon/AddCartPhone/group_payment.png"></div>
                </div>
            </div>
            <div class="d-flex align-items-center payment" data-payment="1">
                <div class="check-box"></div>
                <p class="text">Thanh toán bằng thẻ ATM nội địa/Internet Banking (Miễn phí thanh toán)</p>
            </div>
            <div class="d-flex align-items-center payment" data-payment="2">
                <div class="check-box"></div>
                <p class="text">Thanh toán bằng ví Momo</p>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-lg-center content-button">
            <button class="btn-cancel">Hủy</button>
            <button class="btn-confirm-buy-card-phone"
                    data-customer="{{isset($data_customer) ? $data_customer['id'] : ''}}"
                    data-url="{{url('recharge-card-phone/save_card_phone')}}"
            >Thanh toán
            </button>
        </div>
    </div>

    {{--    Chọn hình thức thanh toán mua thẻ game--}}
    <div class="popup-order-information-game">
        <p class="title-popup">Thông tin đơn hàng</p>
        <div class="d-flex align-items-center">
            <p class="denominations order-price-game">Mệnh giá: </p>
            <img class="order-category-game pl-5" id="order-category" style="object-fit:contain; width:227px; height:63px; ">
        </div>
        <div class="select-payment">
            <p class="title-popup">Chọn hình thức thanh toán</p>
            <div class="d-flex align-items-center payment-game" data-payment="0">
                <div class="check-box"></div>
                <div class="d-flex align-items-center justify-content-between" style="width: 90%">
                    <p class="text">Thanh toán bằng thẻ quốc tế Visa, Master, JCB</p>
                    <div class="image"><img src="Icon/AddCartPhone/group_payment.png"></div>
                </div>
            </div>
            <div class="d-flex align-items-center payment-game" data-payment="1">
                <div class="check-box"></div>
                <p class="text">Thanh toán bằng thẻ ATM nội địa/Internet Banking (Miễn phí thanh toán)</p>
            </div>
            <div class="d-flex align-items-center payment-game" data-payment="2">
                <div class="check-box"></div>
                <p class="text">Thanh toán bằng ví Momo</p>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-lg-center content-button">
            <button class="btn-cancel">Hủy</button>
            <button class="btn-confirm-buy-card-game"
                    data-customer="{{isset($data_customer) ? $data_customer['id'] : ''}}"
                    data-url="{{url('recharge-card-phone/save_card_game')}}"
            >Thanh toán
            </button>
        </div>
    </div>
</div>
@include('frontend.base.footer')
<!-- END: Footer-->
@include('frontend.base.script')
<script src="js/AddCartPhone/index.js"></script>
</body>
<!-- END: Body-->
</html>
