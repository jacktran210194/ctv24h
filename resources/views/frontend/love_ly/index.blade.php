<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/lovely/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="container">
        <div class="main-lovely mt-5 mb-5">
            <div class="header_lovely d-flex">
                <a href="<?= URL::to('love_ly?type=product_lovely') ?>"
                   class="p-3 <?= $type === 'product_lovely' ? 'active' : 'inactive' ?>">
                    Sản phẩm yêu thích
                </a>
                <a href="<?= URL::to('love_ly?type=shop_lovely') ?>"
                   class="p-3 ml-2 <?= $type === 'shop_lovely' ? 'active' : 'inactive' ?> ">
                    Cửa hàng yêu thích
                </a>
            </div>
            <div class="mt-5">
                <?php if ($type === 'product_lovely'): ?>
                    @include('frontend.love_ly.product')
                <?php else: ?>
                    @include('frontend.love_ly.shop')
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
<script src="js\products\index.js">

</script>
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-save", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            data['customer_id'] = $(this).attr(' data-customer');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                // success: function () {
                //     window.location.reload();
                // },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });
</script>
</body>
<!-- END: Body-->
</html>
