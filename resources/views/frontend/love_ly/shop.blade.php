@if($dataLoveShop)
    @foreach($dataLoveShop as $value)
        <div class="d-flex item-product mb-4">
            <a href="{{url('love_ly/unlike_shop/'.$value->id)}}"><i class="fa fa-times fa-lg"
                                                                    style="position: absolute; z-index: 100; right: 1%; top: 8%;"></i></a>
            <div class="col-lg-2 p-0">
                <a href="{{url('seeshop/'.$value->shop_id)}}">
                    <img style="height: 130px; width: 170px; object-fit: cover;" src="{{$value->shop_image}}"
                         class="img-product">
                </a>
            </div>
            <div class="col-lg-10 pr-4 d-flex flex-column justify-content-around">
                <div>
                    <div class="float-left bookmark-wrapper d-flex align-items-center">
                        <p class="name-product"><a href="{{url('seeshop/'.$value->shop_id)}}">{{$value->shop_name}}</a></p>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="col-lg-7 p-0 d-flex justify-content-between">
                        <div>
                            <div class="d-flex align-items-center">
                                <object data="Icon/cart/shop_name.svg" width="20" height="20"></object>
{{--                                <b class="ml-3">ctv24h</b>--}}
                                <object data="Icon/lovely/mail.svg" class="ml-3" width="50"></object>
                            </div>
{{--                            <div class="mt-2">--}}
{{--                                        <span class="text-note"><span--}}
{{--                                                class="text-red">15,6k</span> Người Theo Dõi </span>--}}
{{--                                <span class="text-note pl-2"><span--}}
{{--                                        class="text-red">2,4k</span> Đang Theo Dõi </span>--}}
{{--                            </div>--}}
                        </div>
                        <div>
                            <div class="d-flex align-items-center justify-content-center">
                                <object data="Icon/lovely/product.svg" width="20" height="20"></object>
                                <b class="ml-1 text-red">{{$value->count_product}}</b>
                            </div>
                            <div class="mt-2 d-flex align-items-center">
                                <span>Sản Phẩm</span>
                            </div>
                        </div>
                        <div>
                            <div class="d-flex align-items-center justify-content-center">
                                <object data="Icon/lovely/feedback.svg" width="20" height="20"></object>
                                <b class="ml-1 text-red">{{$value->count_review}}</b>
                            </div>
                            <div class="mt-2 d-flex align-items-center">
                                <span>Đấu Giá</span>
                            </div>
                        </div>
                        <div>
                            <div class="d-flex align-items-center justify-content-center">
                                <object data="Icon/lovely/percent_feedback.svg" width="20" height="20"></object>
                                <b class="ml-1 text-red">{{$value->percent_feedback ?? 0}}%</b>
                            </div>
                            <div class="mt-2 d-flex align-items-center">
                                <span>Tỉ Lệ Phản Hồi</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 p-0 d-flex align-items-center justify-content-center">
                        <a class="btn btn-save d-flex align-items-center" href="{{url('seeshop/'.$value->shop_id)}}">
                            <object data="Icon/lovely/shop.svg" width="20" height="20"></object>
                            <span class="save-shop pl-3">Xem Shop</span>
                        </a>
                        <a class="btn btn-save d-flex align-items-center ml-3"
                           href="{{url('chat/messenger/shop/'.$value->customer_id)}}">
                            <object data="Icon/lovely/chat_with_shop.svg" width="20" height="20"></object>
                            <span class="save-shop pl-3">Chat với shop</span>
                        </a>
                    </div>
                </div>
                <div></div>
            </div>
        </div>
    @endforeach
@endif
<div class="paginate pagination paginate-review">
    {{ $dataLoveShop->render("pagination::bootstrap-4") }}
</div>
