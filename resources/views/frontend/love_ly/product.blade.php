@if($dataFavouriteProduct)
    @foreach($dataFavouriteProduct as $value)
        <div class="d-flex item-product mb-4">
            <div class="col-lg-2 p-0">
                <a href="{{url('details_product/'.$value->product_id)}}">
                    <img style="height: 130px; width: 170px; object-fit: cover;" src="{{$value->product_image}}"
                         class="img-product">
                </a>
            </div>
            <div class="col-lg-10 pr-4 d-flex flex-column justify-content-around">
                <div>
                    <div class="float-left bookmark-wrapper d-flex align-items-center">
                        <a href="{{url('details_product/'.$value->product_id)}}">
                            <span class="name-product">{{$value->product_name}}</span>
                        </a>
                    </div>
                    <div class="float-right d-flex align-items-center">
                        <span class="old-price">{{number_format($value->product_price)}} đ</span>
                        <span class="new-price ml-3">{{number_format($value->product_price_discount)}} đ</span>
                    </div>
                </div>
                <div>
                    <div class="float-left bookmark-wrapper d-flex align-items-center">
                        <img style="border-radius: 50%!important;" width="20px" height="20px" src="{{$value->shop_image}}" alt="">
                        <a href="{{url('seeshop/'.$value->shop_id)}}"><b class="ml-3">{{$value->shop_name}}</b></a>
                        <img src="Icon/lovely/star.png">
                    </div>
                    <div class="float-right d-flex align-items-center">
                        <object data="Icon/cart/so_xu.svg" width="20" height="20"></object>
                        <span class="new-price ml-3">100 coin</span>
                    </div>
                </div>
                <div>
                    <div class="float-left bookmark-wrapper d-flex align-items-center">
                        <a class="btn btn-save d-flex align-items-center"
                           data-id="{{$value->shop_id}}"
                           data-msg="Love it?"
                           data-url="{{url('seeshop/love')}}"
                        >
                            <object data="Icon/lovely/save_shop.svg" width="20" height="20"></object>
                            <span class="save-shop pl-3">Lưu shop</span>
                        </a>
                        <a class="btn btn-save d-flex align-items-center ml-3"
                           href="{{url('love_ly/delete_product/'.$value->id)}}">
                            <i class="fal fa-thumbs-down fa-lg"></i>
                            <span class="save-shop pl-3">Bỏ thích</span>
                        </a>
                    </div>
                    <div class="float-right d-flex align-items-center">
                        <a class="btn btn-save d-flex align-items-center"
                           href="{{url('chat/messenger/shop/' . $value->customer_id)}}">
                            <object data="Icon/lovely/chat_with_shop.svg" width="20" height="20"></object>
                            <span class="save-shop pl-3">Chat với shop</span>
                        </a>
                        <a class="btn btn-save d-flex align-items-center ml-3"
                           href="{{url('details_product/'.$value->product_id)}}"
                        >
                            <object data="Icon/lovely/add_to_cart.svg" width="20" height="20"></object>
                            <span class="save-shop pl-3">Thêm vào giỏ hàng</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif

<div class="paginate-product d-flex align-items-center">
    {{--    <div class="content-paginate">--}}
    {{--        <span class="in-paginate">{{$dataFavouriteProduct->currentPage()}}</span>/<span class="all-paginate">{{$dataFavouriteProduct->lastPage()}}</span>--}}
    {{--    </div>--}}
    {{--    <button class="btn-sort next-paginate relative">--}}
    {{--        <a class="d-flex w-100 h-100" href="{{$dataFavouriteProduct->previousPageUrl()}}">--}}
    {{--            <object data="icon/search/polygon_left.svg"></object>--}}
    {{--        </a>--}}
    {{--    </button>--}}
    {{--    <button class="btn-sort prev-paginate relative">--}}
    {{--        <a class="d-flex w-100 h-100" href="{{$dataFavouriteProduct->nextPageUrl()}}">--}}
    {{--            <object data="icon/search/polygon_right.svg"></object>--}}
    {{--        </a>--}}
    {{--    </button>--}}
    {{$dataFavouriteProduct->render("pagination::bootstrap-4")}}
</div>


