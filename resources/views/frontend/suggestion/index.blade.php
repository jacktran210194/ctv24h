<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/suggestion/suggestion.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <!-- BEGIN: Header-->
    <!-- END: Header-->
    <!-- BEGIN: Homepage-->
    <div id="img">
        <div class="container p-0">
            <img class="img img-fluid"
                 src="Icon/suggestion/banner.png"
                 alt="">
        </div>
    </div>
    <div id="category" class="">
        <div class="container menu-category pt-5 p-0">
            <div class="tab-category mb-3">
                <p class="text-menu-tab">NCC NỔI BẬT</p>
            </div>
            <div class="slick-voucher">
                <div class="shop-detail bg-white" style="">
                    <div class="shop-shop" style="">
                        <div class="d-flex info">
                            <a href="">
                                <img class="avt-shop" src="Icon/suggestion/avt_shop.png" alt=""
                                     style="">
                            </a>
                            <div class="info-shop pl-3 d-flex">
                                <div>
                                    <h4 class="text-name">Quần Jean 1hit shop </h4>
                                    <span class="text-address">89 Đặng Văn Bi, P. Trường Thọ, Quận Thủ Đức
                                            </span>
                                </div>
                                <button class="btn btn-default btn-follow"
                                        style="">+ Theo dõi
                                </button>

                            </div>

                        </div>
                        <div class="shop content-gallery-product d-flex">
                            <div>
                                <img class="img_1" height=""
                                     src="Icon/suggestion/product_1.png"
                                     alt="">
                            </div>
                            <div class="list-img pl-3">
                                <div class="pb-3">
                                    <img class="img_23" src="Icon/suggestion/product_2.png" alt="">
                                </div>
                                <div>
                                    <img class="img_23" src="Icon/suggestion/product_3.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shop-detail bg-white" style="">
                    <div class="shop-shop" style="">
                        <div class="d-flex info">
                            <a href="">
                                <img class="avt-shop" src="Icon/suggestion/avt_shop.png" alt=""
                                     style="">
                            </a>
                            <div class="info-shop pl-3 d-flex">
                                <div>
                                    <h4 class="text-name">Quần Jean 1hit shop </h4>
                                    <span class="text-address">89 Đặng Văn Bi, P. Trường Thọ, Quận Thủ Đức
                                            </span>
                                </div>
                                <button class="btn btn-default btn-follow"
                                        style="">+ Theo dõi
                                </button>
                            </div>
                        </div>
                        <div class="shop content-gallery-product d-flex">
                            <div>
                                <img class="img_1" height=""
                                     src="Icon/suggestion/product_1.png"
                                     alt="">
                            </div>
                            <div class="list-img pl-3">
                                <div class="pb-3">
                                    <img class="img_23" src="Icon/suggestion/product_2.png" alt="">
                                </div>
                                <div>
                                    <img class="img_23" src="Icon/suggestion/product_3.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shop-detail bg-white" style="">
                    <div class="shop-shop" style="">
                        <div class="d-flex info">
                            <a href="">
                                <img class="avt-shop" src="Icon/suggestion/avt_shop.png" alt=""
                                     style="">
                            </a>
                            <div class="info-shop pl-3 d-flex">
                                <div>
                                    <h4 class="text-name">Quần Jean 1hit shop </h4>
                                    <span class="text-address">89 Đặng Văn Bi, P. Trường Thọ, Quận Thủ Đức
                                            </span>
                                </div>
                                <button class="btn btn-default btn-follow"
                                        style="">+ Theo dõi
                                </button>

                            </div>

                        </div>
                        <div class="shop content-gallery-product d-flex">
                            <div>
                                <img class="img_1" height=""
                                     src="Icon/suggestion/product_1.png"
                                     alt="">
                            </div>
                            <div class="list-img pl-3">
                                <div class="pb-3">
                                    <img class="img_23" src="Icon/suggestion/product_2.png" alt="">
                                </div>
                                <div>
                                    <img class="img_23" src="Icon/suggestion/product_3.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shop-detail bg-white" style="">
                    <div class="shop-shop" style="">
                        <div class="d-flex info">
                            <a href="">
                                <img class="avt-shop" src="Icon/suggestion/avt_shop.png" alt=""
                                     style="">
                            </a>
                            <div class="info-shop pl-3 d-flex">
                                <div>
                                    <h4 class="text-name">Quần Jean 1hit shop </h4>
                                    <span class="text-address">89 Đặng Văn Bi, P. Trường Thọ, Quận Thủ Đức
                                            </span>
                                </div>
                                <button class="btn btn-default btn-follow"
                                        style="">+ Theo dõi
                                </button>

                            </div>

                        </div>
                        <div class="shop content-gallery-product d-flex">
                            <div>
                                <img class="img_1" height=""
                                     src="Icon/suggestion/product_1.png"
                                     alt="">
                            </div>
                            <div class="list-img pl-3">
                                <div class="pb-3">
                                    <img class="img_23" src="Icon/suggestion/product_2.png" alt="">
                                </div>
                                <div>
                                    <img class="img_23" src="Icon/suggestion/product_3.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shop-detail bg-white" style="">
                    <div class="shop-shop" style="">
                        <div class="d-flex info">
                            <a href="">
                                <img class="avt-shop" src="Icon/suggestion/avt_shop.png" alt=""
                                     style="">
                            </a>
                            <div class="info-shop pl-3 d-flex">
                                <div>
                                    <h4 class="text-name">Quần Jean 1hit shop </h4>
                                    <span class="text-address">89 Đặng Văn Bi, P. Trường Thọ, Quận Thủ Đức
                                            </span>
                                </div>
                                <button class="btn btn-default btn-follow"
                                        style="">+ Theo dõi
                                </button>
                            </div>
                        </div>
                        <div class="shop content-gallery-product d-flex">
                            <div>
                                <img class="img_1" height=""
                                     src="Icon/suggestion/product_1.png"
                                     alt="">
                            </div>
                            <div class="list-img pl-3">
                                <div class="pb-3">
                                    <img class="img_23" src="Icon/suggestion/product_2.png" alt="">
                                </div>
                                <div>
                                    <img class="img_23" src="Icon/suggestion/product_3.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shop-detail bg-white" style="">
                    <div class="shop-shop" style="">
                        <div class="d-flex info">
                            <a href="">
                                <img class="avt-shop" src="Icon/suggestion/avt_shop.png" alt=""
                                     style="">
                            </a>
                            <div class="info-shop pl-3 d-flex">
                                <div>
                                    <h4 class="text-name">Quần Jean 1hit shop </h4>
                                    <span class="text-address">89 Đặng Văn Bi, P. Trường Thọ, Quận Thủ Đức
                                            </span>
                                </div>
                                <button class="btn btn-default btn-follow"
                                        style="">+ Theo dõi
                                </button>

                            </div>

                        </div>
                        <div class="shop content-gallery-product d-flex">
                            <div>
                                <img class="img_1" height=""
                                     src="Icon/suggestion/product_1.png"
                                     alt="">
                            </div>
                            <div class="list-img pl-3">
                                <div class="pb-3">
                                    <img class="img_23" src="Icon/suggestion/product_2.png" alt="">
                                </div>
                                <div>
                                    <img class="img_23" src="Icon/suggestion/product_3.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-1" class="bg-white">
                <div class="tab-category mt-5">
                    <p class="text-menu-tab">SẢN PHẨM NỔI BẬT</p>
                </div>
                <div class="d-flex p-2">
                    <?php for($i = 1;$i <= 6;$i++): ?>
                    <div class="col-lg-2 col-md-6 col-sm-6 pl-1">
                        <div class="card-item">
                            <img src="Icon/deals/product.png" width="100%">
                            <div class="p-3 bg-white d-flex flex-column">
                                <div class="text-bold-600 number-line-2">
                                    Quần jean ống rộng Kèm Hình Thật...
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                                <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                        Giá CTV: ****
                                    </div>
                                    <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                        Lời: 50.000
                                    </div>
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex col-lg-6 p-0">
                                        <img src="Icon/lovely/star.png" width="100%">
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0">
                                        Đã bán 10
                                    </div>
                                </div>
                                <div class="d-flex mt-2 align-items-center">
                                    <div class="d-flex col-lg-6 p-0 align-items-center">
                                        <object data="Icon/deals/like.svg"></object>
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                        Hà Nội
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>
                <div class="d-flex p-2">
                    <?php for($i = 1;$i <= 6;$i++): ?>
                    <div class="col-lg-2 col-md-6 col-sm-6 pl-1">
                        <div class="card-item">
                            <img src="Icon/deals/product.png" width="100%">
                            <div class="p-3 bg-white d-flex flex-column">
                                <div class="text-bold-600 number-line-2">
                                    Quần jean ống rộng Kèm Hình Thật...
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                                <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                        Giá CTV: ****
                                    </div>
                                    <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                        Lời: 50.000
                                    </div>
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex col-lg-6 p-0">
                                        <img src="Icon/lovely/star.png" width="100%">
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0">
                                        Đã bán 10
                                    </div>
                                </div>
                                <div class="d-flex mt-2 align-items-center">
                                    <div class="d-flex col-lg-6 p-0 align-items-center">
                                        <object data="Icon/deals/like.svg"></object>
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                        Hà Nội
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>
                <div class="d-flex p-2">
                    <?php for($i = 1;$i <= 6;$i++): ?>
                    <div class="col-lg-2 col-md-6 col-sm-6 pl-1">
                        <div class="card-item">
                            <img src="Icon/deals/product.png" width="100%">
                            <div class="p-3 bg-white d-flex flex-column">
                                <div class="text-bold-600 number-line-2">
                                    Quần jean ống rộng Kèm Hình Thật...
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                                <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                        Giá CTV: ****
                                    </div>
                                    <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                        Lời: 50.000
                                    </div>
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex col-lg-6 p-0">
                                        <img src="Icon/lovely/star.png" width="100%">
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0">
                                        Đã bán 10
                                    </div>
                                </div>
                                <div class="d-flex mt-2 align-items-center">
                                    <div class="d-flex col-lg-6 p-0 align-items-center">
                                        <object data="Icon/deals/like.svg"></object>
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                        Hà Nội
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>
                <div class="d-flex p-2">
                    <?php for($i = 1;$i <= 6;$i++): ?>
                    <div class="col-lg-2 col-md-6 col-sm-6 pl-1">
                        <div class="card-item">
                            <img src="Icon/deals/product.png" width="100%">
                            <div class="p-3 bg-white d-flex flex-column">
                                <div class="text-bold-600 number-line-2">
                                    Quần jean ống rộng Kèm Hình Thật...
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                                <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                        Giá CTV: ****
                                    </div>
                                    <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                        Lời: 50.000
                                    </div>
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex col-lg-6 p-0">
                                        <img src="Icon/lovely/star.png" width="100%">
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0">
                                        Đã bán 10
                                    </div>
                                </div>
                                <div class="d-flex mt-2 align-items-center">
                                    <div class="d-flex col-lg-6 p-0 align-items-center">
                                        <object data="Icon/deals/like.svg"></object>
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                        Hà Nội
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
        <!-- END: Homepage-->

        <!-- BEGIN: Footer-->
    @include('frontend.base.footer')
    <!-- END: Footer-->
    </div>
</div>

@include('frontend.base.script')
<script src="js/suggestion/index.js"></script>
</body>
<!-- END: Body-->
</html>
