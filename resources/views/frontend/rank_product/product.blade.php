<div class="d-flex pt-5 pb-5 item-product">
    <div class="d-flex flex-column pr-3 pl-3 pt-2 align-items-center">
        <span class="text-bold-600 font-size-12">1</span>
        <object data="Icon/rank_product/gold.svg"></object>
    </div>
    <div>
        <img src="Icon/cart/image_product.png">
    </div>
    <div class="ml-4 d-flex flex-column justify-content-between w-100 pr-5">
        <span class="text-bold-600 number-line-1">Giày Sneakers Unisex Converse Chuck Taylor All Star 1970s Black/ White - 162050C</span>
        <div class="">
            <div class="float-left d-flex bookmark-wrapper align-items-center">
                <object data="Icon/cart/shop_name.svg" width="20" height="20"></object>
                <b class="ml-3">wear.vn</b>
                <object data="Icon/lovely/mail.svg" class="ml-3" width="50"></object>
            </div>
            <div class="float-right">
                <span class="price-old">₫2.000.000</span>
            </div>
        </div>
        <div class="">
            <div class="float-left d-flex bookmark-wrapper align-items-center">
                <img src="Icon/lovely/star.png">
            </div>
            <div class="float-right">
                <span class="price-new text-bold-600">₫1.700.000</span>
            </div>
        </div>
        <div class="">
            <div class="float-left d-flex bookmark-wrapper align-items-center">
                <button class="btn btn-main pl-5 pr-5">Yêu Thích</button>
            </div>
            <div class="float-right">
                <a>
                    <button class="btn btn-main d-flex align-items-center pl-5 pr-5">
                        <object data="Icon/rank_product/cart.svg" width="20"></object><span class="ml-2">Thêm vào giỏ hàng </span>
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="d-flex pt-5 pb-5 item-product">
    <div class="d-flex flex-column pr-3 pl-3 pt-2 align-items-center">
        <span class="text-bold-600 font-size-12">2</span>
        <object data="Icon/rank_product/silver.svg"></object>
    </div>
    <div>
        <img src="Icon/cart/image_product.png">
    </div>
    <div class="ml-4 d-flex flex-column justify-content-between w-100 pr-5">
        <span class="text-bold-600 number-line-1">Giày Sneakers Unisex Converse Chuck Taylor All Star 1970s Black/ White - 162050C</span>
        <div class="">
            <div class="float-left d-flex bookmark-wrapper align-items-center">
                <object data="Icon/cart/shop_name.svg" width="20" height="20"></object>
                <b class="ml-3">wear.vn</b>
                <object data="Icon/lovely/mail.svg" class="ml-3" width="50"></object>
            </div>
            <div class="float-right">
                <span class="price-old">₫2.000.000</span>
            </div>
        </div>
        <div class="">
            <div class="float-left d-flex bookmark-wrapper align-items-center">
                <img src="Icon/lovely/star.png">
            </div>
            <div class="float-right">
                <span class="price-new text-bold-600">₫1.700.000</span>
            </div>
        </div>
        <div class="">
            <div class="float-left d-flex bookmark-wrapper align-items-center">
                <button class="btn btn-main pl-5 pr-5">Yêu Thích</button>
            </div>
            <div class="float-right">
                <a>
                    <button class="btn btn-main d-flex align-items-center pl-5 pr-5">
                        <object data="Icon/rank_product/cart.svg" width="20"></object><span class="ml-2">Thêm vào giỏ hàng </span>
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="d-flex pt-5 pb-5 item-product">
    <div class="d-flex flex-column pr-3 pl-3 pt-2 align-items-center">
        <span class="text-bold-600 font-size-12">3</span>
        <object data="Icon/rank_product/bronze.svg"></object>
    </div>
    <div>
        <img src="Icon/cart/image_product.png">
    </div>
    <div class="ml-4 d-flex flex-column justify-content-between w-100 pr-5">
        <span class="text-bold-600 number-line-1">Giày Sneakers Unisex Converse Chuck Taylor All Star 1970s Black/ White - 162050C</span>
        <div class="">
            <div class="float-left d-flex bookmark-wrapper align-items-center">
                <object data="Icon/cart/shop_name.svg" width="20" height="20"></object>
                <b class="ml-3">wear.vn</b>
                <object data="Icon/lovely/mail.svg" class="ml-3" width="50"></object>
            </div>
            <div class="float-right">
                <span class="price-old">₫2.000.000</span>
            </div>
        </div>
        <div class="">
            <div class="float-left d-flex bookmark-wrapper align-items-center">
                <img src="Icon/lovely/star.png">
            </div>
            <div class="float-right">
                <span class="price-new text-bold-600">₫1.700.000</span>
            </div>
        </div>
        <div class="">
            <div class="float-left d-flex bookmark-wrapper align-items-center">
                <button class="btn btn-main pl-5 pr-5">Yêu Thích</button>
            </div>
            <div class="float-right">
                <a>
                    <button class="btn btn-main d-flex align-items-center pl-5 pr-5">
                        <object data="Icon/rank_product/cart.svg" width="20"></object><span class="ml-2">Thêm vào giỏ hàng </span>
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
