<div class="d-flex pt-5 pb-5 item-product">
    <div class="d-flex flex-column pr-3 pl-3 pt-2 align-items-center">
        <span class="text-bold-600 font-size-12">1</span>
        <object data="Icon/rank_product/gold.svg"></object>
    </div>
    <div>
        <img src="Icon/cart/image_product.png">
    </div>
    <div class="ml-4 d-flex flex-column justify-content-between w-100 pr-5">
        <div class="d-flex justify-content-between">
            <div>
                <div class="d-flex align-items-center">
                    <object data="Icon/cart/shop_name.svg" width="20" height="20"></object>
                    <b class="ml-3">wear.vn</b>
                    <object data="Icon/lovely/mail.svg" class="ml-3" width="50"></object>
                </div>
                <div class="mt-2">
                                        <span class="text-note"><span
                                                class="text-red">15,6k</span> Người Theo Dõi </span>
                    <span class="text-note pl-2"><span
                            class="text-red">2,4k</span> Đang Theo Dõi </span>
                </div>
            </div>
            <div>
                <div class="d-flex align-items-center justify-content-center">
                    <object data="Icon/lovely/product.svg" width="20" height="20"></object>
                    <b class="ml-1 text-red">72</b>
                </div>
                <div class="mt-2 d-flex align-items-center">
                    <span>Sản Phẩm</span>
                </div>
            </div>
            <div>
                <div class="d-flex align-items-center justify-content-center">
                    <object data="Icon/lovely/feedback.svg" width="20" height="20"></object>
                    <b class="ml-1 text-red">4.9</b>
                </div>
                <div class="mt-2 d-flex align-items-center">
                    <span>Đấu Giá</span>
                </div>
            </div>
            <div>
                <div class="d-flex align-items-center justify-content-center">
                    <object data="Icon/lovely/percent_feedback.svg" width="20" height="20"></object>
                    <b class="ml-1 text-red">98%</b>
                </div>
                <div class="mt-2 d-flex align-items-center">
                    <span>Tỉ Lệ Phản Hồi</span>
                </div>
            </div>
        </div>
        <div class="d-flex align-items-center">
            <a class="btn btn-main d-flex align-items-center">
                <object data="Icon/rank_product/like.svg" width="20" height="20"></object>
                <span class="save-shop pl-3 text-white" >Yêu Thích</span>
            </a>
            <a class="btn btn-main d-flex align-items-center ml-3">
                <object data="Icon/lovely/chat_with_shop.svg" width="20" height="20"></object>
                <span class="save-shop pl-3 text-white" >Chat</span>
            </a>
            <a class="btn btn-main d-flex align-items-center ml-3">
                <object data="Icon/lovely/shop.svg" width="20" height="20"></object>
                <span class="save-shop pl-3 text-white">Xem Shop</span>
            </a>
        </div>
    </div>
</div>
<div class="d-flex pt-5 pb-5 item-product">
    <div class="d-flex flex-column pr-3 pl-3 pt-2 align-items-center">
        <span class="text-bold-600 font-size-12">2</span>
        <object data="Icon/rank_product/silver.svg"></object>
    </div>
    <div>
        <img src="Icon/cart/image_product.png">
    </div>
    <div class="ml-4 d-flex flex-column justify-content-between w-100 pr-5">
        <div class="d-flex justify-content-between">
            <div>
                <div class="d-flex align-items-center">
                    <object data="Icon/cart/shop_name.svg" width="20" height="20"></object>
                    <b class="ml-3">wear.vn</b>
                    <object data="Icon/lovely/mail.svg" class="ml-3" width="50"></object>
                </div>
                <div class="mt-2">
                                        <span class="text-note"><span
                                                class="text-red">15,6k</span> Người Theo Dõi </span>
                    <span class="text-note pl-2"><span
                            class="text-red">2,4k</span> Đang Theo Dõi </span>
                </div>
            </div>
            <div>
                <div class="d-flex align-items-center justify-content-center">
                    <object data="Icon/lovely/product.svg" width="20" height="20"></object>
                    <b class="ml-1 text-red">72</b>
                </div>
                <div class="mt-2 d-flex align-items-center">
                    <span>Sản Phẩm</span>
                </div>
            </div>
            <div>
                <div class="d-flex align-items-center justify-content-center">
                    <object data="Icon/lovely/feedback.svg" width="20" height="20"></object>
                    <b class="ml-1 text-red">4.9</b>
                </div>
                <div class="mt-2 d-flex align-items-center">
                    <span>Đấu Giá</span>
                </div>
            </div>
            <div>
                <div class="d-flex align-items-center justify-content-center">
                    <object data="Icon/lovely/percent_feedback.svg" width="20" height="20"></object>
                    <b class="ml-1 text-red">98%</b>
                </div>
                <div class="mt-2 d-flex align-items-center">
                    <span>Tỉ Lệ Phản Hồi</span>
                </div>
            </div>
        </div>
        <div class="d-flex align-items-center">
            <a class="btn btn-main d-flex align-items-center">
                <object data="Icon/rank_product/like.svg" width="20" height="20"></object>
                <span class="save-shop pl-3 text-white" >Yêu Thích</span>
            </a>
            <a class="btn btn-main d-flex align-items-center ml-3">
                <object data="Icon/lovely/chat_with_shop.svg" width="20" height="20"></object>
                <span class="save-shop pl-3 text-white" >Chat</span>
            </a>
            <a class="btn btn-main d-flex align-items-center ml-3">
                <object data="Icon/lovely/shop.svg" width="20" height="20"></object>
                <span class="save-shop pl-3 text-white">Xem Shop</span>
            </a>
        </div>
    </div>
</div>
<div class="d-flex pt-5 pb-5 item-product">
    <div class="d-flex flex-column pr-3 pl-3 pt-2 align-items-center">
        <span class="text-bold-600 font-size-12">3</span>
        <object data="Icon/rank_product/bronze.svg"></object>
    </div>
    <div>
        <img src="Icon/cart/image_product.png">
    </div>
    <div class="ml-4 d-flex flex-column justify-content-between w-100 pr-5">
        <div class="d-flex justify-content-between">
            <div>
                <div class="d-flex align-items-center">
                    <object data="Icon/cart/shop_name.svg" width="20" height="20"></object>
                    <b class="ml-3">wear.vn</b>
                    <object data="Icon/lovely/mail.svg" class="ml-3" width="50"></object>
                </div>
                <div class="mt-2">
                                        <span class="text-note"><span
                                                class="text-red">15,6k</span> Người Theo Dõi </span>
                    <span class="text-note pl-2"><span
                            class="text-red">2,4k</span> Đang Theo Dõi </span>
                </div>
            </div>
            <div>
                <div class="d-flex align-items-center justify-content-center">
                    <object data="Icon/lovely/product.svg" width="20" height="20"></object>
                    <b class="ml-1 text-red">72</b>
                </div>
                <div class="mt-2 d-flex align-items-center">
                    <span>Sản Phẩm</span>
                </div>
            </div>
            <div>
                <div class="d-flex align-items-center justify-content-center">
                    <object data="Icon/lovely/feedback.svg" width="20" height="20"></object>
                    <b class="ml-1 text-red">4.9</b>
                </div>
                <div class="mt-2 d-flex align-items-center">
                    <span>Đấu Giá</span>
                </div>
            </div>
            <div>
                <div class="d-flex align-items-center justify-content-center">
                    <object data="Icon/lovely/percent_feedback.svg" width="20" height="20"></object>
                    <b class="ml-1 text-red">98%</b>
                </div>
                <div class="mt-2 d-flex align-items-center">
                    <span>Tỉ Lệ Phản Hồi</span>
                </div>
            </div>
        </div>
        <div class="d-flex align-items-center">
            <a class="btn btn-main d-flex align-items-center">
                <object data="Icon/rank_product/like.svg" width="20" height="20"></object>
                <span class="save-shop pl-3 text-white" >Yêu Thích</span>
            </a>
            <a class="btn btn-main d-flex align-items-center ml-3">
                <object data="Icon/lovely/chat_with_shop.svg" width="20" height="20"></object>
                <span class="save-shop pl-3 text-white" >Chat</span>
            </a>
            <a class="btn btn-main d-flex align-items-center ml-3">
                <object data="Icon/lovely/shop.svg" width="20" height="20"></object>
                <span class="save-shop pl-3 text-white">Xem Shop</span>
            </a>
        </div>
    </div>
</div>
<div class="d-flex pt-5 pb-5 item-product">
    <div class="d-flex flex-column pr-3 pl-3 pt-2 align-items-center">
        <span class="text-bold-600 font-size-12"></span>
        <object data="Icon/rank_product/gold.svg"></object>
    </div>
    <div>
        <img src="Icon/cart/image_product.png">
    </div>
    <div class="ml-4 d-flex flex-column justify-content-between w-100 pr-5">
        <div class="d-flex justify-content-between">
            <div>
                <div class="d-flex align-items-center">
                    <object data="Icon/cart/shop_name.svg" width="20" height="20"></object>
                    <b class="ml-3">wear.vn</b>
                    <object data="Icon/lovely/mail.svg" class="ml-3" width="50"></object>
                </div>
                <div class="mt-2">
                                        <span class="text-note"><span
                                                class="text-red">15,6k</span> Người Theo Dõi </span>
                    <span class="text-note pl-2"><span
                            class="text-red">2,4k</span> Đang Theo Dõi </span>
                </div>
            </div>
            <div>
                <div class="d-flex align-items-center justify-content-center">
                    <object data="Icon/lovely/product.svg" width="20" height="20"></object>
                    <b class="ml-1 text-red">72</b>
                </div>
                <div class="mt-2 d-flex align-items-center">
                    <span>Sản Phẩm</span>
                </div>
            </div>
            <div>
                <div class="d-flex align-items-center justify-content-center">
                    <object data="Icon/lovely/feedback.svg" width="20" height="20"></object>
                    <b class="ml-1 text-red">4.9</b>
                </div>
                <div class="mt-2 d-flex align-items-center">
                    <span>Đấu Giá</span>
                </div>
            </div>
            <div>
                <div class="d-flex align-items-center justify-content-center">
                    <object data="Icon/lovely/percent_feedback.svg" width="20" height="20"></object>
                    <b class="ml-1 text-red">98%</b>
                </div>
                <div class="mt-2 d-flex align-items-center">
                    <span>Tỉ Lệ Phản Hồi</span>
                </div>
            </div>
        </div>
        <div class="d-flex align-items-center">
            <a class="btn btn-main d-flex align-items-center">
                <object data="Icon/rank_product/like.svg" width="20" height="20"></object>
                <span class="save-shop pl-3 text-white" >Yêu Thích</span>
            </a>
            <a class="btn btn-main d-flex align-items-center ml-3">
                <object data="Icon/lovely/chat_with_shop.svg" width="20" height="20"></object>
                <span class="save-shop pl-3 text-white" >Chat</span>
            </a>
            <a class="btn btn-main d-flex align-items-center ml-3">
                <object data="Icon/lovely/shop.svg" width="20" height="20"></object>
                <span class="save-shop pl-3 text-white">Xem Shop</span>
            </a>
        </div>
    </div>
</div>
