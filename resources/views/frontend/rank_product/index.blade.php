<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/rank_product/style.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="container mb-5">
        <div class="d-flex">
            <div class="col-lg-3">
                @include('frontend.rank_product.category')
            </div>
            <div class="col-lg-9 p-0">
                <div class="mb-5 main-rank">
                    <img src="Icon/rank_product/banner.png" class="w-100">
                    <div class="mt-5">
                        <div class="d-flex header">
                            <div class="d-flex justify-content-center align-items-center p-3 text-bold-600 <?= $type === 'product' ? 'active' : 'inactive' ?>">
                                <a href="<?= URL::to('rank_product') ?>"><span>Top Sản Phẩm Bán Chạy</span></a>
                            </div>
                            <div class="d-flex justify-content-center align-items-center p-3 text-bold-600 <?= $type === 'shop' ? 'active' : 'inactive' ?> ml-3">
                                <a href="<?= URL::to('rank_product?type=shop') ?>"><span>Top NCC Hàng Đầu</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="mt-1">
                        <?php if ($type === 'product') : ?>
                        @include('frontend.rank_product.product')
                        <?php else: ?>
                            @include('frontend.rank_product.shop')
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
