<link rel="stylesheet" type="text/css" href="css/base/category.css">
<div class="main-category p-3">
    <div>
        <p class="text-bold-600 font-size-12">DANH MỤC</p>
        <div class="mt-3">
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Áo khoác dù (17)</span>
            </div>
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Quần short (17)</span>
            </div>
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Kính mát (17)</span>
            </div>
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Áo thun (13)</span>
            </div>
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Kính thời trang (13)</span>
            </div>
            <div class="pt-2 mb-2 ml-4">
                <a>Xem thêm >></a>
            </div>
        </div>
    </div>
    <div class="mt-5">
        <p class="text-bold-600 font-size-12">Nơi bán</p>
        <div class="mt-3">
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Hà Nội</span>
            </div>
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">TP.Hồ Chí Minh</span>
            </div>
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Thái nguyên</span>
            </div>
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Vĩnh Phúc </span>
            </div>
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Hải Phòng </span>
            </div>
            <div class="pt-2 mb-2 ml-4">
                <a>Xem thêm >></a>
            </div>
        </div>
    </div>
    <div class="mt-5">
        <p class="text-bold-600 font-size-12">Đơn vị vận chuyển</p>
        <div class="mt-3">
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">NowShip</span>
            </div>
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Ninja Van</span>
            </div>
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">J&T Express</span>
            </div>
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Giao hàng tiết kiệm </span>
            </div>
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Giao hàng nhanh </span>
            </div>
        </div>
    </div>
    <div class="mt-5">
        <p class="text-bold-600 font-size-12">Loại shop</p>
        <div class="mt-3">
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Shopee Mall</span>
            </div>
            <div class="d-flex align-items-end mb-2">
                <input type="checkbox">
                <span class="ml-1 pt-2">Shop yêu thích</span>
            </div>
        </div>
    </div>
</div>
