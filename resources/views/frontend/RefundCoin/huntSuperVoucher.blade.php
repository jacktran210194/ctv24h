
<div class="guide-refund-coin hunt-voucher">
    <div class="title-box d-flex">
        <div class="image-title">
            <img src="<?php echo  asset("$path/icon_title.png") ?>">
        </div>
        <h2 class="title">săn siêu voucher</h2>
    </div>
    <div class="content-box d-flex justify-content-lg-center">
        <div class="d-flex justify-content-lg-center align-items-center flex-wrap w-75">
            <div class="image w-50" style="padding:0 16px">
                <img class="w-100" src="Icon/RefundCoin/hunt_voucher_1.png">
            </div>
            <div class="image w-50" style="padding:0 16px">
                <img class="w-100" src="Icon/RefundCoin/hunt_voucher_1.png">
            </div>
            <div class="image w-50" style="padding:0 16px">
                <img class="w-100" src="Icon/RefundCoin/hunt_voucher_1.png">
            </div>
            <div class="image w-50" style="padding:0 16px">
                <img class="w-100" src="Icon/RefundCoin/hunt_voucher_1.png">
            </div>
        </div>
    </div>
</div>
<div class="guide-refund-coin category-voucher">
    <div class="title-box d-flex">
        <div class="image-title">
            <img src="<?php echo  asset("$path/icon_title.png") ?>">
        </div>
        <h2 class="title">voucher nghành hàng</h2>
    </div>
    <div class="content-box d-flex justify-content-lg-center" style="margin-top: 50px">
        <div class="d-flex justify-content-lg-center align-items-center flex-wrap slick-voucher-refund" style="margin-right: -40px; width: 100%">
            @if(isset($data))
                @foreach($data as $value)
                    <div class="image position-relative" style="margin-right: 40px; width: calc(100%/3 - 40px)">
                        <img class="w-100 position-relative" src="Icon/RefundCoin/bg_refund_coin.png">
                        <div class="position-absolute w-100 h-100 d-flex" style="top: 0; left: 0; z-index: 1; padding: 15px 31px">
                            <div class="bg-white w-100" style="padding: 20px;">
                                <div class="d-flex align-items-center justify-content-between" style="margin-top: 40px">
                                    <h3 class="font-weight-bold" style="font-size: 18px">{{$value->name}}</h3>
                                    <p class="m-0 check-rules"
                                       style="text-decoration: underline; font-size: 14px; color: #FF9611; cursor: pointer">Điều Kiện</p>
                                    <p class="m-0 rules">Đơn giá tối thiểu <?php echo number_format($value->min_price); ?> VNĐ</p>
                                </div>
                                <div class="d-flex align-items-center justify-content-between">
                                    <div>
                                        <p style="font-style: italic; font-size: 12px">Tối đa {{$value->max_point}} xu</p>
                                        <p style="font-size: 10px; color: #FF9611">hết hạn vào ngày<br>
                                        {{$value->time_stop}}
                                        </p>
                                    </div>
                                    <div class="d-flex justify-content-sm-center align-items-center">
                                        <button class="btn-get-voucher font-weight-bold color-white"
                                                data-value="{{$value->id}}"
                                                style="padding: 10px 23px; background: #EE4D2D;border-radius: 4px;border: none">Lấy mã</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $category = \App\Models\CategoryProductModel::where('id', $value->category_id)->first();
                        ?>
                        <div class="position-absolute d-flex" style="top: 0; left: 50%; transform: translate(-50%, -50%); z-index: 10">
                            <div class="d-flex justify-content-sm-center align-items-center"
                                 style="background: #FFFFFF;border: 10px solid #FF9611;box-sizing: border-box; width: 117px; height: 117px; border-radius: 50%">
                                <div>
                                    <div class="d-flex w-100 align-items-center justify-content-sm-center" style="margin-bottom: 15px">
                                        <img src="{{$category->image}}" style="max-width: 45px">
                                    </div>
                                    <p class="font-weight-bold m-0" style="color: #005DB6;overflow: hidden;text-overflow: ellipsis; white-space: nowrap;max-width: 100px;">
                                       {{$category->name}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
