<?php
use Illuminate\Support\Facades\URL;
//$data_customer = Session::get('data_customer');
?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/RefundCoin/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->
    <?php
    $path = "assets/frontend/Icon/RefundCoin";
     $path_flash = "assets/frontend/Icon/home/Danh_muc";
    ?>
    <div class="body-page">
        <div class="container">
            <div class="banner-image">
                <img src="<?php echo  asset("$path/banner.png") ?>">
            </div>
            <div class="guide-refund-coin">
                <div class="title-box d-flex">
                    <div class="image-title">
                        <img src="<?php echo  asset("$path/icon_title.png") ?>">
                    </div>
                    <h2 class="title">các bước hoàn xu</h2>
                </div>
                <div class="content-box">
                    <div class="content d-flex">
                        <div class="guides">
                            <img src="<?php echo  asset("$path/bg_image.png") ?>">
                            <div class="guides-content">
                                <div class="icon-guide">
                                    <img src="<?php echo  asset("$path/gift-card.png") ?>">
                                </div>
                                <p class="text-guide">lấy voucher hoàn xu xtra</p>
                            </div>
                        </div>

                        <div class="guides">
                            <img src="<?php echo  asset("$path/bg_image.png") ?>">
                            <div class="guides-content">
                                <div class="icon-guide">
                                    <img src="<?php echo  asset("$path/long-dress-with-sleeves.png") ?>">
                                </div>
                                <p class="text-guide">mua sản phẩm
                                    có gắn logo
                                    hoàn xu xtra</p>
                            </div>
                        </div>

                        <div class="guides">
                            <img src="<?php echo  asset("$path/bg_image.png") ?>">
                            <div class="guides-content">
                                <div class="icon-guide">
                                    <img src="<?php echo  asset("$path/shopping-cart.png") ?>">
                                </div>
                                <p class="text-guide">áp dụng voucher tại bước thanh
                                    toán</p>
                            </div>
                        </div>

                        <div class="guides">
                            <img src="<?php echo  asset("$path/bg_image.png") ?>">
                            <div class="guides-content">
                                <div class="icon-guide">
                                    <img src="<?php echo  asset("$path/money.png") ?>">
                                </div>
                                <p class="text-guide">áp dụng voucher tại bước thanh
                                    toán</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include("frontend.RefundCoin.huntSuperVoucher")
            @include("frontend.RefundCoin.flashDealRefund")
            @include("frontend.RefundCoin.hotDealDay")
            @include("frontend.RefundCoin.featuredShop")
            @include("frontend.RefundCoin.exclusivelyForYou")
        </div>
    </div>
<!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
<script src="js/RefundCoin/index.js"></script>
<script>
    $(document).ready(function () {
       $('.check-rules').click(function () {
          if ($(this).next().hasClass('active')){
              $(this).next().removeClass('active');
          }else {
              $('.rules').removeClass('active');
              $(this).next().addClass('active');
          }
       });
       let count = $('.slick-voucher-refund').children().length;
       if (count >3){
           $('.slick-voucher-refund').slick({
               slidesToShow: 3,
               slidesToScroll:1,
               infinite: false,
               dots: false
           })
       }
    });
</script>
</body>
<!-- END: Body-->
</html>
