<?php

use Illuminate\Support\Facades\URL;
//    session_start();
//    if (isset($_SERVER['HTTP_REFERER'])){
//        $refPage = $_SERVER['HTTP_REFERER'];
//    }else{
//        $refPage = '/';
//    }
 ?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/home/style.css">
<link rel="stylesheet" type="text/css" href="css/register/register_ncc.css">
<meta name="csrf-token" content="{{ csrf_token() }}"/>
@include('frontend.login.header')
<div id="main"
     style="background-image: url('{{ asset('assets/frontend/Icon/register/bg_image.png')}}'); background-size: cover">
    <div>
        <div class="bg-white form-register" style="width: 40%;">
            <div class="form">
                <h3>Đăng nhập</h3>
                <div class="form-group">
                    <label class="lb-1" for="">Email/ Số điện thoại</label>
                    <div class="phone d-flex">
                        <input type="text" id="username" class="form-control input-phone" placeholder="(+84) 376487429">
                    </div>
                </div>
                <div class="form-group">
                    <label class="lb-1" for="">Mật khẩu</label>
                    <div class="password">
                        <input id="password-field" type="password" class="form-control" name="password"
                               placeholder="******">
                        <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    </div>
                </div>
                <div class="text-center mt-4">
                    <button id="btn-login" type="button" data-url="{{url('login/check_login')}}"
                            class="btn btn-main btn-login btn-register">Đăng nhập
                    </button>
                </div>
                <div class="pt-3">
                    <a class="text-law" href="{{url('forgot_password')}}">Quên mật khẩu</a>
                </div>
                <div class="text-center pt-3">
                    Hoặc đăng nhập bằng
                </div>
                <div class="d-flex justify-content-around mt-3">
                    <div>
                        <a href="">
                            <img src="Icon/register/google.png" width="42px" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="">
                            <img src="Icon/register/facebook.png" width="42px" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="">
                            <img src="Icon/register/zalo.png" width="42px" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="">
                            <img src="Icon/register/line.png" width="42px" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="">
                            <img src="Icon/register/instagram.png" width="42px" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="">
                            <img src="Icon/register/tiktok.png" width="42px" alt="">
                        </a>
                    </div>

                </div>
                <div class="text-center pt-5">
                    Bạn mới biết đến CTV24h? <a class="law" href="{{url('register')}}">Đăng ký ngay</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: Danh Muc-->
<!-- END: Homepage-->

<!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
@include('frontend.base.script')
<script src="js/home/index.js"></script>
<script>
    //enter input
    let input_1 = document.getElementById("password-field");
    input_1.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("btn-login").click();
        }
    });

    let input_2 = document.getElementById("username");
    input_2.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("btn-login").click();
        }
    });
    //end enter
    $(document).ready(function () {
        $(".toggle-password").click(function () {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });

        $(document).on('click', '.btn-login', function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['username'] = $('#username').val();
            data['password'] = $('#password-field').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                beforeSend: function () {
                },
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        swal("Thất bại", data.msg, "error");
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                }
            });
        });
    })
</script>
<!-- END: Body-->
</html>
