<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/home/style.css">
<link rel="stylesheet" type="text/css" href="css/register/register_ncc.css">
<meta name="csrf-token" content="{{ csrf_token() }}"/>
@include('frontend.login.header')
<div id="main"
     style="background-image: url('{{ asset('assets/frontend/Icon/register/background.png')}}'); background-size: cover">
    <div>
        <div class="bg-white form-register" style="width: 40%;">
            <div class="form">
                <h3 style="color: #005DB6" class="mb-4">Đặt lại mật khẩu</h3>
                <div class="form-group">
                    <div class="phone d-flex">
                        <input type="text" id="username" class="form-control input-phone"
                               placeholder="Email/Số điện thoại">
                    </div>
                </div>
                <div class="form-group">
                    <div class="position-relative">
                        <input type="range" min="1" max="100" value="0" class="slider_1" id="myRange">
                        <p class="text-otp position-absolute position-center" style="">Trượt để nhận mã OTP</p>
                    </div>
                </div>
                <div id="recaptcha-container" class="mb-3"></div>
                <div class="form-group">
                    <input type="text" class="form-control form-otp" placeholder="Nhập mã OTP"
                           disabled>
                </div>
                <div class="text-center mt-4 d-flex float-right">
                    <a href="{{url('login')}}">
                        <button type="button"
                                class="btn btn-back btn-back mr-2">Trở lại
                        </button>
                    </a>
                    <button type="button"
                            data-url="{{url('forgot_password/check_phone')}}"
                            class="btn btn-main btn-change btn-register">Hoàn thành
                    </button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- End: Danh Muc-->
<!-- END: Homepage-->

<!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
@include('frontend.base.script')
{{--<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-app.js"></script>--}}

{{--<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->--}}
{{--<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-analytics.js"></script>--}}

{{--<!-- Add Firebase products that you want to use -->--}}
{{--<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-auth.js"></script>--}}
{{--<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-firestore.js"></script>--}}
{{--<script src="js/otp/index.js"></script>--}}
<script>
    $(document).ready(function () {
        $(".toggle-password").click(function () {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
        let verificationId;
        $(document).on('change', '#myRange', async function () {
            let value = $(this).val();
            if (value == 100) {
                $('#recaptcha-container').show();
                let applicationVerifier = new firebase.auth.RecaptchaVerifier(
                    'recaptcha-container');
                let phone = $('.input-phone').val();
                let fs = phone.slice(0, 3);
                if (fs !== '+84') {
                    phone = phone.slice(1, phone.length);
                    phone = '+84' + phone;
                }
                let provider = new firebase.auth.PhoneAuthProvider();
                try {
                    verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                    $('#recaptcha-container').hide();
                    $('.form-otp').prop('disabled', false);
                } catch (e) {
                    console.log(e);
                    alert('Số điện thoại vừa nhập không đúng !');
                    // location.reload();
                }
            }
        });

        $(document).on('click', '.btn-register', async function () {
            try {
                let verificationCode = $('.form-otp').val();
                let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
                try {
                    await firebase.auth().signInWithCredential(phoneCredential);
                    let url = $(this).attr('data-url');
                    let form_data = new FormData();
                    form_data.append('phone', $('#username').val());
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: form_data,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        beforeSend: function () {
                            $('.btn-change').prop('disabled', true);
                        },
                        success: function (data) {
                            if (data.status) {
                                window.location.href = data.url;
                            } else {
                                swal("Thất bại", data.msg, "error");
                            }
                        },
                        error: function () {
                            console.log('error')
                        },
                        complete: function () {
                            $('.btn-change').prop('disabled', false);
                        }
                    });
                } catch (e) {
                    alert('Mã OTP không chính xác');
                }
            } catch (e) {
                alert('Mã OTP không chính xác');
            }
        });
    })
</script>
<!-- END: Body-->
</html>
