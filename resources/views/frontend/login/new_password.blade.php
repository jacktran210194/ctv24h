<?php
$data = Session::get('data_phone');
use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/home/style.css">
<link rel="stylesheet" type="text/css" href="css/register/register_ncc.css">
<meta name="csrf-token" content="{{ csrf_token() }}"/>
@include('frontend.login.header')
<div id="main"
     style="background-image: url('{{ asset('assets/frontend/Icon/register/background.png')}}'); background-size: cover">
    <div>
        <div class="bg-white form-register" style="width: 40%;">
            <div class="form">
                <h3 style="color: #005DB6" class="mb-4">{{$title}}</h3>
                <div class="form-group">
                    <div class="password">
                        <input id="password-field-1" type="password" class="form-control" name=""
                               placeholder="Nhập mật khẩu mới">
                        <span toggle="#password-field-1" class="fa fa-fw fa-eye field-icon toggle-password-1"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="password">
                        <input id="password-field-2" type="password" class="form-control" name=""
                               placeholder="Nhập lại mật khẩu mới">
                        <span toggle="#password-field-2" class="fa fa-fw fa-eye field-icon toggle-password-2"></span>
                    </div>
                </div>
                <div class="text-center mt-4 d-flex float-right">
                    <a href="{{url('forgot_password')}}">
                        <button type="button"
                                class="btn btn-back btn-back mr-2">Trở lại
                        </button>
                    </a>
                    <button type="button"
                            data-url="{{url('forgot_password/new_password/save')}}"
                            data-phone="{{isset($data) ? $data : ''}}"
                            class="btn btn-main btn-save btn-register">Hoàn thành
                    </button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- End: Danh Muc-->
<!-- END: Homepage-->

<!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
@include('frontend.base.script')
<script>
    $(document).ready(function () {
        $(".toggle-password-1").click(function () {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });

        $(".toggle-password-2").click(function () {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });

        $(document).on('click', '.btn-save', function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('password', $('#password-field-1').val());
            form_data.append('re_password', $('#password-field-2').val());
            form_data.append('check_phone', $(this).attr('data-phone'));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        swal("Thành công", data.msg, "success");
                        window.location.href = data.url;
                    } else {
                        swal("Thất bại", data.msg, "error");
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });
    })
</script>
</body>
<!-- END: Body-->
</html>
