<?php
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/my_auction/auction_details.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="container">
        <div class="turn-1">
            <div class="content-gallery-product d-flex pt-4 pl-3">
                <div class="img-product-small">
                    <div class="img-1">
                        <img src="Icon/chitietdaugia/prd1.png" alt="">
                    </div>
                    <div class="img-1">
                        <img src="Icon/chitietdaugia/prd2.png" alt="">
                    </div>
                    <div class="img-1">
                        <img src="Icon/chitietdaugia/prd3.png" alt="">
                    </div>
                    <div class="img-1">
                        <img src="Icon/chitietdaugia/prd4.png" alt="">
                    </div>
                </div>
                <div class="img-product-large">
                    <div class="img-5">
                        <img src="Icon/chitietdaugia/prd5.png" alt="">
                    </div>
                </div>
                <div class="title pr-4 pl-5 pt-3">
                    <span class="title-1">LOVIIE BASICS- Giày Sandal Đế Bệt Dây Mảnh 2cm- Sandal 2cm Quai Mảnh Dây Chéo Xỏ Ngón Đen - Kem - Trắng - Sandal 2P-S200518</span>
                    <div class="d-flex tab-bar-1 justify-content-center">
                        <div class="bar-1 col-lg-4 mr-3 pl-4 align-items-center">
                            <center>
                                <div class="d-flex pt-3 align-items-center">
                                    <img class="bar-1-img" src="Icon/chitietdaugia/bua.svg">
                                    <span class="bar-1-text">Số Lượt Đấu Giá</span>
                                </div>
                               <div>
                                   <p>07</p>
                               </div>
                            </center>
                        </div>
                        <div class="bar-1 col-lg-4 mr-3 pl-4 align-items-center">
                            <center>
                                <div class="d-flex pt-3 align-items-center">
                                    <img class="bar-1-img" src="Icon/chitietdaugia/time.svg">
                                    <span class="bar-1-text">Thời gian còn lại</span>
                                </div>
                                <div id="DigitalCLOCK" class="clock" onload="showTime()">
                                    <div id="showtime-h" class="showtime"></div>
                                    <div id="showtime-m" class="showtime"></div>
                                    <div id="showtime-s" class="showtime"></div>
                                </div>
                            </center>
                        </div>
                        <div class="bar-1 col-lg-4 pl-4 align-items-center">
                            <center>
                                <div class="d-flex pt-3 pl-4 align-items-center">
                                    <img class="bar-1-img" src="Icon/chitietdaugia/time.svg">
                                    <span class="bar-1-text">Bước giá</span>
                                </div>
                                <div>
                                    <p>2.000</p>
                                </div>
                            </center>
                        </div>
                    </div>
                    <div class="d-flex tab-bar-2 justify-content-center">
                        <div class="bar-2-1 col-lg-6 d-flex">
                            <center>
                                <img class="img-coin" src="Icon/chitietdaugia/coin.svg">
                                <span class="tab-bar-2-text">Giá Hiện Tại:</span>
                                <span class="tab-bar-2-number">120.000</span>
                                <span class="tab-bar-2-d">đ</span>
                            </center>
                        </div>
                        <div class="bar-2 col-lg-6">
                            <center>
                                <img class="img-coin" src="Icon/chitietdaugia/coin.svg">
                                <span class="tab-bar-2-text">Giá Mua Ngay:</span>
                                <span class="tab-bar-2-number">156.000</span>
                                <span class="tab-bar-2-d">đ</span>
                            </center>
                        </div>
                    </div>
                    <div class="d-flex tab-bar-3 mt-4">
                        <div>
                            <span class="text-transport">Vận chuyển</span>
                        </div>
                        <div class="pl-5">
                            <p>Miễn phí vận chuyển : Đơn hàng trên 200.000đ</p>
                            <p>Miễn phí tối đa : 30.000đ</p>
                        </div>
                    </div>
                    <div class="d-flex tab-bar-4">
                        <div class="bar-3 col-lg-4 mr-3 pt-2 pb-2">
                            <center>
                                <a class="bar-3-text" href="">Đấu Giá</a>
                            </center>
                        </div>
                        <div class="bar-3 col-lg-4 ml-3 pt-2 pb-2">
                            <center>
                                <a class="bar-3-text" href="">Mua Ngay</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive-lg pl-2 pr-2 pt-3 pb-4">
                <table class="turn-1-tb">
                    <thead>
                        <tr class="turn-1-tr-head">
                            <td width="30%" class="td-top">Hồ sơ</td>
                            <td width="20%" class="td-top">Giá thầu</td>
                            <td width="20%" class="td-top">Số lượng</td>
                            <td width="30%" class="td-top">Ngày giờ</td>
                        </tr>
                    </thead>
                    <tbody class="bd-bd">
                        <tr class="turn-1-tr-body-y">
                            <td class="td-bt-cup">
                                <img src="Icon/chitietdaugia/cup.png" alt="">
                                <span class="td-bt">Vũ Thị Phượng</span>
                            </td>
                            <td class="td-bt">300.000đ</td>
                            <td class="td-bt">1</td>
                            <td class="td-bt">10-11-2020 16:10:55</td>
                        </tr>
                        <tr class="turn-1-tr-body-w">
                            <td class="td-bt">Vũ Thị Phượng</td>
                            <td class="td-bt">300.000đ</td>
                            <td class="td-bt">1</td>
                            <td class="td-bt">10-11-2020 16:10:55</td>
                        </tr>
                        <tr class="turn-1-tr-body-y">
                            <td class="td-bt">Vũ Thị Phượng</td>
                            <td class="td-bt">300.000đ</td>
                            <td class="td-bt">1</td>
                            <td class="td-bt">10-11-2020 16:10:55</td>
                        </tr>
                        <tr class="turn-1-tr-body-w">
                            <td class="td-bt">Vũ Thị Phượng</td>
                            <td class="td-bt">300.000đ</td>
                            <td class="td-bt">1</td>
                            <td class="td-bt">10-11-2020 16:10:55</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="turn-2 mt-4 pb-2">
            <div class="turn-2-1">
                <div class="d-flex">
                    <div class="pt-2 pl-4 pr-2">
                        <div class="p-1">
                            <img src="Icon/chitietdaugia/img_shop.png" alt="">
                        </div>
                        <div>
                            <img class="turn-2-shop" src="Icon/chitietdaugia/shopyeuthich.png" alt="">
                        </div>
                    </div>
                    <div class="ml-3 mt-3">
                        <div>
                            <span class="turn-2-1-text-1">Quần Jean 1hit shop</span>
                        </div>
                        <div class="mt-2">
                            <span class="turn-2-1-text-2">Online 10 phút trươc </span>
                        </div>
                        <div class="turn-2-button d-flex justify-content-around w-100 mt-2 ml-2">
                            <div class="button-1 col-lg-6 pl-3 pt-2 pb-2 mr-3">
                                <center>
                                    <img src="Icon/chitietdaugia/chatngay.svg">
                                    <a class="button-1-chat" href="">Chat Ngay</a>
                                </center>
                            </div>
                            <div class="button-2 col-lg-6 pl-3 pt-2 pb-2">
                                <center>
                                    <img src="Icon/chitietdaugia/xemshop.svg">
                                    <a class="button-1-view" href="">Xem Shop</a>
                                </center>
                            </div>
                        </div>
                    <div>
                </div>
            </div>

            <div class="turn-2-2">
                <div>
                    <img src="Icon/chitietdaugia/clothes.png" alt="">
                    <span class="turn-2-2-text-sp">72</span>
                    <p class="pt-3">Sản Phẩm</p>
                </div>
            </div>

            <div class="turn-2-3">
                <div>
                    <img class="turn-2-3-img" src="Icon/chitietdaugia/star.png" alt="">
                    <span class="turn-2-3-text-dg">4.9</span>
                    <p class="pt-2">Đánh Giá</p>
                </div>
            </div>

            <div class="turn-2-4">
                <div>
                    <img class="turn-2-4-img" src="Icon/chitietdaugia/chat.png" alt="">
                    <span class="turn-2-4-text-ph">98%</span>
                    <p class="pt-2">Tỉ Lệ Phản Hồi</p>
                </div>
            </div>

            <div class="turn-2-5">
                <div>
                    <img class="turn-2-5-img" src="Icon/chitietdaugia/clock.png" alt="">
                    <span class="turn-2-5-text-tg">Trong vài giờ</span>
                    <p class="pt-2">Thời Gian Phản Hồi</p>
                </div>
            </div>

        </div>
    </div>
        </div>
        <div class="turn-3 mt-4 mb-3 pb-4">
            <div class="turn-3-title pt-4 pb-2">
                <h4 class="mr-5 ml-5">CHI TIẾT SẢN PHẨM</h4>
            </div>
            <div class="mr-5 ml-5 mt-4">
                <h5>Thiết kế nhỏ gọn</h5>
                <span class="turn-3-text-1">Cáp Remax RC-024i lấy cảm hứng thiết kế từ dây chìa khóa tiện dụng, đa năng. Nhìn ngoài sản phẩm giống như chìa khóa nhỏ, giúp bạn có thể dễ dàng mang theo bất cứ đâu, thời điểm nào.</span>
            </div>
            <div class="mr-5 ml-5 mt-4">
                <span class="turn-3-text-1">Đặc biệt, bạn có thể tháo rời đoạn lõi thẳng giữa các dây chuyền từ chìa khóa khi sử dụng.</span>
            </div>
            <div class="mr-5 ml-5 mt-4">
                <h5>Cao cấp chất lượng</h5>
                <span class="turn-3-text-1">Cáp RC-024i được  thiết kế từ đồng nguyên chất, không pha tạp với bất kỳ loại kim loại nào, làm giảm tính năng của sản phẩm. Nhờ this thiết kế chất lượng giúp cho đường truyền dữ liệu được truyền với tốc độ hơi cao, không đạt được hiệu quả tốt nhất.</span>
            </div>
            <div class="mr-5 ml-5 mt-4">
                <span class="turn-3-text-1">No just have a small small metery , lợi ích, có thể sử dụng mục tiêu linh hoạt mà không có  cáp RC-024i  là sản phẩm tích hợp hai chức năng là pin và cáp chuyển dữ liệu rất tiện lợi.</span>
            </div>
            <div class="mr-5 ml-5 mt-4">
                <span class="turn-3-text-1">Cáp Lightning Remax RC-024i đặc biệt tương thích với dòng sản phẩm iphone từ 5 trở lên với dữ liệu truyền dẫn tốc độ và không ghim vô cùng nhanh, an toàn cho thiết bị.</span>
            </div>
        </div>

        <div class="section-dau-gia mb-5"   >
                <?php
                $path = "assets/frontend/Icon/home/Danh_muc";
                ?>
                <div class="title-section pt-4">
                    <div class="content">
                        <h2>SẢN PHẨM ĐẤU GIÁ LIÊN QUAN</h2>
                    </div>
                </div>
                <div class="product-section pb-5">
                    <div class="section-product d-flex">
                        <?php for ($i = 0; $i < 8; $i++): ?>
                            <div class="col-lg-2 p-1 product-card">
                                <img src="<?php echo asset("$path/dau_gia_image.png")?>" width="100%">
                                <img class="product-img-1 pr-1 pt-1" src="<?php echo asset("$path/dau_gia_icon_1.png")?>">
                                <img class="product-img-2 pr-2 pt-2" src="<?php echo asset("$path/dau_gia_icon_2.png")?>">
                                <div class="information-product">
                                    <div class="product-title">
                                        <span>Áo thun nữ</span>
                                    </div>
                                    <div class="information">
                                        <div class="text-information">
                                            <span>Giá Khởi Điểm:</span>
                                            <span>Thầu Hiện Tại:</span>
                                            <span>Mua Ngay:</span>
                                            <span>Số Người Đấu Giá:</span>
                                            <span>Bước giá:</span>
                                            <span>Thời gian còn lại:</span>
                                        </div>
                                        <div class="price-information">
                                            <span>1.000</span>
                                            <span>30.000</span>
                                            <span>37.000</span>
                                            <span>10</span>
                                            <span>2.000</span>
                                            <span>10:00:00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>
                    </div>
                </div>
            </div>


    </div>

    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>



@include('frontend.base.script')
<script src="js/auction/index.js"></script>
<script src="js/my_auction/index.js"></script>
</body>
<!-- END: Body-->
</html>
