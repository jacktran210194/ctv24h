<?php
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/my_auction/auction.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="container">
        <div class="main mt-3">
            <div class="header d-flex">
                <div class="col-lg-3 p-0">
                    <div>
                        <div class="float-left align-items-center">
                            <span class="intro">Chào mừng bạn đến với CTV24h</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 d-flex flex-column p-0">
                    <div>
                        <div class="float-left pr-4">
                            <span>Tham gia cộng đồng kiếm tiền mua bán hộ</span>
                        </div>
                        <div class="float-left">
                            <a href="" class="suggest">
                                <span class="btn-suggest">Gợi ý dành cho CTV</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 p-0">
                    <div class="d-flex float-right">
                        <div class="float-right">
                            <a href="" class="login">
                                <span class="btn-login">Đăng ký</span>
                            </a>
                        </div>
                        <div class="float-right pl-4">
                            <a href="" class="register">
                                <span class="btn-register">Đăng nhập</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="title pt-3 ">
                <div class="float-left">
                    <span style="font-size: 26px; font-weight: bold;" class="my-auction">Đấu giá của tôi</span>
                </div>
            </div>

            <div class="body mt-5 d-flex">
                <div class="notification mr-4 pb-3">
                    <div class="col-lg-12 pt-5">
                        <div class="align-items-center pb-4">
                            <span class="text-title">Tìm kiếm sản phẩm theo từ khóa</span>
                        </div>
                        <div>
                            <span class="text-desc">Bạn có thể tìm kiếm sản phẩm theo từ khóa, ví dụ: “tên sản phẩm”. Bạn cũng có thể tìm kiếm bằng cách kết hợp nhiều từ khóa hoặc bằng cách kết hợp các tiêu chí như giá hiện tại.</span>
                        </div>
                    </div>

                    <div class="col-lg-12 pt-4">
                        <div class="align-items-center pb-4">
                            <span class="text-title">Tìm kiếm sản phẩm theo danh mục</span>
                        </div>
                        <div>
                            <span class="text-desc">Bạn có thể tìm kiếm qua danh mục, ví dụ: “Điện thoại”. Bạn cũng có thể tìm kiếm bằng cách kết hợp nhiều từ khóa hoặc bằng cách kết hợp các tiêu chí như giá hiện tại.</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 p-0 ml-2">
                    <div class="float-right">
                        <img src="Icon/chitietdaugia/slide.svg" alt="">
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
