<?php
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/my_auction/guide.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="container">
        <div class="main mt-3">
            <div class="header d-flex">
                <div class="col-lg-3">
                    <div>
                        <div class="float-left align-items-center">
                            <span class="intro">Chào mừng bạn đến với CTV24h</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 d-flex flex-column p-0">
                    <div>
                        <div class="float-left pr-4">
                            <span>Tham gia cộng đồng kiếm tiền mua bán hộ</span>
                        </div>
                        <div class="float-left">
                            <a href="" class="suggest">
                                <span class="btn-suggest">Gợi ý dành cho CTV</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 p-0">
                    <div class="d-flex float-right">
                        <div class="float-right">
                            <a href="" class="login">
                                <span class="btn-login">Đăng ký</span>
                            </a>
                        </div>
                        <div class="float-right pl-4">
                            <a href="" class="register">
                                <span class="btn-register">Đăng nhập</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="body d-flex pt-4">
                <div class="col-lg-8 mr-3">
                    <div class="">
                        <span class="text-title">Đấu giá của tôi</span>
                    </div>
                    <div class="buy mt-3 pt-3">
                        <div class="text-buy pt-3 pl-3">
                            <span>Mua</span>
                        </div>
                        <div class="col-lg-12">
                            <div class="d-flex pt-2 pl-4 pr-4 justify-content-around w-100">
                                <div class="button p-2 mr-3 col-lg-3  <?= $type === 'follow' ? 'checked' : '' ?>">
                                    <center>
                                        <a href="<?= URL::to('auction?type=follow') ?>">Danh sách theo dõi</a>
                                    </center>
                                </div>
                                <div class="button p-2 mr-3 col-lg-3  <?= $type === 'going' ? 'checked' : '' ?>">
                                    <center>
                                        <a href="<?= URL::to('auction?type=going') ?>">Đang đấu giá</a>
                                    </center>
                                </div>
                                <div class="button p-2 mr-3 col-lg-3  <?= $type === 'done' ? 'checked' : '' ?>">
                                    <center>
                                        <a href="<?= URL::to('auction?type=done') ?>">Đã đấu giá</a>
                                    </center>
                                </div>
                                <div class="button p-2 col-lg-3  <?= $type === 'success' ? 'checked' : '' ?>">
                                    <center>
                                        <a href="<?= URL::to('auction?type=success') ?>">Đấu giá thành công</a>
                                    </center>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 pt-4 pl-3">
                            <div class="text-buy">
                                <span>Bán</span>
                            </div>
                            <div class="d-flex pt-2 pl-4 pr-4 pb-5 justify-content-around w-100">
                                <div class="button p-2 mr-4 col-lg-4  <?= $type === 'follow' ? 'checked' : '' ?>">
                                    <center>
                                        <a href="">Triển lãm</a>
                                    </center>
                                </div>
                                <div class="button p-2 mr-4 col-lg-4  <?= $type === 'going' ? 'checked' : '' ?>">
                                    <center>
                                        <a href="">Danh sách kết thúc</a>
                                    </center>
                                </div>
                                <div class="button p-2 col-lg-4  <?= $type === 'success' ? 'checked' : '' ?>">
                                    <center>
                                        <a href="">Tạo đấu giá</a>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4">
                        <span class="text-title-2">Hướng dẫn sử dụng</span>
                    </div>
                    <div class="map mt-3 pt-3 pr-3">
                        <center>
                            <div class="pl-2">
                                <img src="Icon/chitietdaugia/map.png">
                            </div>
                        </center>
                        <div class="col-lg-12">
                            <div class="text-1">
                                <span>Trước khi đấu giá</span>
                            </div>
                            <div class="text-2 mt-4">
                                <span>Nếu bạn tìm thấy sản phẩm mình thích, hãy kiểm tra trang sản phẩm để biết thêm thông tin.</span>
                            </div>
                            <div class="content d-flex">
                                <div class="col-lg-6  p-0 left-content">
                                    <div class="text-3">
                                        <span>1. Uy tín người bán đấu giá</span>
                                    </div>
                                    <div class="text-4">
                                        <span>Hãy xác định những người bạn có thể giao dịch với sự an tâm bằng cách xem xét số lượng đánh giá và nội dung của đánh giá.</span>
                                    </div>
                                    <div class="text-3">
                                        <span>2. Thông tin đấu giá</span>
                                    </div>
                                    <div class="text-4">
                                        <span>Hãy xác định những người bạn có thể giao dịch với sự an tâm bằng cách xem xét số lượng đánh giá và nội dung của đánh giá.</span>
                                    </div>
                                    <div class="text-3">
                                        <span>3. Thông tin về sản phẩm</span>
                                    </div>
                                    <div class="text-4">
                                        <span>Bạn có thể xem nội dung mô tả của sản phẩm, tình trạng, nhãn hiện, v.v.</span>
                                    </div>
                                    <div class="text-3">
                                        <span>4. Hình ảnh sản phẩm</span>
                                    </div>
                                    <div class="text-4">
                                        <span>Hình ảnh của sản phẩm được đăng tải lên mô tả tình trạng, số lượng, phụ kiện, v.v</span>
                                    </div>
                                    <div class="text-4">
                                        <span>Lưu ý: Nếu ảnh bị nhòe hoặc quá nhỏ để kiểm tra tình trạng sản phẩm, hãy cẩn thận trước khi đấu giá sản phẩm!</span>
                                    </div>
                                </div>
                                <div class="col-lg-6 p-0 right-content">
                                    <div class="image1 float-right ml-5">
                                        <img src="Icon/chitietdaugia/guide1.png">

                                    </div>
                                </div>
                            </div>
                            <div class="text-5">
                                <span>Quá trình đấu giá</span>
                            </div>
                            <center>
                                <div class="box-1 justify-content-center p-3 ml-3 mt-4">
                                    <span class="text-6">Bạn không thể hủy giá thầu của mình. Hãy chỉ đặt giá thầu trên các phiên đấu giá mà bạn sẵn sàng mua.</span>
                                </div>
                            </center>
                            <div class="col-lg-12 p-0">
                                <div class="text-3 mt-5">
                                    <span>1. Nhập số tiền</span>
                                </div>
                                <div class="text-4">
                                    <span>Nhấn vào nút “Đấu giá” trên trang sản phẩm để hiển thị màn hình đấu giá, hãy nhập số tiền bạn có thể thanh toán cho sản phẩm và nhấn nút “Đấu giá”.</span>
                                </div>
                                <div class="mb-4">
                                    <img src="Icon/chitietdaugia/guide2.png">
                                </div>
                                <div class="text-4 pb-4">
                                    <span>Hệ thống sẽ tự động đặt giá (Đấu giá tự động) trong phạm vi bước giá gần nhất so với giá đấu cao nhất!</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 p-0 mt-5">
                    <div class="float-right">
                        <img src="Icon/chitietdaugia/slide.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
