<?php
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/my_auction/auction.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="container">
        <div class="main mt-3">
            <div class="header d-flex">
                <div class="col-lg-3 p-0">
                    <div>
                        <div class="float-left align-items-center">
                            <span class="intro">Chào mừng bạn đến với CTV24h</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 d-flex flex-column p-0">
                    <div>
                        <div class="float-left pr-4">
                            <span>Tham gia cộng đồng kiếm tiền mua bán hộ</span>
                        </div>
                        <div class="float-left">
                            <a href="" class="suggest">
                                <span class="btn-suggest">Gợi ý dành cho CTV</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 p-0">
                    <div class="d-flex float-right">
                        <div class="float-right">
                            <a href="" class="login">
                                <span class="btn-login">Đăng ký</span>
                            </a>
                        </div>
                        <div class="float-right pl-4">
                            <a href="" class="register">
                                <span class="btn-register">Đăng nhập</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="title pt-3 ">
                <div class="float-left">
                    <span style="font-size: 26px; font-weight: bold;" class="my-auction">Đấu giá của tôi</span>
                </div>
            </div>

            <div class="body mt-5 d-flex">
                <div class="buy mr-4 pb-3">
                    <div class="col-lg-12 pt-5">
                        <div class="text-buy">
                            <div class="bookmark-wrapper p-0">
                                <span>Mua</span>
                            </div>
                        </div>
                        <div class="d-flex pt-2 pl-4 pr-4 justify-content-around w-100">
                            <div class="button p-2 mr-3 col-lg-3  <?= $type === 'follow' ? 'checked' : '' ?>">
                                <center>
                                    <a href="<?= URL::to('auction?type=follow') ?>">Danh sách theo dõi</a>
                                </center>
                            </div>
                            <div class="button p-2 mr-3 col-lg-3  <?= $type === 'going' ? 'checked' : '' ?>">
                                <center>
                                    <a href="<?= URL::to('auction?type=going') ?>">Đang đấu giá</a>
                                </center>
                            </div>
                            <div class="button p-2 mr-3 col-lg-3  <?= $type === 'done' ? 'checked' : '' ?>">
                                <center>
                                    <a href="<?= URL::to('auction?type=done') ?>">Đã đấu giá</a>
                                </center>
                            </div>
                            <div class="button p-2 col-lg-3  <?= $type === 'success' ? 'checked' : '' ?>">
                                <center>
                                    <a href="<?= URL::to('auction?type=success') ?>">Đấu giá thành công</a>
                                </center>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 pt-5">
                        <div class="text-buy">
                            <div class="bookmark-wrapper p-0">
                                <span>Bán</span>
                            </div>
                        </div>
                        <div class="d-flex pt-2 pl-4 pr-4 justify-content-around w-100">
                            <div class="button p-2 mr-4 col-lg-4  <?= $type === 'follow' ? 'checked' : '' ?>">
                                <center>
                                    <a href="">Triển lãm</a>
                                </center>
                            </div>
                            <div class="button p-2 mr-4 col-lg-4  <?= $type === 'going' ? 'checked' : '' ?>">
                                <center>
                                    <a href="">Danh sách kết thúc</a>
                                </center>
                            </div>
                            <div class="button p-2 col-lg-4  <?= $type === 'success' ? 'checked' : '' ?>">
                                <center>
                                    <a href="">Tạo đấu giá</a>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 p-0 ml-2">
                    <div class="float-right">
                        <img src="Icon/chitietdaugia/slide.svg" alt="">
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
