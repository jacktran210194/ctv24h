<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/chat/style.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="container">
        <div class="d-flex mb-5">
            <div class="col-lg-3 list-chat">
                <b class="title-chat pl-3">CHAT</b>
                <div class="view-chat align-items-center pl-3 pr-3">
                    <input class="form-control form-search-chat" placeholder="Tìm Kiếm Khách Hàng">
                    <object class="icon-chat" data="Icon/chat/icon_search.svg"></object>
                </div>
                <div class="mt-5 box-messenger">
                    <?php foreach ($list_mes as $value): ?>
                    <div class="item-chat-<?= $value['id'] ?> item-chat-data">
                        <a href="<?= URL::to('chat/messenger/' . $value['id']) ?>">
                            <div class="d-flex align-items-center p-3 <?= $value['active'] ? 'active' : 'inactive' ?>">
                                <div class="col-lg-3">
                                    <div class="avatar">
                                        <img
                                            src="<?= !empty($value['avt_customer']) ? $value['avt_customer'] : 'Icon/chat/avarta.png' ?>">
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="pl-2 align-items-center">
                                        <p class="name m-0 mb-2"><?= $value['name_customer'] ?></p>
                                        <p class="messenger messenger-last-<?= $value['id'] ?> number-line-1 m-0"><?= $value['messenger'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-lg-6 ml-3 mr-3">
                <?php if (count($list_mes)) : ?>
                <div class="d-flex align-items-center p-3 messenger-people customer-active pl-5"
                     data-group-id="<?= $group_messenger_id ?? '' ?>" data-id="<?= $id_customer_active ?? '' ?>">
                    <div class="avatar">
                        <img src="<?= !empty($avt_customer_active) ? $avt_customer_active : 'Icon/chat/avarta.png' ?>">
                    </div>
                    <div class="pl-3 align-items-center">
                        <p class="name m-0"><?= $name_customer_active ?? '' ?></p>
                    </div>
                </div>
                <div class="mt-2 list-messenger list-messenger-<?= $group_messenger_id ?? '' ?>">
                    <?php foreach ($list_messenger as $value) : ?>
                    <?php if ($value['active']) : ?>
                    <div class="d-flex align-items-center mt-3">
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-9 p-0 d-flex justify-content-end">
                            <div class="box-chat box-my p-3">
                                <span><?= $value['messenger'] ?></span>
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <img
                                src="<?= !empty($value['user_send_avt']) ? $value['user_send_avt'] : 'Icon/chat/avarta.png' ?>"
                                class="img-avt-chat">
                        </div>
                    </div>
                    <?php else: ?>
                    <div class="d-flex align-items-center mt-3">
                        <div class="col-lg-1">
                            <img
                                src="<?= !empty($value['user_receiver_avt']) ? $value['user_receiver_avt'] : 'Icon/chat/avarta.png' ?>"
                                class="img-avt-chat">
                        </div>
                        <div class="col-lg-9 ml-3 mr-3">
                            <div class="box-chat box-guest p-3">
                                <span><?= $value['messenger'] ?></span>
                            </div>
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                <div class="align-items-center mt-3">
                    <div class="d-flex justify-content-center">
                        <span class="btn text-suggest text-pick">Kiểm Tra Hàng</span>
                        <span class="btn text-suggest text-info">Khách Sỉ</span>
                        <span class="btn text-suggest text-warning">Đã Mua Hàng</span>
                        <span class="btn text-suggest text-purple">Đã Tư Vấn</span>
                        <span class="btn text-suggest text-success">Cần chăm sóc</span>
                    </div>
                    <div class="d-flex messenger-send">
                        <div class="col-lg-6 pl-2 pr-2 align-items-center d-flex">
                            <input class="form-control form-send-messenger" id="form-send-messenger" placeholder="Gửi tin nhắn">
                        </div>
                        <div
                            class="col-lg-6 d-flex flex-row-reverse p-0 justify-content-between align-items-center">
                            <div id="btn-send-messenger" class="btn btn-send-messenger" data-url="<?= URL::to('chat/send') ?>">
                                <object data="Icon/chat/send_mes.svg"></object>
                            </div>
                            <object data="Icon/chat/send_note.svg"></object>
                            <object data="Icon/chat/send_product.svg"></object>
                            <object data="Icon/chat/send_Icon.svg"></object>
                            <object data="Icon/chat/send_image.svg"></object>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="col-lg-3 list-product p-0">
                <div class="title-product d-flex justify-content-center align-items-center pt-3 pb-3">
                    SẢN PHẨM
                </div>
                <div class="view-chat align-items-center pl-3 pr-3">
                    <input class="form-control form-search-chat" placeholder="Tìm Kiếm">
                    <object class="icon-chat" data="Icon/chat/icon_search.svg"></object>
                </div>
                <div class="mt-5 box-product">
                    <div class="mb-3">
                        <div class="d-flex align-items-center p-2">
                            <div class="col-lg-3 d-flex align-items-center">
                                <img src="Icon/cart/image_product.png" class="img-product-suggest">
                            </div>
                            <div class="col-lg-9 pl-3 align-items-center">
                                <p class="name-product m-0 mb-2">Áo khoác nữ lót lông hottren 2020, chuẩn hàng quảng
                                    châu loại 1 sẵn DN</p>
                                <p class="price m-0">700.000đ</p>
                            </div>
                        </div>
                        <div class="col-lg-12 d-flex mt-3 align-items-center">
                            <div class="col-lg-9">
                                <span class="quantity-product">0 có sẵn | 12 đã bán </span>
                            </div>
                            <div class="col-lg-3 d-flex justify-content-end">
                                <button class="btn btn-send">Gửi</button>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="d-flex align-items-center p-2">
                            <div class="col-lg-3 d-flex align-items-center">
                                <img src="Icon/cart/image_product.png" class="img-product-suggest">
                            </div>
                            <div class="col-lg-9 pl-3 align-items-center">
                                <p class="name-product m-0 mb-2">Áo khoác nữ lót lông hottren 2020, chuẩn hàng quảng
                                    châu loại 1 sẵn DN</p>
                                <p class="price m-0">700.000đ</p>
                            </div>
                        </div>
                        <div class="col-lg-12 d-flex mt-3 align-items-center">
                            <div class="col-lg-9">
                                <span class="quantity-product">0 có sẵn | 12 đã bán </span>
                            </div>
                            <div class="col-lg-3 d-flex justify-content-end">
                                <button class="btn btn-send">Gửi</button>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="d-flex align-items-center p-2">
                            <div class="col-lg-3 d-flex align-items-center">
                                <img src="Icon/cart/image_product.png" class="img-product-suggest">
                            </div>
                            <div class="col-lg-9 pl-3 align-items-center">
                                <p class="name-product m-0 mb-2">Áo khoác nữ lót lông hottren 2020, chuẩn hàng quảng
                                    châu loại 1 sẵn DN</p>
                                <p class="price m-0">700.000đ</p>
                            </div>
                        </div>
                        <div class="col-lg-12 d-flex mt-3 align-items-center">
                            <div class="col-lg-9">
                                <span class="quantity-product">0 có sẵn | 12 đã bán </span>
                            </div>
                            <div class="col-lg-3 d-flex justify-content-end">
                                <button class="btn btn-send">Gửi</button>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="d-flex align-items-center p-2">
                            <div class="col-lg-3 d-flex align-items-center">
                                <img src="Icon/cart/image_product.png" class="img-product-suggest">
                            </div>
                            <div class="col-lg-9 pl-3 align-items-center">
                                <p class="name-product m-0 mb-2">Áo khoác nữ lót lông hottren 2020, chuẩn hàng quảng
                                    châu loại 1 sẵn DN</p>
                                <p class="price m-0">700.000đ</p>
                            </div>
                        </div>
                        <div class="col-lg-12 d-flex mt-3 align-items-center">
                            <div class="col-lg-9">
                                <span class="quantity-product">0 có sẵn | 12 đã bán </span>
                            </div>
                            <div class="col-lg-3 d-flex justify-content-end">
                                <button class="btn btn-send">Gửi</button>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="d-flex align-items-center p-2">
                            <div class="col-lg-3 d-flex align-items-center">
                                <img src="Icon/cart/image_product.png" class="img-product-suggest">
                            </div>
                            <div class="col-lg-9 pl-3 align-items-center">
                                <p class="name-product m-0 mb-2">Áo khoác nữ lót lông hottren 2020, chuẩn hàng quảng
                                    châu loại 1 sẵn DN</p>
                                <p class="price m-0">700.000đ</p>
                            </div>
                        </div>
                        <div class="col-lg-12 d-flex mt-3 align-items-center">
                            <div class="col-lg-9">
                                <span class="quantity-product">0 có sẵn | 12 đã bán </span>
                            </div>
                            <div class="col-lg-3 d-flex justify-content-end">
                                <button class="btn btn-send">Gửi</button>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="d-flex align-items-center p-2">
                            <div class="col-lg-3 d-flex align-items-center">
                                <img src="Icon/cart/image_product.png" class="img-product-suggest">
                            </div>
                            <div class="col-lg-9 pl-3 align-items-center">
                                <p class="name-product m-0 mb-2">Áo khoác nữ lót lông hottren 2020, chuẩn hàng quảng
                                    châu loại 1 sẵn DN</p>
                                <p class="price m-0">700.000đ</p>
                            </div>
                        </div>
                        <div class="col-lg-12 d-flex mt-3 align-items-center">
                            <div class="col-lg-9">
                                <span class="quantity-product">0 có sẵn | 12 đã bán </span>
                            </div>
                            <div class="col-lg-3 d-flex justify-content-end">
                                <button class="btn btn-send">Gửi</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
<script src='js/chat/index.js'></script>
<script src="{{ asset('js/app.js') }}"></script>

<script>
    channel.bind('my-event', function (data) {
        let length = $('.item-chat-' + data.message.group_id ).length;
        if (length > 0) {
            let group_id = $('.customer-active').attr('data-group-id');
            if (group_id == data.message.group_id) {
                $('.list-messenger-' + data.message.group_id).append('<div class="d-flex align-items-center mt-3">\n' +
                    '                        <div class="col-lg-1">\n' +
                    '                            <img src="' + data.message.avt_customer + '" class="img-avt-chat">\n' +
                    '                        </div>\n' +
                    '                        <div class="col-lg-9 ml-3 mr-3">\n' +
                    '                            <div class="box-chat box-guest p-3">\n' +
                    '                                <span>' + data.message.messenger + '</span>\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '                        <div class="col-lg-2"></div>\n' +
                    '                    </div>');
                $('.list-messenger-' + data.message.group_id).animate({
                    scrollTop: $('.list-messenger-' + data.message.group_id)[0].scrollHeight
                }, 0);
            }
            $('.messenger-last-' + data.message.group_id).text(data.message.messenger);
            let html = $('.item-chat-' + data.message.group_id).html();
            if (!html) {
                html = '<a href="' + data.message.url_link + '">\n' +
                    '                    <div class="d-flex align-items-center p-3 inactive">\n' +
                    '                    <div class="col-lg-3">\n' +
                    '                    <div class="avatar">\n' +
                    '                    <img src="' + data.message.avt_customer + '">\n' +
                    '                    </div>\n' +
                    '                    </div>\n' +
                    '                    <div class="col-lg-9">\n' +
                    '                    <div class="pl-2 align-items-center">\n' +
                    '                    <p class="name m-0 mb-2">' + data.message.name_customer + '</p>\n' +
                    '                    <p class="messenger messenger-last-' + data.message.group_id + ' number-line-1 m-0">' + data.message.messenger + '</p>\n' +
                    '                    </div>\n' +
                    '                    </div>\n' +
                    '                    </div>\n' +
                    '                    </a>'
            }
            html = '<div class="item-chat-' + data.message.group_id + '">' + html + '</div>';
            $('.item-chat-' + data.message.group_id).remove();
            $(".box-messenger").prepend(html);
        }
    });

    //enter input
    let input = document.getElementById("form-send-messenger");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("btn-send-messenger").click();
        }
    });
    //end enter

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('click', '.btn-send-messenger', function () {
            let mes = $('.form-send-messenger').val();
            if (mes) {
                let avt = '{{$my_avt ?? ''}}';
                $('.form-send-messenger').val('');
                $('.list-messenger-{{$group_messenger_id}}').append('<div class="d-flex align-items-center mt-3">\n' +
                    '                        <div class="col-lg-2">\n' +
                    '                        </div>\n' +
                    '                        <div class="col-lg-9 p-0 d-flex justify-content-end">\n' +
                    '                            <div class="box-chat box-my p-3">\n' +
                    '                                <span>' + mes + '</span>\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '                        <div class="col-lg-1">\n' +
                    '                            <img src="' + avt + '" class="img-avt-chat">\n' +
                    '                        </div>\n' +
                    '                    </div>');
                $('.list-messenger-{{$group_messenger_id}}').animate({
                    scrollTop: $('.list-messenger-{{$group_messenger_id}}')[0].scrollHeight
                }, 0);
                $('.messenger-last-{{$group_messenger_id}}').text(mes);
                let html = $('.item-chat-{{$group_messenger_id}}').html();
                html = '<div class="item-chat-{{$group_messenger_id}}">' + html + '</div>';
                $('.item-chat-{{$group_messenger_id}}').remove();
                $(".box-messenger").prepend(html);
                let url = $(this).attr('data-url');
                let data = {};
                data['messenger'] = mes;
                data['user_id'] = $('.customer-active').attr('data-id');
                data['group_messenger_id'] = $('.customer-active').attr('data-group-id');
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: 'json',
                    data: data,
                    success: function (data) {
                        $(".count-chat").text(data.count);
                    },
                    error: function () {
                        console.log('error')
                    }
                });
            }
        })
    })
</script>
</body>
<!-- END: Body-->
</html>
