<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
{{--<link rel="stylesheet" type="text/css" href="css/home/style.css">--}}
<link rel="stylesheet" type="text/css" href="css/user/bank.css">


<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <div class="container">
        <div class="row">
            @include('frontend.user.menu')
            <div class="col-lg-9 p-0 pr-3 mb-5">
                <div class="bank-1 bg-white">
                    <div class="d-flex align-items-center">
                        <h3>Thẻ Tín Dụng/Thẻ Ghi Nợ</h3>
                        <button type="button" class="btn btn-main btn-add preventdefault">Thêm Thẻ Tín Dụng</button>
                    </div>
                    <div class="d-flex align-items-center bank-bank">
                        <div class="bank">
                            <div class="info-bank">
                                <img src="Icon/user/visa.png" width="77" alt="">
                                <p class="status float-right">Mặc định</p>
                                <p class="text-number-bank pt-4 font-weight-bold">**** **** **** 3011</p>
                            </div>
                        </div>
                        <div class="action preventdefault">
                            <a href="">
                                <p class="text">Xoá</p>
                            </a>
                            <div class="d-flex align-items-center">
                                <a href="">
                                    <p class="text">Thiết lập mặc định</p>
                                </a>
                                <label class="switch">
                                    <input type="checkbox" checked>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bank-1 bg-white mt-3">
                    <div class="d-flex align-items-center">
                        <h3>Tài Khoản Ngân Hàng Của Tôi</h3>
                        <button type="button" class="btn btn-main btn-add preventdefault">Thêm Tài Khoản Ngân Hàng</button>
                    </div>
                    <div class="d-flex align-items-center bank-bank">
                        <div class="bank">
                            <div class="info-bank">
                                <img src="Icon/user/techcombank.png" alt="">
                                <p class="status float-right">Mặc định</p>
                                <p class="pt-4 font-weight-bold name-bank">TECHCOMBANK - NHTMCP KY THUONG VN</p>
                                <p class="font-weight-bold name">BUI THI HUYEN PHUONG</p>
                                <p class="address">Khu vực: Hà Nội</p>
                                <p class="name-branch">Chi Nhánh: TCB LINH ĐÀM</p>
                                <p class="number-bank float-right font-weight-bold">**** **** **** 3011</p>
                            </div>
                        </div>
                        <div class="action preventdefault">
                            <a href="">
                                <p class="text">Xoá</p>
                            </a>
                            <div class="d-flex align-items-center">
                                <a href="">
                                    <p class="text">Thiết lập mặc định</p>
                                </a>
                                <label class="switch">
                                    <input type="checkbox" checked>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('frontend.base.footer')

@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
