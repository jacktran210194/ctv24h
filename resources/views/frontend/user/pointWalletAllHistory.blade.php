<?php if(isset($dataHistoryCoin)): ?>
<?php foreach ($dataHistoryCoin as $value): ?>
<div class="information-point d-flex justify-content-lg-start align-items-center">
    <div class="icon-point align-items-center d-flex justify-content-lg-center" style="width: 60px">
        <img src="Icon/user/dollar_1.png" style="width: 100%">
    </div>
    <div class="d-flex align-items-center justify-content-lg-between w-100">
        <div class="infor">
            <p class="title-infor">{{$value['status']}}</p>
            <p class="text">CTV24h Point từ {{$value['status']}}</p>
            <p class="time">{{$value['created_at']}}</p>
        </div>
        <div class="plus-point">+ {{$value['value']}} Point</div>
    </div>
</div>
<?php endforeach; ?>
<?php endif; ?>

