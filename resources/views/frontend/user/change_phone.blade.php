<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<style>
    .input_otp input {
        box-sizing: border-box;
        box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.05);
        border-radius: 8px;
    }

    .border_red {
        border: 2px solid red !important;
        box-sizing: border-box;
        box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.05);
        border-radius: 8px;
    }

</style>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
{{--<link rel="stylesheet" type="text/css" href="css/home/style.css">--}}
<link rel="stylesheet" type="text/css" href="css/user/user.css">


<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <div class="container">
        <div class="row">
            @include('frontend.user.menu')
            <input type="hidden" class="phone_default" value="{{$data_login['phone']}}">
            <div class="col-lg-9 p-0 pr-3 bg-white mb-5">
                <div class="title">
                    <h3>{{$title}}</h3>
                    <p>Để đảm bảo tính bảo mật, vui lòng làm theo các bước sau để đổi số điện thoại</p>
                </div>
                <div class="border-bottom"></div>
                <div class="form_info">
                    <div class="vertical text-center mt-5">
                        <img src="Icon/user/xac_minh_1.png" alt="">
                    </div>
                    <div class="form_phone_1" style="display: block">
                        <div class="d-flex justify-content-center align-items-lg-start mt-5">
                            <div class="mr-3" style="width: 15%">
                                <p>Số điện thoại</p>
                                <p class="mb-2">Tài khoản liên kết</p>
                                <br><br><br><br>
                                <p class="">Mật khẩu</p>
                            </div>
                            <div style="width: 40%">
                                <p class="show_phone"></p>
                                <p>Nếu bạn thay đổi số điện thoại trên CTV24, số điện thoại của các "Tài khoản liên kết"
                                    cũng sẽ được thay đổi.
                                    Nếu bạn có bất kỳ thắc mắc nào về liên kết tài khoản, vui lòng liên hệ <span><a
                                            style="color: blue;text-decoration: underline">Bộ phận CSKH</a></span></p>
                                <p>
                                    <input type="password" class="form-control check_password" placeholder="Mật khẩu">
                                </p>
                            </div>
                        </div>
                        <div class="text-center mt-3">
                            <button data-id="{{$data_login['id']}}" data-url="{{url('user/change_phone/check_pass')}}"
                                    style="width: 100px" class="btn btn-main btn_check_pass" disabled>Xác nhận
                            </button>
                        </div>
                        <div id="recaptcha"></div>
                    </div>
                    <div class="form_phone_2" style="display: none">
                        <div class="mt-5">
                            <div class="title_form text-center">
                                <h3>Nhập mã OTP</h3>
                                <p>Nhập mã OTP chúng tôi vừa gửi cho số điện thoại của bạn</p>
                            </div>
                            <div id="otp" class="input_otp d-flex align-items-center justify-content-center">
                                <input onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="first"
                                       maxlength="1" type="text" class="form-control text-center ml-2 otp_1 otp"
                                       style="width: 50px;height: 50px">
                                <input onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="second"
                                       maxlength="1" type="text" class="form-control text-center ml-2 otp_2 otp"
                                       style="width: 50px;height: 50px">
                                <input onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="third"
                                       maxlength="1" type="text" class="form-control text-center ml-2 otp_3 otp"
                                       style="width: 50px;height: 50px">
                                <input onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="fourth"
                                       maxlength="1" type="text" class="form-control text-center ml-2 otp_4 otp"
                                       style="width: 50px;height: 50px">
                                <input onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="fifth"
                                       maxlength="1" type="text" class="form-control text-center ml-2 otp_5 otp"
                                       style="width: 50px;height: 50px">
                                <input onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="sixth"
                                       maxlength="1" type="text" class="form-control text-center ml-2 otp_6 otp"
                                       style="width: 50px;height: 50px">
                            </div>
                            <p class="text-red text-center text_error mt-2" style="display: none;">Mã OTP sai vui lòng
                                nhập lại.</p>
                            <div class="text-center mt-4">
                                Bạn chưa nhận được mã OTP ?
                                <button style="height: 25px; line-height: 0px"
                                        class="btn ml-2 btn-main btn_return_otp preventdefault">
                                    Gửi lại
                                </button>
                            </div>
                            <div class="text-center mt-3">
                                <button style="width: 100px" class="btn btn-main btn_check_otp" disabled>Xác nhận
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form_phone_3" style="display: none">
                        <div class="col-lg-6" style="margin: auto;">
                            <p class="mt-5 text-center">Nhập số điện thoại mới của bạn</p>
                            <div class="form-group">
                                <label for="">Số điện thoại cũ</label>
                                <input type="text" class="form-control phone_old">
                            </div>
                            <div class="form-group">
                                <label for="">Số điện thoại mới</label>
                                <input type="text" class="form-control phone_new">
                            </div>
                            <div class="text-center mt-5">
                                <button data-id="{{$data_login['id']}}" data-url="{{url('user/change_phone/save')}}"
                                        style="width: 100px" class="btn btn-main btn_save_phone">Xác nhận
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form_phone_4" style="display: none">
                        <div class="mt-5 text-center">
                            <i style="color: #2DC258" class="fa fa-check fa-4x"></i>
                            <p class="mt-4 text-bold-400" style="font-size: 24px">Số điện thoại của bạn đã được thay
                                đổi</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('frontend.base.footer')
@include('frontend.base.script')
<script>
    //focus
    function OTPInput() {
        const inputs = document.querySelectorAll('#otp > *[id]');
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener('keydown', function (event) {
                if (event.key === "Backspace") {
                    inputs[i].value = '';
                    if (i !== 0)
                        inputs[i - 1].focus();
                } else {
                    if (i === inputs.length - 1 && inputs[i].value !== '') {
                        return true;
                    } else if (event.keyCode > 47 && event.keyCode < 58) {
                        inputs[i].value = event.key;
                        if (i !== inputs.length - 1)
                            inputs[i + 1].focus();
                        event.preventDefault();
                    } else if (event.keyCode > 64 && event.keyCode < 91) {
                        inputs[i].value = String.fromCharCode(event.keyCode);
                        if (i !== inputs.length - 1)
                            inputs[i + 1].focus();
                        event.preventDefault();
                    }
                }
            });
        }
    }
    OTPInput();

    $(document).ready(function () {
        //validate
        let specialChars = "<>@!#$%^&*_+[]{}?:;|'\"\\/~`-=()+-qwertyuiopasdfghjklzxcvbnm., ";
        let check = function (string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true;
                }
            }
            return false;
        };

        let xac_minh_2 = '<img src="Icon/user/xac_minh_2.png">';
        let xac_minh_3 = '<img src="Icon/user/xac_minh_3.png">';
        let xac_minh_4 = '<img src="Icon/user/xac_minh_4.png">';
        let phone_defalut = $('.phone_default').val();
        let verificationId;
        let check_phone = phone_defalut.slice(8, 10);
        let show_phone = '********' + check_phone;
        $('.show_phone').text(show_phone);

        //view 1
        $(document).on('keyup', '.check_password', function () {
            let password = $(this).val();
            if (password !== '') {
                $('.btn_check_pass').prop('disabled', false);
            } else {
                $('.btn_check_pass').prop('disabled', true);
            }
        });

        //nhập pass và gửi otp
        $(document).on('click', '.btn_check_pass', function () {
            $(this).prop('disabled', true);
            let password = $('.check_password').val();
            let customer_id = $(this).attr('data-id');
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('customer_id', customer_id);
            form_data.append('password', password);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: async function (data) {
                    if (data.status) {
                        let applicationVerifier = new firebase.auth.RecaptchaVerifier(
                            'recaptcha', {
                                size: "invisible",
                                callback: function () {
                                    submitPhoneNumberAuth();
                                }
                            });
                        let fs = phone_defalut.slice(0, 3);
                        if (fs !== '+84') {
                            phone_defalut = phone_defalut.slice(1, phone_defalut.length);
                            phone_defalut = '+84' + phone_defalut;
                        }
                        let provider = new firebase.auth.PhoneAuthProvider();
                        try {
                            verificationId = await provider.verifyPhoneNumber(phone_defalut, applicationVerifier);
                            $('.vertical').html(xac_minh_2);
                            $('.form_phone_1').hide();
                            $('.form_phone_2').show();

                        } catch (e) {
                            alert('Đã có lỗi xảy ra!');
                            console.log(e);
                            // location.reload();
                        }
                    } else {
                        $('.btn_check_pass').prop('disabled', false);
                        swal('Thông báo', data.msg, 'error');
                    }
                },
                error: function (e) {
                    console.log('error:' + e);
                },
            })

        });

        // view 2

        // $(document).on('keyup', '.otp_1', function () {
        //     let value = $(this).val();
        //     if (check(value) || value == '') {
        //         $(this).addClass('border_red');
        //     } else {
        //         // $('.otp_2').prop('disabled', false);
        //         $('.otp_2').focus();
        //         $(this).removeClass('border_red');
        //         // $(this).prop('disabled', true);
        //     }
        //
        // });
        // $(document).on('keyup', '.otp_2', function () {
        //     let value = $(this).val();
        //     if (check(value) || value == '') {
        //         $(this).addClass('border_red');
        //     } else {
        //         // $('.otp_3').prop('disabled', false);
        //         $('.otp_3').focus();
        //         $(this).removeClass('border_red');
        //         // $(this).prop('disabled', true);
        //     }
        // });
        // $(document).on('keyup', '.otp_3', function () {
        //     let value = $(this).val();
        //     if (check(value) || value == '') {
        //         $(this).addClass('border_red');
        //     } else {
        //         // $('.otp_4').prop('disabled', false);
        //         $('.otp_4').focus();
        //         $(this).removeClass('border_red');
        //         // $(this).prop('disabled', true);
        //     }
        // });
        // $(document).on('keyup', '.otp_4', function () {
        //     let value = $(this).val();
        //     if (check(value) || value == '') {
        //         $(this).addClass('border_red');
        //     } else {
        //         // $('.otp_5').prop('disabled', false);
        //         $('.otp_5').focus();
        //         $(this).removeClass('border_red');
        //         // $(this).prop('disabled', true);
        //     }
        // });
        // $(document).on('keyup', '.otp_5', function () {
        //     let value = $(this).val();
        //     if (check(value) || value == '') {
        //         $(this).addClass('border_red');
        //     } else {
        //         // $('.otp_6').prop('disabled', false);
        //         $('.otp_6').focus();
        //         $(this).removeClass('border_red');
        //         // $(this).prop('disabled', true);
        //     }
        // });
        $(document).on('keyup', '.otp_6', function () {
            let value = $(this).val();
            if (check(value) || value == '') {
                // $(this).addClass('border_red');
            } else {
                // $(this).prop('disabled', true);
                // $(this).removeClass('border_red');
                $('.btn_check_otp').prop('disabled', false);
            }
        });

        $(document).on('click', '.btn_check_otp', async function () {
            $(this).prop('disabled', true);
            let otp_1 = $('.otp_1').val();
            let otp_2 = $('.otp_2').val();
            let otp_3 = $('.otp_3').val();
            let otp_4 = $('.otp_4').val();
            let otp_5 = $('.otp_5').val();
            let otp_6 = $('.otp_6').val();
            let otp_full = otp_1 + otp_2 + otp_3 + otp_4 + otp_5 + otp_6;
            try {
                let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, otp_full);
                try {
                    await firebase.auth().signInWithCredential(phoneCredential);
                    $('.vertical').html(xac_minh_3);
                    $('.form_phone_2').hide();
                    $('.form_phone_3').show();

                } catch (e) {
                    console.log(e);
                    $('.otp').addClass('border_red');
                    $('.otp').val('');
                    $('.text_error').show();
                    $('.otp_1').prop('disabled', false);
                    $('.otp_1').focus();
                    $(this).prop('disabled', false);
                }
            } catch (e) {
                console.log(e);
                $('.otp').addClass('border_red');
                $('.otp').val('');
                $('.text_error').show();
                $('.otp_1').prop('disabled', false);
                $('.otp_1').focus();
                $(this).prop('disabled', false);
            }
        });

        //view 3
        $(document).on('click', '.btn_save_phone', function () {
            let phone_old = $('.phone_old').val();
            let phone_new = $('.phone_new').val();
            let customer_id = $(this).attr('data-id');
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('customer_id', customer_id);
            form_data.append('phone_old', phone_old);
            form_data.append('phone_new', phone_new);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $(this).prop('disabled', true);
                },
                success: function (data) {
                    if (data.status) {
                        $('.vertical').html(xac_minh_4);
                        $('.form_phone_3').hide();
                        $('.form_phone_4').show();
                    } else {
                        swal('Thông báo', data.msg, 'warning');
                    }
                },
                error: function (e) {
                    console.log('error:' + e);
                },
                complete: function () {
                    $(this).prop('disabled', false);
                }
            });
        })
    });
</script>
</body>
<!-- END: Body-->
</html>
