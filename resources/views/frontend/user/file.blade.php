<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
{{--<link rel="stylesheet" type="text/css" href="css/home/style.css">--}}
<link rel="stylesheet" type="text/css" href="css/user/user.css">


<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <div class="container">
        <div class="row">
            @include('frontend.user.menu')
            <input type="hidden" class="phone_default" value="{{$data_customer['phone']}}">
            <div class="col-lg-9 p-0 pr-3 bg-white mb-5">
                <div class="title">
                    <h3>Hồ Sơ Tài Khoản</h3>
                    <p>Quản lý thông tin hồ sơ để bảo mật tài khoản</p>
                </div>
                <div class="border-bottom"></div>
                <div class="user-img">
                    <img id="avatar"
                         src="<?= isset($data_customer) && File::exists(substr($data_customer['image'], 1)) ? $data_customer['image'] : 'Icon/user/avatar.png' ?>"
                         class="img-fluid avatar-user" alt="">
                    <button class="btn btn-main ml-4 btn-img">+ Chọn ảnh đại diện</button>
                    <input type='file' hidden accept="image/*" name="image" id="image"
                           onchange="changeImg(this)">
                </div>
                <div class="col-md-10 info-user">
                    <form>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="">Họ và tên</label>
                                <input type="text" class="form-control" id="name"
                                       value="<?= isset($data_customer) ? $data_customer['name'] : '' ?>">
                            </div>
                            <div class="col-sm-6 position-relative">
                                <label>Email</label>
                                <input type="email" class="form-control" id="email"
                                       value="<?= isset($data_customer) ? $data_customer['email'] : '' ?>">
                                <a style="right: 7%;top: 60%" class="position-absolute preventdefault" href="{{url('user/change_email')}}">
                                    <i class="fa fa-pencil fa-lg"></i>
                                </a>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="">Tên shop</label>
                                <input type="text" class="form-control" id="shop_name"
                                       value="{{isset($data_shop) ? $data_shop['name'] : ''}}">
                            </div>
                            <div class="col-sm-6 position-relative">
                                <label for="">Số điện thoại</label>
                                <input disabled type="text" class="form-control" id="phone"
                                       value="">
                                <a style="right: 7%;top: 60%" class="position-absolute" href="{{url('user/change_phone')}}">
                                    <i class="fa fa-pencil fa-lg"></i>
                                </a>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="">Giới tính</label>
                                <div class="d-flex">
                                    <input @isset($data_customer) @if($data_customer['gender'] == 0) checked
                                           @endif @endisset type="radio" value="0" id="gender" name="gender"><span
                                        class="pl-2">Nam</span>
                                    <input @isset($data_customer) @if($data_customer['gender'] == 1) checked
                                           @endif @endisset type="radio" value="1" id="gender" name="gender"
                                           class="ml-5"><span class="pl-2">Nữ</span>
                                    {{--                                    <input @isset($data_customer) @if($data_customer['gender'] == 2) checked @endif @endisset type="radio" value="2" id="gender" name="gender" class="ml-5"><span class="pl-2">Khác</span>--}}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="inputPostalCode">Ngày sinh</label>
                                <input type="date" class="form-control" id="birthday"
                                       value="<?= isset($data_customer) ? date('Y-m-d', strtotime($data_customer['birthday'])) : date('Y-m-d') ?>">
                            </div>
                        </div>
                        <button data-id="{{$data_customer['id']}}" data-url="{{url('user/file/save')}}" type="button"
                                class="btn btn-main px-4 float-right btn-add-account">Lưu thay đổi
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('frontend.base.footer')
@include('frontend.base.script')
<script>
    $(document).ready(function () {
        let phone_default = $('.phone_default').val();
        let check_phone = phone_default.slice(8, 10);
        let show_phone = '********'+check_phone;
        $('#phone').val(show_phone);
        $(document).on("click", ".btn-img", function () {
            $('#image').click();
        });
        $(document).on("click", ".btn-add-account", function () {
            let email = $('#email').val();

            function validateEmail(email_check) {
                const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email_check).toLowerCase());
            }

            if (!validateEmail(email)) {
                swal('Thông báo', 'Không đúng định dạng email !', 'warning');
                return false;
            }
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('name', $('#name').val());
            form_data.append('email', $('#email').val());
            form_data.append('shop_name', $('#shop_name').val());
            form_data.append('gender', $('#gender:checked').val());
            form_data.append('birthday', $('#birthday').val());
            form_data.append('image', $('#image')[0].files[0]);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        swal("Thành công", data.msg, "success");
                        // window.location.href = data.url;
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 1500);
                    } else {
                        swal("Thất bại", data.msg, "error");
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });
    })
</script>
</body>
<!-- END: Body-->
</html>
