<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/user/user.css">


<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <div class="container">
        <div class="row">
            @include('frontend.user.menu')
            <div class="col-lg-9 p-0 pr-3 bg-white mb-5">
                <div class="title text-center">
                    <h3>Đổi mật khẩu</h3>
                    <p>Để bảo mật tài khoản, vui lòng không chia sẻ mật khẩu cho người khác</p>
                </div>

                <div class="col-lg-6" style="margin: auto">
                    <div class="form-group">
                        <label for="">Mật khẩu hiện tại</label>
                        <input type="password" id="password_old" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Mật khẩu mới</label>
                        <input type="password" id="password_new" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Xác nhận mật khẩu</label>
                        <input type="password" id="re_password_new" class="form-control">
                    </div>
                    <div class="d-flex align-items-center mt-5">
                        <a style="text-decoration: underline; " href="{{url('forgot_password')}}">Quên mật khẩu?</a>
                        <div style="position: absolute; right: 3%;">
                            <button data-id="{{isset($data) ? $data['id'] : ''}}" data-url="{{url('user/change_password/save')}}" class="btn btn-main btn-add-account">Xác nhận</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@include('frontend.base.footer')
@include('frontend.base.script')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-add-account", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('password_old', $('#password_old').val());
            form_data.append('password_new', $('#password_new').val());
            form_data.append('re_password_new', $('#re_password_new').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        swal("Thành công", data.msg, "success");
                        window.location.href = data.url;
                    } else {
                        swal("Thất bại", data.msg, "error");
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });
    })
</script>
</body>
<!-- END: Body-->
</html>
