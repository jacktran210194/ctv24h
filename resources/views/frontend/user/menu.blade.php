<?php
$login = Session::get('data_customer');
if (isset($login)) {
    $data_login = \App\Models\CustomerModel::find($login->id);
}
?>
<link rel="stylesheet" type="text/css" href="css/user/user_menu.css">
<div class="col-lg-3">
    <div id="info-user" class="d-flex align-items-center bg-white">
        <img width="49px" height="49px"
             src="<?= isset($data_login) && File::exists(substr($data_login->image, 1)) ? $data_login->image : 'Icon/user/avatar.png' ?>"
             alt=""
             class="img-fluid avatar">
        <div class="ml-4">
            <span class="font-weight-bold"><?= isset($data_login) ? $data_login->name : '' ?></span>
            <a href="{{url('user/file')}}">
                <div id="edit" class="mt-2">
                    <i class="fa fa-pencil"></i>
                    <span>Sửa hồ sơ</span>
                </div>
            </a>
        </div>
    </div>
    <div class="border-bottom border-bottom-menu"></div>
    <div id="menu" class="align-items-center bg-white">
        <ul>
            <li class="li-cha d-flex align-items-center">
                <object data="Icon/user/info.svg" width="20" height="20"></object>
                <span class="pl-2"><a class="active" href="{{url('user/file')}}">Tài khoản của tôi</a></span>
            </li>
            <li class="li-con"><a href="{{url('user/file')}}">Hồ Sơ</a></li>
            <li class="li-con"><a href="{{url('user/bank')}}">Ngân hàng</a></li>
            <li class="li-con"><a href="{{url('user/address')}}">Địa chỉ</a></li>
            <li class="li-con"><a href="{{url('user/change_password')}}">Đổi mật khẩu</a></li>
            <li class="li-cha d-flex align-items-center">
                <object data="Icon/user/order.svg" width="20" height="20"></object>
                <span class="pl-2"><a href="{{url('my_history/by_product')}}">Đơn mua</a></span>
            </li>
            <li class="li-cha d-flex align-items-center">
                <object data="Icon/user/notification.svg" width="20" height="20"></object>
                <span class="pl-2"><a href="{{url('notification/update_order')}}">Thông báo</a></span>
            </li>
            <li class="li-con"><a href="{{url('notification/update_order')}}">Cập nhật đơn hàng</a></li>
            <li class="li-con"><a href="{{url('notification/promotion')}}">Khuyến mãi</a></li>
            <li class="li-con"><a href="{{url('notification/update_wallet')}}">Cập nhật ví</a></li>
            <li class="li-con"><a href="{{url('notification/online')}}">Hoạt động</a></li>
            <li class="li-con"><a href="{{url('notification/update_review')}}">Cập nhật đánh giá</a></li>
            <li class="li-con"><a href="{{url('notification/update_floor')}}">Cập nhật sàn CTV24H</a></li>
            <li class="li-cha d-flex align-items-center preventdefault">
                <object data="Icon/user/voucher.svg" width="20" height="20"></object>
                <span class="pl-2"><a href="">Ví Voucher</a></span>
            </li>
            <li class="li-cha d-flex align-items-center">
                <object data="Icon/user/point.svg" width="20" height="20"></object>
                <span class="pl-2"><a href="{{url("user/point-wallet")}}">Ví Point</a></span>
            </li>
            <li class="li-cha d-flex align-items-center">
                <object data="Icon/user/point.svg" width="20" height="20"></object>
                <span class="pl-2"><a class="preventdefault">Ví Xu</a></span>
            </li>
        </ul>
    </div>
</div>
