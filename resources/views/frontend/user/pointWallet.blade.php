<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
{{--<link rel="stylesheet" type="text/css" href="css/home/style.css">--}}
<link rel="stylesheet" type="text/css" href="css/user/user.css">

<link rel="stylesheet" type="text/css" href="css/user/point-wallet.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <div class="container">
        <div class="row">
            @include('frontend.user.menu')
            <div class="col-lg-9 p-0 pr-3 bg-white mb-5">
                <h2 class="title">Ví Point</h2>
                <div class="d-flex align-items-center total-point">
                    <div class="image d-flex justify-content-lg-center align-items-center" style="max-width: 50px">
                        <img src="Icon/user/dollar_1.png" style="width: 100%">
                    </div>
                    <div class="number-piont">
                        <p class="point">{{$coin}} <span>Point</span></p>
                        <p class="date-time">{{$coin}} point sẽ hết hạn ngày 20-12-2020</p>
                    </div>
                </div>
                <div class="nav-point-wallet d-flex justify-content-lg-start">
                    <ul class="d-flex align-items-center justify-content-lg-center nav-point">
                        <li class="d-flex align-items-center justify-content-lg-center
                         <?= $type === 'all-history' ? 'active' : '' ?>
                         ">
                            <a href="<?= URL::to('user/point-wallet?type=all-history') ?>">
                                TẤT CẢ LỊCH SỬ
                            </a>
                        </li>
                        <li class="d-flex align-items-center justify-content-lg-center
                        <?= $type === 'received' ? 'active' : '' ?>
                        ">
                            <a href="<?= URL::to('user/point-wallet?type=received') ?>">
                                ĐÃ NHẬN
                            </a>
                        </li>
                        <li class="d-flex align-items-center justify-content-lg-center
                        <?= $type === 'used' ? 'active' : '' ?>
                        ">
                            <a href="<?= URL::to('user/point-wallet?type=used') ?>">
                                ĐÃ DÙNG
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="content-point-wallet">
                    <?php if( $type === 'received' ) :?>
                        @include("frontend.user.pointWalltetReceived")
                    <?php elseif ( $type === 'used' ) :?>
                        @include("frontend.user.pointWalltetUsed")
                     <?php else: ?>
                        @include("frontend.user.pointWalletAllHistory")
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
@include('frontend.base.footer')
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
