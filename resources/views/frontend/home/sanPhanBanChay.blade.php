<div class="section product-recommer">
    <div class="tab-section">
        <a href="#" class="title-link relative preventdefault">Sản phẩm bán chạy hàng quốc tế</a>
        <a href="#" class="title-link relative preventdefault">Hàng có sẵn</a>
        <a href="#" class="title-link relative preventdefault">Hàng Nhật</a>
        <a href="#" class="title-link relative preventdefault">Hàng Hàn</a>
        <a href="#" class="title-link relative preventdefault">Hàng Đức</a>
    </div>
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    $path_2 = "assets/frontend/Icon/flashsale";
    ?>
    <div class="product-section relative">
        <button class="next-arrow btn-arrow prev-arrow-selling"><img style="width: 7px" src="<?php echo asset("$path/next_arrow.png")?>"></button>
        <button class="prev-arrow btn-arrow next-arrow-selling"><img style="width: 7px" src="<?php echo asset("$path/prev_arrow.png")?>"></button>
        <div class="container-section-product slide-section-selling">
            <div class="content border-box preventdefault">
                <div class="img-product remove-mg">
                    <img src="<?php echo asset("$path_2/product_sale_1.jpg")?>" class="lazy" alt="image-product">
                </div>
                <div class="title-product-n relative">
                    <span>Áo phông thun polo nam Galvin cotton cá sấu trắng đen tay ngắn lỡ cổ bẻ Leo Vatino 12A</span>
                </div>
                <div class="product-price">
                    <p class="price">220.000 - 109.000</p>
                    <p class="pass">Giá CTV: ********</p>
                    <div class="form btn-sign-up relative">
                        <button>Đăng ký CTV để xem giá</button>
                    </div>
                </div>
                <div class="review">
                    <img src="<?php echo asset("$path/icon-review.png")?>">
                    <p>Đã bán 10</p>
                </div>
                <div class="review">
                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                    <p>Hà Nội</p>
                </div>
            </div>
            <div class="content border-box preventdefault">
                <div class="img-product remove-mg">
                    <img src="<?php echo asset("$path_2/product_sale_2.jpg")?>" class="lazy" alt="image-product">
                </div>
                <div class="title-product-n relative">
                    <span>Quần Dài Nike Full Swoosh Thêu - Hot Trend 2021</span>
                </div>
                <div class="product-price">
                    <p class="price">350.000 - 279.000</p>
                    <p class="pass">Giá CTV: ********</p>
                    <div class="form btn-sign-up relative">
                        <button>Đăng ký CTV để xem giá</button>
                    </div>
                </div>
                <div class="review">
                    <img src="<?php echo asset("$path/icon-review.png")?>">
                    <p>Đã bán 10</p>
                </div>
                <div class="review">
                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                    <p>Hà Nội</p>
                </div>
            </div>
            <div class="content border-box preventdefault">
                <div class="img-product remove-mg">
                    <img src="<?php echo asset("$path_2/product_sale_3.jpg")?>" class="lazy" alt="image-product">
                </div>
                <div class="title-product-n relative">
                    <span>Áo thun Polo nam cổ bẻ TEDI</span>
                </div>
                <div class="product-price">
                    <p class="price">399.000 - 279.000</p>
                    <p class="pass">Giá CTV: ********</p>
                    <div class="form btn-sign-up relative">
                        <button>Đăng ký CTV để xem giá</button>
                    </div>
                </div>
                <div class="review">
                    <img src="<?php echo asset("$path/icon-review.png")?>">
                    <p>Đã bán 10</p>
                </div>
                <div class="review">
                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                    <p>Hà Nội</p>
                </div>
            </div>
            <div class="content border-box preventdefault">
                <div class="img-product remove-mg">
                    <img src="<?php echo asset("$path_2/product_sale_4.jpg")?>" class="lazy" alt="image-product">
                </div>
                <div class="title-product-n relative">
                    <span>Áo phông nam cổ bẻ ngắn tay phối viền cao cấp </span>
                </div>
                <div class="product-price">
                    <p class="price">150.000 - 84.000</p>
                    <p class="pass">Giá CTV: ********</p>
                    <div class="form btn-sign-up relative">
                        <button>Đăng ký CTV để xem giá</button>
                    </div>
                </div>
                <div class="review">
                    <img src="<?php echo asset("$path/icon-review.png")?>">
                    <p>Đã bán 10</p>
                </div>
                <div class="review">
                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                    <p>Hà Nội</p>
                </div>
            </div>
            <div class="content border-box preventdefault">
                <div class="img-product remove-mg">
                    <img src="<?php echo asset("$path_2/product_sale_5.jpg")?>" class="lazy" alt="image-product">
                </div>
                <div class="title-product-n relative">
                    <span>Giày da bò nam CHEAPSTORE CS419 Xám</span>
                </div>
                <div class="product-price">
                    <p class="price">680.000 - 449.000</p>
                    <p class="pass">Giá CTV: ********</p>
                    <div class="form btn-sign-up relative">
                        <button>Đăng ký CTV để xem giá</button>
                    </div>
                </div>
                <div class="review">
                    <img src="<?php echo asset("$path/icon-review.png")?>">
                    <p>Đã bán 10</p>
                </div>
                <div class="review">
                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                    <p>Hà Nội</p>
                </div>
            </div>

            <div class="content border-box preventdefault">
                <div class="img-product remove-mg">
                    <img src="<?php echo asset("$path_2/product_sale_6.jpg")?>" class="lazy" alt="image-product">
                </div>
                <div class="title-product-n relative">
                    <span>Giày Sneaker Da Unisex DINCOX D20 Năng Động Cá Tính White</span>
                </div>
                <div class="product-price">
                    <p class="price">490.000 - 392.000</p>
                    <p class="pass">Giá CTV: ********</p>
                    <div class="form btn-sign-up relative">
                        <button>Đăng ký CTV để xem giá</button>
                    </div>
                </div>
                <div class="review">
                    <img src="<?php echo asset("$path/icon-review.png")?>">
                    <p>Đã bán 10</p>
                </div>
                <div class="review">
                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                    <p>Hà Nội</p>
                </div>
            </div>
            <div class="content border-box preventdefault">
                <div class="img-product remove-mg">
                    <img src="<?php echo asset("$path_2/product_sale_7.jpg")?>" class="lazy" alt="image-product">
                </div>
                <div class="title-product-n relative">
                    <span>Áo thun nam tay ngắn có cổ 💙FREESHIP💙 áo phông nam đẹp</span>
                </div>
                <div class="product-price">
                    <p class="price">270.000 - 370.000</p>
                    <p class="pass">Giá CTV: ********</p>
                    <div class="form btn-sign-up relative">
                        <button>Đăng ký CTV để xem giá</button>
                    </div>
                </div>
                <div class="review">
                    <img src="<?php echo asset("$path/icon-review.png")?>">
                    <p>Đã bán 10</p>
                </div>
                <div class="review">
                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                    <p>Hà Nội</p>
                </div>
            </div>
        </div>
    </div>

</div>
