<?php $path = "assets/frontend/Icon/products"; ?>
@if(count($dataSugesstionProduct) && $dataSugesstionProduct)
    @foreach ($dataSugesstionProduct as $value)
        @php
            $product_value = \App\Models\AttributeValueModel::where('product_id', $value->id)->get();
            $price_ctv_min = \App\Models\AttributeValueModel::where('product_id', $value->id)->min('price_ctv');
            $price_ctv_max = \App\Models\AttributeValueModel::where('product_id', $value->id)->max('price_ctv');
        @endphp
        <?php
        $min = 0;
        $max = 0;
        foreach ($product_value as $item){
            $ctv = $item->price - $item->price_ctv;
            if ($min == 0){
                $min = $ctv;
            }elseif ($min > $ctv){
                $min = $ctv;
            }
            if ($max == 0){
                $max = $ctv;
            }elseif ($max < $ctv){
                $max = $ctv;
            }
        }
        ?>
        <div class="content border-box relative sp-product-content">
            <a href="{{url('details_product/'.(isset($value->product_id)?$value->product_id:$value->id))}}">
                <div class="img-product">
                    <img src="{{$value->image}}" class="lazy">
                </div>
                <div class="item-content-product d-flex justify-content-between flex-column">
                    <div class="title-product-n relative">
                        <p class="card-title">{{$value->name}}</p>
                    </div>
                    <div class="product-price">
                        @if($value->price_discount == $value->price)
                            <p class="price"><span>{{number_format($value->price_discount)}} đ</span></p>
                        @else
                            <p class="price"><span>{{number_format($value->price_discount)}} đ</span> -
                                <span class="price-sale">{{number_format($value->price)}} đ</span></p>
                        @endif

                        @if($price_ctv_min == $price_ctv_max)
                            <div class="d-flex justify-content-between">
                                <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ</p>
                            </div>
                        @else
                            <div class="d-flex justify-content-between">
                                <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ - {{number_format($price_ctv_max)}}đ</p>
                            </div>
                        @endif

                        @if($min == $max)
                            <div class="d-flex justify-content-between" style="margin-top: 10px">
                                <p>Lời: <span class="text-red">{{number_format($min)}}đ</span>
                                </p>
                            </div>
                        @else
                            <div class="d-flex justify-content-between" style="margin-top: 10px">
                                <p>Lời: <span class="text-red">{{number_format($min)}}đ - {{number_format($max)}}đ</span>
                                </p>
                            </div>
                        @endif
                    </div>
                    <div class="review">
                        @if($value->avg_rating == 0)
                            <img src="<?php echo asset("$path/0_sao.png")?>">
                        @else
                            @if($value->avg_rating == 1)
                                <img src="<?php echo asset("$path/1_sao.png")?>">
                            @elseif($value->avg_rating == 2)
                                <img src="<?php echo asset("$path/2_sao.png")?>">
                            @elseif($value->avg_rating == 3)
                                <img src="<?php echo asset("$path/3_sao.png")?>">
                            @elseif($value->avg_rating == 4)
                                <img src="<?php echo asset("$path/4_sao.png")?>">
                            @elseif($value->avg_rating == 5)
                                <img src="<?php echo asset("$path/5_sao.png")?>">
                            @elseif($value->avg_rating > 1 && $value->avg_rating < 2)
                                <img src="<?php echo asset("$path/1_5_sao.png")?>">
                            @elseif($value->avg_rating > 2 && $value->avg_rating < 3)
                                <img src="<?php echo asset("$path/2_5_sao.png")?>">
                            @elseif($value->avg_rating > 3 && $value->avg_rating < 4)
                                <img src="<?php echo asset("$path/3_5_sao.png")?>">
                            @elseif($value->avg_rating > 4 && $value->avg_rating < 5)
                                <img src="<?php echo asset("$path/4_5_sao.png")?>">
                            @endif
                        @endif
                        <p>Đã bán {{$value->is_sell}}</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;"
                             src="Icon/home/Danh_muc/icon-like.png">
                        <p>{{$value->place}}</p>
                    </div>
                </div>
            </a>
        </div>
    @endforeach
@else
    <h3 class="p-3 text-red">Không có sản phẩm nào phù hợp !</h3>
@endif
