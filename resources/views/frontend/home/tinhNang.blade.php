<div class="section section-feature">
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    ?>
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <img src="<?php echo asset("$path/icon-logo-homepage.png")?>">
            </div>
            <h2>Các tính năng đề xuất của CTV24H</h2>
        </div>
    </div>
    <div class="slide-feature flex-items">
        <div class="imge-banner">
            <a href="{{url('CTV24hCart')}}">
                <img src="Icon/home/Danh_muc/banner_cart.png" class="lazy" alt="">
            </a>
        </div>
        <div class="imge-banner">
            <a href="{{url('recharge-card-phone')}}">
                <img src="Icon/home/Danh_muc/banner_thanh_toan.png" class="lazy" alt="">
            </a>
        </div>
        <div class="imge-banner">
            <a href="{{url('recharge-card')}}">
                <img src="Icon/home/Danh_muc/banner_dien_nuoc.png" class="lazy" alt="">
            </a>
        </div>
        <div class="imge-banner">
            <a href="{{url('voucher')}}">
                <img src="Icon/home/Danh_muc/Frame_voucher.png" class="lazy" alt="">
            </a>
        </div>
    </div>
</div>
