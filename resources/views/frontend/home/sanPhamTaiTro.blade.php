<div class="section section-san-pham-trend sp-product">
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    ?>
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <img src="<?php echo asset("$path/icon-logo-homepage.png")?>">
            </div>
            <h2>Sản phẩm được tài trợ</h2>
        </div>
        <div class="link"><a href="{{url('sponsored_product')}}">Xem Thêm</a></div>
    </div>
    <div class="product-section relative">
        <div class="container-section-product slide-section-sponsor">
            <?php if(isset($dataSponsoredProduct)) :?>
            <?php foreach ($dataSponsoredProduct as $value) :?>
            <div class="content border-box relative sp-product-content border-yellow">
                <a href="{{url('details_product/'.$value->id)}}">
                    <div class="img-product">
                        <img src="{{$value->image}}" class="lazy">
                    </div>
                    <div class="container-sale">
                        <div class="image-background-sale">
                            <img src="<?php echo asset("$path/Vector_sale.png")?>">
                        </div>
                        <div class="text-sale">
                            <p class="text-number-sale">{{$value->value}}%</p>
                            <p class="text-sale-off">off</p>
                        </div>
                    </div>
                    <div class="item-content-product">
                        <div class="title-product-n relative">
                            <p class="card-title">{{$value->name}}</p>
                        </div>
                        <div class="product-price">
                            <p class="price"><span>{{number_format($value->price_discount)}}</span> - <span class="price-sale">{{number_format($value->price)}}</span></p>
                            <p class="pass">Giá CTV: ********</p>
                            <div class="form btn-sign-up relative">
                                <button>Đăng ký CTV để xem giá</button>
                            </div>
                        </div>
                        <div class="review">
                            <img src="<?php echo asset("$path/icon-review.png")?>">
                            <p>Đã bán {{$value->is_sell}}</p>
                        </div>
                        <div class="review">
                            <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                            <p>{{$value->place}}</p>
                        </div>
                    </div>
                </a>
            </div>
            <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="banner-section">
        <img src="<?php echo asset("$path/fash_banner_new.png")?>" class="lazy" alt="banner" style="max-height: 145px">
    </div>
</div>
<div class="section section-logo">
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <img src="<?php echo asset("$path/icon-logo-homepage.png")?>">
            </div>
            <h2>Thương hiệu nổi bật</h2>
        </div>
    </div>
    <?php if(isset($dataBrand)) :?>
    <div class="relative">
        <div class="slide-logo flex-items">
            <?php foreach($dataBrand as $value) :?>
                <a href="{{url('brand/'.$value->id)}}">
                    <div class="image-logo preventdefault">
                        <img style="object-fit: cover" width="116px" height="64px" src="{{$value->image}}" class="lazy" alt="logo">
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
    <?php endif; ?>
</div>
