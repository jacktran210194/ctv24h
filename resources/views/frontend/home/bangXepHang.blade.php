<div class="section section-bxh">
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    ?>
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <img src="<?php echo asset("$path/icon-logo-homepage.png")?>">
            </div>
            <h2>bảng xếp hạng top cv dẫn dầu trong tháng</h2>
        </div>
    </div>
    <div class="container-sections">
        <div class="content-banner-section">
            <div class="flex-items">
                <div class="image-bxh preventdefault">
                    <img src="<?php echo asset("$path/image_bxh_1.png")?>" class="lazy">
                </div>
                <div class="image-bxh preventdefault">
                    <img src="<?php echo asset("$path/image_bxh_2.png")?>" class="lazy">
                </div>
            </div>
        </div>
    </div>
</div>
