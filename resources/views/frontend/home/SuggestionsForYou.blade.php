<div class="suggested-for-you">
    <?php
    $path = "assets/frontend/Icon/products";
    $pathNew = "assets/frontend/Icon/home/Danh_muc";
    use App\Models\ProductModel;
    use App\Models\SubCategoryProductChildModel;
    ?>
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <img src="<?php echo asset("$pathNew/icon-logo-homepage.png")?>">
            </div>
            <h2>gợi ý cho bạn</h2>
        </div>
    </div>
    <div class="body-search flex container">
        <div class="fillter">
            <div class="title-fillter flex">
                <div class="icon_fillter">
                    <object data="icon/search/icon_fillter.svg" width="12px"></object>
                </div>
                <div class="title-text">BỘ LỌC TÌM KIẾM</div>
            </div>
            <div class="filter-catagory">
                <p class="title">Theo danh mục</p>
                @if(isset($category_product))
                    @foreach($display_category as $items)
                        @if(!empty($items))
                            <div class="sub-filter">
                                <p class="title">{{$items->name}}</p>
                                <div class="dropdown-filter" style="display: none">
                                    @foreach($category_product as $value)
                                        @if($items->id == $value->category_product_id)
                                            <?php
                                            $count_category = ProductModel::where('category_sub_child', $value->id)->count();
                                            ?>
                                            {{--                                            @if($count_category > 0)--}}
                                            <div class="flex content content-filter">
                                                <div>
                                                    <input class="check_category" type="checkbox" id="{{$value->id}}">
                                                </div>
                                                <p class="text">{{$value->name}}<span class="number">({{$count_category}})</span>
                                                </p>
                                            </div>
                                            {{--                                            @endif--}}
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
            <div class="filter-catagory">
                <p class="title">Nơi bán</p>
                @if(count($list_city) && $list_city)
                    @php $i=0; @endphp
                    @foreach($list_city as $value_city)
                        @php $i++; @endphp
                        @if($i <= 5)
                            <div class="flex content filter-place">
                                <input class="check_place" type="checkbox" value="{{$value_city->name}}">
                                <p class="text">{{$value_city->name}}</p>
                            </div>
                        @endif
                    @endforeach
                @endif

                @if(count($list_city) && $list_city)
                    @php $y=0; @endphp
                    @foreach($list_city as $value_city)
                        @if($y++ >= 5 )
                            <div style="display: none;" class="load_more_city flex content filter-place">
                                <input class="check_place" type="checkbox" value="{{$value_city->name}}">
                                <p class="text">{{$value_city->name}}</p>
                            </div>
                        @endif
                    @endforeach
                @endif
                <p class="load-more btn_load_more" style="cursor: pointer;">Xem Thêm ></p>
                <p class="load-more btn_hide_more" style="cursor: pointer; display: none;">Ẩn bớt ></p>
            </div>
            <div class="filter-catagory">
                <p class="title">Đơn vị vận chuyển</p>
                @if(isset($shipping))
                    @foreach($shipping as $value)
                        <div class="flex content filter-shipping">
                            <input class="check_shipping" type="checkbox" value="{{$value->id}}">
                            <p class="text">{{$value ->name }}</p>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="filter-catagory form-fillter">
                <p class="title">Khoảng giá</p>
                <div class="form-fillter-price flex">
                    <input type="number" placeholder="TỪ" class="price-filter-1 check_price_1">
                    <span class="icon-space">-</span>
                    <input type="number" placeholder="ĐẾN" class="price-filter-2 check_price_2">
                </div>
                <button class="btn btn-filter">Áp dụng</button>
            </div>
            <div class="filter-catagory">
                <p class="title">Loại shop</p>
                <div class="flex content">
                    <input class="check_type_shop" type="checkbox">
                    <p class="text">Shopee Mall</p>
                </div>
                <div class="flex content">
                    <input class="check_type_shop" type="checkbox">
                    <p class="text">Shop yêu thích</p>
                </div>
            </div>
            <div class="filter-catagory">
                <p class="title">Tình trạng</p>
                <div class="flex content filter-status">
                    <input class="check_type_product" type="checkbox" value="1">
                    <p class="text">Sản phẩm mới </p>
                </div>
                <div class="flex content filter-status">
                    <input class="check_type_product" type="checkbox" value="0">
                    <p class="text">Sản phẩm đã từng </p>
                </div>
            </div>
            <div class="filter-catagory">
                <p class="title">Đánh giá </p>
                <div class="flex content">
                    <button class="filter-ratings check_rating" value="5">
                        <object data="Icon/search/icon_star_five.svg"></object>
                    </button>
                </div>
                <div class="flex content">
                    <button class="d-flex align-items-center w-100 filter-ratings check_rating" value="4">
                        <object data="Icon/search/icon_star_for.svg"></object>
                        <p class="text">trở lên </p>
                    </button>
                </div>
                <div class="flex content">
                    <button class="d-flex align-items-center w-100 filter-ratings check_rating" value="3">
                        <object data="Icon/search/icon_star_three.svg"></object>
                        <p class="text">trở lên </p>
                    </button>
                </div>
                <div class="flex content">
                    <button class="d-flex align-items-center w-100 filter-ratings check_rating" value="2">
                        <object data="Icon/search/icon_star_tow.svg"></object>
                        <p class="text">trở lên </p>
                    </button>
                </div>
                <div class="flex content">
                    <button class="d-flex align-items-center w-100 filter-ratings check_rating" value="1">
                        <object data="Icon/search/icon_star_one.svg"></object>
                        <p class="text">trở lên </p>
                    </button>
                </div>
            </div>
            <div class="filter-catagory">
                <p class="title">Dịch vụ và khuyến mãi</p>
                <div class="flex content">
                    <input class="check_service" type="checkbox">
                    <p class="text">Freeship Xtra </p>
                </div>
                <div class="flex content">
                    <input class="check_service" type="checkbox">
                    <p class="text">Hoàn xu Xtra </p>
                </div>
                <div class="flex content">
                    <input class="check_service" type="checkbox">
                    <p class="text">Đang giảm giá </p>
                </div>
                <div class="flex content">
                    <input class="check_servie" type="checkbox">
                    <p class="text">Miễn phí vận chuyển </p>
                </div>
                <p class="load-more">Thêm
                    <object width="7px" data="icon/search/icon_arrow_down.svg"></object>
                </p>
            </div>
            <button class="btn clear-all">Xóa tất cả</button>
        </div>
        <div class="container-left">
            <div class="product-search">
                <?php
                $pathSlide = "assets/frontend/Icon/home/Danh_muc";
                ?>
                <div class="container-section-product">
                    @if(count($dataSugesstionProduct))
                        @foreach ($dataSugesstionProduct as $value)
                            @php
                                $product_value = \App\Models\AttributeValueModel::where('product_id', $value->id)->get();
                                $price_ctv_min = \App\Models\AttributeValueModel::where('product_id', $value->id)->min('price_ctv');
                                $price_ctv_max = \App\Models\AttributeValueModel::where('product_id', $value->id)->max('price_ctv');
                            @endphp
                            <?php
                                $min = 0;
                                $max = 0;
                                foreach ($product_value as $item){
                                    $ctv = $item->price - $item->price_ctv;
                                    if ($min == 0){
                                        $min = $ctv;
                                    }elseif ($min > $ctv){
                                        $min = $ctv;
                                    }
                                    if ($max == 0){
                                        $max = $ctv;
                                    }elseif ($max < $ctv){
                                        $max = $ctv;
                                    }
                                }
                            ?>
                            <div class="content border-box relative sp-product-content">
                                <a href="{{url('details_product/'.$value->id)}}">
                                    <div class="img-product">
                                        <img src="{{$value->image}}" class="lazy">
                                    </div>
                                    <div class="item-content-product justify-content-between">
                                        <div class="title-product-n relative">
                                            <p class="card-title">{{$value->name}}</p>
                                        </div>
                                        <div class="product-price">
                                            @if($value->price_discount == $value->price)
                                            <p class="price"><span>{{number_format($value->price_discount)}} đ</span></p>
                                            @else
                                                <p class="price"><span>{{number_format($value->price_discount)}} đ</span> -
                                                    <span class="price-sale">{{number_format($value->price)}} đ</span></p>
                                            @endif

                                            @if($price_ctv_min == $price_ctv_max)
                                                <div class="d-flex justify-content-between">
                                                    <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ</p>
                                                </div>
                                            @else
                                                <div class="d-flex justify-content-between">
                                                    <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ - {{number_format($price_ctv_max)}}đ</p>
                                                </div>
                                            @endif

                                            @if($min == $max)
                                                <div class="d-flex justify-content-between" style="margin-top: 10px">
                                                    <p>Lời: <span class="text-red">{{number_format($min)}}đ</span>
                                                    </p>
                                                </div>
                                            @else
                                                <div class="d-flex justify-content-between" style="margin-top: 10px">
                                                    <p>Lời: <span class="text-red">{{number_format($min)}}đ - {{number_format($max)}}đ</span>
                                                    </p>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="review">
                                            @if($value->avg_rating == 0)
                                                <img src="<?php echo asset("$path/0_sao.png")?>">
                                            @else
                                                @if($value->avg_rating == 1)
                                                    <img src="<?php echo asset("$path/1_sao.png")?>">
                                                @elseif($value->avg_rating == 2)
                                                    <img src="<?php echo asset("$path/2_sao.png")?>">
                                                @elseif($value->avg_rating == 3)
                                                    <img src="<?php echo asset("$path/3_sao.png")?>">
                                                @elseif($value->avg_rating == 4)
                                                    <img src="<?php echo asset("$path/4_sao.png")?>">
                                                @elseif($value->avg_rating == 5)
                                                    <img src="<?php echo asset("$path/5_sao.png")?>">
                                                @elseif($value->avg_rating > 1 && $value->avg_rating < 2)
                                                    <img src="<?php echo asset("$path/1_5_sao.png")?>">
                                                @elseif($value->avg_rating > 2 && $value->avg_rating < 3)
                                                    <img src="<?php echo asset("$path/2_5_sao.png")?>">
                                                @elseif($value->avg_rating > 3 && $value->avg_rating < 4)
                                                    <img src="<?php echo asset("$path/3_5_sao.png")?>">
                                                @elseif($value->avg_rating > 4 && $value->avg_rating < 5)
                                                    <img src="<?php echo asset("$path/4_5_sao.png")?>">
                                                @endif
                                            @endif
                                            <p>Đã bán {{$value->is_sell}}</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>{{$value->place}}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
