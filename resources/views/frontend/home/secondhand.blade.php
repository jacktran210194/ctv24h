<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/home/secondhan.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <!-- BEGIN: Header-->
    <!-- END: Header-->
    <!-- BEGIN: Homepage-->
    <div id="img">
        <div class="container">
            <div class="row">
                <img class="img img-fluid"
                     src="Icon/store_old/banner.png"
                     alt="">
            </div>
        </div>
    </div>
    <div id="category">
        <div class="container menu-category bg-white mb-5">
            <div class="row">
                <div class="d-flex category-menu-sencond">
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="d-flex category-menu-sencond">
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                    <div class="p-3">
                        <center>
                            <div class="icon-menu-category"></div>
                            <p class="pt-2">Túi xách</p>
                        </center>
                    </div>
                </div>
            </div>
            <div id="tab-1">
                <div class="row tab-category">
                    <p class="text-menu-tab">TÌM KIẾM HÀNG ĐẦU</p>
                </div>

                <div class="row d-flex p-2">
                    <div class="col-lg-4">
                        <div class="show-image">
                            <div class="float-left">
                                <img class="image-1" width="230px" height="232px"
                                     src="Icon/suggestion/sp_1.png"
                                     alt="">
                            </div>
                            <div class="list-image">
                                <div class="image-2-3">
                                    <img class="img-fluid img-list" width="113px" height="113px"
                                         src="Icon/suggestion/sp_2.png"
                                         alt="">
                                </div>
                                <div>
                                    <img class="img-fluid img-list" width="113px" height="113px"
                                         src="Icon/suggestion/sp_3.png"
                                         alt="">
                                </div>

                            </div>

                        </div>
                        <p class="text-category">Quần Jean Nữ</p>
                    </div>
                    <div class="col-lg-4">
                        <div class="show-image">
                            <div class="float-left">
                                <img class="image-1" width="230px" height="232px"
                                     src="Icon/suggestion/sp_1.png"
                                     alt="">
                            </div>
                            <div class="list-image">
                                <div class="image-2-3">
                                    <img class="img-fluid img-list" width="113px" height="113px"
                                         src="Icon/suggestion/sp_2.png"
                                         alt="">
                                </div>
                                <div>
                                    <img class="img-fluid img-list" width="113px" height="113px"
                                         src="Icon/suggestion/sp_3.png"
                                         alt="">
                                </div>

                            </div>

                        </div>
                        <p class="text-category">Quần Jean Nữ</p>
                    </div>
                    <div class="col-lg-4">
                        <div class="show-image">
                            <div class="float-left">
                                <img class="image-1" width="230px" height="232px"
                                     src="Icon/suggestion/sp_1.png"
                                     alt="">
                            </div>
                            <div class="list-image">
                                <div class="image-2-3">
                                    <img class="img-fluid img-list" width="113px" height="113px"
                                         src="Icon/suggestion/sp_2.png"
                                         alt="">
                                </div>
                                <div>
                                    <img class="img-fluid img-list" width="113px" height="113px"
                                         src="Icon/suggestion/sp_3.png"
                                         alt="">
                                </div>

                            </div>

                        </div>
                        <p class="text-category">Quần Jean Nữ</p>
                    </div>
                </div>
            </div>
            <div id="tab-1">
                <div class="row tab-category">
                    <p class="text-menu-tab">GỢI Ý DÀNH CHO BẠN</p>
                </div>
                <div class="row">
                    <div class="row d-flex p-4">
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_1.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_2.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_3.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_4.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_5.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_6.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="row d-flex p-4">
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_1.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_2.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_3.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_4.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_5.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_6.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="row d-flex p-4">
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_1.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_2.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_3.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_4.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_5.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="card">
                                <img width="185px" height="183px" class="card-img-top img-fluid"
                                     src="Icon/suggestion/sp_6.png"
                                     alt="">
                                <div class="card-body">
                                    <p class="card-title">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh Viên</p>
                                    <p class="card-text">Mua kèm deal sốc</p>
                                    <p class="card-price">190.000</p>
                                    <p class="card-price-ctv">Giá CTV: ********</p>
                                    <p class="card-text">Đăng ký CTV để xem giá</p>
                                    <div class="d-flex align-items-center align-content-center">
                                        <div class="">
                                            <object data="Icon/suggestion/start.png" type=""></object>
                                            <object data="Icon/suggestion/like.png" type=""></object>
                                        </div>
                                        <div class="pl-2">
                                            <span>Đã bán 10</span>
                                            <span>Hà Nội</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Homepage-->

        <!-- BEGIN: Footer-->
    @include('frontend.base.footer')
    <!-- END: Footer-->
    </div>
</div>

@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
