<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/suggestion/suggestion.css">
<link rel="stylesheet" type="text/css" href="css/ElectronicDevice/style.css">
<link rel="stylesheet" type="text/css" href="css/search/style.css">
<link rel="stylesheet" type="text/css" href="css/Vuocher/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <!-- BEGIN: Header-->
    <!-- END: Header-->
    <!-- BEGIN: Homepage-->
    <?php
    $path = "assets/frontend/Icon/Vuocher";
    ?>
    <div class="electronicdevice">
        <div id="img">
            <div class="container p-0">
                <img src="<?php echo asset("$path/banner.png")?>" alt="">
            </div>
        </div>
        <div id="category" class="">
            <div class="container menu-category pt-5 p-0">
                <div class="content-category">
                    <div class="tab-category mb-3">
                        <p class="text-menu-tab">THƯƠNG HIỆU NỔI BẬT</p>
                    </div>
                </div>
                <div class="slick-voucher">
                    <div class="shop-detail" style="">
                        <div class="info">
                            <div class="infor-image">
                                <img src="<?php echo asset("$path/Highlands_Coffee_logo.png")?>" alt="">
                            </div>
                            <div class="info-shop d-flex">
                                <div>
                                    <span class="text-address">LG</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="shop-detail" style="">
                        <div class="info">
                            <div class="infor-image">
                                <img src="<?php echo asset("$path/Grab_logo.png")?>" alt="">
                            </div>
                            <div class="info-shop d-flex">
                                <div>
                                    <span class="text-address">Samsung</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="shop-detail" style="">
                        <div class="info">
                            <div class="infor-image">
                                <img src="<?php echo asset("$path/Lotteria_logo.png")?>" alt="">
                            </div>
                            <div class="info-shop d-flex">
                                <div>
                                    <span class="text-address">TCL</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="shop-detail" style="">
                        <div class="info">
                            <div class="infor-image">
                                <img src="<?php echo asset("$path/Starbucks_Corporation_Logo.png")?>" alt="">
                            </div>
                            <div class="info-shop d-flex">
                                <div>
                                    <span class="text-address">Apple</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="shop-detail" style="">
                        <div class="info">
                            <div class="infor-image">
                                <img src="<?php echo asset("$path/Be-logo.png")?>" alt="">
                            </div>
                            <div class="info-shop d-flex">
                                <div>
                                    <span class="text-address">Sony</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="shop-detail" style="">
                        <div class="info">
                            <div class="infor-image">
                                <img src="<?php echo asset("$path/aha_logo.png")?>" alt="">
                            </div>
                            <div class="info-shop d-flex">
                                <div>
                                    <span class="text-address">Realme</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="shop-detail" style="">
                        <div class="info">
                            <div class="infor-image">
                                <img src="<?php echo asset("$path/Starbucks_Corporation_Logo.png")?>" alt="">
                            </div>
                            <div class="info-shop d-flex">
                                <div>
                                    <span class="text-address">LG</span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="tab-1" class="bg-white">
                    <div class="tab-category mt-5">
                        <p class="text-menu-tab">TOP ITEM</p>
                    </div>
                    <div class="slick-item voucher-logo">
                        <div class="content-slide">
{{--                            @if(isset($category_child))--}}
{{--                                @foreach($category_child as $value)--}}
{{--                                    <div class="item-content">--}}
{{--                                        <div class="image">--}}
{{--                                            <img width="185px" height="152px" src="{{$value->image}}">--}}
{{--                                        </div>--}}
{{--                                        <div class="title">--}}
{{--                                            <span>{{$value->name}}</span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @endforeach--}}
{{--                            @endif--}}
                            <div class="item-content">
                                <div class="image">
                                    <img src="<?php echo asset("$path/ellipse_1.png")?>" alt="">
                                </div>
                                <div class="title">
                                    <p class="tl-1">sự kiện - giải trí</p>
                                    <p class="tl-2"><span>Từ</span> 35.000đ</p>
                                </div>
                            </div>
                            <div class="item-content">
                                <div class="image">
                                    <img src="<?php echo asset("$path/ellipse_2.png")?>" alt="">
                                </div>
                                <div class="title">
                                    <p class="tl-1">Sức khoẻ - Làm đẹp</p>
                                    <p class="tl-2"><span>Từ</span> 25.000đ</p>
                                </div>
                            </div>
                            <div class="item-content">
                                <div class="image">
                                    <img src="<?php echo asset("$path/ellipse_3.png")?>" alt="">
                                </div>
                                <div class="title">
                                    <p class="tl-1">ăn uống</p>
                                    <p class="tl-2"><span>Từ</span> 9.150đ</p>
                                </div>
                            </div>
                            <div class="item-content">
                                <div class="image">
                                    <img src="<?php echo asset("$path/ellipse_4.png")?>" alt="">
                                </div>
                                <div class="title">
                                    <p class="tl-1">du lịch</p>
                                    <p class="tl-2"><span>Từ</span> 18.000đ</p>
                                </div>
                            </div>
                            <div class="item-content">
                                <div class="image">
                                    <img src="<?php echo asset("$path/ellipse_5.png")?>" alt="">
                                </div>
                                <div class="title">
                                    <p class="tl-1">thẻ cào điện thoại</p>
                                    <p class="tl-2"><span>Từ</span> 1.500đ</p>
                                </div>
                            </div>
                            <div class="item-content">
                                <div class="image">
                                    <img src="<?php echo asset("$path/ellipse_6.png")?>" alt="">
                                </div>
                                <div class="title">
                                    <p class="tl-1">khoá học đào tạo</p>
                                    <p class="tl-2"><span>Từ</span> 8.999đ</p>
                                </div>
                            </div>
                            <div class="item-content">
                                <div class="image">
                                    <img src="<?php echo asset("$path/ellipse_4.png")?>" alt="">
                                </div>
                                <div class="title">
                                    <p class="tl-1">du lịch</p>
                                    <p class="tl-2"><span>Từ</span> 18.000đ</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body-seaech flex container">
                    <?php
                    $path = "assets/frontend/Icon/products";
                    ?>
                    <div class="fillter">
                        <div class="title-fillter flex">
                            <div class="icon_fillter">
                                <object data="icon/search/icon_fillter.svg" width="12px"></object>
                            </div>
                            <div class="title-text">BỘ LỌC TÌM KIẾM</div>
                        </div>
                        <div class="filter-catagory">
                            <p class="title color-red">Voucher & Dịch vụ</p>
{{--                            @if(isset($category_child))--}}
{{--                                @foreach($category_child as $value)--}}
{{--                                    <div class="flex content">--}}
{{--                                        <input type="checkbox">--}}
{{--                                        <p class="text">{{$value['name']}}</p>--}}
{{--                                    </div>--}}
{{--                                @endforeach--}}
{{--                            @endif--}}
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Nhà hàng - Ăn uống</p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Du lịch - Khách sạn</p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Sức khỏe - Làm đẹp</p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Sự kiện - Giải trí</p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Khóa học</p>
                            </div>
                            <p class="load-more">Xem Thêm ></p>
                        </div>
                        <div class="filter-catagory">
                            <p class="title">Nơi bán</p>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Hà Nội</p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">TP.Hồ Chí Minh</p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Thái nguyên</p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Vĩnh Phúc</p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Hải Phòng </p>
                            </div>
                            <p class="load-more">Xem Thêm ></p>
                        </div>
                        <div class="filter-catagory">
                            <p class="title">Đơn vị vận chuyển</p>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">NowShip</p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Ninja Van</p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">J&T Express</p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Giao hàng tiết kiệm</p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Giao hàng nhanh</p>
                            </div>
                            <p class="load-more">Xem Thêm ></p>
                        </div>
                        <div class="filter-catagory form-fillter">
                            <p class="title">Khoảng giá</p>
                            <div class="form-fillter-price flex">
                                <input type="text" placeholder="TỪ">
                                <span class="icon-space">-</span>
                                <input type="text" placeholder="ĐẾN">
                            </div>
                            <button class="btn">Áp dụng</button>
                        </div>
                        <div class="filter-catagory">
                            <p class="title">Loại shop</p>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Shopee Mall</p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Shop yêu thích</p>
                            </div>
                        </div>
                        <div class="filter-catagory">
                            <p class="title">Tình trạng</p>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Sản phẩm mới </p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Sản phẩm đã từng </p>
                            </div>
                        </div>
                        <div class="filter-catagory">
                            <p class="title">Đánh giá </p>
                            <div class="flex content">
                                <object data="icon/search/icon_star_five.svg"></object>
                            </div>
                            <div class="flex content">
                                <object data="icon/search/icon_star_for.svg"></object>
                                <p class="text">trở lên </p>
                            </div>
                            <div class="flex content">
                                <object data="icon/search/icon_star_three.svg"></object>
                                <p class="text">trở lên </p>
                            </div>
                            <div class="flex content">
                                <object data="icon/search/icon_star_tow.svg"></object>
                                <p class="text">trở lên </p>
                            </div>
                            <div class="flex content">
                                <object data="icon/search/icon_star_one.svg"></object>
                                <p class="text">trở lên </p>
                            </div>
                        </div>
                        <div class="filter-catagory">
                            <p class="title">Dịch vụ và khuyến mãi</p>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Freeship Xtra </p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Hoàn xu Xtra </p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Đang giảm giá </p>
                            </div>
                            <div class="flex content">
                                <input type="checkbox">
                                <p class="text">Miễn phí vận chuyển </p>
                            </div>
                            <p class="load-more">Thêm
                                <object width="7px" data="icon/search/icon_arrow_down.svg"></object>
                            </p>
                        </div>
                        <button class="btn clear-all">Xóa tất cả</button>
                    </div>
                    <div class="container-left">
                        <div class="sort-container flex">
                            <div class="content-sort flex">
                                <p class="title-sort">Sắp Xếp Theo</p>
                                <button class="btn-sort active">Liên Quan</button>
                                <button class="btn-sort ">Mới Nhất</button>
                                <button class="btn-sort ">Bán Chạy</button>
                                <button class="btn-sort btn-option relative">Giá
                                    <object data="icon/search/btn_arrow_down.svg"></object>
                                </button>
                            </div>
                            <div class="flex paginate">
                                <div class="content-paginate">
                                    <span class="in-paginate">5</span>/<span class="all-paginate">10</span>
                                </div>
                                <button class="btn-sort next-paginate relative">
                                    <object data="icon/search/polygon_left.svg"></object>
                                </button>
                                <button class="btn-sort prev-paginate relative">
                                    <object data="icon/search/polygon_right.svg"></object>
                                </button>
                            </div>
                        </div>
                        <div class="product-search voucher-product">
                            <?php
                            $path = "assets/frontend/Icon/Vuocher";
                            $pathSlide = "assets/frontend/Icon/home/Danh_muc";
                            ?>
{{--                            <div class="container-section-product">--}}
{{--                                @if(isset($product))--}}
{{--                                    @foreach($product as $value)--}}
{{--                                        <div class="content border-box">--}}
{{--                                            <a href="{{url('details_product/'.$value->id)}}">--}}
{{--                                                <div class="img-product">--}}
{{--                                                    <img width="185px" height="190px" src="{{$value->image}}">--}}
{{--                                                </div>--}}
{{--                                                <div class="title-product-n relative">--}}
{{--                                                    <span>{{$value->name}}</span>--}}
{{--                                                </div>--}}
{{--                                                <div class="product-price">--}}
{{--                                                    <p class="price">{{number_format($value->price_discount)}}đ--}}
{{--                                                        - {{number_format($value->price)}}đ</p>--}}
{{--                                                    <p class="pass">Giá CTV: ******** <span>Lời: 50.000</span></p>--}}
{{--                                                    <div class="form btn-sign-up relative">--}}
{{--                                                        <a href="#">Đăng ký CTV để xem giá</a>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="review">--}}
{{--                                                    <img src="<?php echo asset("$pathSlide/icon-review.png")?>">--}}
{{--                                                    <p>Đã bán {{$value->is_sell}}</p>--}}
{{--                                                </div>--}}
{{--                                                <div class="review">--}}
{{--                                                    <img style="width: 20px;"--}}
{{--                                                         src="<?php echo asset("$pathSlide/icon-like.png")?>">--}}
{{--                                                    <p>{{$value->warehouse}}</p>--}}
{{--                                                </div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    @endforeach--}}
{{--                                @endif--}}
{{--                            </div>--}}
                            <div class="container-section-product">
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_1.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_2.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_3.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_4.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_5.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>

                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_1.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_2.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_3.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_4.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_5.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>

                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_1.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_2.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_3.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_4.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_5.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>

                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_1.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_2.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_3.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_4.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_5.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>

                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_1.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_2.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_3.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_4.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="content border-box">
                                    <a href="{{url('details_product/2')}}">
                                        <div class="img-product">
                                            <img width="185px" height="190px" src="<?php echo  asset("$path/vuocher_5.png")?>">
                                        </div>
                                        <div class="title-product-n relative">
                                            <span>Ưu đãi 20k cho chuyến xe GrabCar, GrabBike </span>
                                        </div>
                                        <div class="product-price">
                                            <p class="price">270.000 - 370.000</p>
                                        </div>
                                        <div class="review">
                                            <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                            <p>Đã bán 10</p>
                                        </div>
                                        <div class="review">
                                            <img style="width: 20px;"
                                                 src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                            <p>Hà Nội</p>
                                        </div>
                                    </a>
                                </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: Homepage-->

            <!-- BEGIN: Footer-->
        @include('frontend.base.footer')
        <!-- END: Footer-->
        </div>
    </div>
</div>

@include('frontend.base.script')
<script src="js/Voucher/index.js"></script>
</body>
<!-- END: Body-->
</html>
