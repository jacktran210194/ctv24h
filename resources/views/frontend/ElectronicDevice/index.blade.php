<?php

use Illuminate\Support\Facades\URL;
use App\Models\ProductModel;
?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/suggestion/suggestion.css">
<link rel="stylesheet" type="text/css" href="css/ElectronicDevice/style.css">
<link rel="stylesheet" type="text/css" href="css/search/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <!-- BEGIN: Header-->
    <!-- END: Header-->
    <!-- BEGIN: Homepage-->
    <div class="electronicdevice">
        <div id="img">
            <div class="container p-0">
                <img class="img img-fluid"
                     src="Icon/ElectronicDevice/banner_30-4_H.png"
                     alt="">
            </div>
        </div>
        <div id="category" class="">
            <div class="container menu-category pt-5 p-0">
                <div class="body-seaech flex container">
                    <?php
                    $path = "assets/frontend/Icon/products";
                    ?>
                    <div class="fillter">
                            <div class="title-fillter flex">
                                <div class="icon_fillter">
                                    <object data="icon/search/icon_fillter.svg" width="12px"></object>
                                </div>
                                <div class="title-text">BỘ LỌC TÌM KIẾM</div>
                            </div>
                            <div class="filter-catagory">
                                <p class="title">Theo danh mục</p>
                                @if(isset($category))
                                    <div class="sub-filter">
                                        <p class="title">{{$category->name}}</p>
                                        <div class="dropdown-filter" style="display: none">
                                            @foreach( $category_product_sub_child as $key )
                                                <div class="flex content content-filter">
                                                    <input type="checkbox" id="{{$key->id}}">
                                                    <p class="text">{{$key->name}}
                                                    </p>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                 @else
                                    <h3>Tạm thời danh mục chưa có sản phẩm</h3>
                                @endif
                                {{--                <p class="load-more">Xem Thêm ></p>--}}
                            </div>
                            <div class="filter-catagory">
                            <p class="title">Nơi bán</p>
                            @if(count($list_city) && $list_city)
                                @php $i=0; @endphp
                                @foreach($list_city as $value_city)
                                    @php $i++; @endphp
                                    @if($i <= 5)
                                        <div class="flex content filter-place">
                                            <input class="check_place" type="checkbox" value="{{$value_city->name}}">
                                            <p class="text">{{$value_city->name}}</p>
                                        </div>
                                    @endif
                                @endforeach
                            @endif

                            @if(count($list_city) && $list_city)
                                @php $y=0; @endphp
                                @foreach($list_city as $value_city)
                                    @if($y++ >= 5 )
                                        <div style="display: none;" class="load_more_city flex content filter-place">
                                            <input class="check_place" type="checkbox" value="{{$value_city->name}}">
                                            <p class="text">{{$value_city->name}}</p>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                            <p class="load-more btn_load_more" style="cursor: pointer;">Xem Thêm ></p>
                            <p class="load-more btn_hide_more" style="cursor: pointer; display: none;">Ẩn bớt ></p>
                        </div>
                            <div class="filter-catagory">
                                <p class="title">Đơn vị vận chuyển</p>
                                @if(isset($shipping))
                                    @foreach($shipping as $value)
                                        <div class="flex content filter-shipping">
                                            <input type="checkbox" value="{{$value->id}}">
                                            <p class="text">{{$value ->name }}</p>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="filter-catagory form-fillter">
                                <p class="title">Khoảng giá</p>
                                <div class="form-fillter-price flex">
                                    <input type="number" placeholder="TỪ" class="price-filter-1">
                                    <span class="icon-space">-</span>
                                    <input type="number" placeholder="ĐẾN" class="price-filter-2">
                                </div>
                                <button class="btn btn-filter">Áp dụng</button>
                            </div>
                            <div class="filter-catagory">
                                <p class="title">Loại shop</p>
                                <div class="flex content">
                                    <input type="checkbox" class="preventdefault">
                                    <p class="text">Shopee Mall</p>
                                </div>
                                <div class="flex content">
                                    <input type="checkbox" class="preventdefault">
                                    <p class="text">Shop yêu thích</p>
                                </div>
                            </div>
                            <div class="filter-catagory">
                                <p class="title">Tình trạng</p>
                                <div class="flex content filter-status">
                                    <input type="checkbox" value="1">
                                    <p class="text">Sản phẩm mới </p>
                                </div>
                                <div class="flex content filter-status">
                                    <input type="checkbox" value="0">
                                    <p class="text">Sản phẩm đã từng </p>
                                </div>
                            </div>
                            <div class="filter-catagory">
                                <p class="title">Đánh giá </p>
                                <div class="flex content">
                                    <button class="filter-ratings" value="5">
                                        <object data="icon/search/icon_star_five.svg"></object>
                                    </button>
                                </div>
                                <div class="flex content">
                                    <button class="d-flex align-items-center w-100 filter-ratings" value="4">
                                        <object data="icon/search/icon_star_for.svg"></object>
                                        <p class="text">trở lên </p>
                                    </button>
                                </div>
                                <div class="flex content">
                                    <button class="d-flex align-items-center w-100 filter-ratings" value="3">
                                        <object data="icon/search/icon_star_three.svg"></object>
                                        <p class="text">trở lên </p>
                                    </button>
                                </div>
                                <div class="flex content">
                                    <button class="d-flex align-items-center w-100 filter-ratings" value="2">
                                        <object data="icon/search/icon_star_tow.svg"></object>
                                        <p class="text">trở lên </p>
                                    </button>
                                </div>
                                <div class="flex content">
                                    <button class="d-flex align-items-center w-100 filter-ratings" value="1">
                                        <object data="icon/search/icon_star_one.svg"></object>
                                        <p class="text">trở lên </p>
                                    </button>
                                </div>
                            </div>
                            <div class="filter-catagory">
                                <p class="title">Dịch vụ và khuyến mãi</p>
                                <div class="flex content">
                                    <input type="checkbox" class="preventdefault">
                                    <p class="text">Freeship Xtra </p>
                                </div>
                                <div class="flex content">
                                    <input type="checkbox" class="preventdefault">
                                    <p class="text">Hoàn xu Xtra </p>
                                </div>
                                <div class="flex content">
                                    <input type="checkbox" class="preventdefault">
                                    <p class="text">Đang giảm giá </p>
                                </div>
                                <div class="flex content">
                                    <input type="checkbox" class="preventdefault">
                                    <p class="text">Miễn phí vận chuyển </p>
                                </div>
                                <p class="load-more">Thêm
                                    <object width="7px" data="icon/search/icon_arrow_down.svg"></object>
                                </p>
                            </div>
                            <button class="btn clear-all">Xóa tất cả</button>
                        </div>
                    <div class="container-left">
                        <div class="sort-container flex">
                            <div class="content-sort flex">
                                <p class="title-sort">Sắp Xếp Theo</p>
                                <button class="btn-sort <?= $type === 'related' ? 'active' : '' ?>">
                                    <a style="color: <?= $type === 'related' ? '#fff' : '' ?>"
                                       href="<?= URL::to('ElectronicDevice/' . $category->id . '?type=related') ?>">
                                        Liên Quan
                                    </a>
                                </button>
                                <button class="btn-sort <?= $type === 'new' ? 'active' : '' ?>">
                                    <a style="color: <?= $type === 'new' ? '#fff' : '' ?>"
                                       href="<?= URL::to('ElectronicDevice/' . $category->id . '?type=new') ?>">
                                        Mới Nhất
                                    </a>
                                </button>
                                <button class="btn-sort <?= $type === 'best_seller' ? 'active' : '' ?>">
                                    <a style="color: <?= $type === 'best_seller' ? '#fff' : '' ?>"
                                       href="<?= URL::to('ElectronicDevice/' . $category->id . '?type=best_seller') ?>">
                                        Bán Chạy
                                    </a>
                                </button>
                                <button class="btn-sort btn-option relative <?= $type === 'price' ? 'active' : '' ?>">
                                    <a style="color: <?= $type === 'price' ? '#fff' : '' ?>"
                                       href="<?= URL::to('ElectronicDevice/' . $category->id . '?type=price') ?>">
                                        <object data="icon/search/btn_arrow_down.svg">
                                        </object>
                                        Giá
                                    </a>
                                </button>
                            </div>
                            <div class="flex paginate">
                                <div class="content-paginate">
                                    <span class="in-paginate">5</span>/<span class="all-paginate">10</span>
                                </div>
                                <button class="btn-sort next-paginate relative">
                                    <object data="icon/search/polygon_left.svg"></object>
                                </button>
                                <button class="btn-sort prev-paginate relative">
                                    <object data="icon/search/polygon_right.svg"></object>
                                </button>
                            </div>
                        </div>
                        <div class="product-search">
                            <?php
                            $path = "assets/frontend/Icon/products";
                            $pathSlide = "assets/frontend/Icon/home/Danh_muc";
                            ?>
                            <div class="container-section-product">
                                @if(count($data_product))
                                    @foreach ($data_product as $value)
                                        @php
                                            $product_value = \App\Models\AttributeValueModel::where('product_id', $value->id)->get();
                                            $price_ctv_min = \App\Models\AttributeValueModel::where('product_id', $value->id)->min('price_ctv');
                                            $price_ctv_max = \App\Models\AttributeValueModel::where('product_id', $value->id)->max('price_ctv');
                                        @endphp
                                        <?php
                                        $min = 0;
                                        $max = 0;
                                        foreach ($product_value as $item){
                                            $ctv = $item->price - $item->price_ctv;
                                            if ($min == 0){
                                                $min = $ctv;
                                            }elseif ($min > $ctv){
                                                $min = $ctv;
                                            }
                                            if ($max == 0){
                                                $max = $ctv;
                                            }elseif ($max < $ctv){
                                                $max = $ctv;
                                            }
                                        }
                                        ?>
                                        <div class="content border-box relative sp-product-content">
                                            <a href="{{url('details_product/'.(isset($value->product_id)?$value->product_id:$value->id))}}">
                                                <div class="img-product">
                                                    <img src="{{$value->image}}" class="lazy">
                                                </div>
                                                <div class="item-content-product d-flex justify-content-between flex-column">
                                                    <div class="title-product-n relative">
                                                        <p class="card-title">{{$value->name}}</p>
                                                    </div>
                                                        <div class="product-price">
                                                            @if($value->price_discount == $value->price)
                                                                <p class="price"><span>{{number_format($value->price_discount)}} đ</span></p>
                                                            @else
                                                                <p class="price"><span>{{number_format($value->price_discount)}} đ</span> -
                                                                    <span class="price-sale">{{number_format($value->price)}} đ</span></p>
                                                            @endif

                                                            @if($price_ctv_min == $price_ctv_max)
                                                                <div class="d-flex justify-content-between">
                                                                    <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ</p>
                                                                </div>
                                                            @else
                                                                <div class="d-flex justify-content-between">
                                                                    <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ - {{number_format($price_ctv_max)}}đ</p>
                                                                </div>
                                                            @endif

                                                            @if($min == $max)
                                                                <div class="d-flex justify-content-between" style="margin-top: 10px">
                                                                    <p>Lời: <span class="text-red">{{number_format($min)}}đ</span>
                                                                    </p>
                                                                </div>
                                                            @else
                                                                <div class="d-flex justify-content-between" style="margin-top: 10px">
                                                                    <p>Lời: <span class="text-red">{{number_format($min)}}đ - {{number_format($max)}}đ</span>
                                                                    </p>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    <div class="review">
                                                        @if($value->avg_rating == 0)
                                                            <img src="<?php echo asset("$path/0_sao.png")?>">
                                                        @else
                                                            @if($value->avg_rating == 1)
                                                                <img src="<?php echo asset("$path/1_sao.png")?>">
                                                            @elseif($value->avg_rating == 2)
                                                                <img src="<?php echo asset("$path/2_sao.png")?>">
                                                            @elseif($value->avg_rating == 3)
                                                                <img src="<?php echo asset("$path/3_sao.png")?>">
                                                            @elseif($value->avg_rating == 4)
                                                                <img src="<?php echo asset("$path/4_sao.png")?>">
                                                            @elseif($value->avg_rating == 5)
                                                                <img src="<?php echo asset("$path/5_sao.png")?>">
                                                            @elseif($value->avg_rating > 1 && $value->avg_rating < 2)
                                                                <img src="<?php echo asset("$path/1_5_sao.png")?>">
                                                            @elseif($value->avg_rating > 2 && $value->avg_rating < 3)
                                                                <img src="<?php echo asset("$path/2_5_sao.png")?>">
                                                            @elseif($value->avg_rating > 3 && $value->avg_rating < 4)
                                                                <img src="<?php echo asset("$path/3_5_sao.png")?>">
                                                            @elseif($value->avg_rating > 4 && $value->avg_rating < 5)
                                                                <img src="<?php echo asset("$path/4_5_sao.png")?>">
                                                            @endif
                                                        @endif
                                                        <p>Đã bán {{$value->is_sell}}</p>
                                                    </div>
                                                    <div class="review">
                                                        <img style="width: 20px;"
                                                             src="Icon/home/Danh_muc/icon-like.png">
                                                        <p>{{$value->place}}</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                    @else
                                    <h3 class="text-center">Hiện không có sản phẩm nào ở danh mục này</h3>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: Homepage-->

            <!-- BEGIN: Footer-->
        @include('frontend.base.footer')
        <!-- END: Footer-->
        </div>
    </div>
</div>

@include('frontend.base.script')
<script src="js/ElectronicDevice/index.js"></script>
<script src="js/base/filter.js"></script>
</body>
<!-- END: Body-->
</html>
