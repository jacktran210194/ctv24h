<div class="bg-white" style="border-radius: 18px; padding: 22px; width: 327px">
    <h3 class="font-weight-bold" style="font-size: 17px;margin-bottom: 20px">Tạo danh mục</h3>
    <div class="ct-container">
        @if(isset($category))
            @foreach($category as $key)
                <div class="d-flex align-items-center" style="margin-bottom: 20px">
                    <div class="radio" data-value="{{$key->id}}"></div>
                    <p class="m-0 text-capitalize" style="font-size: 14px; color: #333333;padding-left: 20px">{{ $key->name }}</p>
                </div>
            @endforeach
        @endif
        <div class="d-flex justify-content-sm-center align-items-center w-100" style="margin-top: 30px">
            @if(isset($shop->active))
                <button class="btn-category-collaborator text-center font-weight-bold"
                    style="border: none;padding: 15px; width: 178px;background: linear-gradient(180deg, #D1292F 0%, #FF5722 155.26%);border-radius: 8px;font-size: 16px; color: #ffffff ">Tạo danh mục</button>
                @else
                <button  class="text-center font-weight-bold btn-close-popup"
                        style="border: none;padding: 15px; width: 178px;background: linear-gradient(180deg, #D1292F 0%, #FF5722 155.26%);border-radius: 8px;font-size: 16px; color: #ffffff ">Bạn chưa cài đặt shop</button>
            @endif
        </div>
    </div>
</div>
