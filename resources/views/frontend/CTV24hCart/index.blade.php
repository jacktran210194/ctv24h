<?php
use Illuminate\Support\Facades\URL;
$data_customer = Session::get('data_customer');
?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/CTV24hCart/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->
    <?php
    $path = "assets/frontend/Icon/CTV24hCart";
    ?>
    <div class="container body-page">
        <div class="banner-page">
            <img style="width: 100%" src="<?php echo asset("$path/banner.png")?>">
        </div>
        <div class="offer-monopoly">
            <div class="container-offer">
                <div class="header-offer">
                    <img src="<?php echo asset("$path/Group.png")?>">
                    <h3 class="title">ƯU ĐÃI ĐỘC QUYỀN </h3>
                </div>
                <div class="infor-offer">
                    <div class="offer">
                        <div class="icon_offer">
                            <img src="<?php echo asset("$path/icon_money.png")?>">
                        </div>
                        <div class="content-offer">
                            <p class="title-content-offer">HOÀN TIỀN MỌI CHI TIÊU </p>
                            <p class="text-content-offer">Hoàn tiền lên đến 10% cho chi tiêu tại CTV24H, siêu thị, ăn uống, grab và các chi tiêu khác </p>
                        </div>
                    </div>
                    <div class="offer">
                        <div class="icon_offer">
                            <img src="<?php echo asset("$path/icon_giff.png")?>">
                        </div>
                        <div class="content-offer">
                            <p class="title-content-offer">nHIỀU QUÀ TẶNG ONLINE  </p>
                            <p class="text-content-offer">Tặng voucher mua sắm tại shopee, tặng quà vào sinh nhật </p>
                        </div>
                    </div>
                    <div class="offer">
                        <div class="icon_offer">
                            <img src="<?php echo asset("$path/icon_shipping.png")?>">
                        </div>
                        <div class="content-offer">
                            <p class="title-content-offer">MIỄN PHÍ VẬN CHUYỂN QUANH NĂM </p>
                            <p class="text-content-offer">Tặng lên đến 04 voucher miễn phí vận chuyển mỗi tháng </p>
                        </div>
                    </div>
                    <div class="offer">
                        <div class="icon_offer">
                            <img src="<?php echo asset("$path/icon_cart_phone.png")?>">
                        </div>
                        <div class="content-offer">
                            <p class="title-content-offer">CHẠM TAY MỞ THẺ NGAY</p>
                            <p class="text-content-offer">Miễn phí phát hành và thường niên, mở thẻ phi vật lý nhanh chóng</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="feature-platinum">
            <div class="platinum">
                <div class="content-left width-50">
                    <img style="width: 100%" src="<?php echo asset("$path/platium-cart.png")?>">
                </div>
                <div class="content-right width-50">
                    <div class="title-platinum header-offer">
                        <img src="<?php echo asset("$path/Group.png")?>">
                        <h3 class="title">Thẻ tín dụng super platinum</h3>
                    </div>
                    <ul class="content-platinum">
                        <li style="display: flex; align-items: center">
                            <img src="<?php echo asset("$path/icon_checked.png")?>">
                            <span>Hoàn tiền đến 10% cho mọi chi tiêu</span>
                        </li>
                        <li style="display: flex; align-items: center">
                            <img src="<?php echo asset("$path/icon_checked.png")?>">
                            <span>Nhận voucher khi mở thẻ thành công</span>
                        </li>
                        <li style="display: flex; align-items: center">
                            <img src="<?php echo asset("$path/icon_checked.png")?>">
                            <span>Nhận voucher sinh nhật hoàn 50% tối đa 100k xu</span>
                        </li>
                        <li style="display: flex; align-items: center">
                            <img src="<?php echo asset("$path/icon_checked.png")?>">
                            <span>Nhân thêm 4 lượt miễn phí vận chuyển mỗi tháng </span>
                        </li>
                        <li style="display: flex; align-items: center">
                            <img src="<?php echo asset("$path/icon_checked.png")?>">
                            <span>Trả góp 0% lãi suất</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="platinum">
                <div class="content-right width-50">
                    <div class="title-platinum header-offer">
                        <img src="<?php echo asset("$path/Group.png")?>">
                        <h3 class="title">Thẻ tín dụng CTV24H platinum</h3>
                    </div>
                    <ul class="content-platinum">
                        <li style="display: flex; align-items: center">
                            <img src="<?php echo asset("$path/icon_checked.png")?>">
                            <span>Hoàn tiền đến 6% cho mọi chi tiêu</span>
                        </li>
                        <li style="display: flex; align-items: center">
                            <img src="<?php echo asset("$path/icon_checked.png")?>">
                            <span>Nhận voucher khi mở thẻ thành công</span>
                        </li>
                        <li style="display: flex; align-items: center">
                            <img src="<?php echo asset("$path/icon_checked.png")?>">
                            <span>Nhận voucher sinh nhật hoàn 50% tối đa 100k xu</span>
                        </li>
                        <li style="display: flex; align-items: center">
                            <img src="<?php echo asset("$path/icon_checked.png")?>">
                            <span>Nhân thêm 2 lượt miễn phí vận chuyển mỗi tháng </span>
                        </li>
                        <li style="display: flex; align-items: center">
                            <img src="<?php echo asset("$path/icon_checked.png")?>">
                            <span>Trả góp 0% lãi suất</span>
                        </li>
                    </ul>
                </div>
                <div class="content-left width-50">
                    <img style="width: 100%" src="<?php echo asset("$path/platum-cart-2.png")?>">
                </div>
            </div>
        </div>
        <div class="guide-open-cart">
            <div class="header-offer">
                <img src="<?php echo asset("$path/Group.png")?>">
                <h3 class="title">CÁC BƯỚC MỞ THẺ</h3>
            </div>
            <div class="guide">
                <div class="user-guide">
                    <div class="box-guide">
                        <div class="icon_guide">
                            <img src="<?php echo asset("$path/ellipse_phone.png")?>" class="index_1">
                            <img src="<?php echo asset("$path/vector.png")?>" class="vector">
                        </div>
                        <div class="content-guide">
                            <P class="title-guide">BƯỚC 01</P>
                            <P class="content-guide">Đăng ký thông tin mở thẻ tại Website/ứng dụng </P>
                        </div>
                    </div>
                    <div class="box-guide">
                        <div class="icon_guide">
                            <img src="<?php echo asset("$path/ellipse_file.png")?>" class="index_1">
                            <img src="<?php echo asset("$path/vector.png")?>" class="vector">
                        </div>
                        <div class="content-guide">
                            <P class="title-guide">BƯỚC 02</P>
                            <P class="content-guide">Nộp hồ sơ và bổ sung các chứng từ nếu cần</P>
                        </div>
                    </div>
                    <div class="box-guide">
                        <div class="icon_guide">
                            <img src="<?php echo asset("$path/ellipse_cart.png")?>" class="index_1">
                            <img src="<?php echo asset("$path/vector.png")?>" class="vector">
                        </div>
                        <div class="content-guide">
                            <P class="title-guide">BƯỚC 03</P>
                            <P class="content-guide">Kích hoạt thẻ và mua sắm</P>
                        </div>
                    </div>
                    <div class="box-guide">
                        <div class="icon_guide">
                            <img src="<?php echo asset("$path/ellipse_checked.png")?>" class="index_1">
                            <img src="<?php echo asset("$path/vector.png")?>" class="vector">
                        </div>
                        <div class="content-guide">
                            <P class="title-guide">BƯỚC 04</P>
                            <P class="content-guide">Khách hàng nhận thẻ theo địa chỉ đã đăng ký </P>
                        </div>
                    </div>
                </div>
                <button class="btn-open-cart">
                    <a href="{{url('/')}}" class="preventdefault">
                        <span>Mở thẻ ngay</span>
                        <img src="<?php echo asset("$path/Vector_arrow.png")?>" style="width: 23px">
                    </a>
                </button>
            </div>
        </div>
    </div>
<!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
