<div class="section-danhmuc">
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    ?>
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <img class="lazy" src="Icon/home/Danh_muc/icon-logo-homepage.png" alt="" >
            </div>
            <h2>DANH MỤC</h2>
        </div>
        <div class="link"><a href="#">Xem Thêm</a></div>
    </div>
    <div class="product-section relative">
        <div class="container-section">
            <?php if(isset($dataCategory)) :?>
            <?php $i = 0; ?>
            @foreach($dataCategory as $value)
                <?php if($i == 0) :?>
                    <div class="product">
                        <?php endif ;?>
                        <a href="{{url('ElectronicDevice/'.$value->id)}}">
                            <div class="content-product">
                                <div class="img-product">
                                    <img style="object-fit: contain;" class="lazy" src="{{$value->image}}" alt="">
                                </div>
                                <div class="title-product">
                                    <p>{{$value->name}}</p>
                                </div>
                            </div>
                        </a>
                        <?php
                        $i++;
                        if ($i == 2){

                        $i = 0;
                        ?>
                    </div>
                <?php
                }
                ?>
            @endforeach
            <?php endif; ?>
        </div>
    </div>
</div>
