<?php
$data_customer = Session::get('data_customer');
?>
<div class="ctn-chat flex">
    <button class="close-popup-chat"><img src="Icon/base/black-arrow-gif.png" width="20px"></button>
    <div class="tab-bar-chat">
        <h2 class="title-chat">CHAT</h2>
        <form class="search-customer">
            <input type="text" placeholder="Tìm Kiếm Khách Hàng">
            <label class="icon-search">
                <object data="Icon/base/Search.svg"></object>
            </label>
        </form>
        <?php foreach ($list_mes as $value): ?>
        <div class="item-chat-<?= $value['id'] ?> item-chat-data customer flex <?= $value['active'] ? 'active' : 'inactive' ?>">
            <a href="<?= URL::to('chat/popup-chat/' . $value['id']) ?>" class="d-flex">
                <div class="img-customer">
                    <img
                        src="<?= !empty($value['avt_customer']) ? $value['avt_customer'] : 'Icon/chat/avarta.png' ?>">
                </div>
                    <div class="name-customer">
                        <p class="name"><?= $value['name_customer'] ?></p>
                         <p class="messenger messenger-last-<?= $value['id'] ?>"><?= $value['messenger'] ?></p>
                    </div>
            </a>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="form-messenger">
        <?php if (count($list_mes)) : ?>
            <div class="flex header-form customer-active"
                 data-group-id="<?= $group_messenger_id ?? '' ?>" data-id="<?= $id_customer_active ?? '' ?>"
                data-customer-id="<?= $data_customer->id ?? '' ?>"
            >
                <div class="img-customer">
                    <img src="<?= !empty($avt_customer_active) ? $avt_customer_active : 'Icon/chat/avarta.png' ?>">
                </div>
                <div class="name-customer">
                    <h2><?= $name_customer_active ?? '' ?></h2>
                </div>
            </div>
            <div class="chat-content list-messenger-<?= $group_messenger_id ?? '' ?>">
                <?php foreach ($list_messenger as $value) : ?>
                <?php if ($value['active']) : ?>
                <div class="flex ctn-chat-content shop-chat">
                    <div class="text-chat">
                        <label><?= $value['messenger'] ?></label>
                    </div>
                    <div class="img-customer">
                        <img
                            src="<?= !empty($value['user_send_avt']) ? $value['user_send_avt'] : 'Icon/chat/avarta.png' ?>"
                            >
                    </div>
                </div>
                <?php else: ?>
                <div class="flex ctn-chat-content customer-chat">
                    <div class="img-customer">
                        <img
                            src="<?= !empty($value['user_receiver_avt']) ? $value['user_receiver_avt'] : 'Icon/chat/avarta.png' ?>"
                            >
                    </div>
                    <div class="text-chat">
                        <label><?= $value['messenger'] ?></label>
                    </div>
                </div>
                <?php endif; ?>
                <?php endforeach; ?>
            </div>
        <div class="footer-form">
            <ul class="nav-form flex">
                <li class="sub-nav bg-nav-1">Kiểm Tra Hàng</li>
                <li class="sub-nav bg-nav-2">Khách Sỉ</li>
                <li class="sub-nav bg-nav-3">Đã Mua Hàng</li>
                <li class="sub-nav bg-nav-4">Đã Tư Vấn</li>
                <li class="sub-nav bg-nav-5">Cần chăm sóc</li>
            </ul>
            <div class="form-send-messenger flex">
                <input type="text" placeholder="Gửi tin nhắn" id="form-send-messenger" class="input-messenger">
                <button class="icon-form"><img style="width: 20px" src="Icon/base/icon_file.png"></button>
                <button class="icon-form"><img style="width: 20px" src="Icon/base/icon_smail.png"></button>
                <button class="icon-form"><img style="width: 20px" src="Icon/base/icon_cart.png"></button>
                <button class="icon-form"><img style="width: 20px" src="Icon/base/icon_calendar.png"></button>
                <button class="icon-form btn-send" id="btn-send-messenger"><img src="Icon/base/icon_send.png" style="width: 16px"></button>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
<script>
    //enter input
    let input = document.getElementById("form-send-messenger");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("btn-send-messenger").click();
        }
    });
    //end enter
</script>
