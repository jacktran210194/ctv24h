<div class="section section-san-pham-trend">
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    ?>
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <img src="<?php echo asset("$path/icon-logo-homepage.png")?>">
            </div>
            <h2>sản phẩm TREND</h2>
        </div>
        <div class="link preventdefault"><a href="#">Xem Thêm</a></div>
    </div>
    <div class="product-section relative">
        <div class="slide-product relative">

{{--            <button class="next-arrow btn-arrow btn-arrow-small prev-arrow-trend">--}}
{{--                <object data="Icon/base/next_arrow.svg" width="7px"></object>--}}
{{--            </button>--}}
{{--            <button class="prev-arrow btn-arrow btn-arrow-small next-arrow-trend">--}}
{{--                <object data="Icon/base/pr_arrow.svg" width="7px"></object>--}}
{{--            </button>--}}
            <div class="container-section-product slide-section-trend">
                <?php if(isset($dataTrendingProduct)) :?>
                <?php foreach ($dataTrendingProduct as $value) :?>
                    <div class="container-slide content border-box">
                        <a href="{{url('details_product/'.$value->id)}}" class="d-flex flex-column justify-content-between">
                            <div class="img-product">
                                <img style="height: 180px;" src="{{$value->image}}">
                            </div>
                            <div class="title-product-n relative" style="height: 34px">
                                <span>{{$value->name}}</span>
                            </div>
                            <div class="product-price">
                                <p class="price">{{number_format($value->price_discount)}}đ - {{number_format($value->price)}}
                                    đ</p>
                                <p class="pass">Giá CTV: ******** <span>Lời: 50.000</span></p>
{{--                                <div class="form btn-sign-up relative">--}}
{{--                                    <a href="#">Đăng ký CTV để xem giá</a>--}}
{{--                                </div>--}}
                            </div>
                            <div class="review">
                                <img src="<?php echo asset("$path/icon-review.png")?>">
                                <p>Đã bán {{$value->is_sell}}</p>
                            </div>
                            <div class="review">
                                <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                                <p>{{$value->place}}</p>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
