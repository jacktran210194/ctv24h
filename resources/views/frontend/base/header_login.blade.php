<?php $path = "assets/frontend/Icon/home/Danh_muc";
$pathIcon = "assets/frontend/Icon/home/icon_home";  ?>
<div class="container-fluid" style="background-color: #F7F7F7 !important">
    <div class="container">
        <div class="pt-3 pb-3 align-items-center d-flex">
            <div class="col-lg-6 d-flex align-items-center">
                <div class="dropdown">
                    <div class="select-option dropdown-toggle preventdefault" id="dropdownMenuButton"
                         data-toggle="dropdown"
                         aria-haspopup="true" aria-expanded="false">
                        <div class="img-nation">
                            <img src="<?php echo asset("$path/vietnam.png")?>">
                        </div>
                        <span class="" type="button">
                            Vietnamese
                        </span>
                    </div>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item select-option" href="#"
                           data-src="<?php echo asset("$path/vietnam.png")?>"
                        >
                            <div class="img-nation">
                                <img src="<?php echo asset("$path/vietnam.png")?>">
                            </div>
                            <span>
                                    Vietnamese
                                </span>
                        </a>
                        <a class="dropdown-item select-option" href="#"
                           data-src="<?php echo asset("$path/english.png")?>"
                        >
                            <div class="img-nation">
                                <img src="<?php echo asset("$path/english.png")?>">
                            </div>
                            <span>
                                    English
                                </span>
                        </a>
                        <a class="dropdown-item select-option" href="#"
                           data-src="<?php echo asset("$path/korean.png")?>"
                        >
                            <div class="img-nation">
                                <img src="<?php echo asset("$path/korean.png")?>">
                            </div>
                            <span>
                                    Korea
                                </span>
                        </a>
                        <a class="dropdown-item select-option" href="#"
                           data-src="<?php echo asset("$path/Japan.png")?>"
                        >
                            <div class="img-nation">
                                <img src="<?php echo asset("$path/Japan.png")?>">
                            </div>
                            <span>
                                    Japan
                                </span>
                        </a>
                    </div>
                </div>
                @php
                    $data_shop = \App\Models\ShopModel::where('customer_id',$data_login->id)->first();
                @endphp
                <a href="@isset($data_shop){{url('seeshop/'.$data_shop->id)}}@endisset" class="pl-5">Cửa hàng của
                    tôi</a>
            </div>
            <div class="col-lg-6" style="padding-right: 0">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                </div>
                <div class="float-right d-flex align-items-center">
                    @if($data_login['point'] < 2500)
                        <a href="{{url('recharge-card')}}">
                        <span>Còn <span class="mr-1" style="color: #FFC303; font-weight: 600">
                            @php
                                $point = $data_login["point"];
                                if ($point < 500) {
                                    $update_point = 500 - $point;
                                    echo $update_point;
                                } elseif ($point >= 500 && $point < 1500) {
                                    $update_point = 1500 - $point;
                                    echo $update_point;
                                } elseif ($point >= 1500 && $point < 2500) {
                                    $update_point = 2500 - $point;
                                    echo $update_point;
                                } else {
                                    echo "Bạn đang ở hạng kim cương";
                                }
                            @endphp
                        </span>
                        </span>
                        </a>
                        <a href="{{url('recharge-card')}}">
                            <object class="m-0" data="Icon/base/coin.svg"></object>
                        </a>
                        <div class="percent-rank ml-2">
                            <div class="total-percent" style="width: 80%"></div>
                        </div>
                        <a href="{{url('recharge-card')}}">
                            <span class="ml-2">Lên hạng
                        @php
                            $point = $data_login["point"];
                            if ($point < 500) {
                                $update_point = 500 - $point;
                                echo 'bạc';
                            } elseif ($point >= 500 && $point < 1500) {
                                $update_point = 1500 - $point;
                                echo 'vàng';
                            } elseif ($point >= 1500 && $point < 2500) {
                                $update_point = 2500 - $point;
                                echo 'kim cương';
                            }
                        @endphp
                        </span>
                        </a>
                    @endif
                    <div class="ml-3">
                        <div class="dropdown float-right">
                            <div type="button" class="d-flex align-items-center btn-popup-menu">
                                <img
                                    src="<?= isset($data_login) && File::exists(substr($data_login->image, 1)) ? $data_login->image : 'Icon/base/avatar.png' ?>"
                                    alt="" width="40px" class="img-fluid-avatar">
                                <div class="justify-items-center ml-2">
                                    <span
                                        class="text-bold-800">{{isset($data_login) ? $data_login->name : ''}}</span>
                                    <div class="d-flex align-items-center justify-content-start text-rank">
                                        @if($data_login['point'] < 500)
                                            <img class="mr-1" style="width: 14px!important; height: 10px!important; object-fit: contain;"
                                                 src="Icon/base/rank_bronze.png"
                                                 alt="">
                                        @elseif($data_login['point'] >= 500 && $data_login['point'] < 1500)
                                            <img class="mr-1" style="width: 14px!important; height: 10px!important;object-fit: contain;"
                                                 src="Icon/base/rank_sliver.png"
                                                 alt="">
                                        @elseif($data_login['point'] >= 1500 && $data_login['point'] < 2500)
                                            <img class="mr-1" style="width: 14px!important; height: 10px!important;object-fit: contain;"
                                                 src="Icon/base/rank_gold.png"
                                                 alt="">
                                        @else
                                            <i style="color: #54c8ff" class="fa fa-gem fa-2x fa-lg mr-1"></i>
                                        @endif
                                        <span class="m-0" style="font-size: 10px"> Hạng: @if($data_login['point'] < 500)
                                                Đồng @elseif($data_login['point'] >= 500 && $data_login['point'] < 1500)
                                                Bạc @elseif($data_login['point'] >= 1500 && $data_login['point'] < 2500)
                                                Vàng @else Kim cương @endif</span>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-menu menu-popup">
                                <a class="item-menu-popup" href="{{url('user/file')}}">Tài khoản của tôi</a>
                                <a class="item-menu-popup" href="{{url('my_history/by_product')}}">Đơn mua</a>
                                <a class="item-menu-popup" href="{{url('logout_customer')}}">Đăng xuất</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
