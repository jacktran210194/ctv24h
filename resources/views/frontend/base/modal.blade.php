<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <p class="message-modal">Bạn có muốn xoá ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-confirm" data-url="" data-dismiss="modal">Đồng ý</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
