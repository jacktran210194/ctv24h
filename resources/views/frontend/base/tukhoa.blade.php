<div class="section-tukhoa">
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    ?>
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <img src="<?php echo asset("$path/icon-logo-homepage.png")?>">
            </div>
            <h2>Từ khóa hot</h2>
        </div>
    </div>
    <div class="product-section relative">
        <button class="next-arrow btn-arrow next-arrow-key-word "> <img style="width: 12px" src="<?php echo asset("$path/next_arrow.png")?>" alt=""></button>
        <button class="prev-arrow btn-arrow prev-arrow-key-word "><img style="width: 12px" src="<?php echo asset("$path/prev_arrow.png")?>" alt=""></button>
        <div class="container-section-product slide-section-tu-khoa">
            <?php if (isset($dataHotKey)) :?>
                @foreach($dataHotKey as $value)
                    <div class="content">
                        <div class="img-product">
                            <img class="lazy" style="height: 185px;" src="{{$value->image}}" alt="">
                        </div>
                        <div class="title-product">
                            <a href="{{url("/")}}">#{{$value->key}}</a>
                        </div>
                    </div>
                @endforeach
            <?php endif; ?>
        </div>
    </div>
</div>
