<div class="section-fiash-sale">
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    use Illuminate\Support\Facades\URL;?>
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <div class="image">
                    <img src="<?php echo asset("$path/fash_sale-removebg-preview.png")?>">
                </div>
                <div id="DigitalCLOCK" class="clock" onload="showTime()">
                    <div id="showtime-h" class="showtime"></div>
                    <div id="showtime-m" class="showtime"></div>
                    <div id="showtime-s" class="showtime"></div>
                </div>
            </div>
            <div class="form-search-section">
                <form method="post" action="">
                    <label class="icon-search">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M6.66669 12.667C3.35298 12.667 0.666687 9.9807 0.666687 6.66699C0.666687 3.35328 3.35298 0.666992 6.66669 0.666992C9.9804 0.666992 12.6667 3.35328 12.6667 6.66699C12.6667 9.9807 9.9804 12.667 6.66669 12.667ZM6.66667 11.3337C9.244 11.3337 11.3333 9.24432 11.3333 6.66699C11.3333 4.08966 9.244 2.00033 6.66667 2.00033C4.08934 2.00033 2 4.08966 2 6.66699C2 9.24432 4.08934 11.3337 6.66667 11.3337ZM15.1381 15.1384C15.3985 14.878 15.3985 14.4559 15.1381 14.1956L12.4714 11.5289C12.2111 11.2686 11.789 11.2686 11.5286 11.5289C11.2683 11.7893 11.2683 12.2114 11.5286 12.4717L14.1953 15.1384C14.4556 15.3987 14.8778 15.3987 15.1381 15.1384Z" fill="#000000"/>
                        </svg>
                    </label>
                    <input type="text" placeholder="Tìm Kiếm ">
                </form>
            </div>
        </div>
        <div class="link preventdefault"><a href="<?= URL::to('flash_sale') ?>">Xem Thêm</a> </div>
    </div>
    <div class="product-section relative">
        <button class="next-arrow btn-arrow next-arrow-fash"><img style="width: 12px" src="<?php echo asset("$path/next_arrow.png")?>"></button>
        <button class="prev-arrow btn-arrow prev-arrow-fash"><img style="width: 12px" src="<?php echo asset("$path/prev_arrow.png")?>"></button>
        <div class="container-section-product slide-section-fash">
            <div class="content preventdefault">
                <a href="<?= URL::to('products') ?>">
                <div class="img-product">
                    <img class="lazy" src="<?php echo asset("$path/fash_giay.png")?>" alt="">
                </div>
                <div class="container-sale">
                    <div class="image-background-sale">
                        <img src="<?php echo asset("$path/Vector_sale.png")?>">
                    </div>
                    <div class="image-sale">
                        <span class="font-weight-bold color-white d-block" style="font-size: 20px">70%</span>
                        <span class="font-weight-bold color-white d-block text-uppercase text-center" style="font-size: 12px">off</span>
                    </div>
                </div>
                <div class="information-product">
                    <div class="price-product">
                        <span class="old-price">200.000Đ</span>
                        <span class="last-price">120.000Đ</span>
                    </div>
                    <div class="information-sale">
                        <span class="text">Đã bán 100</span>
                        <span class="icon-fire">
                            <object data="Icon/base/fire.svg"></object>
                        </span>
                        <span class="icon-1">
                            <svg width="165" height="12" viewBox="0 0 165 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="163.663" height="10.6751" rx="5.33755" fill="#F99E22"/>
                            </svg>
                        </span>
                        <span class="icon-2">
                            <svg width="53" height="12" viewBox="0 0 53 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="52.2947" height="10.6751" rx="5.33755" fill="#FF0000"/>
                            </svg>
                        </span>
                    </div>
                </div>
                </a>
            </div>
            <div class="content preventdefault">
                <div class="img-product">
                    <img class="lazy" src="<?php echo asset("$path/fash_vay.png")?>" alt="">
                </div>
                <div class="container-sale">
                    <div class="image-background-sale">
                        <img src="<?php echo asset("$path/Vector_sale.png")?>">
                    </div>
                    <div class="image-sale">
                        <span class="font-weight-bold color-white d-block" style="font-size: 20px">50%</span>
                        <span class="font-weight-bold color-white d-block text-uppercase text-center" style="font-size: 12px">off</span>
                    </div>
                </div>
                <div class="information-product">
                    <div class="price-product">
                        <span class="old-price">200.000Đ</span>
                        <span class="last-price">100.000Đ</span>
                    </div>
                    <div class="information-sale">
                        <span class="text">Đã bán 100</span>
                        <span class="icon-fire">
                            <object data="Icon/base/fire.svg"></object>
                        </span>
                        <span class="icon-1">
                            <svg width="165" height="12" viewBox="0 0 165 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="163.663" height="10.6751" rx="5.33755" fill="#F99E22"/>
                            </svg>
                        </span>
                        <span class="icon-2">
                            <svg width="53" height="12" viewBox="0 0 53 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="52.2947" height="10.6751" rx="5.33755" fill="#FF0000"/>
                            </svg>
                        </span>
                    </div>
                </div>
            </div>
            <div class="content preventdefault" >
                <div class="img-product">
                    <img class="lazy" src="<?php echo asset("assets/frontend/Icon/flashsale/flash_sale_1.png")?>" alt="">
                </div>
                <div class="container-sale">
                    <div class="image-background-sale">
                        <img src="<?php echo asset("$path/Vector_sale.png")?>">
                    </div>
                    <div class="image-sale">
                        <span class="font-weight-bold color-white d-block" style="font-size: 20px">50%</span>
                        <span class="font-weight-bold color-white d-block text-uppercase text-center" style="font-size: 12px">off</span>
                    </div>
                </div>
                <div class="information-product">
                    <div class="price-product">
                        <span class="old-price">120.000Đ</span>
                        <span class="last-price">60.000Đ</span>
                    </div>
                    <div class="information-sale">
                        <span class="text">Đã bán 120</span>
                        <span class="icon-fire">
                            <object data="Icon/base/fire.svg"></object>
                        </span>
                        <span class="icon-1">
                            <svg width="165" height="12" viewBox="0 0 165 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="163.663" height="10.6751" rx="5.33755" fill="#F99E22"/>
                            </svg>
                        </span>
                        <span class="icon-2">
                            <svg width="53" height="12" viewBox="0 0 53 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="52.2947" height="10.6751" rx="5.33755" fill="#FF0000"/>
                            </svg>
                        </span>
                    </div>
                </div>
            </div>
            <div class="content preventdefault">
                <div class="img-product">
                    <img class="lazy" src="<?php echo asset("assets/frontend/Icon/flashsale/flash_sale_2.jpg")?>" alt="">
                </div>
                <div class="container-sale">
                    <div class="image-background-sale">
                        <img src="<?php echo asset("$path/Vector_sale.png")?>">
                    </div>
                    <div class="image-sale">
                        <span class="font-weight-bold color-white d-block" style="font-size: 20px">40%</span>
                        <span class="font-weight-bold color-white d-block text-uppercase text-center" style="font-size: 12px">off</span>
                    </div>
                </div>
                <div class="information-product">
                    <div class="price-product">
                        <span class="old-price">200.000Đ</span>
                        <span class="last-price">120.000Đ</span>
                    </div>
                    <div class="information-sale">
                        <span class="text">Đã bán 10</span>
                        <span class="icon-fire">
                            <object data="Icon/base/fire.svg"></object>
                        </span>
                        <span class="icon-1">
                            <svg width="165" height="12" viewBox="0 0 165 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="163.663" height="10.6751" rx="5.33755" fill="#F99E22"/>
                            </svg>
                        </span>
                        <span class="icon-2">
                            <svg width="53" height="12" viewBox="0 0 53 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="52.2947" height="10.6751" rx="5.33755" fill="#FF0000"/>
                            </svg>
                        </span>
                    </div>
                </div>
            </div>
            <div class="content preventdefault">
                <div class="img-product">
                    <img class="lazy" src="<?php echo asset("assets/frontend/Icon/flashsale/flash_sale_3.jpg")?>" alt="">
                </div>
                <div class="container-sale">
                    <div class="image-background-sale">
                        <img src="<?php echo asset("$path/Vector_sale.png")?>">
                    </div>
                    <div class="image-sale">
                        <span class="font-weight-bold color-white d-block" style="font-size: 20px">40%</span>
                        <span class="font-weight-bold color-white d-block text-uppercase text-center" style="font-size: 12px">off</span>
                    </div>
                </div>
                <div class="information-product">
                    <div class="price-product">
                        <span class="old-price">200.000Đ</span>
                        <span class="last-price">120.000Đ</span>
                    </div>
                    <div class="information-sale">
                        <span class="text">Đã bán 200</span>
                        <span class="icon-fire">
                            <object data="Icon/base/fire.svg"></object>
                        </span>
                        <span class="icon-1">
                            <svg width="165" height="12" viewBox="0 0 165 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="163.663" height="10.6751" rx="5.33755" fill="#F99E22"/>
                            </svg>
                        </span>
                        <span class="icon-2">
                            <svg width="53" height="12" viewBox="0 0 53 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="52.2947" height="10.6751" rx="5.33755" fill="#FF0000"/>
                            </svg>
                        </span>
                    </div>
                </div>
            </div>
            <div class="content preventdefault">
                <div class="img-product">
                    <img class="lazy" src="<?php echo asset("assets/frontend/Icon/flashsale/flash_sale_4.jpg")?>" alt="">
                </div>
                <div class="container-sale">
                    <div class="image-background-sale">
                        <img src="<?php echo asset("$path/Vector_sale.png")?>">
                    </div>
                    <div class="image-sale">
                        <span class="font-weight-bold color-white d-block" style="font-size: 20px">50%</span>
                        <span class="font-weight-bold color-white d-block text-uppercase text-center" style="font-size: 12px">off</span>
                    </div>
                </div>
                <div class="information-product">
                    <div class="price-product">
                        <span class="old-price">100.000Đ</span>
                        <span class="last-price">50.000Đ</span>
                    </div>
                    <div class="information-sale">
                        <span class="text">Đã bán 100</span>
                        <span class="icon-fire">
                            <object data="Icon/base/fire.svg"></object>
                        </span>
                        <span class="icon-1">
                            <svg width="165" height="12" viewBox="0 0 165 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="163.663" height="10.6751" rx="5.33755" fill="#F99E22"/>
                            </svg>
                        </span>
                        <span class="icon-2">
                            <svg width="53" height="12" viewBox="0 0 53 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="52.2947" height="10.6751" rx="5.33755" fill="#FF0000"/>
                            </svg>
                        </span>
                    </div>
                </div>
            </div>
            <div class="content preventdefault">
                <div class="img-product">
                    <img class="lazy" src="<?php echo asset("assets/frontend/Icon/flashsale/flash_sale_5.jpg")?>" alt="">
                </div>
                <div class="container-sale">
                    <div class="image-background-sale">
                        <img src="<?php echo asset("$path/Vector_sale.png")?>">
                    </div>
                    <div class="image-sale">
                        <span class="font-weight-bold color-white d-block" style="font-size: 20px">0%</span>
                        <span class="font-weight-bold color-white d-block text-uppercase text-center" style="font-size: 12px">off</span>
                    </div>
                </div>
                <div class="information-product">
                    <div class="price-product">
                        <span class="old-price">200.000Đ</span>
                        <span class="last-price">220.000Đ</span>
                    </div>
                    <div class="information-sale">
                        <span class="text">Đã bán 80</span>
                        <span class="icon-fire">
                            <object data="Icon/base/fire.svg"></object>
                        </span>
                        <span class="icon-1">
                            <svg width="165" height="12" viewBox="0 0 165 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="163.663" height="10.6751" rx="5.33755" fill="#F99E22"/>
                            </svg>
                        </span>
                        <span class="icon-2">
                            <svg width="53" height="12" viewBox="0 0 53 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.684204" y="0.527344" width="52.2947" height="10.6751" rx="5.33755" fill="#FF0000"/>
                            </svg>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="banner-section">
        <img class="lazy" src="<?php echo asset("$path/fash_banner.png")?>" style="max-height: 150px" alt="">
    </div>
</div>
