<?php
use Illuminate\Support\Facades\URL;
?>
<div id="footer" class="pl-4 mt-5">
    <div class="container">
        <h3 class="pt-4" style="font-weight: 900">DANH MỤC</h3>
        <div class="row m-0 mt-5">
            @if(count($display_category))
                @foreach($display_category as $value)
                    <div class="col-lg-24 pr-3">
                        <div class="mt-3">
                            <a href="{{url('ElectronicDevice/'.$value->id)}}"><h5>{{$value->name}}</h5></a>
                            <span>
                                @if(isset($display_category_child))
                                    @foreach($display_category_child as $v)
                                        @if($v['category_id']==$value['id'])
                                            <a>{{$v['name']}}</a> |
                                        @endif
                                    @endforeach
                                @endif
                            </span>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="row m-0 mt-5">
            <div class="col-lg-24 pr-3">
                <div class="mt-3">
                    <h5>CHĂM SÓC KHÁCH HÀNG</h5>
                    <p>Trung tâm trợ giúp</p>
                    <p>Hướng dẫn mua hàng</p>
                    <p>Hướng dẫn NCC</p>
                    <p>Hướng dẫn CTV</p>
                    <p>Thanh toán</p>
                    <p>Tích điểm</p>
                    <p>Vận chuyển</p>
                    <p>Chăm sóc khách hàng</p>
                    <p>Chính sách bảo hàng</p>
                </div>
            </div>
            <div class="col-lg-24 pr-3">
                <div class="mt-3">
                    <h5>VỀ CTV24H</h5>
                    <p>Giới thiệu</p>
                    <p>Tuyển dụng</p>
                    <p>Điều khoản</p>
                    <p>Chính sách bảo mật</p>
                    <p>Kênh người bán</p>
                    <p>Kênh cộng tác viên</p>
                    <p>Flash sale</p>
                </div>
            </div>
            <div class="col-lg-24 pr-3">
                <div class="mt-3">
                    <h5>THANH TOÁN</h5>
                    <img width="206px" src="Icon/base/checkout.png" alt="">
                </div>
                <div class="mt-3">
                    <h6>ĐƠN VỊ VẬN CHUYỂN</h6>
                    <img width="206px" src="Icon/base/shipping.png" alt="">
                </div>
            </div>
            <div class="col-lg-24 pr-3">
                <div class="mt-3">
                    <h5>THEO DÕI CHÚNG TÔI</h5>
                    <a class="preventdefault" href="">
                        <div class="d-flex align-items-center pb-2">
                            <object data="Icon/base/facebook.svg" type=""></object>
                            <span class="follow">Facebook</span>
                        </div>
                    </a>
                    <a class="preventdefault" href="">
                        <div class="d-flex align-items-center pb-2">
                            <object data="Icon/base/instagram.svg" type=""></object>
                            <span class="follow">Instagram</span>
                        </div>
                    </a>
                    <a class="preventdefault" href="">
                        <div class="d-flex align-items-center pb-2">
                            <object data="Icon/base/linkedln.svg" type=""></object>
                            <span class="follow">Linkedin</span>
                        </div>
                    </a>
                    <a class="preventdefault" href="">
                        <div class="d-flex align-items-center pb-2">
                            <object data="Icon/base/youtube.svg" type=""></object>
                            <span class="follow">Youtube</span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-24 pr-3">
                <div class="mt-3">
                    <h5>TẢI ỨNG DỤNG NGAY THÔI</h5>
                    <div class="d-flex align-items-center">
                        <img class="logo-img preventdefault" width="87px" src="Icon/base/qr-code.png" alt="">
                        <a href="" class="pl-2 preventdefault">
                            <img class="logo-img" src="Icon/base/app_mobile.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-bottom ">
        </div>
{{--        <div class="btn btn-main btn-chat d-flex align-items-center text-white" data-url="<?= URL::to('chat/get_form_chat') ?>">--}}
{{--            <object data="Icon/base/icon_chat.svg" width="30px" type=""></object>--}}
{{--            <span class="pl-2">CHAT</span>--}}
{{--        </div>--}}
        <p class="mt-5">Copyright © CTV24h 2021</p>
    </div>
</div>
<div class="popup-chat">
</div>
<div class="loader-wrapper">
    <div class="banner-loading" style="width: 100%">
        <img src="Icon/base/shopping-loader.gif">
    </div>
</div>
