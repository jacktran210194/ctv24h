<div class="container-fluid" style="background-color: rgba(183, 88, 0, 0.25) !important;">
    <div class="container">
        <div class="pt-3 pb-3 align-items-center d-flex">
            <div class="col-lg-6 d-flex align-items-center p-0">
                <a href="{{url('WelcomeCTV24H')}}" class="color-black">Chào mừng bạn đến với CTV24h</a>
                <a href="{{url('suggestion')}}" class="pl-5 color-black">Gợi ý dành cho CTV</a>
            </div>
            <div class="col-lg-6 d-flex justify-content-end align-items-center">
                <div class="dropdown float-right">
                    <div type="button" class="d-flex align-items-center">
                        <a href="{{url('register')}}">
                            <span style="cursor: pointer"> Đăng ký </span>
                        </a>
                    </div>
                </div>
                <a href="{{url('login')}}" class="pl-5">Đăng nhập</a>
                <div class="dropdown pl-5">
                    <div class="select-option dropdown-toggle preventdefault" id="dropdownMenuButton"
                         {{--                         data-toggle="dropdown"--}}
                         aria-haspopup="true" aria-expanded="false">
                        <div class="img-nation">
                            <img src="<?php echo asset("$path/vietnam.png")?>">
                        </div>
                        <span class="" type="button">
                            Vietnamese
                        </span>
                    </div>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item select-option" href="#"
                           data-src="<?php echo asset("$path/vietnam.png")?>"
                        >
                            <div class="img-nation">
                                <img src="<?php echo asset("$path/vietnam.png")?>">
                            </div>
                            <span>
                                    Vietnamese
                                </span>
                        </a>
                        <a class="dropdown-item select-option" href="#"
                           data-src="<?php echo asset("$path/english.png")?>"
                        >
                            <div class="img-nation">
                                <img src="<?php echo asset("$path/english.png")?>">
                            </div>
                            <span>
                                    English
                                </span>
                        </a>
                        <a class="dropdown-item select-option" href="#"
                           data-src="<?php echo asset("$path/korean.png")?>"
                        >
                            <div class="img-nation">
                                <img src="<?php echo asset("$path/korean.png")?>">
                            </div>
                            <span>
                                    Korea
                                </span>
                        </a>
                        <a class="dropdown-item select-option" href="#"
                           data-src="<?php echo asset("$path/Japan.png")?>"
                        >
                            <div class="img-nation">
                                <img src="<?php echo asset("$path/Japan.png")?>">
                            </div>
                            <span>
                                    Japan
                                </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
