<div class="section section-tim-kiem">
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    $path_2 = "assets/frontend/Icon/flashsale";
    ?>
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <img src="<?php echo asset("$path/icon-logo-homepage.png")?>">
            </div>
            <h2>tìm kiếm hàng đầu </h2>
        </div>
{{--        <div class="link"><a href="#" class="preventdefault">Xem Thêm</a> </div>--}}
    </div>
    <div class="gallery-product relative">
        <button class="next-arrow btn-arrow btn-arrow-small prev-arrow-search"><img style="width: 7px" src="<?php echo asset("$path/next_arrow.png")?>"></button>
        <button class="prev-arrow btn-arrow btn-arrow-small next-arrow-search"><img style="width: 7px" src="<?php echo asset("$path/prev_arrow.png")?>"></button>
        <div class="container-gallery-product-search">
            <div class="content-gallery-product preventdefault">
                <div class="image-product">
                    <div class="image_1">
                        <img class="lazy" src="<?php echo asset("$path/quan-jean-nu-1.png")?>" alt="">
                        <div class="number-1">1</div>
                    </div>
                    <div class="image_2">
                        <div class="image_content_2">
                            <img class="lazy" src="<?php echo asset("$path/quan-jean-nu-2.png")?>" alt="">
                            <div class="number_2">2</div>
                        </div>
                        <div class="image_content_3">
                            <img class="lazy" src="<?php echo asset("$path/quan-jean-nu-3.png")?>" alt="">
                            <div class="number_2 number_3">3</div>
                        </div>
                    </div>
                </div>
                <div class="title-gallery-section">
                    <span>Quần Jean Nữ</span>
                </div>
            </div>
            <div class="content-gallery-product preventdefault">
                <div class="image-product">
                    <div class="image_1">
                        <img class="lazy"
                             src="<?php echo asset("$path_2/product_1.jpg")?>"
                             alt=""
                        >
                        <div class="number-1">1</div>
                    </div>
                    <div class="image_2">
                        <div class="image_content_2">
                            <img class="lazy"
                                 src="<?php echo asset("$path_2/product_5.jpg")?>"
                                 alt=""
                            >
                            <div class="number_2">2</div>
                        </div>
                        <div class="image_content_3">
                            <img class="lazy"
                                 src="<?php echo asset("$path_2/product_4.jpg")?>"
                                 alt=""
                            >
                            <div class="number_2 number_3">3</div>
                        </div>
                    </div>
                </div>
                <div class="title-gallery-section">
                    <span>Áo Thun Nam Có Cổ</span>
                </div>
            </div>
            <div class="content-gallery-product preventdefault">
                <div class="image-product">
                    <div class="image_1">
                        <img class="lazy"
                             src="<?php echo asset("$path_2/phone_case.jpg")?>"
                             alt=""
                        >
                        <div class="number-1">1</div>
                    </div>
                    <div class="image_2">
                        <div class="image_content_2">
                            <img class="lazy"
                                 src="<?php echo asset("$path_2/phone_case_1.jpg")?>"
                                 alt=""
                            >
                            <div class="number_2">2</div>
                        </div>
                        <div class="image_content_3">
                            <img
                                class="lazy"
                                src="<?php echo asset("$path_2/phone_case_2.jpg")?>"
                                alt=""
                            >
                            <div class="number_2 number_3">3</div>
                        </div>
                    </div>
                </div>
                <div class="title-gallery-section">
                    <span>Ốp Lưng Iphone</span>
                </div>
            </div>
            <div class="content-gallery-product preventdefault">
                <div class="image-product">
                    <div class="image_1">
                        <img class="lazy"
                             src="<?php echo asset("$path_2/product_1.jpg")?>"
                             alt=""
                        >
                        <div class="number-1">1</div>
                    </div>
                    <div class="image_2">
                        <div class="image_content_2">
                            <img class="lazy"
                                 src="<?php echo asset("$path_2/product_5.jpg")?>"
                                 alt=""
                            >
                            <div class="number_2">2</div>
                        </div>
                        <div class="image_content_3">
                            <img class="lazy"
                                 src="<?php echo asset("$path_2/product_4.jpg")?>"
                                 alt=""
                            >
                            <div class="number_2 number_3">3</div>
                        </div>
                    </div>
                </div>
                <div class="title-gallery-section">
                    <span>Áo Thun Nam Có Cổ</span>
                </div>
            </div>
        </div>
    </div>
</div>
