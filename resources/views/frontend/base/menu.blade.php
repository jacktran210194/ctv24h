<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto active"><a class="navbar-brand" href="{{url('admin')}}">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0">Chapp Server</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather Icon-x d-block d-xl-none font-medium-4 primary toggle-Icon"></i><i class="toggle-Icon feather Icon-disc font-medium-4 d-none d-xl-block collapse-toggle-Icon primary" data-tIcon="Icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="<?= isset($page) && $page == 'home' ? 'active' : '' ?> nav-item"><a href="{{url('admin')}}"><i class="feather Icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span><span class="badge badge badge-warning badge-pill float-right mr-2">2</span></a>
            </li>
            <li class=" navigation-header"><span>Quản lý</span>
            </li>
            <li class="<?= isset($page) && $page == 'home' ? 'active' : '' ?> nav-item"><a href="{{url('admin')}}"><i class="feather Icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
            </li>
        </ul>
    </div>
</div>
