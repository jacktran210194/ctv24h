<div class="section section-tim-kiem">
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    ?>
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <img src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/icon-logo-homepage.png")?>">
            </div>
            <h2>HÀNG ĐỒNG GIÁ</h2>
        </div>
{{--        <div class="link"><a href="#" class="preventdefault">Xem Thêm</a> </div>--}}
    </div>
    <div class="gallery-product relative">
        <button class="next-arrow btn-arrow btn-arrow-small prev-arrow-gallery"><img style="width: 12px" src="<?php echo asset("$path/next_arrow.png")?>"></button>
        <button class="prev-arrow btn-arrow btn-arrow-small next-arrow-gallery"><img style="width: 12px" src="<?php echo asset("$path/prev_arrow.png")?>"></button>
        <div class="container-gallery-product">
            <div class="content-gallery-product preventdefault">
                <div class="image-product">
                    <div class="image_1">
                        <img
                            class="lazy"
                            src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/vay_1.png")?>"
                            alt=""
                        >
                    </div>
                    <div class="image_2">
                        <div class="image_content_2">
                            <img class="lazy"
                                 src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/vay_2.png")?>"
                                 alt=""
                            >
                        </div>
                        <div class="image_content_3">
                            <img class="lazy"
                                 src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/vay_3.png")?>"
                                 alt=""
                            >
                        </div>
                    </div>
                </div>
                <div class="title-gallery-section">
                    <span>Đồng Giá 99k</span>
                </div>
            </div>
            <div class="content-gallery-product preventdefault">
                <div class="image-product">
                    <div class="image_1">
                        <img
                            class="lazy"
                            src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/ao_1.png")?>"
                            alt=""
                        >
                    </div>
                    <div class="image_2">
                        <div class="image_content_2">
                            <img class="lazy"
                                 src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/ao_2.png")?>"
                                 alt=""
                            >
                        </div>
                        <div class="image_content_3">
                            <img class="lazy"
                                 src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/ao_3.png")?>"
                                 alt=""
                            >
                        </div>
                    </div>
                </div>
                <div class="title-gallery-section">
                    <span>Đồng Giá 199k</span>
                </div>
            </div>
            <div class="content-gallery-product preventdefault">
                <div class="image-product">
                    <div class="image_1">
                        <img class="lazy" src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/giay_1.png")?>">
                    </div>
                    <div class="image_2">
                        <div class="image_content_2">
                            <img class="lazy" src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/giay_2.png")?>"
                            >
                        </div>
                        <div class="image_content_3">
                            <img class="lazy" src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/giay_3.png")?>">
                        </div>
                    </div>
                </div>
                <div class="title-gallery-section">
                    <span>Đồng Giá 299k</span>
                </div>
            </div>
            <div class="content-gallery-product preventdefault">
                <div class="image-product">
                    <div class="image_1">
                        <img class="lazy" src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/ao_1.png")?>">
                    </div>
                    <div class="image_2">
                        <div class="image_content_2">
                            <img class="lazy" src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/ao_2.png")?>">
                        </div>
                        <div class="image_content_3">
                            <img class="lazy" src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/ao_3.png")?>">
                        </div>
                    </div>
                </div>
                <div class="title-gallery-section">
                    <span>Đồng Giá 199k</span>
                </div>
            </div>
        </div>
    </div>
</div>
