<div class="section-dau-gia">
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    $path_2 = "assets/frontend/Icon/flashsale";
    use Illuminate\Support\Facades\URL;?>
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <div class="image">
                    <img class="lazy" src="<?php echo asset("$path/icon_dau_gia.png")?>">
                </div>
            </div>
            <h2>Đấu giá</h2>
            <div class="form-search-section">
                <form method="post" action="">
                    <label class="icon-search">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M6.66669 12.667C3.35298 12.667 0.666687 9.9807 0.666687 6.66699C0.666687 3.35328 3.35298 0.666992 6.66669 0.666992C9.9804 0.666992 12.6667 3.35328 12.6667 6.66699C12.6667 9.9807 9.9804 12.667 6.66669 12.667ZM6.66667 11.3337C9.244 11.3337 11.3333 9.24432 11.3333 6.66699C11.3333 4.08966 9.244 2.00033 6.66667 2.00033C4.08934 2.00033 2 4.08966 2 6.66699C2 9.24432 4.08934 11.3337 6.66667 11.3337ZM15.1381 15.1384C15.3985 14.878 15.3985 14.4559 15.1381 14.1956L12.4714 11.5289C12.2111 11.2686 11.789 11.2686 11.5286 11.5289C11.2683 11.7893 11.2683 12.2114 11.5286 12.4717L14.1953 15.1384C14.4556 15.3987 14.8778 15.3987 15.1381 15.1384Z" fill="#000000"/>
                        </svg>
                    </label>
                    <input type="text" placeholder="Tìm Kiếm ">
                </form>
            </div>
        </div>
        <div class="link"><a href="#" class="preventdefault">Xem Thêm</a> </div>
    </div>
        <div class="product-section relative">
            <button class="next-arrow btn-arrow prev-arrow-auction"><img style="width: 12px" src="<?php echo asset("$path/next_arrow.png")?>"></button>
            <button class="prev-arrow btn-arrow next-arrow-auction"><img style="width: 12px" src="<?php echo asset("$path/prev_arrow.png")?>"></button>
            <div class="container-section-product slide-section">
                    <a href="<?= URL::to('details_product') ?>" class="content preventdefault">
                        <div class="img-product">
                            <img class="lazy" src="<?php echo asset("$path/dau_gia_image.png")?>" alt="">
                        </div>
                        <div class="container-sale">
                            <div class="image-background-sale">
                                <img src="<?php echo asset("$path/dau_gia_icon_1.png")?>">
                            </div>
                            <div class="image-sale">
                                <img src="<?php echo asset("$path/dau_gia_icon_2.png")?>">
                            </div>
                        </div>
                        <div class="information-product">
                            <div class="prodcut-title">
                                <span>Áo thun nữ</span>
                            </div>
                            <div class="information">
                                <div class="text-information">
                                    <span>Giá Khởi Điểm:</span>
                                    <span>Thầu Hiện Tại:</span>
                                    <span>Mua Ngay:</span>
                                    <span>Số Người Đấu Giá:</span>
                                    <span>Bước giá:</span>
                                    <span>Thời gian còn lại:</span>
                                </div>
                                <div class="price-information">
                                    <span>100.000</span>
                                    <span>300.000</span>
                                    <span>370.000</span>
                                    <span>10</span>
                                    <span>200.000</span>
                                    <span>10:00:00</span>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="<?= URL::to('details_product') ?>" class="content preventdefault">
                        <div class="img-product">
                            <img class="lazy" src="<?php echo asset("$path_2/product_1.jpg")?>" alt="">
                        </div>
                        <div class="container-sale">
                            <div class="image-background-sale">
                                <img src="<?php echo asset("$path/dau_gia_icon_1.png")?>">
                            </div>
                            <div class="image-sale">
                                <img src="<?php echo asset("$path/dau_gia_icon_2.png")?>">
                            </div>
                        </div>
                        <div class="information-product">
                            <div class="prodcut-title">
                                <span>Bộ thể thao nam</span>
                            </div>
                            <div class="information">
                                <div class="text-information">
                                    <span>Giá Khởi Điểm:</span>
                                    <span>Thầu Hiện Tại:</span>
                                    <span>Mua Ngay:</span>
                                    <span>Số Người Đấu Giá:</span>
                                    <span>Bước giá:</span>
                                    <span>Thời gian còn lại:</span>
                                </div>
                                <div class="price-information">
                                    <span>100.000</span>
                                    <span>200.000</span>
                                    <span>270.000</span>
                                    <span>10</span>
                                    <span>200.000</span>
                                    <span>10:00:00</span>
                                </div>
                            </div>
                        </div>
                    </a>
                <a href="<?= URL::to('details_product') ?>" class="content preventdefault">
                    <div class="img-product">
                        <img class="lazy" src="<?php echo asset("$path_2/product_2.jpg")?>" alt="">
                    </div>
                    <div class="container-sale">
                        <div class="image-background-sale">
                            <img src="<?php echo asset("$path/dau_gia_icon_1.png")?>">
                        </div>
                        <div class="image-sale">
                            <img src="<?php echo asset("$path/dau_gia_icon_2.png")?>">
                        </div>
                    </div>
                    <div class="information-product">
                        <div class="prodcut-title">
                            <span>Áo Thun Tay Lỡ Nam</span>
                        </div>
                        <div class="information">
                            <div class="text-information">
                                <span>Giá Khởi Điểm:</span>
                                <span>Thầu Hiện Tại:</span>
                                <span>Mua Ngay:</span>
                                <span>Số Người Đấu Giá:</span>
                                <span>Bước giá:</span>
                                <span>Thời gian còn lại:</span>
                            </div>
                            <div class="price-information">
                                <span>80.000</span>
                                <span>100.000</span>
                                <span>170.000</span>
                                <span>50</span>
                                <span>100.000</span>
                                <span>10:00:00</span>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?= URL::to('details_product') ?>" class="content preventdefault">
                    <div class="img-product">
                        <img class="lazy" src="<?php echo asset("$path_2/product_3.jpg")?>" alt="">
                    </div>
                    <div class="container-sale">
                        <div class="image-background-sale">
                            <img src="<?php echo asset("$path/dau_gia_icon_1.png")?>">
                        </div>
                        <div class="image-sale">
                            <img src="<?php echo asset("$path/dau_gia_icon_2.png")?>">
                        </div>
                    </div>
                    <div class="information-product">
                        <div class="prodcut-title">
                            <span>Áo khoác Jean Nam GDragon</span>
                        </div>
                        <div class="information">
                            <div class="text-information">
                                <span>Giá Khởi Điểm:</span>
                                <span>Thầu Hiện Tại:</span>
                                <span>Mua Ngay:</span>
                                <span>Số Người Đấu Giá:</span>
                                <span>Bước giá:</span>
                                <span>Thời gian còn lại:</span>
                            </div>
                            <div class="price-information">
                                <span>100.000</span>
                                <span>200.000</span>
                                <span>270.000</span>
                                <span>100</span>
                                <span>200.000</span>
                                <span>10:00:00</span>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?= URL::to('details_product') ?>" class="content preventdefault">
                    <div class="img-product">
                        <img class="lazy" src="<?php echo asset("$path_2/product_4.jpg")?>" alt="">
                    </div>
                    <div class="container-sale">
                        <div class="image-background-sale">
                            <img src="<?php echo asset("$path/dau_gia_icon_1.png")?>">
                        </div>
                        <div class="image-sale">
                            <img src="<?php echo asset("$path/dau_gia_icon_2.png")?>">
                        </div>
                    </div>
                    <div class="information-product">
                        <div class="prodcut-title">
                            <span>Áo polo nam ngắn tay</span>
                        </div>
                        <div class="information">
                            <div class="text-information">
                                <span>Giá Khởi Điểm:</span>
                                <span>Thầu Hiện Tại:</span>
                                <span>Mua Ngay:</span>
                                <span>Số Người Đấu Giá:</span>
                                <span>Bước giá:</span>
                                <span>Thời gian còn lại:</span>
                            </div>
                            <div class="price-information">
                                <span>200.000</span>
                                <span>200.000</span>
                                <span>270.000</span>
                                <span>200</span>
                                <span>200.000</span>
                                <span>10:00:00</span>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?= URL::to('details_product') ?>" class="content preventdefault">
                    <div class="img-product">
                        <img class="lazy" src="<?php echo asset("$path_2/product_5.jpg")?>" alt="">
                    </div>
                    <div class="container-sale">
                        <div class="image-background-sale">
                            <img src="<?php echo asset("$path/dau_gia_icon_1.png")?>">
                        </div>
                        <div class="image-sale">
                            <img src="<?php echo asset("$path/dau_gia_icon_2.png")?>">
                        </div>
                    </div>
                    <div class="information-product">
                        <div class="prodcut-title">
                            <span>Áo phông nam cổ bẻ ngắn tay</span>
                        </div>
                        <div class="information">
                            <div class="text-information">
                                <span>Giá Khởi Điểm:</span>
                                <span>Thầu Hiện Tại:</span>
                                <span>Mua Ngay:</span>
                                <span>Số Người Đấu Giá:</span>
                                <span>Bước giá:</span>
                                <span>Thời gian còn lại:</span>
                            </div>
                            <div class="price-information">
                                <span>250.000</span>
                                <span>200.000</span>
                                <span>370.000</span>
                                <span>100</span>
                                <span>200.000</span>
                                <span>10:00:00</span>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?= URL::to('details_product') ?>" class="content preventdefault">
                    <div class="img-product">
                        <img class="lazy" src="<?php echo asset("$path_2/product_1.jpg")?>" alt="">
                    </div>
                    <div class="container-sale">
                        <div class="image-background-sale">
                            <img src="<?php echo asset("$path/dau_gia_icon_1.png")?>">
                        </div>
                        <div class="image-sale">
                            <img src="<?php echo asset("$path/dau_gia_icon_2.png")?>">
                        </div>
                    </div>
                    <div class="information-product">
                        <div class="prodcut-title">
                            <span>Bộ thể thao nam</span>
                        </div>
                        <div class="information">
                            <div class="text-information">
                                <span>Giá Khởi Điểm:</span>
                                <span>Thầu Hiện Tại:</span>
                                <span>Mua Ngay:</span>
                                <span>Số Người Đấu Giá:</span>
                                <span>Bước giá:</span>
                                <span>Thời gian còn lại:</span>
                            </div>
                            <div class="price-information">
                                <span>100.000</span>
                                <span>200.000</span>
                                <span>270.000</span>
                                <span>10</span>
                                <span>200.000</span>
                                <span>10:00:00</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
</div>
