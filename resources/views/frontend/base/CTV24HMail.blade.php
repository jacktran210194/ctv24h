<div class="section section-ctv">
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    $path_2 = "assets/frontend/Icon/flashsale";
    ?>
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <img src="<?php echo asset("assets/frontend/Icon/home/Danh_muc/icon-logo-homepage.png")?>">
            </div>
            <h2>CTV24h Mall</h2>
            <div class="content-title-icon">
                <object data="Icon/home/icon_home/icon_squal.svg" width="16px" height="16px"></object>
                <span class="title-icon">7 Ngày Miễn Phí Trả Hàng</span>
            </div>
            <div class="content-title-icon">
                <object data="Icon/home/icon_home/icon_caption.svg" width="16px" height="16px"></object>
                <span class="title-icon">Hàng Chính Hãng 100%</span>
            </div>
            <div class="content-title-icon">
                <object data="Icon/home/icon_home/icon_shipping.svg" width="16px" height="16px"></object>
                <span class="title-icon">Miễn Phí Vận Chuyển</span>
            </div>
        </div>
        <div class="link"><a href="{{url('mall')}}">Xem Thêm</a> </div>
    </div>
    <div class="product-section flex-items preventdefault">
        <div class="banner-section">
            <div class="slide-banner flex-items">
                <div class="image-banner slide">
                    <img src="<?php echo asset("$path/ctv_banner.png")?>"
                         class="w-100 lazy" alt="banner">
                </div>
                <div class="image-banner slide">
                    <img src="<?php echo asset("$path/ctv_banner.png")?>"
                         class="w-100 lazy" alt="banner">
                </div>
                <div class="image-banner slide">
                    <img src="<?php echo asset("$path/ctv_banner.png")?>"
                         class="w-100 lazy" alt="banner">
                </div>
            </div>
        </div>
        <div class="container-section-product w-70 relative">
            <button class="next-arrow btn-arrow btn-arrow-small prev-arrow-ctv"><img style="width: 7px" src="<?php echo asset("$path/next_arrow.png")?>"></button>
            <button class="prev-arrow btn-arrow btn-arrow-small next-arrow-ctv"><img style="width: 7px" src="<?php echo asset("$path/prev_arrow.png")?>"></button>
            <div class="slide-section-ctv w-100">
                <div class="content border-box col-4new">
                    <div class="img-product remove-mg">
                        <img src="<?php echo asset("$path/jean-trend.png")?>" class="lazy">
                    </div>
                    <div class="title-product-n relative">
                        <span>Quần jean ống rộng Kèm Hình Thật...</span>
                    </div>
                    <div class="product-price">
                        <p class="price">270.000 - 370.000</p>
                        <p class="pass">Giá CTV: ********</p>
                        <div class="form btn-sign-up relative">
                            <button>Đăng ký CTV để xem giá</a>
                        </div>
                    </div>
                    <div class="review">
                        <img src="<?php echo asset("$path/icon-review.png")?>">
                        <p>Đã bán 10</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                        <p>Hà Nội</p>
                    </div>
                </div>
                <div class="content border-box col-4new">
                    <div class="img-product remove-mg">
                        <img src="<?php echo asset("$path/trend_ao_gio.png")?>" class="lazy">
                    </div>
                    <div class="title-product-n relative">
                        <span>Áo khoác mùa đông kèm hình thật</span>
                        <div class="sale-title">
                            <a href="#" class="sale">
                                Mua kèm deal sốc
                            </a>
                        </div>
                    </div>
                    <div class="product-price">
                        <p class="price">270.000 - 370.000</p>
                        <p class="pass">Giá CTV: ********</p>
                        <div class="form btn-sign-up relative">
                            <button>Đăng ký CTV để xem giá</button>
                        </div>
                    </div>
                    <div class="review">
                        <img src="<?php echo asset("$path/icon-review.png")?>">
                        <p>Đã bán 10</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                        <p>Hà Nội</p>
                    </div>
                </div>
                <div class="content border-box col-4new">
                    <div class="img-product remove-mg">
                        <img src="<?php echo asset("$path/trend_giay.png")?>" class="lazy">
                    </div>
                    <div class="title-product-n relative">
                        <span>[Mã WABRBIT77 giảm 10% đơn 500K] Giày Biti's HunterX2k20</span>
                        <div class="sale-title">
                            <a href="#" class="sale">
                                Mua 2 Giảm 5%
                            </a>
                        </div>
                    </div>
                    <div class="product-price">
                        <p class="price">270.000 - 370.000</p>
                        <p class="pass">Giá CTV: ********</p>
                        <div class="form btn-sign-up relative">
                            <button>Đăng ký CTV để xem giá</button>
                        </div>
                    </div>
                    <div class="review">
                        <img src="<?php echo asset("$path/icon-review.png")?>">
                        <p>Đã bán 10</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                        <p>Hà Nội</p>
                    </div>
                </div>
                <div class="content border-box col-4new">
                    <div class="img-product remove-mg">
                        <img src="<?php echo asset("$path_2/product_1.jpg")?>" class="lazy">
                    </div>
                    <div class="title-product-n relative">
                        <span>Bộ quần áo thể thao Kèm Hình Thật...</span>
                        <div class="sale-title">
                            <a href="#" class="sale">
                                Mua kèm deal sốc
                            </a>
                        </div>
                    </div>
                    <div class="product-price">
                        <p class="price">270.000 - 370.000</p>
                        <p class="pass">Giá CTV: ********</p>
                        <div class="form btn-sign-up relative">
                            <button>Đăng ký CTV để xem giá</button>
                        </div>
                    </div>
                    <div class="review">
                        <img src="<?php echo asset("$path/icon-review.png")?>">
                        <p>Đã bán 10</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                        <p>Hà Nội</p>
                    </div>
                </div>
                <div class="content border-box col-4new">
                    <div class="img-product remove-mg">
                        <img src="<?php echo asset("$path/trend_tui_sach.png")?>" class="lazy">
                    </div>
                    <div class="title-product-n relative">
                        <span>Quần jean ống rộng Kèm Hình Thật...</span>
                        <div class="sale-title">
                            <a href="#" class="sale">
                                Mua kèm deal sốc
                            </a>
                        </div>
                    </div>
                    <div class="product-price">
                        <p class="price">270.000 - 370.000</p>
                        <p class="pass">Giá CTV: ********</p>
                        <div class="form btn-sign-up relative">
                            <a href="#">Đăng ký CTV để xem giá</a>
                        </div>
                    </div>
                    <div class="review">
                        <img src="<?php echo asset("$path/icon-review.png")?>">
                        <p>Đã bán 10</p>
                    </div>
                    <div class="review">
                        <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                        <p>Hà Nội</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
