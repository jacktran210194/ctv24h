<?php
use App\Models\CartModel;use Illuminate\Support\Facades\URL;
$data_user = Session::get('data_customer');
$key_search = \App\Models\KeySearchModel::take(5)->orderBy('number', 'desc')->get();
if (isset($data_user)) {
    $data_login = \App\Models\CustomerModel::find($data_user->id);
    $data_quantity_prd = CartModel::where('customer_id', $data_login->id)->count();
    $data_noti = \App\Models\OrderModel::where('customer_id', $data_login['id'])->where('is_selected', 1)->get();
    $count_noti = 0;
    if(count($data_noti)){
        foreach ($data_noti as $key => $value) {
            $check_noti = \App\Models\NotificationCustomerModel::where('customer_id', $data_login['id'])->where('order_id', $value['id'])->first();
            if ($check_noti) {
                $data_noti[$key]['is_read'] = true;
            } else {
                $data_noti[$key]['is_read'] = false;
                $count_noti ++;
            }
        }
    }
}
?>
<div id="overlay"></div>
<div class="announcement">
    <?php
    $path = "assets/frontend/Icon/home/Danh_muc";
    $pathIcon = "assets/frontend/Icon/home/icon_home";
    ?>
    <div class="img-header">
        <img class="img-header-top" src="<?php echo asset("$path/announcement.png")?>">
    </div>
</div>
<div id="header" class="header-container">
    <div class="container">
        <div class="container-wrapper">
            <div class="pt-3">
                <div class="float-left d-flex align-items-center relative">
                    <a href="<?= URL::to('rank_product') ?>" id="ratings" class="_2QcfeN preventdefault" style="display: none">Xếp hạng</a>
                    <a href="<?= URL::to('deals') ?>" class="_2QcfeN preventdefault" style="display: none">Siêu sale</a>
                    <a href="<?= URL::to('coupons') ?>" class="_2QcfeN">Phiếu giảm giá</a>
                    <a href="<?= URL::to('international_goods') ?>" class="_2QcfeN preventdefault" style="display: none">Hàng quốc tế</a>
                    @if(isset($data_user))
                        <a class="_2QcfeN check-order"><span>Kiểm tra đơn hàng</span>
                        </a>
                        <div class="popup-check-order">
                            <div class="arrow-popup">
                                <p class="title-popup">Kiểm Tra Đơn Hàng</p>
                                <form class="form-check-order" method="GET" action="<?= URL::to('search_order/') ?>">
                                    <label class="title-input">Nhập mã đơn hàng </label>
                                    <input required type="text" id="order_code" name="order_code" class="code-order">
                                    <button type="submit" class="btn btn-main btn_check_order">Gửi</button>
                                </form>
                            </div>
                        </div>
                    @endif
                    <a class="_2QcfeN preventdefault" style="display: none">Tải ứng dụng</a>
                </div>
                <div class="float-right d-flex align-items-center">
                    <a class="_2QcfeN preventdefault" style="display:none;">Hỗ trợ</a>
                    @if(!$data_user)
                        <a href="{{url('login')}}"
                           class="_2QcfeN"
                        >Kênh NCC
                        </a>
                    @else
                        <a target="_blank" href="{{url('admin/supplier_admin/')}}"
                           class="_2QcfeN"
                        >Kênh NCC
                        </a>
                    @endif

                    @if(!$data_user)
                        <a href="{{url('login')}}"
                           class="_2QcfeN"
                        >Kênh CTV
                        </a>
                    @else
                        <a target="_blank" href="{{url('admin/collaborator_admin/')}}"
                           class="_2QcfeN"
                        >Kênh CTV
                        </a>
                    @endif
                    <a href="<?= URL::to('sell_with_ctv24') ?>" class="_2QcfeN">Mua bán cùng CTV24h</a>
                </div>
            </div>
        </div>
        <div class="mt-4 container-wrapper-search d-flex">
            <div class="col-lg-2 p-0 align-items-center pt-3">
                <a href="<?= URL::to('/') ?>"><img class="logo-img" src="Icon/logo.png"></a>
            </div>
            <div class="container-form-search p-0 pt-2" style="margin: 0 2rem">
                <form method="GET" action="<?= URL::to('search/') ?>">
                    <div class="d-flex content-form-search">
                        <div class="form-search form-control">
                            <button style="border: none; background: none; margin-right: 5px!important;" type="submit"
                                    class="Icon-search">
                                <object style="width: 23px!important; height: 23px!important;"
                                        data="Icon/home/Search.svg"></object>
                            </button>
                            <input required type="text" name="keyword" value="{{isset($keyword) ? $keyword : ''}}"
                                   placeholder="Search">
                        </div>
                    </div>
                </form>
                <div class="mt-2">
                    <div class="float-left d-flex align-items-center">
                        @isset($key_search)
                            @foreach($key_search as $value)
                                <a href="{{url('search?keyword='.$value->title)}}" class="_1QcfeN">{{$value->title}}</a>
                            @endforeach
                        @endisset
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-0 justify-content-between d-flex">
                <a href="<?= URL::to('cart') ?>" class="icon_cart">
                    <div class="d-flex justify-content-center">
                        <img src="<?php echo asset("$pathIcon/cart.png")?>" style="width: 30px; height: 30px">
                    </div>
                    <p class="_2QcfeN pt-1">Giỏ hàng</p>
                    <?php
                    if (isset($data_user)) {
                        $count_order_cart = CartModel::where('customer_id', $data_user->id)->count();
                        echo '<p class="count-item">' . $count_order_cart . '</p>';
                    }
                    ?>
                </a>
                <a href="<?= URL::to('promotion') ?>" style="display: none">
                    <div class="d-flex justify-content-center">
                        <img src="<?php echo asset("$pathIcon/sale.png")?>" style="width: 30px; height: 30px">
                    </div>
                    <p class="_2QcfeN pt-1">Khuyến mại</p>
                </a>
                <a href="<?= URL::to('chat') ?>" class="position-relative">
                    <div class="d-flex justify-content-center">
                        <img src="<?php echo asset("$pathIcon/chat.png")?>" style="width: 30px; height: 30px">
                    </div>
                    <p class="_2QcfeN pt-1">Chat</p>
                    <?php
                    if (isset($data_user)) {
                        $count_chat = \App\Models\MessegeModel::where('user_receiver_id', $data_user->id)->where('checked', 0)->count();
                        echo '<p class="m-0 count-chat position-absolute">' . $count_chat . '</p>';
                    }
                    ?>
                </a>
                <a href="<?= URL::to('love_ly') ?>">
                    <div class="d-flex justify-content-center">
                        <img src="<?php echo asset("$pathIcon/like.png")?>" style="width: 30px; height: 30px">
                    </div>
                    <p class="_2QcfeN pt-1">Yêu thích</p>
                </a>
                <a href="<?= URL::to('my_history/by_product?status=all') ?>">
                    <div class="d-flex justify-content-center">
                        <img src="<?php echo asset("$pathIcon/alert.png")?>" style="width: 30px; height: 30px">
                    </div>
                    <p class="_2QcfeN pt-1">Lịch sử</p>
                </a>
                <a href="<?= URL::to('my_auction') ?>" class="preventdefault" style="display: none">
                    <div class="d-flex justify-content-center">
                        <img src="<?php echo asset("$pathIcon/auction.png")?>" style="width: 30px; height: 30px">
                    </div>
                    <p class="_2QcfeN pt-1">Đấu giá của tôi</p>
                </a>
                <a href="<?= URL::to('notification/update_order') ?>">
                    <div class="d-flex justify-content-center">
                        <img src="<?php echo asset("$pathIcon/notification.png")?>" style="width: 30px; height: 30px">
                    </div>
                    <p class="_2QcfeN pt-1">Thông báo</p>
                    @if($data_user && $count_noti > 0)
                        <p class="count-item">@if($count_noti > 99) 99+ @else {{$count_noti}} @endif</p>
                    @endif
                </a>
            </div>
        </div>
    </div>
    <div class="popup-ratings">
        <div class="content-rating">
            <button class="close-popup"><img src="<?php echo asset("$path/icon_close.png")?>"></button>
            <div class="content-image">
                <img src="<?php echo asset("$path/Component.png")?>">
            </div>
        </div>
    </div>
</div>
<div class="announcement">
    <img class="img-header-top" src="<?php echo asset("$path/announcement_2.png")?>">
</div>
@if(isset($data_user))
    @include('frontend.base.header_login')
@else
    @include('frontend.base.header_not_login')
@endif


