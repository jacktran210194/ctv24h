<div class="section section-xu-huong">
    <div class="title-section">
        <div class="content">
            <div class="logo">
                <img src="Icon/home/Danh_muc/icon-logo-homepage.png">
            </div>
            <h2>XU HƯỚNG TÌM KIẾM</h2>
        </div>
{{--        <div class="link preventdefault"><a href="#">Xem Tất Cả</a> </div>--}}
    </div>
    <div class="relative">
        <div class="container-info-product">
            <?php if(isset($dataKeySearch)):?>
            <?php foreach ($dataKeySearch as $value) :?>
            <div class="info-product">
                <a class="d-flex justify-content-lg-center align-items-center w-100" href="{{url('search?keyword='.$value->title)}}">
                    <div class="info">
                        <div class="titel-product">
                            <span>{{$value->title}}</span>
                        </div>
                        <div class="text">
                            <span class="price">{{number_format($value->number/1000)}}k + sản phẩm</span>
                        </div>
                    </div>
                    <div class="image-prodcut">
                        <img class="lazy" src="{{$value->image}}" alt="">
                    </div>
                </a>
            </div>
            <?php endforeach ;?>
            <?php endif; ?>
        </div>
    </div>
</div>
