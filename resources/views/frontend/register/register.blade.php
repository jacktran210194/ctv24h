<!DOCTYPE html>
<?php use Illuminate\Support\Facades\URL;?>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/home/style.css">
<link rel="stylesheet" type="text/css" href="css/register/register_ncc.css">
<link href="https://cdn.bootcss.com/font-awesome/5.7.2/css/all.min.css">
<link rel="stylesheet" type="text/css" href="css/captcha/slidercaptcha.css">
<script src="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.js"></script>
<link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.css"/>
<meta name="csrf-token" content="{{ csrf_token() }}"/>
@include('frontend.register.header')
<div class="" id="main"
     style="background-image: url('{{ asset('assets/frontend/Icon/register/bg_image.png')}}'); background-size: cover; background-repeat: no-repeat; padding-bottom: 100px">
    <div>
        <div class="bg-white form-register position-relative" style="width: 40%;">
            <div class="form">
                <h3>Đăng Ký</h3>
                <div class="form-group">
                    <label class="lb-1" for="">Họ và tên</label>
                    <input type="text" id="name" class="form-control" placeholder="Họ và tên" value="">
                </div>
                <div class="form-group">
                    <label class="lb-1" for="">Email</label>
                    <input type="email" id="email" class="form-control" placeholder="Địa chỉ email" value="">
                </div>
                <div class="form-group">
                    <label class="lb-1" for="">Số điện thoại</label>
                    <div class="phone d-flex">
                        <input type="text" id="phone" class="form-control input-phone"
                               placeholder="Nhập số điện thoại của bạn"
                               value="">
                        <div class="form_btn_otp">
                            <button style="width: 120px!important;" class="btn btn-main btn_send_otp">Gửi mã OTP
                            </button>
                        </div>
                    </div>
                </div>
                <div id="recaptcha-container"></div>
                <div id="captcha" class="mb-3"></div>
                <div id="recaptcha-container-1"></div>
                <div id="captcha-1" class="mb-3"></div>
                <div class="form-group form_add_otp" style="display: none;">
                    <label class="lb-1" for="">Mã OTP</label>
                    <div class="d-flex">
                        <input type="text" class="form-control form-otp"
                               placeholder="Nhập Mã OTP được gửi về Số Điện Thoại">
                        <button style="width: 120px!important;" class="btn btn-main btn_check_otp"> Xác minh OTP
                        </button>
                    </div>
                </div>
                <div class="form-group">
                    <label class="lb-1" for="">Mật khẩu</label>
                    <div class="password">
                        <input id="password-field" type="password" class="form-control" name="password"
                               placeholder="Tối thiểu 8 kí tự bao gồm cả chữ và số">
                        <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    </div>
                </div>
                {{--                <div class="d-flex align-content-center align-items-center">--}}
                {{--                    <div class="row">--}}
                {{--                        <div class="col-lg-7">--}}
                {{--                            <div class="form-group">--}}
                {{--                                <label class="lb-1">Ngày sinh </label>--}}
                {{--                                <input id="birthday" type="date" class="form-control input_date">--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                        <div class="col-lg-5">--}}
                {{--                            <div class="form-group">--}}
                {{--                                <label class="lb-1" for="">Giới tính</label>--}}
                {{--                                <div class="d-flex">--}}
                {{--                                    <label class="container_radio">Nam--}}
                {{--                                        <input value="0" type="radio" id="gender" name="gender">--}}
                {{--                                        <span class="radio_config"></span>--}}
                {{--                                    </label>--}}
                {{--                                    <label class="container_radio ml-3">Nữ--}}
                {{--                                        <input value="1" type="radio" id="gender" name="gender">--}}
                {{--                                        <span class="radio_config"></span>--}}
                {{--                                    </label>--}}
                {{--                                </div>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                <div class="text-center mt-4">
                    <button disabled type="button" class="btn btn-main btn-register"
                            data-url="<?= URL::to('register/save') ?>">
                        Đăng ký
                    </button>
                </div>
                <div class="pt-3">
                    <a class="text-law" href="{{url('forgot_password')}}">Quên mật khẩu</a>
                </div>
                <div class="text-center pt-3">
                    Hoặc đăng ký bằng
                </div>
                <div class="d-flex justify-content-around mt-3">
                    <div>
                        <a>
                            <img src="Icon/register/google.png" width="42px" alt="">
                        </a>
                    </div>
                    <div>
                        <a>
                            <img src="Icon/register/facebook.png" width="42px" alt="">
                        </a>
                    </div>
                    <div>
                        <a>
                            <img src="Icon/register/zalo.png" width="42px" alt="">
                        </a>
                    </div>
                    <div>
                        <a>
                            <img src="Icon/register/line.png" width="42px" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="">
                            <img src="Icon/register/instagram.png" width="42px" alt="">
                        </a>
                    </div>
                    <div>
                        <a>
                            <img src="Icon/register/tiktok.png" width="42px" alt="">
                        </a>
                    </div>
                </div>
                <div class="text-center pt-3">
                    Bạn đã là thành viên? <a class="law" href="{{url('login')}}">Đăng nhập ngay</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: Danh Muc-->
<!-- END: Homepage-->

<!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
@include('frontend.base.script')
<script src="js/captcha/longbow.slidercaptcha.js"></script>
<script>

    $(document).ready(function () {
        let i = 0;
        let verificationId;
        let provider;
        let applicationVerifier;
        let phone;
        let phone_auth;
        let html = '<button disabled style="width: 120px!important;" class="btn btn-main btn_return_send_otp">\nGửi lại OTP\n' +
            '                            <span id="timer"></span></button>';

        $(document).on('click', '.btn_send_otp', async function () {
            phone = $('.input-phone').val();
            let check_phone = phone.slice(0, 1);
            let length_phone = phone.length;
            if ($('#phone').val() == '') {
                swal('Thông báo', "Vui lòng nhập số điện thoại !", "error");
            } else if (check_phone !== '0') {
                swal('Thông báo', 'Số điện thoại bạn vừa nhập không hợp lệ !', 'error');
            } else if ((length_phone < 10) || (length_phone > 10)) {
                swal('Thông báo', 'Độ dài số điện thoại không hợp lệ !', 'error');
            } else {
                $(this).prop('disabled', true);
                let fs = phone.slice(0, 3);
                if (fs !== '+84') {
                    phone = phone.slice(1, phone.length);
                    phone = '+84' + phone;
                }
                $('#captcha').removeClass('hide_btn');
                $('#recaptcha-container').show();
                sliderCaptcha({
                    id: 'captcha',
                    width: 300,
                    height: 200,
                    sliderL: 42,
                    sliderR: 9,
                    offset: 5,
                    loadingText: 'Loading...',
                    failedText: 'Thử lại',
                    barText: 'Kéo sang phải để hoàn thành',
                    repeatIcon: 'fa fa-redo',
                    setSrc: function () {
                        return window.location.origin + '/assets/frontend/Icon/register/image_pic_'+ Math.round(Math.random() * 7) +'.jpg';
                    },
                    onSuccess: async function () {
                        applicationVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
                            'size': 'invisible',
                        });
                        provider = new firebase.auth.PhoneAuthProvider();
                        try {
                            verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                            $('#recaptcha-container').hide();
                            $('#captcha').addClass('hide_btn');
                            $('.form_add_otp').show();
                            let phone_check = phone.slice(0, 1);
                            if (phone_check !== '0') {
                                phone = phone.slice(3, phone.length);
                                phone_auth = '0' + phone;
                            }
                        } catch (e) {
                            console.log(e);
                            swal("Thông báo", "Đã có lỗi xảy ra !", "error");
                            // setTimeout(function () {
                            //     location.reload();
                            // }, 1500);
                        }
                    }
                });
            }
        });

        $(document).on('click', '.btn_return_send_otp', function () {
            phone = $('.input-phone').val();
            let check_phone = phone.slice(0, 1);
            let length_phone = phone.length;
            if ($('#phone').val() == '') {
                swal('Thông báo', "Vui lòng nhập số điện thoại !", "error");
            } else if (check_phone !== '0') {
                swal('Thông báo', 'Số điện thoại bạn vừa nhập không hợp lệ !', 'error');
            } else if ((length_phone < 10) || (length_phone > 10)) {
                swal('Thông báo', 'Độ dài số điện thoại không hợp lệ !', 'error');
            } else {
                $(this).prop('disabled', true);
                let fs = phone.slice(0, 3);
                if (fs !== '+84') {
                    phone = phone.slice(1, phone.length);
                    phone = '+84' + phone;
                }
                $('#captcha-1').removeClass('hide_btn');
                $('#recaptcha-container-1').show();
                sliderCaptcha({
                    id: 'captcha-1',
                    width: 300,
                    height: 200,
                    sliderL: 42,
                    sliderR: 9,
                    offset: 5,
                    loadingText: 'Loading...',
                    failedText: 'Thử lại',
                    barText: 'Kéo sang phải để hoàn thành',
                    repeatIcon: 'fa fa-redo',
                    setSrc: function () {
                        return window.location.origin + '/assets/frontend/Icon/register/image_pic_'+ Math.round(Math.random() * 7) +'.jpg';
                    },
                    onSuccess: async function () {
                        applicationVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container-1', {
                            'size': 'invisible',
                        });
                        provider = new firebase.auth.PhoneAuthProvider();
                        try {
                            verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                            $('.btn_send_otp').prop('disabled', true);
                            $('#recaptcha-container-1').hide();
                            $('#captcha-1').addClass('hide_btn');
                            $('.form_add_otp').show();
                            let phone_check = phone.slice(0, 1);
                            if (phone_check !== '0') {
                                phone = phone.slice(3, phone.length);
                                phone_auth = '0' + phone;
                            }
                        } catch (e) {
                            console.log(e);
                            swal("Thông báo", "Đã có lỗi xảy ra !", "error");
                            setTimeout(function () {
                                location.reload();
                            }, 1500);
                        }
                    }
                });
            }
        });

        $(document).on('click', '.btn_check_otp', async function () {
            if ($('.form-otp').val() == '') {
                swal('Thông báo', 'Vui lòng nhập mã OTP !', 'error');
            }
            try {
                let verificationCode = $('.form-otp').val();
                let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
                try {
                    await firebase.auth().signInWithCredential(phoneCredential);
                    swal("Thông báo,", "Xác minh số điện thoại thành công !", "success");
                    $('.input-phone').prop('disabled', true);
                    $('.btn-register').prop('disabled', false);
                    $('.btn_return_send_otp').hide();
                    $('.btn_send_otp').hide();
                    $('.form_add_otp').hide();
                } catch (e) {
                    swal("Thông báo", "Mã OTP không chính xác !", "error");
                    i++;
                    provider = undefined;
                    verificationId = undefined;
                    applicationVerifier = undefined;
                    $('.btn_send_otp').addClass('hide_btn');
                    let count = 61;
                    let counter = setInterval(timer, 1000);

                    function timer() {
                        count = count - 1;
                        if (count < 0) {
                            clearInterval(counter);
                            return;
                        }
                        if (count == 0) {
                            $('.btn_return_send_otp').prop('disabled', false);
                            $('#timer').hide();
                        }
                        $('#timer').html(count + 's');
                    }

                    timer();
                    $('.form_btn_otp').append(html);
                    if (i == 2) {
                        window.location.reload();
                    }
                    $('.form_add_otp').hide();
                    let emtyhtml = "";
                    $('#recaptcha-container').html(emtyhtml);
                    $('#captcha').html(emtyhtml);
                    $('.form-otp').html(emtyhtml);
                }
            } catch (e) {
                swal("Thông báo", "Xác minh thất bại !", "error");
                setTimeout(function () {
                    location.reload();
                }, 1500);
            }
        });

        $(document).on('click', '.btn-register', function () {
            if ($('#name').val() == '' || $('#phone').val() == '' || $('#password-field').val() == '' || $('#email').val() == '') {
                swal("Thông báo", "Vui lòng nhập đủ thông tin !", "error");
            } else {
                let url = $(this).attr('data-url');
                let form_data = new FormData();
                let email = $('#email').val();
                function validateEmail(email_check) {
                    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(String(email_check).toLowerCase());
                }
                if(!validateEmail(email)){
                    swal('Thông báo','Không đúng định dạng email !','warning');
                    return false;
                }
                if (phone_auth == undefined) {
                    swal("Thông báo", "Số điện thoại của bạn chưa được xác minh!", "error");
                } else {
                    form_data.append('name', $('#name').val());
                    form_data.append('phone', phone_auth);
                    form_data.append('password', $('#password-field').val());
                    // form_data.append('birthday', $('#birthday').val());
                    // form_data.append('gender', $('#gender:checked').val());
                    form_data.append('email', $('#email').val());
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: form_data,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status) {
                                swal("Thông báo", data.msg, "success");
                                window.location.href = data.url;
                            } else {
                                swal("Thông báo", data.msg, "error");
                            }
                        },
                        error: function () {
                            console.log('error');
                        },
                    });
                }
            }
        });

        $(document).on('click', '.toggle-password', function () {
            $(this).toggleClass("fa-eye fa-eye-slash");
            let input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    })
</script>
<!-- END: Body-->
</html>
