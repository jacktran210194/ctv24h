<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<style>
    .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 35px;
        background: #ffe6e6;
        border-radius: 0.1875rem;
        outline: none;
        -webkit-transition: .2s;
        transition: opacity .2s;
        /*pointer-events: none;*/
    }

    .slider:hover {
        opacity: 1;
    }

    .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 45px;
        height: 40px;
        cursor: pointer;
        background-color: red;
        border-radius: 3px 0 0 3px !important;
    }

    .slider::-moz-range-thumb {
        width: 45px;
        height: 35px;
        background: #04AA6D;
        cursor: pointer;
    }
</style>

<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/home/style.css">
<link rel="stylesheet" type="text/css" href="css/register/register_ncc.css">
<meta name="csrf-token" content="{{ csrf_token() }}" />
@include('frontend.register.header')
<div id="main"
     style="background-image: url('{{ asset('assets/frontend/Icon/register/bg_image.png')}}'); background-size: cover; background-repeat: no-repeat; padding-bottom: 100px">
    <div>
        <div class="bg-white form-register" style="width: 40%;">
            <div class="form">
                <h3>Đăng Ký NCC</h3>
                <div class="form-group">
                    <label class="lb-1" for="">Họ và tên</label>
                    <input type="text" id="name" class="form-control" placeholder="Họ và tên" value="">
                </div>
                <div class="form-group">
                    <label class="lb-1" for="">Số điện thoại</label>
                    <div class="phone d-flex">
                        <input type="text" id="phone" class="form-control input-phone" placeholder="0389212343"
                               value="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="position-relative">
                        <input type="range" min="1" max="100" value="0" class="slider" id="myRange">
                        <p class="text-otp position-absolute position-center" style="">Trượt để nhận mã OTP</p>
                    </div>
                </div>
                <div id="recaptcha-container"></div>
                <div class="form-group">
                    <label class="lb-1" for="">Mã OTP</label>
                    <input type="text" class="form-control form-otp" placeholder="Nhập Mã OTP được gửi về Số Điện Thoại"
                           disabled>
                </div>
                <div class="form-group">
                    <label class="lb-1" for="">Mật khẩu</label>
                    <div class="password">
                        <input id="password-field" type="password" class="form-control" name="password"
                               placeholder="Tối thiểu 8 kí tự bao gồm cả chữ và số">
                        <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="lb-1" for="">Số chứng minh thư nhân dân/CMT</label>
                    <input type="text" id="id_card" class="form-control" placeholder="Nhập số chứng minh thư nhân dân">
                </div>
                <div class="form-group">
                    <label class="lb-1" for="">Giấy phép kinh doanh</label>
                    <input type="text" id="business_license" class="form-control" placeholder="Nhập câu hỏi bí mật của bạn">
                </div>
                <div class="form-group">
                    <label class="lb-1" for="">Thông tin cung ứng sản phẩm</label>
                    <input id="product_supply" type="text" class="form-control" placeholder="Nhập thông tin cũng ứng sản phẩm ">
                </div>
                <div class="form-group">
                    <label class="lb-1" for="">Giấy tờ nhập khẩu</label>
                    <textarea class="form-control" name="" id="import_papers" cols="30" rows="3"></textarea>
                </div>
                <p class="text-center text-law">Bằng cách bấm vào đăng ký, tôi đồng ý với các <a class="law" href="">điều
                        khoản</a></p>
                <div class="text-center mt-4">
                    <button type="button" class="btn btn-main btn-register" data-url="{{url('register/save_ncc')}}" >Đăng ký</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: Danh Muc-->
<!-- END: Homepage-->

<!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
@include('frontend.base.script')
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-firestore.js"></script>
<script src="js/otp/index.js"></script>
<script>
    $(document).ready(function () {
        $(".toggle-password").click(function () {

            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
        let verificationId;
        $(document).on('change', '#myRange', async function () {
            let value = $(this).val();
            if (value == 100) {
                $('#recaptcha-container').show();
                let applicationVerifier = new firebase.auth.RecaptchaVerifier(
                    'recaptcha-container');
                let phone = $('.input-phone').val();
                let fs = phone.slice(0, 3);
                if (fs !== '+84') {
                    phone = phone.slice(1, phone.length);
                    phone = '+84' + phone;
                }
                let provider = new firebase.auth.PhoneAuthProvider();
                try {
                    verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                    $('#recaptcha-container').hide();
                    $('.form-otp').prop('disabled', false);
                } catch (e) {
                    console.log('e');
                    swal("Thất bại", "Số điện thoại vừa nhập không đúng !", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
                }
            }
        });
        $(document).on('click', '.btn-register', async function () {
            try {
                let verificationCode = $('.form-otp').val();
                let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
                try {
                    await firebase.auth().signInWithCredential(phoneCredential);
                    let url = $(this).attr('data-url');
                    let form_data = new FormData();
                    form_data.append('name', $('#name').val());
                    form_data.append('phone', $('#phone').val());
                    form_data.append('password', $('#password-field').val());
                    form_data.append('id_card', $('#id_card').val());
                    form_data.append('business_license', $('#business_license-field').val());
                    form_data.append('product_supply', $('#product_supply').val());
                    form_data.append('import_papers', $('#import_papers').val());

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: form_data,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status) {
                                swal("Thành công", data.msg, "success");
                                window.location.href = data.url;
                            } else {
                                swal("Thất bại", data.msg, "error");
                            }
                        },
                        error: function () {
                            console.log('error')
                        },
                    });
                } catch (e) {
                    swal("Thất bại", "Mã OTP không chính xác !", "error");
                }
            } catch (e) {
                swal("Thất bại", "Mã OTP không chính xác !", "error");
            }
        });
    })

</script>
</body>
<!-- END: Body-->
</html>
