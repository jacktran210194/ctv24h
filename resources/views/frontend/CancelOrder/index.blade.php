<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/DetailOrder/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->
    <?php
    $path = "assets/frontend/Icon/DetailOrder";
    ?>
    <div class="container body-page cancel-order">
        <div class="d-flex nav-page">
            <p class="come-back">
                TRỞ LẠI
            </p>
            <div class="title-details-order d-flex">
                <p class="title-order">Chi Tiết Đơn Hàng: 210112397FQC</p>
                <p class="delivered">ĐÃ HỦY</p>
            </div>
        </div>
        <div class="detail-deliver">
            <div class="d-flex steps-order">
                <div class="box-steps orders-placed active">
                    <div class="d-flex image-icon">
                        <div class="icon-steps">
                            <img src="<?php echo asset("$path/Group-16715.png") ?>">
                        </div>
                    </div>
                    <div class="text-steps">
                        <p class="title-steps">Đơn hàng đã đặt </p>
                        <p class="times">14:31  12-01-2021</p>
                    </div>
                </div>
                <div class="box-steps orders-placed active">
                    <div class="d-flex image-icon">
                        <div class="icon-steps">
                            <img src="<?php echo asset("$path/cancel.png") ?>">
                        </div>
                    </div>
                    <div class="text-steps">
                        <p class="title-steps">Đơn hàng đã bị hủy </p>
                        <p class="times">14:31  12-01-2021</p>
                    </div>
                </div>
            </div>
            <div class="d-flex information-order">
                <div class="box-information">
                    <p class="title-information">Địa chỉ người nhận </p>
                    <p class="name">Nguyễn Thị Hồng Vân (+84)385928310</p>
                    <p class="address">CT2, C14 Bắc Hà, Phường Trung Văn,
                        Quận Nam Từ Liêm, Hà Nội,</p>
                </div>
                <div class="box-information">
                    <p class="title-information">Tình trạng vận chuyển</p>
                    <p class="transporters">Nhà vận chuyển: <span class="name-transporters">Giao hàng tiết kiệm</span> </p>
                    <p class="status">Tình trạng: <span class="review-status cancel ">Đã hủy</span> </p>
                </div>
                <div class="box-information">
                    <p class="title-information">Hình thức thanh toán</p>
                    <p class="name">Thanh toán tiền mặt khi nhận hàng </p>
                </div>
            </div>
        </div>
        <div class="item-content">
            <div class="d-flex item-shop">
                <div class="d-flex name-shop">
                    <object data="Icon/cart/shop_name.svg" style="width: 23px; height: 23px;"></object>
                    <p class="name">Haustore .s4e</p>
                </div>
                <button class="btn-like">Yêu thích</button>
                <a href="{{url("seeshop/2")}}">
                    <button class="btn-seeshop">Xem shop</button>
                </a>
                <button class="btn-chat-shop">
                    <img src="<?php echo asset("$path/chat.png") ?>">
                    <p>Chat</p>
                </button>
            </div>
            <div class="d-flex item-product">
                <div class="d-flex image-product">
                    <img src="Icon/DetailOrder/img.png">
                </div>
                <div class="infor-product">
                    <p class="title-product">Áo khoác nữ lót lông hottren 2020, chuẩn hàng quảng châu loại 1 sẵn DN</p>
                    <p class="brank">Phân loại: Xanh - M</p>
                    <p class="quantity">x1</p>
                </div>
                <div class="price-product">
                    <p class="price">1.400.000đ</p>
                    <p class="discount-price">700.000đ</p>
                </div>
            </div>
        </div>
        <div class="bottom d-flex">
            <div class="total-price">
                <div class="d-flex content">
                    <p class="text-content">Tổng số tiền: </p>
                    <p class="text-right">710.000đ</p>
                </div>
                <div class="d-flex content">
                    <p class="text-content">Vận chuyển giao hàng nhanh: </p>
                    <p class="text-right">20.000đ</p>
                </div>
                <div class="d-flex content all-price">
                    <p class="text-content">Tổng số tiền:</p>
                    <p class="text-right">730.000đ</p>
                </div>
            </div>
        </div>
        <div class="button d-flex"><button class="btn-buy-again">Mua lần nữa</button></div>
    </div>
    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
