<?php
use App\Models\CustomerModel;use Illuminate\Support\Facades\URL;
$data_customer = Session::get('data_customer');
if (isset($data_customer)) {
    $customer = CustomerModel::find($data_customer['id']);
}
?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/rechargeCard/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->
    <?php
    $path = "assets/frontend/Icon/rechargeCard";
    $pathIcon = "assets/frontend/Icon/WelcomeCTV24H";
    ?>
    <div class="container body-page">
        <div class="banner-page">
            <img src="<?php echo asset("$path/banner.png")?>">
        </div>
        <?php if($type === 'recharge')  :?>
        <div class="benefit">
            <h2>
                <?php
                if ($customer["point"] < 500) {
                    echo "Bạn đang ở hạng đồng";
                } elseif ($customer["point"] >= 500 && $customer["point"] < 1500) {
                    echo "Bạn đang ở hạng bạc";
                } elseif ($customer["point"] >= 1500 && $customer["point"] < 2500) {
                    echo "Bạn đang ở hạng vàng";
                } else {
                    echo "Bạn đang ở hạng kim cương";
                }
                ?>
            </h2>
            <div class="form-benefit">
                <div class="member box">
                    <div class="header-box">
                        <p class="text">Quyền lợi</p>
                        <p class="title">Thành viên</p>
                    </div>
                    <div class="content-body">
                        <ul class="content">
                            <li>Nhận biểu tượng vip</li>
                            <li>Đổi quà trên trang vip</li>
                            <li>Ưu tiên hỗ trợ qua hotline, website</li>
                            <li>Tham gia minigame nhận quà</li>
                            <li>Tham gia khảo sát nhận quà</li>
                            <li>Nhận quà sinh nhật, lễ tết và các sự kiện mua sắm trong năm</li>
                        </ul>
                    </div>
                </div>
                <div class="basic box">
                    <div class="header-box">
                        <img class="absolute" src="<?php echo asset("$path/bg_border_1.png")?>">
                        <p class="text-basic">Basic</p>
                    </div>
                    <div class="content-body">
                        <ul class="content basic-checked">
                            <li>Giảm giá 0%</li>
                            <li><img src="<?php echo asset("$pathIcon/icon_false.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_false.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_false.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_false.png")?>"></li>
                        </ul>
                    </div>
                </div>
                <div class="basic box silver">
                    <div class="header-box">
                        <img class="absolute" src="<?php echo asset("$path/bg_border_2.png")?>">
                        <div class="text-basic">
                            <div class="icon"><img src="<?php echo asset("$path/icon_1.png")?>"></div>
                            <p class="text">Silver</p>
                        </div>
                        <p class="number-point"> 500 điểm </p>
                    </div>
                    <div class="content-body">
                        <ul class="content basic-checked">
                            <li>Giảm giá 1,5%</li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li class="btn-piont">
                                <button class="btn-change-piont"
                                data-point = "500" data-money="99000"
                                >
                                    <a href="
                                    @if($data_login == null)
                                    <?= URL::to('login') ?>
                                    @else
                                    <?= URL::to('recharge-card?type=currentPoint') ?>
                                    @endif
                                        ">Nâng hạng <span style="display: block">99$</span></a>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="basic box gold">
                    <div class="header-box">
                        <img class="absolute" src="<?php echo asset("$path/bg_border_3.png")?>">
                        <div class="text-basic">
                            <div class="icon"><img src="<?php echo asset("$path/icon_2.png")?>"></div>
                            <p class="text">Gold</p>
                        </div>
                        <p class="number-point"> 1500 điểm </p>
                    </div>
                    <div class="content-body">
                        <ul class="content basic-checked">
                            <li>Giảm giá 3%</li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li class="btn-piont">
                                <button class="btn-change-piont"
                                data-point = "1500" data-money="199000"
                                >
                                    <a href="
                                    @if($data_login == null)
                                    <?= URL::to('login') ?>
                                    @else
                                    <?= URL::to('recharge-card?type=currentPoint') ?>
                                    @endif
                                        ">Nâng hạng <span style="display: block">199$</span></a>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="basic box platinum">
                    <div class="header-box">
                        <img class="absolute" src="<?php echo asset("$path/bg_border_4.png")?>">
                        <div class="text-basic">
                            <div class="icon"><img src="<?php echo asset("$path/icon_3.png")?>"></div>
                            <p class="text">Platinum</p>
                        </div>
                        <p class="number-point"> 2500 điểm </p>
                    </div>
                    <div class="content-body">
                        <ul class="content basic-checked">
                            <li>Giảm giá 4%</li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li><img src="<?php echo asset("$pathIcon/icon_true.png")?>"></li>
                            <li class="btn-piont">
                                <button class="btn-change-piont"
                                data-point = "2500" data-money="299000"
                                >
                                    <a href="
                                    @if($data_login == null)
                                    <?= URL::to('login') ?>
                                    @else
                                    <?= URL::to('recharge-card?type=currentPoint') ?>
                                    @endif
                                        ">Nâng hạng <span style="display: block">299$</span></a>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php else :?>
        <div class="action-change-piont">
            <div class="container-left">
                <div class="customer d-flex align-items-center w-100">
                    <div class="image">
                        <?php
                        if (empty($customer['image'])) {
                            echo "<img src='Icon/user/images_customer.png'>";
                        } else {
                            echo "<img src='" . $customer['image'] . "'>";
                        }
                        ?>
                    </div>
                    <div class="name-customer">
                        <p class="name" style="font-size: 26px!important;"> {{ $customer["name"] }}</p>
                        <p class="point-of-customer" style="font-size: 24px!important;">
                            <?php
                            if ($customer["point"] < 500) {
                                echo "Bạn đang ở hạng đông";
                            } elseif ($customer["point"] >= 500 && $customer["point"] < 1500) {
                                echo "Bạn đang ở hạng bạc";
                            } elseif ($customer["point"] >= 1500 && $customer["point"] < 2500) {
                                echo "Bạn đang ở hạng vàng";
                            } else {
                                echo "Bạn đang ở hạng kim cương";
                            }
                            ?>
                        </p>
                    </div>
                </div>
                <div class="input-range">
                    <div class="d-flex align-items-center justify-content-lg-between">
                        <p class="range" data-range="{{$customer["point"]}}">
                            <span class="before"></span>
                            <span class="after"></span>
                        </p>
                        <img src="Icon/user/icon_gold.png">
                    </div>
                    <p class="update-piont">
                        <?php
                        $point = $customer["point"];
                        if ($point < 500) {
                            $update_point = 500 - $point;
                            echo "Bạn cần" ."<span>" . $update_point . "</span>" . "điểm để lên hạng Bạc";
                        } elseif ($point >= 500 && $point < 1500) {
                            $update_point = 1500 - $point;
                            echo "Bạn cần" ."<span>" . $update_point . "</span>" . "điểm để lên hạng Vàng";
                        } elseif ($point >= 1500 && $point < 2500) {
                            $update_point = 2500 - $point;
                            echo "Bạn cần" ."<span>" . $update_point . "</span>" . "điểm để lên hạng Kim Cương";
                        } else {
                            echo "Bạn đang ở hạng kim cương";
                        }
                        ?>
                    </p>
                </div>

                <p class="tite-box">
                    Số điểm hiện tại của bạn
                </p>
                <p class="current-point">
                    {{$customer["point"]}}
                </p>

                <div class="box-btn">
                    <button class="btn-current-point">Nâng Hạng</button>
                </div>
            </div>
            <div class="container-right">
                <img src="<?php echo asset("$path/banner-2.png")?>">
            </div>
        </div>
        <?php endif; ?>
    </div>
    <div class="container-popup-current">
        <div class="popup-current-piont">
            <div class="header-popup-2">
                <h2 class="m-0">Đổi Điểm</h2>
            </div>
            <div class="body-popup">
                <p class="title-body-popup">Số điểm hiện tại </p>
                <p class="number">
                    @isset($customer)
                        {{$customer['point']}}
                    @endisset
                </p>
                <p class="title-body-popup">Số tiền để lên hạng <span>
                    @isset($customer)
                        <?php
                            $point = $customer["point"];
                            if ($point < 500) {
                                echo " Bạc ";
                            } elseif ($point >= 500 && $point < 1500) {
                                echo "Vàng";
                            } elseif ($point >= 1500 && $point < 2500) {
                                echo "Kim Cương";
                            } else {

                            }
                            ?>
                    @endisset
                </span></p>
                <p class="number">
                    @isset($customer)
                        <?php
                        $point = $customer["point"];
                        if ($point < 500) {
                            echo " 99$ ";
                        } elseif ($point >= 500 && $point < 1500) {
                            echo "199$";
                        } elseif ($point >= 1500 && $point < 2500) {
                            echo "299$";
                        } else {
                        }
                        ?>
                    @endisset
                </p>
            </div>
            <div class="option-popup">
                <p class="title-option-popup">Hình thức đổi điểm</p>
                <div class="option">
                    <p class="radio" data-option="payByWallet"></p>
                    <label>Thanh toán bằng thẻ quốc tế Visa, Master, JCB</label>
                </div>
                <div class="option">
                    <p class="radio" data-option="payByWallet"></p>
                    <label>Thanh toán bằng thẻ ATM nội địa/Internet Banking (Miễn phí thanh toán)</label>
                </div>
                <div class="option">
                    <p class="radio active" data-option="exchangeCoins"></p>
                    <label>Thanh toán bằng ví Momo</label>
                </div>
                <div class="box-btn">
                    <button class="btn-exchange-coins" onclick="showPopup()">Đổi điểm</button>
                    <button class="btn-popup btn-edit" onclick="cancel()">Hủy</button>
                </div>
            </div>
        </div>
    </div>
    <div class="popup-confirm-current">
        <div class="icon-popup">
            <img src="<?php echo asset("$path/Error.png")?>">
        </div>
        <div class="text-popup">
            <h3>Xác nhận đổi điểm</h3>
            <p>Bạn có muốn đổi điểm không? Nếu có bạn vui lòng nhập mật khẩu xác nhận.</p>
        </div>
        <div class="form-password">
            <label style="display: block">Mật Khẩu</label>
            <input class="passWord" type="password">
        </div>
        <div class="bottom-popup">
            <button class="btn-popup btn-confirm" onclick="confirm()">Xác nhận</button>
            <button class="btn-popup btn-edit" onclick="editPopup()">Huỷ</button>
        </div>
    </div>
    <div class="popup-public">
        <div class="icon-popup">
            <img src="<?php echo asset("$path/Success.png")?>">
        </div>
        <div class="text-popup">
            <h3>Đổi điểm thành công</h3>
            <p>Hệ thống thông báo bạn đã đổi điểm thành công </p>
        </div>
        <div class="bottom-popup" style="justify-content: center">
            <button class="btn-popup btn-public" onclick="closedPopup()">Xác nhận</button>
        </div>
    </div>
    <div class="screen-cover"></div>
@include('frontend.RechargeCard.popupNapTienQuaNganHang')
<!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
<script src="js/rechargeCard/index.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>
</body>
<!-- END: Body-->
</html>
