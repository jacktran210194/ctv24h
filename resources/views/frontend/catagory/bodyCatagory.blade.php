<div class="header-search flex">
    <div class="container-nav flex container">
        <div class="title-nav">
            <p class="search-relate">
                Tìm kiếm liên quan
            </p>
        </div>
        <div class="container-navigation relative">
            <div class="next-arrow button-arrow">
                <object data="icon/search/icon_next.svg" width="8px"></object>
            </div>
            <ul class="navigation flex">
                <li class="flex bg-nav-1 content-nav">Phụ kiện âm thanh</li>
                <li class="flex bg-nav-2 content-nav">Phụ kiện âm thanh</li>
                <li class="flex bg-nav-3 content-nav">Phụ kiện âm thanh</li>
                <li class="flex bg-nav-4 content-nav">Phụ kiện âm thanh</li>
                <li class="flex bg-nav-5 content-nav">Phụ kiện âm thanh</li>
                <li class="flex bg-nav-1 content-nav">Phụ kiện âm thanh</li>
            </ul>
            <div class="prev-arrow button-arrow">
                <object data="icon/search/icon_preview.svg" width="8px"></object>
            </div>
        </div>
    </div>
</div>
<div class="body-seaech flex container">
    <?php
    $path = "assets/frontend/Icon/products";
    ?>
    <div class="fillter">
        <div class="title-fillter flex">
            <div class="icon_fillter">
                <object data="icon/search/icon_fillter.svg" width="12px"></object>
            </div>
            <div class="title-text">BỘ LỌC TÌM KIẾM</div>
        </div>
        <div class="filter-catagory">
            <p class="title">Theo danh mục</p>
{{--            @if(isset($category_child))--}}
{{--                @foreach($category_child as $value)--}}
{{--                    <div class="flex content">--}}
{{--                        <input type="checkbox">--}}
{{--                        <p class="text">{{$value->name}} <span class="number">(17)</span></p>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--            @endif--}}
            {{--            <div class="flex content">--}}
            {{--                <input type="checkbox">--}}
            {{--                <p class="text">Quần short <span class="number">(17)</span></p>--}}
            {{--            </div>--}}
            {{--            <div class="flex content">--}}
            {{--                <input type="checkbox">--}}
            {{--                <p class="text">Kính mát <span class="number">(17)</span></p>--}}
            {{--            </div>--}}
            {{--            <div class="flex content">--}}
            {{--                <input type="checkbox">--}}
            {{--                <p class="text">Áo thun <span class="number">(17)</span></p>--}}
            {{--            </div>--}}
            {{--            <div class="flex content">--}}
            {{--                <input type="checkbox">--}}
            {{--                <p class="text">Kính thời trang <span class="number">(17)</span></p>--}}
            {{--            </div>--}}
            <p class="load-more">Xem Thêm ></p>
        </div>
        <div class="filter-catagory">
            <p class="title">Nơi bán</p>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Hà Nội</p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">TP.Hồ Chí Minh</p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Thái nguyên</p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Vĩnh Phúc</p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Hải Phòng </p>
            </div>
            <p class="load-more">Xem Thêm ></p>
        </div>
        <div class="filter-catagory">
            <p class="title">Đơn vị vận chuyển</p>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">NowShip</p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Ninja Van</p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">J&T Express</p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Giao hàng tiết kiệm</p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Giao hàng nhanh</p>
            </div>
            <p class="load-more">Xem Thêm ></p>
        </div>
        <div class="filter-catagory form-fillter">
            <p class="title">Khoảng giá</p>
            <div class="form-fillter-price flex">
                <input type="text" placeholder="TỪ">
                <span class="icon-space">-</span>
                <input type="text" placeholder="ĐẾN">
            </div>
            <button class="btn">Áp dụng</button>
        </div>
        <div class="filter-catagory">
            <p class="title">Loại shop</p>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Shopee Mall</p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Shop yêu thích</p>
            </div>
        </div>
        <div class="filter-catagory">
            <p class="title">Tình trạng</p>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Sản phẩm mới </p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Sản phẩm đã từng </p>
            </div>
        </div>
        <div class="filter-catagory">
            <p class="title">Đánh giá </p>
            <div class="flex content">
                <object data="icon/search/icon_star_five.svg"></object>
            </div>
            <div class="flex content">
                <object data="icon/search/icon_star_for.svg"></object>
                <p class="text">trở lên </p>
            </div>
            <div class="flex content">
                <object data="icon/search/icon_star_three.svg"></object>
                <p class="text">trở lên </p>
            </div>
            <div class="flex content">
                <object data="icon/search/icon_star_tow.svg"></object>
                <p class="text">trở lên </p>
            </div>
            <div class="flex content">
                <object data="icon/search/icon_star_one.svg"></object>
                <p class="text">trở lên </p>
            </div>
        </div>
        <div class="filter-catagory">
            <p class="title">Dịch vụ và khuyến mãi</p>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Freeship Xtra </p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Hoàn xu Xtra </p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Đang giảm giá </p>
            </div>
            <div class="flex content">
                <input type="checkbox">
                <p class="text">Miễn phí vận chuyển </p>
            </div>
            <p class="load-more">Thêm
                <object width="7px" data="icon/search/icon_arrow_down.svg"></object>
            </p>
        </div>
        <button class="btn clear-all">Xóa tất cả</button>
    </div>
    <div class="container-left">
        <div class="flex shop-love">
            <div class="box-1 flex div-box">
                <div class="image-button">
                    <div class="iamge-shop"><img src="<?php echo asset("$path/shop_love.png")?>"></div>
                    <p class="shop">Shop yêu thích</p>
                </div>
                <div class="infor-shop">
                    <h2 class="tite-shop">Quần Jean 1hit shop </h2>
                    <div class="link-button flex">
                        <p class="number-follow">
                            <span class="number">15,6k </span>
                            Người Theo Dõi
                        </p>
                        <p class="number-follow">
                            <span class="number">2,4k </span>
                            Đang Theo Dõi
                        </p>
                    </div>
                </div>
            </div>
            <div class="box-2 div-box">
                <p class="price-buy">
                    <object data="icon/products/t-shirt.svg" width="20px"></object>
                    72
                    <span> Sản Phẩm </span>
                </p>
            </div>
            <div class="box-3 div-box">
                <p class="price-buy">
                    <object data="icon/products/favorite_2.svg" width="20px"></object>
                    4.9
                    <span>  Đánh Giá </span>
                </p>
            </div>
            <div class="box-4 div-box">
                <p class="price-buy">
                    <object data="icon/products/chat_bubbles_with_ellipsis.svg" width="20px"></object>
                    98%
                    <span> Tỉ Lệ Phản Hồi </span>
                </p>
            </div>
            <div class="box-5 div-box">
                <p class="price-buy">
                    <object data="icon/products/clock.svg" width="20px"></object>
                    Trong vài giờ
                    <span> Thời Gian Phản Hồi </span>
                </p>
            </div>
        </div>
        <div class="sort-container flex">
            <div class="content-sort flex">
                <p class="title-sort">Sắp Xếp Theo</p>
                <button class="btn-sort active">Liên Quan</button>
                <button class="btn-sort ">Mới Nhất</button>
                <button class="btn-sort ">Bán Chạy</button>
                <button class="btn-sort btn-option relative">Giá
                    <object data="icon/search/btn_arrow_down.svg"></object>
                </button>
            </div>
            <div class="flex paginate">
                <div class="content-paginate">
                    <span class="in-paginate">5</span>/<span class="all-paginate">10</span>
                </div>
                <button class="btn-sort next-paginate relative">
                    <object data="icon/search/polygon_left.svg"></object>
                </button>
                <button class="btn-sort prev-paginate relative">
                    <object data="icon/search/polygon_right.svg"></object>
                </button>
            </div>
        </div>
        <div class="product-search">
            <?php
            $pathSlide = "assets/frontend/Icon/home/Danh_muc";
            ?>
            <div class="container-section-product">
{{--                @if(isset($product))--}}
{{--                    @foreach($product as $value)--}}
{{--                        <div class="content border-box">--}}
{{--                            <div class="img-product">--}}
{{--                                <img src="<?php echo asset("$pathSlide/jean-trend.png")?>">--}}
{{--                                <img width="185px" height="190px" src="{{$value->image}}" alt="">--}}
{{--                            </div>--}}
{{--                            <div class="title-product-n relative">--}}
{{--                                <span>{{$value->name_product}}</span>--}}
{{--                            </div>--}}
{{--                            <div class="product-price">--}}
{{--                                <p class="price">{{number_format($value->price_discount)}}đ - {{number_format($value->price)}}đ</p>--}}
{{--                                <p class="pass">Giá CTV: ******** <span>Lời: 50.000</span></p>--}}
{{--                                <div class="form btn-sign-up relative">--}}
{{--                                    <a href="#">Đăng ký CTV để xem giá</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="review">--}}
{{--                                <img src="<?php echo asset("$pathSlide/icon-review.png")?>">--}}
{{--                                <p>Đã bán {{$value->is_sell}}</p>--}}
{{--                            </div>--}}
{{--                            <div class="review">--}}
{{--                                <img style="width: 20px;" src="<?php echo asset("$pathSlide/icon-like.png")?>">--}}
{{--                                <p>{{$value->warehouse}}</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @endforeach--}}
{{--                @endif--}}
            </div>
        </div>
    </div>
</div>
