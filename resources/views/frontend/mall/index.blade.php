<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/mall/mall.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <!-- BEGIN: Header-->
    <!-- END: Header-->
    <!-- BEGIN: Homepage-->
    <div id="img">
        <div class="container">
            <div class="row">
                <img class="img img-fluid"
                     src="Icon/mall/banner.png"
                     alt="">
                <div class="d-flex justify-content-center justify-content-around"
                     style="margin: auto; height: 108px;background: white; width: 100%;text-align: center">
                    <div class="return_free d-flex align-items-center">
                        <object data="Icon/mall/shop1.svg" type=""></object>
                        <div class="text-content">
                            <h4 class="text-title-top">7 NGÀY MIỄN PHÍ TRẢ HÀNG</h4>
                            <span class="text-content-top">Trả hàng miễn phí trong 7 ngày</span>
                        </div>
                    </div>
                    <div class="genuine d-flex align-items-center">
                        <object data="Icon/mall/shop2.svg" type=""></object>
                        <div class="text-content">
                            <h4 class="text-title-top">HÀNG CHÍNH HÃNG 100%</h4>
                            <span class="text-content-top">Đảm bảo hàng chính hãng hoặc hoàn tiền gấp đôi</span>
                        </div>
                    </div>
                    <div class="free_ship d-flex align-items-center">
                        <object data="Icon/mall/shop3.svg" type=""></object>
                        <div class="text-content">
                            <h4 class="text-title-top">MIỄN PHÍ VẬN CHUYỂN</h4>
                            <span class="text-content-top">Giao hàng miễn phí toàn quốc</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div id="category" class="pb-5">
        <div class="container menu-category bg-white">
            <div class="row tab-category">
                <p class="text-menu-tab">THƯƠNG HIỆU ƯA CHUỘNG</p>
            </div>
            <div class="slick-voucher p-5 m-0" style="padding: 43px 61px;">
                <object data="Icon/mall/brand.svg" class="mr-5" height="128"></object>
                <object data="Icon/mall/brand.svg" class="mr-5" height="128"></object>
                <object data="Icon/mall/brand2.svg" class="mr-5" height="128"></object>
                <object data="Icon/mall/brand3.svg" class="mr-5" height="128"></object>
                <object data="Icon/mall/brand2.svg" class="mr-5" height="128"></object>
                <object data="Icon/mall/brand3.svg" class="mr-5" height="128"></object>
                <object data="Icon/mall/brand2.svg" class="mr-5" height="128"></object>
                <object data="Icon/mall/brand3.svg" class="mr-5" height="128"></object>
                <object data="Icon/mall/brand2.svg" class="mr-5" height="128"></object>
                <object data="Icon/mall/brand3.svg" class="mr-5" height="128"></object>
                <object data="Icon/mall/brand2.svg" class="mr-5" height="128"></object>
                <object data="Icon/mall/brand3.svg" class="mr-5" height="128"></object>
            </div>
        </div>
        <div class="container bg-white p-0">
            <div style="width: 50%;" class="tab-category mt-5">
                <p class="text-menu-tab">THƯƠNG HIỆU NỔI BẬT TRONG NGÀY</p>
            </div>
            <div class="d-flex">
                <div class="col-lg-4 p-1 pr-5">
                    <img src="Icon/mall/sidebar1.png" width="100%" height="100%" alt="">
                </div>
                <div class="col-lg-8 p-1 d-flex">
                    <div class="col-lg-3 p-0 pr-2">
                        <div class="card-item">
                            <img src="Icon/deals/product.png" width="100%">
                            <div class="p-3 bg-white d-flex flex-column">
                                <div class="text-bold-600 number-line-2">
                                    Quần jean ống rộng Kèm Hình Thật...
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                                <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                        Giá CTV: ****
                                    </div>
                                    <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                        Lời: 50.000
                                    </div>
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex col-lg-6 p-0">
                                        <img src="Icon/lovely/star.png" width="100%">
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0">
                                        Đã bán 10
                                    </div>
                                </div>
                                <div class="d-flex mt-2 align-items-center">
                                    <div class="d-flex col-lg-6 p-0 align-items-center">
                                        <object data="Icon/deals/like.svg"></object>
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                        Hà Nội
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 p-0 pr-2">
                        <div class="card-item">
                            <img src="Icon/deals/product.png" style="width: 100%;">
                            <div class="p-3 bg-white d-flex flex-column">
                                <div class="text-bold-600 number-line-2">
                                    Quần jean ống rộng Kèm Hình Thật...
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                                <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                        Giá CTV: ****
                                    </div>
                                    <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                        Lời: 50.000
                                    </div>
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex col-lg-6 p-0">
                                        <img src="Icon/lovely/star.png" width="100%">
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0">
                                        Đã bán 10
                                    </div>
                                </div>
                                <div class="d-flex mt-2 align-items-center">
                                    <div class="d-flex col-lg-6 p-0 align-items-center">
                                        <object data="Icon/deals/like.svg"></object>
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                        Hà Nội
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 p-0 pr-2">
                        <div class="card-item">
                            <img src="Icon/deals/product.png" width="100%">
                            <div class="p-3 bg-white d-flex flex-column">
                                <div class="text-bold-600 number-line-2">
                                    Quần jean ống rộng Kèm Hình Thật...
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                                <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                        Giá CTV: ****
                                    </div>
                                    <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                        Lời: 50.000
                                    </div>
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex col-lg-6 p-0">
                                        <img src="Icon/lovely/star.png" width="100%">
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0">
                                        Đã bán 10
                                    </div>
                                </div>
                                <div class="d-flex mt-2 align-items-center">
                                    <div class="d-flex col-lg-6 p-0 align-items-center">
                                        <object data="Icon/deals/like.svg"></object>
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                        Hà Nội
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 p-0 pr-2">
                        <div class="card-item">
                            <img src="Icon/deals/product.png" width="100%">
                            <div class="p-3 bg-white d-flex flex-column">
                                <div class="text-bold-600 number-line-2">
                                    Quần jean ống rộng Kèm Hình Thật...
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                                <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                        Giá CTV: ****
                                    </div>
                                    <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                        Lời: 50.000
                                    </div>
                                </div>
                                <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                                <div class="d-flex mt-2">
                                    <div class="d-flex col-lg-6 p-0">
                                        <img src="Icon/lovely/star.png" width="100%">
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0">
                                        Đã bán 10
                                    </div>
                                </div>
                                <div class="d-flex mt-2 align-items-center">
                                    <div class="d-flex col-lg-6 p-0 align-items-center">
                                        <object data="Icon/deals/like.svg"></object>
                                    </div>
                                    <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                        Hà Nội
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div style="" class="row tab-category mt-5">
                <p class="text-menu-tab">GỢI Ý CHO BẠN</p>
            </div>
        </div>
        <div class="container bg-white d-flex mb-3 align-content-center align-items-center" style="height: 72px">
            <h4 style="line-height: 72px;">Sắp xếp theo</h4>
            <div class="d-flex" style="padding-left: 26px">
                <button class="btn btn-main menu-menu">Liên Quan</button>
                <button class="btn btn-default menu-menu">Mới Nhất</button>
                <button class="btn btn-default menu-menu">Bán Chạy</button>
                <select style="width: 25%" class="form-control menu-menu" name="" id="">
                    <option value="">Giá</option>
                    <option value="">Tăng dần</option>
                    <option value="">Giảm dần</option>
                </select>
            </div>
        </div>
        <div class="container p-0">
            <div id="product" class="d-flex justify-content-center justify-content-center justify-content-between">
                <?php for($i = 1;$i <= 6;$i++):?>
                <div class="card mr-1">
                    <img width="100%" class="card-img-top img-fluid"
                         src="Icon/mall/sp_1.png"
                         alt="">
                    <img style="position: absolute; right: 0%" class="img-mall" width="34px" height="39px"
                         src="Icon/mall/mall.png" alt="">
                    <div class="card-body">
                        <p class="card-title number-line-2">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh
                            Viên</p>
                        <p class="card-price">190.000 - 370.000</p>
                        <p class="card-price-ctv">Giá CTV: ********</p>
                        <p class="card-text">Đăng ký CTV để xem giá</p>
                        <div class="d-flex align-items-center align-content-center">
                            <div class="">
                                <object data="Icon/suggestion/start.png" type=""></object>
                                <object data="Icon/suggestion/like.png" type=""></object>
                            </div>
                            <div class="pl-2">
                                <span>Đã bán 10</span>
                                <span>Hà Nội</span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endfor; ?>
            </div>
            <div id="product" class="pt-2 d-flex justify-content-center justify-content-center justify-content-between">
                <?php for($i = 1;$i <= 6;$i++):?>
                <div class="card mr-1">
                    <img width="185px" height="183px" class="card-img-top img-fluid"
                         src="Icon/mall/sp_1.png"
                         alt="">
                    <img style="position: absolute; right: 0%" class="img-mall" width="34px" height="39px"
                         src="Icon/mall/mall.png" alt="">
                    <div class="card-body">
                        <p class="card-title number-line-2">Quần Jean Ống Rộng Kèm Hình Thật Rất Đẹp Giá Rẻ Sinh
                            Viên</p>
                        <p class="card-price">190.000 - 370.000</p>
                        <p class="card-price-ctv">Giá CTV: ********</p>
                        <p class="card-text">Đăng ký CTV để xem giá</p>
                        <div class="d-flex align-items-center align-content-center">
                            <div class="">
                                <object data="Icon/suggestion/start.png" type=""></object>
                                <object data="Icon/suggestion/like.png" type=""></object>
                            </div>
                            <div class="pl-2">
                                <span>Đã bán 10</span>
                                <span>Hà Nội</span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
    <!-- END: Homepage-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
</div>

@include('frontend.base.script')
<script src="js/mall/index.js"></script>
</body>
<!-- END: Body-->
</html>
