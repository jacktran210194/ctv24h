<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/sell_with_ctv24/style.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="container mb-5">
        <img src="Icon/sell_with_ctv24/banner.png">
        <div class="d-flex justify-content-center m-5">
            <object data="Icon/sell_with_ctv24/content.svg"></object>
        </div>
        <div class="d-flex justify-content-around m-5 view-content">
            <div class="d-flex position-relative">
                <div class="rotate-view pl-5 pr-5 pt-3 pb-3 bg-blue position-absolute">
                    <span>Nhà bán hàng</span>
                </div>
                <div>
                    <div class="header-content d-flex justify-content-center">
                        <span>TRONG NƯỚC</span>
                    </div>
                    <div class="main-content p-3">
                        <div class="d-flex mt-5">
                            <object data="Icon/sell_with_ctv24/icon_tick.svg" width="15"></object>
                            <span class="pl-2">Bạn là Cá nhân / Hộ kinh doanh / Công ty ở Việt Nam </span>
                        </div>
                        <div class="d-flex mt-5">
                            <object data="Icon/sell_with_ctv24/icon_tick.svg" width="15"></object>
                            <span class="pl-2">Không cần giấy phép kinh doanh </span>
                        </div>
                        <div class="d-flex mt-5">
                            <object data="Icon/sell_with_ctv24/icon_tick.svg" width="15"></object>
                            <span class="pl-2">Không thu phí người lớn </span>
                        </div>
                        <div class="m-5 d-flex justify-content-center">
                            <a href="<?= URL::to('register/ctv') ?>" class="btn btn-main preventdefault">Đăng Ký Ngay</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-flex position-relative">
                <div class="rotate-view pl-5 pr-5 pt-3 pb-3 bg-green position-absolute">
                    <span>Nhà bán hàng</span>
                </div>
                <div>
                    <div class="header-content d-flex justify-content-center">
                        <span>NGOÀI NƯỚC</span>
                    </div>
                    <div class="main-content p-3">
                        <div class="d-flex mt-5">
                            <object data="Icon/sell_with_ctv24/icon_tick.svg" width="15"></object>
                            <span class="pl-2">Bạn là doanh nghiệp đang hoạt động tại Trung Quốc </span>
                        </div>
                        <div class="d-flex mt-5">
                            <object data="Icon/sell_with_ctv24/icon_tick.svg" width="15"></object>
                            <span class="pl-2">Giao hàng dễ dàng với trung tâm phân loại tại nước ngoài của chúng tôi  </span>
                        </div>
                        <div class="d-flex mt-5">
                            <object data="Icon/sell_with_ctv24/icon_tick.svg" width="15"></object>
                            <span class="pl-2">Chỉ thanh toán phí hoa hồng khi bạn bán được hàng  </span>
                        </div>
                        <div class="m-5 d-flex justify-content-center">
                            <a href="<?= URL::to('register/ncc') ?>" class="btn btn-main preventdefault">Đăng Ký Ngay</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-around">
            <object data="Icon/sell_with_ctv24/one.svg"></object>
            <object data="Icon/sell_with_ctv24/two.svg"></object>
            <object data="Icon/sell_with_ctv24/three.svg"></object>
            <object data="Icon/sell_with_ctv24/four.svg"></object>
            <object data="Icon/sell_with_ctv24/five.svg"></object>
        </div>
        <div class="d-flex justify-content-center m-5">
            <img src="Icon/sell_with_ctv24/info.png">
        </div>
        <div class="d-flex justify-content-center footer-sell">
            <div class="col-lg-6 d-flex align-items-center pl-5">
                <div class="text-white text-bold-800 text-register">
                    Đăng ký ngay >>
                </div>
            </div>
            <div class="col-lg-6 d-flex justify-content-end pr-5">
                <img src="Icon/sell_with_ctv24/sale.png" class="img-sale">
            </div>
        </div>
    </div>
    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
