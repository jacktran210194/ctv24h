@if(isset($address_ctv) && count($address_ctv))
    @foreach($address_ctv as $value)
        <div class="d-flex align-items-center address">
            <label class="checkbox-address m-0">
                <input type="radio" name="address_ctv" value="{{$value->id}}">
                <span class="checkmark"></span>
            </label>
            <div class="p-2 ">
                <p class="font-weight-bold font-size-17" style="line-height: 24px">
                    {{$value->name}} - {{$value->phone}}
                </p>
                <p class="m-0 color-gray">{{$value->address}} - {{$value->ward_name}} -  {{$value->district_name}}  -  {{$value->city_name}}</p>
            </div>
        </div>
    @endforeach
@else
    <p class="text-center">Không tìm thấy số điện thoại hoặc tên khách hàng phù hợp</p>
@endif
