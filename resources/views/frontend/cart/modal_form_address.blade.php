<?php
use Illuminate\Support\Facades\URL;
?>
<div class="position-absolute position-center">
    <div class="p-5 bg-white" style="border-radius: 18px;">
        <h3 style="color: #005DB6">Nhập thông tin người nhận</h3>
        <div class="mt-4" style="width: 25vw">
            <div class="form-group">
                <input class="form-control" id="phone" placeholder="Số điện thoại"
                       value="">
            </div>
            <div class="form-group">
                <input class="form-control" id="name" placeholder="Họ và tên"
                       value="">
            </div>
            <div class="form-group">
                <select class="form-control form-address" data-value="city" id="citySelect"
                        data-url="<?= URL::to('cart/filter_address_customer?type=city&&id=') ?>">
                    <option value="">Tỉnh/Thành Phố</option>
                    <?php foreach ($city as $value): ?>
                    <option
                        <?= isset($data) && $data->city_id === $value['id'] ? 'selected' : '' ?> value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control form-address" data-value="district" id="districtSelect"
                        data-url="<?= URL::to('cart/filter_address_customer?type=district&&id=') ?>">
                    <option value="">Quận/Huyện</option>
                    <?php if (isset($district)): ?>
                    <?php foreach ($district as $value): ?>
                    <option
                        <?= isset($data) && $data->district_id === $value['id'] ? 'selected' : '' ?> value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" id="wardSelect">
                    <option value="">Phường/Xã</option>
                    <?php if (isset($ward)): ?>
                    <?php foreach ($ward as $val): ?>
                    <option
                        <?= isset($data) && $data->ward_id === $val['ward_id'] ? 'selected' : '' ?> value="<?= $val['ward_id'] ?>"><?= $val['name'] ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
            <div class="form-group">
                <input class="form-control" id="address" placeholder="Toà nhà, tên đường..."
                       value="">
            </div>
            <div class="form-group d-flex justify-content-end">
                <div type="button" class="btn icon-back-modal btn-back-modal-address">Trở lại</div>
                <div type="button" class="btn btn-main ml-2 btn-add-maps"
                     data-id="<?= isset($data) ? $data->id : '' ?>"
                     data-url="<?= URL::to('cart/save_info_customer_address') ?>">
                    Hoàn thành
                </div>
            </div>
        </div>
    </div>
</div>
