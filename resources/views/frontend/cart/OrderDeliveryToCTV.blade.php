<?php

use Illuminate\Support\Facades\URL;
$data_login = Session::get('data_customer');
$data_check_coin = Session::get('data_check_coin');

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/cart/style.css">
<link rel="stylesheet" type="text/css" href="css/home/style.css">
<!-- END: Head-->
<style>
    .hide_form {
        display: none !important;
    }

    .show_form {
        /*display: inline-block!important;*/
    }
</style>
<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    @if(count($dataShop))
        <div class="container">
            <div class="nav-cart-page">
                <a style="color: #fff;" class="nav-link active"
                >Giao cho CTV</a>
                <a class="nav-link"
                >Giao cho khách của CTV</a>
            </div>
            <div class="container-location">
                <div class="location d-flex justify-content-between align-items-center">
                    <div class="d-flex align-items-center">
                        <img src="Icon/cart/location.png" style="width: 26px;">
                        <h3 class="title-address">Địa Chỉ Nhận Hàng </h3>
                    </div>
                    <div>
                        <button class="btn-edit-location" data-url="{{url("address-customer")}}">
                            <img src="Icon/cart/edit.png">
                        </button>
                    </div>
                </div>
                <div id="form-location-customer">
                    <?php foreach ($address as $value): ?>
                    <?php if ($value ['default'] === 1): ?>
                    <div class="infor-location-customer">
                        <h3 class="name-customer"><?= $value['name'] . ' ' . $value['phone'] ?></h3>
                        <p class="address-customer"><?= $value['full_address'] ?></p>
                    </div>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="d-flex align-items-center header-cart mt-3 p-3">
                <div class="col-lg-6 align-items-center">
                    <span class="ml-3">Sản Phẩm</span>
                </div>
                <div class="col-lg-6 align-items-center d-flex">
                    <div class="col-lg-2 align-items-center">
                        <center>Đơn giá</center>
                    </div>
                    <div class="col-lg-4 align-items-center">
                        <center>Số lượng</center>
                    </div>
                    <div class="col-lg-3 align-items-center">
                        <center>Số tiền</center>
                    </div>
                    <div class="col-lg-3 align-items-center">
                        <center>Thao tác</center>
                    </div>
                </div>
            </div>

            <!-- BEGIN: Option 1-->
            <?php
            $countProduct = 0;
            $total = 0;
            $total_shop = 0;
            $total_all = 0;
            ?>
            <?php if(isset($dataOrderShop)): ?>
            <?php foreach ($dataOrderShop as $value): ?>
            <div class="container-item-cart">
                <div class="d-flex align-items-center header-cart mt-3 p-3">
                    <div class="col-lg-12 align-items-center d-flex item-cart-shop">
                        {{--                <input type="checkbox" class="checked-shop">--}}
                        <object data="Icon/cart/shop_name.svg" class="ml-3" width="20" height="20"></object>
                        <a href="{{url('seeshop/'.$value->shop_id)}}"><b class="ml-3">{{$value->name_shop}}</b></a>
                        <object data="Icon/cart/love.svg" class="ml-3" width="70" height="auto"></object>
                        <div class="align-items-center more-cart p-2 ml-3">
                            <span class="text-bold-700 color-text-danger">Mua kèm deal sốc </span>
                            <span class="text-bold-700 ml-5">Xem Thêm</span>
                            <span class="text-bold-700 ml-2"></span>
                        </div>
                    </div>
                </div>

                <?php foreach($value->prd as $item) : ?>
                <div class="d-flex align-items-center header-cart mt-3 p-3 relative item-cart">
                    {{--            <a href="{{url('cart/delete_product/'.$item->id)}}"--}}
                    {{--               data-url="{{url('cart/delete_product/'.$item->id)}}"--}}
                    {{--            >--}}
                    {{--                <span class="icon-close"><i class="fa fa-times" aria-hidden="true"></i></span>--}}
                    {{--            </a>--}}
                    <div class="col-lg-6 align-items-center d-flex">
                        {{--                <input type="checkbox" name="cart[]" class="check-product"--}}
                        {{--                       data-id="{{$item->id}}"--}}
                        {{--                >--}}
                        <img style="height: 100px;" src="{{$item->image}}" class="ml-3 img-product">
                        <b class="ml-3">{{$item->name}} </b>
                        <div class="ml-5 justify-content-center">
                            <p class="color-type"><?= __('Phân loại :')?></p>
                            <p class="color-type"> {{$item->cate_2}} - {{$item->size_of_value}} </p>
                        </div>
                    </div>
                    <div class="col-lg-6 align-items-center d-flex">
                        <div class="col-lg-2 align-items-center">
                            <center>
                                <b class="price-new">{{number_format($item->price_attr)}}đ</b>
                                <p class="price-old pt-2 m-0">{{number_format($item->price_ctv)}}đ</p>
                            </center>
                        </div>
                        <div class="col-lg-4 align-items-center">
                            <center>
                                <div class="d-flex view-add-cart justify-content-center">
                                    <input class="input-order" value="{{$item->quantity}}" type="number"
                                           data-url="{{url('cart/update/'.$item->id)}}"
                                           data-id="{{$item->id}}"
                                           style="border: none;" readonly
                                    >
                                </div>
                            </center>
                        </div>
                        <div class="col-lg-3 align-items-center">
                            <center>
                                <span class="total-price">{{number_format($item->price_attr * $item->quantity)}}đ</span>
                            </center>
                        </div>
                        <?php
                        $total = $item->price_attr * $item->quantity;
                        $countProduct += 1;
                        $total_shop += $total;
                        ?>
                        <div class="col-lg-3 align-items-center">
                            <div class="dropdown relative">
                                <b class="dropdown-toggle text-right show-content preventdefault" type="button"
                                   id="dropdownMenuButton">
                                    Tìm sản phẩm tương tự
                                </b>
                            </div>
                        </div>
                    </div>
                    <div class="popup-cart">
                        <?php
                        $path = "assets/frontend/Icon/home/Danh_muc";
                        ?>
                        <div class="container-section-product slide-popup">
                            <div class="content border-box">
                                <div class="img-product remove-mg">
                                    <img src="<?php echo asset("$path/jean-trend.png")?>">
                                </div>
                                <div class="title-product-n relative">
                                    <span>Quần jean ống rộng Kèm Hình Thật...</span>
                                </div>
                                <div class="product-price">
                                    <p class="price">270.000 - 370.000</p>
                                    <p class="pass">Giá CTV: ********</p>
                                    <div class="form btn-sign-up relative">
                                        <a href="#">Đăng ký CTV để xem giá</a>
                                    </div>
                                </div>
                                <div class="review">
                                    <img src="<?php echo asset("$path/icon-review.png")?>">
                                    <p>Đã bán 10</p>
                                </div>
                                <div class="review">
                                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                                    <p>Hà Nội</p>
                                </div>
                            </div>
                            <div class="content border-box">
                                <div class="img-product remove-mg">
                                    <img src="<?php echo asset("$path/trend_ao_gio.png")?>">
                                </div>
                                <div class="title-product-n relative">
                                    <span>Quần jean ống rộng Kèm Hình Thật...</span>
                                    <div class="sale-title">
                                        <a href="#" class="sale">
                                            Mua kèm deal sốc
                                        </a>
                                    </div>
                                </div>
                                <div class="product-price">
                                    <p class="price">270.000 - 370.000</p>
                                    <p class="pass">Giá CTV: ********</p>
                                    <div class="form btn-sign-up relative">
                                        <a href="#">Đăng ký CTV để xem giá</a>
                                    </div>
                                </div>
                                <div class="review">
                                    <img src="<?php echo asset("$path/icon-review.png")?>">
                                    <p>Đã bán 10</p>
                                </div>
                                <div class="review">
                                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                                    <p>Hà Nội</p>
                                </div>
                            </div>
                            <div class="content border-box">
                                <div class="img-product remove-mg">
                                    <img src="<?php echo asset("$path/trend_giay.png")?>">
                                </div>
                                <div class="title-product-n relative">
                                    <span>Quần jean ống rộng Kèm Hình Thật...</span>
                                    <div class="sale-title">
                                        <a href="#" class="sale">
                                            Mua 2 Giảm 5%
                                        </a>
                                    </div>
                                </div>
                                <div class="product-price">
                                    <p class="price">270.000 - 370.000</p>
                                    <p class="pass">Giá CTV: ********</p>
                                    <div class="form btn-sign-up relative">
                                        <a href="#">Đăng ký CTV để xem giá</a>
                                    </div>
                                </div>
                                <div class="review">
                                    <img src="<?php echo asset("$path/icon-review.png")?>">
                                    <p>Đã bán 10</p>
                                </div>
                                <div class="review">
                                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                                    <p>Hà Nội</p>
                                </div>
                            </div>
                            <div class="content border-box">
                                <div class="img-product remove-mg">
                                    <img src="<?php echo asset("$path/jean-trend.png")?>">
                                </div>
                                <div class="title-product-n relative">
                                    <span>Quần jean ống rộng Kèm Hình Thật...</span>
                                    <div class="sale-title">
                                        <a href="#" class="sale">
                                            Mua kèm deal sốc
                                        </a>
                                    </div>
                                </div>
                                <div class="product-price">
                                    <p class="price">270.000 - 370.000</p>
                                    <p class="pass">Giá CTV: ********</p>
                                    <div class="form btn-sign-up relative">
                                        <a href="#">Đăng ký CTV để xem giá</a>
                                    </div>
                                </div>
                                <div class="review">
                                    <img src="<?php echo asset("$path/icon-review.png")?>">
                                    <p>Đã bán 10</p>
                                </div>
                                <div class="review">
                                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                                    <p>Hà Nội</p>
                                </div>
                            </div>
                            <div class="content border-box">
                                <div class="img-product remove-mg">
                                    <img src="<?php echo asset("$path/trend_tui_sach.png")?>">
                                </div>
                                <div class="title-product-n relative">
                                    <span>Quần jean ống rộng Kèm Hình Thật...</span>
                                    <div class="sale-title">
                                        <a href="#" class="sale">
                                            Mua kèm deal sốc
                                        </a>
                                    </div>
                                </div>
                                <div class="product-price">
                                    <p class="price">270.000 - 370.000</p>
                                    <p class="pass">Giá CTV: ********</p>
                                    <div class="form btn-sign-up relative">
                                        <a href="#">Đăng ký CTV để xem giá</a>
                                    </div>
                                </div>
                                <div class="review">
                                    <img src="<?php echo asset("$path/icon-review.png")?>">
                                    <p>Đã bán 10</p>
                                </div>
                                <div class="review">
                                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                                    <p>Hà Nội</p>
                                </div>
                            </div>
                            <div class="content border-box">
                                <div class="img-product remove-mg">
                                    <img src="<?php echo asset("$path/trend_ao_phong.png")?>">
                                </div>
                                <div class="title-product-n relative">
                                    <span>Quần jean ống rộng Kèm Hình Thật...</span>
                                </div>
                                <div class="product-price">
                                    <p class="price">270.000 - 370.000</p>
                                    <p class="pass">Giá CTV: ********</p>
                                    <div class="form btn-sign-up relative">
                                        <a href="#">Đăng ký CTV để xem giá</a>
                                    </div>
                                </div>
                                <div class="review">
                                    <img src="<?php echo asset("$path/icon-review.png")?>">
                                    <p>Đã bán 10</p>
                                </div>
                                <div class="review">
                                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                                    <p>Hà Nội</p>
                                </div>
                            </div>
                            <div class="content border-box">
                                <div class="img-product remove-mg">
                                    <img src="<?php echo asset("$path/trend_ao_phong.png")?>">
                                </div>
                                <div class="title-product-n relative">
                                    <span>Quần jean ống rộng Kèm Hình Thật...</span>
                                </div>
                                <div class="product-price">
                                    <p class="price">270.000 - 370.000</p>
                                    <p class="pass">Giá CTV: ********</p>
                                    <div class="form btn-sign-up relative">
                                        <a href="#">Đăng ký CTV để xem giá</a>
                                    </div>
                                </div>
                                <div class="review">
                                    <img src="<?php echo asset("$path/icon-review.png")?>">
                                    <p>Đã bán 10</p>
                                </div>
                                <div class="review">
                                    <img style="width: 20px;" src="<?php echo asset("$path/icon-like.png")?>">
                                    <p>Hà Nội</p>
                                </div>
                            </div>
                        </div>
                        <div class="paginate">
                            <div class="previous border-paginate">
                                <object data="Icon/cart/previous.svg"></object>
                            </div>
                            <div class="append-dots">
                                {{--                        <div class="number-paginate border-paginate is-paginate">2</div>--}}
                                {{--                        <div class="number-paginate border-paginate">3</div>--}}
                            </div>
                            <div class="next border-paginate">
                                <object data="Icon/cart/next.svg"></object>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endforeach; ?>
            <?php
            $total_all += $total_shop;
            ?>
            <?php endif; ?>
        <!-- End: Option 2-->

            <!-- Begin: voucher-->
            <div class="d-flex align-items-center header-cart mt-3 p-3">
                <div class="col-lg-6 align-items-center">
                    <label class="note">Lời nhắn </label>
                    <input class="note-order" type="text" name="note" id="note" placeholder="Lưu ý cho Người bán...">
                </div>
                <div class="col-lg-6 align-items-center d-flex">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-2"></div>
                    <div class="col-lg-3" style="text-align: center">
                        <span>Voucher shop</span>
                    </div>
                    <div class="col-lg-6">
                        <center class="voucher">
                            <span>Chọn Hoặc Nhập Mã Voucher</span>
                            <span class="ml-3"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                        </center>
                    </div>
                </div>
            </div>
            <!-- End: voucher-->

            <!-- Begin: shipping-->
            <div class="d-flex justify-content-end header-cart mt-1 p-3">
                <div class="d-flex shipping">
                    <p class="title-shipping-unit">
                        Đơn vị vận chuyển:
                    </p>
                    <div class="infor-shipping-unit">
                        <p class="name-infor-shipping name_shipping_show">Viettel Post</p>
                        <p class="sub-name-infor-shipping"></p>
                    </div>
                    <p class="edit-shipping-unit" style="cursor: pointer">Thay đổi đơn vị vận chuyển</p>
                </div>
                <div class="d-flex align-items-center pr-5">
                    <span class="ml-5 text-xu" style="font-weight: bold; color: #000"> 10.000đ</span>
                </div>
            </div>
            <!-- End: shipping-->

            <!-- Begin: all product-->
            <div class="d-flex align-items-center header-cart p-3">
                <div class="col-lg-6 align-items-center">
                </div>
                <div class="col-lg-6 align-items-center d-flex">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8" style="display: flex; justify-content: flex-end;padding-right: 25px;">
                        <span class="total-product">Tổng số tiền ({{$countProduct}} sản phẩm):</span>
                        <span class="total-price" style="font-size: 24px;">{{number_format($total_all)}}đ</span>
                    </div>
                </div>
            </div>
            <!-- End: all product-->

            <div class="form-vuocher" style="margin-top: 20px">
                <div class="d-flex">
                    <div class="d-flex d-flex-left">
                        <img src="Icon/cart/Group_1.png">
                        <p>Voucher sàn</p>
                    </div>
                    <div class="d-flex d-flex-right">
                        <center class="voucher">
                            <span>Chọn Hoặc Nhập Mã Voucher</span>
                            <span class="ml-3"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                        </center>
                    </div>
                </div>
            </div>

            <div class="form-vuocher">
                <div class="d-flex">
                    <div class="d-flex d-flex-left">
                        <object data="Icon/cart/so_xu.svg" style="width: 30px"></object>
                        <p>Xu</p>
                        <span>Dùng {{$data_login['coin'] ?? 0}} xu </span>
                    </div>
                    <div class="d-flex d-flex-right">
                        <input data-coin="{{$data_login['coin'] ?? 0}}" class="check_coin_view_ctv" @if($data_check_coin == 1) checked @endif type="checkbox">
                        <label>- {{$data_login['coin'] ?? 0}} xu</label>
                    </div>
                </div>
            </div>
            <input type="hidden" class="type_check_coin" value="{{$data_check_coin ?? 0}}">
            <!-- Begin: checkout-->
            <div class="form-checkout">
                <div class="nav-checkout d-flex">
                    <p class="nav-title">Phương thức thanh toán </p>
                    <div class="nav-right">
                        <p class="nav-title-right name-payment-show">Thanh toán khi nhận hàng </p>
                        <p class="edit-title-right" style="cursor: pointer">Thay đổi phương thức thanh toán</p>
                    </div>
                </div>
                <div class="d-flex" style="justify-content: flex-end">
                    <div class="container-d-flex">
                        <div class="d-flex" style="justify-content: space-between; margin-bottom: 20px">
                            <span class="title-left">Cộng xu</span>
                            <span class="title-right">+{{number_format($total_all * 0.1/100)}} xu</span>
                        </div>
                        <div class="d-flex" style="justify-content: space-between; margin-bottom: 20px">
                            <span class="title-left">Tổng tiền hàng:</span>
                            <span class="title-price">{{number_format($total_all)}}đ</span>
                        </div>
                        <div class="d-flex" style="justify-content: space-between; margin-bottom: 20px">
                            <span class="title-left">Tổng phí vận chuyển:</span>
                            <span class="title-price"> 10.000đ</span>
                        </div>
                        <div class="d-flex form_coin justify-content-between" style="margin-bottom: 20px;">
                            <span class="title-left">Dùng xu</span>
                            <span class="title-right">-{{number_format($data_login['coin'])}} đ</span>
                        </div>
                        {{--                <div class="btn-add-infor-cunstomer">--}}
                        {{--                    <button>Nhập thông tin người nhận</button>--}}
                        {{--                </div>--}}
                    </div>
                </div>
            </div>

            <?php
            $total_payment = $total_all + 10000;
            ?>
            <div class="d-flex total-payment">
                <div class="form-payment d-flex">
                    <div class="price-payment">
                        <span> Tổng thanh toán:</span>
                        <span data-total-old="{{$total_payment}}" data-total="{{$total_payment}}"
                              class="total-price total_price"> {{number_format($data_order->total_price + 10000)}}đ</span>
                    </div>
                    <div class="btn-payment">
                        <button class="checkout-cart"
                                data-url="{{url('cart/checkout')}}"
                                data-customer="{{$value->customer_id}}"
                                data-order-id="{{$value->order_id}}"
                                data-total-payment="{{$data_order->total_price + 10000}}"
                                data-wallet="<?= isset($customer) ? $customer->wallet : '' ?>"
                        >Mua hàng
                        </button>
                    </div>
                </div>
            </div>
            <!-- End: checkout-->
        </div>
    @else
        <h3 class="text-center">Giỏ hàng trống</h3>
    @endif
    <div class="w-100 modal-fixed" id="modalAddress"></div>
    <div class="popup-edit-shipping">
        <div class="container-popup">
            <div class="header-popups">
                <p class="title-header">ĐƠN VỊ VẬN CHUYỂN</p>
            </div>
            <div class="information-shipping-unit">
                @if(isset($data_shipping))
                    @foreach($data_shipping as $valueShip)
                        <div class="d-flex justify-content-between sub-shipping-unit">
                            <div class="container-shipping">
                                <div class="name-shipping-unit d-flex align-items-center">
                                    <div
                                        class="selected name_shipping <?= isset($valueShip) && ($valueShip->default == 1) ? 'checked' : '' ?>"
                                        data-id="{{$valueShip->shipping_id}}"
                                        data-name="{{$valueShip->name_shipping}}"
                                    ></div>
                                    <p class="name">{{$valueShip->name_shipping}}</p>
                                </div>
                                <div class="dropdown-shipping">
                                    <div class="d-flex content-dropdown">
                                        <div style="cursor: pointer" class="ratio-check-box"
                                             data-type-ship="0"
                                        ></div>
                                        <div class="sub-content-shipping">
                                            <p class="title-dropdown">Tất cả các ngày trong tuần </p>
                                            <p class="label">Phù hợp với địa chỉ nhà riêng, luôn có người nhận hàng </p>
                                        </div>
                                    </div>
                                    <div class="d-flex content-dropdown">
                                        <div style="cursor: pointer" class="ratio-check-box"
                                             data-type-ship="1"
                                        ></div>
                                        <div class="sub-content-shipping">
                                            <p class="title-dropdown">Chỉ giao hàng giờ hành chính </p>
                                            <p class="label">Phù hợp với địa chỉ văn phòng cơ quan</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-lg-center align-items-center border-box">
                                <img width="90px" height="23px" src="{{$valueShip->image_shipping}}">
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="d-flex justify-content-lg-end align-items-center">
                <button class="btn btn-cancel">Trở lại</button>
                <button class="btn btn-confirm-shipping"
                >Hoàn thành
                </button>
            </div>
        </div>
    </div>
    <div class="popup-edit-payment">
        <div class="container-popup-payment">
            <div class="header-popups-payment">
                <p class="title-header-payment ml-2">PHƯƠNG THỨC THANH TOÁN</p>
            </div>
            <div class="information-payment-unit">
                <div class="d-flex justify-content-between sub-payment-unit">
                    <div class="container-payment">
                        <div class="name-payment-unit d-flex align-items-center">
                            <div class="selected checked name_payment"
                                 data-id="2"
                                 data-name="Thanh toán khi nhận hàng"
                            ></div>
                            <p class="name">Thanh toán khi nhận hàng</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between sub-payment-unit">
                    <div class="container-payment">
                        <div class="name-payment-unit d-flex align-items-center">
                            <div class="selected name_payment"
                                 data-id="4"
                                 data-name="CTV24H Pay"
                            ></div>
                            <p class="name">CTV24H Pay</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between sub-payment-unit">
                    <div class="container-payment">
                        <div class="name-payment-unit d-flex align-items-center">
                            <div class="selected name_payment"
                                 data-id="0"
                                 data-name="Ví CTV24H"
                            ></div>
                            <p class="name">Ví CTV24H (<?= isset($customer) ? number_format($customer->wallet) : 0 ?>đ)</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between sub-payment-unit">
                    <div class="container-payment">
                        <div class="name-payment-unit d-flex align-items-center">
                            <div class="selected name_payment"
                                 data-id="3"
                                 data-name="Thẻ ATM nội địa/ Internet Banking"
                            ></div>
                            <p class="name">Thẻ ATM nội địa/ Internet Banking</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between sub-payment-unit">
                    <div class="container-payment">
                        <div class="name-payment-unit d-flex align-items-center">
                            <div class="selected name_payment"
                                 data-id="5"
                                 data-name="Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB"
                            ></div>
                            <p class="name">Thanh toán bằng thẻ quốc tế Visa, Master Card, JCB</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between sub-payment-unit">
                    <div class="container-payment">
                        <div class="name-payment-unit d-flex align-items-center">
                            <div class="selected name_payment"
                                 data-id="1"
                                 data-name="Trả góp"
                            ></div>
                            <p class="name">Trả góp</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-lg-center align-items-center payment-visa">
                <div class="d-flex justify-content-lg-center align-items-center payment-visa-logo">
                    <img src="Icon/cart/visa.png">
                </div>
                <div class="d-flex align-items-center payment-visa-name-bank">
                    <p class="title">Ho Chi Minh City Development Commercial Joint Stock Bank **** 3011</p>
                </div>
            </div>
            <div class="d-flex justify-content-lg-center align-items-center other-card">
                <div class="d-flex align-items-center header-card create-card">
                    <span class="plus">+</span>
                    <p class="title-other-card">Thẻ khác</p>
                </div>
            </div>
            <div class="d-flex justify-content-lg-end align-items-center">
                <button class="btn btn-cancel-payment">Trở lại</button>
                <button class="btn btn-confirm-payment"
                >Hoàn thành
                </button>
            </div>
        </div>
    </div>
    <div class="popup-info-customer">
        <div class="container p-5">
            <h3 class="text-header">Nhập thông tin người nhận</h3>
            <br>
            <div class="form-group">
                <input data-url="{{url('card/search_customer')}}" type="text" class="form-control"
                       placeholder="Họ và tên người nhận" name="name_customer_ctv" id="name_customer_ctv">
                <div id="customerList"></div>
                <ul>

                </ul>
            </div>
            <div class="form-group">
                <input type="number" class="form-control" placeholder="Số điện thoại liên hệ" id="phone_customer_ctv">
            </div>
            <div class="form-group">
                <select required name="city" id="city" class="form-control choose_city city cityS"
                        data-url="{{url('select_district')}}"

                >
                    <option class="cityS" value="">Tỉnh/thành</option>
                    @foreach($city as $value)
                        <option class="cityS" value="{{$value->id}}"
                                data-city-name="{{$value->name}}"
                        >{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select required name="district" id="district" class="form-control choose_district district"
                        data-url="{{url('select_ward')}}"
                >
                    <option value=""
                            data-district-name="{{$value->name}}"
                    >Quận/huyện
                    </option>
                </select>
            </div>
            <div class="form-group">
                <select required name="ward" id="ward" class="form-control ward"
                >
                    <option value=""
                            data-ward-name="{{$value->name}}"
                    >Phường/xã
                    </option>
                </select>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Tòa nhà, Tên Đường..." id="address_customer_ctv">
            </div>
            <div class="d-flex justify-content-lg-end align-items-center">
                <button class="btn btn-cancel-customer-ctv">Trở lại</button>
                <button class="btn btn-confirm-customer-ctv"
                        data-url="{{url('cart/save_address_customer')}}"
                        data-order="{{$value->order_id}}"
                >Hoàn thành
                </button>
            </div>
        </div>
    </div>
    <div class="popup-info-customer-show">
        <div class="container-info-customer-show p-5">
            <h3 class="text-header-info-customer-show">Xác nhận thông tin người nhận</h3>
            <br>
            <div class="form-group">
                <p>Họ và tên: <span class="name-show"></span></p>
            </div>
            <div class="form-group">
                <p>Số điện thoại: <span class="phone-show"></span></p>
            </div>

            <div class="form-group">
                <p>Địa chỉ nhận hàng: <span class="address-show"></span></p>
            </div>
            <div class="d-flex justify-content-lg-end align-items-center">
                <button class="btn btn-cancel-customer-ctv-show">Trở lại</button>
                <button class="btn btn-confirm-customer-ctv-show">Hoàn thành
                </button>
            </div>
        </div>
    </div>
    <div class="popup-credit-card position-fixed position-center">
        <div class="head-popup">
            <span class="text-blue text-bold-700 font-size-16">Thêm thẻ tín dụng/ghi nợ</span>
        </div>
        <div class="body-popup mt-4">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Họ và tên chủ thẻ">
            </div>
            <div class="d-flex form-group align-items-center justify-content-center">
                <div class="col-lg-5 p-0">
                    <input type="text" class="form-control" placeholder="Số thẻ tín dụng/Ghi nợ">
                </div>
                <div class="d-flex col-lg-7 p-0 align-items-center justify-content-lg-around">
                    <div class="img-unit">
                        <img style="width: 50px; height: 20px; border-radius: 8px; object-fit: cover;"
                             src="Icon/cart/visa_blur.png" alt="">
                    </div>
                    <div>
                        <img style="width: 45px; height: 20px; border-radius: 8px; object-fit: cover;"
                             src="Icon/cart/mastercard.png" alt="">
                    </div>
                    <div>
                        <img style="width: 45px; height: 20px; border-radius: 8px; object-fit: cover;"
                             src="Icon/cart/jcb.png" alt="">
                    </div>
                    <div>
                        <img style="width: 45px; height: 20px; border-radius: 8px; object-fit: cover;"
                             src="Icon/cart/logoexpress.png" alt="">
                    </div>
                </div>
            </div>
            <div class="d-flex align-items-center justify-content-between">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Ngày hết hạn">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="cvv">
                </div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="14/168 Nguyễn Xiển">
            </div>
        </div>
        <div class="footer-popup-credit-card d-flex float-right">
            <button style="width: 110px;" class="btn-back-card mr-3">Trở lại</button>
            <button style="width: 110px;" class="btn btn-done">Hoàn thành</button>
        </div>
    </div>
    <!-- END: Cart-->
@include('frontend.address.modal_maps')
@include('frontend.address.modal_success')
@include('frontend.address.modal_error')
<!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>

@include('frontend.base.script')
@include('frontend.address.modal_delete')

<script src="js/address/index.js"></script>
<script src="js/address/maps.js"></script>
{{--<script async defer--}}
{{--        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYutTAaM_NN7-HgRsJb5mPky4ANnuesaU&callback=initMap"--}}
{{--        type="text/javascript"></script>--}}
<script src="js/cart/index.js"></script>
<script>
    if ($('.check_coin_view_ctv').is(':checked')) {
        $('.form_coin').removeClass('hide_form');
    } else {
        $('.form_coin').addClass('hide_form');
    }

    $('.check_coin_view_ctv').click(function () {
        let total_old = $('.total_price').attr('data-total-old');
        let check_coin = $(this).is(':checked') ? 1 : 0;
        let total_price_1 = $('.total_price').attr('data-total');
        let total_price_order = parseInt(total_price_1);
        let coins_1 = $(this).attr('data-coin');
        let coins = parseInt(coins_1);
        if (check_coin == 1) {
            $('.form_coin').removeClass('hide_form');
            total_price_order = total_price_order - coins;
            $('.total_price').text(formatNumber(total_price_order, '.', ',') + ' đ');
            $('.checkout-cart').attr('data-total-payment'   ,total_price_order);

        } else {
            $('.form_coin').addClass('hide_form');
            $('.total_price').text(formatNumber(total_old, '.', ',') + ' đ');
            $('.checkout-cart').attr('data-total-payment',total_old);
        }
    });
</script>
</body>
<!-- END: Body-->
</html>



