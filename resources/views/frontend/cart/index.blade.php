<?php

use Illuminate\Support\Facades\URL;
$data_login = Session::get('data_customer');

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/cart/style.css">
<link rel="stylesheet" type="text/css" href="css/home/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    @if(count($dataShop))
        <?php if($type === 'OrderDeliveryToCTV')  :?>
        @include('frontend.cart.OrderDeliveryToCTV')
        <?php elseif($type === 'OrderDeliveryCustomersCTV') :?>
        @include('frontend.cart.OrderDeliveryCustomersCTV')
        <?php else :?>
        <div class="container">
            <div class="note-cart d-flex align-items-center">
                <object data="Icon/cart/free-delivery.svg" width="30" height="30"></object>
                <p class="m-0 pl-3">Nhấn vào mục Mã Giảm giá ở cuối trang để hưởng miễn phí vận chuyển bạn nhé !</p>
            </div>
            <div class="d-flex align-items-center header-cart mt-3 p-3">
                <div class="col-lg-6 align-items-center">
{{--                    <input type="checkbox">--}}
                    <span class="ml-3">Sản Phẩm</span>
                </div>
                <div class="col-lg-6 align-items-center d-flex">
                    <div class="col-lg-2 align-items-center">
                        <center>Đơn giá</center>
                    </div>
                    <div class="col-lg-4 align-items-center">
                        <center>Số lượng</center>
                    </div>
                    <div class="col-lg-3 align-items-center">
                        <center>Số tiền</center>
                    </div>
                    <div class="col-lg-3 align-items-center">
                        <center>Thao tác</center>
                    </div>
                </div>
            </div>

            <!-- BEGIN: Option 1-->

            <?php
            $total = 0;
            $total_shop = 0;
            $total_all = 0;
            ?>
            <?php if(isset($dataShop)): ?>
            <?php foreach ($dataShop as $value): ?>
            <div class="container-item-cart">
                <div class="d-flex align-items-center header-cart mt-3 p-3">
                    <div class="col-lg-12 align-items-center d-flex item-cart-shop">
                        <input type="checkbox" class="checked-shop" value="{{$value->shop_id}}">
                        <object data="Icon/cart/shop_name.svg" class="ml-3" width="20" height="20"></object>
                        <a href="{{url('seeshop/'.$value->id_shop)}}"><b class="ml-3">{{$value->name_shop}}</b></a>
                        <object data="Icon/cart/love.svg" class="ml-3" width="70" height="auto"></object>
                        <div class="align-items-center more-cart p-2 ml-3">
                            <span class="text-bold-700 color-text-danger preventdefault">Mua kèm deal sốc </span>
                            <span class="text-bold-700 ml-5 preventdefault">Xem Thêm</span>
                            <span class="text-bold-700 ml-2">></span>
                        </div>
                    </div>
                </div>

                <?php foreach($value->prd as $item) : ?>
                    <div class="d-flex align-items-center header-cart mt-3 p-3 relative item-cart">
                        <a onclick="return confirm('Bạn có thực sự muốn xóa không?')" href="{{url('cart/delete_product/'.$item->id)}}"
                           data-url="{{url('cart/delete_product/'.$item->id)}}"
                        >
                            <span class="icon-close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </a>
                        <div class="col-lg-6 align-items-center d-flex">
                            <input type="checkbox" name="cart[]" class="check-product"
                                   value="{{$item->id}}"
                                   data-url="{{url('cart/check_cart')}}"
                            >
                            <a href="{{url('details_product/'.$item->id_product)}}">
                                <img style="height: 100px;" src="{{$item->image}}" class="ml-3 img-product">
                            </a>
                            <a href="{{url('details_product/'.$item->id_product)}}">
                                <b class="ml-3" style="max-width: 297px !important;">{{$item->name}} </b>
                            </a>

                            <div class="ml-5 justify-content-center">
                                <p class="color-type"><?= __('Phân loại :')?></p>
                                <p class="color-type"> {{$item->cate_2}} - {{$item->size_of_value}}</p>
                            </div>
                        </div>
                        <div class="col-lg-6 align-items-center d-flex">
                            <div class="col-lg-2 align-items-center">
                                <center>
                                    <b class="price-new"
                                    >{{number_format($item->price_cart)}}đ</b>
                                        @if($item->percent > 0)
                                            <p class="price-old pt-2 m-0">{{number_format($item->price_attr)}}đ</p>
                                        @endif
                                </center>
                            </div>
                            <div class="col-lg-4 align-items-center">
                                <center>
                                    <div class="d-flex view-add-cart justify-content-center">
                                        <button style="background: none!important; color: black!important;"
                                                class="btn btn-cart btn-minus"
                                                data-url="{{url('cart/update/'.$item->id)}}"
                                                data-price="{{$item->price_attr}}"
                                                data-value = {{$item->id}}
                                        >-
                                        </button>
                                        <input pattern=" 0+\.[0-9]*[1-9][0-9]*$" name="itemConsumption"
                                               onkeypress="return event.charCode >= 48 && event.charCode <= 57"
                                               class="input-cart input-cart-{{$item->id}}" value="{{$item->quantity}}"
                                               type="number"
                                               data-url="{{url('cart/update/'.$item->id)}}"
                                               data-id="{{$item->id}}"
                                               data-price="{{$item->price_attr}}"
                                        >
                                        <button style="background: none!important; color: black!important;"
                                                class="btn btn-cart btn-plus"
                                                data-url="{{url('cart/update/'.$item->id)}}"
                                                data-price="{{$item->price_attr}}"
                                                data-value = "{{$item->id}}"
                                        >+
                                        </button>
                                    </div>
                                </center>
                            </div>
                            <div class="col-lg-3 align-items-center total-temp">
                                <center><span
                                        class="total-price" id="price_{{$item->id}}"
                                        data-price-temporary="{{$item->price_attr * $item->quantity}}"
                                    >{{number_format($item->price_cart * $item->quantity)}}đ</span>
                                </center>
                            </div>
                            <?php
                            $total = $item->price_attr * $item->quantity;
                            $total_shop += $total;
                            ?>
                            <div class="col-lg-3 align-items-center preventdefault">
                                <div class="dropdown relative">
                                    <b class="dropdown-toggle text-right show-content preventdefault" type="button"
                                       id="dropdownMenuButton">
                                        Tìm sản phẩm tương tự
                                    </b>
                                </div>
                            </div>
                        </div>
                        <div class="popup-cart">
                            <?php
                            $path = "assets/frontend/Icon/home/Danh_muc";
                            ?>
                            <div class="container-section-product slide-popup">
                                @if(isset($dataRelatedProduct))
                                    @foreach($dataRelatedProduct as $value)
                                        @if($value->category_id == $item->category_id)
                                            <div class="content border-box relative sp-product-content">
                                                <a href="{{url('details_product/'.$value->id)}}">
                                                    <div class="img-product">
                                                        <img src="{{$value->image}}">
                                                    </div>
                                                    <div class="item-content-product">
                                                        <div class="title-product-n relative">
                                                            <p class="card-title">{{$value->name}}</p>
                                                        </div>
                                                        <div class="product-price">
                                                            <p class="price"><span>{{number_format($value->price_min)}} đ</span> -
                                                                <span class="price-sale">{{number_format($value->price_max)}} đ</span></p>
                                                            <p class="pass">Giá CTV: ********</p>
                                                            <div class="form btn-sign-up relative">
                                                                <button>Đăng ký CTV để xem giá</button>
                                                            </div>
                                                        </div>
                                                        <div class="review">
                                                            <img src="<?php echo asset("$path/icon-review.png")?>">
                                                            <p>Đã bán {{$value->is_sell}}</p>
                                                        </div>
                                                        <div class="review">
                                                            <img style="width: 20px;"
                                                                 src="<?php echo asset("$path/icon-like.png")?>">
                                                            <p>{{$value->place}}</p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                            <div class="paginate">
                                <div class="previous border-paginate">
                                    <object data="Icon/cart/previous.svg"></object>
                                </div>
                                <div class="append-dots">
                                    <div class="number-paginate border-paginate is-paginate">2</div>
                                    <div class="number-paginate border-paginate">3</div>
                                </div>
                                <div class="next border-paginate">
                                    <object data="Icon/cart/next.svg"></object>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
        <?php
        $total_all += $total_shop;
        ?>
        <?php endif; ?>
        <!-- Begin: voucher-->
            <div class="d-flex align-items-center header-cart mt-3 p-3">
                <div class="col-lg-6 align-items-center">
                </div>
                <div class="col-lg-6 align-items-center d-flex">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-2"></div>
                    <div class="col-lg-2"></div>
                    <div class="col-lg-6">
                        <center class="voucher">
                            <span>Chọn Hoặc Nhập Mã Voucher</span>
                            <span class="ml-3"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                        </center>
                    </div>
                </div>
            </div>
            <!-- End: voucher-->

            <!-- Begin: xu-->
            <div class="d-flex justify-content-end header-cart mt-1 p-3">
                <div class="d-flex align-items-center pr-5">
                    <input value="{{isset($data_login) ? $data_login->coin : 0}}" class="check_coin" type="checkbox">
                    <object data="Icon/cart/so_xu.svg" class="ml-3" width="20" height="20"></object>
                    <b class="ml-3">Xu</b>
                    <span class="ml-3 text-xu">Dùng {{isset($data_login) ? $data_login->coin : 0}} xu</span>
                    <span class="ml-5 text-xu">-{{isset($data_login) ? number_format($data_login->coin) : 0}}đ</span>
                </div>
            </div>
            <!-- End: xu-->

            <!-- Begin: bonus-->
            <div class="d-flex justify-content-end header-cart mt-1 p-3">
                <div class="d-flex align-items-center pr-5">
                    <span class="mr-3 text-xu">Cộng xu</span>
                    <span class="ml-5 text-bonus">+{{number_format($total_all * 0.1/100)}} xu</span>
                </div>
            </div>
            <!-- End: bonus-->

            <!-- Begin: checkout-->
            <div class="align-items-center d-flex header-cart mt-1 p-3">
                <div class="col-lg-6 align-items-center d-flex">
                    <input type="checkbox" class="checked-all">
                    <b class="ml-3">Chọn tất cả </b>
                </div>
                <div class="col-lg-6 align-items-center">
                    <div class="mr-auto float-left bookmark-wrapper">
                    </div>
                    <div class="float-right d-flex align-items-center pr-5">
                        <span>Tổng tiền hàng</span>
                        <span class="ml-5 price-total" data-value="0">0đ</span>
{{--                        <span class="ml-5 price-total">{{number_format($total_all)}}đ</span>--}}
                    </div>
                </div>
            </div>
            <div class="align-items-center header-cart p-3 mb-5">
                <div class="col-lg-12 align-items-center mb-5">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    </div>
                    <div class="float-right d-flex align-items-center pr-5 mb-5">
                        <button type="button" data-customer="{{isset($data_login) ? $data_login['id'] : ''}}"
                                class="mr-2 btn btn-danger btn-checkout" data-url="{{url('cart/save')}}">Đặt giao cho
                            CTV
                        </button>
                        {{--                        <a class="btn btn-primary btn-add-order"--}}
                        {{--                           data-url--}}
                        {{--                           href="<?= URL::to('cart?type=OrderDeliveryCustomersCTV') ?>">Đặt giao cho--}}
                        {{--                            khách của CTV</a>--}}
                        <button type="button" data-customer="{{isset($data_login) ? $data_login['id'] : ''}}"
                                class="mr-2 btn btn-primary btn-checkout-customer-ctv"
                                data-url="{{url('cart/save_order_to_customer_ctv')}}">Đặt giao cho khách của CTV
                        </button>
                    </div>
                </div>
            </div>
            <!-- End: checkout-->
        </div>
        <?php endif; ?>
    @else
        <h3 class="text-center">Giỏ hàng trống</h3>
    @endif
    <div class="w-100 modal-fixed" id="modalAddress"></div>
@include('frontend.address.modal_maps')
@include('frontend.address.modal_success')
@include('frontend.address.modal_error')
<!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>

@include('frontend.base.script')
@include('frontend.address.modal_delete')

<script src="js/address/index.js"></script>
<script src="js/address/maps.js"></script>
{{--<script async defer--}}
{{--        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYutTAaM_NN7-HgRsJb5mPky4ANnuesaU&callback=initMap"--}}
{{--        type="text/javascript"></script>--}}
<script src="js/cart/index.js"></script>
</body>
<!-- END: Body-->
</html>
