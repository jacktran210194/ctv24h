<?php
$path = "assets/frontend/Icon/products";
$data_login = Session::get('data_customer');
use Illuminate\Support\Facades\URL;?>
<p class="content-shipping" style="color: #000;padding: 9px 0;">Đơn hàng được tạo bởi </p>
<div class="customer flex">
    <div class="image-customer">
        <img src="{{$data_login -> image}}">
    </div>
    <div class="name-customer">{{$data_login -> name}}</div>
</div>
@foreach($miniCart as $key)
    <div class="item item_1">
        <div class="container-item">
            <div class="flex">
                <div class="div-quantity flex">
                    <a class="minus" data-url="{{url('cart/update/'.$key->cart_id)}}"
                       data-id="{{$key->cart_id}}"
                    >-</a>
                    <span class="quantity" data-quantity="{{ $key->quantity_prd }}">{{ $key->quantity_prd }}</span>
                    <a class="plus" data-url="{{url('cart/update/'.$key->cart_id)}}"
                       data-id="{{$key->cart_id}}" data-quantity-product="{{ $key->value_quantity }}">
                        <object data="Icon/products/icon_plus.svg" width="8px"></object>
                    </a>
                </div>
                <div class="titel-product">
                    <p class="content-shipping title font-weight-bold" style="color: #222222">{{ $key->name_prd }}</p>
                </div>
            </div>
            <div class="d-flex justify-content-between" style="margin-top: 10px">
                <p class="font-weight-bold">{{$key->name_category}} : </p>
                <p class="font-weight-bold">{{$key->name_value}}/{{$key->name_size}}</p>
            </div>
            <div class="price-product flex">
                <p class="text font-weight-bold">Giá</p>
                <p class="price color-red">
                    {{ number_format($key->quantity_prd*$key->price_prd)}} đ
                </p>
            </div>
            <div class="price-product flex reward-points">
                <p class="text font-weight-bold">Điểm thưởng</p>
                <p class="points color-red">+ 350 Xu (1%)</p>
            </div>
        </div>
    </div>
@endforeach
<div class="total-price flex">
    <p class="text-total">Tổng cộng </p>
    <p class="total color-red">
        <?php
        $array = [];
        ?>
        @foreach($miniCart as $key)
            <?php
            $priceItem = $key->price_prd;
            $totalPriceItem = $priceItem * $key->quantity_prd;
            array_push($array, $totalPriceItem);
            ?>
        @endforeach
        <?php echo number_format(array_sum($array)); ?>đ
    </p>
</div>
<div class="btn-checkout">
    <a href="{{url("cart")}}">
        <button class="checkout">Đặt hàng</button>
    </a>
</div>
