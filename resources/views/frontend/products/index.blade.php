<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/products/style.scss">

<!-- or the reference on CDN -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
<!-- END: Head-->
<style>
    .content_percent::-webkit-scrollbar-track {
        background: #f1f1f1;
        border-radius: 4px;
    }
    .content_percent::-webkit-scrollbar-thumb {
        background: #d10e0c;
        border-radius: 4px;
    }
    #primary-slider .splide__track{
        width: 100%;
    }
    .popup-edit-product{
        opacity: 0;
        z-index: -100;
        transition: 1s;
    }
    .popup-edit-product.active{
        opacity: 1;
        z-index: 10000;
    }
    .bg-red{
        background: linear-gradient(180deg, #D1292F 0%, #FF5722 155.26%);
    }
    .border-radius-8{
        border-radius: 8px;
    }
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
</style>
<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->
@include('frontend.products.informationProduct')
<!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
    <div class="popup-add-drop-ship d-flex align-items-center justify-content-sm-center w-100 h-100"></div>
    <div class="popup-edit-product d-flex justify-content-center align-items-center position-fixed w-100 h-100" style="top: 0; left: 0; background: #33333330;"></div>
</div>
@include('frontend.base.script')
<script src="js\products\index.js"></script>
<script src="js\products\ajax_review.js"></script>
<script src="js\products\ajax_download_file.js"></script>
<script src="js\products\update_quantity_mini_cart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>
{{--// JS dropship//--}}
<script>
    $(document).ready(function () {
        let url = window.location.origin + '/drop-ship';
        $('.btn-add-dropship').click(function () {
            getData();
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function getData(){
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'html',
                success: function (data) {
                    $('.popup-add-drop-ship').addClass("active");
                    $('.popup-add-drop-ship').html(data);
                }
            });
        };
        $('.popup-add-drop-ship').click(function (e) {
            if ($(e.target).is(".popup-add-drop-ship") === true) {
                $('.popup-add-drop-ship').removeClass("active");
                $('.popup-add-drop-ship').html("");
            }
        });
        $(document).on("click", ".btn-close-popup", function () {
            $('.popup-add-drop-ship').removeClass("active");
        });
        //----------------//
        let html = '<div><input type="text" class="w-100 input-category" placeholder="Tên danh mục"></div>' +
            '<div class="d-flex justify-content-between align-items-center" style="margin-top: 30px">' +
            '<button class="btn-cancel btn-ctg font-weight-bold" style="background: #ECECEC;color: #333333;">Hủy</button>'+
            '<button class="btn-create-category btn-ctg font-weight-bold" style="background: linear-gradient(180deg, #D1292F 0%, #FF5722 155.26%);color: #ffffff">Tạo danh mục</button>'
            +'</div>';
        $(document).on("click", ".btn-category-collaborator", function () {
            $(".ct-container").html(html);
        });
        $(document).on("click", ".btn-cancel", function () {
            getData();
        });
        $(document).on("click", ".btn-create-category", function () {
            let data ={};
            let url_post = window.location.origin + '/drop-ship/add';
            data['name'] = $('.input-category').val();
            if ( data['name'] == "" ){
                alert('không để trống');
            }else {
                $.ajax({
                    url: url_post,
                    type: "POST",
                    dataType: "json",
                    data: data,
                    success:function (data) {
                        if (data.status){
                            getData();
                        }else {
                            swal('Thất bại', data.msg, 'error');
                        }
                    }
                });
            }
        });
        //-----------------add product drop ship--------------//
        $(document).on("click",".popup-add-drop-ship .radio", function () {
            let url = window.location.origin + '/details_product/add_product_drop_ship';
            let data ={};
            data['product_id'] = {{$data->id}};
            data['shop_id'] = {{$data->shop_id}};
            data['category'] = $(this).attr("data-value");
            $.ajax({
                url: url,
                data: data,
                type:'POST',
                dataType:'json',
                success:function (data) {
                    $(".popup-edit-product").addClass("active");
                    $('.popup-add-drop-ship').removeClass("active");
                    $(".popup-edit-product").html(data.prod);
                },
                error:function () {
                    console.log('error');
                }
            });
        });
        $(".close-popup-drop-ship").click(function () {
            location.reload();
        });
    });
    @if(isset($data_customer))
    channel.bind('my-event', function (data) {
        let length = $('.item-chat-' + data.message.group_id).length;
        if (length > 0) {
            let group_id = $('.customer-active').attr('data-group-id');
            if (group_id == data.message.group_id) {
                $('.list-messenger-' + data.message.group_id).append('<div class="flex ctn-chat-content customer-chat">\n' +
                    '                        <div class="img-customer">\n' +
                    '                            <img src="' + data.message.avt_customer + '" class="img-avt-chat">\n' +
                    '                        </div>\n' +
                    '                            <div class="text-chat">\n' +
                    '                                <label>' + data.message.messenger + '</label>\n' +
                    '                            </div>\n' +
                    '                    </div>');
                $('.list-messenger-' + data.message.group_id).animate({
                    scrollTop: $('.list-messenger-' + data.message.group_id)[0].scrollHeight
                }, 0);
            }
            $('.messenger-last-' + data.message.group_id).text(data.message.messenger);
        }
    });
    @endif
</script>
</body>
<!-- END: Body-->
</html>
