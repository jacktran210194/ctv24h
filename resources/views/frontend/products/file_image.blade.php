<div class="d-flex justify-content-lg-center align-items-center"><button class="download-all">Tải Tất Cả</button></div>
<div class="popup-file-image">
    @if(count($data_image))
        @foreach($data_image as $value)
            <div class="content-image">
                <div class="image">
                    <img style="object-fit: cover;" width="86px" height="86px" src="{{$value->image}}">
                </div>
                <div class="download">
                    <a class="link-file-image" href="{{$value->image}}" download="<?php echo str_replace("/uploads/images/product/", " ", $value->image ) ?>">Tải ảnh</a>
                </div>
            </div>
        @endforeach
    @endif
</div>

