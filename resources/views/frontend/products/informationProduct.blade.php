<div id="product-page">
    <div id="product">
        <?php
        $path = "assets/frontend/Icon/products";
        $data_login = Session::get('data_customer') ?? null;
        use Illuminate\Support\Facades\URL;?>
        <div class="container-product">
            <div class="product-gallery width-50">
                <div id="primary-slider" class="img-gallery splide">
                    <div class="splide__track">
                        <div class="splide__list">
                            @if(isset($data->video) && $data->video != 'undefined')
                                <div class="img relative splide__slide">
                                    <iframe width="354px" height="473px" src="{{$data->video}}?autoplay=0&mute=1"
                                            title="YouTube video player" frameborder="0"
                                            allow="accelerometer; autoplay=0; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                </div>
                            @endif
                            <div class="img splide__slide">
                                <img style="object-fit: contain;" width="354px" height="473px" src="{{$data->image}}">
                            </div>
                            @if(count($data_image))
                                @foreach($data_image as $value)
                                    <div class="img splide__slide">
                                        <img style="object-fit: contain;" width="354px" height="473px"
                                             src="{{$value->image}}">
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div id="secondary-slider" class="img-thumbnail splide">
                    <div class="splide__track">
                        <div class="splide__list">
                            @if(isset($data->video) && $data->video != 'undefined')
                                <div class="img relative splide__slide">
                                    <iframe class="w-100" src="{{$data->video}}?autoplay=0&mute=1"
                                            title="YouTube video player" frameborder="0"
                                            allow="accelerometer; autoplay=0; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                </div>
                            @endif
                            <div class="img relative splide__slide">
                                <img class="w-100" src="{{$data->image}}">
                            </div>
                            @if(count($data_image))
                                @foreach($data_image as $value)
                                    <div class="img relative splide__slide">
                                        <img style="object-fit: contain;" class="w-100" src="{{$value->image}}">
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="footer-gallery">
                    <div class="link">
                        <div class="icon-social">
                            <p class="text">Chia sẻ ngay:</p>
                            <div class="Icon">
                                <object data="Icon/products/logo-facebook.svg" width="16px"></object>
                                <object data="Icon/products/logo-zalo.svg" width="16px"></object>
                                <object data="Icon/products/like.svg" width="16px"></object>
                                <object data="Icon/products/instagram.svg" width="16px"></object>
                            </div>
                        </div>
                        <button style="font-size: 12px!important; width: auto!important;"
                                class="button kt-ctv flex preventdefault ml-3">
                            <object style="margin-right: 10px!important;" width="20px"
                                    data="Icon/products/icon_money.svg"></object>
                            Kiếm tiền cùng CTV24h
                        </button>
                        <input type="hidden" class="count_like" value="{{$count_like}}">
                        <?php
                        if (isset($data_login)) {
                            $dataFavouriteShop = \App\Models\FavouriteProductModel::where('customer_id', $data_login->id)->where('product_id', $data->id)->first();
                        }
                        ?>
                        <?php if(isset($dataFavouriteShop)) :?>
                        <div class="liked">
                            {{--                                Đã thích--}}
                            <a @if(!$data_login) href="{{url('login')}}"
                               @endif class="d-flex align-items-center flex-wrap"
                               data-id="{{$data->id}}"
                               data-url="{{url('details_product/'.$data->id.'/dislike')}}"
                            >
                                <div class="d-flex align-items-center flex-wrap">
                                    <div class="icon_like">
                                        <img data-id="{{$data->id}}"
                                             data-url="{{url('details_product/'.$data->id.'/dislike')}}"
                                             class="show_like dislike-product" src="Icon/like/like_xanh.png">
                                    </div>
                                    <p><span class="text_like_1">Đã Thích</span> <span class="text_count_like_1">({{$count_like}})</span>
                                    </p>
                                </div>
                            </a>
                        </div>
                        <?php else: ?>
                        <div class="liked">
                            {{--                                Chưa thích--}}
                            <a @if(!$data_login) href="{{url('login')}}"
                               @endif class="d-flex align-items-center flex-wrap"
                            >
                                <div class="d-flex align-items-center flex-wrap">
                                    <div class="icon_like">
                                        <img data-id="{{$data->id}}"
                                             data-url="{{url('details_product/'.$data->id.'/like')}}"
                                             class="show_like like-product" src="Icon/like/like_xam.png">
                                    </div>
                                    <p><span class="text_like_1">Thích</span> <span class="text_count_like_1">({{$count_like}})</span>
                                    </p>
                                </div>
                            </a>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="upload">
                        <div class="btn-upload" onclick="copy('#description-content')">
                            <object data="Icon/products/tep_tin.svg" width="20px"></object>
                            <p>Sao Chép</p>
                        </div>
                        <div id="target" contentEditable="true"></div>
                        <div class="btn-upload btn-upload-image">
                            <object data="Icon/products/upload.svg" width="20px"></object>
                            Tải Ảnh
                        </div>
                        <div class="btn-upload btn-create-order"
                             data-id="{{$data->id}}"
                             data-shop="{{$dataShop->id}}"
                             data-url="{{url('details_product/add_to_mini_cart')}}"
                             data-customer=<?= isset($data_login) ? $data_login['id'] : ''?>
                        >
                            <object data="Icon/products/add-file.svg" width="20px"></object>
                            <p>Tạo Đơn Nhanh</p>
                        </div>
                    </div>
                    <div id="file-download"></div>
                    <div>
                        <div class="upload-dropship">
                            <div class="btn-upload btn-dropship">
                                <object data="Icon/products/dropship.svg" width="20px"></object>
                                <p>Dropship</p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="information width-50">
                <h1 class="title-product">
                    <?= isset($data) ? $data->name : '' ?>
                </h1>
                <div class="total-review">
                    @if($total_comment == 0)
                        <div class="star-review sub-content">
                            <img src="<?php echo asset("$path/0_sao.png")?>">
                        </div>
                    @else
                        @if($average_review == 1)
                            <div class="star-review sub-content">
                                <img src="<?php echo asset("$path/1_sao.png")?>">
                            </div>
                        @elseif($average_review == 2)
                            <div class="star-review sub-content">
                                <img src="<?php echo asset("$path/2_sao.png")?>">
                            </div>
                        @elseif($average_review == 3)
                            <div class="star-review sub-content">
                                <img src="<?php echo asset("$path/3_sao.png")?>">
                            </div>
                        @elseif($average_review == 4)
                            <div class="star-review sub-content">
                                <img src="<?php echo asset("$path/4_sao.png")?>">
                            </div>
                        @elseif($average_review == 5)
                            <div class="star-review sub-content">
                                <img src="<?php echo asset("$path/5_sao.png")?>">
                            </div>
                        @elseif($average_review > 1 && $average_review < 2)
                            <div class="star-review sub-content">
                                <img src="<?php echo asset("$path/1_5_sao.png")?>">
                            </div>
                        @elseif($average_review > 2 && $average_review < 3)
                            <div class="star-review sub-content">
                                <img src="<?php echo asset("$path/2_5_sao.png")?>">
                            </div>
                        @elseif($average_review > 3 && $average_review < 4)
                            <div class="star-review sub-content">
                                <img src="<?php echo asset("$path/3_5_sao.png")?>">
                            </div>
                        @elseif($average_review > 4 && $average_review < 5)
                            <div class="star-review sub-content">
                                <img src="<?php echo asset("$path/4_5_sao.png")?>">
                            </div>
                        @endif
                    @endif
                    <div class="star-review sub-content">
                        <span class="number-rivew"><?= isset($total_comment) ? $total_comment : ''?></span> Đánh Giá
                    </div>
                    <div class="star-review sub-content">
                        <span class="number-rivew"><?= isset($data) ? $data->is_sell : '' ?></span> Đã bán
                    </div>
                </div>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="price-product">
                        <div class="price">
                            <span class="price-old" style="display: none"><?= isset($data) ? number_format($data->price) : '' ?> đ</span>
                            <span class="price-buy">{{number_format($product_attribute->price)}} đ</span>
                        </div>
                        <div class="price-sale info_percent position-relative">
                            <img class="img_percent" style="width: 45px"
                                 src="<?php echo asset("$path/img_percent.png")?>">
                            <span style="z-index: 60000; bottom: 45%!important;"
                                  class="percent_product position-absolute position-center text-bold-700 text-white">{{$percent}}%</span>
                        </div>
                    </div>
                    @if($data_login)
                        @if($check_drop_ship)
                            <button class="d-flex justify-content-sm-center align-items-center bg-white"
                                    style="border:1.5px solid #FF0000; padding: 10px; border-radius: 8px">
                                <img src="Icon/products/icon_sale_product.png">
                                <span class="font-weight-bold"
                                      style="font-size: 14px; color: #FF0000; margin-left: 15px">Đã đăng ký bán sản phẩm</span>
                            </button>
                        @else
                            @if($data_login->id == $id_customer)
                            @else
                                <button
                                    class="btn-add-dropship d-flex justify-content-sm-center align-items-center bg-white"
                                    style="border:1.5px solid #FF0000; padding: 10px; border-radius: 8px">
                                    <img src="Icon/products/icon_sale_product.png">
                                    <span class="font-weight-bold"
                                          style="font-size: 14px; color: #FF0000; margin-left: 15px">Đăng ký bán sản phẩm</span>
                                </button>
                            @endif
                        @endif
                    @endif
                </div>
                <div class="note">
                    <div class="div-image">
                        <img src="<?php echo asset("$path/star.png")?>">
                    </div>
                    <div class="div-text">
                        Nâng cấp tài khoản ngay hoặc tích lũy đủ Point để nâng hạng và nhận chiết khấu hấp dẫn
                    </div>
                </div>
                <div class="product-description">
                    <div class="sign-up-ctv">
                        <p class="sub-title-description">Giá đại lý/CTV</p>
                        <div class="card"
                             style="border: 1px solid #ECECEC;box-sizing: border-box;box-shadow: 0px 16px 24px rgba(0, 0, 0, 0.06), 0px 2px 6px rgba(0, 0, 0, 0.04), 0px 0px 1px rgba(0, 0, 0, 0.04);border-radius: 12px;">
                            <div class="card-body info_price_ctv">
                                <span class="text-bold-700 price_ctv" style="font-size: 24px!important;">{{number_format($product_attribute->price_ctv)}} đ</span>
                            </div>
                        </div>
                        {{--                        <button class="bt-sign-up">Đăng ký CTV để xem giá</button>--}}
                        <div class="ml-4">|</div>
                        <p class="sub-title-description ml-4 info_price_profit">Lợi nhuận <span class="price_profit">{{number_format($product_attribute->price - $product_attribute->price_ctv)}} đ</span>
                        </p>
                    </div>
                    {{--                    <div class="deal">--}}
                    {{--                        <p class="sub-title-description">Deal sốc</p>--}}
                    {{--                        <button class="bt-sign-up">Mua kèm deal sốc</button>--}}
                    {{--                    </div>--}}
                    <div class="shipping">
                        <p class="sub-title-description">Vận chuyển</p>
                        <p class="content-shipping">Gửi từ : <span class="text-bold-700">{{$data->place_product}}</span>
                            @if(isset($address_customer))
                            <span style="display: block">Gửi đến: <span
                                    class="text-bold-700">{{$address_customer->address}}</span></span>
                            @endif
                            <span style="display: block">Phí vận chuyển dự tính : <span
                                    class="text-bold-700">10.000 đ</span></span></p>
                    </div>
                    <div class="shipping">
                        <p class="sub-title-description">Loại sản phẩm</p>
                        <p class="text-bold-700">@if($data->dropship == 0) Có sẵn @else Dropship @endif </p>
                    </div>
                    <input hidden value="{{$data->id}}" name="id_product">
                    @if(isset($data_percent) && count($data_percent))
                        <div class="d-flex" style="margin-top: 20px">
                            <div class="bg-red color-white font-weight-bold d-flex justify-content-center align-items-center" style="background: linear-gradient(180deg, #D1292F 0%, #FF5722 155.26%); padding: 10px 0px; width: 110px">Chiết khấu</div>
                            <div class="d-flex w-100 justify-content-between align-items-center content_percent" style="overflow-x: auto; overflow-y: hidden; padding-bottom: 10px; margin-left: 30px">
                                @foreach($data_percent as $value)
                                    @php $price_percent = $product_attribute->price_ctv - $product_attribute->price_ctv * $value->percent / 100; @endphp
                                    <div class="percent" style="margin-right: 10px; flex: 0 0 33%" data-percent="{{$value->percent}}">
                                        <p class="color-white font-weight-bold text-center" style="background: linear-gradient(180deg, #D1292F 0%, #FF5722 155.26%); padding: 10px 0; font-size: 16px">Từ {{$value->quantity_1}}-{{$value->quantity_2}}SP</p>
                                        <div style="margin-top: 10px">
                                            <p class="font-size-24 text-center price_percent" style="color: orange; font-size: 20px">{{number_format($price_percent)}}đ</p>
                                            <div class="text-center" style="font-size: 16px; margin-top: 5px">
                                                <span class="price_decoration" style="color: #999999; text-decoration: line-through; padding-right: 5px;">{{number_format($product_attribute->price_ctv)}}đ</span> |
                                                <span class="text-uppercase" style="padding-left: 5px; color: red">{{$value->percent}}%</span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                        <div class="content-color-swath">
                            <div class="color-swath color-swath_{{$title_group_attr->id}}">
                                <p class="sub-title-description">{{$title_group_attr->category_1}}</p>
                                @foreach($attr as $index => $key)
                                    <div class="color @if($index == 0) active @endif " data-value="{{$key->id}}">
                                        <button class="bt-color"
                                        >{{$key->category_2}}</button>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="size">
                            <p class="sub-title-description">{{$title_value_attr->value}}</p>
                            <div class="container-size">
                                @foreach($item as $key)
                                    <div class="content-size flex" data-type="{{$key->attribute_id}}"
                                         data-size="{{$key->size_of_value}}"
                                         data-price="{{$key->price}}"
                                         data-price-goc="{{$data->price}}"
                                         data-price-ctv="{{$key->price_ctv}}"
                                         data-id="{{$key->id}}"
                                         data-quantity="{{$key->quantity}}">
                                        <span class="size">{{$key->size_of_value}}</span>
                                        <span class="inventory">{{$key->quantity}} Sản phẩm có sẵn</span>
                                        <button class="minus" style="border: none; outline: none">-</button>
                                        <input class="quantity" value="0" style="width: 30px; border: none; outline: none; text-align: center;">
                                        <button class="plus d-flex justify-content-center align-items-center" style="border: none; outline: none">
                                            <object data="Icon/products/icon_plus.svg" width="8px"></object>
                                        </button>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="footer-container-product">
            <div class="container-atc">
                <button class="button checkout flex"
                        data-id="{{$data->id}}"
                        data-shop="{{$dataShop->id}}"
                        data-url="{{url('details_product/add_to_cart')}}"
                        data-customer=<?= isset($data_login) ? $data_login['id'] : ''?>
                >Mua ngay
                </button>
                <button class="button add-to-cart flex"
                        data-id="{{$data->id}}"
                        data-shop="{{$dataShop->id}}"
                        data-url="{{url('details_product/add_to_cart')}}"
                        data-customer=<?= isset($data_login) ? $data_login['id'] : ''?>
                >
                    <object width="20px" data="Icon/products/icon_cart.svg"></object>
                    Thêm vào giỏ hàng
                </button>
            </div>
        </div>
        <div class="mini-cart box-shadow">
        </div>
    </div>
    <div id="product-combo">
        <h2 class="title-combo">Mua kèm Deal sốc</h2>
        <div class="flex ">
            <div class="product-combos flex">
                <div class="cb-content-product">
                    <div class="cb-image-product">
                        <img src="<?php echo asset("$path/cb_product_1.png")?>">
                    </div>
                    <div class="cb-title-product">Áo croptop</div>
                    <div class="cb-price">
                        <span class="price-buy">150.000</span>
                        <span class="price-old">
                            180.000
                        </span>
                    </div>
                    <div class="icon_like_deal">
                        <object data="Icon/products/liked.svg" width="12px"></object>
                    </div>
                </div>
                <div class="icon_plus">
                    <object width="11px" data="Icon/products/icon_plus_black.svg"></object>
                </div>
                <div class="cb-content-product">
                    <div class="cb-image-product">
                        <img src="<?php echo asset("$path/cb_product_2.png")?>">
                    </div>
                    <div class="cb-title-product">Quần ống rộng</div>
                    <div class="cb-price">
                        <span class="price-buy">150.000</span>
                        <span class="price-old">
                            180.000
                        </span>
                    </div>
                    <div class="icon_like_deal">
                        <object data="Icon/products/liked.svg" width="12px"></object>
                    </div>
                </div>
                <div class="icon_plus">
                    <object width="11px" data="Icon/products/icon_plus_black.svg"></object>
                </div>
                <div class="cb-content-product">
                    <div class="cb-image-product">
                        <img src="<?php echo asset("$path/cb_product_3.png")?>">
                    </div>
                    <div class="cb-title-product">Túi đeo chéo</div>
                    <div class="cb-price">
                        <span class="price-buy">150.000</span>
                        <span class="price-old">
                            180.000
                        </span>
                    </div>
                    <div class="icon_like_deal">
                        <object data="Icon/products/liked.svg" width="12px"></object>
                    </div>
                </div>
                <div class="form-add-cart">
                    <div class="total-price">
                        <span class="text-total">Tổng:</span>
                        <span class="price-buy">450.000</span>
                    </div>
                    <div class="total-price">
                        <span class="text-total">Tiết kiệm được:</span>
                        <span class="price-buy">90.000</span>
                    </div>
                    <div class="add-cart">
                        <button class="button preventdefault">Thêm tất cả vào giỏ hàng</button>
                    </div>
                    <div class="add-wishlist">
                        <button class="button preventdefault">Thêm tất cả vào yêu thích</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="shop-love" class="flex">
        <div class="box-1 flex div-box">
            <a href="{{url('seeshop/'.$data->shop_id)}}">
                <div class="image-button">
                    <div class="iamge-shop"><img
                            src="@if($dataShop->image) {{$dataShop->image}} @else <?php echo asset("$path/shop_love.png")?> @endif">
                    </div>
                    <p class="shop">Shop yêu thích</p>
                </div>
            </a>
            <div class="infor-shop">
                <a href="{{url('seeshop/'.$data->shop_id)}}">
                    <h2 class="tite-shop"><?= isset($dataShop) ? $dataShop->name_shop : '' ?></h2>
                </a>
                <p class="sub">Online 10 phút trươc </p>
                <?php
                if (isset($dataMessenger)) {
                    $link = 'chat/messenger/' . $dataMessenger->id;
                    $url = 'chat/popup-chat/' . $dataMessenger->id;
                } else {
                    $supplier_id = isset($dataShop) ? $dataShop->customer_id : 0;
                    $link = 'chat/messenger/shop/' . $supplier_id;
                    $url = 'chat/popup-chat/shop/' . $supplier_id;
                }
                ?>
                <div class="link-button">
                    <a class="button" href="<?= URL::to($link) ?>" data-chat="<?= URL::to($url) ?>"
                       data-user-id="<?php if (isset($data_login)) echo $data_login->id ?>"
                    >
                        <object data="Icon/products/icon_chat.svg" width="12px"></object>
                        Chat Ngay
                    </a>
                    <button class="button">
                        <a href="{{url('seeshop/'.$data->shop_id)}}">
                            <object data="Icon/products/icon_shop.svg" width="12px"></object>
                            Xem Shop
                        </a>
                    </button>
                </div>
            </div>
        </div>
        <div class="box-2 div-box">
            <p class="price-buy">
                <object data="Icon/products/t-shirt.svg" width="20px"></object>
                <?= isset($dataCountProduct) ? $dataCountProduct : '0' ?>
                <span> Sản Phẩm </span>
            </p>
        </div>
        <div class="box-3 div-box">
            <p class="price-buy">
                <object data="Icon/products/favorite_2.svg" width="20px"></object>
                4.9
                <span>  Đánh Giá </span>
            </p>
        </div>
        <div class="box-4 div-box">
            <p class="price-buy">
                <object data="Icon/products/chat_bubbles_with_ellipsis.svg" width="20px"></object>
                {{$response_rate}}%
                <span> Tỉ Lệ Phản Hồi </span>
            </p>
        </div>
        <div class="box-5 div-box">
            <p class="price-buy">
                <object data="Icon/products/clock.svg" width="20px"></object>
                {{$dataShop->time_feedback}}
                <span> Thời Gian Phản Hồi </span>
            </p>
        </div>
    </div>
    <div class="container">
        <div class="row position-relative">
            <div class="col-lg-9">
                <div id="product-details" class="box-shadow">
                    <h2 class="title-details">Chi tiết sản phẩm</h2>
                    <div class="container-product-details flex">
                        <div class="content-left flex">
                            <div class="text-details">
                                <p class="inventory">Danh mục:</p>
                                <p class="inventory">Thương hiệu:</p>
                                <p class="inventory">Chất liệu:</p>
                            </div>
                            <div class="text-details">
                                <p class="content-shipping color-blue"><?= isset($data) ? $data->name_category : '' ?></p>
                                <p class="content-shipping"><?= isset($data) ? $data->name_brand : 'No Brand' ?></p>
                                <p class="content-shipping"><?= isset($data) ? $data->material : '' ?></p>
                            </div>
                            <div class="content-right flex">
                                <div class="text-details">
                                    <p class="inventory">Xuất xứ:</p>
                                    <p class="inventory">Kho hàng:</p>
                                    <p class="inventory">Gửi từ:</p>
                                </div>
                                <div class="text-details">
                                    <p class="content-shipping"><?= isset($data) ? $data->original : '' ?></p>
                                    <p class="content-shipping"><?= isset($data) ? $data->warehouse : '' ?></p>
                                    <p class="content-shipping"><?= isset($data) ? $data->place : '' ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="description-product" class="box-shadow max-w">
                    <h2 class="title-details">Bài đăng mẫu</h2>
                    <div id="description-content">
                        <p style="word-wrap: break-word;">{!! $data->sort_desc !!}</p>
                    </div>
                </div>
                <div id="description-product" class="box-shadow max-w">
                    <h2 class="title-details">Mô tả sản phẩm</h2>
                    <p style="word-wrap: break-word;">{!! $data->desc !!}</p>
                </div>
                <div id="wacths-product" class="box-shadow max-w">
                    <div class="section section-san-pham-trend sp-product">
                        <?php
                        $pathSlide = "assets/frontend/Icon/home/Danh_muc";
                        $pathimage = "assets/frontend/Icon/base";
                        ?>
                        <div class="title-section">
                            <div class="content">
                                <h2 class="title-details">SẢN PHẨM ĐÃ XEM</h2>
                            </div>
                            <div class="link"><a class="preventdefault">Xem Thêm</a></div>
                        </div>
                        <div class="product-section relative">
                            <div class="container-section-product slide-product-watched">
                                <?php if(isset($dataGetSeenProduct)): ?>
                                <?php foreach ($dataGetSeenProduct as $value): ?>
                                    @php
                                        $product_value = \App\Models\AttributeValueModel::where('product_id', $value->id)->get();
                                        $price_ctv_min = \App\Models\AttributeValueModel::where('product_id', $value->id)->min('price_ctv');
                                        $price_ctv_max = \App\Models\AttributeValueModel::where('product_id', $value->id)->max('price_ctv');
                                    @endphp
                                    <?php
                                    $min = 0;
                                    $max = 0;
                                    foreach ($product_value as $item){
                                        $ctv = $item->price - $item->price_ctv;
                                        if ($min == 0){
                                            $min = $ctv;
                                        }elseif ($min > $ctv){
                                            $min = $ctv;
                                        }
                                        if ($max == 0){
                                            $max = $ctv;
                                        }elseif ($max < $ctv){
                                            $max = $ctv;
                                        }
                                    }
                                    ?>
                                <div class="content border-box relative sp-product-content border-yellow"
                                     style="width: calc((100%/4) - 17px)!important;">
                                    <a href="{{url('details_product/'.$value->id)}}">
                                        <div class="img-product">
                                            <img src="{{$value->image}}">
                                        </div>
                                        @if(isset($value->percent))
                                        <div class="container-sale">
                                            <div class="image-background-sale">
                                                <img src="<?php echo asset("$pathSlide/Vector_sale.png")?>">
                                            </div>
                                            <div class="text-sale">
                                                <p class="text-number-sale">{{$value->percent->percent}}%</p>
                                                <p class="text-sale-off">off</p>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="item-content-product justify-content-between">
                                            <div class="title-product-n relative">
                                                <span>{{$value->name}}</span>
                                            </div>
                                            <div class="product-price">
                                                <p class="price">
                                                    <span>{{number_format($value->price_discount)}} đ</span> -
                                                    <span class="price-sale">{{number_format($value->price)}} đ</span>
                                                </p>
                                                <div class="d-flex justify-content-between">
                                                    <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ - {{number_format($price_ctv_max)}}đ</p>
                                                </div>
                                                <div class="d-flex justify-content-between" style="margin-top: 10px">
                                                    <p>Lời: <span class="text-red">{{number_format($min)}}đ - {{number_format($max)}}đ</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="review">
                                                @if($value->avg_rating == 0)
                                                    <img src="<?php echo asset("$path/0_sao.png")?>">
                                                @else
                                                    @if($value->avg_rating == 1)
                                                        <img src="<?php echo asset("$path/1_sao.png")?>">
                                                    @elseif($value->avg_rating == 2)
                                                        <img src="<?php echo asset("$path/2_sao.png")?>">
                                                    @elseif($value->avg_rating == 3)
                                                        <img src="<?php echo asset("$path/3_sao.png")?>">
                                                    @elseif($value->avg_rating == 4)
                                                        <img src="<?php echo asset("$path/4_sao.png")?>">
                                                    @elseif($value->avg_rating == 5)
                                                        <img src="<?php echo asset("$path/5_sao.png")?>">
                                                    @elseif($value->avg_rating > 1 && $value->avg_rating < 2)
                                                        <img src="<?php echo asset("$path/1_5_sao.png")?>">
                                                    @elseif($value->avg_rating > 2 && $value->avg_rating < 3)
                                                        <img src="<?php echo asset("$path/2_5_sao.png")?>">
                                                    @elseif($value->avg_rating > 3 && $value->avg_rating < 4)
                                                        <img src="<?php echo asset("$path/3_5_sao.png")?>">
                                                    @elseif($value->avg_rating > 4 && $value->avg_rating < 5)
                                                        <img src="<?php echo asset("$path/4_5_sao.png")?>">
                                                    @endif
                                                @endif
                                                <p>Đã bán {{$value->is_sell}}</p>
                                            </div>
                                            <div class="review">
                                                <img style="width: 20px;"
                                                     src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                                <p>{{$value->place}}</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="review" class="box-shadow max-w">
                    <h2 class="title-details">Đánh giá</h2>
                    <div class="star-total-review flex">
                        <div class="content-left-review">
                            <h2 class="total-review">{{$average_review}}</h2>
                            @if($total_comment == 0)
                                <div class="img-star"><img src="<?php echo asset("$path/0_sao.png")?>"></div>
                            @else
                                @if($average_review == 1)
                                    <div class="img-star"><img src="<?php echo asset("$path/1_sao.png")?>"></div>
                                @elseif($average_review == 2)
                                    <div class="img-star"><img src="<?php echo asset("$path/2_sao.png")?>"></div>
                                @elseif($average_review == 3)
                                    <div class="img-star"><img src="<?php echo asset("$path/3_sao.png")?>"></div>
                                @elseif($average_review == 4)
                                    <div class="img-star"><img src="<?php echo asset("$path/4_sao.png")?>"></div>
                                @elseif($average_review == 5)
                                    <div class="img-star"><img src="<?php echo asset("$path/5_sao.png")?>"></div>
                                @elseif($average_review > 1 && $average_review < 2)
                                    <div class="img-star"><img src="<?php echo asset("$path/1_5_sao.png")?>"></div>
                                @elseif($average_review > 2 && $average_review < 3)
                                    <div class="img-star"><img src="<?php echo asset("$path/2_5_sao.png")?>"></div>
                                @elseif($average_review > 3 && $average_review < 4)
                                    <div class="img-star"><img src="<?php echo asset("$path/3_5_sao.png")?>"></div>
                                @elseif($average_review > 4 && $average_review < 5)
                                    <div class="img-star"><img src="<?php echo asset("$path/4_5_sao.png")?>"></div>
                                @endif
                            @endif
                            <p class="comment">{{$total_comment}} nhận xét</p>
                        </div>
                        <div class="content-right-review">
                            <div class="five-star flex">
                                <div class="image-star">
                                    <img src="<?php echo asset("$path/Group_five_star.png")?>">
                                </div>
                                <div class="tad-review"><p class="bg-total-review"></p></div>
                                <div class="p-total-review">{{$count_5_star}}</div>
                            </div>
                            <div class="for-star flex">
                                <div class="image-star">
                                    <img src="<?php echo asset("$path/for_star.png")?>">
                                </div>
                                <div class="tad-review"><p class="bg-total-review"></p></div>
                                <div class="p-total-review">{{$count_4_star}}</div>
                            </div>
                            <div class="three-star flex">
                                <div class="image-star">
                                    <img src="<?php echo asset("$path/three_star.png")?>">
                                </div>
                                <div class="tad-review"><p class="bg-total-review"></p></div>
                                <div class="p-total-review">{{$count_3_star}}</div>
                            </div>
                            <div class="tow-star flex">
                                <div class="image-star">
                                    <img src="<?php echo asset("$path/tow_star.png")?>">
                                </div>
                                <div class="tad-review"><p class="bg-total-review"></p></div>
                                <div class="p-total-review">{{$count_2_star}}</div>
                            </div>
                            <div class="one-star flex">
                                <div class="image-star">
                                    <img src="<?php echo asset("$path/one_star.png")?>">
                                </div>
                                <div class="tad-review"><p class="bg-total-review"></p></div>
                                <div class="p-total-review">{{$count_1_star}}</div>
                            </div>
                        </div>
                    </div>
                    @if(count($data_review))
                        <div class="filter-comment flex">
                            <button class="btn btn-ajax-review bg-red">
                                <a href={{ url( "details_product/" .$data->id ."/all_review") }}>
                                    Tất cả
                                </a>
                            </button>
                            <button class="btn btn-ajax-review">
                                <a href={{ url( "details_product/" .$data->id ."/review-video-image") }}>
                                    Có hình ảnh/ có video
                                </a>
                            </button>
                            <button class="btn btn-ajax-review">
                                <a href={{ url( "details_product/" .$data->id ."/review-comment") }}>
                                    Có bình luận
                                </a>
                            </button>
                            <button class="btn flex btn-ajax-review <?=  $count_1_star == 0 ? 'disabled' : '' ?>">
                                <a href={{ url( "details_product/" .$data->id ."/review-one-star") }}>
                                    1
                                    <object width="13px" data="Icon/products/icon_one_star.svg"></object>
                                </a>
                            </button>
                            <button class="btn flex btn-ajax-review <?=  $count_2_star == 0 ? 'disabled' : '' ?>">
                                <a href={{ url( "details_product/" .$data->id ."/review-two-star") }}>
                                    2
                                    <object width="13px" data="Icon/products/icon_one_star.svg"></object>
                                </a>
                            </button>
                            <button class="btn flex btn-ajax-review <?=  $count_3_star == 0 ? 'disabled' : '' ?>">
                                <a href={{ url( "details_product/" .$data->id ."/review-three-star") }}>3
                                    <object width="13px" data="Icon/products/icon_one_star.svg"></object>
                                </a>
                            </button>
                            <button class="btn flex btn-ajax-review <?=  $count_4_star == 0 ? 'disabled' : '' ?>">
                                <a href={{ url( "details_product/" .$data->id ."/review-four-star") }}>4
                                    <object width="13px" data="Icon/products/icon_one_star.svg"></object>
                                </a>
                            </button>
                            <button class="btn flex btn-ajax-review <?=  $count_5_star == 0 ? 'disabled' : '' ?>">
                                <a href={{ url( "details_product/" .$data->id ."/review-five-star") }}>5
                                    <object width="13px" data="Icon/products/icon_one_star.svg"></object>
                                </a>
                            </button>
                        </div>
                        <div class="coantainer-review">
                            <h2 class="title-details">Đánh giá sản phẩm</h2>
                            <div id="review-content"></div>
                        </div>
                        <div id="data-review"
                             data-total-review="{{$total_comment}}"
                             data-five-star="{{$count_5_star}}"
                             data-four-star="{{$count_4_star}}"
                             data-three-star="{{$count_3_star}}"
                             data-two-star="{{$count_2_star}}"
                             data-one-star="{{$count_1_star}}"
                             style="display: none !important;"
                        ></div>
                        <div class="show-video-review d-flex justify-content-lg-center align-items-center">

                        </div>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 position-absolute text-center scroll-width-none"
                 style="top: 0; right: 0; overflow: auto; height: calc(100% - 20px);">
                <div class="card box-shadow">
                    <div class="card-body">
                        <h2 class="title-details">SẢN PHẨM BÁN CHẠY</h2>
                        <div class="list_best_seller">
                            @if($data_seller && count($data_seller))
                                @foreach ($data_seller as $value)
                                    @php
                                        $product_value = \App\Models\AttributeValueModel::where('product_id', $value->id)->get();
                                        $price_ctv_min = \App\Models\AttributeValueModel::where('product_id', $value->id)->min('price_ctv');
                                        $price_ctv_max = \App\Models\AttributeValueModel::where('product_id', $value->id)->max('price_ctv');
                                    @endphp
                                    <?php
                                    $min = 0;
                                    $max = 0;
                                    foreach ($product_value as $item){
                                        $ctv = $item->price - $item->price_ctv;
                                        if ($min == 0){
                                            $min = $ctv;
                                        }elseif ($min > $ctv){
                                            $min = $ctv;
                                        }
                                        if ($max == 0){
                                            $max = $ctv;
                                        }elseif ($max < $ctv){
                                            $max = $ctv;
                                        }
                                    }
                                    ?>
                                    <div class="content border-box w-100 relative sp-product-content"
                                         style="margin-bottom: 30px">
                                        <a href="{{url('details_product/'.$value->id)}}" class="w-100">
                                            <div class="img-product m-0">
                                                <img style="width: 237px!important; height: 237px!important;"
                                                     src="{{$value->image}}" class="lazy">
                                            </div>
                                            <div class="item-content-product justify-content-between">
                                                <div class="title-product-n relative">
                                                    <p class="card-title">{{$value->name}}</p>
                                                </div>
                                                <div class="product-price">
                                                    <p class="price">
                                                        <span>{{number_format($value->price_discount)}} đ</span> -
                                                        <span
                                                            class="price-sale">{{number_format($value->price)}} đ</span>
                                                    </p>
                                                    <div class="d-flex justify-content-between">
                                                        <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ - {{number_format($price_ctv_max)}}đ</p>
                                                    </div>
                                                    <div class="d-flex justify-content-between" style="margin-top: 10px">
                                                        <p>Lời: <span class="text-red">{{number_format($min)}}đ - {{number_format($max)}}đ</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="review">
                                                    @if($value->avg_rating == 0)
                                                        <img src="<?php echo asset("$path/0_sao.png")?>">
                                                    @else
                                                        @if($value->avg_rating == 1)
                                                            <img src="<?php echo asset("$path/1_sao.png")?>">
                                                        @elseif($value->avg_rating == 2)
                                                            <img src="<?php echo asset("$path/2_sao.png")?>">
                                                        @elseif($value->avg_rating == 3)
                                                            <img src="<?php echo asset("$path/3_sao.png")?>">
                                                        @elseif($value->avg_rating == 4)
                                                            <img src="<?php echo asset("$path/4_sao.png")?>">
                                                        @elseif($value->avg_rating == 5)
                                                            <img src="<?php echo asset("$path/5_sao.png")?>">
                                                        @elseif($value->avg_rating > 1 && $value->avg_rating < 2)
                                                            <img src="<?php echo asset("$path/1_5_sao.png")?>">
                                                        @elseif($value->avg_rating > 2 && $value->avg_rating < 3)
                                                            <img src="<?php echo asset("$path/2_5_sao.png")?>">
                                                        @elseif($value->avg_rating > 3 && $value->avg_rating < 4)
                                                            <img src="<?php echo asset("$path/3_5_sao.png")?>">
                                                        @elseif($value->avg_rating > 4 && $value->avg_rating < 5)
                                                            <img src="<?php echo asset("$path/4_5_sao.png")?>">
                                                        @endif
                                                    @endif
                                                    <p>Đã bán {{$value->is_sell}}</p>
                                                </div>
                                                <div class="review">
                                                    <img style="width: 20px;"
                                                         src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                                    <p>{{$value->place}}</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            @else
                                <p class="text-bold-700 text-center">Không có dữ liệu !</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="wacths-product" class="box-shadow max-w">
        <div class="section section-san-pham-trend sp-product">
            <?php
            $pathSlide = "assets/frontend/Icon/home/Danh_muc";
            $pathimage = "assets/frontend/Icon/base";
            ?>
            <div class="title-section">
                <div class="content">
                    <h2 class="title-details">SẢN PHẨM KHÁC CỦA SHOP</h2>
                </div>
                <div class="link"><a class="preventdefault">Xem Thêm</a></div>
            </div>
            <div class="product-section relative">
                <div class="container-section-product slide-more-product">
                    <?php if(isset($dataOrtherProduct)): ?>
                    <?php foreach($dataOrtherProduct as $value) :?>
                        @php
                            $product_value = \App\Models\AttributeValueModel::where('product_id', $value->id)->get();
                            $price_ctv_min = \App\Models\AttributeValueModel::where('product_id', $value->id)->min('price_ctv');
                            $price_ctv_max = \App\Models\AttributeValueModel::where('product_id', $value->id)->max('price_ctv');
                        @endphp
                        <?php
                        $min = 0;
                        $max = 0;
                        foreach ($product_value as $item){
                            $ctv = $item->price - $item->price_ctv;
                            if ($min == 0){
                                $min = $ctv;
                            }elseif ($min > $ctv){
                                $min = $ctv;
                            }
                            if ($max == 0){
                                $max = $ctv;
                            }elseif ($max < $ctv){
                                $max = $ctv;
                            }
                        }
                        ?>
                    <a href="{{url("details_product/$value->id")}}"
                       class="content border-box relative sp-product-content border-yellow">
                        <div class="img-product">
                            <img src="{{$value->image}}">
                        </div>
                        @if(isset($value->percent))
                            <div class="container-sale">
                                <div class="image-background-sale">
                                    <img src="<?php echo asset("$pathSlide/Vector_sale.png")?>">
                                </div>
                                <div class="text-sale">
                                    <p class="text-number-sale">{{$value->percent->percent}}%</p>
                                    <p class="text-sale-off">off</p>
                                </div>
                            </div>
                        @endif
                        <div class="item-content-product justify-content-between">
                            <div class="title-product-n relative">
                                <p class="name_product">{{$value->name}}</p>
                            </div>
                            <div class="container-price d-flex flex-column justify-content-between">
                                <div class="product-price">
                                    <p class="price"><span>{{number_format($value->price_discount)}} đ</span> -
                                        <span class="price-sale">{{number_format($value->price)}} đ</span></p>
                                    <div class="d-flex justify-content-between">
                                        <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ - {{number_format($price_ctv_max)}}đ</p>
                                    </div>
                                    <div class="d-flex justify-content-between" style="margin-top: 10px">
                                        <p>Lời: <span class="text-red">{{number_format($min)}}đ - {{number_format($max)}}đ</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="review">
                                    @if($value->avg_rating == 0)
                                        <img src="<?php echo asset("$path/0_sao.png")?>">
                                    @else
                                        @if($value->avg_rating == 1)
                                            <img src="<?php echo asset("$path/1_sao.png")?>">
                                        @elseif($value->avg_rating == 2)
                                            <img src="<?php echo asset("$path/2_sao.png")?>">
                                        @elseif($value->avg_rating == 3)
                                            <img src="<?php echo asset("$path/3_sao.png")?>">
                                        @elseif($value->avg_rating == 4)
                                            <img src="<?php echo asset("$path/4_sao.png")?>">
                                        @elseif($value->avg_rating == 5)
                                            <img src="<?php echo asset("$path/5_sao.png")?>">
                                        @elseif($value->avg_rating > 1 && $value->avg_rating < 2)
                                            <img src="<?php echo asset("$path/1_5_sao.png")?>">
                                        @elseif($value->avg_rating > 2 && $value->avg_rating < 3)
                                            <img src="<?php echo asset("$path/2_5_sao.png")?>">
                                        @elseif($value->avg_rating > 3 && $value->avg_rating < 4)
                                            <img src="<?php echo asset("$path/3_5_sao.png")?>">
                                        @elseif($value->avg_rating > 4 && $value->avg_rating < 5)
                                            <img src="<?php echo asset("$path/4_5_sao.png")?>">
                                        @endif
                                    @endif
                                    <p>Đã bán {{$value->is_sell}}</p>
                                </div>
                                <div class="review">
                                    <img style="width: 20px;" src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                    <p>{{$value->place}}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div id="wacths-product" class="box-shadow max-w">
        <div class="section section-san-pham-trend sp-product">
            <?php
            $pathSlide = "assets/frontend/Icon/home/Danh_muc";
            $pathimage = "assets/frontend/Icon/base";
            ?>
            <div class="title-section">
                <div class="content">
                    <h2 class="title-details">SẢN PHẨM TƯƠNG TỰ</h2>
                </div>
                <div class="link"><a class="preventdefault">Xem Thêm</a></div>
            </div>
            <div class="product-section relative">
                <div class="container-section-product slide-similar-product">
                    <?php if(isset($dataSimilarProduct)): ?>
                    <?php foreach($dataSimilarProduct as $value) :?>
                        @php
                            $product_value = \App\Models\AttributeValueModel::where('product_id', $value->id)->get();
                            $price_ctv_min = \App\Models\AttributeValueModel::where('product_id', $value->id)->min('price_ctv');
                            $price_ctv_max = \App\Models\AttributeValueModel::where('product_id', $value->id)->max('price_ctv');
                        @endphp
                        <?php
                        $min = 0;
                        $max = 0;
                        foreach ($product_value as $item){
                            $ctv = $item->price - $item->price_ctv;
                            if ($min == 0){
                                $min = $ctv;
                            }elseif ($min > $ctv){
                                $min = $ctv;
                            }
                            if ($max == 0){
                                $max = $ctv;
                            }elseif ($max < $ctv){
                                $max = $ctv;
                            }
                        }
                        ?>
                    <a href="{{url("details_product/$value->id")}}"
                       class="content border-box relative sp-product-content border-yellow">
                        <div class="img-product">
                            <img src="{{$value->image}}">
                        </div>
                        @if(isset($value->percent))
                            <div class="container-sale">
                                <div class="image-background-sale">
                                    <img src="<?php echo asset("$pathSlide/Vector_sale.png")?>">
                                </div>
                                <div class="text-sale">
                                    <p class="text-number-sale">{{$value->percent->percent}}%</p>
                                    <p class="text-sale-off">off</p>
                                </div>
                            </div>
                        @endif
                        <div class="item-content-product justify-content-between">
                            <div class="title-product-n relative">
                                <p class="name_product">{{$value->name}}</p>
                            </div>
                            <div class="product-price">
                                <p class="price"><span>{{number_format($value->price_discount)}} đ</span> -
                                    <span class="price-sale">{{number_format($value->price)}} đ</span></p>
                                <div class="d-flex justify-content-between">
                                    <p class="pass">Giá CTV: {{number_format($price_ctv_min)}}đ - {{number_format($price_ctv_max)}}đ</p>
                                </div>
                                <div class="d-flex justify-content-between" style="margin-top: 10px">
                                    <p>Lời: <span class="text-red">{{number_format($min)}}đ - {{number_format($max)}}đ</span>
                                    </p>
                                </div>
                            </div>
                            <div class="review">
                                @if($value->avg_rating == 0)
                                    <img src="<?php echo asset("$path/0_sao.png")?>">
                                @else
                                    @if($value->avg_rating == 1)
                                        <img src="<?php echo asset("$path/1_sao.png")?>">
                                    @elseif($value->avg_rating == 2)
                                        <img src="<?php echo asset("$path/2_sao.png")?>">
                                    @elseif($value->avg_rating == 3)
                                        <img src="<?php echo asset("$path/3_sao.png")?>">
                                    @elseif($value->avg_rating == 4)
                                        <img src="<?php echo asset("$path/4_sao.png")?>">
                                    @elseif($value->avg_rating == 5)
                                        <img src="<?php echo asset("$path/5_sao.png")?>">
                                    @elseif($value->avg_rating > 1 && $value->avg_rating < 2)
                                        <img src="<?php echo asset("$path/1_5_sao.png")?>">
                                    @elseif($value->avg_rating > 2 && $value->avg_rating < 3)
                                        <img src="<?php echo asset("$path/2_5_sao.png")?>">
                                    @elseif($value->avg_rating > 3 && $value->avg_rating < 4)
                                        <img src="<?php echo asset("$path/3_5_sao.png")?>">
                                    @elseif($value->avg_rating > 4 && $value->avg_rating < 5)
                                        <img src="<?php echo asset("$path/4_5_sao.png")?>">
                                    @endif
                                @endif
                                <p>Đã bán {{$value->is_sell}}</p>
                            </div>
                            <div class="review">
                                <img style="width: 20px;" src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                <p>{{$value->place}}</p>
                            </div>
                        </div>
                    </a>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="before"></div>
    <div class="checked">
        <div class="image">
            <img src="<?php echo asset("$path/icon_checked.png")?>">
        </div>
        <p class="alert">Sản phẩm được thêm vào giỏ hàng</p>
    </div>
    <div class="mini-cart mini-cart-2 box-shadow">
    </div>
</div>

