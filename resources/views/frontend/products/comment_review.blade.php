<div class="review-content">
    <div class="all-content-review">
        @foreach($data_comment as $v)
            <div class="content-review">
                <div class="image-customer">
                    <img style="height: 65px!important;" @if($v['avatar_customer'] != null) src="{{$v['avatar_customer']}} @else src='Icon/user/avatar.png' @endif ">
                </div>
                <div class="infor-customer">
                    <h2 class="title-details name-customer">{{$v['name_customer']}}</h2>
                    <div class="review-star">
                        <?php if ($v['ratings'] == 1) :?>
                        <img src="Icon/products/one_star.png">
                        <?php elseif ($v['ratings'] == 2) :?>
                        <img src="Icon/products/tow_star.png">
                        <?php elseif ($v['ratings'] == 3) :?>
                        <img src="Icon/products/three_star.png">
                        <?php elseif ($v['ratings'] == 4) :?>
                        <img src="Icon/products/for_star.png">
                        <?php else: ?>
                        <img src="Icon/products/star_review.png">
                        <?php endif; ?>
                    </div>
                    <p class="sub-title-description classify-review"> {{$v['category_product']}} </p>
                    <p class="content-shipping content-comment">
                        @if($v['comment'] != 'undefined')
                            {{$v['comment']}}
                        @endif
                        <span class="comment_customer" style="display: block">{{$v['customer_comment']}}</span>
                    </p>
                    <p class="time-review color-gray font-weight-bold" style="font-size: 12px; color: #999999;margin-top: 10px">{{$v->created_at}}</p>
                    @if($v['status_reply'] == 1)
                        <div class="p-1 mt-3 ml-4">
                            <div class="d-flex align-items-center">
                                <img width="40px" height="40px" class="mr-2" style="border-radius: 50%"
                                    @if($v['shop_image'] != null) src="{{$v['shop_image']}}" @else src='Icon/user/avatar.png' @endif
                                     alt="">
                                <span style="font-size: 16px" class="text-bold-700 m-0">{{$v['shop_name']}}</span>
                            </div>
                            <div class="content_rep" style="margin-left: 48px">
                                <p style="font-size: 15px">{{$v['reply']}}</p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</div>
<div class="paginate pagination paginate-review">
    {{ $data_comment->render("pagination::bootstrap-4") }}
</div>
