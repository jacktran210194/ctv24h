<div class="review-content">
    <div class="all-content-review">
        @foreach($data_video_image as $v)
            <div class="content-review">
                <div class="image-customer">
                    <img style="height: 65px!important;" @if($v['avatar_customer'] != null) src="{{$v['avatar_customer']}} @else src='Icon/user/avatar.png'@endif ">
                </div>
                <div class="infor-customer">
                    <h2 class="title-details name-customer">{{$v['name_customer']}}</h2>
                    <div class="review-star">

                        <?php if ($v['ratings'] == 1) :?>
                        <img src="Icon/products/one_star.png">
                        <?php elseif ($v['ratings'] == 2) :?>
                        <img src="Icon/products/tow_star.png">
                        <?php elseif ($v['ratings'] == 3) :?>
                        <img src="Icon/products/three_star.png">
                        <?php elseif ($v['ratings'] == 4) :?>
                        <img src="Icon/products/for_star.png">
                        <?php else: ?>
                        <img src="Icon/products/star_review.png">
                        <?php endif; ?>
                    </div>
                    <p class="sub-title-description classify-review"> {{$v['category_product']}} </p>
                    <p class="content-shipping content-comment">
                        @if($v['comment'] != 'undefined')
                            {{$v['comment']}}
                        @endif
                        <span style="display: block">{{$v['customer_comment']}}</span>
                    </p>
                    <div>
                        <div class="image-comment">
                            @if($v['is_image'] == 1)
                                @if(strpos($v['video_review'], ".mp4") == true)
                                    <div class="relative" style="margin-right: 10px;">
                                        <video class="video-comment">
                                            <source src="{{$v['video_review']}}">
                                        </video>
                                        <div class="icon-play-video d-flex align-items-center justify-content-lg-center"
                                             data-src="{{$v['video_review']}}"
                                        >
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M7.99998 0C3.58172 0 0 3.58172 0 7.99998C0 12.4183 3.58172 16 7.99998 16C12.4183 16 16 12.4183 16 7.99998C15.9953 3.58369 12.4163 0.00472098 7.99998 0ZM11.3691 8.25488C11.3137 8.36598 11.2237 8.45604 11.1125 8.51142V8.51427L6.54113 10.8C6.25881 10.9411 5.91562 10.8266 5.77453 10.5443C5.73442 10.464 5.71376 10.3754 5.71426 10.2857V5.71429C5.71413 5.39869 5.96983 5.14275 6.28543 5.14259C6.37419 5.14255 6.46175 5.16318 6.54113 5.20285L11.1125 7.48858C11.395 7.62934 11.5099 7.97243 11.3691 8.25488Z" fill="white"/>
                                            </svg>
                                        </div>
                                    </div>
                                @else
                                    <img class="image-preview" src="{{$v['image_review']}}">
                                @endif
                            @endif
                        </div>
                        <p class="time-review color-gray font-weight-bold" style="font-size: 12px; color: #999999;margin-top: 10px">{{$v->created_at}}</p>
                    </div>
                    @if($v['status_reply'] == 1)
                        <div class="p-1 mt-3 ml-4">
                            <div class="d-flex align-items-center">
                                <img width="40px" height="40px" class="mr-2" style="border-radius: 50%"
                                     @if($v['shop_image'] != null) src="{{$v['shop_image']}}" @else src='Icon/user/avatar.png' @endif
                                     alt="">
                                <span style="font-size: 16px" class="text-bold-700 m-0">{{$v['shop_name']}}</span>
                            </div>
                            <div class="content_rep" style="margin-left: 48px">
                                <p style="font-size: 15px">{{$v['reply']}}</p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</div>
<div class="paginate pagination paginate-review">
    {{ $data_video_image->render("pagination::bootstrap-4") }}
</div>
