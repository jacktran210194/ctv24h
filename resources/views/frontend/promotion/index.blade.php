<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/promotion/style.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
    @include('frontend.base.header')
    <!-- END: Header-->

    <div class="p-0 bg-main">
        <img src="Icon/promotion/banner.png" class="w-100">
        <div class="m-5 d-flex justify-content-center">
            <object data="Icon/promotion/voucher_nganh_hang.svg" width="30%"></object>
        </div>
        <div class="container">
            <div class="slick-voucher">
                <object data="Icon/promotion/electro.svg"></object>
                <object data="Icon/promotion/electro.svg"></object>
                <object data="Icon/promotion/electro.svg"></object>
                <object data="Icon/promotion/electro.svg"></object>
                <object data="Icon/promotion/electro.svg"></object>
                <object data="Icon/promotion/electro.svg"></object>
            </div>
        </div>
        <div class="m-5 d-flex justify-content-center">
            <object data="Icon/promotion/voucher_san.svg" width="30%"></object>
        </div>
        <div class="container pb-5">
            <div class="row">
                <div class="col-lg-6 d-flex justify-content-lg-end align-items-center">
                    <img src="Icon/promotion/voucher.png">
                </div>
                <div class="col-lg-6 d-flex justify-content-lg-start align-items-center">
                    <img src="Icon/promotion/voucher.png">
                </div>
                <div class="col-lg-6 d-flex justify-content-lg-end align-items-center">
                    <img src="Icon/promotion/voucher.png">
                </div>
                <div class="col-lg-6 d-flex justify-content-lg-start align-items-center">
                    <img src="Icon/promotion/voucher.png">
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Footer-->
    @include('frontend.base.footer')
    <!-- END: Footer-->
</div>
@include('frontend.base.script')
<script src="js/promotion/index.js"></script>
</body>
<!-- END: Body-->
</html>
