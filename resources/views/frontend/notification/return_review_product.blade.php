<div class="container-popup-review d-flex justify-content-lg-center align-items-center">
    <div class="popup-review">
        <h3 class="title">Đánh giá sản phẩm </h3>
        <div class="product-review d-flex align-items-center">
            <div
                class="image d-flex justify-content-lg-center align-items-center"
                style="max-width: 117px">
                <img style="width: 100%!important;"
                     src="{{$data->product_image}}">
            </div>
            <div class="title-product">
                <h4>{{$data->product_name}}</h4>
                {{$data->category_product}}
            </div>
        </div>
        <div
            class="d-flex justify-content-lg-center align-items-center ratings">
            <button class="btn btn-star-rating @if($data['ratings'] >= 1) active @endif" data-ratings="1">
                <svg xmlns="http://www.w3.org/2000/svg" width="28"
                     height="27" viewBox="0 0 28 27" fill="none">
                    <path
                        d="M27.6992 10.3578C27.5224 9.8111 27.0375 9.42279 26.4638 9.37108L18.6708 8.66348L15.5893 1.45084C15.3621 0.922242 14.8446 0.580078 14.2697 0.580078C13.6947 0.580078 13.1773 0.922242 12.95 1.45207L9.8685 8.66348L2.07433 9.37108C1.50165 9.42402 1.01796 9.8111 0.840185 10.3578C0.662408 10.9045 0.82659 11.5042 1.25981 11.8822L7.15034 17.0483L5.41336 24.6997C5.28626 25.2623 5.50462 25.8438 5.97141 26.1812C6.22232 26.3625 6.51587 26.4548 6.81189 26.4548C7.06712 26.4548 7.32029 26.386 7.54751 26.25L14.2697 22.2324L20.9894 26.25C21.4811 26.5458 22.1009 26.5188 22.5667 26.1812C23.0337 25.8428 23.2518 25.261 23.1247 24.6997L21.3878 17.0483L27.2783 11.8832C27.7115 11.5042 27.8769 10.9056 27.6992 10.3578Z"
                        fill="#ccc"/>
                </svg>
            </button>
            <button class="btn btn-star-rating @if($data['ratings'] >= 2) active @endif" data-ratings="2">
                <svg xmlns="http://www.w3.org/2000/svg" width="28"
                     height="27" viewBox="0 0 28 27" fill="none">
                    <path
                        d="M27.6992 10.3578C27.5224 9.8111 27.0375 9.42279 26.4638 9.37108L18.6708 8.66348L15.5893 1.45084C15.3621 0.922242 14.8446 0.580078 14.2697 0.580078C13.6947 0.580078 13.1773 0.922242 12.95 1.45207L9.8685 8.66348L2.07433 9.37108C1.50165 9.42402 1.01796 9.8111 0.840185 10.3578C0.662408 10.9045 0.82659 11.5042 1.25981 11.8822L7.15034 17.0483L5.41336 24.6997C5.28626 25.2623 5.50462 25.8438 5.97141 26.1812C6.22232 26.3625 6.51587 26.4548 6.81189 26.4548C7.06712 26.4548 7.32029 26.386 7.54751 26.25L14.2697 22.2324L20.9894 26.25C21.4811 26.5458 22.1009 26.5188 22.5667 26.1812C23.0337 25.8428 23.2518 25.261 23.1247 24.6997L21.3878 17.0483L27.2783 11.8832C27.7115 11.5042 27.8769 10.9056 27.6992 10.3578Z"
                        fill="#ccc"/>
                </svg>
            </button>
            <button class="btn btn-star-rating @if($data['ratings'] >= 3) active @endif" data-ratings="3">
                <svg xmlns="http://www.w3.org/2000/svg" width="28"
                     height="27" viewBox="0 0 28 27" fill="none">
                    <path
                        d="M27.6992 10.3578C27.5224 9.8111 27.0375 9.42279 26.4638 9.37108L18.6708 8.66348L15.5893 1.45084C15.3621 0.922242 14.8446 0.580078 14.2697 0.580078C13.6947 0.580078 13.1773 0.922242 12.95 1.45207L9.8685 8.66348L2.07433 9.37108C1.50165 9.42402 1.01796 9.8111 0.840185 10.3578C0.662408 10.9045 0.82659 11.5042 1.25981 11.8822L7.15034 17.0483L5.41336 24.6997C5.28626 25.2623 5.50462 25.8438 5.97141 26.1812C6.22232 26.3625 6.51587 26.4548 6.81189 26.4548C7.06712 26.4548 7.32029 26.386 7.54751 26.25L14.2697 22.2324L20.9894 26.25C21.4811 26.5458 22.1009 26.5188 22.5667 26.1812C23.0337 25.8428 23.2518 25.261 23.1247 24.6997L21.3878 17.0483L27.2783 11.8832C27.7115 11.5042 27.8769 10.9056 27.6992 10.3578Z"
                        fill="#ccc"/>
                </svg>
            </button>
            <button class="btn btn-star-rating @if($data['ratings'] >= 4) active @endif" data-ratings="4">
                <svg xmlns="http://www.w3.org/2000/svg" width="28"
                     height="27" viewBox="0 0 28 27" fill="none">
                    <path
                        d="M27.6992 10.3578C27.5224 9.8111 27.0375 9.42279 26.4638 9.37108L18.6708 8.66348L15.5893 1.45084C15.3621 0.922242 14.8446 0.580078 14.2697 0.580078C13.6947 0.580078 13.1773 0.922242 12.95 1.45207L9.8685 8.66348L2.07433 9.37108C1.50165 9.42402 1.01796 9.8111 0.840185 10.3578C0.662408 10.9045 0.82659 11.5042 1.25981 11.8822L7.15034 17.0483L5.41336 24.6997C5.28626 25.2623 5.50462 25.8438 5.97141 26.1812C6.22232 26.3625 6.51587 26.4548 6.81189 26.4548C7.06712 26.4548 7.32029 26.386 7.54751 26.25L14.2697 22.2324L20.9894 26.25C21.4811 26.5458 22.1009 26.5188 22.5667 26.1812C23.0337 25.8428 23.2518 25.261 23.1247 24.6997L21.3878 17.0483L27.2783 11.8832C27.7115 11.5042 27.8769 10.9056 27.6992 10.3578Z"
                        fill="#ccc"/>
                </svg>
            </button>
            <button class="btn btn-star-rating @if($data['ratings'] == 5) active @endif" data-ratings="5">
                <svg xmlns="http://www.w3.org/2000/svg" width="28"
                     height="27" viewBox="0 0 28 27" fill="none">
                    <path
                        d="M27.6992 10.3578C27.5224 9.8111 27.0375 9.42279 26.4638 9.37108L18.6708 8.66348L15.5893 1.45084C15.3621 0.922242 14.8446 0.580078 14.2697 0.580078C13.6947 0.580078 13.1773 0.922242 12.95 1.45207L9.8685 8.66348L2.07433 9.37108C1.50165 9.42402 1.01796 9.8111 0.840185 10.3578C0.662408 10.9045 0.82659 11.5042 1.25981 11.8822L7.15034 17.0483L5.41336 24.6997C5.28626 25.2623 5.50462 25.8438 5.97141 26.1812C6.22232 26.3625 6.51587 26.4548 6.81189 26.4548C7.06712 26.4548 7.32029 26.386 7.54751 26.25L14.2697 22.2324L20.9894 26.25C21.4811 26.5458 22.1009 26.5188 22.5667 26.1812C23.0337 25.8428 23.2518 25.261 23.1247 24.6997L21.3878 17.0483L27.2783 11.8832C27.7115 11.5042 27.8769 10.9056 27.6992 10.3578Z"
                        fill="#ccc"/>
                </svg>
            </button>
        </div>
        <div class="content-comment">
            <div
                class="comment d-flex flex-wrap justify-content-lg-center align-items-center">
                <button class="btn btn-comment @if($data['comment'] == 'Chất lượng sản phẩm
                                                                    tuyệt vời') selected @endif">Chất lượng sản phẩm
                    tuyệt vời
                </button>
                <button class="btn btn-comment @if($data['comment'] == 'Đóng gói sản phẩm rất
                                                                    đẹp và chắc chắn') selected @endif">Đóng gói sản
                    phẩm rất đẹp và chắc chắn
                </button>
                <button class="btn btn-comment @if($data['comment'] == 'Shop phục vụ rất tốt') selected @endif">Shop
                    phục vụ rất tốt
                </button>
                <button class="btn btn-comment @if($data['comment'] == 'Rất đáng tiền') selected @endif">Rất đáng tiền
                </button>
                <button class="btn btn-comment @if($data['comment'] == 'Thời gian giao hàng rất
                                                                    nhanh') selected @endif">Thời gian giao hàng rất
                    nhanh
                </button>
            </div>
        </div>
        <div
            class="note-review d-flex justify-content-lg-between align-items-center">
            <div id="textarea">
                <textarea id="comment_customer">{{$data['customer_comment']}}</textarea>
            </div>
            <div
                class="form-upload d-flex justify-content-center align-content-center">
                <input type="file" id="myFile" name="filename"
                       accept="video/*,image/*"
                       onchange="readURL(this);">
                <div
                    class="upload-files d-flex justify-content-lg-center align-items-center">
                    @if($data['image_review'] && $data['is_image'])
                        <div style="display: none;!important;" class="btn icon-upload">
                            <img src="Icon/chat/upload.png">
                            <p>Tải ảnh lên</p>
                        </div>
                        <img style="display: block!important;" id="blah" src="{{$data['image_review']}}"/>
                        <video controls autoplay muted id="video-tag">
                            <source id="video-source" src="">
                        </video>
                    @elseif($data['video_review'] && $data['is_image'])
                        <div style="display: none!important;" class="btn icon-upload">
                            <img src="Icon/chat/upload.png">
                            <p>Tải ảnh lên</p>
                        </div>
                        <img id="blah" src=""/>
                        <video style="display: block!important;" controls autoplay muted id="video-tag">
                            <source id="video-source" src="{{$data['video_review']}}">
                        </video>
                    @else
                        <div style="" class="btn icon-upload">
                            <img src="Icon/chat/upload.png">
                            <p>Tải ảnh lên</p>
                        </div>
                        <img id="blah" src=""/>
                        <video controls autoplay muted id="video-tag">
                            <source id="video-source" src="">
                        </video>
                    @endif
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-lg-end align-items-center">
            @if($data->status==0)
                <button class="btn btn-bottom btn-back-review">Trở lại</button>
                <button class="btn btn-bottom btn-up-review ml-2"
                        data-update-review="{{url('notification/review-product/save')}}"
                        data-id="{{$data->id}}"
                        name="uploadclick">Cập nhật
                </button>
            @else
                <button class="btn btn-bottom btn-back-review">Trở lại</button>
            @endif

        </div>
    </div>
</div>

