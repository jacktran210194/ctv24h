<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
{{--<link rel="stylesheet" type="text/css" href="css/home/style.css">--}}
<link rel="stylesheet" type="text/css" href="css/notification/notification.css">


<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <div class="container">
        <div class="row">
            @include('frontend.user.menu')

            <div class="col-lg-9 p-0 pr-3 desc-noti">
                @if(count($data))
                    @foreach($data as $value)
                        <div id="notification" class="bg-white d-flex">
                            <div class="float-left img-noti">
                                <img width="102px" height="105px"
                                     src="{{$value->image}}"
                                     alt="">
                            </div>
                            <div class="info-noti">
                                <div class="title-noti">
                                    <div class="button float-right">
                                        <p>{{date('H:i:s d-m-Y', strtotime($value->created_at))}}</p>
                                    </div>
                                    <div class="pl-4">
                                        <h3>{{$value->title}}</h3>
                                        <p>{!! $value->message !!}</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="border-bottom"></div>
                    @endforeach
                @else
                    <h5 class="text-center">Không có dữ liệu</h5>
                @endif
            </div>

        </div>
    </div>
</div>
@include('frontend.base.footer')


@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
