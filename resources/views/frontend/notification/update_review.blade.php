<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
{{--<link rel="stylesheet" type="text/css" href="css/home/style.css">--}}
<link rel="stylesheet" type="text/css" href="css/notification/notification.css">


<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <div class="container">
        <div class="row">
            @include('frontend.user.menu')
            <div class="col-lg-9 p-0 pr-3 desc-noti">
                @if(count($data))
                    @foreach($data as $value)
                        <div id="notification" class="bg-white d-flex">
                            <div class="float-left img-noti">
                                <img width="102px" height="105px"
                                     src="{{$value->product_image}}"
                                     alt="">
                            </div>
                            <div class="info-noti">
                                <div class="title-noti">
                                    <div class="button float-right">
                                        <p>{{date('H:i:s d-m-Y', strtotime($value->date))}}</p>
                                        <button data-id="{{$value->id}}"
                                                data-url="{{url('notification/return_review_product/'.$value->id)}}"
                                                class="btn btn-main btn_return_review">Xem lại đánh giá
                                        </button>
                                    </div>
                                    <div class="pl-4" style="width:90%;">
                                        <h3>Bạn đã đánh giá sản phẩm: <a
                                                href="{{url('details_product/'.$value->product_id)}}">{{$value->product_name}}</a>
                                        </h3>
                                        <p>Bạn đã đánh giá sản phẩm: <a
                                                href="{{url('details_product/'.$value->product_id)}}">{{$value->product_name}}</a>.
                                            Hãy đánh giá người mua hàng</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="border-bottom"></div>
                    @endforeach
                @else
                    <h5 class="text-center">Không có dữ liệu</h5>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="show-review"></div>
@include('frontend.base.footer')
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
<script>
    $(document).ready(function () {
        let ratings_star;
        $(document).on("click", ".upload-files", function (ev) {
            $("#myFile").click();
        });
        $(document).on('click','.btn-comment',function () {
            $(".btn-comment").removeClass("selected");
            $(this).addClass("selected");
        });
        $(document).on('click', '.btn-back-review', function () {
            $(".container-popup-review").removeClass("show-popup");
        });
        //xem lại
        $(document).on('click','.btn_return_review',function () {
            let url = $(this).attr('data-url');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $(".show-review").html(data);
                    $('.container-popup-review').addClass("show-popup");
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        $(document).on("click", ".btn-star-rating", function () {
            ratings_star = $(this).attr("data-ratings");
            let rating = $(this).attr("data-ratings");
            $(".btn-star-rating").each(function () {
                var ratings = $(this).attr("data-ratings");
                if (ratings <= rating) {
                    $(this).addClass("active");
                } else {
                    $(this).removeClass("active");
                }
            });
        });

        $(document).on("click", ".btn-up-review", function () {
            let commemt_customer = $('#comment_customer').val();
            let comment = $('.btn-comment.selected').text();
            if (ratings_star == null) {
                alert("Bạn chưa chọn đánh giá")
            } else if (comment == null && commemt_customer === "") {
                alert("Bạn chưa bình luận")
            } else {
                let id = $(this).attr('data-id')
                let url = $(this).attr("data-update-review");
                let form_data = new FormData();
                form_data.append('id',id);
                form_data.append('image', $('#myFile')[0].files[0]);
                form_data.append('ratings', ratings_star);
                form_data.append('comment', comment);
                form_data.append('customer_comment', commemt_customer);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    type: "POST",
                    data: form_data,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.status) {
                            swal("Thông báo", data.msg, "success");
                            window.location.href = data.url;
                        } else {
                            swal("Thông báo", data.msg, "error");
                        }
                    },
                    error: function () {
                        console.log('error')
                    },
                });
            }
        });
    });
    function readURL(input) {
        const videoSrc = document.querySelector("#video-source");
        const videoTag = document.querySelector("#video-tag");
        $('#video-tag').css('display', 'none');
        $('#blah').css('display', 'none');
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                if (input.files[0]["type"] == "video/mp4") {
                    videoSrc.src = e.target.result;
                    videoTag.load();
                    $('.icon-upload').css('display', 'none');
                    $('#video-tag').css('display', 'block');
                } else {
                    $('#blah')
                        .attr('src', e.target.result)
                        .css('display', 'block');
                    $('.icon-upload').css('display', 'none');
                }
            }.bind(this)
            reader.readAsDataURL(input.files[0]);
        }
    }

</script>

