<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/notification/notification.css">
<link rel="stylesheet" type="text/css" href="css/notification/noti.scss">


<!-- END: Head-->
<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <div class="container">
        <div class="row">
            @include('frontend.user.menu')
            <div class="col-lg-9 p-0 pr-3 desc-noti">
                @if(count($data) && $data)
                    @foreach($data as $value)
                        <div id="notification" class="bg-white d-flex">
                            <div class="float-left img-noti">
                                <img width="102px" height="105px"
                                     src="{{$value->image_product}}"
                                     alt="">
                            </div>
                            <div class="info-noti">
                                <div class="title-noti">
                                    <div class="button float-right">
                                        <p>{{$value->created_at}}</p>
                                        <a href="{{url('search_order?order_code='.$value->order_code)}}">
                                            <button class="btn btn-main">Xem chi tiết đơn hàng</button>
                                        </a>
                                        <br>
                                        <br>
                                        <br>
                                        @if($value->is_read == true)
                                            <h5><span class="badge badge-success float-right">Đã đọc</span></h5>
                                        @else
                                            <h5><span class="badge badge-secondary float-right">Chưa đọc</span></h5>
                                        @endif
                                    </div>
                                    <div class="pl-4">
                                        <h3>Đơn hàng: {{$value->order_code}} - {{$value->content}}</h3>
                                        <p>Đơn hàng {{$value->order_code}} đang ở trong trạng
                                            thái {{$value->content}}</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="border-bottom"></div>
                    @endforeach
                @else
                    <h3>Không có thông báo nào !</h3>
                @endif
{{--                <div class="paginate pagination paginate-review justify-content-end">--}}
{{--                    {{ $data->render("pagination::bootstrap-4") }}--}}
{{--                </div>--}}
                <div class="paginate-review">
                    <div class="paginate pagination justify-content-end">
                        {{ $data->render("pagination::bootstrap-4") }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('frontend.base.footer')


@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
