<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
{{--<link rel="stylesheet" type="text/css" href="css/home/style.css">--}}
<link rel="stylesheet" type="text/css" href="css/notification/notification.css">


<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
@include('frontend.base.header')
<div id="main">
    <div class="container">
        <div class="row">
            @include('frontend.user.menu')
            <div class="col-lg-9 p-0 pr-3 desc-noti">
                <div id="notification" class="bg-white d-flex">
                    <div class="float-left img-noti">
                        <img width="102px" height="105px"
                             src="Icon/cart/image_product.png"
                             alt="">
                    </div>
                    <div class="info-noti">
                        <div class="title-noti">
                            <div class="button float-right">
                                <p>19:26 19-11-2020</p>
                                <button class="btn btn-main">Xem chi tiết</button>
                            </div>
                            <div class="pl-4">
                                <h3>Tung Voucher Trùm Cuối 100K</h3>
                                <p>Vui lòng chọn "Đã nhận hàng" cho đơn 76328JH31231G.
                                    Nếu bạn hài lòng tất cả sản phẩm và không có nhu cầu trả
                                    hàng/Hoàn tiền. Đánh giá ngay và nhận 100 xu</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="border-bottom"></div>
                <div id="notification" class="bg-white d-flex">
                    <div class="float-left img-noti">
                        <img width="102px" height="105px"
                             src="Icon/cart/image_product.png"
                             alt="">
                    </div>
                    <div class="info-noti">
                        <div class="title-noti">
                            <div class="button float-right">
                                <p>19:26 19-11-2020</p>
                                <button class="btn btn-default">Xem chi tiết</button>
                            </div>
                            <div class="pl-4">
                                <h3>Tết Sale: Hàng Thương Hiệu Chỉ Từ 8K</h3>
                                <p>Vui lòng chọn "Đã nhận hàng" cho đơn 76328JH31231G.
                                    Nếu bạn hài lòng tất cả sản phẩm và không có nhu cầu trả
                                    hàng/Hoàn tiền. Đánh giá ngay và nhận 100 xu</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="border-bottom"></div>
                <div id="notification" class="bg-white d-flex">
                    <div class="float-left img-noti">
                        <img width="102px" height="105px"
                             src="Icon/cart/image_product.png"
                             alt="">
                    </div>
                    <div class="info-noti">
                        <div class="title-noti">
                            <div class="button float-right">
                                <p>19:26 19-11-2020</p>
                                <button class="btn btn-default">Xem chi tiết</button>
                            </div>
                            <div class="pl-4">
                                <h3>Tung Voucher Trùm Cuối 100K</h3>
                                <p>Vui lòng chọn "Đã nhận hàng" cho đơn 76328JH31231G.
                                    Nếu bạn hài lòng tất cả sản phẩm và không có nhu cầu trả
                                    hàng/Hoàn tiền. Đánh giá ngay và nhận 100 xu</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="border-bottom"></div>
                <div id="notification" class="bg-white d-flex">
                    <div class="float-left img-noti">
                        <img width="102px" height="105px"
                             src="Icon/cart/image_product.png"
                             alt="">
                    </div>
                    <div class="info-noti">
                        <div class="title-noti">
                            <div class="button float-right">
                                <p>19:26 19-11-2020</p>
                                <button class="btn btn-default">Xem chi tiết</button>
                            </div>
                            <div class="pl-4">
                                <h3>Tiến Hành Giao</h3>
                                <p>Vui lòng chọn "Đã nhận hàng" cho đơn 76328JH31231G.
                                    Nếu bạn hài lòng tất cả sản phẩm và không có nhu cầu trả
                                    hàng/Hoàn tiền. Đánh giá ngay và nhận 100 xu</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="border-bottom"></div>
                <div id="notification" class="bg-white d-flex">
                    <div class="float-left img-noti">
                        <img width="102px" height="105px"
                             src="Icon/cart/image_product.png"
                             alt="">
                    </div>
                    <div class="info-noti">
                        <div class="title-noti">
                            <div class="button float-right">
                                <p>19:26 19-11-2020</p>
                                <button class="btn btn-default">Xem chi tiết</button>
                            </div>
                            <div class="pl-4">
                                <h3>Xem Loạt Deal Cực Hot</h3>
                                <p>Vui lòng chọn "Đã nhận hàng" cho đơn 76328JH31231G.
                                    Nếu bạn hài lòng tất cả sản phẩm và không có nhu cầu trả
                                    hàng/Hoàn tiền. Đánh giá ngay và nhận 100 xu</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="border-bottom"></div>
                <div id="notification" class="bg-white d-flex">
                    <div class="float-left img-noti">
                        <img width="102px" height="105px"
                             src="Icon/cart/image_product.png"
                             alt="">
                    </div>
                    <div class="info-noti">
                        <div class="title-noti">
                            <div class="button float-right">
                                <p>19:26 19-11-2020</p>
                                <button class="btn btn-default">Xem chi tiết</button>
                            </div>
                            <div class="pl-4">
                                <h3>Sale Tết: Freeship 0Đ</h3>
                                <p>Vui lòng chọn "Đã nhận hàng" cho đơn 76328JH31231G.
                                    Nếu bạn hài lòng tất cả sản phẩm và không có nhu cầu trả
                                    hàng/Hoàn tiền. Đánh giá ngay và nhận 100 xu</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="border-bottom"></div>
                <div id="notification" class="bg-white d-flex">
                    <div class="float-left img-noti">
                        <img width="102px" height="105px"
                             src="Icon/cart/image_product.png"
                             alt="">
                    </div>
                    <div class="info-noti">
                        <div class="title-noti">
                            <div class="button float-right">
                                <p>19:26 19-11-2020</p>
                                <button class="btn btn-default">Xem chi tiết</button>
                            </div>
                            <div class="pl-4">
                                <h3>Tết Sale: Hàng Thương Hiệu Chỉ Từ 8K</h3>
                                <p>Vui lòng chọn "Đã nhận hàng" cho đơn 76328JH31231G.
                                    Nếu bạn hài lòng tất cả sản phẩm và không có nhu cầu trả
                                    hàng/Hoàn tiền. Đánh giá ngay và nhận 100 xu</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('frontend.base.footer')


@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
