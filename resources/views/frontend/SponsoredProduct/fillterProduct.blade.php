<div class="suggested-for-you">
    <?php
    $path = "assets/frontend/Icon/products";
    $pathNew = "assets/frontend/Icon/home/Danh_muc";
    ?>
    <div class="body-search flex container">
        <div class="fillter">
            <div class="title-fillter flex">
                <div class="icon_fillter">
                    <object data="icon/search/icon_fillter.svg" width="12px"></object>
                </div>
                <div class="title-text">BỘ LỌC TÌM KIẾM</div>
            </div>
            <div class="filter-catagory">
                <p class="title">Theo danh mục</p>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Áo khoác dù <span class="number">(17)</span></p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Quần short <span class="number">(17)</span></p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Kính mát <span class="number">(17)</span></p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Áo thun <span class="number">(17)</span></p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Kính thời trang <span class="number">(17)</span></p>
                </div>
                <p class="load-more">Xem Thêm ></p>
            </div>
            <div class="filter-catagory">
                <p class="title">Nơi bán</p>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Hà Nội</p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">TP.Hồ Chí Minh</p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Thái nguyên</p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Vĩnh Phúc</p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Hải Phòng </p>
                </div>
                <p class="load-more">Xem Thêm ></p>
            </div>
            <div class="filter-catagory">
                <p class="title">Đơn vị vận chuyển</p>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">NowShip</p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Ninja Van</p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">J&T Express</p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Giao hàng tiết kiệm</p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Giao hàng nhanh</p>
                </div>
                <p class="load-more">Xem Thêm ></p>
            </div>
            <div class="filter-catagory form-fillter">
                <p class="title">Khoảng giá</p>
                <div class="form-fillter-price flex">
                    <input type="text" placeholder="TỪ">
                    <span class="icon-space">-</span>
                    <input type="text" placeholder="ĐẾN">
                </div>
                <button class="btn">Áp dụng</button>
            </div>
            <div class="filter-catagory">
                <p class="title">Loại shop</p>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Shopee Mall</p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Shop yêu thích</p>
                </div>
            </div>
            <div class="filter-catagory">
                <p class="title">Tình trạng</p>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Sản phẩm mới </p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Sản phẩm đã từng </p>
                </div>
            </div>
            <div class="filter-catagory">
                <p class="title">Đánh giá </p>
                <div class="flex content">
                    <object data="icon/search/icon_star_five.svg"></object>
                </div>
                <div class="flex content">
                    <object data="icon/search/icon_star_for.svg"></object>
                    <p class="text">trở lên </p>
                </div>
                <div class="flex content">
                    <object data="icon/search/icon_star_three.svg"></object>
                    <p class="text">trở lên </p>
                </div>
                <div class="flex content">
                    <object data="icon/search/icon_star_tow.svg"></object>
                    <p class="text">trở lên </p>
                </div>
                <div class="flex content">
                    <object data="icon/search/icon_star_one.svg"></object>
                    <p class="text">trở lên </p>
                </div>
            </div>
            <div class="filter-catagory">
                <p class="title">Dịch vụ và khuyến mãi</p>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Freeship Xtra </p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Hoàn xu Xtra </p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Đang giảm giá </p>
                </div>
                <div class="flex content">
                    <input type="checkbox">
                    <p class="text">Miễn phí vận chuyển </p>
                </div>
                <p class="load-more">Thêm
                    <object width="7px" data="icon/search/icon_arrow_down.svg"></object>
                </p>
            </div>
            <button class="btn clear-all">Xóa tất cả</button>
        </div>
        <div class="container-left">
            <div class="product-search">
                <?php
                $pathSlide = "assets/frontend/Icon/home/Danh_muc";
                ?>
                <div class="bg-white container-section-product">
                    @isset($data_product)
                        @foreach($data_product as $value)
                            <a href="{{url('details_product/'.$value->id)}}">
                                <div class="content border-box">
                                    <div class="img-product">
                                        <img width="185px" height="190px" src="{{$value->image}}">
                                    </div>
                                    <div class="title-product-n relative">
                                        <span class="card-title">{{$value->name}}</span>
                                    </div>
                                    <div class="product-price">
                                        <p class="price">{{number_format($value->price_discount)}} đ - {{$value->price}}
                                            đ</p>
                                        <p class="pass">Giá CTV: ******** <span>Lời: 50.000</span></p>
                                        <div class="form btn-sign-up relative">
                                            <a href="#">Đăng ký CTV để xem giá</a>
                                        </div>
                                    </div>
                                    <div class="review">
                                        <img src="<?php echo asset("$pathSlide/icon-review.png")?>">
                                        <p>Đã bán {{$value->is_sell}}</p>
                                    </div>
                                    <div class="review">
                                        <img style="width: 20px;" src="<?php echo asset("$pathSlide/icon-like.png")?>">
                                        <p>{{$value->warehouse}}</p>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    @endisset
                </div>
            </div>
        </div>
    </div>
</div>
