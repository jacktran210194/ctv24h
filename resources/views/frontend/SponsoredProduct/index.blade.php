<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/SponsoredProduct/style.css">
<link rel="stylesheet" type="text/css" href="css/search/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->
    <?php
    $path = "assets/frontend/Icon/suggestion";
    ?>
    <div class="container body-page">
        <div class="banner">
            <img src="<?php echo asset("$path/banner_2.png") ?>">
        </div>
        <div class="ncc-highlight">
            <div class="title-ncc">
                <div class="image-title"><img src="<?php echo asset("$path/image_titlte.png") ?>"></div>
                <h2 class="top-ncc">TOP NCC NỔI BẬT </h2>
            </div>
        </div>
        <div class="container-ncc">
            <div class="slick-voucher">
                @if(isset($data_supplier))
                    @foreach($data_supplier as $value)
                        <div class="shop-detail bg-white" style="">
                            <div class="shop-shop" style="">
                                <div class="d-flex info">
                                    <a href="">
                                        <img class="avt-shop" height="53px" width="53px" src="{{$value->image}}" alt=""
                                             style="">
                                    </a>
                                    <div class="info-shop pl-3 d-flex">
                                        <div>
                                            <h4 class="text-name">{{$value->name}}</h4>
                                            <span class="text-address">89 Đặng Văn Bi, P. Trường Thọ, Quận Thủ Đức
                                            </span>
                                        </div>
                                        <button class="btn btn-default btn-follow"
                                                style="">+ Theo dõi
                                        </button>

                                    </div>

                                </div>
                                <div class="content-gallery-product d-flex">
                                    <img width="100%" height="310px" style="object-fit: cover" class=""
                                         src="{{$value->image}}"
                                         alt="">
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        @include("frontend.SponsoredProduct.fillterProduct")
    </div>
    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
<script src="js/SponsoredProduct/index.js"></script>
</body>
<!-- END: Body-->
</html>
