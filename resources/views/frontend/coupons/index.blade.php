<?php
$dataLogin = Session::get('data_customer');
if($dataLogin){
    $dataSupplier = \App\Models\SupplierModel::where('customer_id', $dataLogin->id)->first();
    $dataCollaborator = \App\Models\CollaboratorModel::where('customer_id', $dataLogin->id)->first();
}
use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/coupons/style.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="container pb-5">
        <img src="Icon/coupons/banner.png">
        <div class="d-flex justify-content-center mt-5">
            <img src="Icon/coupons/content.png">
        </div>
        <div class="d-flex justify-content-center mt-5 position-relative">
            <object data="Icon/coupons/bg-coupons.svg" width="50%"></object>
            <div class="title-content">
                Mã dành cho nCC
            </div>
        </div>
        <div class="d-flex mt-5">
            @if(isset($dataVoucherNCC))
                @foreach($dataVoucherNCC as $value)
                    <div class="col-lg-4 d-flex">
                        <div class="bg-white shadow-offset p-3">
                            <div class="d-flex">
                                <div style="position: relative">
                                    <img src="Icon/coupons/coupon.png">
                                    <div style="position: absolute; bottom: .9rem; left: .65rem; object-fit: cover">
                                        <img
                                            style="width: 4.8rem; height: 4.8rem; border-radius: 5px; border: 1px solid #60C6CF"
                                            src="{{$value->image}}">
                                    </div>
                                </div>
                                <div class="d-flex flex-column justify-content-between pl-3">
                                    <span class="text-bold-600 font-size-12">{{$value->title}}</span>
                                    <span class="text-red">{{$value->content}} </span>
                                    <span class="">Hạn sử dụng: {{date('d-m-Y', strtotime($value->time_end))}} </span>
                                </div>
                            </div>
                            <?php
                                if(isset($dataSupplier)){
                                    $dataCheck = \App\Models\CustomerVoucherCtv24hModel::where('customer_id', $dataSupplier->customer_id)->where('voucher_id', $value->id)->first();
                                }
                            ?>
                            <?php if(!isset($dataCheck)) :?>
                                <a @if(!isset($dataSupplier)) href="{{url('login')}}" @endif class="btn-received-code"
                                   data-id="{{$value->id}}"
                                   data-url="<?= URL::to('coupons/save')?>"
                                >
                                    <div class="w-100 btn btn-save-voucher btn-main text-bold-600 mt-3">
                                        Nhận mã
                                    </div>
                                </a>
                            <?php else: ?>
                                <a class="btn-received-code"
                                   data-id="{{$value->id}}"
                                   data-url="<?= URL::to('coupons/save')?>"
                                >
                                    <div class="w-100 btn btn-save-voucher btn-grey text-bold-600 mt-3">
                                        Đã nhận mã
                                    </div>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="d-flex justify-content-center mt-5 position-relative">
            <object data="Icon/coupons/bg-coupons.svg" width="50%"></object>
            <div class="title-content">
                Mã dành cho CTV
            </div>
        </div>
        <div class="d-flex mt-5">
            <div class="col-lg-3 d-flex">
                <div class="coupon-ctv-24">
                    <div class="header-cpctv24 p-5 d-flex flex-column justify-content-around align-items-center">
                        <span class="pb-5 text-bold-600 font-size-12">Mã giảm giá</span>
                        <img src="Icon/coupons/ctv24.png">
                    </div>
                    <div class="p-4 d-flex flex-column">
                        <span class="text-bold-600 font-size-12"><?= isset($dataVoucherCTVLeft) ? $dataVoucherCTVLeft->title : '' ?> <?= isset($dataVoucherCTVLeft) ? $dataVoucherCTVLeft->discount_value : '' ?><?= isset($dataVoucherCTVLeft) && $dataVoucherCTVLeft->money_or_percent == 0 ? 'đ' : '%' ?></span>
                        <span class="font-italic pt-3">Giảm tối đa <?= isset($dataVoucherCTVLeft) ? $dataVoucherCTVLeft->money_max/1000 : '' ?>K</span>
                        <span class="font-italic pt-3">Có hiệu lực từ <?= isset($dataVoucherCTVLeft) ? $dataVoucherCTVLeft->time_start : '' ?></span>
                        <div class="d-flex justify-content-between pt-3 align-items-center">
                            <?php if(isset($dataCollaborator))
                                $dataCheckCTV = \App\Models\CustomerVoucherCtv24hModel::where('customer_id', $dataCollaborator['customer_id'])->where('voucher_id', $dataVoucherCTVLeft['id'])->first();
                            ?>
                            <?php if(!isset($dataCheckCTV)): ?>
                                <a class="btn_save_voucher_ctv_left"
                                   data-id="<?= isset($dataVoucherCTVLeft) ? $dataVoucherCTVLeft->id : '' ?>"
                                   data-url="<?= URL::to('coupons/save_coupons_ctv_left') ?>"
                                >
                                    <div class="btn btn-main text-bold-600 pl-4 pr-4 btn_voucher_ctv_left">Lưu</div>
                                </a>
                            <?php else: ?>
                                <a class="btn_save_voucher_ctv_left"
                                   data-id="<?= isset($dataVoucherCTVLeft) ? $dataVoucherCTVLeft->id : '' ?>"
                                   data-url="<?= URL::to('coupons/save_coupons_ctv_left') ?>"
                                >
                                    <div class="btn btn-grey text-bold-600 pl-4 pr-4 btn_voucher_ctv_left">Đã lưu</div>
                                </a>
                            <?php endif; ?>
                            <div class="font-italic color-blue">Điều Kiện</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 d-flex">
                <div class="row">
                    {{--                <div class="d-flex flex-column">--}}
                    @if($dataVoucherCTV)
                        @foreach($dataVoucherCTV as $val)
                            <div class="col-lg-6">
                                <div class="item-coupon d-flex w-100 position-relative">
                                    <img src="Icon/coupons/item.png" class="w-100">
                                    <div class="position-absolute w-100 d-flex">
                                        <div class="col-lg-3 d-flex flex-column justify-content-around align-items-center p-3">
                                            <object data="Icon/coupons/electro.svg"></object>
                                            <span class="text-white pt-3 text-center">{{$val->category}} </span>
                                        </div>
                                        <div class="col-lg-9 p-0 p-3 d-flex flex-column justify-content-between">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="btn btn-red font-large-1">Tối đa {{$val->money_max/1000}}K</div>
                                                <span>{{$val->title . ' '. $val->discount_value . $val->money_or_percent}}</span>
                                                <span class="color-blue">Dùng ngay</span>
                                            </div>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <span class="font-italic">Tối đa {{$val->coin_max/1000}}K xu</span>
                                            </div>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <span class="font-large-2" style="color: #FF7A00;">Sắp hết hạn: Còn 8 giờ </span>
                                                <span style="cursor: pointer" class="color-blue font-large-2">Điều kiện</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            {{--                <div class="col-lg-6 d-flex flex-column justify-content-between">--}}
            {{--                    <div class="item-coupon d-flex w-100 position-relative">--}}
            {{--                        <img src="Icon/coupons/item.png" class="w-100">--}}
            {{--                        <div class="position-absolute w-100 d-flex">--}}
            {{--                            <div class="col-lg-3 d-flex flex-column justify-content-around align-items-center p-3">--}}
            {{--                                <object data="Icon/coupons/delivery_1.svg"></object>--}}
            {{--                                <span class="text-white pt-3">Tiêu Dùng </span>--}}
            {{--                            </div>--}}
            {{--                            <div class="col-lg-9 p-0 p-3 d-flex flex-column justify-content-between">--}}
            {{--                                <div class="d-flex justify-content-between align-items-center">--}}
            {{--                                    <div class="btn btn-red font-large-1">Tối đa 20K</div>--}}
            {{--                                    <span>Hoàn 50% Xu</span>--}}
            {{--                                    <span class="color-blue">Dùng ngay</span>--}}
            {{--                                </div>--}}
            {{--                                <div class="d-flex justify-content-between align-items-center">--}}
            {{--                                    <span class="font-italic">Tối đa 250k xu</span>--}}
            {{--                                </div>--}}
            {{--                                <div class="d-flex justify-content-between align-items-center">--}}
            {{--                                    <span class="font-large-2" style="color: #FF7A00;">Sắp hết hạn: Còn 8 giờ </span>--}}
            {{--                                    <span class="color-blue font-large-2">Điều kiện</span>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                    <div class="item-coupon d-flex w-100 position-relative">--}}
            {{--                        <img src="Icon/coupons/item.png" class="w-100">--}}
            {{--                        <div class="position-absolute w-100 d-flex">--}}
            {{--                            <div class="col-lg-3 d-flex flex-column justify-content-around align-items-center p-3">--}}
            {{--                                <object data="Icon/coupons/high_heels.svg"></object>--}}
            {{--                                <span class="text-white pt-3">Giày Cao Gót  </span>--}}
            {{--                            </div>--}}
            {{--                            <div class="col-lg-9 p-0 p-3 d-flex flex-column justify-content-between">--}}
            {{--                                <div class="d-flex justify-content-between align-items-center">--}}
            {{--                                    <div class="btn btn-red font-large-1">Tối đa 20K</div>--}}
            {{--                                    <span>Hoàn 50% Xu</span>--}}
            {{--                                    <span class="color-blue">Dùng ngay</span>--}}
            {{--                                </div>--}}
            {{--                                <div class="d-flex justify-content-between align-items-center">--}}
            {{--                                    <span class="font-italic">Tối đa 250k xu</span>--}}
            {{--                                </div>--}}
            {{--                                <div class="d-flex justify-content-between align-items-center">--}}
            {{--                                    <span class="font-large-2" style="color: #FF7A00;">Sắp hết hạn: Còn 8 giờ </span>--}}
            {{--                                    <span class="color-blue font-large-2">Điều kiện</span>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                    <div class="item-coupon d-flex w-100 position-relative">--}}
            {{--                        <img src="Icon/coupons/item.png" class="w-100">--}}
            {{--                        <div class="position-absolute w-100 d-flex">--}}
            {{--                            <div class="col-lg-3 d-flex flex-column justify-content-around align-items-center p-3">--}}
            {{--                                <object data="Icon/coupons/electro.svg"></object>--}}
            {{--                                <span class="text-white pt-3">Điện Tử </span>--}}
            {{--                            </div>--}}
            {{--                            <div class="col-lg-9 p-0 p-3 d-flex flex-column justify-content-between">--}}
            {{--                                <div class="d-flex justify-content-between align-items-center">--}}
            {{--                                    <div class="btn btn-red font-large-1">Tối đa 20K</div>--}}
            {{--                                    <span>Hoàn 50% Xu</span>--}}
            {{--                                    <span class="color-blue">Dùng ngay</span>--}}
            {{--                                </div>--}}
            {{--                                <div class="d-flex justify-content-between align-items-center">--}}
            {{--                                    <span class="font-italic">Tối đa 250k xu</span>--}}
            {{--                                </div>--}}
            {{--                                <div class="d-flex justify-content-between align-items-center">--}}
            {{--                                    <span class="font-large-2" style="color: #FF7A00;">Sắp hết hạn: Còn 8 giờ </span>--}}
            {{--                                    <span class="color-blue font-large-2">Điều kiện</span>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
        </div>
    </div>
</div>
<!-- END: Cart-->

<!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
<script src="js/coupons/index.js"></script>
</body>
<!-- END: Body-->
</html>
