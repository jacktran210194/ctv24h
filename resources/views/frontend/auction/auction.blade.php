<?php
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/auction/auction.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="container">
        <div class="main-auction mt-3 mb-5">
            <div class="d-flex menu-tab align-items-center">
                <div class="tab col-lg-3">
                    <h3 class="p-2">ĐẤU GIÁ</h3>
                </div>

                <div class="col-lg-9 d-flex">
                    <div class="tab-menu align-items-center p-3 col-lg-3 <?= $type === 'follow' ? 'checked' : '' ?>">
                        <center>
                            <a href="<?= URL::to('auction?type=follow') ?>" >Danh Sách Theo Dõi</a>
                        </center>
                    </div>

                    <div class="tab-menu align-items-center p-3 col-lg-3 <?= $type === 'going' ? 'checked' : '' ?>">
                        <center>
                            <a href="<?= URL::to('auction?type=going') ?>" >Đang Đấu Giá</a>
                        </center>
                    </div>
                    <div class="tab-menu align-items-center p-3 col-lg-3 <?= $type === 'done' ? 'checked' : '' ?>">
                    <center>
                        <a href="<?= URL::to('auction?type=done') ?>" >Đã Đấu Giá</a>
                    </center>
                </div>
                <div class="tab-menu align-items-center p-3 col-lg-3 <?= $type === 'success' ? 'checked' : '' ?>">
                <center>
                    <a href="<?= URL::to('auction?type=success') ?>" >Đấu Giá Thành Công</a>
                </center>
            </div>
        </div>
    </div>
    <?php if($type=== 'follow' || $type=== 'going') :?>
        <div class="card-product">
            <?php for($j = 0; $j < 4; $j++) :?>
            <div class="d-flex w-100 mt-3">
                <?php for ($i = 0; $i < 6; $i++): ?>
                <div class="col-lg-2 p-0 pr-1">
                    <div class="card">
                        <img class="card-img-1" src="http://chapp.test:8080/assets/frontend/Icon/home/Danh_muc/dau_gia_icon_1.png" >
                        <img class="card-img-2 pr-1 pt-1" src="http://chapp.test:8080/assets/frontend/Icon/home/Danh_muc/dau_gia_icon_2.png" >
                        <img class="card-img-3" src="Icon/chitietdaugia/dau_gia_image.png" width="100%">
                        <div class="card-body p-2 pt-4">
                            <h5 class="card-title">Áo thun nữ</h5>
                            <table class="table-responsive-lg" width="100%">
                                <tr class="black">
                                    <td>Giá Khởi Điểm:</td>
                                    <td>1.000</td>
                                </tr>
                                <tr class="red">
                                    <td>Thầu Hiện Tại:</td>
                                    <td>30.000</td>
                                </tr>
                                <tr class="black">
                                    <td>Mua Ngay:</td>
                                    <td>37.000</td>
                                </tr>
                                <tr class="red">
                                    <td>Số Người Đấu Giá:</td>
                                    <td>10</td>
                                </tr>
                                <tr class="black">
                                    <td>Bước Giá:</td>
                                    <td>2.000</td>
                                </tr>
                                <tr class="red">
                                    <td>Thời Gian Còn Lại:</td>
                                    <td>10:00:00</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <?php endfor; ?>
            </div>
            <?php endfor ;?>
        </div>
    <?php elseif($type === 'done') :?>
        <?php for($i = 0; $i < 2; $i++) :?>
        <div class="mt-3">
        <div class="item-product mb-3">
            <div class="d-flex item-product-top p-0">
                <div class="col-lg-2 pt-2 pl-3 pb-2">
                    <img src="Icon/chitietdaugia/product1.svg" class="img-product">
                </div>
                <div class="col-lg-10 pr-5 d-flex flex-column d-flex justify-content-center">
                    <div>
                        <div class="float-left bookmark-wrapper d-flex align-items-center pb-2">
                            <span class="name-product">Áo khoác lót lông hot trend 2020, chuẩn hàng quảng châu loại 1 sẵn DN </span>
                        </div>
                        <div class="float-right d-flex align-items-center">
                            <span class="turn-auction">4 lượt đấu</span>
                            <span class="price">700.000đ</span>
                        </div>
                    </div>
                    <div>
                        <div class="float-left bookmark-wrapper d-flex align-items-center">
                            <span class="type">Phân loại: Xanh - M</span>
                        </div>
                        <div class="float-right bookmark-wrapper d-flex align-items-center">
                            <span class="status">THẤT BẠI</span>
                            <object data="Icon/chitietdaugia/trophy3.svg" width="20" height="20"></object>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-product-bottom pt-2 pr-5">
                <div class="float-right bookmark-wrapper d-flex align-items-center">
                    <a class="btn btn-main">
                        <span class="auction-again align-item-center">Đấu Giá Lại</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
        <?php endfor ;?>

        <?php for($i = 0; $i < 2; $i++) :?>
        <div class="mt-3">
            <div class="item-product mb-3">
                <div class="d-flex item-product-top p-0">
                    <div class="col-lg-2 pt-2 pl-3 pb-2">
                        <img src="Icon/chitietdaugia/product2.svg" class="img-product">
                    </div>
                    <div class="col-lg-10 pr-5 d-flex flex-column d-flex justify-content-center">
                        <div>
                            <div class="float-left bookmark-wrapper d-flex align-items-center pb-2">
                                <span class="name-product2">Áo khoác lót lông hot trend 2020, chuẩn hàng quảng châu loại 1 sẵn DN </span>
                            </div>
                            <div class="float-right d-flex align-items-center">
                                <span class="turn-auction2">4 lượt đấu</span>
                                <span class="price2">700.000đ</span>
                            </div>
                        </div>
                        <div>
                            <div class="float-left bookmark-wrapper d-flex align-items-center">
                                <span class="type">Phân loại: Xanh - M</span>
                            </div>
                            <div class="float-right bookmark-wrapper d-flex align-items-center">
                                <span class="status2">CHIẾN THẮNG</span>
                                <object data="Icon/chitietdaugia/trophy3.svg" width="20" height="20"></object>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item-product-bottom pt-2">
                    <div class="float-left bookmark-wrapper d-flex align-items-center pb-2 pl-3">
                        <span class="notification">Bạn đã chiến thắng trong phiên đấu giá sản phẩm này. Vui lòng thanh toán ngay</span>
                    </div>
                    <div class="float-right bookmark-wrapper d-flex align-items-center pr-5">
                        <a class="btn btn-main">
                            <span class="auction-again align-item-center">Thanh Toán Ngay</span>
                        </a>
                    </div>
                    <div class="float-right bookmark-wrapper d-flex align-items-center pr-3">
                        <a class="btn btn-main-reject">
                            <span class="auction-reject align-item-center">Từ Chối Thanh Toán</span>
                        </a>
                    </div>


                </div>
            </div>
        </div>
        <?php endfor ;?>
    <?php else :?>
        <?php for($i = 0; $i < 4; $i++) :?>
        <div class="mt-3">
            <div class="item-product mb-3">
                <div class="d-flex item-product-top p-0">
                    <div class="col-lg-2 pt-2 pl-3 pb-2">
                        <img src="Icon/chitietdaugia/product2.svg" class="img-product">
                    </div>
                    <div class="col-lg-10 pr-5 d-flex flex-column d-flex justify-content-center">
                        <div>
                            <div class="float-left bookmark-wrapper d-flex align-items-center pb-2">
                                <span class="name-product2">Áo khoác lót lông hot trend 2020, chuẩn hàng quảng châu loại 1 sẵn DN </span>
                            </div>
                            <div class="float-right d-flex align-items-center">
                                <span class="turn-auction2">4 lượt đấu</span>
                                <span class="price2">700.000đ</span>
                            </div>
                        </div>
                        <div>
                            <div class="float-left bookmark-wrapper d-flex align-items-center">
                                <span class="type">Phân loại: Xanh - M</span>
                            </div>
                            <div class="float-right bookmark-wrapper d-flex align-items-center">
                                <span class="status2">CHIẾN THẮNG</span>
                                <object data="Icon/chitietdaugia/trophy3.svg" width="20" height="20"></object>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item-product-bottom pt-2">
                    <div class="float-left bookmark-wrapper d-flex align-items-center pb-2 pl-3">
                        <span class="notification">Bạn đã chiến thắng trong phiên đấu giá sản phẩm này. Vui lòng thanh toán ngay</span>
                    </div>
                    <div class="float-right bookmark-wrapper d-flex align-items-center pr-5">
                        <a class="btn btn-main">
                            <span class="auction-again align-item-center">Thanh Toán Ngay</span>
                        </a>
                    </div>
                    <div class="float-right bookmark-wrapper d-flex align-items-center pr-3">
                        <a class="btn btn-main-reject">
                            <span class="auction-reject align-item-center">Từ Chối Thanh Toán</span>
                        </a>
                    </div>


                </div>
            </div>
        </div>
        <?php endfor ;?>
    <?php endif ;?>
</div>

    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
</body>
<!-- END: Body-->
</html>
