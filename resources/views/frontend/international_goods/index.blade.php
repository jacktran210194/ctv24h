<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/international_goods/style.css">

<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="p-0 pb-5 main-ig">
        <img src="Icon/international_goods/banner.png" width="100%">
        <div class="container">
            <div class="d-flex p-5 justify-content-center position-relative">
                <img src="Icon/international_goods/item-content.png">
                <div class="position-absolute item-title position-center">
                    Hôm nay có gì
                </div>
            </div>
            <div class="slick-voucher">
                <object data="Icon/international_goods/vbn.svg"></object>
                <object data="Icon/international_goods/kgss.svg"></object>
                <object data="Icon/international_goods/spdg.svg"></object>
                <object data="Icon/international_goods/udtth.svg"></object>
                <object data="Icon/international_goods/spdg.svg"></object>
                <object data="Icon/international_goods/kgss.svg"></object>
            </div>
            <div class="d-flex p-5 justify-content-center position-relative">
                <img src="Icon/international_goods/item-content.png">
                <div class="position-absolute item-title position-center">
                    Siêu phẩm đồng giá
                </div>
            </div>
            <div class="d-flex">
                <div class="col-lg-4 p-1">
                    <img src="Icon/international_goods/10k.png" width="100%" height="100%">
                </div>
                <div class="col-lg-8 item-slick p-0 pl-2">
                    <?php for($i = 0; $i < 7;$i++): ?>
                    <div class="col-lg-3 p-1">
                        <img src="Icon/deals/product.png" width="100%">
                        <div class="p-3 bg-white d-flex flex-column">
                            <div class="text-bold-600 number-line-2">
                                Quần jean ống rộng Kèm Hình Thật...
                            </div>
                            <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                            <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                            <div class="d-flex mt-2">
                                <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                    Giá CTV: ****
                                </div>
                                <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                    Lời: 50.000
                                </div>
                            </div>
                            <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                            <div class="d-flex mt-2">
                                <div class="d-flex col-lg-6 p-0">
                                    <img src="Icon/lovely/star.png" width="100%">
                                </div>
                                <div class="d-flex justify-content-end col-lg-6 p-0">
                                    Đã bán 10
                                </div>
                            </div>
                            <div class="d-flex mt-2 align-items-center">
                                <div class="d-flex col-lg-6 p-0 align-items-center">
                                    <object data="Icon/deals/like.svg"></object>
                                </div>
                                <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                    Hà Nội
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="d-flex">
                <div class="col-lg-4 p-1">
                    <img src="Icon/international_goods/10k.png" width="100%" height="100%">
                </div>
                <div class="col-lg-8 item-slick p-0 pl-2">
                    <?php for($i = 0; $i < 7;$i++): ?>
                    <div class="col-lg-3 p-1">
                        <img src="Icon/deals/product.png" width="100%">
                        <div class="p-3 bg-white d-flex flex-column">
                            <div class="text-bold-600 number-line-2">
                                Quần jean ống rộng Kèm Hình Thật...
                            </div>
                            <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                            <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                            <div class="d-flex mt-2">
                                <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                    Giá CTV: ****
                                </div>
                                <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                    Lời: 50.000
                                </div>
                            </div>
                            <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                            <div class="d-flex mt-2">
                                <div class="d-flex col-lg-6 p-0">
                                    <img src="Icon/lovely/star.png" width="100%">
                                </div>
                                <div class="d-flex justify-content-end col-lg-6 p-0">
                                    Đã bán 10
                                </div>
                            </div>
                            <div class="d-flex mt-2 align-items-center">
                                <div class="d-flex col-lg-6 p-0 align-items-center">
                                    <object data="Icon/deals/like.svg"></object>
                                </div>
                                <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                    Hà Nội
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="d-flex">
                <div class="col-lg-4 p-1">
                    <img src="Icon/international_goods/50k.png" width="100%" height="100%">
                </div>
                <div class="col-lg-8 item-slick p-0 pl-2">
                    <?php for($i = 0; $i < 7;$i++): ?>
                    <div class="col-lg-3 p-1">
                        <img src="Icon/deals/product.png" width="100%">
                        <div class="p-3 bg-white d-flex flex-column">
                            <div class="text-bold-600 number-line-2">
                                Quần jean ống rộng Kèm Hình Thật...
                            </div>
                            <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                            <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                            <div class="d-flex mt-2">
                                <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                    Giá CTV: ****
                                </div>
                                <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                    Lời: 50.000
                                </div>
                            </div>
                            <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                            <div class="d-flex mt-2">
                                <div class="d-flex col-lg-6 p-0">
                                    <img src="Icon/lovely/star.png" width="100%">
                                </div>
                                <div class="d-flex justify-content-end col-lg-6 p-0">
                                    Đã bán 10
                                </div>
                            </div>
                            <div class="d-flex mt-2 align-items-center">
                                <div class="d-flex col-lg-6 p-0 align-items-center">
                                    <object data="Icon/deals/like.svg"></object>
                                </div>
                                <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                    Hà Nội
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="d-flex p-5 justify-content-center position-relative">
                <img src="Icon/international_goods/item-content.png">
                <div class="position-absolute item-title position-center">
                    Deal hot linh đình
                </div>
            </div>
            <div class="d-flex">
                <div class="col-lg-3">
                    <img src="Icon/international_goods/ttsd.png" width="100%">
                </div>
                <div class="col-lg-3">
                    <img src="Icon/international_goods/dtld.png" width="100%">
                </div>
                <div class="col-lg-3">
                    <img src="Icon/international_goods/dstn.png" width="100%">
                </div>
                <div class="col-lg-3">
                    <img src="Icon/international_goods/lddc.png" width="100%">
                </div>
            </div>
            <div class="d-flex p-5 justify-content-center position-relative">
                <img src="Icon/international_goods/item-content.png">
                <div class="position-absolute item-title position-center">
                    Đừng bỏ lỡ
                </div>
            </div>
            <div class="d-flex">
                <div class="col-lg-4">
                    <img src="Icon/international_goods/mkgs.png" width="100%">
                </div>
                <div class="col-lg-4">
                    <img src="Icon/international_goods/mngs.png" width="100%">
                </div>
                <div class="col-lg-4">
                    <img src="Icon/international_goods/mhhx.png" width="100%">
                </div>
            </div>
            <div class="d-flex p-5 justify-content-center position-relative">
                <img src="Icon/international_goods/item-content.png">
                <div class="position-absolute item-title position-center">
                    Ưu đãi ngành vàng
                    <br/>
                    Rộn ràng mua sắm
                </div>
            </div>
            <div class="d-flex">
                <div class="col-lg-3">
                    <img src="Icon/international_goods/ttte.png" width="100%">
                </div>
                <div class="col-lg-3">
                    <img src="Icon/international_goods/dtpk.png" width="100%">
                </div>
                <div class="col-lg-3">
                    <img src="Icon/international_goods/ncds.png" width="100%">
                </div>
                <div class="col-lg-3">
                    <img src="Icon/international_goods/tv.png" width="100%">
                </div>
            </div>
            <div class="d-flex p-5 justify-content-center position-relative">
                <img src="Icon/international_goods/item-content.png">
                <div class="position-absolute item-title position-center">
                    Ưu đãi ngành vàng
                    <br/>
                    Rộn ràng mua sắm
                </div>
            </div>
            <div class="d-flex p-5 justify-content-center">
                <img src="Icon/international_goods/hn.png" width="50%">
            </div>
            <div class="d-flex">
                <div class="w-100 item-slick-2 p-0 pl-2">
                    <?php for($i = 0; $i < 12;$i++): ?>
                    <div class="col-lg-2 p-1">
                        <img src="Icon/deals/product.png" width="100%">
                        <div class="p-3 bg-white d-flex flex-column">
                            <div class="text-bold-600 number-line-2">
                                Quần jean ống rộng Kèm Hình Thật...
                            </div>
                            <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                            <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                            <div class="d-flex mt-2">
                                <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                    Giá CTV: ****
                                </div>
                                <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                    Lời: 50.000
                                </div>
                            </div>
                            <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                            <div class="d-flex mt-2">
                                <div class="d-flex col-lg-6 p-0">
                                    <img src="Icon/lovely/star.png" width="100%">
                                </div>
                                <div class="d-flex justify-content-end col-lg-6 p-0">
                                    Đã bán 10
                                </div>
                            </div>
                            <div class="d-flex mt-2 align-items-center">
                                <div class="d-flex col-lg-6 p-0 align-items-center">
                                    <object data="Icon/deals/like.svg"></object>
                                </div>
                                <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                    Hà Nội
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="d-flex p-5 justify-content-center">
                <img src="Icon/international_goods/hh.png" width="50%">
            </div>
            <div class="d-flex">
                <div class="w-100 item-slick-2 p-0 pl-2">
                    <?php for($i = 0; $i < 12;$i++): ?>
                    <div class="col-lg-2 p-1">
                        <img src="Icon/deals/product.png" width="100%">
                        <div class="p-3 bg-white d-flex flex-column">
                            <div class="text-bold-600 number-line-2">
                                Quần jean ống rộng Kèm Hình Thật...
                            </div>
                            <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                            <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                            <div class="d-flex mt-2">
                                <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                    Giá CTV: ****
                                </div>
                                <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                    Lời: 50.000
                                </div>
                            </div>
                            <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                            <div class="d-flex mt-2">
                                <div class="d-flex col-lg-6 p-0">
                                    <img src="Icon/lovely/star.png" width="100%">
                                </div>
                                <div class="d-flex justify-content-end col-lg-6 p-0">
                                    Đã bán 10
                                </div>
                            </div>
                            <div class="d-flex mt-2 align-items-center">
                                <div class="d-flex col-lg-6 p-0 align-items-center">
                                    <object data="Icon/deals/like.svg"></object>
                                </div>
                                <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                    Hà Nội
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="d-flex p-5 justify-content-center">
                <img src="Icon/international_goods/hd.png" width="50%">
            </div>
            <div class="d-flex">
                <div class="w-100 item-slick-2 p-0 pl-2">
                    <?php for($i = 0; $i < 12;$i++): ?>
                    <div class="col-lg-2 p-1">
                        <img src="Icon/deals/product.png" width="100%">
                        <div class="p-3 bg-white d-flex flex-column">
                            <div class="text-bold-600 number-line-2">
                                Quần jean ống rộng Kèm Hình Thật...
                            </div>
                            <span class="promo-product text-red p-2 mt-2 text-center">
                            Mua 2 Giảm 5%
                        </span>
                            <span class="text-red mt-2 text-bold-600">
                           270.000 - 370.000
                        </span>
                            <div class="d-flex mt-2">
                                <div class="d-flex text-bold-600 font-large-1 col-lg-6 p-0">
                                    Giá CTV: ****
                                </div>
                                <div class="d-flex justify-content-end font-large-1 col-lg-6 p-0">
                                    Lời: 50.000
                                </div>
                            </div>
                            <span class="promo-product text-red p-2 mt-2 text-center">
                            Đăng ký CTV để xem giá
                        </span>
                            <div class="d-flex mt-2">
                                <div class="d-flex col-lg-6 p-0">
                                    <img src="Icon/lovely/star.png" width="100%">
                                </div>
                                <div class="d-flex justify-content-end col-lg-6 p-0">
                                    Đã bán 10
                                </div>
                            </div>
                            <div class="d-flex mt-2 align-items-center">
                                <div class="d-flex col-lg-6 p-0 align-items-center">
                                    <object data="Icon/deals/like.svg"></object>
                                </div>
                                <div class="d-flex justify-content-end col-lg-6 p-0 align-items-center  ">
                                    Hà Nội
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
<script src="js/international_goods/index.js"></script>
</body>
<!-- END: Body-->
</html>
