<button class="btn-address add-new-address btn-main btn-add-new-address"
    data-url="{{url("get_modal_address")}}"
    >Thêm địa chỉ mới</button>
<button class="btn-address btn-setup-address"><a href="{{url("user/address")}}">Thiết lập địa chỉ</a></button>
<?php use Illuminate\Support\Facades\URL;foreach ($address as $value): ?>
<div class="infor-location-customer select-location <?php if ($value ['default'] === 1) echo "selected" ?> ">
    <p class="check-box" data-id="<?= $value['id'] ?>"></p>
    <h3 class="name-customer"><?= $value['name'] . ' ' . $value['phone'] ?></h3>
    <p class="address-customer"><?= $value['full_address'] ?></p>
</div>
<?php endforeach; ?>
<div class="footer-btn">
    <button class="btn-address btn-finish" data-url="<?= URL::to('address/default_ajax/') ?>">Hoàn thành</button>
    <button class="btn-address btn-back">Trở Lại</button>
</div>
