<?php for ($i = 0; $i < 15; $i++): ?>
<div class="col-row-5">
    <div class="border-pr m-1">
        <img src="Icon/flashsale/product.png" width="100%">
        <div class="bg-white">
            <div class="p-3">
                <div class="text-bold-600 number-line-2">Quần jean ống rộng Kèm Hình Thật...</div>
                <div class="pt-3">
                    <span class="text-red text-bold-600 font-size-12">270.000đ </span>
                    <span class="color-8C8C8C">370.000đ </span>
                </div>
                <div class="pt-3 d-flex">
                    <div class="col-lg-8 d-flex align-items-center p-0 pr-4">
                        <div class="w-100 bg-orange-c d-flex align-items-center justify-content-center pt-2 pr-2 pb-2 position-relative">
                            <span class="color-white font-large-n position-absolute position-center" style="z-index: 100">Đã bán 10</span>
                            <div class="position-absolute bg-red-c" style="width: 20%;"></div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex justify-content-center ali">
                        <div class="btn btn-main font-large-1">
                            Mua ngay
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endfor; ?>
