<?php

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html>
<!-- BEGIN: Head-->
@include('frontend.base.head')
<link rel="stylesheet" type="text/css" href="css/flash_sale/style.css">
<!-- END: Head-->

<!-- BEGIN: Body-->
<body>
<div id="main">
    <!-- BEGIN: Header-->
@include('frontend.base.header')
<!-- END: Header-->

    <!-- BEGIN: Cart-->
    <div class="container-fluild">
        <div class="slick-flash-sale">
            <img src="Icon/flashsale/banner.png" width="100%">
            <img src="Icon/flashsale/banner.png" width="100%">
            <img src="Icon/flashsale/banner.png" width="100%">
            <img src="Icon/flashsale/banner.png" width="100%">
        </div>
        <img src="Icon/flashsale/flash_sale.png" width="100%">
        <div class="w-100 bg-white">
            <div class="container">
                <div class="d-flex">
                    <div class="col-lg-3 p-3 border-lr d-flex flex-column justify-content-center align-items-center">
                        <div class="text-red text-bold-600">09:00  | Đang Diễn Ra</div>
                        <div class="d-flex pt-2 justify-content-center align-items-center">
                            <span class="font-small-1 pr-1">Kết thúc trong:</span>
                            <div class="d-flex justify-content-center align-items-center">
                                <div class="btn-main p-1">09</div>
                                <span class="p-1">:</span>
                                <div class="btn-main p-1">09</div>
                                <span class="p-1">:</span>
                                <div class="btn-main p-1">09</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 p-3 border-lr d-flex justify-content-center align-items-center">
                        <span><span class="text-bold-600">12:00 |</span> Sắp Diễn Ra</span>
                    </div>
                    <div class="col-lg-3 p-3 border-lr d-flex justify-content-center align-items-center">
                        <span><span class="text-bold-600">15:00 |</span> Sắp Diễn Ra</span>
                    </div>
                    <div class="col-lg-3 p-3 border-lr d-flex justify-content-center align-items-center">
                        <span><span class="text-bold-600">18:00 |</span> Sắp Diễn Ra</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="d-flex pt-3 pb-3">
                <div class="col-lg-1" style="display: inline">Đang bán</div>
                <div class="w-100">
                    <div class="border-100"></div>
                </div>
            </div>
            <div class="row m-0 p-0">
                @include('frontend.flashsale.upcoming')
            </div>
        </div>
        <!-- End: checkout-->
    </div>
    <!-- END: Cart-->

    <!-- BEGIN: Footer-->
@include('frontend.base.footer')
<!-- END: Footer-->
</div>
@include('frontend.base.script')
<script src="js/flash_sale/index.js"></script>
</body>
<!-- END: Body-->
</html>
