<?php
$page = 'shop_confirm';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h3 class="mb-2">Thông tin cửa hàng</h3>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="img_shop">
                            <img width="400px" height="280px" class="image-preview border_radius"
                                 src="{{$data_shop->image}}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <label class="text-bold-700">Tên shop</label>
                        <div class="form-label-group position-relative has-icon-left">
                            <input disabled type="text" name="name" id="name" class="form-control text-bold-700"
                                   value="{{isset($data_shop) ? $data_shop['name'] : ''}}">
                            <div class="form-control-position" style="padding-left: 9px;">
                                <img width="18px" height="18px"
                                     src="../../assets/admin/app-assets/images/icon_dashboard/shop.png" alt="">
                            </div>
                        </div>

                        <label class="text-bold-700">Thuộc nhà cung cấp</label>
                        <div class="form-label-group position-relative has-icon-left">
                            <input disabled type="text" name="name" id="name" class="form-control text-bold-700"
                                   value="{{isset($data_shop) ? $data_shop['name_supplier'] : ''}}">
                            <div class="form-control-position" style="padding-left: 9px;">
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                        <label class="text-bold-700">Ngày sinh</label>
                        <div class="form-label-group position-relative has-icon-left">
                            <input disabled type="text" name="name" id="name" class="form-control text-bold-700"
                                   value="{{isset($data_shop) ? $data_shop['date_supplier'] : ''}}">
                            <div class="form-control-position" style="padding-left: 9px;">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>

                        <label class="text-bold-700">Ngày gia nhập</label>
                        <div class="form-label-group position-relative has-icon-left">
                            <input disabled type="text" name="name" id="name" class="form-control text-bold-700"
                                   value="{{isset($data_shop) ? $data_shop['date'] : ''}}">
                            <div class="form-control-position" style="padding-left: 9px;">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3 p-1">
                    <div class="col-lg-4">
                        <div class="card_id">
                            <div class="d-flex">
                                <div class="title_card">
                                    <h5 class="text-bold-700">Số CMND/CCCD</h5>
                                    <h5 class="text-bold-700">Ngày cấp</h5>
                                    <h5 class="text-bold-700">Nơi cấp</h5>
                                </div>
                                <div class="info_card ml-3">
                                    <h5>{{$data_shop->card_id}}</h5>
                                    <h5>{{$data_shop->card_date_create}}</h5>
                                    <h5>{{$data_shop->card_address}}</h5>
                                </div>
                            </div>
                            <div class="image_card mt-1">
                                <div>
                                    <h5 for="">CMND/CCCD mặt trước</h5>
                                    <img class="border_shadow image-preview" width="290px" height="179px"
                                         src="{{$data_shop->image_card_1}}" alt="">
                                </div>
                                <div class="mt-1">
                                    <h5 for="">CMND/CCCD mặt sau</h5>
                                    <img class="border_shadow image-preview" width="290px" height="179px"
                                         src="{{$data_shop->image_card_2}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card_id">
                            <div class="d-flex">
                                <div class="title_card">
                                    <h5 class="text-bold-700">Tên doanh nghiệp</h5>
                                    <h5 class="text-bold-700">Ngành nghề kinh doanh</h5>
                                </div>
                                <div class="info_card ml-3">
                                    <h5>{{$data_shop->name_company}}</h5>
                                    <h5>{{$data_shop->business}}</h5>
                                </div>
                            </div>
                            <div class="image_card mt-1">
                                <div>
                                    <h5>Giấy phép đăng ký kinh doanh</h5>
                                    <img class="border_shadow image-preview" width="290px" height="179px"
                                         src="{{$data_shop->business_license}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card_id">
                            <h5 class="text-bold-700">Giấy tờ liên quan</h5>
                            <br>
                            <br>
                            <br>
                            <div class="image_card mt-1">
                                <div>
                                    <img class="border_shadow image-preview" width="290px" height="179px"
                                         src="{{$data_shop->related_document_1}}" alt="">
                                </div>
                                <div class="mt-1">
                                    <img class="border_shadow image-preview" width="290px" height="179px"
                                         src="{{$data_shop->related_document_2}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end mt-3">
                    @if($data_shop->active == 0)
                        <button data-url="{{url('admin/shop_confirm/confirm')}}" data-id="{{$data_shop->id}}"
                                style="width: 150px!important; height: 50px!important;"
                                class="btn btn-success mr-1 text-bold-700 f-16 btn_confirm_shop">Xác nhận
                        </button>
                        <button data-url="{{url('admin/shop_confirm/cancel')}}" data-id="{{$data_shop->id}}"
                                style="width: 150px!important; height: 50px!important;"
                                class="btn btn_main mr-1 text-bold-700 f-16 btn_cancel_shop">Từ chối
                        </button>
                    @elseif($data_shop->active == 1)
                        <button data-url="{{url('admin/shop_confirm/cancel')}}" data-id="{{$data_shop->id}}"
                                style="width: 150px!important; height: 50px!important;"
                                class="btn btn_main mr-1 text-bold-700 f-16 btn_cancel_shop">Từ chối
                        </button>
                    @else
                        <button data-url="{{url('admin/shop_confirm/confirm')}}" data-id="{{$data_shop->id}}"
                                style="width: 150px!important; height: 50px!important;"
                                class="btn btn-success mr-1 text-bold-700 f-16 btn_confirm_shop">Xác nhận
                        </button>
                    @endif
                    <a href="{{url('admin/shop_confirm')}}">
                        <button style="width: 150px!important; height: 50px!important;"
                                class="btn btn_sliver mr-1 text-bold-700 f-16">Trở về
                        </button>
                    </a>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn_confirm_shop", function () {
            let url = $(this).attr('data-url');
            let id = $(this).attr('data-id');
            let form_data = new FormData();
            form_data.append('id', id);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        swal('Thông báo', 'Xác nhận thành công !', 'success');
                        window.location.href = data.url;
                    } else {
                        swal("Thất bại", data.msg, "error");
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });

        $(document).on("click", ".btn_cancel_shop", function () {
            let url = $(this).attr('data-url');
            let id = $(this).attr('data-id');
            let form_data = new FormData();
            form_data.append('id', id);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        swal('Thông báo', 'Từ chối xác nhận !', 'success');
                        window.location.href = data.url;
                    } else {
                        swal("Thất bại", data.msg, "error");
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
