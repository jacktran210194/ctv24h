<?php
$page = 'shop_confirm';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <div class="table table-responsive table-responsive-sm border_table">
                    <table class="table table-hover">
                        <thead class="bg_table">
                        <tr class="text-center text-bold-700">
                            <th class="text-left">Tên cửa hàng</th>
                            <th>Hình ảnh</th>
                            <th class="text-left">Thuộc NCC</th>
                            <th>Ngày gia nhập</th>
                            <th>Trạng thái</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data_shop))
                            @foreach($data_shop as $value)
                                <tr class="text-center text-bold-700">
                                    <td class="text-left">{{$value->name}}</td>
                                    <td>
                                        <img class="image-preview border_radius" width="60px" height="60px"
                                             src="{{$value->image}}">
                                    </td>
                                    <td class="text-left">{{$value->name_supplier}}</td>
                                    <td>{{$value->date}}</td>
                                    <td class="text-red">{{$value->is_active}}</td>
                                    <td class="text-underline"><a class="text-blue"
                                                                  href="{{url('admin/shop_confirm/detail/'.$value->id)}}">Chi
                                            tiết cửa hàng</a></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
