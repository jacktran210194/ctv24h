<?php
$page = 'voucher';
use Illuminate\Support\Facades\File;use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label class="lb">Voucher dành cho</label>
                                <select name="" id="type" class="form-control">
                                    <option <?= isset($data) && ($data->type == 0) ? 'selected' : '' ?> value="0">Nhà cung cấp</option>
                                    <option <?= isset($data) && ($data->type == 1) ? 'selected' : '' ?> value="1">Cộng tác viên</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="lb">Tiêu đề</label>
                                <input type="text" id="title" placeholder="Tiêu đề"
                                       value="<?= isset($data) ? $data['title'] : ''?>"
                                       class="form-control">
                            </div>
                            <div class="form-group content-unit">
                                <label class="lb">Nội dung</label>
                                <input type="text" id="content" placeholder="Nội dung"
                                       value="<?= isset($data) ? $data['content'] : ''?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Mã voucher</label>
                                <input type="text" id="code" placeholder="Mã voucher"
                                       value="<?= isset($data) ? $data['code'] : '' ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Thời gian bắt đầu</label>
                                <input type="datetime-local" id="time_start" placeholder="Thời gian bắt đầu"
                                       value="<?= isset($data) ? date('Y-m-d\TH:i:s', strtotime($data['time_start'])) : date('Y-m-d\TH:i:s') ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Thời gian kết thúc</label>
                                <input type="datetime-local" id="time_end" placeholder="Thời gian kết thúc"
                                       value="<?= isset($data) ? date('Y-m-d\TH:i:s', strtotime($data['time_end'])) : date('Y-m-d\TH:i:s') ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group align-items-center money-or-percent-unit">
                                <label class="" style="width: 250px">Loại giảm giá / mức giảm:</label>
                                <div class="d-flex w-100">
                                    <select id="money_or_percent" class="form-control mr-1">
                                        <option value="0" <?= isset($data) && $data->money_or_percent == 0 ? 'selected'  : '' ?>>Theo số tiền</option>
                                        <option value="1" <?= isset($data) && $data->money_or_percent == 1 ? 'selected'  : '' ?>>Theo phần trăm</option>
                                    </select>
                                    <input required type="number" id="discount_value"
                                           value="<?= isset($data) ? $data->discount_value : '' ?>" class="form-control ml-1" placeholder="Giá trị giảm">
                                </div>
                            </div>
                            <div class="form-group money-max-unit">
                                <label class="lb">Số tiền giảm tối đa</label>
                                <input type="number" id="money_max" placeholder="Số tiền giảm tối đa"
                                       value="<?= isset($data) ? $data['money_max'] : ''?>"
                                       class="form-control">
                            </div>
                            <div class="form-group coin-max-unit">
                                <label class="lb">Số xu giảm tối đa</label>
                                <input type="number" id="coin_max" placeholder="Số xu giảm tối đa"
                                       value="<?= isset($data) ? $data['coin_max'] : ''?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label class="lb">Giá trị đơn hàng tối thiểu</label>
                                <input type="number" id="min_order_value" placeholder="Giá trị đơn hàng tối thiểu"
                                       value="<?= isset($data)? $data['min_order_value'] : '' ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Số lượng</label>
                                <input type="number" id="number" placeholder="Số lượng"
                                       value="<?= isset($data)? $data['number'] : '' ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group discount-percent-unit">
                                <label class="lb">Phần trăm giảm</label>
                                <input type="number" id="discount_percent" placeholder="Phần trăm giảm"
                                       value="<?= isset($data)? $data['discount_percent'] : '' ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group voucher-value-unit">
                                <label class="lb">Giá trị voucher</label>
                                <input type="number" id="voucher_value" placeholder="Giá trị voucher"
                                       value="<?= isset($data)? $data['voucher_value'] : '' ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group category-unit">
                                <label for="first-name-floating-icon">Danh mục</label>
                                <select class="form-control" name="category_id" id="category_id">
                                    <?php foreach ($category as $value) :?>
                                        <option @isset($data) @if($value->id == $data->category_id) selected @endif @endisset value="{{$value->id}}">{{$value->name}}</option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ảnh</label>
                                <div class="form-label-group">
                                    <input type='file' value="" accept="image/*" name="image" id="image" onchange="changeImg(this)">
                                    <br>
                                    <img width="120px" height="120px" id="avatar" class="thumbnail image-show-qr"
                                         src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/images/avt.jpg' ?>">
                                </div>
                            </div>
                            <?php if(!isset($data)): ?>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/voucher/save') ?>"><i class="fa fa-floppy-o"
                                                                                            aria-hidden="true"></i> Lưu
                            </button>
                            <?php endif; ?>
                            <a href="<?= URL::to('admin/voucher') ?>" class="btn btn-danger"
                               style="color: white"><i class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<!-- BEGIN: Footer-->
@include('admin.base.footer')
<!-- END: Footer-->
@include('admin.base.script')
<script type="text/javascript">
    $(document).ready(function () {
        $('.content-unit').show();
        $('.discount-percent-unit').show();
        $('.voucher-value-unit').show();
        $('.category-unit').hide();
        $('.money-max-unit').hide();
        $('.coin-max-unit').hide();
        $('.money-or-percent-unit').hide();

        $(document).on("click", ".btn-add-account", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('category_id', $('#category_id').val());
            form_data.append('title', $('#title').val());
            form_data.append('content', $('#content').val());
            form_data.append('code', $('#code').val());
            form_data.append('time_start', $('#time_start').val());
            form_data.append('time_end', $('#time_end').val());
            form_data.append('number', $('#number').val());
            form_data.append('active', $('#active').val());
            form_data.append('type', $('#type').val());
            form_data.append('discount_percent', $('#discount_percent').val());
            form_data.append('voucher_value', $('#voucher_value').val());
            form_data.append('money_or_percent', $('#money_or_percent').val());
            form_data.append('discount_value', $('#discount_value').val());
            form_data.append('money_max', $('#money_max').val());
            form_data.append('money_coin', $('#money_coin').val());
            form_data.append('image', $('#image')[0].files[0]);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("change", '#type', function () {
            if($(this).val() == 1){
                $('.content-unit').hide();
                $('.category-unit').show();
                $('.discount-percent-unit').hide();
                $('.voucher-value-unit').hide();
                $('.money-max-unit').show();
                $('.coin-max-unit').show();
                $('.money-or-percent-unit').show();
            } else {
                $('.content-unit').show();
                $('.category-unit').hide();
                $('.discount-percent-unit').show();
                $('.voucher-value-unit').show();
                $('.money-max-unit').hide();
                $('.coin-max-unit').hide();
                $('.money-or-percent-unit').hide();
            }
        });
        if($('#type').val() == 1){
            $('.content-unit').hide();
            $('.category-unit').show();
            $('.discount-percent-unit').hide();
            $('.voucher-value-unit').hide();
            $('.money-max-unit').show();
            $('.coin-max-unit').show();
            $('.money-or-percent-unit').show();
        } else {
            $('.content-unit').show();
            $('.category-unit').hide();
            $('.discount-percent-unit').show();
            $('.voucher-value-unit').show();
            $('.money-max-unit').hide();
            $('.coin-max-unit').hide();
            $('.money-or-percent-unit').hide();
        }
    });
</script>
<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>

