<?php
$page = 'shipping';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Tên đơn vị vận chuyển</label>
                                <input type="text" name="name" id="name" placeholder="Tên đơn vị..."
                                       value="<?= isset($data) ? $data['name'] : '' ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Hotline</label>
                                <input type="number" name="phone" id="phone" placeholder="Hotline..."
                                       value="<?= isset($data) ? $data['phone'] : '' ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ</label>
                                <input type="text" name="address" id="address" placeholder="Địa chỉ"
                                       value="<?= isset($data) ? $data['address'] : '' ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Khối lượng tối đa</label>
                                <input type="number" id="max_weight" placeholder="Cân nặng tối đa"
                                       value="<?= isset($data) ? $data['max_weight'] : '' ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="form-label-group">
                                    <input type='file' value="" accept="image/*" name="image" id="image"
                                           onchange="changeImg(this)">
                                    <br>
                                    <img width="120px" height="120px" id="avatar" class="thumbnail image-show-qr"
                                         src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/images/avt.jpg' ?>">
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/shipping/save') ?>"><i class="fa fa-floppy-o"
                                                                                        aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/shipping') ?>" class="btn btn-danger" style="color: white"><i
                                    class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).on("click", ".btn-add-account", function () {
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('id', $(this).attr('data-id'));
        form_data.append('name', $('#name').val());
        form_data.append('phone', $('#phone').val());
        form_data.append('address', $('#address').val());
        form_data.append('max_weight',$('#max_weight').val());
        form_data.append('image', $('#image')[0].files[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                showLoading();
            },
            success: function (data) {
                if (data.status) {
                    window.location.href = data.url;
                } else {
                    alert(data.msg);
                }
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
