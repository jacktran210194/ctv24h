<?php
$page = 'category_product_child';
use Illuminate\Support\Facades\URL;
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Tên danh mục</label>
                                <input required type="text" name="name" id="name"
                                       value="<?= isset($data) ? $data['name'] : '' ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Danh mục cha</label>
                                <select name="category_id" id="category_id" class="form-control">
                                    @foreach($category as $value)
                                        @foreach($category_1 as $value_1)
                                            @if($value_1->category_id == $value->id)
                                    <option
                                        @isset($data) @if($value_1['id']==$data['category_product_child_id']) selected @endif @endisset data-value="{{$value->id}}" value="{{$value_1->id}}">{{$value->name}} : {{$value_1->name}} </option>
                                            @endif
                                         @endforeach
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="form-label-group">
                                    <input type='file' value="" accept="image/*" name="image" id="image"
                                           onchange="changeImg(this)">
                                    <br>
                                    <img width="120px" height="120px" id="avatar" class="thumbnail image-show-qr"
                                         src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/images/avt.jpg' ?>">
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    ><i
                                    class="fa fa-floppy-o"
                                    aria-hidden="true"></i>
                                Lưu
                            </button>
                            <a href="<?= URL::to('admin/category_product_child') ?>" class="btn btn-danger"><i
                                    class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        let url = window.location.origin + '/admin/sub_category_product_child/save';
        let category_product_id = $("#category_id option:selected").attr("data-value");
        $('#category_id').change(function(){
            category_product_id = $(this).find(':selected').attr('data-value');
        });
        $(".btn-add-account").click(function () {
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('name', $('#name').val());
            form_data.append('category_id', $('#category_id').val());
            form_data.append('category_product_id', category_product_id);
            form_data.append('image', $('#image')[0].files[0]);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log(form_data)
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
