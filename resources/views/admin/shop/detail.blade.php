<?php
$page = 'shop';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="pb-1">
                                <label for="">Tên shop:</label>
                                <span class="text-bold-700"><?= isset($data) ? $data->name : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Danh mục:</label>
                                <span class="text-bold-700"><?= isset($data) ? $data->category : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Ngày tham gia:</label>
                                <span class="text-bold-700"><?= isset($data) ? $data->date : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Thời gian phản hồi:</label>
                                <span class="text-bold-700"><?= isset($data) ? $data->time_feedback : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Tỷ lệ đơn hàng thất bại: </label>
                                <span class="text-bold-700"><?= isset($data) ? $data->percent_order_false : '' ?> %</span>
                            </div>
                            <div class="pb-1">
                                <label for="">Tỷ lệ phản hồi: </label>
                                <span class="text-bold-700"><?= isset($data) ? $data->percent_feedback : '' ?> %</span>
                            </div>
                            <div class="pb-1">
                                <label for="">Tổng số sản phẩm:</label>
                                <span class="text-bold-700"><?= isset($data) ? $data->product : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Tổng đánh giá:</label>
                                <span class="text-bold-700"><?= isset($data) ? $data->review : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Báo cáo:</label>
                                <span class="text-bold-700"><?= isset($data) ? $data->report : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Nhà cung cấp:</label>
                                <span class="text-bold-700"><?= isset($data) ? $data->name_supplier : '' ?></span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="pb-1 form-group">
                                <label for="">Hình ảnh:</label>
                                <br>
                                <img width="150px" height="150px" class="image-preview" src="<?= isset($data) ? $data->image_shop : '' ?>" alt="">
                            </div>

                            <div class="form-group">
                                <label class="lb">Link youtube: </label>
                                <div>
                                    <iframe frameborder="0" allowfullscreen class="mt-2" id="youtube" width="500" height="250" src="<?= isset($data) ? str_replace(array('watch', '?v='), array('embed', '/'), $data->video) : '' ?>">
                                    </iframe>
                                </div>
{{--                                <a target="true" href=""><span class="text-bold-700"></span></a>--}}
                            </div>
                            <a href="<?= URL::to('admin/shop') ?>" class="btn btn-danger" style="color: white"><i
                                    class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).on("change", "#video", function () {
        let value = $(this).val();
        value = value.replace('watch', 'embed');
        value = value.replace('?v=', '/');
        $("#youtube").attr('src', value);
    });

    $(document).on("click", ".btn-add-account", function () {
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('id', $(this).attr('data-id'));
        form_data.append('name', $('#name').val());
        form_data.append('date', $('#date').val());
        form_data.append('time_feedback', $('#time_feedback').val());
        form_data.append('report', $('#report').val());
        form_data.append('category_id', $('#category_id').val());
        form_data.append('video', $('#video').val());
        form_data.append('desc', CKEDITOR.instances['desc'].getData());
        form_data.append('image', $('#image')[0].files[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                showLoading();
            },
            success: function (data) {
                if (data.status) {
                    window.location.href = data.url;
                } else {
                    alert(data.msg);
                }
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>

