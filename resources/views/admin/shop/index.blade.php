<?php
$page = 'shop';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <a href="{{url('admin/shop/add')}}" class="btn btn-success"><i class="fa fa-plus"
                                                                                   aria-hidden="true"></i> Thêm cửa hàng</a>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <!-- content -->
                <div class="table-responsive table-responsive-lg">
                    <table class="table data-list-view table-sm">
                        <thead>
                        <tr>
                            <th scope="col">Tên shop</th>
                            <th scope="col">Hình ảnh</th>
                            <th scope="col">Ngày tham gia</th>
                            <th scope="col">Danh mục shop</th>
                            <th scope="col">Sản phẩm</th>
                            <th width="20%" scope="col">Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data))
                            @foreach($data as $value)
                                <tr>
                                    <td>{{$value->name}}</td>
                                    <td>
                                        <img width="40px" height="auto" class="image-preview" src="{{$value->image}}"
                                             alt="">
                                    </td>
                                    <td>{{$value->date}}</td>
                                    <td>{{$value->category}}</td>
                                    <td>{{$value->product}}</td>
                                    <td>
                                        <a href="{{url('admin/shop/detail/'.$value->id)}}">
                                            <button class="btn btn-info" title="Chi tiết"><i class="fa fa-info"
                                                                                                  aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <a href="{{url('admin/shop/edit/'.$value->id)}}">
                                            <button class="btn btn-warning" title="Sửa"><i class="fa fa-pencil-square-o"
                                                                               aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <button
                                            class="btn btn-delete btn-danger" title="Xoá"
                                            data-url="{{url('admin/shop/delete/'.$value->id)}}"
                                        >
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">Không có dữ liệu</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
