<?php
$page = 'shop';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Tên shop</label>
                                <input required type="text" name="name" id="name"
                                       value="<?= isset($data) ? $data['name'] : '' ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Ngày tham gia</label>
                                <input required type="date" name="date" id="date"
                                       value="<?= isset($data) ? date('Y-m-d', strtotime($data['date'])) : date('Y-m-d') ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Thời gian phản hồi</label>
                                <input required type="text" name="time_feedback" id="time_feedback"
                                       value="<?= isset($data) ? $data['time_feedback'] : '' ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Báo cáo</label>
                                <input required type="text" name="report" id="report"
                                       value="<?= isset($data) ? $data['report'] : '' ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Danh mục shop</label>
                                <select class="form-control" name="category_id" id="category_id">
                                    @foreach($category_shop as $value)
                                        <option @isset($data) @if($data['category_id'] == $value['id'])  selected
                                                @endif @endisset value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nhà cung cấp</label>
                                <select class="form-control" name="supplier_id" id="supplier_id">
                                    <?php foreach ($dataSupplier as $value) :?>
                                    <option <?= isset($data) && ($value->id == $data->supplier_id) ? 'selected' : '' ?> value="{{$value->id}}">{{$value->name}}</option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <input hidden class="input_image" type='file' value="" accept="image/*" id="image"
                                       onchange="changeImg(this)">
                                <button class="btn btn-warning btn-image-1"><i class="fa fa-image"></i> Hình đại diện
                                </button>
                                <br>
                                <br>
                                <img width="120px" height="120px" id="avatar" class="thumbnail image-show-qr"
                                     src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/images/avt.jpg' ?>">
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Mô tả shop</label>
                                <textarea class="form-control ckeditor" name="desc" id="desc" cols="30"
                                          rows="3"><?= isset($data) ? $data['desc'] : '' ?></textarea>
                            </div>
                            <div class="form-group">
                                <label class="lb">Link youtube</label>
                                <input type="text" id="video" placeholder="Link youtube" value="<?= isset($data) ? $data['video'] : '' ?>" class="form-control">
                                <iframe frameborder="0" allowfullscreen class="mt-2" id="youtube" width="500" height="250" src="<?= isset($data) ? str_replace(array('watch', '?v='), array('embed', '/'), $data['link']) : '' ?>">
                                </iframe>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <input hidden class="form-control input_list_image" type='file' accept="image/*"
                                       id="imageQr"
                                       multiple/>
                                <button class="btn btn-success btn-show-list"><i class="fa fa-image"></i> Thư viện ảnh
                                </button>
                                <br>
                                <label for="">(Tối đa 8 hình ảnh)</label>
                                <div class="row image-show">
                                    <?php if (isset($data_image) && count($data_image)): ?>
                                    <?php foreach ($data_image as $value): ?>
                                    <div class="pip col-lg-2 col-md-3" style="position: relative">
                                        <img class="mr-2 mt-2" style="width: 100px; height: 100px;"
                                             src="<?= $value['image'] ?>"
                                             title=""><br>
                                        <button type="button" style="position: absolute;top: 20px;left: 120px;"
                                                class="fa fa-times btn btn-danger btn-delete-image"
                                                data-url="<?= URL::to('admin/shop/delete_image/' . $value['id']) ?>"></button>
                                    </div>
                                    <?php  endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/shop/save') ?>"><i class="fa fa-floppy-o"
                                                                                    aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/shop') ?>" class="btn btn-danger" style="color: white"><i
                                    class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
@include('admin.base.script_product')
{{--<script src="dashboard/index.js"></script>--}}
<script>
$(document).ready(function () {
    let form_data = new FormData();
    $(document).on("click", ".btn-delete-image", function () {
        let url = $(this).attr('data-url');
        $('.btn-confirm').attr('data-url', url);
        $('.message-modal').text($(this).attr('data-msg'));
        $('#myModal').modal('show');
    });

    $(document).on("click", ".btn-remove", function () {
        let index = $(".btn-remove").index(this);
        let values = form_data.getAll("img[]");
        values.splice(index, 1);
        form_data.delete("img[]");
        for (let i = 0; i < values.length; i++) {
            form_data.append("img[]", values[i]);
        }
        $(this).parent(".pip").remove();
    });
    $(document).on("change", "#imageQr", function () {
        readURL(this);
    });
    $(document).on("click", ".btn-upload-image-qr", function () {
        $('#imageQr').trigger('click');
    });

    $(document).on("change", "#video", function () {
        let value = $(this).val();
        value = value.replace('watch', 'embed');
        value = value.replace('?v=', '/');
        $("#youtube").attr('src', value);
    });

    $(document).on("click", ".btn-add-account", function () {
        let url = $(this).attr('data-url');
        form_data.append('id', $(this).attr('data-id'));
        form_data.append('name', $('#name').val());
        form_data.append('date', $('#date').val());
        form_data.append('time_feedback', $('#time_feedback').val());
        form_data.append('report', $('#report').val());
        form_data.append('category_id', $('#category_id').val());
        form_data.append('video', $('#video').val());
        form_data.append('supplier_id', $('#supplier_id').val());
        form_data.append('desc', CKEDITOR.instances['desc'].getData());
        form_data.append('image', $('#image')[0].files[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                showLoading();
            },
            success: function (data) {
                if (data.status) {
                    window.location.href = data.url;
                } else {
                    alert(data.msg);
                }
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
    function readURL(input) {
        if (input.files) {
            let filesAmount = input.files.length;
            for (let i = 0; i < filesAmount; i++) {
                let reader = new FileReader();

                reader.onload = function (e) {
                    let file = e.target;
                    let html = "<div class=\"pip col-lg-2 col-md-3\" style='position: relative'>" +
                        "<img class=\"mr-3 mt-2\" style=\"width: 100px; height: 100px;\"  src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                        "<br/><button style='position: absolute;top: 20px;left: 120px;' class=\"fa fa-times btn btn-danger btn-remove\"></button>" +
                        "</div>";
                    $('.image-show').append(html);
                }
                form_data.append('img[]', $('#imageQr')[0].files[i]);
                reader.readAsDataURL(input.files[i]);
            }
        }
    }
});
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
