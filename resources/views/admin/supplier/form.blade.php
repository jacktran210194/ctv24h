<?php
$page = 'supplier';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Họ và tên</label>
                                <input required type="text" name="name" id="name" placeholder="Họ và tên..."
                                       value="<?= isset($data) ? $data['name'] : '' ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Tên tài khoản</label>
                                <input required type="text" name="username" id="username" placeholder="Tên tài khoản..."
                                       value="<?= isset($data) ? $data['username'] : '' ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ email</label>
                                <input required type="email" name="email" id="email" placeholder="Email..."
                                       value="<?= isset($data) ? $data['email'] : '' ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Số điện thoại</label>
                                <input required type="number" name="phone" id="phone" placeholder="Số điện thoại..."
                                       value="<?= isset($data) ? $data['phone'] : '' ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Ngày sinh</label>
                                <input required type="date" name="birthday" id="birthday"
                                       value="<?= isset($data) ? date('Y-m-d', strtotime($data['birthday'])) : date('Y-m-d') ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Giới tính</label>
                                <select class="form-control" name="gender" id="gender">
                                    <option @isset($data) @if($data['gender']==0) selected @endif @endisset value="0">
                                        Nam
                                    </option>
                                    <option @isset($data) @if($data['gender']==1) selected @endif @endisset value="1">
                                        Nữ
                                    </option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Số CMND / Căn cước công dân</label>
                                <input required type="text" name="id_card" id="id_card" placeholder="CMND..."
                                       value="<?= isset($data) ? $data['id_card'] : '' ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Giấy phép kinh doanh</label>
                                <input required type="text" name="business_license" id="business_license"
                                       placeholder="Giấy phép kinh doanh..."
                                       value="<?= isset($data) ? $data['business_license'] : '' ?>"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="form-label-group">
                                    <input type='file' value="" accept="image/*" name="image" id="image" onchange="changeImg(this)">
                                    <br>
                                    <img width="120px" height="120px" id="avatar" class="thumbnail image-show-qr"
                                         src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/avt.jpg' ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <lable>Mật khẩu</lable>
                                <input required type="password" class="form-control" name="password" id="password" placeholder="Mật khẩu...">
                            </div>
                            <div class="form-group">
                                <label>Thông tin cung ứng sản phẩm</label>
                                <input required type="text" name="product_supply" id="product_supply" placeholder="Sản phẩm cung ứng"
                                       value="<?= isset($data) ? $data['product_supply'] : '' ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Giấy tờ nhập khẩu</label>
                                <textarea required class="form-control" name="import_papers" id="import_papers" cols="30"
                                          rows="4"><?= isset($data) ? $data['import_papers'] : '' ?></textarea>
                            </div>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/supplier/save') ?>"><i class="fa fa-floppy-o"
                                                                                      aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/supplier') ?>" class="btn btn-danger" style="color: white"><i
                                    class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).on("click", ".btn-add-account", function () {
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('id', $(this).attr('data-id'));
        form_data.append('name', $('#name').val());
        form_data.append('phone', $('#phone').val());
        form_data.append('password', $('#password').val());
        form_data.append('gender', $('#gender').val());
        form_data.append('birthday', $('#birthday').val());
        form_data.append('id_card', $('#id_card').val());
        form_data.append('business_license', $('#business_license').val());
        form_data.append('product_supply', $('#product_supply').val());
        form_data.append('import_papers', $('#import_papers').val());
        form_data.append('username', $('#username').val());
        form_data.append('email', $('#email').val());
        form_data.append('image', $('#image')[0].files[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                showLoading();
            },
            success: function (data) {
                if (data.status) {
                    window.location.href = data.url;
                } else {
                    alert(data.msg);
                }
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
