<?php
$page = 'supplier';

use Illuminate\Support\Facades\File;use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label class="lb">Chủ tài khoản</label>
                                <input type="text" id="user_name" name="user_name" placeholder="Chủ tài khoản" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Ngân hàng</label>
                                <input type="text" id="bank_name" name="bank_name" placeholder="Ngân hàng" class="form-control">
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label class="lb">Số tài khoản</label>
                                <input type="text" id="bank_account" name="bank_account" placeholder="Số tài khoản" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Ngày kích hoạt</label>
                                <input type="date" id="date_active" name="data_active" placeholder="Ngày kích hoạt" class="form-control">
                            </div>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/supplier/wallet/'.$data['id'].'/add') ?>"><i class="fa fa-floppy-o"
                                                   aria-hidden="true"></i> Lưu
                            </button>
                            <a href="{{url('admin/supplier/wallet/'.$data['id'])}}" class="btn btn-danger"><i class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<!-- BEGIN: Footer-->
@include('admin.base.footer')
<!-- END: Footer-->
@include('admin.base.script')
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".btn-add-account", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('user_id', $(this).attr('data-id'));
            form_data.append('user_name', $('#user_name').val());
            form_data.append('bank_name', $('#bank_name').val());
            form_data.append('bank_account', $('#bank_account').val());
            form_data.append('date_active', $('#date_active').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });
</script>
<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>

