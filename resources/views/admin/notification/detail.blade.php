<?php
$page = 'notification';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="pb-1">
                                <label for="">Tiêu đề: </label>
                                <span class="text-bold-700">{{$data['title']}}</span>
                            </div>
                            <div class="pb-1">
                                <label for="">Nội dung: </label>
                                <span class="text-bold-700">{!! $data['message'] !!}</span>
                            </div>
                            <div class="pb-1">
                                <label for="">Ngày tạo: </label>
                                <span class="text-bold-700">{{date('d-m-Y H:i:s', strtotime($data['created_at']))}}</span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="pb-1 form-group">
                                <label for="">Hình ảnh: </label>
                                <br>
                                <img width="150px" height="150px" class="image-preview" src="{{$data['image']}}" alt="">
                            </div>
                            <a href="<?= URL::to('admin/notification') ?>" class="btn btn-danger" style="color: white"><i
                                    class="fa fa-undo" aria-hidden="true"></i> Quay lại</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>

