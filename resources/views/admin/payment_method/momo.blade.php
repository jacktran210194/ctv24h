<?php
$page = 'momo';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>Momo</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content">
                <!-- content -->
                <div class="m-1">
                    <div class="form-group">
                        <label class="lb">Partner Code</label>
                        <input type="text" id="partnerCode" placeholder="Partner Code" class="form-control"
                               value="<?= isset($data) ? $data['partnerCode'] : '' ?>">
                    </div>
                    <div class="form-group">
                        <label class="lb">Access Key</label>
                        <input type="text" id="accessKey" placeholder="Access Key" class="form-control"
                               value="<?= isset($data) ? $data['accessKey'] : '' ?>">
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-add-account"
                                data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                data-url="<?= URL::to('admin/payment_method/momo/save') ?>"><i class="fa fa-floppy-o"
                                                                                   aria-hidden="true"></i> Lưu
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
</div>
@include('frontend.address.modal_success')
@include('frontend.address.modal_error')

    <!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
<script src="{{asset('assets/frontend/js/base/index.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".btn-add-account", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('partnerCode', $('#partnerCode').val());
            form_data.append('accessKey', $('#accessKey').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    console.log('data', data);
                    if (data.status) {
                        window.location.reload();
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

