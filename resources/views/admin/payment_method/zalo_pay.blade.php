<?php
$page = 'zalo_pay';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>Zalo Pay</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content">
                <!-- content -->
                <div class="m-1">
                    <div class="form-group">
                        <label class="lb">User Id</label>
                        <input type="text" id="user_id" placeholder="User Id" class="form-control"
                               value="<?= isset($data) ? $data['muid'] : '' ?>">
                    </div>
                    <div class="form-group">
                        <label class="lb">Access Token</label>
                        <input type="text" id="access_token" placeholder="Access Token" class="form-control"
                               value="<?= isset($data) ? $data['maccesstoken'] : '' ?>">
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-add-account"
                                data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                data-url="<?= URL::to('admin/payment_method/zalo_pay/save') ?>"><i class="fa fa-floppy-o"
                                                                                   aria-hidden="true"></i> Lưu
                        </button>
                        <button type="button" class="btn btn-success btn-check-connect"
                                data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                data-url="<?= URL::to('admin/payment_method/zalo_pay/connect') ?>"><i class="fa fa-plug" aria-hidden="true"></i> Kiểm tra kết nối
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
</div>
@include('frontend.address.modal_success')
@include('frontend.address.modal_error')

    <!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
<script src="{{asset('assets/frontend/js/base/index.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".btn-add-account", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('user_id', $('#user_id').val());
            form_data.append('access_token', $('#access_token').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    if (data.status) {
                        window.location.reload();
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });

        $(document).on("click", ".btn-check-connect", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('user_id', $('#user_id').val());
            form_data.append('access_token', $('#access_token').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    data = JSON.parse(data.response);
                    console.log('data', data);
                    if (data.returncode === 1) {
                        $('.text-mes-success').text('Kết nối thành công');
                        $('#modalSuccess').show();
                        setTimeout(function () {
                            $('#modalSuccess').fadeOut("slow");
                        }, 1000)
                    } else {
                        $('.text-mes-error').text(data.returnmessage);
                        $('#modalError').show();
                        setTimeout(function () {
                            $('#modalError').fadeOut("slow");
                        }, 1000)
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

