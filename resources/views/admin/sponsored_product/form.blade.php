<?php
$page = 'sponsored_product';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Sản phẩm</label>
                                <select name="" id="product_id" class="form-control">
                                    <?php if(isset($data)) :?>
                                        <?php foreach ($data as $value) :?>
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                        <?php endforeach; ?>
                                    <?php else :?>
                                        <tr>
                                            <td colspan="6">Không có dữ liệu</td>
                                        </tr>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="">Giá trị giảm</label>
                                <input class="form-control" type="number" name="value" id="value" value="">
                            </div>

                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-url="<?= URL::to('admin/product/sponsored/save') ?>"><i class="fa fa-floppy-o"
                                                                                       aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/product/sponsored') ?>" class="btn btn-danger"><i
                                    class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).on("click", ".btn-add-account", function () {
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('id', $(this).attr('data-id'));
        form_data.append('product_id', $('#product_id').val());
        form_data.append('value', $('#value').val());

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                showLoading();
            },
            success: function (data) {
                if (data.status) {
                    window.location.href = data.url;
                } else {
                    alert(data.msg);
                }
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
