<div class="form-group form_attr">
    <div class="form-group d-flex">
        <label style="width: 8%;">Thuộc tính</label>
        <input style="width: 35%;" class="attribute_name form-control" type="text">
    </div>
    <div class="form-group d-flex">
        <label style="width: 8%;">Giá trị</label>
        <input style="width: 35%;" class="attribute_value form-control" type="text">
        <button data-url="<?= URL::to('admin/product/form_item') ?>"
                class="btn-add-value btn btn-primary"><i class='fa fa-plus'></i></button>
    </div>
    <div class="form-group">
        <div class="form-1"></div>
    </div>
</div>
