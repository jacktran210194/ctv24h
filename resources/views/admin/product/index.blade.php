<?php
$page = 'product';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <a href="{{url('admin/product/add')}}" class="btn btn-success"><i class="fa fa-plus"
                                                                                   aria-hidden="true"></i> Thêm sản phẩm</a>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="form-label-group position-relative has-icon-left">
            <input type="text" id="search" class="form-control" name="search" placeholder="Nhập tên sản phẩm...">
            <div class="form-control-position">
                <i class="feather icon-search"></i>
            </div>
        </div>
        <div class="float-right">
            <select name="" id="is_status" class="form-control">
                <option value="all">-- Lọc trạng thái sản phẩm --</option>
                <option value="0">Sản phẩm còn hàng</option>
                <option value="1">Sản phẩm hết hàng</option>
                <option value="2">Sản phẩm bị đánh vi phạm</option>
                <option value="3">Sản phẩm bị ẩn</option>
            </select>
        </div>
        <div class="clearfix mb-2"></div>
        <div class="card">
            <div class="card-content">
                <!-- content -->
                <div class="table-responsive table-responsive-lg">
                    <table class="table data-list-view table-sm">
                        <thead>
                        <tr>
                            <th scope="col">Tên sản phẩm</th>
                            <th scope="col">Hình ảnh</th>
                            <th scope="col">Danh mục</th>
                            <th>Thuộc cửa hàng</th>
                            <th scope="col">Loại sản phẩm</th>
                            <th>Kho</th>
                            <th>Đã bán</th>
                            <th scope="col">Trạng thái</th>
                            <th scope="col">Hành động</th>
                        </tr>
                        </thead>
                        <tbody id="data_product">
{{--                        @if(count($data))--}}
{{--                            @foreach($data as $value)--}}
{{--                                <tr>--}}
{{--                                    <td>{{$value->name}}</td>--}}
{{--                                    <td>--}}
{{--                                        <img width="40px" height="40px" class="image-preview" src="{{$value->image}}"--}}
{{--                                             alt="">--}}
{{--                                    </td>--}}
{{--                                    <td>{{$value->category}}</td>--}}
{{--                                    <td>{{$value->shop_name}}</td>--}}
{{--                                    <td>{{$value->dropship}}</td>--}}
{{--                                    <td>{{$value->warehouse}}</td>--}}
{{--                                    <td>{{$value->is_sell}}</td>--}}
{{--                                    <td>{{$value->status}}</td>--}}
{{--                                    <td>--}}
{{--                                        <a href="{{url('admin/product/edit/'.$value->id)}}">--}}
{{--                                            <button class="btn btn-warning" title="Sửa"><i class="fa fa-pencil-square-o"--}}
{{--                                                                                           aria-hidden="true"></i>--}}
{{--                                            </button>--}}
{{--                                        </a>--}}
{{--                                        <button--}}
{{--                                            class="btn btn-delete btn-danger" title="Xoá"--}}
{{--                                            data-url="{{url('admin/product/delete/'.$value->id)}}"--}}
{{--                                        >--}}
{{--                                            <i class="fa fa-trash" aria-hidden="true"></i>--}}
{{--                                        </button>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                        @else--}}
{{--                            <tr>--}}
{{--                                <td colspan="6">Không có dữ liệu</td>--}}
{{--                            </tr>--}}
{{--                        @endif--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        fetch_customer_data();
        function fetch_customer_data(query = '')
        {
            $.ajax({
                url:"{{ route('search_product_admin') }}",
                method:'GET',
                data:{query:query},
                dataType:'json',
                success:function(data)
                {
                    $('tbody').html(data.table_data);
                }
            })
        }
        $(document).on('keyup', '#search', function(){
            let query = $(this).val();
            fetch_customer_data(query);
        });

        function getData(){
            let status = $('#is_status').val();
            $.get('/admin/product/filter_order/'+status, function (data) {
                $('#data_product').html(data);
            })
        }
        $(document).on('change','#is_status', function () {
            getData();
        });

        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
