<?php
$page = 'product';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Tên sản phẩm</label>
                                <input required type="text" name="name" id="name"
                                       value="<?= isset($data) ? $data['name'] : '' ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Đơn giá</label>
                                <input required type="number" name="price" id="price"
                                       value="<?= isset($data) ? $data['price'] : '' ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Giá giảm</label>
                                <input required type="number" name="price_discount" id="price_discount"
                                       value="<?= isset($data) ? $data['price_discount'] : '' ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label>SKU phân loại</label>
                                <input required type="text" name="sku_type" id="sku_type"
                                       value="<?= isset($data) ? $data['sku_type'] : '' ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Danh mục sản phẩm</label>
                                <select class="form-control" name="category_id" id="category_id">
                                    @foreach($category as $value)
                                        <option @isset($data) @if($data['category_id'] == $value['id'])  selected
                                                @endif @endisset value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Thương hiệu</label>
                                <select class="form-control" id="brand">
                                    <option @isset($data) @if($data['brand']==0) selected @endif @endisset value="0">No
                                        Brand
                                    </option>
                                    @foreach($brand as $value)
                                        <option @isset($data) @if($data['brand'] == $value['id'])  selected
                                                @endif @endisset value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Thuộc cửa hàng</label>
                                <select class="form-control" id="shop_id">
                                    @foreach($shop as $value)
                                        <option @isset($data) @if($data['shop_id'] == $value['id'])  selected
                                                @endif @endisset value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <input hidden class="input_image" type='file' value="" accept="image/*" id="image"
                                       onchange="changeImg(this)">
                                <button class="btn btn-warning btn-image-1"><i class="fa fa-image"></i> Hình đại diện
                                </button>
                                <br>
                                <br>
                                <img width="120px" height="120px" id="avatar" class="thumbnail image-show-qr"
                                     src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/images/avt.jpg' ?>">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Bài đăng mẫu</label>
                                <textarea class="form-control" id="sort_desc" cols="4"
                                          rows="3"><?= isset($data) ? $data['sort_desc'] : '' ?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Mô tả sản phẩm</label>
                                <textarea class="form-control ckeditor" name="desc" id="desc" cols="30"
                                          rows="3"><?= isset($data) ? $data['desc'] : '' ?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Trạng thái</label>
                                <select class="form-control" name="is_status" id="is_status">
                                    <option @isset($data) @if($data['is_status'] == 0) selected
                                            @endif @endisset value="0">Sản phẩm còn hàng
                                    </option>
                                    <option @isset($data) @if($data['is_status'] == 1) selected
                                            @endif @endisset value="1">Sản phẩm hết hàng
                                    </option>
                                    <option @isset($data) @if($data['is_status'] == 2) selected
                                            @endif @endisset value="2">Sản phẩm bị đánh vi phạm
                                    </option>
                                    <option @isset($data) @if($data['is_status'] == 3) selected
                                            @endif @endisset value="3">Sản phẩm bị ẩn
                                    </option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Dropship</label>
                                <select class="form-control" name="" id="dropship">
                                    <option @isset($data) @if($data['dropship']==1) selected @endif @endisset value="1">
                                        Có
                                    </option>
                                    <option @isset($data) @if($data['dropship']==0) selected @endif @endisset value="0">
                                        Không
                                    </option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Tồn kho</label>
                                <input class="form-control" type="number" name="warehouse" id="warehouse"
                                       value="<?= isset($data) ? $data['warehouse'] : '' ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Xuất xứ</label>
                                <input class="form-control" type="text" name="original" id="original"
                                       value="<?= isset($data) ? $data['original'] : '' ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Chất liệu</label>
                                <input class="form-control" type="text" name="material" id="material"
                                       value="<?= isset($data) ? $data['material'] : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input hidden class="form-control input_list_image" type='file' accept="image/*"
                                       id="imageQr"
                                       multiple/>
                                <button class="btn btn-success btn-show-list"><i class="fa fa-image"></i> Thư viện ảnh
                                </button>
                                <br>
                                <label for="">(Tối đa 8 hình ảnh)</label>
                                <div class="row image-show">
                                    <?php if (isset($data_image) && count($data_image)): ?>
                                    <?php foreach ($data_image as $value): ?>
                                    <div class="pip col-lg-2 col-md-3" style="position: relative">
                                        <img class="mr-2 mt-2" style="width: 100px; height: 100px;"
                                             src="<?= $value['image'] ?>"
                                             title=""><br>
                                        <button type="button" style="position: absolute;top: 20px;left: 120px;"
                                                class="fa fa-times btn btn-danger btn-delete-image"
                                                data-url="<?= URL::to('admin/product/delete_image/' . $value['id']) ?>"></button>
                                    </div>
                                    <?php  endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group mt-2">
                                <input style="width: 20px; height: 20px;"
                                       <?= isset($attr) && count($attr) ? 'checked' : '' ?> class="check_attr"
                                       type="checkbox"> Phân loại hàng hoá
                            </div>
                            <div class="attr-product"
                                 style="display: <?= isset($attr) && count($attr) ? 'block' : 'none' ?>;">
                                <div class="form-group">
                                    <button data-url="<?= URL::to('admin/product/form_attr') ?>"
                                            class="btn-add-group btn btn-success">Thêm nhóm phân loại
                                    </button>
                                    <button class="btn btn-warning btn-delete-group">Xoá nhóm phân loại</button>
                                </div>
                                <div class="form-group form_attr">
                                    <div class="form-1"></div>
                                </div>
                                <?php if (isset($attr) && count($attr)): ?>
                                   <?= $html_attr ?? '' ?>
                                <?php endif; ?>
                                <div class="more-email"></div>
                            </div>
                            <div class="form-group mt-2">
                                <input style="width: 20px; height: 20px;" class="check_price_discount"
                                       type="checkbox">
                                Mua nhiều giảm giá
                            </div>
                            <div class="form_discount" style="display: none;">
                                <table>
                                    <tr>
                                        <td>Từ (sản phẩm)</td>
                                        <td><input type="text" id="befor_number" class="form-control"></td>
                                        <td>Đến sản phẩm</td>
                                        <td><input type="text" id="after_number" class="form-control"></td>
                                        <td>Giá</td>
                                        <td><input type="text" id="price_combo" class="form-control"></td>
                                        <td>
                                            <button class="btn btn-danger"><i class="fa fa-plus-square"></i> Thêm
                                                khoảng
                                                giá
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="form-group mt-2">
                                <input class="check_auction" style="width: 20px;height: 20px;" type="checkbox"> Sản
                                phẩm
                                đấu giá
                            </div>
                            <div class="form_auction" style="display: none;">
                                <table>
                                    <tr>
                                        <td>Giá khởi điểm</td>
                                        <td><input type="number" id="price_start" class="form-control"></td>
                                        <td>Giá mua ngay</td>
                                        <td><input type="number" id="price_buy_now" class="form-control"></td>
                                        <td>Bước giá</td>
                                        <td><input type="text" id="price_step" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td>Thời gian đấu giá</td>
                                        <td><input type="datetime-local" id="time_auction" class="form-control">
                                        </td>
                                        <td>Thời gian bắt đầu</td>
                                        <td><input type="datetime-local" id="time_start_auction"
                                                   class="form-control"></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="mt-2">
                                <table>
                                    <tr>
                                        <td>Cân nặng sau đóng gói</td>
                                        <td><input type="number" id="weight" class="form-control"
                                                   placeholder="(gram)" value="{{isset($data) ? $data['weight'] : ''}}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kích thước đóng gói</td>
                                        <td><input type="text" class="form-control" id="length"
                                                   placeholder="Chiều dài"
                                                   value="{{isset($data) ? $data['length'] : ''}}">
                                        </td>
                                        <td><input type="text" class="form-control" id="width"
                                                   placeholder="Chiều rộng"
                                                   value="{{isset($data) ? $data['width'] : ''}}">
                                        </td>
                                        <td><input type="text" class="form-control" id="height"
                                                   placeholder="Chiều cao"
                                                   value="{{isset($data) ? $data['height'] : ''}}">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="form-group mt-2">
                                <input class="check_booking" style="width: 20px;height: 20px;" type="checkbox"> Hàng
                                đặt
                                trước
                            </div>
                            <div class="form_booking" style="display: none;">
                                <table>
                                    <tr>
                                        <td>Thời gian bắt đầu</td>
                                        <td><input type="datetime-local" id="time_start_booking"
                                                   class="form-control"></td>
                                        <td>Tình trạng</td>
                                        <td><input type="text" id="status_booking" class="form-control"></td>
                                        <td>SKU sản phẩm</td>
                                        <td><input type="text" id="sku_product_booking" class="form-control"></td>
                                    </tr>
                                </table>
                            </div>
                            <br>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/product/save') ?>"><i class="fa fa-floppy-o"
                                                                                       aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/product') ?>" class="btn btn-danger"><i
                                    class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- END: Content-->
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
@include('admin.base.script_product')
<script>
    $(document).ready(function () {
        let form_data = new FormData();
        $(document).on("click", ".btn-delete-image", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            $('#myModal').modal('show');
        });
        $(document).on("click", ".btn-delete-item", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            $('#myModal').modal('show');
        });

        $(document).on("click", ".btn-delete-attr", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            $('#myModal').modal('show');
        });

        $(document).on("click", ".btn-remove", function () {
            let index = $(".btn-remove").index(this);
            let values = form_data.getAll("img[]");
            values.splice(index, 1);
            form_data.delete("img[]");
            for (let i = 0; i < values.length; i++) {
                form_data.append("img[]", values[i]);
            }
            $(this).parent(".pip").remove();
        });
        $(document).on("change", "#imageQr", function () {
            readURL(this);
        });
        $(document).on("click", ".btn-upload-image-qr", function () {
            $('#imageQr').trigger('click');
        });
        $(document).on('click', '.btn-add-group', function (e) {
            e.preventDefault();
            let url = $(this).attr('data-url');
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    $(".more-email").append(data.html);
                },
                error: function (error) {
                    console.log(error)
                },
            });
        });
        $(document).on('click', '.btn-add-value', function () {
            let url = $(this).attr('data-url');
            let dom = $(this).parents('.form_attr:first').find('.form-1');
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    dom.append(data.html);
                },
                error: function (error) {
                    console.log(error)
                },
            });
        });

        $(document).on("click", ".btn-add-account", function () {
            let url = $(this).attr('data-url');
            let attribute = [];
            // let check = true;
            $(".attribute_name").each(function () {
                let attribute_item = {};
                attribute_item['name'] = $(this).val();
                attribute_item['item'] = [];
                attribute_item['id'] = $(this).attr('data-id') ?? '';
                $(this).parents('.form_attr:first').find('.attribute_value').each(function () {
                    if ($(this).val()) {
                        let item = {};
                        item['name'] = $(this).val();
                        item['id'] = $(this).attr('data-id') ?? '';
                        attribute_item['item'].push(item);
                    }
                });
                // if (!attribute_item['name'] || !attribute_item['item'].length) {
                //     check = false;
                //     return false;
                // }
                attribute.push(attribute_item);
            });
            // if (!check) {
            //     alert('Abc');
            //     return false;
            // }
            console.log('attr', attribute);
            let check_attr = $('.check_attr').is(':checked') ? 1 : 0;
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('name', $('#name').val());
            form_data.append('price', $('#price').val());
            form_data.append('price_discount', $('#price_discount').val());
            form_data.append('sku_type', $('#sku_type').val());
            form_data.append('category_id', $('#category_id').val());
            form_data.append('brand', $('#brand').val());
            form_data.append('is_status', $('#is_status').val());
            form_data.append('warehouse', $('#warehouse').val());
            form_data.append('dropship', $('#dropship').val());
            form_data.append('desc', CKEDITOR.instances['desc'].getData());
            form_data.append('weight', $('#weight').val());
            form_data.append('height', $('#height').val());
            form_data.append('length', $('#length').val());
            form_data.append('width', $('#width').val());
            form_data.append('original', $('#original').val());
            form_data.append('material', $('#material').val());
            form_data.append('image', $('#image')[0].files[0]);
            form_data.append('attr', JSON.stringify(attribute));
            form_data.append('check_attr', check_attr);
            form_data.append('shop_id',$('#shop_id').val());
            form_data.append('sort_desc',$('#sort_desc').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });

        function readURL(input) {
            if (input.files) {
                let filesAmount = input.files.length;
                for (let i = 0; i < filesAmount; i++) {
                    let reader = new FileReader();

                    reader.onload = function (e) {
                        let file = e.target;
                        let html = "<div class=\"pip col-lg-2 col-md-3\" style='position: relative'>" +
                            "<img class=\"mr-2 mt-2\" style=\"width: 100px; height: 100px;\"  src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                            "<br/><button style='position: absolute;top: 20px;left: 120px;' class=\"fa fa-times btn btn-danger btn-remove\"></button>" +
                            "</div>";
                        $('.image-show').append(html);
                    }
                    form_data.append('img[]', $('#imageQr')[0].files[i]);
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
