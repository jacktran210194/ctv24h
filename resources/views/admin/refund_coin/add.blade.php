<?php
$page = 'refund_coin';
use Illuminate\Support\Facades\File;use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
{{--                            <div class="form-group">--}}
{{--                                <label class="lb">Voucher dành cho</label>--}}
{{--                                <select name="" id="type" class="form-control">--}}
{{--                                    <option selected value="1">Cộng tác viên</option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
                            <div class="form-group">
                                <label class="lb">Tiêu đề</label>
                                <input type="text" id="title" placeholder="Tiêu đề"
                                       class="form-control">
                            </div>
{{--                            <div class="form-group content-unit">--}}
{{--                                <label class="lb">Nội dung</label>--}}
{{--                                <input type="text" id="content" placeholder="Nội dung"--}}
{{--                                       class="form-control">--}}
{{--                            </div>--}}
                            <div class="form-group">
                                <label class="lb">Mã voucher</label>
                                <input type="text" id="code" placeholder="Mã voucher"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Thời gian bắt đầu</label>
                                <input type="datetime-local" id="time_start" placeholder="Thời gian bắt đầu"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Thời gian kết thúc</label>
                                <input type="datetime-local" id="time_end" placeholder="Thời gian kết thúc"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lb">Số lượng</label>
                                <input type="number" id="number" placeholder="Số lượng"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label class="lb">Đơn giá tối thiểu</label>
                                <div class="d-flex justify-content-between align-items-center form-control">
                                    <input type="number" id="min_price" min="10000" style="width:calc (100% - 50px); border: none; outline: none">
                                    <span style="width: 50px" class="text-center">VNĐ</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="lb">Số xu hoàn tối đa</label>
                                <div class="d-flex justify-content-between align-items-center form-control">
                                    <input type="number" id="max_refund" style="width:calc (100% - 50px); border: none; outline: none">
                                    <span style="width: 50px" class="text-center">Xu</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="lb">Hoàn xu theo:</label>
                                <select class="form-control" id="style_refund_coin">
                                    <option value="1">số xu</option>
                                    <option value="2">phần trăm</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="lb">Hoàn xu:</label>
                                <input type="number" id="number_coin" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="first-name-floating-icon">Danh mục</label>
                                <select class="form-control" name="category_id" id="category_id">
                                    <?php foreach ($category as $value) :?>
                                    <option @isset($data) @if($value->id == $data->category_id) selected @endif @endisset value="{{$value->id}}">{{$value->name}}</option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
{{--                            <div class="form-group">--}}
{{--                                <label>Ảnh</label>--}}
{{--                                <div class="form-label-group">--}}
{{--                                    <input type='file' value="" accept="image/*" name="image" id="image" onchange="changeImg(this)">--}}
{{--                                    <br>--}}
{{--                                    <img width="120px" height="120px" id="avatar" class="thumbnail image-show-qr"--}}
{{--                                         src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/images/avt.jpg' ?>">--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    ><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/voucher') ?>" class="btn btn-danger"
                               style="color: white"><i class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<!-- BEGIN: Footer-->
@include('admin.base.footer')
<!-- END: Footer-->
@include('admin.base.script')
<script type="text/javascript">
    $(document).ready(function () {
        let image, min_price;
        $('.content-unit').show();
        $('.category-unit').hide();
        $(document).on("click", ".btn-add-account", function () {
            let url = window.location.href + '/voucher';
            let form_data = new FormData();
             min_price = $('#min_price').val();
            form_data.append('category_id', $('#category_id').val());
            form_data.append('style_refund_coin', $('#style_refund_coin').val());
            form_data.append('title', $('#title').val());
            form_data.append('min_price', min_price);
            form_data.append('code', $('#code').val());
            form_data.append('time_start', $('#time_start').val());
            form_data.append('time_end', $('#time_end').val());
            form_data.append('number', $('#number').val());
            form_data.append('max_refund', $('#max_refund').val());
            form_data.append('number_coin', $('#number_coin').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    if (data.status) {
                        swal('thành công',data.msg, 'success');
                        location.reload();
                    } else {
                        swal('thất bại',data.msg, 'error');
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("change", '#type', function () {
            if($(this).val() == 1){
                $('.content-unit').hide();
                $('.category-unit').show();
            } else {
                $('.content-unit').show();
                $('.category-unit').hide();
            }
        });
        if($('#type').val() == 1){
            $('.content-unit').hide();
            $('.category-unit').show();
        } else {
            $('.content-unit').show();
            $('.category-unit').hide();
        }
    });
</script>
<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>

