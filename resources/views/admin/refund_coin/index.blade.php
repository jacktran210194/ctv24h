<?php
    $page = 'refund_coin';
    ?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <a href="{{url('admin/refund_coin/add')}}" class="btn btn-success"><i class="fa fa-plus"
                                                                                      aria-hidden="true"></i> Thêm</a>
                    {{--                                        <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>--}}
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content">
                <!-- content -->
                <div class="table-responsive table-responsive-lg">
                    <table class="table data-list-view table-sm">
                        <thead>
                        <tr>
                            <th scope="col">Tiêu đề</th>
                            <th scope="col">Mã hoàn xu</th>
                            <th scope="col">Thời gian bắt đầu</th>
                            <th scope="col">Thời gian kết thúc</th>
                            <th scope="col">Số lượng</th>
                            <th scope="col">Đơn giá tối thiểu</th>
                            <th scope="col">Số xu hoàn tối đa</th>
                            <th scope="col">Trạng thái</th>
                            <th scope="col">Hoàn xu theo</th>
                            <th scope="col">Hoàn xu</th>
                            <th scope="col">Danh muc</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data))
                            @foreach($data as $value)
                                <tr>
                                    <td>{{$value->name}}</td>
                                    <td>{{$value->code_voucher}}</td>
                                    <td>{{$value->time_star}}</td>
                                    <td>{{$value->time_stop}}</td>
                                    <td>{{$value->quantity}}</td>
                                    <td>{{$value->min_price}}</td>
                                    <td>
                                        {{$value->max_point}}
                                    </td>
                                    <td>
                                        @if($value->quantity == 0)
                                            hết hàng
                                        @elseif($value->status == 1)
                                            đang hoạt động
                                        @elseif($value->status == 2)
                                            hết hạn sử dụng
                                         @elseif($value->status == 3)
                                            ẩn
                                         @else
                                        @endif
                                    </td>
                                    <td>
                                        @if($value->type == 1)
                                            số xu
                                        @else
                                            phần trăm
                                        @endif
                                    </td>
                                    <td>{{$value->refund_coin}}</td>
                                    <td>
                                        <?php
                                            $category = \App\Models\CategoryProductModel::where('id', $value->category_id)->first();
                                            echo $category->name;
                                        ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-info"
                                           href="{{url('admin/delete/edit/'.$value->id)}}"><i
                                                class="feather icon-edit"></i></a>
                                        <button
                                            class="btn btn-delete btn-danger"
                                            data-url="{{url('admin/refund_coin/delete/'.$value->id)}}">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>Không có dữ liệu</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-active", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            jQuery.noConflict();
            $('#myModal').modal('show');

        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

