<?php
$page = 'card';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Chọn nhà mạng</label>
                                <select name="" id="category_id" class="form-control">
                                    @foreach($category as $value)
                                        <option @isset($data) @if($data['category_id'] == $value['id']) selected @endif @endisset value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Chọn mệnh giá</label>
                                <select name="" id="price" class="form-control">
                                    <option @isset($data) @if($data['price'] == 10000) selected @endif @endisset value="10000">10.000 đ</option>
                                    <option @isset($data) @if($data['price'] == 20000) selected @endif @endisset value="20000">20.000 đ</option>
                                    <option @isset($data) @if($data['price'] == 50000) selected @endif @endisset value="50000">50.000 đ</option>
                                    <option @isset($data) @if($data['price'] == 100000) selected @endif @endisset value="100000">100.000 đ</option>
                                    <option @isset($data) @if($data['price'] == 200000) selected @endif @endisset value="200000">200.000 đ</option>
                                    <option @isset($data) @if($data['price'] == 300000) selected @endif @endisset value="300000">300.000 đ</option>
                                    <option @isset($data) @if($data['price'] == 500000) selected @endif @endisset value="500000">500.000 đ</option>
                                </select>
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="">Seri</label>
                                <input type="text" class="form-control" id="seri" value="<?=  isset($data) ? $data['seri'] : '' ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Mã thẻ</label>
                                <input type="text" class="form-control" id="code" value="<?= isset($data) ? $data['code'] : '' ?>">
                            </div>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/card/save') ?>"><i class="fa fa-floppy-o"
                                                                                    aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/card') ?>" class="btn btn-danger"><i
                                    class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).on("click", ".btn-add-account", function () {
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('id', $(this).attr('data-id'));
        form_data.append('seri', $('#seri').val());
        form_data.append('code', $('#code').val());
        form_data.append('category_id', $('#category_id').val());
        form_data.append('price', $('#price').val());
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                showLoading();
            },
            success: function (data) {
                if (data.status) {
                    window.location.href = data.url;
                } else {
                    alert(data.msg);
                }
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
