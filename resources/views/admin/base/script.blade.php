<?php

use Illuminate\Support\Facades\URL;

?>

<!-- BEGIN: Vendor JS-->
<script
    src="https://code.jquery.com/jquery-3.5.1.js"
    integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous"></script>

<script src="app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->
<!-- BEGIN: Page Vendor JS-->
{{--<script src="app-assets/vendors/js/charts/apexcharts.min.js"></script>--}}
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="app-assets/js/core/app-menu.js"></script>
<script src="app-assets/js/core/app.js"></script>
<script src="app-assets/js/scripts/components.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="app-assets/js/scripts/pages/dashboard-ecommerce.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>

<!-- END: Page JS-->

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".image-preview", function () {
            $('#overlay')
                .css({backgroundImage: `url(${this.src})`})
                .addClass('open')
                .one('click', function () {
                    $(this).removeClass('open');
                });
        });

        $('.popup_default').click(function () {
           $('.popup-ratings').show();
        });

        $('.close-popup').click(function () {
            $('.popup-ratings').hide();
        });

        $(document).on("click", ".btn-confirm", function () {
            let url = $(this).attr('data-url');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    alert(data.msg);
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-reject", function () {
            let url = $(this).attr('data-url');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    alert(data.msg);
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        })
    });
</script>

<script type="text/javascript">
    function changeImg(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                let html = '<img class="position-absolute border-radius-8" style="width: 100%; height: 100%; top: 0; left: 0; object-fit: cover" src="'+ e.target.result +'" >';
                $('.btn-image-1').html(html);
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function changeFrame(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //Thay đổi đường dẫn ảnh
                $('#avatar2').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function () {
        $('#avatar').click(function () {
            $('#img').click();
        });
    });
    $(document).ready(function () {
        $('#avatar2').click(function () {
            $('#img').click();
        });
    });
</script>
<script>
    function showLoading() {
        $('.loading').show();
    }

    function hideLoading() {
        $('.loading').hide();
    }
</script>
