<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <p class="message-modal">Bạn có muốn xoá ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-confirm" data-url="" data-dismiss="modal">Đồng ý
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="my-popup" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <p class="message-modal">Bạn có muốn <span class="status-action"></span> ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-edit" data-active="" data-url="" data-dismiss="modal">
                    Đồng ý
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Hủy</button>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="width: 135%!important; right: 10%!important;">
            <div class="modal-body">
                <h3 class="message-modal text-center text-red text-bold-700">Hướng dẫn nhập phân loại cho sản phẩm</h3>
                <div class="p-1">
                    VD: Sản phẩm của bạn chỉ có 1 phân loại là màu đen. Thì hãy nhập màu đen vào nhóm phân loại 1. Và
                    Freesize ở nhóm phân loại 2.
                </div>
                <img src="../../assets/admin/app-assets/images/icon_dashboard/phanloai_1.png" alt="">
                <img src="../../assets/admin/app-assets/images/icon_dashboard/phanloai_2.png" alt="">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-confirm" data-url="" data-dismiss="modal">Đồng ý
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal_rep" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="width: 135%!important; right: 10%!important;">
            <div class="modal-body">
                <h4 class="text-bold-700 text-center">Trả lời đánh giá</h4>
                <div class="p-1">
                    <div class="form-group">
                        <textarea name="" id="reply" cols="30" rows="6" class="form-control" placeholder="Trả lời của bạn"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_main btn-confirm-rep" data-url="" data-dismiss="modal">Trả lời
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Đóng</button>
            </div>
        </div>

    </div>
</div>


