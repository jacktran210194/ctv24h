<?php
$data_login = Session::get('data_supplier');
if (isset($data_login)) {
    $data_shop = \App\Models\ShopModel::where('supplier_id', $data_login->id)->first();
}
use Illuminate\Contracts\Session\Session;?>
<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto active"><a class="navbar-brand" href="{{url('/')}}">
                    <div class="brand-logo"><img src="../../assets/admin/app-assets/images/logo/logo_ctv.png"></div>
                    {{--                    <h2 class="brand-text mb-0">CTV24H - NCC</h2>--}}
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i
                        class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i
                        class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary"
                        data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item open @if($page == 'order_supplier' || $page == 'order_supplier_cancel' || $page == 'order_supplier_refund') bg-open @endif">
                <a class="d-flex align-items-center">@if($page == 'order_supplier' || $page == 'order_supplier_cancel' || $page == 'order_supplier_refund')
                        <img src="../../assets/admin/app-assets/images/menu/donhang_trang.png">@else<img
                            src="../../assets/admin/app-assets/images/menu/donhang_xam.png">@endif<span
                        class="menu-title" data-i18n="Dashboard">Đơn Hàng</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'order_supplier' ? 'active' : '' ?>"><a
                            href="{{url('admin/order/all')}}"><span class="menu-item">Tất cả</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'order_supplier_cancel' ? 'active' : '' ?>"><a
                            href="{{url('admin/order/cancel')}}"><span class="menu-item">Đơn huỷ</span></a></li>
                    <li class="popup_default <?= isset($page) && $page == 'order_supplier_refund' ? 'active' : '' ?>"><a><span class="menu-item">Hoàn tiền</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item open @if($page === 'shipping_supplier_setting' || $page === 'shipping_supplier') bg-open @endif">
                <a>@if($page == 'shipping_supplier_setting' || $page == 'shipping_supplier')<img
                        src="../../assets/admin/app-assets/images/menu/vanchuyen_trang.png">@else<img
                        src="../../assets/admin/app-assets/images/menu/vanchuyen_xam.png">@endif<span class="menu-title"
                                                                                                      data-i18n="Dashboard">Vận chuyển</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'shipping_supplier_setting' ? 'active' : '' ?> shipping_supplier_setting"><a
                            href="{{url('admin/order/shipping')}}"><span class="menu-item">Quản lý vận chuyển</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'shipping_supplier' ? 'active' : '' ?>"><a
                            href="{{url('admin/shipping_supplier')}}"><span class="menu-item">Cài đặt vận chuyển</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item open @if($page == 'product_supplier' || $page == 'product_supplier_trending' || $page == 'product_supplier_add' || $page == 'product_supplier_spam') bg-open @endif">
                <a>@if($page == 'product_supplier' || $page == 'product_supplier_trending' || $page == 'product_supplier_add' || $page == 'product_supplier_spam')
                        <img src="../../assets/admin/app-assets/images/menu/sanpham_trang.png">@else<img
                            src="../../assets/admin/app-assets/images/menu/sanpham_xam.png">@endif<span
                        class="menu-title" data-i18n="Dashboard">Sản phẩm</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'product_supplier' ? 'active' : '' ?>"><a
                            href="{{url('admin/product_supplier')}}"><span class="menu-item">Tất cả sản phẩm </span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'product_supplier_trending' ? 'active' : '' ?>"><a
                            href="{{url('admin/product_supplier/trending')}}"><span
                                class="menu-item">Sản phẩm trend</span></a>
                    </li>
                    @if(($data_shop->name_company != null && $data_shop->business != null && $data_shop->card_id != null && $data_shop->card_date_create != null && $data_shop->card_address != null && $data_shop->image_card_1 != null && $data_shop->image_card_2 != null && $data_shop->business_license != null && $data_shop->related_document_1 != null && $data_shop->related_document_2 != null) && $data_shop->active == 1)
                        <li class="<?= isset($page) && $page == 'product_supplier_add' ? 'active' : '' ?>"><a
                                href="{{url('admin/product_supplier/add')}}"><span
                                    class="menu-item">Thêm sản phẩm </span></a>
                        </li>
                    @else
                        <li class="<?= isset($page) && $page == 'product_supplier_add' ? 'active' : '' ?>"><a
                                href="{{url('admin/notification_update_info')}}"><span
                                    class="menu-item">Thêm sản phẩm </span></a>
                        </li>
                    @endif
                    <li class="popup_default <?= isset($page) && $page == 'product_supplier_spam' ? 'active' : '' ?>"><a><span
                                class="menu-item">Sản phẩm vi phạm</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item open @if($page == 'marketing' || $page == 'adv') bg-open @endif">
                <a>@if($page == 'marketing' || $page == 'ads')<img
                        src="../../assets/admin/app-assets/images/menu/marketing_trang.png">@else<img
                        src="../../assets/admin/app-assets/images/menu/marketing_xam.png">@endif<span class="menu-title"
                                                                                                      data-i18n="Dashboard">Marketing</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'marketing' ? 'active' : '' ?>"><a
                            href="{{url('admin/marketing_supplier')}}"><span
                                class="menu-item">Kênh marketing </span></a></li>
                    <li class="<?= isset($page) && $page == 'ads' ? 'active' : '' ?>"><a><span class="menu-item">Quảng cáo</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item open @if($page == 'shop_supplier' || $page == 'shop_review' || $page == 'category_shop_supplier' || $page == 'shop_update') bg-open @endif">
                <a href="">@if($page == 'shop_supplier' || $page == 'shop_review' || $page == 'category_shop_supplier' || $page == 'shop_update')
                        <img src="../../assets/admin/app-assets/images/menu/cuahang_trang.png">@else<img
                            src="../../assets/admin/app-assets/images/menu/cuahang_xam.png">@endif<span
                        class="menu-title" data-i18n="Dashboard">Cửa hàng</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'shop_review' ? 'active' : '' ?>"><a
                            href="{{url('admin/shop_supplier/review')}}"><span
                                class="menu-item">Đánh giá shop</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'shop_supplier' ? 'active' : '' ?>"><a
                            href="{{url('admin/shop_supplier/')}}"><span class="menu-item">Hồ sơ shop</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'category_shop_supplier' ? 'active' : '' ?>"><a
                            href="{{url('admin/shop_supplier/category')}}"><span class="menu-item">Danh mục shop</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'shop_update' ? 'active' : '' ?>"><a
                            href="{{url('admin/shop_supplier/update_info')}}"><span
                                class="menu-item">Cập nhật shop</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item open @if($page == 'data_sale' || $page == 'data_online') bg-open @endif">
                <a>@if($page == 'data_sale' || $page == 'data_online')<img
                        src="../../assets/admin/app-assets/images/menu/dulieubanhang_trang.png">@else<img
                        src="../../assets/admin/app-assets/images/menu/dulieubanhang_xam.png">@endif<span
                        class="menu-title" data-i18n="Dashboard">Hiệu quả hoạt động</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'data_sale' ? 'active' : '' ?>"><a
                            href="{{url('admin/data_sale')}}"><span class="menu-item">Phân tích bán hàng</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'data_online' ? 'active' : '' ?>"><a><span
                                class="menu-item">Hạng của Shop</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item open @if($page == 'financial_wallet_supplier' || $page == 'financial_banking_supplier' || $page == 'financial_wallet_revenue_supplier' || $page == 'financial_wallet_withdrawal_supplier' || $page == 'financial_wallet_refund_supplier') bg-open @endif">
                <a href="">@if($page == 'financial_wallet_supplier' || $page == 'financial_banking_supplier' || $page == 'financial_wallet_revenue_supplier' || $page == 'financial_wallet_withdrawal_supplier' || $page == 'financial_wallet_refund_supplier')
                        <img src="../../assets/admin/app-assets/images/menu/taichinh_trang.png">@else<img
                            src="../../assets/admin/app-assets/images/menu/taichinh_xam.png">@endif<span
                        class="menu-title" data-i18n="Dashboard">Tài chính</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'financial_wallet_revenue_supplier' ? 'active' : '' ?>"><a
                            href="{{url('admin/manage_financial/revenue')}}"><span
                                class="menu-item">Doanh thu</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'financial_wallet_supplier' ? 'active' : '' ?>"><a
                            href="{{url('admin/manage_financial/wallet/status_all')}}"><span class="menu-item">Ví</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'financial_payment_setting_supplier' ? 'active' : '' ?>"><a
                            href="{{url('admin/manage_financial/payment_setting')}}"><span class="menu-item">Thiết lập thanh toán</span></a>
                    </li>
                    {{--                    <li class=""><a @if($page == 'financial_wallet_revenue_supplier' || $page == 'financial_wallet_withdrawal_supplier' || $page == 'financial_wallet_refund_supplier') style="color: #36fb1e" @endif><span class="menu-item">Theo dõi tài chính</span></a>--}}
                    {{--                        <ul>--}}
                    {{--                            <li class="<?= isset($page) && $page == 'financial_wallet_withdrawal_supplier' ? 'active' : '' ?>"><a href="{{url('admin/manage_financial/revenue/withdrawal')}}"><span class="menu-item">Rút tiền</span></a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="<?= isset($page) && $page == 'financial_wallet_refund_supplier' ? 'active' : '' ?>"><a href="{{url('admin/manage_financial/revenue/refund')}}"><span class="menu-item">Hoàn tiền từ đơn hàng</span></a>--}}
                    {{--                            </li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                </ul>
            </li>
            <li class="nav-item open @if($page == 'manage_ctv' || $page == 'manage_dropship') bg-open @endif">
                <a>@if($page == 'manage_ctv' || $page == 'manage_dropship')<img
                        src="../../assets/admin/app-assets/images/menu/ctv_trang.png">@else<img
                        src="../../assets/admin/app-assets/images/menu/ctv_xam.png">@endif<span class="menu-title"
                                                                                                data-i18n="Dashboard">Cộng tác viên</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'manage_ctv' ? 'active' : '' ?>"><a
                            href="{{url('admin/manage_ctv')}}"><span class="menu-item">Danh sách CTV</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'manage_dropship' ? 'active' : '' ?>"><a
                            href="{{url('admin/manage_ctv/dropship')}}"><span class="menu-item">Hàng Dropship</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item open @if($page == 'address_ncc' || $page == 'shop_setting' || $page == 'profile') bg-open @endif">
                <a>@if($page == 'address_ncc' || $page == 'shop_setting' || $page == 'profile')<img
                        src="../../assets/admin/app-assets/images/menu/taikhoan_trang.png">@else<img
                        src="../../assets/admin/app-assets/images/menu/taikhoan_xam.png">@endif<span class="menu-title"
                                                                                                     data-i18n="Dashboard">Tài khoản</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'address_ncc' ? 'active' : '' ?>"><a
                            href="{{url('admin/profile/address')}}"><span class="menu-item">Địa chỉ</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'shop_setting' ? 'active' : '' ?>"><a
                            href="{{url('admin/profile/setting')}}"><span
                                class="menu-item">Thiết lập shop</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'profile' ? 'active' : '' ?>"><a
                            href="{{url('admin/profile')}}"><span class="menu-item">Thông tin tài khoản</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item open @if($page == 'introduction_member' || $page == 'introduction_product') bg-open @endif">
                <a>@if($page == 'introduction_member' || $page == 'introduction_member')<img
                        src="../../assets/admin/app-assets/images/menu/taikhoan_trang.png">@else<img
                        src="../../assets/admin/app-assets/images/menu/taikhoan_xam.png">@endif<span class="menu-title"
                                                                                                     data-i18n="Dashboard">Giới thiệu</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'introduction_member' ? 'active' : '' ?>"><a
                            href="{{url('admin/introduction/member')}}"><span class="menu-item">Giới thiệu thành viên</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'introduction_product' ? 'active' : '' ?>"><a
                            href="{{url('admin/introduction/product')}}"><span
                                class="menu-item">Giới thiệu sản phẩm</span></a>
                    </li>
                </ul>
            </li>

            {{--            <li class="<?= isset($page) && $page == 'supplier' ? 'active' : '' ?> nav-item"><a href="{{url('admin/supplier')}}"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Nhà cung cấp</span></a>--}}
            {{--            </li>--}}
{{--            <li class="nav-item"><a href=""><i class="fa fa-toggle-down"></i><span class="menu-title"--}}
{{--                                                                                   data-i18n="Dashboard">Voucher</span></a>--}}
{{--                <ul>--}}
{{--                    <li class="<?= isset($page) && $page == 'voucher_category' ? 'active' : '' ?>"><a--}}
{{--                            href="{{url('admin/my_voucher/category')}}"><i class="feather icon-circle"></i><span--}}
{{--                                class="menu-item">Danh mục voucher</span></a>--}}
{{--                    </li>--}}
{{--                    <li class="<?= isset($page) && $page == 'voucher' ? 'active' : '' ?>"><a--}}
{{--                            href="{{url('admin/my_voucher')}}"><i class="feather icon-circle"></i><span--}}
{{--                                class="menu-item">Danh sách voucher</span></a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            <li class="<?= isset($page) && $page == 'promotion' ? 'bg-open' : '' ?> nav-item"><a--}}
{{--                    href="{{url('admin/promotion')}}"><i class="fa fa-long-arrow-down"></i><span class="menu-title"--}}
{{--                                                                                                 data-i18n="Dashboard">Mã giảm giá của tôi</span></a>--}}
{{--            </li>--}}
{{--            <li class="<?= isset($page) && $page == 'key_search' ? 'bg-open' : '' ?> nav-item"><a--}}
{{--                    href="{{url('admin/key_search_supplier')}}"><i class="fa fa-search-plus"></i><span--}}
{{--                        class="menu-title" data-i18n="Dashboard">Xu hướng tìm kiếm</span></a>--}}
{{--            </li>--}}
{{--            --}}{{--            <li class="<?= isset($page) && $page == 'tranport_setting' ? 'active' : '' ?> nav-item"><a href="{{url('admin/tranport_setting')}}"><i class="fa fa-car"></i><span class="menu-title" data-i18n="Dashboard">Đơn vị vận chuyển</span></a>--}}
{{--            --}}{{--            </li>--}}
{{--            <li class="<?= isset($page) && $page == 'wallet' ? 'bg-open' : '' ?> nav-item"><a--}}
{{--                    href="{{url('admin/manage_wallet')}}"><i class="fa fa-money"></i><span class="menu-title"--}}
{{--                                                                                           data-i18n="Dashboard">Ví</span></a>--}}
{{--            </li>--}}
        </ul>
    </div>
</div>
