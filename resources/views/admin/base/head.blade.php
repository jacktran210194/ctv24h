<?php

use Illuminate\Support\Facades\URL;

?>
<head>
    <title><?= $title ?? '' ?></title>
    <base href="{{url('assets/admin')}}/" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicons.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/charts/apexcharts.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/order.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/home-dashboard.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/ctv_rank.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/dashboard-ecommerce.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/card-analytics.css">



    <!-- END: Page CSS-->

    <!--  Chart JS-->
    <!-- END: Chart JS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END: Custom CSS-->
    <style>
        .show-dropdown {
            display: block;
        }

        .position-center {
            left: 50%;
            top: 50%;
            -webkit-transform: translateX(-50%) translateY(-50%);
        }

        .modal-fixed {
            height: 100vh;
            position: fixed;
            z-index: 10000000;
            background-color: #00000080;
            top: 0;
            animation-name: animatetop;
            animation-duration: 0.4s;
        }
        .c-container {
            max-width: 27rem;
            margin: 1rem auto 0;
            padding: 1rem;
        }

        /*=======================
           O-Circle
        =========================*/

        .o-circle {
            display: flex;
            width: 10.555rem;
            height: 10.555rem;
            justify-content: center;
            align-items: flex-start;
            border-radius: 50%;
            animation: circle-appearance .8s ease-in-out 1 forwards, set-overflow .1s 1.1s forwards;
        }

        .c-container__circle {
            margin: 0 auto 5.5rem;
        }

        /*=======================
            C-Circle Sign
        =========================*/

        .o-circle__sign {
            position: relative;
            opacity: 0;
            background: #fff;
            animation-duration: .8s;
            animation-delay: .2s;
            animation-timing-function: ease-in-out;
            animation-iteration-count: 1;
            animation-fill-mode: forwards;
        }

        .o-circle__sign::before,
        .o-circle__sign::after {
            content: "";
            position: absolute;
            background: inherit;
        }

        .o-circle__sign::after {
            left: 100%;
            top: 0%;
            width: 500%;
            height: 95%;
            transform: translateY(4%) rotate(0deg);
            border-radius: 0;
            opacity: 0;
            animation: set-shaddow 0s 1.13s ease-in-out forwards;
            z-index: -1;
        }


        /*=======================
              Sign Success
        =========================*/

        .o-circle__sign--success {
            background: rgb(56, 176, 131);
        }

        .o-circle__sign--success .o-circle__sign {
            width: 1rem;
            height: 6rem;
            border-radius: 50% 50% 50% 0% / 10%;
            transform: translateX(130%) translateY(35%) rotate(45deg) scale(.11);
            animation-name: success-sign-appearance;
        }

        .o-circle__sign--success .o-circle__sign::before {
            bottom: -17%;
            width: 100%;
            height: 50%;
            transform: translateX(-130%) rotate(90deg);
            border-radius: 50% 50% 50% 50% / 20%;

        }

        /*--shadow--*/
        .o-circle__sign--success .o-circle__sign::after {
            background: rgb(40, 128, 96);
        }


        /*=======================
              Sign Failure
        =========================*/

        .o-circle__sign--failure {
            background: rgb(236, 78, 75);
        }

        .o-circle__sign--failure .o-circle__sign {
            width: 1rem;
            height: 7rem;
            transform: translateY(25%) rotate(45deg) scale(.1);
            border-radius: 50% 50% 50% 50% / 10%;
            animation-name: failure-sign-appearance;
        }

        .o-circle__sign--failure .o-circle__sign::before {
            top: 50%;
            width: 100%;
            height: 100%;
            transform: translateY(-50%) rotate(90deg);
            border-radius: inherit;
        }

        /*--shadow--*/
        .o-circle__sign--failure .o-circle__sign::after {
            background: rgba(175, 57, 55, .8);
        }


        /*-----------------------
              @Keyframes
        --------------------------*/

        /*CIRCLE*/
        @keyframes circle-appearance {
            0% {
                transform: scale(0);
            }

            50% {
                transform: scale(1.5);
            }

            60% {
                transform: scale(1);
            }

            100% {
                transform: scale(1);
            }
        }

        /*SIGN*/
        @keyframes failure-sign-appearance {
            50% {
                opacity: 1;
                transform: translateY(25%) rotate(45deg) scale(1.7);
            }

            100% {
                opacity: 1;
                transform: translateY(25%) rotate(45deg) scale(1);
            }
        }

        @keyframes success-sign-appearance {
            50% {
                opacity: 1;
                transform: translateX(130%) translateY(35%) rotate(45deg) scale(1.7);
            }

            100% {
                opacity: 1;
                transform: translateX(130%) translateY(35%) rotate(45deg) scale(1);
            }
        }


        @keyframes set-shaddow {
            to {
                opacity: 1;
            }
        }

        @keyframes set-overflow {
            to {
                overflow: hidden;
            }
        }
        .container-category-1 {
            width: 400px;
            padding-left: 20px;
            margin-bottom: 20px;
        }
        .container-category-2 {
            width: 400px;
            padding-left: 20px;
            margin-bottom: 20px;
        }
        .container-group input {
            width: 100%;
            box-shadow: 0px 4px 20px rgb(0 0 0 / 5%);
            border-radius: 8px;
            border: 1px solid #ECECEC;
            box-sizing: border-box;
            padding: 10px 5px;
            margin-bottom: 10px;
        }
        .container-group-2 input {
            width: 100%;
            box-shadow: 0px 4px 20px rgb(0 0 0 / 5%);
            border-radius: 8px;
            border: 1px solid #ECECEC;
            box-sizing: border-box;
            padding: 10px 5px;
            margin-bottom: 10px;
        }
        .btn-category {
            background: linear-gradient(
                180deg
                , #D1292F 0%, #FF5722 155.26%);
            border-radius: 8px;
            width: 150px;
            height: 40px;
            border: none;
            color: #fff;
            cursor: pointer;
        }
        .btn-category:focus{
            outline: none;
        }
        .table-category-list {
            width: 600px;
            margin-left: 20px;
            border: 1px solid #ECECEC;
            box-sizing: border-box;
            box-shadow: 0px 4px 20px rgb(0 0 0 / 5%);
            border-radius: 8px;
        }
        .header-table {
            background: #ECECEC;
            border-radius: 8px 8px 0px 0px;
            padding: 10px 15px;
        }
        .header-table p {
            margin: 0;
        }
        .body-table .content-box {
            width: calc(100%/3);
            padding: 15px;
            position: relative;
        }
        .body-table .content-box input {
            width: 100%;
            border: none;
            outline: none;
        }
        .body-table .content-box label {
            position: absolute;
            top: 50%;
            right: 5px;
            transform: translate(-50%, -50%);
        }
        .border-bottom{
            border-bottom: 1px solid #ECECEC;
        }
        .container-box{
            width: calc(100% / (2/3));
            border-left: 1px solid #ececec;
        }
        .container-box .content-box{
            width: 50%;
            border-right: 1px solid #ececec;
        }
        .container-box .content-type{
            width: 20%;
        }
        .border-bottom:last-child{
            border-bottom: none;
        }
        .loading_data {
            width: 100%;
            height: 100vh;
            background-color: #00000080;
            display: none;
            position: absolute;
            z-index: 1000;
        }

        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
            position: absolute;
            top: 36%;
            left: 46%;
        }
    </style>
</head>
<div class="loading">
</div>
<div class="loading_data">
    <div class="loader"></div>
</div>
<div id="overlay"></div>
<div id="overlay1"></div>
