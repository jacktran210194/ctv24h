<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto active"><a class="navbar-brand" href="{{url('admin')}}">
                    <div class="brand-logo"><img src="../../assets/admin/app-assets/images/logo/logo_ctv.png"></div>
{{--                    <h2 class="brand-text mb-0">CTV24H | Admin</h2>--}}
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i
                        class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i
                        class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary"
                        data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="<?= isset($page) && $page == 'home' ? 'active' : '' ?> nav-item"><a href="{{url('admin')}}"><i
                        class="feather icon-home"></i><span class="menu-title"
                                                            data-i18n="Dashboard">Dashboard</span></a>
            </li>
            <li class=" navigation-header"><span>Quản lý</span>
            </li>
            <li class=" nav-item"><a href="#"><i class="fa fa-clone"></i><span
                        class="menu-title">Danh mục sản phẩm</span></a>
                <ul class="menu-content">
                    <li class="<?= isset($page) && $page == 'category_product' ? 'active' : '' ?>"><a
                            href="{{url('admin/category_product')}}"><i class="feather icon-circle"></i><span
                                class="menu-item">Danh mục cha</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'category_product_child' ? 'active' : '' ?>"><a
                            href="{{url('admin/category_product_child')}}"><i class="feather icon-circle"></i><span
                                class="menu-item">Danh mục con</span></a>
                    </li>
                    <li class=" {{$page}} <?= isset($page) && $page == 'sub_category_product_child' ? 'active' : '' ?>"><a
                            href="{{url('admin/sub_category_product_child')}}"><i class="feather icon-circle"></i><span
                                class="menu-item">Danh mục con cấp 2</span></a>
                    </li>
                </ul>
            </li>
            <li class="<?= isset($page) && $page == 'trending_product' ? 'active' : '' ?>"><a
                    href="{{url('admin/product/trending')}}"><i class="feather icon-circle"></i><span
                        class="menu-item">Sản phẩm trend</span></a>
            </li>
            <li class="<?= isset($page) && $page == 'brand' ? 'active' : '' ?> nav-item"><a
                    href="{{url('admin/brand')}}"><i class="fa fa-bars"></i><span class="menu-title"
                                                                                  data-i18n="Dashboard">Thương hiệu</span></a>
            </li>
            <li class="<?= isset($page) && $page == 'shop_confirm' ? 'active' : '' ?> nav-item"><a
                    href="{{url('admin/shop_confirm')}}"><i class="fa fa-home"></i></i><span class="menu-title"
                                                                                  data-i18n="Dashboard">Xét duyệt cửa hàng</span></a>
            </li>
            <li class="<?= isset($page) && $page == 'supplier' ? 'active' : '' ?> nav-item"><a
                    href="{{url('admin/supplier')}}"><i class="feather icon-home"></i><span class="menu-title"
                                                                                            data-i18n="Dashboard">Nhà cung cấp</span></a>
            </li>
            <li class="<?= isset($page) && $page == 'shipping' ? 'active' : '' ?> nav-item"><a
                    href="{{url('admin/shipping')}}"><i class="feather icon-home"></i><span class="menu-title"
                                                                                            data-i18n="Dashboard">Đơn vị vận chuyển</span></a>
            </li>
            <li class="<?= isset($page) && $page == 'collaborator' ? 'active' : '' ?> nav-item"><a
                    href="{{url('admin/collaborator')}}"><i class="feather icon-user"></i><span class="menu-title"
                                                                                                data-i18n="Dashboard">Cộng tác viên</span></a>
            </li>

{{--            <li class=" nav-item"><a href="#"><i class="feather icon-calendar"></i><span--}}
{{--                        class="menu-title">Cửa hàng</span></a>--}}
{{--                <ul class="menu-content">--}}
{{--                    <li class="<?= isset($page) && $page == 'shop' ? 'active' : '' ?>"><a--}}
{{--                            href="{{url('admin/shop')}}"><i class="feather icon-circle"></i><span class="menu-item">Danh sách cửa hàng</span></a>--}}
{{--                    </li>--}}
{{--                    <li class="<?= isset($page) && $page == 'category_shop' ? 'active' : '' ?>"><a--}}
{{--                            href="{{url('admin/shop/category')}}"><i class="feather icon-circle"></i><span--}}
{{--                                class="menu-item">Danh mục cửa hàng</span></a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            <li class=" nav-item"><a href="#"><i class="feather icon-calendar"></i><span--}}
{{--                        class="menu-title">Sản phẩm</span></a>--}}
{{--                <ul class="menu-content">--}}
{{--                    <li class="<?= isset($page) && $page == 'product' ? 'active' : '' ?>"><a--}}
{{--                            href="{{url('admin/product')}}"><i class="feather icon-circle"></i><span class="menu-item">Danh sách sản phẩm</span></a>--}}
{{--                    </li>--}}
{{--                    <li class="<?= isset($page) && $page == 'trending_product' ? 'active' : '' ?>"><a--}}
{{--                            href="{{url('admin/product/trending')}}"><i class="feather icon-circle"></i><span--}}
{{--                                class="menu-item">Sản phẩm trend</span></a>--}}
{{--                    </li>--}}
{{--                    <li class="<?= isset($page) && $page == 'sponsored_product' ? 'active' : '' ?> nav-item"><a--}}
{{--                            href="{{url('admin/product/sponsored')}}"><i class="feather icon-circle"></i><span--}}
{{--                                class="menu-title" data-i18n="Dashboard">Sản phẩm được tài trợ</span></a>--}}
{{--                    </li>--}}
{{--                    --}}{{--                    <li class="<?= isset($page) && $page == 'category_product' ? 'active' : '' ?>"><a href="{{url('admin/product/category')}}"><i class="feather icon-circle"></i><span class="menu-item">Danh mục sản phẩm</span></a>--}}
{{--                    --}}{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
            <li class="<?= isset($page) && $page == 'key_search' ? 'active' : '' ?> nav-item"><a
                    href="{{url('admin/key_search')}}"><i class="feather icon-home"></i><span class="menu-title"
                                                                                              data-i18n="Dashboard">Xu hướng tìm kiếm</span></a>
            </li>
            <li class="<?= isset($page) && $page == 'hot_key' ? 'active' : '' ?> nav-item"><a
                    href="{{url('admin/hot_key')}}"><i class="feather icon-home"></i><span class="menu-title"
                                                                                           data-i18n="Dashboard">Từ khóa hot</span></a>
            </li>
            <li class="<?= isset($page) && $page == 'refund_coin' ? 'active' : '' ?> nav-item"><a
                    href="{{url('admin/refund_coin')}}"><i class="feather icon-home"></i><span class="menu-title"
                                                                                           data-i18n="Dashboard">Hoàn xu đơn bất kỳ</span></a>
            </li>
{{--            <li class="nav-item"><a href=""><i class="feather icon-user"></i><span class="menu-title"--}}
            <li class="nav-item <?= isset($page) && $page == 'voucher' ? 'active' : '' ?>"><a href="{{url('admin/voucher')}}"><i class="feather icon-user"></i><span class="menu-title"
                                                                                   data-i18n="Dashboard">Phiếu giảm giá</span></a>
{{--                <ul>--}}
{{--                    <li class="<?= isset($page) && $page == 'voucher_category' ? 'active' : '' ?>"><a--}}
{{--                            href="{{url('admin/voucher/category')}}"><i class="feather icon-circle"></i><span--}}
{{--                                class="menu-item">Danh mục voucher</span></a>--}}
{{--                    </li>--}}
{{--                    <li class="<?= isset($page) && $page == 'voucher' ? 'active' : '' ?>"><a--}}
{{--                            href="{{url('admin/voucher')}}"><i class="feather icon-circle"></i><span class="menu-item">Danh sách voucher</span></a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
            </li>

            <li class="nav-item"><a href=""><i class="feather icon-user"></i><span class="menu-title"
                                                                                   data-i18n="Dashboard">Quản lý nạp thẻ</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'category_card' ? 'active' : '' ?>"><a
                            href="{{url('admin/card/category')}}"><i class="feather icon-circle"></i><span
                                class="menu-item">Nhà mạng</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'card' ? 'active' : '' ?>"><a
                            href="{{url('admin/card')}}"><i class="feather icon-circle"></i><span class="menu-item">Danh sách thẻ cào</span></a>
                    </li>
                </ul>
            <li class=" nav-item"><a href="#"><i class="fa fa-clone"></i><span
                        class="menu-title">Liên kết vận chuyển</span></a>
                <ul class="menu-content">
                    <li class="<?= isset($page) && $page == 'ghtk' ? 'active' : '' ?>"><a
                            href="{{url('admin/delivery/ghtk')}}"><i class="feather icon-circle"></i><span
                                class="menu-item">Giao hàng tiết kiệm</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'j_and_t' ? 'active' : '' ?>"><a
                            href="{{url('admin/delivery/j_and_t')}}"><i class="feather icon-circle"></i><span
                                class="menu-item">J&T</span></a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item"><a href="#"><i class="fa fa-credit-card-alt"></i><span class="menu-title">Liên kết thanh toán</span></a>
                <ul class="menu-content">
                    <li class="<?= isset($page) && $page == 'zalo_pay' ? 'active' : '' ?>"><a
                            href="{{url('admin/payment_method/zalo_pay')}}"><i class="feather icon-circle"></i><span
                                class="menu-item">ZaloPay</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'momo' ? 'active' : '' ?>"><a
                            href="{{url('admin/payment_method/momo')}}"><i class="feather icon-circle"></i><span
                                class="menu-item">Momo</span></a>
                    </li>
                </ul>
            </li>
            <li class="<?= isset($page) && $page == 'notification' ? 'active' : '' ?> nav-item"><a
                    href="{{url('admin/notification')}}"><i class="feather icon-bell"></i><span class="menu-title"
                                                                                           data-i18n="Dashboard">Thông báo</span></a>
            </li>
            <li class="<?= isset($page) && $page == 'fee_ctv24h' ? 'active' : '' ?> nav-item"><a
                    href="{{url('admin/fee_ctv24h')}}"><i class="feather icon-home"></i><span class="menu-title"
                                                                                            data-i18n="Dashboard">Phí sàn CTV24H</span></a>
            </li>
            <li class="<?= isset($page) && $page == 'revenue_ctv24h' ? 'active' : '' ?> nav-item"><a
                    href="{{url('admin/revenue_ctv24h')}}"><i class="fa fa-money"></i><span class="menu-title"
                                                                                              data-i18n="Dashboard">Doanh thu sàn CTV24H</span></a>
            </li>
        </ul>
    </div>
</div>
