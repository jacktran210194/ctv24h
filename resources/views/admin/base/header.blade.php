<?php
$data_admin = Session::get('data_user');
$data_supplier = Session::get('data_supplier');
$data_collaborator = Session::get('data_collaborator');
?>
<ul class="nav navbar-nav float-right">
    <li class="dropdown dropdown-user nav-item">
        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
            <div class="user-nav d-sm-flex d-none"><span
                    class="user-name text-bold-600">
                    @if(isset($data_admin))
                        {{$data_admin['name']}}
                    @elseif(isset($data_supplier))
                        {{$data_supplier['name']}}
                    @elseif(isset($data_collaborator))
                        {{$data_collaborator['name']}}
                    @endif
                </span>
                <span
                    class="user-status">
                    @if(isset($data_admin))
                        <span class="badge badge-success">Admin</span>
                    @elseif(isset($data_supplier))
                        <span class="badge badge-primary">Nhà cung cấp</span>
                    @elseif(isset($data_collaborator))
                        <span class="badge badge-danger">Cộng tác viên</span>
                    @endif
                </span></div>
            <span><img class="round" src="app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar"
                       height="40" width="40"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" id="myDropdown">
            {{--            <a class="dropdown-item" href="{{url('admin/change_password')}}"><i--}}
            {{--                    class="feather icon-message-square"></i> Change Password</a>--}}
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{url('admin/logout')}}"><i class="feather icon-power"></i>
                Logout</a>
        </div>
    </li>
</ul>

<div class="popup-ratings show-popup-ratings" style="display: none">
    <div class="content-rating">
        <button class="close-popup"><img src="http://ctv24h.vn/assets/frontend/Icon/home/Danh_muc/icon_close.png"></button>
        <div class="content-image">
            <img src="http://ctv24h.vn/assets/frontend/Icon/home/Danh_muc/Component.png">
        </div>
    </div>
</div>
