<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto active"><a class="navbar-brand" href="{{url('admin/order_collaborator/all')}}"><div class="brand-logo"><img src="../../assets/admin/app-assets/images/logo/logo_ctv.png"></div></a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item open @if($page == 'order_collaborator' || $page == 'order_supplier_buy' || $page == 'order_supplier_refund') bg-open @endif"><a class="d-flex align-items-center">
                    @if($page == 'order_collaborator' || $page == 'order_supplier_buy' || $page == 'order_supplier_refund')
                        <img src="../../assets/admin/app-assets/images/menu/donhang_trang.png">
                    @else
                        <img src="../../assets/admin/app-assets/images/menu/donhang_xam.png">
                    @endif
                    <span class="menu-title" data-i18n="Dashboard">Đơn Hàng</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'order_collaborator' ? 'active' : '' ?>"><a href="{{url('admin/order_collaborator/all')}}"><span class="menu-item">Tất cả</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'order_supplier_buy' ? 'active' : '' ?>"><a href="{{url('my_history/by_product')}}" ><span class="menu-item">Đơn mua</span></a></li>
                    <li class="<?= isset($page) && $page == 'order_supplier_refund' ? 'active' : '' ?>"><a href="{{url('admin/order_collaborator/return_money')}}"><span class="menu-item">Hoàn tiền</span></a>
                    </li>
                    <li><a><span class="menu-item">Dữ liệu KH</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item open">
                <a>@if($page == 'shipping_supplier_setting' || $page == 'shipping_supplier')<img
                        src="../../assets/admin/app-assets/images/menu/vanchuyen_trang.png">@else<img
                        src="../../assets/admin/app-assets/images/menu/vanchuyen_xam.png">@endif<span class="menu-title"
                                                                                                      data-i18n="Dashboard">Vận chuyển</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'shipping_supplier_setting' ? 'active' : '' ?> shipping_supplier_setting"><a><span style="color: #D6D6D6!important;" class="menu-item">Quản lý vận chuyển</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'shipping_supplier' ? 'active' : '' ?>"><a><span style="color: #D6D6D6!important;" class="menu-item">Cài đặt vận chuyển</span></a>
                    </li>
                </ul>
            </li>
{{--            <li class="nav-item @if($page == 'rank_ctv') bg-open @endif"><a href="{{url('admin/collaborator_rank')}}" class="d-flex align-items-center">--}}
{{--                    @if($page == 'rank_ctv')--}}
{{--                        <img src="../../assets/admin/app-assets/images/menu/hangctv_xam.png">--}}
{{--                    @else--}}
{{--                        <img src="../../assets/admin/app-assets/images/menu/hangctv_xam.png">--}}
{{--                    @endif--}}
{{--                    <span class="menu-title" data-i18n="Dashboard">Hạng CTV</span></a>--}}
{{--            </li>--}}
            <li class="nav-item @if($page == 'manage_supplier') bg-open @endif"><a href="{{url('admin/manage_supplier')}}">
                    @if($page == 'manage_supplier')
                        <img src="../../assets/admin/app-assets/images/menu/ncc_xam.png">
                    @else
                        <img src="../../assets/admin/app-assets/images/menu/ncc_xam.png">
                    @endif
                    <span class="menu-title" data-i18n="Dashboard">Nhà cung cấp</span></a>
            </li>
            <li class="nav-item @if($page == 'collaborator_product') bg-open @endif"><a href="{{url('/admin/collaborator_product')}}">
                    <img src="../../assets/admin/app-assets/images/icons/product.png">
                    <span class="menu-title" data-i18n="Dashboard">Sản phẩm đang bán</span></a>
            </li>
            <li class="nav-item open @if($page == 'collaborator/my-shop') bg-open @endif"><a href="{{url('admin/collaborator/my-shop')}}"><img src="../../assets/admin/app-assets/images/icons/shop.png"><span class="menu-title" data-i18n="Dashboard">Shop của tôi</span></a>
            </li>
            <li class="nav-item open @if($page == 'collaborator_shop/ratings-shop' || $page == 'collaborator_shop/file-shop' || $page == 'collaborator_shop/decorate-shop' || $page == 'collaborator_shop/update-ctv') bg-open @endif">
                <a>
                    @if($page == 'collaborator_shop/ratings-shop' || $page == 'collaborator_shop/file-shop' || $page == 'collaborator_shop/decorate-shop' || $page == 'collaborator_shop/update-ctv')
                        <img src="../../assets/admin/app-assets/images/menu/cuahang_trang.png">
                    @else
                        <img src="../../assets/admin/app-assets/images/menu/cuahang_xam.png">
                    @endif
                    <span class="menu-title" data-i18n="Dashboard">Cửa hàng</span>
                </a>
                <ul>
                    <li class="<?= isset($page) && $page == 'collaborator_shop/ratings-shop' ? 'active' : '' ?>"><a href="{{url('admin/collaborator_shop/ratings-shop')}}"><span class="menu-item">Đánh giá shop</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'collaborator_shop/file-shop' ? 'active' : '' ?>"><a href="{{url('admin/collaborator_shop/file-shop')}}"><span class="menu-item">Hồ sơ shop</span></a>
                    </li>
                    <li class=""><a><span class="menu-item" style="color: #D6D6D6">Danh mục của shop</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'collaborator_shop/decorate-shop' ? 'active' : '' ?>"><a href="{{url('admin/collaborator_shop/decorate-shop')}}"><span style="color: #D6D6D6" class="menu-item">Trang trí shop</span></a>
                    </li>
                    <li>
                        <a>
                            <span class="menu-item" style="color: #D6D6D6">Cập nhật NCC</span>
                        </a>
                    </li>
                    <li class="<?= isset($page) && $page == 'collaborator_shop/update-ctv' ? 'active' : '' ?>">
                        <a href="{{url('admin/collaborator_shop/update-ctv')}}">
                            <span class="menu-item">Cập nhật CTV</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item open @if($page == 'collaborator/data_sale') bg-open @endif"><a><img src="../../assets/admin/app-assets/images/icons/Sales_data.png"><span class="menu-title" data-i18n="Dashboard">Hiệu quả hoạt động</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'collaborator/data_sale' ? 'active' : '' ?>"><a href="{{url('admin/collaborator/data_sale')}}"><span class="menu-item">Phân tích bán hàng</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'data_sell' ? 'active' : '' ?>"><a ><span style="color: #D6D6D6" class="menu-item">Hạng của shop</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'rank_ctv' ? 'active' : '' ?>"><a href="{{url('admin/collaborator_rank')}}"><span class="menu-item">Hạng của CTV</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item open @if($page == 'collaborator_financial_revenue' || $page == 'collaborator_financial_wallet' || $page == 'collaborator_financial_payment_setting') bg-open @endif"><a class="d-flex align-items-center">
                    @if($page == 'collaborator_financial_revenue' || $page == 'collaborator_financial_wallet' || $page == 'collaborator_financial_payment_setting')
                        <img src="../../assets/admin/app-assets/images/menu/taichinh_trang.png">
                    @else
                        <img src="../../assets/admin/app-assets/images/menu/taichinh_xam.png">
                    @endif
                    <span class="menu-title" data-i18n="Dashboard">Tài chính</span></a>
                <ul>
                    <li class="<?= isset($page) && $page == 'collaborator_financial_revenue' ? 'active' : '' ?>"><a href="{{url('admin/collaborator_financial/revenue')}}"><span class="menu-item">Doanh thu</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'collaborator_financial_wallet' ? 'active' : '' ?>"><a href="{{url('admin/collaborator_financial/wallet')}}"><span class="menu-item">Ví</span></a>
                    </li>
                    <li class="<?= isset($page) && $page == 'collaborator_financial_payment_setting' ? 'active' : '' ?>"><a href="{{url('admin/collaborator_financial/payment_setting')}}"><span class="menu-item">Thiết lập thanh toán</span></a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
