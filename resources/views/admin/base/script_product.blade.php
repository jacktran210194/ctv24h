<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-show-list", function () {
            $('.input_list_image').click();
        });
        $(document).on("click", ".btn-image-1", function () {
            $('.input_image').click();
        });
        $(document).on('change', '.check_attr', function () {
            if ($(this).is(':checked')) {
                $('.attr-product').show();
            } else {
                $('.attr-product').hide();
            }
        });
        $(document).on('change', '.check_price_discount', function () {
            if ($(this).is(':checked')) {
                $('.form_discount').show();
            } else {
                $('.form_discount').hide();
            }
        });
        $(document).on('change', '.check_auction', function () {
            if ($(this).is(':checked')) {
                $('.form_auction').show();
            } else {
                $('.form_auction').hide();
            }
        });
        $(document).on('change', '.check_booking', function () {
            if ($(this).is(':checked')) {
                $('.form_booking').show();
            } else {
                $('.form_booking').hide();
            }
        });
        $(".btn-delete-group").on("click", function () {
            $(".more-email").children().last().remove();
        });
        $(document).on('click', '.btn-delete-value', function () {
            $(this).parents('.form_attr:first').find('.form-1').children().last().remove();
        });

        $(document).on("click", ".btn-delete", function () {
            // let url = $(this).attr('data-url');
            // $('.btn-confirm').attr('data-url', url);
            $('#myModal1').modal('show');
        });
    });
</script>
