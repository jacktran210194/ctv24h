<?php
$page = 'category_card';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Tên nhà mạng</label>
                                <input required type="text" name="name" id="name"
                                       value="<?= isset($data) ? $data['name'] : '' ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Phân loại</label>
                                <select name="" id="type" class="form-control">
                                    <option @isset($data) @if($data['type'] == 0) selected @endif @endisset value="0">Thẻ điện thoại</option>
                                    <option @isset($data) @if($data['type'] == 1) selected @endif @endisset value="1">Thẻ game</option>
                                </select>
                            </div>

                            @isset($data['active'])
                                <div class="form-group">
                                    <label>Trạng thái</label>
                                    <select name="" id="active" class="form-control">
                                        <option @isset($data) @if($data['active'] == 1) selected @endif @endisset value="1">Hoạt động</option>
                                        <option @isset($data) @if($data['active'] == 0) selected @endif @endisset value="0">Bảo trì</option>
                                    </select>
                                </div>
                            @endisset
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="form-label-group">
                                    <input type='file' value="" accept="image/*" name="image" id="image"
                                           onchange="changeImg(this)">
                                    <br>
                                    <img height="40px" width="auto" id="avatar" class="thumbnail image-show-qr"
                                         src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/images/avt.jpg' ?>">
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/card/category/save') ?>"><i class="fa fa-floppy-o"
                                                                                    aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/card/category') ?>" class="btn btn-danger"><i
                                    class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).on("click", ".btn-add-account", function () {
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('id', $(this).attr('data-id'));
        form_data.append('name', $('#name').val());
        form_data.append('type', $('#type').val());
        form_data.append('active', $('#active').val());
        form_data.append('image', $('#image')[0].files[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                showLoading();
            },
            success: function (data) {
                if (data.status) {
                    window.location.href = data.url;
                } else {
                    alert(data.msg);
                }
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
