<?php
$page = 'trending_product';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <!-- content -->
                <div class="table-responsive table-responsive-lg">
                    <table class="table data-list-view table-sm">
                        <thead>
                        <tr>
                            <th scope="col">Tên sản phẩm</th>
                            <th scope="col">Hình ảnh</th>
                            <th scope="col">SKU</th>
                            <th scope="col">Danh mục</th>
                            <th scope="col">Giá</th>
                            <th scope="col">Kho</th>
                            <th scope="col">Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data))
                            @foreach($data as $value)
                                <tr>
                                    <td>{{$value->name}}</td>
                                    <td>
                                        <img width="40px" height="auto" class="image-preview" src="{{$value->image}}"
                                             alt="">
                                    </td>
                                    <td>{{$value->sku_type}}</td>
                                    <td>{{$value->category_product}}</td>
                                    <td>{{number_format($value->price)}} đ</td>
                                    <td>{{$value->warehouse}}</td>
                                    <td>
                                        <?php if($value->is_trending == 1) :?>
                                        <button
                                            class="btn btn-delete btn-success"
                                            data-msg="Bạn có muốn thêm sản phẩm này làm phẩm sẩn trend?"
                                            data-url="{{url('admin/product/trending/active/'.$value->id)}}"
                                        >Kích hoạt
                                        </button>
                                        <?php endif ;?>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">Không có dữ liệu</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <p class="message-modal">Bạn có muốn xoá ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-confirm_1" data-url="" data-dismiss="modal">Đồng ý</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm_1').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            jQuery.noConflict();
            $('#myModal').modal('show');
        });

        $(document).on("click", ".btn-confirm_1", function () {
            let url = $(this).attr('data-url');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    window.location.href = data.url;
                },
                error: function () {
                    console.log('error')
                },
            });
        });

    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
