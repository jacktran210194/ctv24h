<?php
$page = 'revenue_ctv24h';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/financial/style.css')}}">
<script src="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.js"></script>
<link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.css"/>
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<!-- END: Head-->
<style>
    .popup-add-bank-account {
        position: fixed;
        top: 0;
        left: 10%;
        width: 85%;
        height: 100%;
        z-index: 10;
        display: none;
    }

    .popup-add-bank-account .show-pop {
        display: block;
        z-index: 20;
    }

    .popup-add-bank-account .container-popup-add-bank-account {
        width: 40%;
        margin: 20px auto;
        background: #fff;
        border-radius: 16px;
        padding: 30px;
        border: 1px solid #005DB6;
    }

    .container-popup-add-bank-account .header-popup-add-bank-account {
        margin-bottom: 10px;
    }
</style>

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <div class="float-right"><h3>Ví của sàn CTV24H</h3><h3 class="text-red">{{number_format($wallet_ctv24h->price)}} đ</h3></div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 p-0">
                        <div id="body-unit">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->
</div>

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-app.js"></script>--}}

{{--<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->--}}
{{--<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-analytics.js"></script>--}}

{{--<!-- Add Firebase products that you want to use -->--}}
{{--<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-auth.js"></script>--}}
{{--<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-firestore.js"></script>--}}
{{--<script src="{{url('assets/admin/js/otp/index.js')}}"></script>--}}
<script>
    $(document).ready(function () {
        //Lịch sử các giao dịch gần đây
        let url_filter = window.location.origin;
        let url = url_filter + '/admin/revenue_ctv24h/status_all';
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $('.status-all').addClass('checked');
                $('.status-revenue').removeClass('checked');
                $('.status-withdrawal').removeClass('checked');
                $('.status-refund').removeClass('checked');
                $('.status-bonus').removeClass('checked');
                $('#body-unit').html(data);
            },
            error: function () {
                console.log('error')
            }
        });

        //Tất cả
        $(document).on("click", ".status-all", function () {
            let url = url_filter + '/admin/revenue_ctv24h/status_all';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.status-all').addClass('checked');
                    $('.status-revenue').removeClass('checked');
                    $('.status-withdrawal').removeClass('checked');
                    $('.status-refund').removeClass('checked');
                    $('.status-bonus').removeClass('checked');
                    $('#body-unit').html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Doanh thu
        $(document).on("click", ".status-revenue", function () {
            let url = url_filter + '/admin/revenue_ctv24h/status_revenue';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.status-all').removeClass('checked');
                    $('.status-revenue').addClass('checked');
                    $('.status-withdrawal').removeClass('checked');
                    $('.status-refund').removeClass('checked');
                    $('.status-bonus').removeClass('checked');
                    $('#body-unit').html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Rút tiền
        $(document).on("click", ".status-withdrawal", function () {
            let url = url_filter + '/admin/revenue_ctv24h/status_withdrawal';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.status-all').removeClass('checked');
                    $('.status-revenue').removeClass('checked');
                    $('.status-withdrawal').addClass('checked');
                    $('.status-refund').removeClass('checked');
                    $('.status-bonus').removeClass('checked');
                    $('#body-unit').html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Hoàn tiền
        $(document).on("click", ".status-refund", function () {
            let url = url_filter + '/admin/revenue_ctv24h/status_refund';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.status-all').removeClass('checked');
                    $('.status-revenue').removeClass('checked');
                    $('.status-withdrawal').removeClass('checked');
                    $('.status-refund').addClass('checked');
                    $('.status-bonus').removeClass('checked');
                    $('#body-unit').html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        // Hoa hồng giới thiệu
        $(document).on("click", ".status-bonus", function () {
            let url = url_filter + '/admin/revenue_ctv24h/status_bonus';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.status-all').removeClass('checked');
                    $('.status-revenue').removeClass('checked');
                    $('.status-withdrawal').removeClass('checked');
                    $('.status-refund').removeClass('checked');
                    $('.status-bonus').addClass('checked');
                    $('#body-unit').html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

    });
</script>
</body>
<!-- END: Footer-->

<!-- END: Body-->
</html>

