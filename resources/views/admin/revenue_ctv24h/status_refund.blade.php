<div class="card">
    <div class="card-content">
        <div class="m-2">
            <span class="title-payment-near">Các giao dịch gần đây</span>
        </div>
        <div class="d-flex table-head m-2">
            <div class="d-flex col-lg-6 align-items-center justify-content-lg-around">
                <div class="col-lg-3">
                    <span>Chọn ngày</span>
                </div>
                <div class="col-lg-5">
                    <input type="date" class="form-control mr-1">
                </div>
                <div class="col-lg-5">
                    <input type="date" class="form-control">
                </div>
            </div>
        </div>
        <div class="main pl-2 pr-2">
            <div class="main-unit align-items-center">
                <div class="d-flex">
                    <div
                        class="tab-menu col-lg-2 col-md-12 align-items-center status-all">
                        <center>
                            <a data-url-all="{{url('admin/revenue_ctv24h/status_all')}}">Tất cả</a>
                        </center>
                    </div>
                    <div
                        class="tab-menu col-lg-2 col-md-12 align-items-center status-revenue">
                        <center>
                            <a data-url-revenue="{{url('admin/revenue_ctv24h/status_revenue')}}">Doanh thu</a>
                        </center>
                    </div>
                    <div
                        class="tab-menu col-lg-2 col-md-12 align-items-center status-withdrawal">
                        <center>
                            <a data-url-withdrawal="{{url('admin/revenue_ctv24h/status_withdrawal')}}">Rút tiền</a>
                        </center>
                    </div>
                    <div
                        class="tab-menu col-lg-3 col-md-12 align-items-center checked status-refund">
                        <center>
                            <a data-url-refund="{{url('admin/revenue_ctv24h/status_refund')}}">Hoàn tiền từ đơn hàng</a>
                        </center>
                    </div>
                    <div
                        class="tab-menu col-lg-3 col-md-12 align-items-center status-bonus">
                        <center>
                            <a data-url-bonus="{{url('admin/revenue_ctv24h/status_bonus')}}">Hoa hồng giới thiệu</a>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive table-responsive-lg p-1">
            <table class="table data-list-view table-sm">
                <thead class="">
                <tr>
                    <th class="text-center" scope="col">Ngày</th>
                    <th class="text-center" scope="col">Loại giao dịch | Mô tả</th>
                    <th>Tình trạng</th>
                    <th class="text-center" scope="col">Số tiền</th>
                </tr>
                </thead>
                <tbody>
               <tr>
                   <td>Không có dữ liệu</td>
               </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
