<?php
$page='collaborator_shop/decorate-shop';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card p-2 border-bottom">
            <div class="border-bottom">
                <h3 class="text-bold-700 mb-2">Trang trí shop</h3>
                <p class="font-size-17 color-gray">Tối ưu hóa giao diện Shop có thể giúp tăng doanh thu cho Shop của bạn</p>
            </div>
            <div class="d-flex align-items-center justify-content-sm-between mg-top-20">
                <ul class="d-flex align-items-center p-0">
                    <li class="btn-view border-radius-8 border_box font-weight-bold color-black font-size-17 btn-version active" data-action="mobile" style="height: 45px; padding: 0 15px;margin-right: 20px; line-height: 45px;">Phiên bản di động</li>
                    <li class="btn-view border-radius-8 border_box font-weight-bold color-black font-size-17 btn-version" data-action="desktop" style="height: 45px; padding: 0 15px;line-height: 45px;">Phiên bản máy tính</li>
                </ul>
                <div class="d-flex align-items-center">
                    <p class="font-size-13 color-black m-0">Dùng Trang trí Shop để thay giao diện mới cho trang chủ của Shop bạn</p>
                    <div style="margin-left: 20px"><img src="{{url('assets/frontend/Icon/rank/Group_14885.png')}}"></div>
                </div>
            </div>
            <div id="decorate_shop">
                <div class="d-flex align-items-center justify-content-sm-center" style="padding: 50px 0">
                    <div class="w-50 d-flex justify-content-sm-center align-items-center">
                        <img src="{{url('assets/frontend/Icon/rank/Group_35949.png')}}">
                    </div>
                    <div class="w-50 d-flex justify-content-sm-center align-items-center">
                        <div style="max-width: 550px">
                            <h3 class="font-weight-bold font-size-24 color-black">Trang trí phiên bản di động hiện tại</h3>
                            <p class="font-size-14" style="line-height: 24px">Lưu nháp những thiết kế Trang trí Shop. Vui lòng chọn "Thay Đổi Thiết Kế" để áp dụng bản trang trí này</p>
                            <p class="font-size-14" style="line-height: 24px">Thời gian lưu lần cuối: 13:44 21-06-2021<br>
                                Thời gian áp dụng lần cuối: 20:42 11-12-2020</p>
                            <button class="mg-top-20 border-radius-16 bg-red color-white font-size-14 font-weight-bold" style="height: 40px;line-height: 40px;border: none;padding: 0 15px">Chỉnh sửa trang trí >></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="chat-footer d-flex justify-content-lg-end w-100">
        <button class="border-radius-8 out-line-none bg-red font-weight-bold color-white d-flex align-items-center"
                style="margin-right: 20px">
            <img src="../../assets/admin/app-assets/images/icons/live_chat.png">
            <a class="color-white" style="margin-left: 10px">Chat với CTV24</a>
        </button>
        <button
            class="border_box border-radius-8 out-line-none font-weight-bold color-gray d-flex align-items-center">
            <img src="../../assets/admin/app-assets/images/icons/chat-bubbles-with-ellipsis.png">
            <a class="color-white" style="margin-left: 10px">Chat với khách hàng</a>
        </button>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
<script src="dashboard/index.js"></script>
<script>
    $(document).ready(function () {
       $('.btn-view').click(function () {
           let action = $(this).attr("data-action");
           $('ul .btn-view').removeClass("active");
           $(this).addClass('active');
           let url;
           switch (action) {
               case 'mobile':
                   url = window.location.origin + '/admin/collaborator_shop/decorate-shop/mobile';
                   break;
               case 'desktop':
                   url = window.location.origin + '/admin/collaborator_shop/decorate-shop/desktop';
                   break;
               default:
           }
           $.ajax({
               url: url,
               dataType:'html',
               type:'GET',
               success:function (data) {
                   $('#decorate_shop').html(data);
               }
           })
       });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
