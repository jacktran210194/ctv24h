<?php
$page = 'collaborator_shop/update-ctv';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>
<style>
</style>
<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3></h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content card-body">
                <h3 class="text-bold-700 f-24">Thông tin cập nhật shop</h3>
                <span class="f-12 text-bold-700" style="color: red;">Cập nhập đầy đủ thông tin để bắt đầu bán hàng !</span>
                <br>
                <br>
                <div class="content-info ml-5" style="width: 60%;">
                    <div class="form-group">
                        <label class="text-bold-700" for="">Họ và tên</label>
                        <input disabled id="name_supplier" type="text" class="form-control input_config" value="{{isset($data_customer) ? $data_customer->name : ''}}">
                    </div>
                    <div class="form-group">
                        <label class="text-bold-700" for="">Ngày sinh</label>
                        <input disabled id="birthday_supplier" type="date" class="form-control input_config" value="{{isset($data_customer) ? date('Y-m-d',strtotime($data_customer->birthday)) : ''}}">
                    </div>
                    <div class="form-group">
                        <label class="text-bold-700" for="">Số CMND/CCCD</label>
                        <input id="card_id" type="number" class="form-control input_config" value="">
                    </div>
                    <div class="form-group">
                        <label class="text-bold-700" for="">Ngày cấp</label>
                        <input id="card_date" type="date" class="form-control input_config" value="">
                    </div>
                    <div class="form-group">
                        <label class="text-bold-700" for="">Nơi cấp</label>
                        <input id="card_address" type="text" class="form-control input_config" value="">
                    </div>
                </div>
                <div class="content_image ml-5">
                    <div class="d-flex align-items-center">
                        <div class="card_before">
                            <span class="text-bold-700" for="">Căn Cước Công Dân (Mặt trước)</span><br>
                            <img class="border_shadow" id="img_1" width="300px" height="189px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/tai_file.png" alt="">
                            <input required id="image_card_1" type="file" class="form-control hidden"
                                   onchange="show_img_1(this)">
                        </div>
                        <div class="card_after ml-3">
                            <span class="text-bold-700" for="">Căn Cước Công Dân (Mặt sau)</span><br>
                            <img class="border_shadow" id="img_2" width="300px" height="189px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/tai_file.png" alt="">
                            <input required id="image_card_2" type="file" class="form-control hidden"
                                   onchange="show_img_2(this)">
                        </div>
                    </div>
                    <div class="d-flex align-items-center mt-2">
                        <div class="card_before">
                            <span class="text-bold-700" for="">Giấy phép đăng ký kinh doanh</span><br>
                            <img class="border_shadow" id="img_3" width="300px" height="189px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/tai_file.png" alt="">
                            <input required id="business_license" type="file" class="form-control hidden"
                                   onchange="show_img_3(this)">
                        </div>
                        <div class="card_after ml-3">
                            <span class="text-bold-700" for="">Các giấy tờ liên quan</span><br>
                            <div class="d-flex align-items-center">
                                <img class="border_shadow" id="img_4" width="300px" height="189px"
                                     src="../../assets/admin/app-assets/images/icon_dashboard/tai_file.png" alt="">
                                <input required id="related_document_1" type="file" class="form-control hidden"
                                       >
                                <img id="img_5" class="ml-2 border_shadow" width="300px" height="189px"
                                     src="../../assets/admin/app-assets/images/icon_dashboard/tai_file.png" alt="">
                                <input required id="related_document_2" type="file" class="form-control hidden"
                                       >
                            </div>
                        </div>
                    </div>
                    <div class="d-flex align-items-center justify-content-end mr-4 mt-4">
                        <a href="{{url('admin/supplier_admin')}}" style="width: 100px!important;" class="btn btn_sliver text-bold-700 mr-2">Huỷ</a>
                        <buttonstyle="width: 100px!important;" class="btn btn_main text-bold-700 btn-save">Lưu</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
<script src="dashboard/index.js"></script>
