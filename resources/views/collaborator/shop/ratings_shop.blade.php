<?php
$page='collaborator_shop/ratings-shop';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-9">
                <div class="card p-1">
                    <div class="form-label-group position-relative has-icon-right mb-3" style="width: 30%;">
                        <input type="text" id="search" class="form-control" name="search"
                               placeholder="Tìm kiếm sản phẩm...">
                        <div class="form-control-position">
                            <img src="../../assets/admin/app-assets/images/icon_dashboard/search.png" alt="">
                        </div>
                    </div>
                    <div class="form_filter d-flex">
                        <div class="title">
                            <h3 class="text-bold-700 f-24 mr-2">Đánh giá Shop</h3>
                        </div>
                        <div class="list_filter">
                            <button class="btn border btn_main btn_all_star btn_sao text-bold-700">Tất cả</button>
                            <button class="btn border btn_1_star btn_sao text-bold-700">1 Sao <img
                                    src="../../assets/admin/app-assets/images/icon_dashboard/sao.png" alt=""></button>
                            <button class="btn border btn_2_star btn_sao text-bold-700">2 Sao <img
                                    src="../../assets/admin/app-assets/images/icon_dashboard/sao.png" alt=""></button>
                            <button class="btn border btn_3_star text-bold-700">3 Sao <img
                                    src="../../assets/admin/app-assets/images/icon_dashboard/sao.png" alt=""></button>
                            <button class="btn border btn_4_star btn_sao text-bold-700">4 Sao <img
                                    src="../../assets/admin/app-assets/images/icon_dashboard/sao.png" alt=""></button>
                            <button class="btn border btn_5_star btn_sao text-bold-700">5 Sao <img
                                    src="../../assets/admin/app-assets/images/icon_dashboard/sao.png" alt=""></button>
                        </div>
                        <div class="position-absolute" style="right: 2%;">
                            <select id="status" class="form-control">
                                <option value="">Tất cả</option>
                                <option value="">Chưa xử lý</option>
                                <option value="">Đã xử lý</option>
                            </select>
                        </div>
                    </div>
                    <div class="get_data_ajax">
                        <div class="table-responsive table-responsive-lg border_table mt-3">
                            <table class="table data-list-view table-sm">
                                <thead>
                                <tr class="bg_table">
                                    <th>Sản phẩm</th>
                                    <th width="40%">Đánh giá của khách</th>
                                    <th class="text-center">Trả lời đánh giá</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <img class="border_table mr-1" width="65px" height="63px"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <div class="info_product">
                                                    <span class="text-bold-700">Áo khoác bông dài quá gối </span>
                                                    <br>
                                                    <span
                                                        class="text-sliver text-bold-700 f-12">ID đơn hàng: 201130C8DQG3BE</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <img class="mr-2" width="70px" height="auto"
                                                     src="../../assets/admin/app-assets/images/icon_dashboard/4sao.png"
                                                     alt="">
                                                <span class="m-0 f-12">02:34 29-10-2020</span>
                                            </div>
                                            <div class="content_review">
                                                <p class="text-sliver f-12">Giao hàng nhanh Sản phẩm mới mua lần đầu thấy cũng được nếu dùng ổn lần sau ủng hộ shop tiếp.</p>
                                            </div>
                                            <div class="img_review d-flex">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn_main">Trả lời</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <img class="border_table mr-1" width="65px" height="63px"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <div class="info_product">
                                                    <span class="text-bold-700">Áo khoác bông dài quá gối </span>
                                                    <br>
                                                    <span
                                                        class="text-sliver text-bold-700 f-12">ID đơn hàng: 201130C8DQG3BE</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <img class="mr-2" width="70px" height="auto"
                                                     src="../../assets/admin/app-assets/images/icon_dashboard/4sao.png"
                                                     alt="">
                                                <span class="m-0 f-12">02:34 29-10-2020</span>
                                            </div>
                                            <div class="content_review">
                                                <p class="text-sliver f-12">Giao hàng nhanh Sản phẩm mới mua lần đầu thấy cũng được nếu dùng ổn lần sau ủng hộ shop tiếp.</p>
                                            </div>
                                            <div class="img_review d-flex">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn_main">Trả lời</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <img class="border_table mr-1" width="65px" height="63px"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <div class="info_product">
                                                    <span class="text-bold-700">Áo khoác bông dài quá gối </span>
                                                    <br>
                                                    <span
                                                        class="text-sliver text-bold-700 f-12">ID đơn hàng: 201130C8DQG3BE</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <img class="mr-2" width="70px" height="auto"
                                                     src="../../assets/admin/app-assets/images/icon_dashboard/4sao.png"
                                                     alt="">
                                                <span class="m-0 f-12">02:34 29-10-2020</span>
                                            </div>
                                            <div class="content_review">
                                                <p class="text-sliver f-12">Giao hàng nhanh Sản phẩm mới mua lần đầu thấy cũng được nếu dùng ổn lần sau ủng hộ shop tiếp.</p>
                                            </div>
                                            <div class="img_review d-flex">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn_main">Trả lời</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <img class="border_table mr-1" width="65px" height="63px"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <div class="info_product">
                                                    <span class="text-bold-700">Áo khoác bông dài quá gối </span>
                                                    <br>
                                                    <span
                                                        class="text-sliver text-bold-700 f-12">ID đơn hàng: 201130C8DQG3BE</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <img class="mr-2" width="70px" height="auto"
                                                     src="../../assets/admin/app-assets/images/icon_dashboard/4sao.png"
                                                     alt="">
                                                <span class="m-0 f-12">02:34 29-10-2020</span>
                                            </div>
                                            <div class="content_review">
                                                <p class="text-sliver f-12">Giao hàng nhanh Sản phẩm mới mua lần đầu thấy cũng được nếu dùng ổn lần sau ủng hộ shop tiếp.</p>
                                            </div>
                                            <div class="img_review d-flex">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                                <img width="65px" height="63px" class="border_table mr-1 image-preview"
                                                     src="{{url('assets/frontend/Icon/rank/Rectangle_5661.png')}}"
                                                     alt="">
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn_main">Trả lời</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="d-flex align-items-center justify-content-lg-center w-100 mg-top-30">
                                <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
                                    <img src="../../assets/admin/app-assets/images/icons/prev.png">
                                </button>
                                <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
                                    1
                                </button>
                                <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13 active">
                                    2
                                </button>
                                <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
                                    3
                                </button>
                                <p class="font-weight-bold font-size-13" style="margin-right: 20px;color: #A2A6B0; margin-bottom: 0">
                                    ...
                                </p>
                                <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
                                    15
                                </button>

                                <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
                                    <img src="../../assets/admin/app-assets/images/icons/next.png">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card p-1">
                    <div class="info_shop text-center mt-1">
                        <img width="72px" height="72px" class="border-tron mb-1"
                             src="app-assets/images/portrait/small/avatar-s-11.jpg"
                             alt="">
                        <h3 class=" text-bold-700">Dong</h3>
                    </div>
                    <div class="reply_review text-center mt-2 p-1">
                        <textarea class="form-control" id="content_reply" cols="30" rows="8"
                                  placeholder="Trả lời người mua"></textarea>
                        <button class="btn btn_main btn_reply mt-1">Gửi trả lời đánh giá</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
<script src="dashboard/index.js"></script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
