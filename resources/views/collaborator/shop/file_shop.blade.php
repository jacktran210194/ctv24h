<?php
$page='collaborator_shop/file-shop';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card p-2">
            <h3 class="text-bold-700 mb-2">Hồ Sơ Shop</h3>
            <div class="row">
                <div class="col-lg-5">
                        <div id="carouselExampleControls" class="carousel slide mb-2" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active position-relative border-radius-16" style="padding-top: 100%">
                                    <img class="d-block w-100 border-radius-16 position-absolute h-100" style="top: 0; left: 0; object-fit: cover"
                                         src="/uploads/images/shop/EBEJmSf81xXCsAgmKMsJ.jpg">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                               data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                               data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    <div class="d-flex align-items-center flex-wrap mb-2" style="margin-right: -20px">
                        <div class="card_shadow d-flex flex-column text-center p-1"
                             style="width: calc(100%/3 - 20px); margin-right: 20px; margin-bottom: 20px">
                            <div class="d-flex justify-content-sm-center">
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/lich.png" width="50px" alt="">
                            </div>
                            <p class="text-sliver f-12 mt-1 text-bold-700" style="height: 50px;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;">Ngày tham gia</p>
                            <p class="pb-1 text-bold-700">
                                08-06-2021
                            </p>
                        </div>
                        <div class="card_shadow d-flex flex-column text-center p-1"
                             style="width: calc(100%/3 - 20px); margin-right: 20px; margin-bottom: 20px">
                            <div class="d-flex justify-content-sm-center">
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/time.png" width="50px" alt="">
                            </div>
                            <p class="text-sliver f-12 mt-1 text-bold-700" style="height: 50px;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;">Thời gian phản hồi</p>
                            <p class="pb-1 text-bold-700">
                                10p</p>
                        </div>
                        <div class="card_shadow d-flex flex-column text-center p-1"
                             style="width: calc(100%/3 - 20px); margin-right: 20px; margin-bottom: 20px">
                            <div class="d-flex justify-content-sm-center">
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/order_fail.png" width="50px" alt="">
                            </div>
                            <p class="text-sliver f-12 mt-1 text-bold-700" style="height: 50px;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;">Tỷ lệ đơn hàng không thành công</p>
                            <p class="pb-1 text-bold-700">
                                0 %</p>
                        </div>
                        <div class="card_shadow d-flex flex-column text-center p-1"
                             style="width: calc(100%/3 - 20px); margin-right: 20px; margin-bottom: 20px">
                            <div class="d-flex justify-content-sm-center">
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/product.png" width="50px" alt="">
                            </div>
                            <p class="text-sliver f-12 mt-1 text-bold-700" style="height: 50px;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;">Sản phẩm</p>
                            <p class="pb-1 text-bold-700">
                                0</p>
                        </div>
                        <div class="card_shadow d-flex flex-column text-center p-1"
                             style="width: calc(100%/3 - 20px); margin-right: 20px; margin-bottom: 20px">
                            <div class="d-flex justify-content-sm-center">
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/chat.png" width="50px" alt="">
                            </div>
                            <p class="text-sliver f-12 mt-1 text-bold-700" style="height: 50px;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;">Tỷ lệ phản hồi</p>
                            <p class="pb-1 text-bold-700">
                                0 %</p>
                        </div>
                        <div class="card_shadow d-flex flex-column text-center p-1"
                             style="width: calc(100%/3 - 20px); margin-right: 20px; margin-bottom: 20px">
                            <div class="d-flex justify-content-sm-center">
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/review.png" width="50px" alt="">
                            </div>
                            <p class="text-sliver f-12 mt-1 text-bold-700" style="height: 50px;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;">Đánh giá</p>
                            <p class="pb-1 text-bold-700">
                                0</p>
                        </div>
                        <div class="card_shadow d-flex flex-column text-center p-1"
                             style="width: calc(100%/3 - 20px); margin-right: 20px; margin-bottom: 20px">
                            <div class="d-flex justify-content-sm-center">
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/category.png" width="50px" alt="">
                            </div>
                            <p class="text-sliver f-12 mt-1 text-bold-700" style="height: 50px;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;">Danh mục của shop</p>
                            <p class="pb-1 text-bold-700">
                            </p>
                        </div>
                        <div class="card_shadow d-flex flex-column text-center p-1"
                             style="width: calc(100%/3 - 20px); margin-right: 20px; margin-bottom: 20px">
                            <div class="d-flex justify-content-sm-center">
                                <img src="../../assets/admin/app-assets/images/icon_dashboard/report.png" width="50px" alt="">
                            </div>
                            <p class="text-sliver f-12 mt-1 text-bold-700" style="height: 50px;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;">Báo cáo của shop</p>
                            <p class="pb-1 text-bold-700">
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <label class="text-bold-700">Tên shop</label>
                    <div class="form-label-group position-relative has-icon-left">
                        <input type="text" name="name" id="name" class="form-control text-bold-700"
                               value="Ngô Đức Huy">
                        <div class="form-control-position" style="padding-left: 9px;">
                            <img width="18px" height="18px"
                                 src="../../assets/admin/app-assets/images/icon_dashboard/shop.png" alt="">
                        </div>
                    </div>
                    <label class="text-bold-700">Hình ảnh shop</label>
                    <div class="row">
                        <div class="col-lg-3">
                            <button class="btn btn_image btn-image-one border_gach p-2">
                                <img class="text-center" width="18px" height="18px"
                                     src="../../assets/admin/app-assets/images/icon_dashboard/image.png" alt="">
                                <br>
                                <div class="btn btn-sm border_xam mt-1">
                                    Tải hình ảnh
                                </div>
                            </button>
                            <input hidden class="input_image" type='file' value="" accept="image/*" id="image"
                                   ">
                        </div>
                        <div class="col-lg-3">
                            <button class="btn btn_image border_gach btn-show-img p-2">
                                <img class="text-center" width="18px" height="18px"
                                     src="../../assets/admin/app-assets/images/icon_dashboard/image.png" alt="">
                                <br>
                                <div class="btn btn-sm border_xam mt-1">
                                    Tải hình ảnh
                                </div>
                            </button>
                            <input hidden class="form-control input_list_image" type='file' accept="image/*"
                                   id="imageQr"
                                   multiple/>
                        </div>
                    </div>
                    <div class="row mt-1 image-show">
                        <div class="col-lg-3">
                                <img width="100px" height="100px" id="avatar" class="thumbnail image-show-qr"
                                     src="/uploads/images/shop/EBEJmSf81xXCsAgmKMsJ.jpg">
                        </div>
{{--                            <div class="pip mb-2 col-lg-3" style="position: relative;">--}}
{{--                                <img class="mr-2" style="width: 100px; height: 100px;"--}}
{{--                                     src="{{$value['image']}}"--}}
{{--                                     title=""><br>--}}
{{--                                <button type="button" style="position: absolute; top:0;"--}}
{{--                                        class="fa fa-times btn-danger btn btn-sm btn-delete-image"--}}
{{--                                        data-url="{{url('admin/shop/delete_image/' . $value['id'])}}"></button>--}}
{{--                            </div>--}}
                    </div>
                    <label class="text-bold-700 mt-1">Link youtube</label>
                    <div class="form-label-group position-relative has-icon-left">
                        <input type="text" id="video" class="form-control"
                               placeholder="" >
                        <div class="form-control-position">
                            <i class="fa fa-youtube"></i>
                        </div>
                    </div>
                    <label class="text-bold-700">Mô tả shop</label>
                    <textarea class="form-control" id="desc" cols="30"
                              rows="7">LoviieBasics - Đơn giản - Trẻ Trung & Hiện Đại
                            (New Concept belong to LoviieCorner Group)
                            _ Team LoviieCorner sẽ đem đến những mẫu concept thiết kế thời trang xu hướng & hiện đại và đề cao sự tinh tế trong từng mẫu thiết kế giày .
                            - Và trên hết là cam kết về chất lượng .  Cam kết hàng giống hình, giống mô tả .
                            🏡 CN1: 89 Đặng Văn Bi, P. Trường Thọ,  Quận Thủ Đức
                            🏡 CN2: 475 Nguyễn Trãi, P.7 Quận 5
                            🏡 CN 3: 267 Đặng Văn Bi, Quận Thủ Đức
                            🏡 CN 4: 306 Võ Văn Ngân, Quận Thủ Đức
                    </textarea>
                    <div class="d-flex justify-content-end mt-2">
                        <button class="btn btn_sliver text-bold-700 mr-2" style="width: 120px">Huỷ</button>
                        <button type="button" class="btn btn_main text-bold-700 btn-add-account" style="width: 120px"
                                data-id=""
                                data-supplier=""
                                data-url=""
                        >Lưu
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
<script src="dashboard/index.js"></script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
