<div class="d-flex align-items-center justify-content-sm-center" style="padding: 50px 0">
    <div class="w-50 d-flex justify-content-sm-center align-items-center">
        <img src="{{url('assets/frontend/Icon/rank/Group_35563.png')}}">
    </div>
    <div class="w-50 d-flex justify-content-sm-center align-items-center">
        <div style="max-width: 550px">
            <h3 class="font-weight-bold font-size-24 color-black">Trang trí phiên bản máy tính hiện tại</h3>
            <a href="{{url('/admin/collaborator_shop/decorate-shop/desktop/pc')}}" target="_blank" class="mg-top-20 border-radius-16 bg-red color-white font-size-14 font-weight-bold" style="height: 40px;line-height: 40px;border: none;padding: 10px 20px">Chỉnh sửa trang trí >></a>
        </div>
    </div>
</div>
