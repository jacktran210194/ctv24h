<div class="d-flex align-items-center justify-content-sm-center" style="padding: 50px 0">
    <div class="w-50 d-flex justify-content-sm-center align-items-center">
        <img src="{{url('assets/frontend/Icon/rank/Group_35949.png')}}">
    </div>
    <div class="w-50 d-flex justify-content-sm-center align-items-center">
        <div style="max-width: 550px">
            <h3 class="font-weight-bold font-size-24 color-black">Trang trí phiên bản di động hiện tại</h3>
            <p class="font-size-14" style="line-height: 24px">Lưu nháp những thiết kế Trang trí Shop. Vui lòng chọn "Thay Đổi Thiết Kế" để áp dụng bản trang trí này</p>
            <p class="font-size-14" style="line-height: 24px">Thời gian lưu lần cuối: 13:44 21-06-2021<br>
                Thời gian áp dụng lần cuối: 20:42 11-12-2020</p>
            <button class="mg-top-20 border-radius-16 bg-red color-white font-size-14 font-weight-bold" style="height: 40px;line-height: 40px;border: none;padding: 0 15px">Chỉnh sửa trang trí >></button>
        </div>
    </div>
</div>
