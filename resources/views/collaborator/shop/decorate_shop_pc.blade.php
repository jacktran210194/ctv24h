
<?php
$data_admin = Session::get('data_user');
$data_supplier = Session::get('data_supplier');
$data_collaborator = Session::get('data_collaborator');
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->
    <body>
        <div class="bg-white d-flex align-items-center justify-content-sm-between" style="padding: 20px 60px">
            <div class="w-20 d-flex justify-content-lg-start align-items-center"><img src="../../assets/admin/app-assets/images/logo/logo_ctv.png"></div>
            <div class="w-50 d-flex align-items-center justify-content-lg-start"><p class="font-weight-bold m-0 font-size-24 color-black">Trang chủ > Cửa hàng > Trang trí shop</p></div>
            <div class="w-30 d-flex align-items-center justify-content-lg-end">
                <div class="d-flex align-items-center">
                    <div><img class="round" src="app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" height="40" width="40"></div>
                    <div style="margin-left: 20px">
                        <p class="font-weight-bold font-size-16 color-black" style="margin-bottom: 5px">
                            @if(isset($data_admin))
                                {{$data_admin['name']}}
                            @elseif(isset($data_supplier))
                                {{$data_supplier['name']}}
                            @elseif(isset($data_collaborator))
                                {{$data_collaborator['name']}}
                            @endif
                        </p>
                        <p
                            class="user-status m-0">
                        @if(isset($data_admin))
                                <span class="badge badge-success">Admin</span>
                            @elseif(isset($data_supplier))
                                <span class="badge badge-primary">Nhà cung cấp</span>
                            @elseif(isset($data_collaborator))
                                <span class="badge badge-danger">Cộng tác viên</span>
                            @endif
                    </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-sm-between mg-top-40">
            <div class="" style="width: 430px">
                <h3 class="text-uppercase font-weight-bold font-size-24 color-black text-center" style="padding: 30px 0">Thiết kế của shop hiện tại</h3>
                <div class="bg-white" style="padding: 30px 0;">
                    <div class="d-flex justify-content-sm-center align-items-center">
                        <div class="w-80">
                            <div class="d-flex justify-content-sm-between align-items-center">
                                <p class="color-gray font-weight-bold font-size-12 m-0">Banner hình ảnh</p>
                                <p class="color-gray font-weight-bold font-size-12 m-0">0/10</p>
                            </div>
                            <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                     style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-sm-center align-items-center mg-top-30">
                        <div class="w-80">
                            <div class="d-flex justify-content-sm-between align-items-center">
                                <p class="color-gray font-weight-bold font-size-12 m-0">Banner - 2 hình</p>
                                <p class="color-gray font-weight-bold font-size-12 m-0">0/10</p>
                            </div>
                            <div class="d-flex justify-content-between align-items-center w-100">
                                <div style="width: calc(50% - 6px)">
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                    </div>
                                </div>
                                <div style="width: calc(50% - 6px)">
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-sm-center align-items-center mg-top-30">
                        <div class="w-80">
                            <div class="d-flex justify-content-sm-between align-items-center">
                                <p class="color-gray font-weight-bold font-size-12 m-0">Banner - tổng hợp</p>
                                <p class="color-gray font-weight-bold font-size-12 m-0">0/5</p>
                            </div>
                            <div class="d-flex justify-content-between w-100">
                                <div style="width: calc(70% - 6px)">
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                    </div>
                                </div>
                                <div style="width: calc(30% - 6px)" class="d-flex justify-content-between flex-column">
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">

                                    </div>
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-sm-center align-items-center mg-top-30">
                        <div class="w-80">
                            <div class="d-flex justify-content-sm-between align-items-center">
                                <p class="color-gray font-weight-bold font-size-12 m-0">Video</p>
                                <p class="color-gray font-weight-bold font-size-12 m-0">0/1</p>
                            </div>
                            <div class="w-100 position-relative" style="padding-top: 55%; background: #ececec">
                                <img src="{{url('assets/frontend/Icon/rank/play-button_1.png')}}" class="position-absolute"
                                     style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-sm-center align-items-center mg-top-30">
                        <div class="w-80">
                            <div class="d-flex justify-content-sm-between align-items-center">
                                <p class="color-gray font-weight-bold font-size-12 m-0">Banner - xoay vòng<br>
                                    và mô tả shop</p>
                                <p class="color-gray font-weight-bold font-size-12 m-0">0/1</p>
                            </div>
                            <div class="d-flex justify-content-between w-100">
                                <div style="width: 74px">
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                    </div>
                                </div>
                                <div style="width: calc(100% - 80px)" class="d-flex justify-content-between flex-column">
                                    <div class="w-100 position-relative" style="padding-top: 35px; background: #ececec">
                                        <span class="font-weight-bold font-size-12 position-absolute"
                                              style="color: #8C8C8C; top: 50%; left: 10px; transform: translate(-50%, -50%)">Aa</span>
                                    </div>
                                    <div class="w-100 position-relative" style="padding-top: 35px; background: #ececec">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-sm-center align-items-center mg-top-30">
                        <div class="w-80">
                            <div class="d-flex justify-content-sm-between align-items-center">
                                <p class="color-gray font-weight-bold font-size-12 m-0">Sản phẩm nổi bật</p>
                                <p class="color-gray font-weight-bold font-size-12 m-0">0/10</p>
                            </div>
                            <div class="d-flex justify-content-between align-items-center w-100">
                                <div style="width: calc(50% - 6px)">
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                    </div>
                                </div>
                                <div style="width: calc(50% - 6px)">
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between align-items-center w-100" style="margin-top: 5px">
                                <div style="width: calc(50% - 6px)">
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-bottom: 5px"></div>
                                    <div class="w-50" style="height: 5px; background: #ececec;"></div>
                                </div>
                                <div style="width: calc(50% - 6px)">
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-bottom: 5px"></div>
                                    <div class="w-50" style="height: 5px; background: #ececec;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-sm-center align-items-center mg-top-30">
                        <div class="w-80">
                            <div class="d-flex justify-content-sm-between align-items-center">
                                <p class="color-gray font-weight-bold font-size-12 m-0">Danh mục nổi bật</p>
                                <p class="color-gray font-weight-bold font-size-12 m-0">0/1</p>
                            </div>
                            <div class="d-flex justify-content-between align-items-center w-100">
                                <div style="width: calc(100%/3 - 6px)">
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                    </div>
                                </div>
                                <div style="width: calc(100%/3 - 6px)">
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                    </div>
                                </div>
                                <div style="width: calc(100%/3 - 6px)">
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between align-items-center w-100" style="margin-top: 5px">
                                <div style="width: calc(100%/3 - 6px)">
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-bottom: 5px"></div>
                                    <div class="w-50" style="height: 5px; background: #ececec;"></div>
                                </div>
                                <div style="width: calc(100%/3 - 6px)">
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-bottom: 5px"></div>
                                    <div class="w-50" style="height: 5px; background: #ececec;"></div>
                                </div>
                                <div style="width: calc(100%/3 - 6px)">
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-bottom: 5px"></div>
                                    <div class="w-50" style="height: 5px; background: #ececec;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-sm-center align-items-center mg-top-30">
                        <div class="w-80">
                            <div class="d-flex justify-content-sm-between align-items-center">
                                <p class="color-gray font-weight-bold font-size-12 m-0">Sản phẩm mới</p>
                                <p class="color-gray font-weight-bold font-size-12 m-0">0/1</p>
                            </div>
                            <div class="d-flex justify-content-between align-items-center w-100">
                                <div style="width: calc(100%/3 - 6px)">
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                    </div>
                                </div>
                                <div style="width: calc(100%/3 - 6px)">
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                    </div>
                                </div>
                                <div style="width: calc(100%/3 - 6px)">
                                    <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between align-items-center w-100" style="margin-top: 5px">
                                <div style="width: calc(100%/3 - 6px)">
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-bottom: 5px"></div>
                                    <div class="w-50" style="height: 5px; background: #ececec;"></div>
                                </div>
                                <div style="width: calc(100%/3 - 6px)">
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-bottom: 5px"></div>
                                    <div class="w-50" style="height: 5px; background: #ececec;"></div>
                                </div>
                                <div style="width: calc(100%/3 - 6px)">
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-bottom: 5px"></div>
                                    <div class="w-50" style="height: 5px; background: #ececec;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-sm-center align-items-center mg-top-30">
                        <div class="w-80">
                            <div class="d-flex justify-content-sm-between align-items-center">
                                <p class="color-gray font-weight-bold font-size-12 m-0">Sản phẩm bán chạy</p>
                                <p class="color-gray font-weight-bold font-size-12 m-0">0/1</p>
                            </div>
                            <div class="d-flex justify-content-between align-items-center w-100">
                                <div style="width: calc(75%/2 - 6px)">
                                    <div class="w-100 position-relative before-dv" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                        <div class="position-absolute arrow-number">1</div>
                                    </div>
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-top: 5px"></div>
                                </div>
                                <div style="width: calc(75%/2 - 6px)">
                                    <div class="w-100 position-relative before-dv" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                        <div class="position-absolute arrow-number">2</div>
                                    </div>
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-top: 5px"></div>
                                </div>
                                <div style="width: calc(25% - 6px)">
                                    <div class="w-100 position-relative before-dv" style="background: #ececec;height: 123px">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                        <div class="position-absolute arrow-number">3</div>
                                    </div>
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-top: 5px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-sm-center align-items-center mg-top-30">
                        <div class="w-80">
                            <div class="d-flex justify-content-sm-between align-items-center">
                                <p class="color-gray font-weight-bold font-size-12 m-0">Ưu đãi khủng</p>
                                <p class="color-gray font-weight-bold font-size-12 m-0">0/1</p>
                            </div>
                            <div class="d-flex justify-content-between align-items-center w-100">
                                <div style="width: calc(75%/2 - 6px)">
                                    <div class="w-100 position-relative before-dv" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                        <div class="position-absolute arrow-number">1</div>
                                    </div>
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-top: 5px"></div>
                                </div>
                                <div style="width: calc(75%/2 - 6px)">
                                    <div class="w-100 position-relative before-dv" style="padding-top: 100%; background: #ececec">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                        <div class="position-absolute arrow-number">2</div>
                                    </div>
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-top: 5px"></div>
                                </div>
                                <div style="width: calc(25% - 6px)">
                                    <div class="w-100 position-relative before-dv" style="background: #ececec;height: 123px">
                                        <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                             style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                        <div class="position-absolute arrow-number">3</div>
                                    </div>
                                    <div class="w-100" style="height: 5px; background: #ececec; margin-top: 5px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-sm-center align-items-center mg-top-30">
                        <div class="w-80">
                            <div class="d-flex justify-content-sm-between align-items-center">
                                <p class="color-gray font-weight-bold font-size-12 m-0">Mô tả shop</p>
                                <p class="color-gray font-weight-bold font-size-12 m-0">0/1</p>
                            </div>
                            <div class="d-flex justify-content-between w-100">
                                <div class="d-flex justify-content-between flex-column w-100">
                                    <div class="w-100 position-relative" style="padding-top: 35px; background: #ececec">
                                        <span class="font-weight-bold font-size-12 position-absolute"
                                              style="color: #8C8C8C; top: 50%; left: 10px; transform: translate(-50%, -50%)">Aa</span>
                                    </div>
                                    <div class="w-100 position-relative" style="padding-top: 35px; background: #ececec; margin-top: 6px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="width: calc(100% - 460px)" class="bg-white">
                <div class="d-flex align-items-center justify-content-between border-bottom" style="padding: 30px 30px 30px 60px ">
                    <p class="m-0 font-size-17" style="color: #999999">Lần áp dụng gần nhất 18:05   22/06/2021</p>
                    <div class="d-flex align-items-center justify-content-lg-end flex-wrap">
                        <button class="font-size-14 font-weight-bold color-gray border_box border-radius-8 border bg-white" style="padding: 10px 5px;margin-bottom: 20px">Sao chép trang trí (Phiên bản di động)</button>
                        <button class="font-size-14 font-weight-bold color-gray border_box border-radius-8 border bg-white" style="padding: 10px 5px; margin-left: 20px; margin-bottom: 20px">Xem trước</button>
                        <button class="font-size-14 font-weight-bold color-gray border_box border-radius-8 border bg-white" style="padding: 10px 15px; margin-left: 20px; margin-bottom: 20px">Lưu</button>
                        <button class="font-size-14 font-weight-bold color-white border_box border-radius-8 border bg-red" style="padding: 10px 5px; margin-left: 20px; margin-bottom: 20px">Áp dụng</button>
                    </div>
                </div>
                <div class="d-flex justify-content-sm-center align-items-center mg-top-40">
                    <div>
                        <div class="d-flex align-items-center justify-content-sm-center" style="width: 375px">
                            <img src="{{url('assets/frontend/Icon/rank/banner_5.png')}}" class="w-100">
                        </div>
                        <div class="d-flex align-items-center justify-content-sm-center" style="width: 375px">
                            <ul class="d-flex justify-content-sm-center align-items-center m-0 p-0 w-100">
                                <li style="width: calc(100%/3); padding: 12px 0; margin-bottom: 2px; border-bottom: 1px solid #D1292F"
                                    class="color-red font-size-14 font-weight-bold text-center">Shop</li>
                                <li style="width: calc(100%/3); padding: 12px 0; margin-bottom: 2px;"
                                    class="color-black font-size-14 font-weight-bold text-center">Sản phẩm</li>
                                <li style="width: calc(100%/3); padding: 12px 0; margin-bottom: 2px;"
                                    class="color-black font-size-14 font-weight-bold text-center">Danh mục</li>
                            </ul>
                        </div>
                        <div class="d-flex align-items-lg-start position-relative" id="video">
                            <button class="border-radius-4 font-size-12 font-weight-bold color-gray border position-absolute"
                                    style="top: 60px; right:100%;  padding: 15px 10px">video</button>
                            <div class="d-flex justify-content-sm-center align-items-center" style="width: 375px">
                                <img src="{{url('assets/frontend/Icon/rank/banner_4.png')}}" class="w-100">
                            </div>
                        </div>
                        <div class="d-flex align-items-lg-start mg-top-20 position-relative" id="banner">
                            <button class="border-radius-4 font-size-12 font-weight-bold color-gray border position-absolute"
                                    style="top: 0; right:100%; padding: 15px 10px">Banner quay vòng</button>
                            <div class="d-flex justify-content-sm-center align-items-center" style="width: 375px">
                                <img src="{{url('assets/frontend/Icon/rank/banner_3.png')}}" class="w-100">
                            </div>
                        </div>
                        <div class="d-flex align-items-lg-start mg-top-20 position-relative" id="banner_2_image">
                            <button class="border-radius-4 font-size-12 font-weight-bold color-gray border position-absolute"
                                    style="top: 40px; right:100%; padding: 15px 10px">Banner - 2 hình</button>
                            <div class="d-flex justify-content-between align-items-center" style="width: 375px">
                                <img src="{{url('assets/frontend/Icon/rank/banner_2.png')}}" style="width: calc(50% - 10px)">
                                <img src="{{url('assets/frontend/Icon/rank/banner_1.png')}}" style="width: calc(50% - 10px)">
                            </div>
                        </div>
                        <div class="d-flex align-items-lg-start mg-top-20 position-relative" id="desc_shop">
                            <button class="border-radius-4 font-size-12 font-weight-bold color-gray border position-absolute"
                                    style="top: 20px; right:100%; padding: 15px 10px">Mô tả shop</button>
                            <div class="d-flex justify-content-between align-items-center" style="width: 375px">
                                <div class="p-1">
                                    <p class="color-red m-0" style="line-height: 16px">1 hit shop</p>
                                    <p class="font-size-14 text-justify color-black" style="line-height: 16px">
                                        It is a long established fact that a reader will be distracted by the readable content of a page when
                                        looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,
                                        as opposed to using 'Content here, content here', making it look like readable English. </p>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-lg-start mg-top-20 position-relative" id="hot_product">
                            <button class="border-radius-4 font-size-12 font-weight-bold color-gray border position-absolute"
                                    style="top: 20px; right:100%; padding: 15px 10px">Sản phẩm nổi bật</button>
                            <div class="d-flex justify-content-between align-items-center" style="width: 375px">
                                <div class="p-1 w-100">
                                    <div class="d-flex justify-content-between align-items-center w-100">
                                        <div style="width: calc(50% - 6px)">
                                            <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                                <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                                     style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                            </div>
                                        </div>
                                        <div style="width: calc(50% - 6px)">
                                            <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                                <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                                     style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-center w-100" style="margin-top: 5px">
                                        <div style="width: calc(50% - 6px)">
                                            <div class="w-100" style="height: 5px; background: #ececec; margin-bottom: 5px"></div>
                                            <div class="w-50" style="height: 5px; background: #ececec;"></div>
                                        </div>
                                        <div style="width: calc(50% - 6px)">
                                            <div class="w-100" style="height: 5px; background: #ececec; margin-bottom: 5px"></div>
                                            <div class="w-50" style="height: 5px; background: #ececec;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-lg-start mg-top-20 position-relative" id="banner_1_image">
                            <button class="border-radius-4 font-size-12 font-weight-bold color-gray border position-absolute"
                                    style="top: 60px; right:100%; padding: 15px 10px">Banner - 1 hình</button>
                            <div class="d-flex justify-content-sm-center align-items-center" style="width: 375px">
                                <img src="{{url('assets/frontend/Icon/rank/banner_6.png')}}" class="w-100">
                            </div>
                        </div>
                        <div class="d-flex align-items-lg-start mg-top-20 position-relative" id="banner_group">
                            <button class="border-radius-4 font-size-12 font-weight-bold color-gray border position-absolute"
                                    style="top: 20px; right:100%; padding: 15px 10px">Banner - tổng hợp</button>
                            <div class="d-flex justify-content-between align-items-center" style="width: 375px">
                                <div class="d-flex justify-content-between w-100">
                                    <div style="width: calc(70% - 6px)">
                                        <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                            <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                                 style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                        </div>
                                    </div>
                                    <div style="width: calc(30% - 6px)" class="d-flex justify-content-between flex-column">
                                        <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">

                                        </div>
                                        <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-lg-start mg-top-20 position-relative" id="selling_products">
                            <button class="border-radius-4 font-size-12 font-weight-bold color-gray border position-absolute"
                                    style="top: 20px; right:100%; padding: 15px 10px">Sản phẩm bán chạy</button>
                            <div class="d-flex justify-content-between align-items-center" style="width: 375px">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <div style="width: calc(75%/2 - 6px)">
                                        <div class="w-100 position-relative before-dv" style="padding-top: 100%; background: #ececec">
                                            <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                                 style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                            <div class="position-absolute arrow-number">1</div>
                                        </div>
                                        <div class="w-100" style="height: 5px; background: #ececec; margin-top: 5px"></div>
                                    </div>
                                    <div style="width: calc(75%/2 - 6px)">
                                        <div class="w-100 position-relative before-dv" style="padding-top: 100%; background: #ececec">
                                            <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                                 style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                            <div class="position-absolute arrow-number">2</div>
                                        </div>
                                        <div class="w-100" style="height: 5px; background: #ececec; margin-top: 5px"></div>
                                    </div>
                                    <div style="width: calc(25% - 6px)">
                                        <div class="w-100 position-relative before-dv" style="background: #ececec;height: 134px">
                                            <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                                 style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                            <div class="position-absolute arrow-number">3</div>
                                        </div>
                                        <div class="w-100" style="height: 5px; background: #ececec; margin-top: 5px"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-lg-start mg-top-20 position-relative" id="category_hot">
                            <button class="border-radius-4 font-size-12 font-weight-bold color-gray border position-absolute"
                                    style="top: 20px; right:100%; padding: 15px 10px">Danh mục nổi bật</button>
                            <div class="d-flex justify-content-between align-items-center" style="width: 375px">
                                <div class="w-100">
                                    <div class="d-flex justify-content-between align-items-center w-100">
                                        <div style="width: calc(100%/3 - 6px)">
                                            <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                                <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                                     style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                            </div>
                                        </div>
                                        <div style="width: calc(100%/3 - 6px)">
                                            <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                                <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                                     style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                            </div>
                                        </div>
                                        <div style="width: calc(100%/3 - 6px)">
                                            <div class="w-100 position-relative" style="padding-top: 100%; background: #ececec">
                                                <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                                     style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-center w-100" style="margin-top: 5px">
                                        <div style="width: calc(100%/3 - 6px)">
                                            <div class="w-100" style="height: 5px; background: #ececec; margin-bottom: 5px"></div>
                                            <div class="w-50" style="height: 5px; background: #ececec;"></div>
                                        </div>
                                        <div style="width: calc(100%/3 - 6px)">
                                            <div class="w-100" style="height: 5px; background: #ececec; margin-bottom: 5px"></div>
                                            <div class="w-50" style="height: 5px; background: #ececec;"></div>
                                        </div>
                                        <div style="width: calc(100%/3 - 6px)">
                                            <div class="w-100" style="height: 5px; background: #ececec; margin-bottom: 5px"></div>
                                            <div class="w-50" style="height: 5px; background: #ececec;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-lg-start mg-top-20 position-relative" id="great_deals">
                            <button class="border-radius-4 font-size-12 font-weight-bold color-gray border position-absolute"
                                    style="top: 20px; right:100%; padding: 15px 10px">Ưu đãi khủng</button>
                            <div class="d-flex justify-content-between align-items-center" style="width: 375px">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <div style="width: calc(75%/2 - 6px)">
                                        <div class="w-100 position-relative before-dv" style="padding-top: 100%; background: #ececec">
                                            <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                                 style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                            <div class="position-absolute arrow-number">1</div>
                                        </div>
                                        <div class="w-100" style="height: 5px; background: #ececec; margin-top: 5px"></div>
                                    </div>
                                    <div style="width: calc(75%/2 - 6px)">
                                        <div class="w-100 position-relative before-dv" style="padding-top: 100%; background: #ececec">
                                            <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                                 style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                            <div class="position-absolute arrow-number">2</div>
                                        </div>
                                        <div class="w-100" style="height: 5px; background: #ececec; margin-top: 5px"></div>
                                    </div>
                                    <div style="width: calc(25% - 6px)">
                                        <div class="w-100 position-relative before-dv" style="background: #ececec;height: 123px">
                                            <img src="{{url('assets/frontend/Icon/rank/icon_Image.png')}}" class="position-absolute"
                                                 style="top: 50%; left: 50%; transform: translate(-50%, -50%)">
                                            <div class="position-absolute arrow-number">3</div>
                                        </div>
                                        <div class="w-100" style="height: 5px; background: #ececec; margin-top: 5px"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
