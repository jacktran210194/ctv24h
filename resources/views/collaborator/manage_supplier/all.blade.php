<div class="d-flex mt-2 mb-2 second">
    <div class="col-lg-3 col-md-12 p-0 search">
        <input type="text" id="search" class="form-control" name="search" placeholder="Tìm kiếm">
        <div class="form-control-position">
            <i class="feather icon-search search-icon"></i>
        </div>
    </div>
    <div class="col-lg-3 col-md-12">
    </div>
    <div class="d-flex col-lg-6 col-md-12 p-0 align-items-center justify-content-lg-end">
        <span>Ngày gia nhập</span>
        <div class="col-lg-4">
            <input type="datetime-local" value="" class="form-control">
        </div>
        <div class="export">
            <center>
                <a href="{{url('admin/manage_supplier/export')}}">Xuất báo cáo</a>
            </center>
        </div>
        <img src="../../assets/admin/app-assets/images/ncc/manage_ctv/small_menu.png">
    </div>
</div>
<div class="title mb-2">
    <span class="text-black text-bold-700 font-size-17">Tất cả</span>
</div>
<div class="table-responsive table-responsive-lg border_table">
    <table class="table data-list-view table-sm">
        <thead class="bg_table">
        <tr class="text-center">
            <th scope="col">Tên shop NCC</th>
            <th scope="col">Số đơn đã Dropship</th>
            <th scope="col">Tổng doanh thu bán ra</th>
            <th scope="col">Lợi nhuận thu về</th>
            <th scope="col">Thao tác</th>
        </tr>
        </thead>
        <tbody id="s-all">
        @if(count($data))
            @foreach($data as $value)
                <tr class="text-center">
                    <td>
                        <div class="d-flex align-items-center justify-content-center">
                            <span class="mr-1">{{$value->name_shop}}</span>
                            <img src="../../assets/admin/app-assets/images/icons/chat-small.png">
                        </div>
                    </td>
                    <td>{{$value->count}}</td>
                    <td>{{number_format($value->total)}} đ</td>
                    <td>{{number_format($value->cod)}} đ</td>
                    <td>
                        <a href="{{url('admin/manage_supplier/detail/'.$value->shop_id)}}" class="text-blue text-bold-700 text-underline">Chi tiết</a>
                        <p style="cursor: pointer" class="text-blue text-bold-700 text-underline mt-1">Xóa NCC</p>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td>Không có dữ liệu</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
