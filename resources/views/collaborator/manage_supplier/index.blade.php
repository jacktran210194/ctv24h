<?php
$page = 'manage_supplier';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ctv/manage_supplier/style.css')}}">
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">

    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <!-- content -->
                <div class="header-unit mb-2">
                    <div class="d-flex align-items-center justify-content-lg-between p-0 col-lg-8 col-md-12">
                        <button class="btn btn-unit checked filter-all">Tất cả

                        </button>
                        <button class="btn btn-unit filter-selling">Đang bán hàng NCC

                        </button>
                        <button class="btn btn-unit filter-order">Đã đặt hàng NCC

                        </button>
                        <button class="btn btn-unit filter-follow">NCC đang theo dõi

                        </button>
                    </div>
                </div>
                <div id="show-content">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
</body>
<script>
    $(document).ready(function () {
        let url_filter = window.location.origin;
        let url = url_filter + '/admin/manage_supplier/all';
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $('.filter-all').addClass('checked');
                $('.filter-selling').removeClass('checked');
                $('.filter-order').removeClass('checked');
                $('.filter-follow').removeClass('checked');
                $('#show-content').html(data);
            },
            error: function () {
                console.log('error')
            }
        });

        //Tất cả
        $(document).on("click", ".filter-all", function () {
            let url = url_filter + '/admin/manage_supplier/all';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.filter-all').addClass('checked');
                    $('.filter-selling').removeClass('checked');
                    $('.filter-order').removeClass('checked');
                    $('.filter-follow').removeClass('checked');
                    $('#show-content').html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Đang bán hàng NCC
        $(document).on("click", ".filter-selling", function () {
            let url = url_filter + '/admin/manage_supplier/selling';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.filter-all').removeClass('checked');
                    $('.filter-selling').addClass('checked');
                    $('.filter-order').removeClass('checked');
                    $('.filter-follow').removeClass('checked');
                    $('#show-content').html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Đã đặt hàng NCC
        $(document).on("click", ".filter-order", function () {
            let url = url_filter + '/admin/manage_supplier/order';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.filter-all').removeClass('checked');
                    $('.filter-selling').removeClass('checked');
                    $('.filter-order').addClass('checked');
                    $('.filter-follow').removeClass('checked');
                    $('#show-content').html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //NCC đang theo dõi
        $(document).on("click", ".filter-follow", function () {
            let url = url_filter + '/admin/manage_supplier/follow';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.filter-all').removeClass('checked');
                    $('.filter-selling').removeClass('checked');
                    $('.filter-order').removeClass('checked');
                    $('.filter-follow').addClass('checked');
                    $('#show-content').html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });
        // $(document).on("click", ".btn-delete", function () {
        //     let url = $(this).attr('data-url');
        //     $('.btn-confirm').attr('data-url', url);
        //     $('#myModal').modal('show');
        // });

        function fetch_supplier_data(query = '') {
            $.ajax({
                url: "{{ route('search_all') }}",
                method: 'GET',
                data: {query: query},
                dataType: 'json',
                success: function (data) {
                    $('#s-all').html(data.table_data);
                    $('#count').html(data.total_data + ' CTV')
                }
            })
        }

        $(document).on('keyup', '#search', function () {
            let query = $(this).val();
            fetch_supplier_data(query);
        });
    });
</script>
<!-- END: Footer-->

<!-- END: Body-->

</html>
