<div class="d-flex mt-2 mb-2 second">
    <div class="col-lg-3 col-md-12 p-0 search">
        <input type="text" id="search" class="form-control" name="search" placeholder="Tìm kiếm">
        <div class="form-control-position">
            <i class="feather icon-search search-icon"></i>
        </div>
    </div>
    <div class="col-lg-3 col-md-12">
    </div>
    <div class="d-flex col-lg-6 col-md-12 p-0 align-items-center justify-content-lg-end">
        <span>Ngày gia nhập</span>
        <div class="col-lg-4">
            <input type="datetime-local" value="" class="form-control">
        </div>
        <div class="export">
            <center>
                <a href="{{url('admin/manage_supplier/export')}}">Xuất báo cáo</a>
            </center>
        </div>
        <img src="../../assets/admin/app-assets/images/ncc/manage_ctv/small_menu.png">
    </div>
</div>
<div class="title mb-2">
    <span class="text-black text-bold-700 font-size-17">Đã đặt hàng NCC</span>
</div>
<div class="table-responsive table-responsive-lg border_table">
    <table class="table data-list-view table-sm">
        <thead class="bg_table">
        <tr class="text-center">
            <th scope="col">Tên shop NCC</th>
            <th scope="col">Số đơn đã Dropship</th>
            <th scope="col">Tổng doanh thu bán ra</th>
            <th scope="col">Lợi nhuận thu về</th>
            <th scope="col">Thao tác</th>
        </tr>
        </thead>
        <tbody id="s-order">
{{--        @if(count($data))--}}
{{--            @foreach($data as $value)--}}
                <tr class="text-center">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <p class="text-blue text-bold-700 text-underline"></p>
                        <p class="text-blue text-bold-700 text-underline"></p>
                    </td>
                </tr>
{{--            @endforeach--}}
{{--        @else--}}
{{--            <tr>--}}
{{--                <td>Không có dữ liệu</td>--}}
{{--            </tr>--}}
{{--        @endif--}}
        </tbody>
    </table>
</div>
