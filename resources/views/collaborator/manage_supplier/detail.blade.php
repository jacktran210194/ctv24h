<?php
$page = 'manage_supplier';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ctv/manage_supplier/style.css')}}">
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">

    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content m-2">
                <!-- content -->
                <div class="header-unit mb-2">
                    <div class="title mb-2">
                        <span class="text-black text-bold-700 font-size-17">Danh mục sản phẩm NCC</span>
                    </div>
                </div>
                <div class="d-flex mt-2 mb-2 second">
                    <div class="col-lg-3 col-md-12 p-0 search">
                        <input type="text" id="searchProduct" class="form-control" name="searchProduct" placeholder="Tìm kiếm">
                        <div class="form-control-position">
                            <i class="feather icon-search search-icon"></i>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12">
                    </div>
                    <div class="d-flex col-lg-6 col-md-12 p-0 align-items-center justify-content-lg-end">
                        <span>Ngày gia nhập</span>
                        <div class="col-lg-4">
                            <input type="datetime-local" value="" class="form-control">
                        </div>
                        <div class="export">
                            <center>
                                <a >Xuất báo cáo</a>
                            </center>
                        </div>
                        <img src="../../assets/admin/app-assets/images/ncc/manage_ctv/small_menu.png">
                    </div>
                </div>
                <div class="table-responsive table-responsive-lg border_table">
                    <table class="table data-list-view table-sm">
                        <thead class="bg_table">
                        <tr>
                            <th class="color-white font-weight-bold bg-red" scope="col">Tên sản phẩm</th>
                            <th class="color-white font-weight-bold bg-red" scope="col">SKU phân loại</th>
                            <th class="color-white font-weight-bold bg-red">Phân loại hàng</th>
                            <th class="color-white font-weight-bold bg-red text-center">Giá</th>
                            <th class="color-white font-weight-bold bg-red text-center">Số lượng</th>
                        </tr>
                        </thead>
                        <tbody id="data_product">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
</body>
<script>
    $(document).ready(function () {
        fetch_customer_data();
        function fetch_customer_data(query = '') {
            $.ajax({
                url: "{{ route('search_pro') }}",
                method: 'GET',
                data: {query: query},
                dataType: 'json',
                success: function (data) {
                    $('tbody').html(data.table_data);
                    $('.total-product').text(data.total_data);
                }
            })
        }
        $(document).on('keyup', '#searchProduct', function () {
            let query = $(this).val();
            fetch_customer_data(query);
        });
    });
</script>
<!-- END: Footer-->

<!-- END: Body-->

</html>
