<?php
$page = 'order_collaborator';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="tab-bar-page">
            <ul class="tab-bar-content d-flex align-items-center justify-content-lg-start flex-wrap">
                <li class="d-flex align-items-center justify-content-lg-center filter-all">
                    <a href="{{url('admin/order_collaborator/all')}}" class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Tất cả</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-wait-confirm">
                    <a href="{{url('admin/order_collaborator/wait_confirm')}}" class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Chờ xác
                        nhận</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center active filter-wait-shipping">
                    <a href="{{url('admin/order_collaborator/wait_shipping')}}" class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Chờ lấy
                        hàng</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-shipping">
                    <a href="{{url('admin/order_collaborator/shipping')}}" class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đang giao</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-shipped">
                    <a href="{{url('admin/order_collaborator/shipped')}}" class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đã giao</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-cancel">
                    <a href="{{url('admin/order_collaborator/cancel')}}" class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đã Huỷ</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-return">
                    <a href="{{url('admin/order_collaborator/return_money')}}" class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Hoàn tiền</a>
                </li>
            </ul>
        </div>
        <div id="show_list_order">
            <div class="card p-1">
                <div class="card-content">
                    <div class="form_filter d-flex">
                        <form action="{{url('admin/order_collaborator/search_order_collaborator')}}" method="GET">
                            @csrf
                            <div class="form-label-group position-relative has-icon-right">
                                <input required type="text" id="search" class="form-control" name="order_code"
                                       placeholder="Nhập mã vận đơn...">
                                <button type="submit" style="border: none; background: none;"
                                        class="form-control-position">
                                    <img src="../../assets/admin/app-assets/images/icon_dashboard/search.png" alt="">
                                </button>
                            </div>
                        </form>
                        <div class="form-group d-flex form-time align-items-center"
                             style="width: 75%; position: absolute; right: 1%">
                            <span style="width: 100%;">Ngày đặt hàng</span>
                            <input style="width: 30%" class="filter_time form-control time-start"
                                   type="datetime-local" value="{{date('Y-m-d\TH:i:s')}}">
                            <input style="width: 30%" class="filter_time form-control time-stop"
                                   type="datetime-local" value="{{date('Y-m-d\TH:i:s')}}">
                            <button type="button" style="width: 20%;" class="btn border btn-filter-order ml-1"
                                    data-url="{{url('admin/order_collaborator/filter_order_date')}}"
                            ><i class="fa fa-search"></i>
                            </button>
                            <a class="btn border ml-1" style="width: 100%;"
                               href="{{url('admin/order_collaborator/export')}}">Xuất
                                báo
                                cáo</a>
                            <a class="ml-1"><img src="../../assets/admin/app-assets/images/icon_dashboard/icon_menu.png"
                                                 alt=""></a>
                        </div>
                    </div>
                    <div class="d-flex align-items-center" style="position: relative;">
                        <h3 id="count_order" class="mb-3 text-bold-600 text-red">{{$total_order ?? 0}} đơn hàng</h3>
                        <button style="right: 0%;" class="btn btn_main position-absolute mb-1"><img
                                src="../../assets/admin/app-assets/images/menu/vanchuyen_trang.png" alt=""> Giao hàng
                            loạt
                        </button>
                    </div>

                    <!-- content -->
                    <div class="table-responsive table-responsive-lg border_table">
                        <table class="table data-list-view table-sm">
                            <thead>
                            <tr class="bg_table text-center">
                                <th width="25%" class="text-left">Sản phẩm</th>
                                <th>Loại sản phẩm</th>
                                <th>Tên NCC</th>
                                <th>Trạng thái</th>
                                <th>Vận chuyển</th>
                                <th>Tổng đơn hàng</th>
                                <th>Lợi nhuận</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                            <tbody id="get">
                            @if(count($data_order) && $data_order)
                                @foreach($data_order as $value)
                                    <tr class="border_tr">
                                        <td colspan="10">
                                            <div class="d-flex align-items-center">
                                                <img style="border-radius: 50%" width="20px" height="20px"
                                                     src="{{$value['shop_image']}}" alt="">
                                                <b style="margin-left: 10px;">{{$value['shop_name']}}</b>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border_tr">
                                        <td>
                                            @if(count($data_order_detail))
                                                @foreach($data_order_detail as $v)
                                                    @if($v['order_id'] == $value['id'])
                                                        <div class="list_product d-flex align-items-center mb-1">
                                                            <div class="img_product">
                                                                <img class="border_radius" width="70px" height="70px"
                                                                     src="{{$v['product_image']}}"
                                                                     alt="">
                                                            </div>
                                                            <div class="info_product ml-1">
                                                    <span class="text-bold-700">{{$v['product_name']}} <span
                                                            class="text-default">(Số lượng: {{$v['quantity']}})</span></span>
                                                                <br>
                                                                <span
                                                                    class="text-sliver f-12 m-0 text-bold-700">ID đơn hàng: {{$value['order_code']}}</span>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                            <a href="{{url('admin/order_collaborator/detail/'.$value['id'])}}"
                                               class="text-underline text-bold-700">Xem thêm</a>
                                        </td>
                                        <td class="text-center text-blue text-bold-700">Hàng dropship</td>
                                        <td class="text-center text-blue text-bold-700">{{$value['supplier_name']}}</td>
                                        <td class="text-center">
                                            <span class="text-bold-700">{{$value['is_status']}}</span>
                                            <br>
                                            @if($value['status'] == 4)
                                                @if($value['cancel_confirm'] == 1)
                                                    <span
                                                        class="text-sliver f-12 text-bold-700">{{$value['user_cancel']}}</span>
                                                    <br>
                                                @endif
                                                <span
                                                    class="text-sliver f-12 text-bold-700">{{$value['reason_cancel']}}</span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <p class="text-bold-700">{{$value['shipping']}}</p>
                                        </td>
                                        <td class="text-center">
                                <span
                                    class="text-red f-18 text-bold-700">{{number_format($value['total_price'])}} đ</span>
                                            <p class="text-sliver">{{$value['payment']}}</p>
                                        </td>
                                        <td class="text-center text-red f-18 text-bold-700">{{number_format($value['bonus'])}}
                                            đ
                                        </td>
                                        <td class="text-center">
                                            <a href="{{url('admin/order_collaborator/detail/'.$value['id'])}}"
                                               class="text-blue text-underline text-bold-700 btn-detail-order">Chi
                                                tiết</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            @if(count($data_order_ctv) && $data_order_ctv)
                                @foreach($data_order_ctv as $value)
                                    <tr class="border_tr">
                                        <td colspan="10">
                                            <div class="d-flex align-items-center">
                                                <img style="border-radius: 50%" width="20px" height="20px"
                                                     src="{{$value['shop_image']}}" alt="">
                                                <b style="margin-left: 10px;">{{$value['shop_name']}}</b>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border_tr">
                                        <td>
                                            @if(count($data_order_detail))
                                                @foreach($data_order_detail as $v)
                                                    @if($v['order_id'] == $value['order_id'])
                                                        <div class="list_product d-flex align-items-center mb-1">
                                                            <div class="img_product">
                                                                <img class="border_radius" width="70px" height="70px"
                                                                     src="{{$v['product_image']}}"
                                                                     alt="">
                                                            </div>
                                                            <div class="info_product ml-1">
                                                    <span class="text-bold-700">{{$v['product_name']}} <span
                                                            class="text-default">(Số lượng: {{$v['quantity']}})</span></span>
                                                                <br>
                                                                <span
                                                                    class="text-sliver f-12 m-0 text-bold-700">ID đơn hàng: {{$value['order_code']}}</span>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                            <a href="{{url('admin/order_collaborator/detail/'.$value['order_id'])}}"
                                               class="text-underline text-bold-700">Xem thêm</a>
                                        </td>
                                        <td class="text-center text-blue text-bold-700">Hàng dropship</td>
                                        <td class="text-center text-blue text-bold-700">{{$value['supplier_name']}}</td>
                                        <td class="text-center">
                                            <span class="text-bold-700">{{$value['is_status']}}</span>
                                            <br>
                                            @if($value['status'] == 4)
                                                @if($value['cancel_confirm'] == 1)
                                                    <span
                                                        class="text-sliver f-12 text-bold-700">{{$value['user_cancel']}}</span>
                                                    <br>
                                                @endif
                                                <span
                                                    class="text-sliver f-12 text-bold-700">{{$value['reason_cancel']}}</span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <p class="text-bold-700">{{$value['shipping']}}</p>
                                        </td>
                                        <td class="text-center">
                                <span
                                    class="text-red f-18 text-bold-700">{{number_format($value['total_price'])}} đ</span>
                                            <p class="text-sliver">{{$value['payment']}}</p>
                                        </td>
                                        <td class="text-center text-red f-18 text-bold-700">{{number_format($value['profit'])}}
                                            đ
                                        </td>
                                        <td class="text-center">
                                            <a href="{{url('admin/order_collaborator/detail/'.$value['order_id'])}}"
                                               class="text-blue text-underline text-bold-700 btn-detail-order">Chi
                                                tiết</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        let url_filter = window.location.origin;

        //filter status

        //all

        // fetch_customer_data();
        // getData();


        function filter() {
            let url = $('.btn-filter-order').attr('data-url');
            let data = {};
            data['time_start'] = $('.time-start').val();
            data['time_stop'] = $('.time-stop').val();
            if (data['time_start'] == '' || data['time_stop'] == '' || (data['time_start'] > data['time_stop'])) {
                swal("Thông báo", "Vui lòng chọn khoảng thời gian phù hợp", "error");
                return false;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: data,
                success: function (data) {
                    $('tbody').html(data.table_data);
                    $('#count_order').html(data.total_data + ' đơn hàng')
                },
                error: function () {
                    console.log('error')
                },
            });
        }

        $(document).on('click', '.btn-filter-order', function () {
            filter();
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
