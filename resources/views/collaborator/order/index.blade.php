<?php
$page = 'order_collaborator';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="tab-bar-page">
            <ul class="tab-bar-content d-flex align-items-center justify-content-lg-start flex-wrap">
                <li class="d-flex align-items-center justify-content-lg-center active filter-all"
                    data-url-all="{{url('admin/order_collaborator/all')}}">
                    <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Tất cả</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-wait-confirm"
                    data-url-waitn-confirm="{{url('admin/order_collaborator/wait_confirm')}}">
                    <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Chờ xác
                        nhận</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-wait-shipping"
                    data-url-wait-shipping="{{url('admin/order_collaborator/wait_shipping')}}">
                    <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Chờ lấy
                        hàng</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-shipping"
                    data-url-shipping="{{url('admin/order_collaborator/shipping')}}">
                    <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đang giao</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-shipped"
                    data-url-shipped="{{url('admin/order_collaborator/shipped')}}">
                    <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đã giao</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-cancel"
                    data-url-cancel="{{url('admin/order_collaborator/cancel')}}">
                    <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đã Huỷ</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center filter-return"
                    data-url-return="{{url('admin/order_collaborator/return_money')}}">
                    <a class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Hoàn tiền</a>
                </li>
            </ul>
        </div>
        <div id="show_list_order">
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        let url_filter = window.location.origin;
        filter_all();
        function filter_all() {
            let url = url_filter + '/admin/order_collaborator/all';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.filter-all').addClass('active');
                    $('.filter-wait-confirm').removeClass('active');
                    $('.filter-wait-shipping').removeClass('active');
                    $('.filter-shipping').removeClass('active');
                    $('.filter-shipped').removeClass('active');
                    $('.filter-cancel').removeClass('active');
                    $('.filter-return').removeClass('active');
                    $("#show_list_order").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        }
        function filter_wait_confirm() {
            let url = url_filter + '/admin/order_collaborator/wait_confirm'
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.filter-wait-confirm').addClass('active');
                    $('.filter-all').removeClass('active');
                    $('.filter-wait-shipping').removeClass('active');
                    $('.filter-shipping').removeClass('active');
                    $('.filter-shipped').removeClass('active');
                    $('.filter-cancel').removeClass('active');
                    $('.filter-return').removeClass('active');
                    $("#show_list_order").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        }
        function filter_wait_shipping() {
            let url = url_filter + '/admin/order_collaborator/wait_shipping'
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.filter-wait-shipping').addClass('active');
                    $('.filter-wait-confirm').removeClass('active');
                    $('.filter-all').removeClass('active');
                    $('.filter-shipping').removeClass('active');
                    $('.filter-shipped').removeClass('active');
                    $('.filter-cancel').removeClass('active');
                    $('.filter-return').removeClass('active');
                    $("#show_list_order").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        }
        function filter_shipping() {
            let url = url_filter + '/admin/order_collaborator/shipping'
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.filter-shipping').addClass('active');
                    $('.filter-wait-confirm').removeClass('active');
                    $('.filter-wait-shipping').removeClass('active');
                    $('.filter-all').removeClass('active');
                    $('.filter-shipped').removeClass('active');
                    $('.filter-cancel').removeClass('active');
                    $('.filter-return').removeClass('active');
                    $("#show_list_order").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        }
        function filter_shipped() {
            let url = url_filter + '/admin/order_collaborator/shipped'
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.filter-shipped').addClass('active');
                    $('.filter-wait-confirm').removeClass('active');
                    $('.filter-wait-shipping').removeClass('active');
                    $('.filter-shipping').removeClass('active');
                    $('.filter-all').removeClass('active');
                    $('.filter-cancel').removeClass('active');
                    $('.filter-return').removeClass('active');
                    $("#show_list_order").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        }
        function filter_cancel() {
            let url = url_filter + '/admin/order_collaborator/cancel'
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.filter-cancel').addClass('active');
                    $('.filter-wait-confirm').removeClass('active');
                    $('.filter-wait-shipping').removeClass('active');
                    $('.filter-shipping').removeClass('active');
                    $('.filter-shipped').removeClass('active');
                    $('.filter-all').removeClass('active');
                    $('.filter-return').removeClass('active');
                    $("#show_list_order").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        }
        function filter_return_money() {
            let url = url_filter + '/admin/order_collaborator/return_money'
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.filter-return').addClass('active');
                    $('.filter-wait-confirm').removeClass('active');
                    $('.filter-wait-shipping').removeClass('active');
                    $('.filter-shipping').removeClass('active');
                    $('.filter-shipped').removeClass('active');
                    $('.filter-cancel').removeClass('active');
                    $('.filter-all').removeClass('active');
                    $("#show_list_order").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        }
        //filter status

        //all
        $(document).on('click', '.filter-all', function () {
            filter_all();
        });

        //chờ xác nhận
        $(document).on('click', '.filter-wait-confirm', function () {
            filter_wait_confirm();
        });

        //chờ lấy hàng
        $(document).on('click', '.filter-wait-shipping', function () {
            filter_wait_shipping();
        });

        //đang giao hàng
        $(document).on('click', '.filter-shipping', function () {
            filter_shipping();
        });

        //đã giao hàng
        $(document).on('click', '.filter-shipped', function () {
            filter_shipped();
        });

        //đã huỷ
        $(document).on('click', '.filter-cancel', function () {
            filter_cancel();
        });

        //Hoàn tiền
        $(document).on('click', '.filter-return', function () {
            filter_return_money();
        });

        // fetch_customer_data();
        // getData();

        function fetch_customer_data(query = '') {
            $.ajax({
                url: "{{ route('search_orderCollaborator') }}",
                method: 'GET',
                data: {query: query},
                dataType: 'json',
                success: function (data) {
                    $('tbody').html(data.table_data);
                    $('#count_order').html(data.total_data + ' đơn hàng')
                }
            })
        }

        $(document).on('keyup', '#search', function () {
            let query = $(this).val();
            fetch_customer_data(query);
        });

        function filter() {
            let url = $('.btn-filter-order').attr('data-url');
            let data = {};
            data['time_start'] = $('.time-start').val();
            data['time_stop'] = $('.time-stop').val();
            if (data['time_start'] == '' || data['time_stop'] == '' || (data['time_start'] > data['time_stop'])) {
                swal("Thông báo", "Vui lòng chọn khoảng thời gian phù hợp", "error");
                return false;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: data,
                success: function (data) {
                    $('tbody').html(data.table_data);
                    $('#count_order').html(data.total_data + ' đơn hàng')
                },
                error: function () {
                    console.log('error')
                },
            });
        }

        $(document).on('click', '.btn-filter-order', function () {
            filter();
        });
        // getData();

        // function getData() {
        //     let status = $('#status').val();
        //     $.get('/admin/order_collaborator/filter_order/' + status, function (data) {
        //         $('#get').html(data);
        //     })
        // }

        // $(document).on("click", ".btn-delete", function () {
        //     let url = $(this).attr('data-url');
        //     $('.btn-confirm').attr('data-url', url);
        //     $('#myModal').modal('show');
        // });
        // $("#status").on('change', function (e) {
        //     getData();
        // });

        // function filter_order_date() {
        //     let url = $('.btn-filter-order').attr('data-url');
        //     let data = {};
        //     data['time_start'] = $('.time-start').val();
        //     data['time_stop'] = $('.time-stop').val();
        //     $.ajaxSetup({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         }
        //     });
        //     $.ajax({
        //         url: url,
        //         type: "POST",
        //         dataType: 'json',
        //         data: data,
        //         success: function (data) {
        //             $('tbody').html(data.table_data);
        //             $('#count').html('Tổng số đơn hàng: ' + data.total_data)
        //         },
        //         error: function () {
        //             console.log('error')
        //         },
        //     });
        // }

        // $(document).on('click', '.btn-filter-order', function () {
        //     filter_order_date();
        // })
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
