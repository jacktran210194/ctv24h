<?php
$page = 'rank_ctv';

use Illuminate\Support\Facades\URL;
$data_login = Session::get('data_collaborator');
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h3>{{$title}}</h3>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-9">
                <div class="card">
                    <div class="card-content m-1">
                        <!-- content -->
                        <h3>Nâng cấp hạng</h3>
                        <br>
                        <div class="d-flex justify-content-sm-between align-items-center">
                            <div class="w-50" style="padding: 0 14px">
                                <div class="p-2 border-radius-16 position-relative">
                                    <div class="content-card position-relative" style="z-index: 2;">
                                        <div class="d-flex">
                                            <div data-option="silver">
                                                <h3>CTV Bạc</h3>
                                                <p>Giảm giá 1,5% trên mỗi sản phẩm</p>
                                                <button class="btn btn-danger btn-update-point mt-5">Nâng cấp ngay</button>
                                            </div>
                                        </div>
                                    </div>
                                    <img class="position-absolute img_bg border-radius-16" src="{{url('assets/frontend/Icon/rank/bg_16891.png')}}" alt="">
                                </div>
                            </div>
                            <div class="w-50 d-flex">
                            <div class="w-50 p-1 m-1" style="background-color: #FFF8E1; border-radius: 16px;">
                                <div class="content-card m-1">
                                    <div class="d-flex">
                                        <div data-option="gold">
                                            <img class="float-right"
                                                 src="{{url('assets/frontend/Icon/rank/cup.png')}}"
                                                 alt="">
                                            <div class="clearfix"></div>
                                            <h3>CTV Vàng</h3>
                                            <p>Giảm giá 1,5% trên mỗi sản phẩm</p>
                                            <button class="btn btn-danger btn-update-point">Nâng cấp ngay</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="background-color: #E1F5FD; border-radius: 16px;" class="w-50 p-1 m-1">
                                <div class="content-card m-1">
                                    <div class="d-flex">
                                        <div data-option="diamond">
                                            <img class="float-right"
                                                 src="{{url('assets/frontend/Icon/rank/cup.png')}}"
                                                 alt="">
                                            <div class="clearfix"></div>
                                            <h3>CTV Kim cương</h3>
                                            <p>Giảm giá 1,5% trên mỗi sản phẩm</p>
                                            <button class="btn btn-danger btn-update-point">Nâng cấp ngay</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="point-rank p-1 mr-2" style="border-radius: 16px; border: 1px solid #E6E7E9">
                                    <h3>Điểm hạng</h3>
                                    <div class="d-flex justify-content-lg-center position-relative mg-top-30">
                                        <img src="{{url('assets/frontend/Icon/rank/piec_chart.png')}}" alt="">
                                        <div class="position-absolute number-point text-center">
                                            <span class="font-weight-bold font-size-36 color-black d-block">{{$point_ctv['point']}}</span>
                                            <span class="font-weight-bold font-size-24 color-black d-block">Point</span>
                                        </div>
                                    </div>
                                    <img src="{{url('assets/frontend/Icon/rank/image.png')}}" alt="" class="w-100">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="point-rank p-1 mr-2" style="border-radius: 16px; border: 1px solid #E6E7E9">
                                    <h3>Chính sách hạng</h3>
                                    <br>
                                    <p class="text-justify">
                                        Chương trình có 4 hạng mức thành tích dành cho người dùng Shopee bao gồm : Thành
                                        viên " , " Bạc " . " Vàng " , " Kim Cương " . Thành tích của người dùng sẽ được
                                        tính dựa trên tổng số đơn hàng và mức chi tiêu đã thực hiện trên sàn CTV24 trong
                                        vòng 6 tháng gần nhất.

                                        Để thăng hạng , người mua hàng phải đạt được yêu cầu về số lượng đơn hàng và mức
                                        tiêu dùng của hạng mức tiếp theo.

                                        Ưu đãi mới nhất sẽ được cập nhật vào ngày 1 hàng tháng dựa trên thử hạng mới
                                        nhất của người dùng

                                        Thành tích của người dùng sẽ được tải xét duyệt 2 lần / năm , tiến hành vào ngày
                                        1 của tháng 1 và tháng 7

                                        Các loại đơn mua hàng không dùng để tích luỹ thành tích : - Đơn mua hàng Voucher
                                        & Dịch vụ - Đơn nạp giá trị các chương trình maketing của CTV24 - Đơn hàng vi
                                        phạm chính sách của CTV24

                                        Chính sách thăng hạng áp dụng cho toàn bộ người dùng trên Ứng dụng CTV24. Đối
                                        tượng người dùng khác nhau sẽ có chính sách khác nhau và chương trình áp dụng
                                        khác nhau theo từng thời điểm.

                                        Các ưu đãi của Các hạng mức tương ứng không thể quy đổi hoặc đổi thành tiền mặt
                                        và không thể chuyển nhượng dưới mọi hình thức


                                    </p>
                                    <div class="d-flex justify-content-lg-center" style="margin-top: 20px">
                                    <img src="{{url('assets/frontend/Icon/rank/img_update_rank.png')}}" alt="" class="w-50">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card">
                    <div class="card-content m-1">
                        <div class="profile">
                            <div class="text-center mb-1">
                                <img src="{{url('assets/frontend/Icon/rank/avt.png')}}" alt="">
                            </div>
                            <div class="text-center">
                                <h4><?= isset($data_login) ? $data_login['name'] : '' ?></h4>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-success btn-sm"><i class="fa fa-user"></i> CTV</button>
                            </div>
                        </div>
                        <br>
                        <div class="rank-user p-1" style="border-radius: 16px;border: 1px solid #E6E7E9;">
                            <div class="d-flex mb-1 align-items-center">
                                <h4>Hạng {{$title_rank}}</h4>
                                <img src="{{url('assets/frontend/Icon/rank/cup-bac.png')}}" alt="">
                            </div>
                            <div class="d-flex align-items-center">
                                <p class="mr-1">Còn  <span class="text-bold-700" style="color: yellow;">{{$point}}</span></p>
                                <p>Lên hạng {{$rank}}</p>
                                <img class="img-fluid" src="{{url('assets/frontend/Icon/rank/cup-vang.png')}}" alt="">
                            </div>
                        </div>
                        <br>
                        <h3>Các cấp hạng</h3>
                        <div class="rank-god p-1" style="border-radius: 16px;border: 1px solid #E6E7E9;">
                            <div class="d-flex align-items-center w-100">
                                <div class="d-flex align-items-center justify-content-sm-center border-radius-8" style="width: 50px; height: 50px; background: linear-gradient(180deg, #D106B1 0%, rgba(255, 255, 255, 0) 100%), #A4009D;">
                                    <img src="{{url('assets/frontend/Icon/rank/OBJECTS.png')}}" alt="">
                                </div>
                                <div class="ml-3">
                                    <p class="text-bold-700">Hạng thành viên </p>
                                    <p class="text-bold-700 m-0" style="color: red;">0/1500</p>
{{--                                    <img src="{{url('assets/frontend/Icon/rank/diem.png')}}" alt="">--}}
                                </div>
                            </div>
                        </div>
                        <div class="rank-god mg-top-20 p-1" style="border-radius: 16px;border: 1px solid #E6E7E9;">
                            <div class="d-flex align-items-center w-100">
                                <div class="d-flex align-items-center justify-content-sm-center border-radius-8" style="width: 50px; height: 50px; background: linear-gradient(90deg, #40D5E6 0%, #5577FF 100%);">
                                    <img src="{{url('assets/frontend/Icon/rank/OBJECTS.png')}}" alt="">
                                </div>
                                <div class="ml-3">
                                    <p class="text-bold-700">Hạng bạc</p>
                                    <p class="text-bold-700 m-0" style="color: red;">500/1500</p>
{{--                                    <img src="{{url('assets/frontend/Icon/rank/diem.png')}}" alt="">--}}
                                </div>
                            </div>
                        </div>
                        <div class="rank-god mg-top-20 p-1" style="border-radius: 16px;border: 1px solid #E6E7E9;">
                            <div class="d-flex align-items-center w-100">
                                <div class="d-flex align-items-center justify-content-sm-center border-radius-8" style="width: 50px; height: 50px; background: #4A4DE6">
                                    <img src="{{url('assets/frontend/Icon/rank/OBJECTS.png')}}" alt="">
                                </div>
                                <div class="ml-3">
                                    <p class="text-bold-700">Hạng vàng</p>
                                    <p class="text-bold-700 m-0" style="color: red;">1000/1500</p>
{{--                                    <img src="{{url('assets/frontend/Icon/rank/diem.png')}}" alt="">--}}
                                </div>
                            </div>
                        </div>
                        <div class="rank-god mg-top-20 p-1" style="border-radius: 16px;border: 1px solid #E6E7E9;">
                            <div class="d-flex align-items-center w-100">
                                <div class="d-flex align-items-center justify-content-sm-center border-radius-8" style="width: 50px; height: 50px; background: #FFA000">
                                    <img src="{{url('assets/frontend/Icon/rank/OBJECTS.png')}}" alt="">
                                </div>
                                <div class="ml-3">
                                    <p class="text-bold-700">Hạng kim cương</p>
                                    <p class="text-bold-700 m-0" style="color: red;">1500/1500</p>
{{--                                    <img src="{{url('assets/frontend/Icon/rank/diem.png')}}" alt="">--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-popup-current">
    <div class="popup-current-piont">
        <div class="header-popup-2">
            <h2 class="m-0 color-white text-center font-size-36">Nâng hạng</h2>
        </div>
        <div class="body-popup">
            <p class="title-body-popup">Số điểm hiện tại </p>
            <p class="number">
                {{$point_ctv['point']}} điểm
            </p>
            <p class="title-body-popup">Số điểm còn thiếu để nâng hạng {{$rank}}</p>
            <p class="number">
                {{$point}} điểm
            </p>
        </div>
        <div class="body-popup" style="border-bottom: none">
            <p class="title-body-popup">Số tiền để lên hạng <span class="title-rank"></span></p>
            <p class="number price-point"></p>
        </div>
             <div class="box-btn d-flex justify-content-lg-end" style="padding-right: 30px">
                <button class="btn-exchange-coins btn-load-point">Nâng hạng</button>
            </div>
    </div>
</div>
<div class="form-load-point d-flex justify-content-lg-center align-items-center position-fixed">
    <div class="border-radius-16 bg-white p-2" style="max-width: 450px;">
        <h3 class="font-size-24 color-black mg-top-20 font-weight-bold">Hình thức nạp điểm</h3>
        <div class="mg-top-40 option-popup">
            <div class="option d-flex justify-content-sm-between">
                <div class="radio d-flex justify-content-sm-center align-items-center active" data-option="exchangeCoins">
                    <p class="m-0 bg-white" style="width: 10px; height: 10px; border-radius: 50%"></p>
                </div>
                <label class="font-size-15">Thanh toán bằng thẻ quốc tế Visa, Master, JCB</label>
            </div>
            <div class="option d-flex justify-content-sm-between">
                <div class="radio d-flex justify-content-sm-center align-items-center" data-option="exchangeCoins">
                    <p class="m-0 bg-white" style="width: 10px; height: 10px; border-radius: 50%"></p>
                </div>
                <label class="font-size-15">Thanh toán bằng thẻ ATM nội địa/Internet Banking (Miễn phí thanh toán)</label>
            </div>
            <div class="option d-flex justify-content-sm-between">
                <div class="radio d-flex justify-content-sm-center align-items-center" data-option="exchangeCoins">
                    <p class="m-0 bg-white" style="width: 10px; height: 10px; border-radius: 50%"></p>
                </div>
                <label class="font-size-15">Thanh toán bằng ví Momo</label>
            </div>
            <div class="option d-flex justify-content-sm-between">
                <div class="radio d-flex justify-content-sm-center align-items-center" data-option="wallet-ctv">
                    <p class="m-0 bg-white" style="width: 10px; height: 10px; border-radius: 50%"></p>
                </div>
                <label class="font-size-15">Thanh toán bằng ví CTV24H</label>
            </div>
        </div>
        <div class="d-flex justify-content-lg-end mg-top-20">
            <button class="btn-exchange-coins btn-popup-pass">Nâng hạng</button>
        </div>
    </div>
</div>
<div class="form-confirm-pass position-fixed d-flex justify-content-lg-center align-items-center">
    <div class="border-radius-16 bg-white p-1" style="max-width: 314px">
        <div class="d-flex justify-content-lg-center align-items-center">
            <img width="26px" src="{{url('assets/frontend/Icon/rank/Error.png')}}" alt="">
        </div>
        <div class="text-center mg-top-10">
            <p class="m-0 font-weight-bold font-size-15">Xác nhận Nạp điểm</p>
            <p class="m-0 font-size-13" style="line-height: 24px">Bạn có muốn đổi điểm không? Nếu có bạn vui lòng nhập mật khẩu xác nhận.</p>
            <div class="mg-top-20">
                <p class="m-0 font-size-13 text-left">Mật Khẩu</p>
                <input class="w-100 border-radius-4 p-1 border out-line-none input-pass" type="password" style="height: 40px">
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-sm-between">
            <button class="btn-exchange-coins btn-confirm-pass" style="width: calc(50% - 10px)">Xác nhận</button>
            <button class="btn-exchange-coins color-red btn-back-pass" style="background: linear-gradient( 180deg, #d1292f1f 0%, #ff572217 155.26%);width: calc(50% - 10px)">Huỷ</button>
        </div>
    </div>
</div>
<div class="form-confirm-otp position-fixed d-flex justify-content-lg-center align-items-center">
    <div class="border-radius-16 bg-white p-1" style="max-width: 314px">
        <div class="text-center mg-top-10">
            <p class="font-weight-bold font-size-15 color-black">Nhập mã OTP</p>
            <p class="font-size-13" style="line-height: 24px">Bạn vui lòng nhập mã OTP được gửi trên số điện thoại đăng ký ngân hàng để xác nhận </p>
            <div class="mg-top-10">
                <p class="m-0 font-size-13 text-left">Mã OTP</p>
                <input class="w-100 border-radius-4 p-1 border out-line-none input-otp" type="text" placeholder="Nhập mã OTP được gửi về số điện thoại" style="height: 40px">
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-sm-between">
            <button class="btn-exchange-coins btn-confirm-otp" style="width: calc(50% - 10px)">Xác nhận</button>
            <button class="btn-exchange-coins color-red" style="background: linear-gradient( 180deg, #d1292f1f 0%, #ff572217 155.26%);width: calc(50% - 10px)">Huỷ</button>
        </div>
    </div>
</div>
<div class="form-confirm-success position-fixed d-flex justify-content-lg-center align-items-center">
    <div class="border-radius-16 bg-white p-1" style="max-width: 314px">
        <div class="d-flex justify-content-lg-center align-items-center">
            <img width="30px" src="{{url('assets/frontend/Icon/rank/Success.png')}}" alt="">
        </div>
        <div class="text-center mg-top-20">
            <p class="m-0 font-weight-bold font-size-15">Nâng hạng thành công</p>
            <p class="m-0 font-size-13" style="line-height: 24px">Hệ thống thông báo bạn đã nâng hạng thành công</p>
        </div>
        <div class="d-flex align-items-center justify-content-sm-center">
            <button class="btn-exchange-coins">Xác nhận</button>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        let price,title_rank,point,type_rank,price_rank;
        let wallet_ctv = 0;
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('#myModal').modal('show');
        });
        $('.btn-update-point').click(function () {
            let rank = $(this).parent().attr("data-option");
            switch (rank) {
                case 'silver':
                    price = '99$';
                    price_rank = 99000;
                    title_rank = 'bạc';
                    point = 500;
                    $('.title-rank').text(title_rank);
                    $('.price-point').text(price);
                    $('.container-popup-current').addClass('active');
                    break;
                case 'gold':
                     price = '199$';
                     price_rank = 199000;
                    title_rank = 'vàng';
                    point = 1000;
                    $('.title-rank').text(title_rank);
                    $('.price-point').text(price);
                    $('.container-popup-current').addClass('active');
                    break;
                case 'diamond':
                    title_rank = 'kim cương';
                    price = '299$';
                    price_rank = 299000;
                    point = 1500;
                    $('.title-rank').text(title_rank);
                    $('.price-point').text(price);
                    $('.container-popup-current').addClass('active');
                    break;
                default:
            }
        });
        $('.container-popup-current').click(function (e) {
            if ($(e.target).is(".container-popup-current") === true) {
                $(this).removeClass('active');
            }
        });
        $('.btn-load-point').click(function () {
            $('.form-load-point').addClass('active');
        });
        $('.form-load-point').click(function (e) {
            if ($(e.target).is(".form-load-point") === true) {
                $(this).removeClass('active');
            }
        });
        $('.btn-popup-pass').click(function () {
            type_rank = $(".form-load-point .radio.active").attr("data-option");
            let data = {};
            wallet_ctv = 1;
            data['title'] = title_rank;
            data['price_rank'] = price_rank;
            if (type_rank === 'wallet-ctv'){
                $.ajax({
                   url : window.location.origin + '/admin/collaborator_rank/check-wallet',
                   type : 'POST',
                   dataType : 'json',
                   data:  data,
                    success: function (data) {
                        if (!data.status){
                            swal('Thất Bại',data.msg, 'error');
                        }else {
                            $('.form-confirm-pass').addClass('active');
                        }
                    }
                });
            }else {
                type_rank = null;
                wallet_ctv = 0;
                $('.form-confirm-pass').addClass('active');
            }
        });
        $('.form-confirm-pass').click(function (e) {
            if ($(e.target).is(".form-confirm-pass") === true) {
                $(this).removeClass('active');
            }
        });
        $('.radio').click(function () {
            $('.option .radio').removeClass("active");
            $(this).parent().each(function () {
               $(this).children().addClass("active");
            });
        });
        $('.btn-back-pass').click(function () {
            $('.form-confirm-pass').removeClass("active");
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.btn-confirm-pass').click(function () {
            let url = window.location.origin + '/admin/collaborator_rank/buy-points';
            let data={};
            data['pass'] = $('.input-pass').val();
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (data) {
                    if (data.status){
                        $('.form-confirm-otp').addClass("active");
                    }else {
                        alert('sai mật khẩu')
                    }
                }
            })
        });
        $('.btn-confirm-otp').click(function () {
            let url = window.location.origin + '/admin/collaborator_rank/buy-points';
            let data={};
            data['point'] = point;
            data['price_rank'] = price_rank;
            data['check'] = wallet_ctv;
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (data) {
                    if (data.status){
                        $('.form-confirm-success').addClass("active");
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    }else {
                        alert('sai mật khẩu')
                    }
                }
            });
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
