<?php
$page='collaborator/my-shop';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="d-flex justify-content-sm-center align-items-center bg-white" style="padding: 60px 50px">
            <div class="w-50" style="padding-right: 20px">
                <h3 class="font-weight-bold font-size-24 color-black text-left">Giới thiệu về shop</h3>
                <div class="mg-top-30">
                    <p class="font-size-17 text-justify" style="line-height: 30px">- Là website tập hợp các sản phẩm mà bạn đã lưu lại và phân loại thành bộ sưu tập.<br>
                        - Là nơi khách hàng của bạn có thể xem thêm tất cả sản phẩm bạn đang bán với giá bán lẻ do bạn tự quy định, tùy chỉnh.<br>
                        - Shop Thành viên cũng là nơi bạn dễ dàng chốt đơn hơn, xây dựng thương hiệu bán hàng cá nhân hiệu quả hơn, lâu dài và chuyên nghiệp hơn.<br>
                        - Bạn có thể tùy chỉnh link webshop TV của bạn bên dưới, bấm vào biểu tượng chỉnh sửa, đằng sau link webshop, và nhập lại tên webshop mà bạn muốn đặt. Lưu ý chỉ nên thay đổi 1 lần, hạn chế thay đổi link webshop dẫn tới khách không truy cập lại được.<br><br>

                        ** Ví dụ link webshop của một Thành viên: https://holy.gocmuasam.net<br><br>

                        Sản phẩm trên webshop này là do Bạn lưu lại trên Cotavi, lưu trên app hay trên web đều được, khi lưu lại thì Bạn sẽ phân loại sản phẩm vào từng bộ sưu tập mà Bạn tùy ý đặt tên. Khi lưu lại thì giá lẻ sẽ là giá lẻ mặc định trên Cotavi, Thành viên muốn chỉnh sửa lại giá lẻ thì vào lại phần Tài khoản -> chọn Sản phẩm đã lưu để chỉnh sửa giá lẻ.<br><br>

                        Có thắc mắc nào hoặc cần hỗ trợ tạo webshop TV, vui lòng Chat với Cotavi.<br>
                    </p>
                    <p class="font-weight-bold font-size-17 color-red">Link shop: https://www.figma.com/file/Yir5xWstWhmEXNgAbsAnhr/CTV</p>
                </div>
            </div>
            <div class="w-50 d-flex justify-content-sm-center align-content-center" style="padding-left: 20px">
                <div>
                    <div class="d-flex justify-content-sm-center align-content-center">
                        <img src="{{url('../../assets/frontend/Icon/rank/image_35.png')}}">
                    </div>
                    <div class="d-flex justify-content-sm-center align-content-center mg-top-30" style="margin-right: -20px">
                        <img src="{{url('../../assets/frontend/Icon/rank/banner-shop.png')}}" style="margin-right: 20px">
                        <img src="{{url('../../assets/frontend/Icon/rank/banner-shop-1.png')}}" style="margin-right: 20px">
                        <img src="{{url('../../assets/frontend/Icon/rank/banner-shop-2.png')}}" style="margin-right: 20px">
                        <img src="{{url('../../assets/frontend/Icon/rank/banner-shop-3.png')}}" style="margin-right: 20px">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  Chat -->
    <div class="chat-footer d-flex justify-content-lg-end w-100">
        <button class="border-radius-8 out-line-none bg-red font-weight-bold color-white d-flex align-items-center"
                style="margin-right: 20px">
            <img src="../../assets/admin/app-assets/images/icons/live_chat.png">
            <a class="color-white" style="margin-left: 10px">Chat với CTV24</a>
        </button>
        <button
            class="border_box border-radius-8 out-line-none font-weight-bold color-gray d-flex align-items-center">
            <img src="../../assets/admin/app-assets/images/icons/chat-bubbles-with-ellipsis.png">
            <a class="color-white" style="margin-left: 10px">Chat với khách hàng</a>
        </button>
    </div>
    <!-- END: Chat -->
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
