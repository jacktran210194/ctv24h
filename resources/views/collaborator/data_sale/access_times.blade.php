<div class=" mg-top-30 justify-content-sm-between">
    <div class="bg-white p-2 border_box border-radius-16 w-100">
        <h3 class="font-weight-bold font-size-24">Tổng Quan</h3>
        <p class="font-size-14 color-black">Phân tích lượng truy cập trang chi tiết sản phẩm và trang chủ của Shop trên ứng dụng Shopee và trên máy tính.</p>
        <div class="mg-top-30 d-flex">
            <div class="w-30 d-flex border_box border-radius-4" style="margin-right: 15px">
                <div class="h-100 bg-red d-flex align-items-center justify-content-lg-center" style="width: 120px;border-radius: 8px 0px 0px 8px;">
                    <div class="w-100">
                        <div class="d-flex justify-content-lg-center w-100">
                            <img src="../../assets/admin/app-assets/images/icons/medal.png">
                        </div>
                        <p class="font-weight-bold font-size-17 color-white text-center" style="margin-top: 10px">Lượt xem</p>
                    </div>
                </div>
                <div class="d-flex align-items-lg-end h-100">
                    <ul class="p-0 m-0 information">
                        <li class="font-weight-bold font-size-17 color-black d-flex align-items-center">Lượt xem</li>
                        <li class="font-weight-bold font-size-17 color-black d-flex align-items-center">Số Lượt xem trung bình</li>
                        <li class="font-weight-bold font-size-17 color-black d-flex align-items-center">Thời gian xem trung bình</li>
                        <li class="font-weight-bold font-size-17 color-black d-flex align-items-center">Tỉ lệ thoát trang </li>
                    </ul>
                </div>
            </div>
            <table class="border_box border-radius-4 w-70">
                <tbody>
                <tr class="bg-red color-white font-weight-bold font-size-17 border-radius-top tr-header">
                    <th class="text-left border-top-left">Tất cả</th>
                    <th>Ứng dụng</th>
                    <th class="border-top-right">Máy tính </th>
                </tr>
                <tr class="font-weight-bold font-size-17 border_bottom tr-body">
                    <td class="text-left">
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="font-weight-bold font-size-17 border_bottom tr-body">
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="font-weight-bold font-size-17 border_bottom tr-body">
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">00:00:51</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">00:00:51</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">00:00:51</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="font-weight-bold font-size-17 tr-body">
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="mg-top-30 d-flex">
            <div class="w-30 d-flex border_box border-radius-4" style="margin-right: 15px">
                <div class="h-100 bg-red d-flex align-items-center justify-content-lg-center" style="width: 120px;border-radius: 8px 0px 0px 8px;">
                    <div class="w-100">
                        <div class="d-flex justify-content-lg-center w-100">
                            <img src="../../assets/admin/app-assets/images/icons/user_3.png">
                        </div>
                        <p class="font-weight-bold font-size-17 color-white text-center" style="margin-top: 10px">Khách truy cập</p>
                    </div>
                </div>
                <div class="d-flex align-items-lg-end h-100">
                    <ul class="p-0 m-0 information">
                        <li class="font-weight-bold font-size-17 color-black d-flex align-items-center">Lượt truy cập</li>
                        <li class="font-weight-bold font-size-17 color-black d-flex align-items-center">Số khách truy cập mới</li>
                        <li class="font-weight-bold font-size-17 color-black d-flex align-items-center">Số khách truy cập hiện tại</li>
                        <li class="font-weight-bold font-size-17 color-black d-flex align-items-center">Người theo dõi mới</li>
                    </ul>
                </div>
            </div>
            <table class="border_box border-radius-4 w-70">
                <tbody>
                <tr class="bg-red color-white font-weight-bold font-size-17 border-radius-top tr-header">
                    <th class="text-left border-top-left">Tất cả</th>
                    <th>Ứng dụng</th>
                    <th class="border-top-right">Máy tính </th>
                </tr>
                <tr class="font-weight-bold font-size-17 border_bottom tr-body">
                    <td class="text-left">
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="font-weight-bold font-size-17 border_bottom tr-body">
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">31</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="font-weight-bold font-size-17 border_bottom tr-body">
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">0</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">0</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">0</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="font-weight-bold font-size-17 tr-body">
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">0</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">0</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-17 color-black" style="margin-right: 15px">0</span>
                            <div class="d-flex align-items-center">
                                <span class="font-size-12 color-black" style="margin-right: 5px">100%</span>
                                <img src="../../assets/admin/app-assets/images/icons/Vector.png">
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="mg-top-30 border_box border-radius-8 bg-white p-2">
        <h3 class="font-weight-bold font-size-24 color-black">Xu hướng số liệu</h3>
        <div class="d-flex border_box border-radius-8 mg-top-30">
            <ul class="p-0  border-top-left bg-red border-bottom-left" style="margin-right: 15px; width: 180px">
                <li class="text-right color-white font-weight-bold font-size-12" style="padding:19px">Nguồn:</li>
                <li class="text-right color-white font-weight-bold font-size-12" style="padding:19px">Lượt xem:</li>
                <li class="text-right color-white font-weight-bold font-size-12" style="padding:19px">Khách truy cập:</li>
            </ul>
            <ul class="p-0 w-100">
                <li class="d-flex align-items-center">
                    <div class="d-flex align-items-center border-bottom w-100" style="padding:19px">
                        <p class="font-weight-bold font-size-12 color-black w-25 m-0 dt-trend-text" style="padding: 0 30px">Tất cả</p>
                        <p class="font-weight-bold font-size-12 color-black w-25 m-0 dt-trend-phone" style="padding: 0 30px">Điện thoại</p>
                        <p class="font-weight-bold font-size-12 color-black w-50 m-0 dt-trend-phone" style="padding: 0 30px">Máy tính</p>
                    </div>
                </li>
                <li class="d-flex align-items-center">
                    <div class="d-flex align-items-center border-bottom w-100" style="padding:19px">
                        <div class="w-25 d-flex align-items-center">
                            <p class="font-weight-bold font-size-12 color-black m-0 dt-v-text bf-v" style="padding: 0 15px; width: fit-content">Lượt xem</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                        <div class="w-25 d-flex align-items-center">
                            <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Số lượt xem trung bình</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                        <div class="w-25 d-flex align-items-center">
                            <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Thời gian xem trung bình</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                        <div class="w-25 d-flex align-items-center">
                            <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Tỉ lệ thoát trang</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                    </div>
                </li>
                <li class="d-flex align-items-center">
                    <div class="d-flex align-items-center border-bottom w-100" style="padding:19px">
                        <div class="w-25 d-flex align-items-center">
                            <p class="font-weight-bold font-size-12 color-black m-0 dt-v-text bf-v" style="padding:  0 15px; width: fit-content">Lượt truy cập</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                        <div class="w-25 d-flex align-items-center">
                            <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Số khách truy cập mới</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                        <div class="w-25 d-flex align-items-center">
                            <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Số khách truy cập hiện tại</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                        <div class="w-25 d-flex align-items-center">
                            <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Người theo dõi mới</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="mg-top-30">
        <img src="../../assets/admin/app-assets/images/icons/graph.png" style="width: 100%">
    </div>
</div>
