
<div class="mg-top-30 p-2 bg-white">
    <div class="tips mg-top-30">
        <p class="m-0 font-weight-bold font-size-12 text-center">
            Chỉ số Đơn Hàng Gián Tiếp vừa được cập nhật như sau: (1) Nếu người xem livestream từ Shop bạn và đặt hàng trong vòng 24 giờ, đây sẽ được tính là đơn hàng từ livestream. (2) Nếu đã xem nhiều livestream Shop bạn từ trước rồi mới đặt đơn, đơn sẽ được tính vào tổng đơn từ lượt xem livestream.
        </p>
    </div>
    <h3 class="font-weight-bold font-size-17 color-black mg-top-30">Chỉ số quan trọng</h3>
    <div class="d-flex justify-content-sm-between mg-top-30" style="padding: 0 2px;">
        <div class="border-radius-16 bg-1 box-data dv-ct dv-statistical">
            <div class="statistical w-100 p-2">
                <div class="d-flex title-box">
                    <h3 class="font-weight-bold font-size-17 color-black" style="margin-right: 10px">Lượt xem</h3>
                    <img src="../../assets/admin/app-assets/images/icons/icon_danger.png" width="20px" height="20px">
                </div>
                <p class=" font-weight-bold font-size-24 color-black mg-top-20">2</p>
            </div>
        </div>
        <div class="border-radius-16 bg-2 box-data dv-ct dv-statistical">
            <div class="w-100 p-2">
                <div class="d-flex title-box">
                    <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Số lượt xem cao nhất</h3>
                    <img src="../../assets/admin/app-assets/images/icons/icon_danger.png" width="20px" height="20px">
                </div>
                <p class=" font-weight-bold font-size-24 color-white mg-top-20">2</p>
            </div>
        </div>
        <div class="border-radius-16 bg-3 box-data dv-ct dv-statistical">
            <div class="w-100 p-2">
                <div class="d-flex title-box">
                    <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Thời gian xem TB</h3>
                    <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                </div>
                <p class=" font-weight-bold font-size-24 color-white mg-top-20">2</p>
            </div>
        </div>
        <div class="border-radius-16 bg-4 box-data dv-ct dv-statistical">
            <div class="w-100 p-2">
                <div class="d-flex title-box">
                    <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Đơn hàng</h3>
                    <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                </div>
                <p class=" font-weight-bold font-size-24 color-white mg-top-20">2</p>
            </div>
        </div>
        <div class="border-radius-16 bg-5 box-data dv-ct dv-statistical">
            <div class="w-100 p-2">
                <div class="d-flex title-box">
                    <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Doanh thu</h3>
                    <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                </div>
                <p class=" font-weight-bold font-size-24 color-white mg-top-20">20.000đ</p>
            </div>
        </div>
    </div>
    <div class="mg-top-30">
        <img src="../../assets/admin/app-assets/images/icons/graph.png" style="width: 100%">
    </div>
</div>
<div class="mg-top-30 bg-white p-2">
    <div class="d-flex justify-content-sm-between">
        <h3 class="font-weight-bold font-size-17 color-black">Tổng quan</h3>
        <div class="d-flex align-items-center">
            <button class="d-flex align-items-center border-radius-8 bg-white group-kg" style="padding: 10px 20px; margin-right: 20px">
                <p class="m-0">Tất cả</p>
                <img src="../../assets/admin/app-assets/images/icons/Polygon.png" style="margin-left: 10px">
            </button>
            <div class="d-flex align-items-center border-radius-8 bg-white group-kg" style="padding: 10px 20px; margin-right: 20px">
                <input type="text" style="width: 150px; height: 24px" class="border-0 out-line-none" placeholder="Tìm kiếm">
                <label class="m-0"><img src="../../assets/admin/app-assets/images/icons/search.png" style="margin-left: 10px"></label>
            </div>
        </div>
    </div>
    <div class="mg-top-30 border-radius-8 bg-red color-white font-weight-bold d-flex">
        <div class="m-0 w-15 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Thông tin live stream </p>
        </div>
        <div class="m-0 w-15 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Lượt xem ưu đãi follower</p>
            <div class="d-flex align-items-center" style="padding-left: 5px">
                <div class="d-flex flex-column">
                    <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                    <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
                </div>
            </div>
        </div>
        <div class="m-0 w-15 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Số người xem cao nhất</p>
            <div class="d-flex align-items-center" style="padding-left: 5px">
                <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                <div class="d-flex flex-column">
                    <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                    <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
                </div>
            </div>
        </div>
        <div class="m-0 w-15 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Thời gian xem TB</p>
            <div class="d-flex align-items-center" style="padding-left: 5px">
                <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                <div class="d-flex flex-column">
                    <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                    <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
                </div>
            </div>
        </div>
        <div class="m-0 w-15 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Đơn hàng gián tiếp</p>
            <div class="d-flex align-items-center" style="padding-left: 5px">
                <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                <div class="d-flex flex-column">
                    <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                    <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
                </div>
            </div>
        </div>
        <div class="m-0 w-15 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Doanh thu gián tiến</p>
            <div class="d-flex align-items-center" style="padding-left: 5px">
                <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                <div class="d-flex flex-column">
                    <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                    <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
                </div>
            </div>
        </div>
        <div class="m-0 w-10 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Thao tác</p>
        </div>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 font-weight-bold font-size-14" style="padding-left: 15px">
            <span class="color-red d-block">[SFP - 128958]</span>
            <span>Phiếu ưu đãi Grab Express </span>
        </p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1000</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >123</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1:10:10</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1000</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >100.000.000 đ</p>
        <div class="w-10 d-flex text-decoration align-items-center color-blue font-weight-bold">
            <img src="../../assets/admin/app-assets/images/icons/del.png">
            <span>Bỏ qua</span>
        </div>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 font-weight-bold font-size-14" style="padding-left: 15px">
            <span class="color-red d-block">[SFP - 128958]</span>
            <span>Phiếu ưu đãi Grab Express </span>
        </p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1000</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >123</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1:10:10</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1000</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >100.000.000 đ</p>
        <div class="w-10 d-flex text-decoration align-items-center color-blue font-weight-bold">
            <img src="../../assets/admin/app-assets/images/icons/del.png">
            <span>Bỏ qua</span>
        </div>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 font-weight-bold font-size-14" style="padding-left: 15px">
            <span class="color-red d-block">[SFP - 128958]</span>
            <span>Phiếu ưu đãi Grab Express </span>
        </p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1000</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >123</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1:10:10</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1000</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >100.000.000 đ</p>
        <div class="w-10 d-flex text-decoration align-items-center color-blue font-weight-bold">
            <img src="../../assets/admin/app-assets/images/icons/del.png">
            <span>Bỏ qua</span>
        </div>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 font-weight-bold font-size-14" style="padding-left: 15px">
            <span class="color-red d-block">[SFP - 128958]</span>
            <span>Phiếu ưu đãi Grab Express </span>
        </p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1000</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >123</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1:10:10</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1000</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >100.000.000 đ</p>
        <div class="w-10 d-flex text-decoration align-items-center color-blue font-weight-bold">
            <img src="../../assets/admin/app-assets/images/icons/del.png">
            <span>Bỏ qua</span>
        </div>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 font-weight-bold font-size-14" style="padding-left: 15px">
            <span class="color-red d-block">[SFP - 128958]</span>
            <span>Phiếu ưu đãi Grab Express </span>
        </p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1000</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >123</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1:10:10</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1000</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >100.000.000 đ</p>
        <div class="w-10 d-flex text-decoration align-items-center color-blue font-weight-bold">
            <img src="../../assets/admin/app-assets/images/icons/del.png">
            <span>Bỏ qua</span>
        </div>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 font-weight-bold font-size-14" style="padding-left: 15px">
            <span class="color-red d-block">[SFP - 128958]</span>
            <span>Phiếu ưu đãi Grab Express </span>
        </p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1000</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >123</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1:10:10</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >1000</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >100.000.000 đ</p>
        <div class="w-10 d-flex text-decoration align-items-center color-blue font-weight-bold">
            <img src="../../assets/admin/app-assets/images/icons/del.png">
            <span>Bỏ qua</span>
        </div>
    </div>
    <div class="d-flex align-items-center justify-content-lg-center w-100 mg-top-30">
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
            <img src="../../assets/admin/app-assets/images/icons/prev.png">
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            1
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13 active">
            2
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            3
        </button>
        <p class="font-weight-bold font-size-13" style="margin-right: 20px;color: #A2A6B0; margin-bottom: 0">
            ...
        </p>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            15
        </button>

        <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
            <img src="../../assets/admin/app-assets/images/icons/next.png">
        </button>
    </div>
</div>
