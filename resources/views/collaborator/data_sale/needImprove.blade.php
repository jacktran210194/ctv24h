<div class="mg-top-30 p-2 bg-white">
    <ul class="d-flex border_box border-radius-4 p-0 content-overview">
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center " style="width: 140px; height: 40px">
            <a href="{{url('/admin/collaborator/data_sale/products')}}">Tổng quan</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="width: 140px; height: 40px">
            <a href="{{url('/admin/collaborator/data_sale/products/products-overview')}}">Hiệu quả</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center active" style="width: 140px; height: 40px">
            <a href="{{url('/admin/collaborator/data_sale/products/need-improve')}}">Cần cải thiện</a>
        </li>
    </ul>
    <h3 class="font-weight-bold font-size-24 mg-top-30">Sản phẩm cần cải thiện</h3>
    <ul class="tb-content d-flex border_box align-items-center justify-content-lg-start mg-top-30 p-0">
        <li class="d-flex align-items-center justify-content-lg-center border-top-left border-bottom-left content-link active">
            <a
                class="w-100 h-100 text-center font-weight-bold font-size-14 d-flex align-items-center justify-content-lg-center">Doanh thu giảm(10)
            </a>
        </li>
        <li class="d-flex align-items-center justify-content-lg-center content-link">
            <a
                class="w-100 h-100 text-center font-weight-bold font-size-14 d-flex align-items-center justify-content-lg-center">Tỉ lệ đơn hoàn trả cao(10)</a>
        </li>
        <li class="d-flex align-items-center justify-content-lg-center content-link">
            <a
                class="w-100 h-100 text-center font-weight-bold font-size-14 d-flex align-items-center justify-content-lg-center">Tỉ lệ giao đơn trễ cao(10)</a>
        </li>
        <li class="d-flex align-items-center justify-content-lg-center content-link">
            <a data-value="2"
               class="w-100 h-100 text-center font-weight-bold font-size-14 d-flex align-items-center justify-content-lg-center">Lượt xem giảm(10)</a>
        </li>
        <li class="d-flex align-items-center justify-content-lg-center content-link">
            <a
                class="w-100 h-100 text-center font-weight-bold font-size-14 d-flex align-items-center justify-content-lg-center">Tỉ lệ hủy đơn do người bán cao(10)</a>
        </li>
        <li class="d-flex align-items-center justify-content-lg-center content-link">
            <a data-value="3"
               class="w-100 h-100 text-center font-weight-bold font-size-14 d-flex align-items-center justify-content-lg-center">Đánh giá xấu(10)</a>
        </li>
        <li class="d-flex align-items-center justify-content-lg-center border-bottom-right border-bottom-right content-link">
            <a data-value="3"
               class="w-100 h-100 text-center font-weight-bold font-size-14 d-flex align-items-center justify-content-lg-center">Tỉ lệ chuyển đổi thấp(10)</a>
        </li>
    </ul>
    <div class="tips mg-top-30">
        <p class="m-0 font-weight-bold font-size-12">Định nghĩa: Danh sách sản phẩm đã thanh toán trong 7 ngày qua và giảm từ 60% trở lên so với số sản phẩm đã thanh toán 7 ngày trước đó.<br>
            Tips: ① Quảng cáo sản phẩm bằng cách sử dụng Quảng cáo Shopee, tham gia các chương trình khuyến mãi của Shopee hoặc tự tạo chương trình khuyến mãitrong Kênh Marketing; ②Làm cho sản phẩm của bạn hấp dẫn hơn bằng cách thêm nhiều hình ảnh và mô tả chi tiết</p>
    </div>
    <div class="mg-top-30 border-radius-8 bg-red color-white font-weight-bold d-flex" style="padding: 15px 0">
        <p class="m-0 w-40 d-flex align-items-center " style="padding-left: 15px">Chi tiết sản phẩm</p>
        <p class="m-0 w-20 text-left ">Doanh thu(7/6 - 13/6) </p>
        <p class="m-0 w-20 text-left ">Doanh Thu (7/6 - 13/6)</p>
        <p class="m-0 w-20 text-center ">Thao tác</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-40" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-20 d-flex text-left font-weight-bold">100.000.000 đ</p>
        <p class="m-0 w-20 d-flex text-left font-weight-bold">100.000.000 đ</p>
        <div class="d-flex justify-content-lg-center align-items-center w-20">
            <div>
                <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                    <img src="../../assets/admin/app-assets/images/icons/edit.png">
                    <span style="margin-left: 5px">Sửa</span>
                </div>
                <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                    <img src="../../assets/admin/app-assets/images/icons/del.png">
                    <span>Bỏ qua</span>
                </div>
            </div>
        </div>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-40" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-20 d-flex text-left font-weight-bold">100.000.000 đ</p>
        <p class="m-0 w-20 d-flex text-left font-weight-bold">100.000.000 đ</p>
        <div class="d-flex justify-content-lg-center align-items-center w-20">
            <div>
                <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                    <img src="../../assets/admin/app-assets/images/icons/edit.png">
                    <span style="margin-left: 5px">Sửa</span>
                </div>
                <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                    <img src="../../assets/admin/app-assets/images/icons/del.png">
                    <span>Bỏ qua</span>
                </div>
            </div>
        </div>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-40" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-20 d-flex text-left font-weight-bold">100.000.000 đ</p>
        <p class="m-0 w-20 d-flex text-left font-weight-bold">100.000.000 đ</p>
        <div class="d-flex justify-content-lg-center align-items-center w-20">
            <div>
                <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                    <img src="../../assets/admin/app-assets/images/icons/edit.png">
                    <span style="margin-left: 5px">Sửa</span>
                </div>
                <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                    <img src="../../assets/admin/app-assets/images/icons/del.png">
                    <span>Bỏ qua</span>
                </div>
            </div>
        </div>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-40" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-20 d-flex text-left font-weight-bold">100.000.000 đ</p>
        <p class="m-0 w-20 d-flex text-left font-weight-bold">100.000.000 đ</p>
        <div class="d-flex justify-content-lg-center align-items-center w-20">
            <div>
                <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                    <img src="../../assets/admin/app-assets/images/icons/edit.png">
                    <span style="margin-left: 5px">Sửa</span>
                </div>
                <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                    <img src="../../assets/admin/app-assets/images/icons/del.png">
                    <span>Bỏ qua</span>
                </div>
            </div>
        </div>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
        <div class="d-flex align-items-center w-40" style="padding-left: 15px">
            <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
            <div>
                <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
            </div>
        </div>
        <p class="m-0 w-20 d-flex text-left font-weight-bold">100.000.000 đ</p>
        <p class="m-0 w-20 d-flex text-left font-weight-bold">100.000.000 đ</p>
        <div class="d-flex justify-content-lg-center align-items-center w-20">
            <div>
                <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                    <img src="../../assets/admin/app-assets/images/icons/edit.png">
                    <span style="margin-left: 5px">Sửa</span>
                </div>
                <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                    <img src="../../assets/admin/app-assets/images/icons/del.png">
                    <span>Bỏ qua</span>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex align-items-center justify-content-lg-center w-100 mg-top-30">
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
            <img src="../../assets/admin/app-assets/images/icons/prev.png">
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            1
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13 active">
            2
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            3
        </button>
        <p class="font-weight-bold font-size-13" style="margin-right: 20px;color: #A2A6B0; margin-bottom: 0">
            ...
        </p>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            15
        </button>

        <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
            <img src="../../assets/admin/app-assets/images/icons/next.png">
        </button>
    </div>
</div>
