<div class=" mg-top-30">
    <div class="d-flex">
        <div class="w-60 d-flex flex-column justify-content-sm-between" style="margin-right: 15px">
            <div class="p-2 border_box border-0 bg-white">
                <ul class="d-flex border_box border-radius-4 p-0 content-overview">
                    <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center active" style="width: 140px; height: 40px">
                        <a href="{{url('/admin/collaborator/data_sale/products')}}">Tổng quan</a>
                    </li>
                    <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="width: 140px; height: 40px">
                        <a href="{{url('/admin/collaborator/data_sale/products/products-overview')}}">Hiệu quả</a>
                    </li>
                    <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="width: 140px; height: 40px">
                        <a href="{{url('/admin/collaborator/data_sale/products/need-improve')}}">Cần cải thiện</a>
                    </li>
                </ul>
                <h3 class="font-weight-bold font-size-17 color-black mg-top-20">Thêm vào giỏ hàng</h3>
                <div class="mg-top-20 d-flex align-items-center w-100 bg-red font-size-14 font-weight-bold color-white" style="border-radius: 8px 8px 0px 0px;">
                    <p class="m-0 w-50 text-center" style="padding: 5px 0">Sản phẩm</p>
                    <p class="m-0 w-50 text-center" style="padding: 5px 0">Tỉ lệ chuyển đổi (theo số lần được thêm vào giỏ hàng)</p>
                </div>
                <div class="d-flex border-0 border_box">
                    <div class="d-flex justify-content-lg-center align-items-center dt-1 w-50" style="min-height: 175px">
                        <div class="text-center" style="max-width: 170px">
                            <p class="font-weight-bold font-size-17 color-black">0</p>
                            <p class="color-gray font-size-14">so với 00:00-16:00 hôm qua 0.00%</p>
                        </div>
                    </div>
                    <div class="d-flex justify-content-lg-center align-items-center dt-2 w-50" style="min-height: 175px">
                        <div class="text-center" style="max-width: 170px">
                            <p class="font-weight-bold font-size-17 color-black">0.00%
                            </p>
                            <p class="color-gray font-size-14">so với 00:00-16:00 hôm qua 0.00%</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="border_box border-0 bg-white p-2 mg-top-30">
                <h3 class="font-weight-bold font-size-17 color-black">Đơn đã xác nhận</h3>
                <div class="mg-top-20 d-flex align-items-center w-100 bg-red font-size-14 font-weight-bold color-white" style="border-radius: 8px 8px 0px 0px;">
                    <p class="m-0 w-25 text-center" style="padding: 15px 0">Sản phẩm</p>
                    <p class="m-0 w-25 text-center" style="padding: 15px 0">Doanh thu</p>
                    <p class="m-0 w-25 text-center" style="padding: 15px 0">Sản phẩm đã duyệt</p>
                    <p class="m-0 w-25 text-center" style="padding: 15px 0">Tỉ lệ chuyển đổi</p>
                </div>
                <div class="d-flex border-0 border_box">
                    <div class="d-flex justify-content-lg-center align-items-center dt-1 w-25 p-1" style="min-height: 175px">
                        <div class="text-center" style="max-width: 170px">
                            <p class="font-weight-bold font-size-17 color-black">0</p>
                            <p class="color-gray font-size-14">so với 00:00-16:00 hôm qua 0.00%</p>
                        </div>
                    </div>
                    <div class="d-flex justify-content-lg-center align-items-center dt-1 w-25 p-1" style="min-height: 175px">
                        <div class="text-center" style="max-width: 170px">
                            <p class="font-weight-bold font-size-17 color-black">0.00%
                            </p>
                            <p class="color-gray font-size-14">so với 00:00-16:00 hôm qua 0.00%</p>
                        </div>
                    </div>
                    <div class="d-flex justify-content-lg-center align-items-center dt-1 w-25 p-1" style="min-height: 175px">
                        <div class="text-center" style="max-width: 170px">
                            <p class="font-weight-bold font-size-17 color-black">0.00%
                            </p>
                            <p class="color-gray font-size-14">so với 00:00-16:00 hôm qua 0.00%</p>
                        </div>
                    </div>
                    <div class="d-flex justify-content-lg-center align-items-center w-25 p-1" style="min-height: 175px">
                        <div class="text-center" style="max-width: 170px">
                            <p class="font-weight-bold font-size-17 color-black">0.00%
                            </p>
                            <p class="color-gray font-size-14">so với 00:00-16:00 hôm qua 0.00%</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-40 p-2 bg-white border_box border-0">
            <h3 class="font-weight-bold font-size-17 color-black">Lượt truy cập</h3>
            <div class="mg-top-30 d-flex flex-wrap dv-box">
                <div class="border_box border-radius-16 ct-box p-1">
                    <p class="font-size-12 font-weight-bold color-black mg-top-20 text-ct">Số khách truy cập sản phẩm</p>
                    <p class="text-center font-weight-bold font-size-17 mg-top-20">0</p>
                    <div class="d-flex align-items-center">
                        <span class="font-size-12 color-gray" style="max-width: 100px">So với 00:00-16:00 hôm qua 0.00%</span>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-13 color-black">100%</span>
                            <img src="../../assets/admin/app-assets/images/icons/right-arrow.png">
                        </div>
                    </div>
                </div>
                <div class="border_box border-radius-16 ct-box p-1">
                    <p class="font-size-12 font-weight-bold color-black mg-top-20 text-ct">Lượt xem trang sản phẩm</p>
                    <p class="text-center font-weight-bold font-size-17 mg-top-20">0</p>
                    <div class="d-flex align-items-center">
                        <span class="font-size-12 color-gray" style="max-width: 100px">So với 00:00-16:00 hôm qua 0.00%</span>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-13 color-black">100%</span>
                            <img src="../../assets/admin/app-assets/images/icons/right-arrow.png">
                        </div>
                    </div>
                </div>
                <div class="border_box border-radius-16 ct-box p-1">
                    <p class="font-size-12 font-weight-bold color-black mg-top-20 text-ct">Số sản phẩm được xem</p>
                    <p class="text-center font-weight-bold font-size-17 mg-top-20">0</p>
                    <div class="d-flex align-items-center">
                        <span class="font-size-12 color-gray" style="max-width: 100px">So với 00:00-16:00 hôm qua 0.00%</span>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-13 color-black">100%</span>
                            <img src="../../assets/admin/app-assets/images/icons/right-arrow.png">
                        </div>
                    </div>
                </div>
                <div class="border_box border-radius-16 ct-box p-1">
                    <p class="font-size-12 font-weight-bold color-black mg-top-20 text-ct">Tỉ lệ thoát trang sản phẩm</p>
                    <p class="text-center font-weight-bold font-size-17 mg-top-20">0</p>
                    <div class="d-flex align-items-center">
                        <span class="font-size-12 color-gray" style="max-width: 100px">So với 00:00-16:00 hôm qua 0.00%</span>
                        <div class="d-flex align-items-center">
                            <span class="font-weight-bold font-size-13 color-black">100%</span>
                            <img src="../../assets/admin/app-assets/images/icons/right-arrow.png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100">
                <img src="../../assets/admin/app-assets/images/icons/BEARD16.png" style="width: 100%">
            </div>
        </div>
    </div>
    <div class="mg-top-30 bg-white p-2 border-0 border_box">
        <h3 class="font-weight-bold font-size-17 color-black">Xu hướng số liệu</h3>
        <div class="d-flex border_box border-radius-8 mg-top-30">
            <ul class="p-0  border-top-left bg-red border-bottom-left" style="margin-right: 15px; width: 180px">
                <li class="text-right color-white font-weight-bold font-size-12" style="padding:19px">Lượt truy cập:</li>
                <li class="text-right color-white font-weight-bold font-size-12" style="padding:19px">Thêm vào giỏ hàng:</li>
                <li class="text-right color-white font-weight-bold font-size-12" style="padding:19px">Đơn đã xác nhận:</li>
            </ul>
            <ul class="p-0 w-100">
                <li class="d-flex align-items-center">
                    <div class="d-flex align-items-center border-bottom w-100" style="padding:19px">
                        <div class="w-25 d-flex align-items-center">
                            <p class="font-weight-bold font-size-12 color-black m-0 dt-v-text bf-v" style="padding: 0 15px; width: fit-content">Lượt khách truy cập sản phẩm</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                        <div class="w-25 d-flex align-items-center" style="margin-left: 20px">
                            <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Lượt xem trang sản phẩm</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                    </div>
                </li>
                <li class="d-flex align-items-center">
                    <div class="d-flex align-items-center border-bottom w-100" style="padding:19px">
                        <div class="w-25 d-flex align-items-center">
                            <p class="font-weight-bold font-size-12 color-black m-0 dt-v-text bf-v" style="padding: 0 15px; width: fit-content">Số khách truy cập sản phẩm</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                        <div class="w-25 d-flex align-items-center" style="margin-left: 20px">
                            <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Sản phẩm</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                    </div>
                </li>
                <li class="d-flex align-items-center">
                    <div class="d-flex align-items-center border-bottom w-100" style="padding:19px">
                        <div class="w-25 d-flex align-items-center">
                            <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Sản phẩm được duyệt</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                        <div class="w-25 d-flex align-items-center" style="margin-left: 20px">
                            <p class="font-weight-bold font-size-12 color-black m-0 at-t bf-v" style="padding: 0 15px; width: fit-content">Sản phẩm</p>
                            <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png">
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="mg-top-30">
            <img src="../../assets/admin/app-assets/images/icons/graph.png" style="width: 100%">
        </div>
    </div>
    <div class="mg-top-30 bg-white p-2 border-0 border_box">
        <h3 class="font-weight-bold font-size-17 color-black">Thứ hạng sản phẩm (Top 10)</h3>
        <ul class="d-flex mg-top-20 border_box border-radius-8 top-trend">
            <li class="d-flex align-items-center justify-content-lg-center color-black h-100">
                <p class="font-weight-bold font-size-14 m-0">Theo lượt truy cập</p>
                <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png" style="margin-left: 10px">
            </li>
            <li class="d-flex align-items-center justify-content-lg-center color-black h-100 active">
                <p class="font-weight-bold font-size-14 m-0">Theo lượt xem trang</p>
                <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png" style="margin-left: 10px">
            </li>
            <li class="d-flex align-items-center justify-content-lg-center color-black h-100">
                <p class="font-weight-bold font-size-14 m-0">Theo doanh thu
                    (Đơn đã thanh toán)</p>
                <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png" style="margin-left: 10px">
            </li>
            <li class="d-flex align-items-center justify-content-lg-center color-black h-100 ">
                <p class="font-weight-bold font-size-14  m-0">Theo số sản phẩm đã bán
                    (Tất cả đơn)</p>
                <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png" style="margin-left: 10px">
            </li>
            <li class="d-flex align-items-center justify-content-lg-center color-black h-100 ">
                <p class="font-weight-bold font-size-14 m-0">Theo tỉ lệ chuyển đổi
                    (Tất cả đơn)</p>
                <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png" style="margin-left: 10px">
            </li>
            <li class="d-flex align-items-center justify-content-lg-center color-black h-100">
                <p class="font-weight-bold font-size-14  m-0">Theo số sản phẩm đã bán
                    (Thêm vào giỏ hàng)</p>
                <img src="../../assets/admin/app-assets/images/icons/Danger_Circle.png" style="margin-left: 10px">
            </li>
        </ul>
        <div class="mg-top-30 border-radius-8 bg-red color-white font-weight-bold d-flex">
            <p class="m-0 w-25" style="padding: 10px 0 10px 50px;">Thứ</p>
            <p class="m-0 w-25 d-flex align-items-center ">Thông tin sản phẩm</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center">Lượt xem trang</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center">Tỉ lệ</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center">Thay đổi</p>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
            <p class="m-0 w-25 font-weight-bold" style="padding: 10px 0 10px 50px;">1</p>
            <div class="d-flex align-items-center w-25">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                    <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
                </div>
            </div>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">10</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">33.33%</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">-</p>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
            <p class="m-0 w-25 font-weight-bold" style="padding: 10px 0 10px 50px;">2</p>
            <div class="d-flex align-items-center w-25">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                    <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
                </div>
            </div>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">10</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">33.33%</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">-</p>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
            <p class="m-0 w-25 font-weight-bold" style="padding: 10px 0 10px 50px;">3</p>
            <div class="d-flex align-items-center w-25">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                    <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
                </div>
            </div>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">10</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">33.33%</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">-</p>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
            <p class="m-0 w-25 font-weight-bold" style="padding: 10px 0 10px 50px;">4</p>
            <div class="d-flex align-items-center w-25">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                    <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
                </div>
            </div>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">10</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">33.33%</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">-</p>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
            <p class="m-0 w-25 font-weight-bold" style="padding: 10px 0 10px 50px;">5</p>
            <div class="d-flex align-items-center w-25">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                    <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
                </div>
            </div>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">10</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">33.33%</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">-</p>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
            <p class="m-0 w-25 font-weight-bold" style="padding: 10px 0 10px 50px;">6</p>
            <div class="d-flex align-items-center w-25">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                    <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
                </div>
            </div>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">10</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">33.33%</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">-</p>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
            <p class="m-0 w-25 font-weight-bold" style="padding: 10px 0 10px 50px;">7</p>
            <div class="d-flex align-items-center w-25">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                    <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
                </div>
            </div>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">10</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">33.33%</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">-</p>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
            <p class="m-0 w-25 font-weight-bold" style="padding: 10px 0 10px 50px;">8</p>
            <div class="d-flex align-items-center w-25">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                    <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
                </div>
            </div>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">10</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">33.33%</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">-</p>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
            <p class="m-0 w-25 font-weight-bold" style="padding: 10px 0 10px 50px;">9</p>
            <div class="d-flex align-items-center w-25">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                    <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
                </div>
            </div>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">10</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">33.33%</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">-</p>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 5px 0">
            <p class="m-0 w-25 font-weight-bold" style="padding: 10px 0 10px 50px;">10</p>
            <div class="d-flex align-items-center w-25">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <p class="m-0 font-weight-bold color-black">Áo khoác bông dài </p>
                    <p class="m-0 font-size-12 color-gray">Mã sản phẩm: 6841165147 </p>
                </div>
            </div>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">10</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">33.33%</p>
            <p class="m-0 w-25 d-flex align-items-center text-center justify-content-lg-center font-weight-bold">-</p>
        </div>
    </div>
</div>
