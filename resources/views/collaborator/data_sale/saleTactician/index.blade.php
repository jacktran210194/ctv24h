<div class="mg-top-30 bg-white p-2">
    <ul class="d-flex border_box border-radius-4 p-0 content-overview">
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center active" style="padding: 10px 20px">
            <a href="{{url('/admin/collaborator/data_sale/sale-tactician')}}">Sản phẩm theo xu hướng</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="padding: 10px 20px">
            <a href="{{url('/admin/collaborator/data_sale/sale-tactician/competitive-product')}}">Sản phẩm cạnh tranh</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="padding: 10px 20px">
            <a href="{{url('/admin/collaborator/data_sale/sale-tactician/top-search')}}">Top từ khoá</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="padding: 10px 20px">
            <a href="{{url('/admin/collaborator/data_sale/sale-tactician/optimize-product')}}">Tối ưu sản phẩm</a>
        </li>
    </ul>
    <h3 class="mg-top-30 font-size-24 font-weight-bold">Sản phẩm theo xu hướng</h3>
    <p class="font-size-14 color-black">Các sản phẩm bán chạy được tính toán dựa trên các xu hướng tìm kiếm từ 08-06-2021 - 14-06-2021.
    </p>
    <div class="mg-top-30 border-top d-flex" style="padding-top: 30px;">
        <div class="w-30">
            <div class="d-flex align-items-center">
                <p class="m-0 font-size-14">Số người xem cao nhất</p>
                <img src="../../assets/admin/app-assets/images/icons/icon_danger.png" width="20px" height="20px">
            </div>
            <div class="mg-top-20 overflow-auto dv-t-search dv-scroll" style="padding-right: 20px">
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">1</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">2</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">3</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">4</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">5</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">6</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">7</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">8</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">9</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">10</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">11</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">12</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">13</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">14</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">15</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">15</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">15</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">15</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100">
                    <p class="w-10 m-0 font-weight-bold font-size-17">15</p>
                    <div class="w-60">
                        <p class="font-weight-bold font-size-17" style="margin-bottom: 5px"> Nồi nấu cháo </p>
                        <p class="m-0 font-weight-bold font-size-14" style="color: #8C8C8C"> 10 sản phẩm</p>
                    </div>
                    <div class="w-30 d-flex justify-content-lg-end">
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17229.png">
                            <p class="font-weight-bold font-size-14 color-black">83.33%</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-70" style="padding-left: 40px; padding-right: 20px">
            <p class="font-weight-bold font-size-14 m-0">Top 30 sản phẩm liên quan đến "chậu hình chữ nhật"
            </p>
            <div class="d-flex flex-wrap dv-scroll overflow-auto" style="margin-right: -20px; max-height: 100vh">
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
