<div class="mg-top-30 bg-white p-2">
    <ul class="d-flex border_box border-radius-4 p-0 content-overview">
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center " style="padding: 10px 20px">
            <a href="{{url('/admin/collaborator/data_sale/sale-tactician')}}">Sản phẩm theo xu hướng</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center " style="padding: 10px 20px">
            <a href="{{url('/admin/collaborator/data_sale/sale-tactician/competitive-product')}}">Sản phẩm cạnh tranh</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center " style="padding: 10px 20px">
            <a href="{{url('/admin/collaborator/data_sale/sale-tactician/top-search')}}">Top từ khoá</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center active" style="padding: 10px 20px">
            <a href="{{url('/admin/collaborator/data_sale/sale-tactician/optimize-product')}}">Tối ưu sản phẩm</a>
        </li>
    </ul>
    <h3 class="mg-top-30 font-size-24 font-weight-bold">Tối ưu sản phẩm</h3>
    <p class="font-size-14 color-black">Gợi ý các sản phẩm mà Shop có thể cải thiện để tăng tính cạnh tranh.
    </p>
    <div class="mg-top-30 border-top d-flex align-items-center justify-content-sm-between" style="padding-top: 50px; padding-bottom: 50px">
        <div class="w-50 position-relative">
            <ul class="p-0 m-0">
                <li class="font-weight-bold color-white bg-blue font-size-24 border-top-left" style="padding:10px 0px 10px 20px">Có thể cải thiện : <span class="font-size-36">43</span></li>
                <li class="font-weight-bold color-white bg-red font-size-24 border-bottom-left" style="padding:10px 0px 10px 20px">Chất lượng đăng bán tốt: : <span class="font-size-36">43</span></li>
            </ul>
            <div class="position-absolute pie-chart border_box">
                <div class="pieContainer d-flex justify-content-lg-center align-items-center h-100 position-relative">
                    <div id="pieSlice1" class="hold"><div class="pie"></div></div>
                    <div id="pieSlice2" class="hold"><div class="pie"></div></div>
                    <div class="innerCircle"><div class="font-weight-bold color-black text-center">
                            <span class="d-block font-size-36">86</span>
                            <span class="font-size-24">Sản phẩm</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-40">
            <p class="m-0 color-black font-weight-bold font-size-24" style="line-height: 33px;">Nhiều sản phẩm cần được cải thiện</p>
            <p class="m-0 color-black font-size-17" style="line-height: 33px;">Chất lượng đăng bán của nhiều sản phẩm của Shop đang được gợi ý để cải thiện. Chất lượng đăng bán tốt sẽ thu hút người mua và tăng độ cạnh tranh hiển thị. Hãy xem qua một vài ví dụ sản phẩm có chất lượng đăng bán tốt và gợi ý cải thiện bên dưới nhé! </p>
        </div>
    </div>
    <ul class="d-flex border_box border-radius-4 p-0 content-overview mg-top-30 w-100 justify-content-sm-between">
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center active" style="padding: 10px 20px">
            <a href="{{url('/admin/data_sale/sale-tactician/optimize-product')}}">Tên sản phẩm còn ngắn 15</a>
        </li>
        <li class="overview-1 font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="padding: 10px 20px">
            <a href="{{url('/admin/data_sale/sale-tactician/optimize-product/fix-brand-product')}}">Sai ngành hàng <span class="color-red">1</span></a>
        </li>
        <li class="overview-1 font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="padding: 10px 20px">
            <a href="{{url('/admin/data_sale/sale-tactician/optimize-product/fix-desc-product')}}">Mô tả còn ngắn <span class="color-red">1</span></a>
        </li>
        <li class="overview-1 font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="padding: 10px 20px">
            <a href="{{url('/admin/data_sale/sale-tactician/optimize-product/fix-image-product')}}">HÌnh ảnh còn ít <span class="color-red">1</span></a>
        </li>
        <li class="overview-1 font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="padding: 10px 20px">
            <a href="{{url('/admin/data_sale/sale-tactician/optimize-product/fix-variant-product')}}">Thiếu thông tin kích thước <span class="color-red">1</span></a>
        </li>
    </ul>
    <div class="tb-content-product">
        <h3 class="color-black font-weight-bold font-size-24 mg-top-30"><span class="color-red">15</span> sản phẩm nên đặt lại tên rõ hơn để tăng khả năng tìm kiếm</h3>
        <div class="mg-top-30 border-radius-8 bg-red color-white font-weight-bold d-flex">
            <p class="m-0 w-30 d-flex align-items-center position-relative bf-input" style="padding-left: 50px"> Sản phẩm</p>
            <div class="m-0 w-20 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
                <p class="m-0">Lần cuối cập nhật lúc</p>
                <div class="d-flex align-items-center" style="padding-left: 5px">
                    <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                    <div class="d-flex flex-column">
                        <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                        <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
                    </div>
                </div>
            </div>
            <div class="m-0 w-35 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
                <p class="m-0">Độ dài hiện tại/Đề xuất</p>
                <div class="d-flex flex-column" style="padding-left: 5px">
                    <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                    <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
                </div>
            </div>
            <div class="m-0 w-15 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
                <p class="m-0">Thao tác</p>
            </div>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 15px 0">
            <div class="d-flex align-items-center w-30 bf-input position-relative bf-gray" style="padding-left: 50px">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <div class="d-flex align-items-center justify-content-sm-between border-radius-8" style="background: rgba(255, 195, 3, 0.1); padding: 5px 10px; margin-bottom: 8px">
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle_1.png">
                        <p class="m-0 font-size-12" style="color: #FFC303; padding-left: 15px">Những mục cần cải thiện</p>
                    </div>
                    <p class="m-0 font-weight-bold color-black">Giỏ đựng đồ rửa bát</p>
                    <p class="m-0 font-weight-bold color-red">DQG3BE</p>
                </div>
            </div>
            <div class="m-0 w-20 d-flex text-center font-weight-bold justify-content-lg-center align-items-center"><p class="m-0">17:32 <br> 31-05-2021</p></div>
            <div class="m-0 w-35 d-flex align-items-center justify-content-sm-between font-weight-bold">
                <p class="m-0 w-90 d-flex align-items-center border-radius-8 position-relative bf-dv w-6" style="background: #C7C7C7; height: 9px"></p>
                <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">19/30</p>
            </div>
            <div class="d-flex justify-content-lg-center align-items-center w-15">
                <div>
                    <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                        <img src="../../assets/admin/app-assets/images/icons/edit.png">
                        <span style="margin-left: 5px">Sửa</span>
                    </div>
                    <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                        <img src="../../assets/admin/app-assets/images/icons/del.png">
                        <span>Bỏ qua</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 15px 0">
            <div class="d-flex align-items-center w-30 bf-input position-relative bf-gray" style="padding-left: 50px">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <div class="d-flex align-items-center justify-content-sm-between border-radius-8" style="background: rgba(255, 195, 3, 0.1); padding: 5px 10px; margin-bottom: 8px">
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle_1.png">
                        <p class="m-0 font-size-12" style="color: #FFC303; padding-left: 15px">Những mục cần cải thiện</p>
                    </div>
                    <p class="m-0 font-weight-bold color-black">Giỏ đựng đồ rửa bát</p>
                    <p class="m-0 font-weight-bold color-red">DQG3BE</p>
                </div>
            </div>
            <div class="m-0 w-20 d-flex text-center font-weight-bold justify-content-lg-center align-items-center"><p class="m-0">17:32 <br> 31-05-2021</p></div>
            <div class="m-0 w-35 d-flex align-items-center justify-content-sm-between font-weight-bold">
                <p class="m-0 w-90 d-flex align-items-center border-radius-8 position-relative bf-dv w-6" style="background: #C7C7C7; height: 9px"></p>
                <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">19/30</p>
            </div>
            <div class="d-flex justify-content-lg-center align-items-center w-15">
                <div>
                    <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                        <img src="../../assets/admin/app-assets/images/icons/edit.png">
                        <span style="margin-left: 5px">Sửa</span>
                    </div>
                    <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                        <img src="../../assets/admin/app-assets/images/icons/del.png">
                        <span>Bỏ qua</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 15px 0">
            <div class="d-flex align-items-center w-30 bf-input position-relative bf-gray" style="padding-left: 50px">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <div class="d-flex align-items-center justify-content-sm-between border-radius-8" style="background: rgba(255, 195, 3, 0.1); padding: 5px 10px; margin-bottom: 8px">
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle_1.png">
                        <p class="m-0 font-size-12" style="color: #FFC303; padding-left: 15px">Những mục cần cải thiện</p>
                    </div>
                    <p class="m-0 font-weight-bold color-black">Giỏ đựng đồ rửa bát</p>
                    <p class="m-0 font-weight-bold color-red">DQG3BE</p>
                </div>
            </div>
            <div class="m-0 w-20 d-flex text-center font-weight-bold justify-content-lg-center align-items-center"><p class="m-0">17:32 <br> 31-05-2021</p></div>
            <div class="m-0 w-35 d-flex align-items-center justify-content-sm-between font-weight-bold">
                <p class="m-0 w-90 d-flex align-items-center border-radius-8 position-relative bf-dv w-6" style="background: #C7C7C7; height: 9px"></p>
                <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">19/30</p>
            </div>
            <div class="d-flex justify-content-lg-center align-items-center w-15">
                <div>
                    <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                        <img src="../../assets/admin/app-assets/images/icons/edit.png">
                        <span style="margin-left: 5px">Sửa</span>
                    </div>
                    <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                        <img src="../../assets/admin/app-assets/images/icons/del.png">
                        <span>Bỏ qua</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 15px 0">
            <div class="d-flex align-items-center w-30 bf-input position-relative bf-gray" style="padding-left: 50px">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <div class="d-flex align-items-center justify-content-sm-between border-radius-8" style="background: rgba(255, 195, 3, 0.1); padding: 5px 10px; margin-bottom: 8px">
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle_1.png">
                        <p class="m-0 font-size-12" style="color: #FFC303; padding-left: 15px">Những mục cần cải thiện</p>
                    </div>
                    <p class="m-0 font-weight-bold color-black">Giỏ đựng đồ rửa bát</p>
                    <p class="m-0 font-weight-bold color-red">DQG3BE</p>
                </div>
            </div>
            <div class="m-0 w-20 d-flex text-center font-weight-bold justify-content-lg-center align-items-center"><p class="m-0">17:32 <br> 31-05-2021</p></div>
            <div class="m-0 w-35 d-flex align-items-center justify-content-sm-between font-weight-bold">
                <p class="m-0 w-90 d-flex align-items-center border-radius-8 position-relative bf-dv w-6" style="background: #C7C7C7; height: 9px"></p>
                <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">19/30</p>
            </div>
            <div class="d-flex justify-content-lg-center align-items-center w-15">
                <div>
                    <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                        <img src="../../assets/admin/app-assets/images/icons/edit.png">
                        <span style="margin-left: 5px">Sửa</span>
                    </div>
                    <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                        <img src="../../assets/admin/app-assets/images/icons/del.png">
                        <span>Bỏ qua</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 15px 0">
            <div class="d-flex align-items-center w-30 bf-input position-relative bf-gray" style="padding-left: 50px">
                <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 10px; max-width: 70px" class="border-radius-8">
                <div>
                    <div class="d-flex align-items-center justify-content-sm-between border-radius-8" style="background: rgba(255, 195, 3, 0.1); padding: 5px 10px; margin-bottom: 8px">
                        <img src="../../assets/admin/app-assets/images/icons/Danger_Circle_1.png">
                        <p class="m-0 font-size-12" style="color: #FFC303; padding-left: 15px">Những mục cần cải thiện</p>
                    </div>
                    <p class="m-0 font-weight-bold color-black">Giỏ đựng đồ rửa bát</p>
                    <p class="m-0 font-weight-bold color-red">DQG3BE</p>
                </div>
            </div>
            <div class="m-0 w-20 d-flex text-center font-weight-bold justify-content-lg-center align-items-center"><p class="m-0">17:32 <br> 31-05-2021</p></div>
            <div class="m-0 w-35 d-flex align-items-center justify-content-sm-between font-weight-bold">
                <p class="m-0 w-90 d-flex align-items-center border-radius-8 position-relative bf-dv w-6" style="background: #C7C7C7; height: 9px"></p>
                <p class="m-0 w-10 d-flex align-items-center font-weight-bold color-black text-center" style="padding-left: 20px">19/30</p>
            </div>
            <div class="d-flex justify-content-lg-center align-items-center w-15">
                <div>
                    <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                        <img src="../../assets/admin/app-assets/images/icons/edit.png">
                        <span style="margin-left: 5px">Sửa</span>
                    </div>
                    <div class="d-flex text-decoration align-items-center color-blue font-weight-bold">
                        <img src="../../assets/admin/app-assets/images/icons/del.png">
                        <span>Bỏ qua</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-lg-center w-100 mg-top-30">
            <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
                <img src="../../assets/admin/app-assets/images/icons/prev.png">
            </button>
            <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
                1
            </button>
            <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13 active">
                2
            </button>
            <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
                3
            </button>
            <p class="font-weight-bold font-size-13" style="margin-right: 20px;color: #A2A6B0; margin-bottom: 0">
                ...
            </p>
            <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
                15
            </button>

            <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
                <img src="../../assets/admin/app-assets/images/icons/next.png">
            </button>
        </div>
    </div>
</div>
