<div class="mg-top-30 bg-white p-2">
    <ul class="d-flex border_box border-radius-4 p-0 content-overview">
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center " style="padding: 10px 20px">
            <a href="{{url('/admin/collaborator/data_sale/sale-tactician')}}">Sản phẩm theo xu hướng</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center active" style="padding: 10px 20px">
            <a href="{{url('/admin/collaborator/data_sale/sale-tactician/competitive-product')}}">Sản phẩm cạnh tranh</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="padding: 10px 20px">
            <a href="{{url('/admin/collaborator/data_sale/sale-tactician/top-search')}}">Top từ khoá</a>
        </li>
        <li class="overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="padding: 10px 20px">
            <a href="{{url('/admin/collaborator/data_sale/sale-tactician/optimize-product')}}">Tối ưu sản phẩm</a>
        </li>
    </ul>
    <h3 class="mg-top-30 font-size-24 font-weight-bold">Sản phẩm cạnh tranh</h3>
    <p class="font-size-14 color-black">Sản phẩm trong Shopee tương tự như các sản phẩm trong Shop bạn. Được tính toán dựa trên hình ảnh, giá cả và các yếu tố khác.
    </p>
    <div class="mg-top-30 border-top d-flex" style="padding-top: 30px;">
        <div class="w-30">
            <div class="d-flex align-items-center justify-content-sm-between">
                <p class="m-0 font-size-14 font-weight-bold color-black">Sản phẩm của </p>
                <div class="d-flex align-items-center">
                    <p class="m-0 font-size-14 font-weight-bold color-black" style="padding-right: 15px">Theo số sản phẩm đã bán</p>
                    <img src="../../assets/admin/app-assets/images/icons/Polygon.png">
                </div>
            </div>
            <button class="w-100 border border-radius-8 bg-white mg-top-30 d-flex justify-content-sm-between align-items-center" style="padding: 10px 20px">
                <p class="m-0 font-weight-bold font-size-14 color-gray">Thiết bị điện gia dụng/ thiết bị điệ...</p>
                <img src="../../assets/admin/app-assets/images/icons/Polygon.png">
            </button>
            <button class="w-100 border border-radius-8 bg-white mg-top-30 d-flex justify-content-sm-between align-items-center" style="padding: 10px 20px">
                <input class="w-80 border-0 w-100 h-100 color-gray font-weight-bold font-size-14 out-line-none" placeholder="Tìm sản phẩm">
                <img src="../../assets/admin/app-assets/images/icons/search.png">
            </button>
            <div class="mg-top-20">
                <div class=" mg-top-10 d-flex align-items-center w-100 border-red border_box border-radius-8 p-1">
                    <p class="w-10 m-0 font-weight-bold font-size-17 color-red">1</p>
                    <div class="w-90 d-flex justify-content-sm-between align-items-center">
                        <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 24px; max-width: 70px" class="border-radius-8">
                        <div>
                            <p class="font-size-14 color-black" style="margin-bottom: 5px"><span class="font-weight-bold">[ CHÍNH HÃNG ]</span>Bàn là hơi nước SUNH...</p>
                            <p class="m-0 font-size-14 color-red">390K</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100 p-1">
                    <p class="w-10 m-0 font-weight-bold font-size-17 color-red">2</p>
                    <div class="w-90 d-flex justify-content-sm-between align-items-center">
                        <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 24px; max-width: 70px" class="border-radius-8">
                        <div>
                            <p class="font-size-14 color-black" style="margin-bottom: 5px"><span class="font-weight-bold">[ CHÍNH HÃNG ]</span>Bàn là hơi nước SUNH...</p>
                            <p class="m-0 font-size-14 color-red">390K</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100 p-1">
                    <p class="w-10 m-0 font-weight-bold font-size-17 color-red">3</p>
                    <div class="w-90 d-flex justify-content-sm-between align-items-center">
                        <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 24px; max-width: 70px" class="border-radius-8">
                        <div>
                            <p class="font-size-14 color-black" style="margin-bottom: 5px"><span class="font-weight-bold">[ CHÍNH HÃNG ]</span>Bàn là hơi nước SUNH...</p>
                            <p class="m-0 font-size-14 color-red">390K</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100 p-1">
                    <p class="w-10 m-0 font-weight-bold font-size-17 color-red">4</p>
                    <div class="w-90 d-flex justify-content-sm-between align-items-center">
                        <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 24px; max-width: 70px" class="border-radius-8">
                        <div>
                            <p class="font-size-14 color-black" style="margin-bottom: 5px"><span class="font-weight-bold">[ CHÍNH HÃNG ]</span>Bàn là hơi nước SUNH...</p>
                            <p class="m-0 font-size-14 color-red">390K</p>
                        </div>
                    </div>
                </div>
                <div class=" mg-top-10 d-flex align-items-center w-100 p-1">
                    <p class="w-10 m-0 font-weight-bold font-size-17 color-red">5</p>
                    <div class="w-90 d-flex justify-content-sm-between align-items-center">
                        <img src="../../assets/admin/app-assets/images/icons/Rectangle.png" style="margin-right: 24px; max-width: 70px" class="border-radius-8">
                        <div>
                            <p class="font-size-14 color-black" style="margin-bottom: 5px"><span class="font-weight-bold">[ CHÍNH HÃNG ]</span>Bàn là hơi nước SUNH...</p>
                            <p class="m-0 font-size-14 color-red">390K</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-70" style="padding-left: 40px; padding-right: 20px">
            <p class="font-weight-bold font-size-14 m-0">Top 30 sản phẩm liên quan đến "chậu hình chữ nhật" </p>
            <div class="d-flex flex-wrap dv-scroll overflow-auto" style="margin-right: -20px; max-height: 100vh">
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
                <div class="mg-top-20 border_box border-0 bg-white" style="width: calc(25% - 20px); margin-right: 20px; padding-bottom: 16px">
                    <div class="d-flex w-100">
                        <div class="w-100 position-relative" style="padding-top: 100%">
                            <img src="../../assets/admin/app-assets/images/icons/Rectangle_6324.png" class="position-absolute w-100" style="top: 0; left: 0">
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <h2 class="mg-top-10 font-weight-bold font-size-14 text-overflow" style="-webkit-line-clamp: 2;">Quần jean ống rộng Kèm Hình Thật...</h2>
                        <p class="font-weight-bold font-size-12 color-red m-0">270.000đ - 370.000đ</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
