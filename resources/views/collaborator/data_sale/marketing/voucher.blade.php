<div class="mg-top-30 p-2 bg-white">
    <ul class="d-flex border_box border-radius-4 p-0 content-overview">
        <li class="text-center overview font-weight-bold font-size-14 d-flex align-items-center justify-content-lg-center " style="height: 40px; padding: 0 20px">
            <a href="{{url('/admin/collaborator/data_sale/marketing')}}">Chương trình khuyến mãi</a>
        </li>
        <li class="text-center overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="height: 40px; padding: 0 20px">
            <a href="{{url('/admin/collaborator/data_sale/marketing/combo-sale')}}">Combo khuyến mãi</a>
        </li>
        <li class="text-center overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="height: 40px; padding: 0 20px">
            <a href="{{url('/admin/collaborator/data_sale/marketing/follower-offers')}}"> Ưu đãi follower</a>
        </li>
        <li class="text-center overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center active" style="height: 40px; padding: 0 20px">
            <a href="{{url('/admin/collaborator/data_sale/marketing/voucher')}}">Voucher</a>
        </li>
        <li class="text-center overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="height: 40px; padding: 0 20px">
            <a href="{{url('/admin/collaborator/data_sale/marketing/flash-sale')}}">Flash Sale của Shop</a>
        </li>
        <li class="text-center overview font-weight-bold font-size-14 color-black d-flex align-items-center justify-content-lg-center" style="height: 40px; padding: 0 20px">
            <a href="{{url('admin/collaborator/data_sale/marketing/deal-shock')}}">Mua kèm deal sốc</a>
        </li>
    </ul>
    <h3 class="font-weight-bold font-size-17 color-black mg-top-30">Chỉ số quan trọng</h3>
    <div class="d-flex justify-content-sm-between mg-top-30" style="padding: 0 2px;">
        <div class="border-radius-16 bg-1 box-data dv-ct dv-statistical">
            <div class="statistical w-100 p-2">
                <div class="d-flex title-box">
                    <h3 class="font-weight-bold font-size-17 color-black" style="margin-right: 10px">Đã sử dụng</h3>
                    <img src="../../assets/admin/app-assets/images/icons/icon_danger.png" width="20px" height="20px">
                </div>
                <div>
                    <p class=" font-weight-bold font-size-24 color-black mg-top-20">200.000đ</p>
                    <div class="d-flex justify-content-sm-between align-items-center mg-top-20">
                        <p class="font-size-12 color-black" style="margin: 0;">Tăng trưởng so với hôm qua</p>
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17228.png">
                            <p class="font-weight-bold font-size-14 color-black" style="margin: 0;">83.33%</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-radius-16 bg-2 box-data dv-ct dv-statistical">
            <div class="statistical w-100 p-2">
                <div class="d-flex title-box">
                    <h3 class="font-weight-bold font-size-17 color-black" style="margin-right: 10px">Người mua</h3>
                    <img src="../../assets/admin/app-assets/images/icons/icon_danger.png" width="20px" height="20px">
                </div>
                <div>
                    <p class=" font-weight-bold font-size-24 color-black mg-top-20">2</p>
                    <div class="d-flex justify-content-sm-between align-items-center mg-top-20">
                        <p class="font-size-12 color-black" style="margin: 0;">Tăng trưởng so với hôm qua</p>
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/Frame_17228.png" style="max-width: 25px;margin-bottom: 5px">
                            <p class="font-weight-bold font-size-14 color-black" style="margin: 0;">83.33%</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-radius-16 bg-3 box-data dv-ct dv-statistical">
            <div class="w-100 p-2">
                <div class="d-flex title-box">
                    <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Đã bán</h3>
                    <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                </div>
                <div>
                    <p class=" font-weight-bold font-size-24 color-white mg-top-20">2</p>
                    <div class="d-flex justify-content-sm-between align-items-center mg-top-20">
                        <p class="font-size-12 color-white" style="margin: 0;">Tăng trưởng so với hôm qua</p>
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/trendin-up.png" style="max-width: 25px;margin-bottom: 5px">
                            <p class="font-weight-bold font-size-14 color-white" style="margin: 0;">83.33%</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-radius-16 bg-4 box-data dv-ct dv-statistical">
            <div class="w-100 p-2">
                <div class="d-flex title-box">
                    <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Doanh thu</h3>
                    <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                </div>
                <div>
                    <p class=" font-weight-bold font-size-24 color-white mg-top-20">20.000<span class="font-size-12">đ</span></p>
                    <div class="d-flex justify-content-sm-between align-items-center mg-top-20">
                        <p class="font-size-12 color-white" style="margin: 0;">Tăng trưởng so với hôm qua</p>
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/trendin-up.png" style="max-width: 25px;margin-bottom: 5px">
                            <p class="font-weight-bold font-size-14 color-white" style="margin: 0;">83.33%</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-radius-16 bg-5 box-data dv-ct dv-statistical">
            <div class="w-100 p-2">
                <div class="d-flex title-box">
                    <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Doanh thu/ Khách</h3>
                    <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                </div>
                <div>
                    <p class=" font-weight-bold font-size-24 color-white mg-top-20">20.000<span class="font-size-12">đ</span></p>
                    <div class="d-flex justify-content-sm-between align-items-center mg-top-20">
                        <p class="font-size-12 color-white" style="margin: 0;">Tăng trưởng so với hôm qua</p>
                        <div>
                            <img src="../../assets/admin/app-assets/images/icons/trendin-up.png" style="max-width: 25px;margin-bottom: 5px">
                            <p class="font-weight-bold font-size-14 color-white" style="margin: 0;">83.33%</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mg-top-30">
        <img src="../../assets/admin/app-assets/images/icons/graph.png" style="width: 100%">
    </div>
</div>
<div class="mg-top-30 bg-white p-2">
    <h3 class="font-weight-bold font-size-17 color-black">Hiệu quả</h3>
    <div class="d-flex justify-content-sm-between mg-top-30">
        <div class="d-flex align-items-center">
            <span class="font-size-17 color-black font-weight-bold" style="margin-right: 20px">Lọc: </span>
            <button class="d-flex align-items-center border-radius-8 font-size-14 group-kg" style="padding: 10px 20px; margin-right: 20px">
                <p class="m-0">Trạng thái: <span class="font-weight-bold">Tất cả</span></p>
            </button>
            <button class="d-flex align-items-center border-radius-8 font-size-14 group-kg" style="padding: 10px 20px; margin-right: 20px">
                <p class="m-0">Người tạo: <span class="font-weight-bold">Tất cả</span></p>
            </button>
            <button class="d-flex align-items-center border-radius-8 font-size-14 group-kg" style="padding: 10px 20px; margin-right: 20px">
                <p class="m-0">Chương trình: <span class="font-weight-bold">Tất cả</span></p>
            </button>
            <button class="d-flex align-items-center border-radius-8 font-size-14 group-kg" style="padding: 10px 20px; margin-right: 20px">
                <p class="m-0">Ưu đãi: <span class="font-weight-bold">Tất cả</span></p>
            </button>

        </div>
        <button class="d-flex align-items-center border-radius-8 bg-white border-red" style="padding: 10px 20px; margin-right: 20px">
            <p class="m-0 color-red font-weight-bold">Thiết lập</p>
            <img src="../../assets/admin/app-assets/images/icons/Polygon.png" style="margin-left: 10px">
        </button>
    </div>
    <div class="mg-top-30 border-radius-8 bg-red color-white font-weight-bold d-flex">
        <p class="m-0 w-15 d-flex align-items-center " style="padding-left: 15px">Mã giảm giá</p>
        <div class="m-0 w-15 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Từ</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="m-0 w-15 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Đến</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="m-0 w-10 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Đã sử dụng</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="m-0 w-10 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Số lượng sản phẩm đã bán</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="m-0 w-10 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Người mua</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="m-0 w-10 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Doanh thu</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
        <div class="m-0 w-15 d-flex align-items-center text-center justify-content-lg-center" style="padding: 0 10px">
            <p class="m-0">Doanh thu trên mỗi người mua</p>
            <div class="d-flex flex-column" style="padding-left: 5px">
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_up.png"></button>
                <button class="bg-transparent p-0 border-0"> <img src="../../assets/admin/app-assets/images/icons/Polygon_down.png"></button>
            </div>
        </div>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 d-flex align-items-center color-red font-weight-bold font-size-14" style="padding-left: 15px">BCbsdvbk</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >20/2/2020</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >30/12/2021</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >100</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >200</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" style="padding-right: 10px" >100.000 VNĐ</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 d-flex align-items-center color-red font-weight-bold font-size-14" style="padding-left: 15px">BCbsdvbk</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >20/2/2020</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >30/12/2021</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >100</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >200</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" style="padding-right: 10px" >100.000 VNĐ</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 d-flex align-items-center color-red font-weight-bold font-size-14" style="padding-left: 15px">BCbsdvbk</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >20/2/2020</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >30/12/2021</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >100</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >200</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" style="padding-right: 10px" >100.000 VNĐ</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 d-flex align-items-center color-red font-weight-bold font-size-14" style="padding-left: 15px">BCbsdvbk</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >20/2/2020</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >30/12/2021</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >100</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >200</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" style="padding-right: 10px" >100.000 VNĐ</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 d-flex align-items-center color-red font-weight-bold font-size-14" style="padding-left: 15px">BCbsdvbk</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >20/2/2020</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >30/12/2021</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >100</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >200</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" style="padding-right: 10px" >100.000 VNĐ</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 d-flex align-items-center color-red font-weight-bold font-size-14" style="padding-left: 15px">BCbsdvbk</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >20/2/2020</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >30/12/2021</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >100</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >200</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" style="padding-right: 10px" >100.000 VNĐ</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 d-flex align-items-center color-red font-weight-bold font-size-14" style="padding-left: 15px">BCbsdvbk</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >20/2/2020</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >30/12/2021</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >100</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >200</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" style="padding-right: 10px" >100.000 VNĐ</p>
    </div>
    <div class="border mg-top-30 d-flex align-items-center border-radius-8" style="padding: 25px 0">
        <p class="m-0 w-15 d-flex align-items-center color-red font-weight-bold font-size-14" style="padding-left: 15px">BCbsdvbk</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >20/2/2020</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" >30/12/2021</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >100</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >10</p>
        <p class="m-0 w-10 font-size-14 text-center font-weight-bold" >200</p>
        <p class="m-0 w-15 font-size-14 text-center font-weight-bold" style="padding-right: 10px" >100.000 VNĐ</p>
    </div>
    <div class="d-flex align-items-center justify-content-lg-center w-100 mg-top-30">
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
            <img src="../../assets/admin/app-assets/images/icons/prev.png">
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            1
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13 active">
            2
        </button>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            3
        </button>
        <p class="font-weight-bold font-size-13" style="margin-right: 20px;color: #A2A6B0; margin-bottom: 0">
            ...
        </p>
        <button class="d-flex align-items-center justify-content-lg-center btn-pagination font-weight-bold font-size-13">
            15
        </button>

        <button class="d-flex align-items-center justify-content-lg-center btn-pagination">
            <img src="../../assets/admin/app-assets/images/icons/next.png">
        </button>
    </div>
</div>
