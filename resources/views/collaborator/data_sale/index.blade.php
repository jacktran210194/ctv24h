<?php
$page='collaborator/data_sale';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="border_box border-radius-8 bg-white">
            <div class="tab-bar-page">
                <ul class="tab-bar-content border-bottom d-flex align-items-center justify-content-lg-start flex-wrap">
                    <li class="d-flex align-items-center justify-content-lg-center active">
                        <a href="{{url('/admin/collaborator/data_sale')}}"
                           class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Tổng
                            quan</a>
                    </li>
                    <li class="d-flex align-items-center justify-content-lg-center content-link">
                        <a href="{{url('/admin/collaborator/data_sale/access-times')}}"
                           class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Lượt truy
                            cập</a>
                    </li>
                    <li class="d-flex align-items-center justify-content-lg-center content-link">
                        <a href="{{url('/admin/collaborator/data_sale/products')}}"
                           class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Sản
                            phẩm </a>
                    </li>
                    <li class="d-flex align-items-center justify-content-lg-center content-link">
                        <a href="{{url('/admin/collaborator/data_sale/revenue')}}"
                           class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Doanh
                            thu</a>
                    </li>
                    <li class="d-flex align-items-center justify-content-lg-center content-link">
                        <a href="{{url('/admin/collaborator/data_sale/marketing')}}"
                           class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Marketing</a>
                    </li>
                    <li class="d-flex align-items-center justify-content-lg-center content-link">
                        <a href="{{url('/admin/collaborator/data_sale/chat')}}"
                           class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Chat</a>
                    </li>
                    <li class="d-flex align-items-center justify-content-lg-center content-link">
                        <a href="{{url('/admin/collaborator/data_sale/CTV-live/feed')}}"
                           class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">CTV
                            Live/feed</a>
                    </li>
                    <li class="d-flex align-items-center justify-content-lg-center content-link">
                        <a href="{{url('/admin/collaborator/data_sale/sale-tactician')}}"
                           class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Quân sư
                            bán hàng</a>
                    </li>
                </ul>
            </div>
            <div class="d-flex align-items-center justify-content-sm-between bottom-box">
                <div class="d-flex align-items-center">
                    <p class="font-weight-bold font-size-14" style="margin: 0;margin-right: 10px">Loại đơn hàng</p>
                    <p class="button-confirm font-size-14 color-gray group-kg m-0 border-radius-4 d-flex align-items-center justify-content-lg-center"
                       style="margin-left: 5px">Đã xác nhận</p>
                </div>
                <div class="d-flex align-items-center">
                    <p class="m-0 font-size-14 color-gray">Thời gian báo cáo</p>
                    <button class="border_box group-kg border-radius-4 d-flex align-items-center btn-calendar bg-white">
                        <img src="../../assets/admin/app-assets/images/icons/calendar.png">
                        <span class="font-weight-bold font-size-14 calendar" style="margin-left: 5px">30/08/2020 - 02/09/2020</span>
                    </button>
                    <button
                        class="btn-upload-file d-flex group-kg bg-white align-items-center justify-content-lg-center font-weight-bold font-size-14 border-radius-4">
                        <span style="margin-right: 5px">Tải dữ liệu</span>
                        <img src="../../assets/admin/app-assets/images/icons/Arrow-Right.png">
                    </button>
                </div>
            </div>
        </div>
        <div class="container-body">
            <div class="d-flex mg-top-30 justify-content-sm-between">
                <div class="w-75" style="margin-right: 10px">
                    <div class="p-2 position-relative bg-white border_box border-radius-6" style="min-height: 450px">
                        <h3 class="font-weight-bold font-size-24">Biểu đồ</h3>
                        <div class="position-absolute data-chart">
                            <img src="../../assets/admin/app-assets/images/icons/no-data.png">
                        </div>
                    </div>
                    <div class="mg-top-30 bg-white border_box border-radius-6">
                        <h3 class="font-weight-bold font-size-24 border-bottom p-2">Thứ hạng sản phẩm</h3>
                        <div class="mg-top-30 p-2 d-flex justify-content-sm-between">
                            <div class="d-flex table-efficiency">
                                <ul class="border_box border-radius-8 p-1 w-20">
                                    <li class="font-weight-bold font-size-14 text-overflow">Theo doanh thu</li>
                                    <li class="font-weight-bold font-size-14 text-overflow active">Theo số sản phẩm đã
                                        bán
                                    </li>
                                    <li class="font-weight-bold font-size-14 text-overflow">Theo lượt xem</li>
                                    <li class="font-weight-bold font-size-14 text-overflow">Theo tỷ lệ chuyển đổi</li>
                                </ul>
                            </div>
                            <div class="border_box border_radius w-70 position-relative" style="min-height: 230px">
                                <ul class="border_box border-radius-top d-flex p-1 w-100 bg-red">
                                    <li class="font-weight-bold font-size-14 color-white w-20 text-center">Thứ tự</li>
                                    <li class="font-weight-bold font-size-14 color-white w-40 text-center">Thông tin sản
                                        phẩm
                                    </li>
                                    <li class="font-weight-bold font-size-14 color-white w-40 text-center">Theo doanh
                                        thu
                                    </li>
                                </ul>
                                <div class="position-absolute data-chart">
                                    <img src="../../assets/admin/app-assets/images/icons/no-data.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-25 bg-white p-1 border_box border-radius-6" style="padding-top: 2rem">
                    <h3 class="font-weight-bold font-size-24 text-center">Dữ Liệu Bán Hàng</h3>
                    <div class="mg-top-30 border-radius-16 bg-1 box-data">
                        <div class="d-flex title-box">
                            <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Doanh
                                thu</h3>
                            <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                        </div>
                        <div style="margin-top: 20px" class="d-flex justify-content-sm-between align-items-lg-end">
                            <div>
                                <p class="m-0 font-weight-bold f-20 color-white">0đ</p>
                                <p class="f-12 color-white" style="margin: 0; margin-top: 20px">Tăng trưởng so với hôm
                                    qua</p>
                            </div>
                            <div>
                                <img src="../../assets/admin/app-assets/images/icons/trendin-up.png">
                                <p class="font-weight-bold font-size-14 color-white"
                                   style="margin: 0; margin-top: 20px">0%</p>
                            </div>
                        </div>
                    </div>
                    <div class="mg-top-30 border-radius-16 bg-2 box-data">
                        <div class="d-flex title-box">
                            <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Đơn
                                hàng</h3>
                            <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                        </div>
                        <div style="margin-top: 20px" class="d-flex justify-content-sm-between align-items-lg-end">
                            <div>
                                <p class="m-0 font-weight-bold font-size-36 color-white">0</p>
                                <p class="font-size-12 color-white" style="margin: 0; margin-top: 20px">Tăng trưởng so
                                    với hôm qua</p>
                            </div>
                            <div>
                                <img src="../../assets/admin/app-assets/images/icons/trendin-up.png">
                                <p class="font-weight-bold font-size-14 color-white"
                                   style="margin: 0; margin-top: 20px">150
                                    %</p>
                            </div>
                        </div>
                    </div>
                    <div class="mg-top-30 border-radius-16 bg-3 box-data">
                        <div class="d-flex title-box">
                            <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Tỉ lệ
                                chuyển đổi</h3>
                            <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                        </div>
                        <div style="margin-top: 20px" class="d-flex justify-content-sm-between align-items-lg-end">
                            <div>
                                <p class="m-0 font-weight-bold font-size-36 color-white">10
                                    %</p>
                                <p class="font-size-12 color-white" style="margin: 0; margin-top: 20px">Tăng trưởng so
                                    với hôm qua</p>
                            </div>
                            <div>
                                <img src="../../assets/admin/app-assets/images/icons/trendin-up.png">
                                <p class="font-weight-bold font-size-14 color-white"
                                   style="margin: 0; margin-top: 20px">10
                                    %</p>
                            </div>
                        </div>
                    </div>
                    <div class="mg-top-30 border-radius-16 bg-4 box-data">
                        <div class="d-flex title-box">
                            <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Doanh
                                thu/đơn</h3>
                            <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                        </div>
                        <div style="margin-top: 20px" class="d-flex justify-content-sm-between align-items-lg-end">
                            <div>
                                <p class="m-0 font-weight-bold f-20 color-white">20000
                                    đ</p>
                                <p class="font-size-12 color-white" style="margin: 0; margin-top: 20px">Tăng trưởng so
                                    với hôm qua</p>
                            </div>
                            <div>
                                <img src="../../assets/admin/app-assets/images/icons/trendin-up.png">
                                <p class="font-weight-bold font-size-14 color-white"
                                   style="margin: 0; margin-top: 20px">150
                                    %</p>
                            </div>
                        </div>
                    </div>
                    <div class="mg-top-30 border-radius-16 bg-5 box-data">
                        <div class="d-flex title-box">
                            <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Lượt truy
                                cập </h3>
                            <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                        </div>
                        <div style="margin-top: 20px" class="d-flex justify-content-sm-between align-items-lg-end">
                            <div>
                                <p class="m-0 font-weight-bold font-size-36 color-white">10</p>
                                <p class="font-size-12 color-white" style="margin: 0; margin-top: 20px">Tăng trưởng so
                                    với hôm qua</p>
                            </div>
                            <div>
                                <img src="../../assets/admin/app-assets/images/icons/trendin-up.png">
                                <p class="font-weight-bold font-size-14 color-white"
                                   style="margin: 0; margin-top: 20px">0 %</p>
                            </div>
                        </div>
                    </div>
                    <div class="mg-top-30 border-radius-16 bg-6 box-data">
                        <div class="d-flex title-box">
                            <h3 class="font-weight-bold font-size-17 color-white" style="margin-right: 10px">Lượt
                                xem</h3>
                            <img src="../../assets/admin/app-assets/images/icons/Iconly.png" width="20px" height="20px">
                        </div>
                        <div style="margin-top: 20px" class="d-flex justify-content-sm-between align-items-lg-end">
                            <div>
                                <p class="m-0 font-weight-bold font-size-36 color-white">211</p>
                                <p class="font-size-12 color-white" style="margin: 0; margin-top: 20px">Tăng trưởng so
                                    với hôm qua</p>
                            </div>
                            <div>
                                <img src="../../assets/admin/app-assets/images/icons/trendin-up.png">
                                <p class="font-weight-bold font-size-14 color-white"
                                   style="margin: 0; margin-top: 20px">12.00 %</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  Chat -->
        <div class="chat-footer d-flex justify-content-lg-end w-100">
            <button class="border-radius-8 out-line-none bg-red font-weight-bold color-white d-flex align-items-center"
                    style="margin-right: 20px">
                <img src="../../assets/admin/app-assets/images/icons/live_chat.png">
                <a class="color-white" style="margin-left: 10px">Chat với CTV24</a>
            </button>
            <button
                class="border_box border-radius-8 out-line-none font-weight-bold color-gray d-flex align-items-center">
                <img src="../../assets/admin/app-assets/images/icons/chat-bubbles-with-ellipsis.png">
                <a class="color-white" style="margin-left: 10px">Chat với khách hàng</a>
            </button>
        </div>
        <!-- END: Chat -->
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        let url;
        $(".tab-bar-content .content-link a").click(function (ev) {
            ev.preventDefault();
            url = $(this).attr('href');
            $('.tab-bar-content li').removeClass("active");
            $(this).parent().addClass("active");
            getData();
        });
        $(document).on("click", ".content-overview .overview", function (ev) {
            ev.preventDefault();
            url = $(this).children().attr('href');
            $(".content-overview .overview").removeClass("active");
            $(this).addClass("active");
            getData();
        });
        function getData() {
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'html',
                success: function (data) {
                    $('.container-body').html(data);
                }
            })
        }
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
