<?php
$page = 'collaborator_financial_revenue';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ctv/financial/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    {{--                    <a href="{{url('admin/manage_supplier/add')}}" class="btn btn-success"><i class="fa fa-plus"--}}
                    {{--                                                                                               aria-hidden="true"></i> Thêm</a>--}}
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
{{--            <div class="card-content">--}}
{{--                <div class="revenue m-2">--}}
{{--                    <div class="head">--}}
{{--                        <p>Tổng hợp doanh thu</p>--}}
{{--                    </div>--}}
{{--                    <div class="d-flex align-items-center">--}}
{{--                        <div class="col-lg-4 col-md-12 revenue-unit mr-2">--}}
{{--                            <p class="text-lv1">Sẽ thanh toán</p>--}}
{{--                            <p class="total">Tổng cộng</p>--}}
{{--                            <div class="d-flex align-items-center total-price">--}}
{{--                                <span class="number-price">0</span>--}}
{{--                                <span class="money">đ</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-lg-4 col-md-12 revenue-unit">--}}
{{--                            <p class="text-lv1">Đã thanh toán</p>--}}
{{--                            <p class="total">Tuần này</p>--}}
{{--                            <div class="d-flex align-items-center total-price">--}}
{{--                                <span class="number-price">0</span>--}}
{{--                                <span class="money">đ</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <!-- content -->
                                <div class="table-responsive table-responsive-lg">
                                    <table class="table data-list-view table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">Loại doanh thu</th>
                                            <th scope="col">Đơn hàng</th>
                                            <th scope="col">Người mua</th>
                                            <th scope="col">Ngày thanh toán</th>
                                            <th scope="col">Trạng thái</th>
                                            <th scope="col">Số tiền</th>
                                            <th scope="col">Lợi nhuận</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($data))
                                            @foreach($data as $value)
                                                <tr>
                                                    <td><span class="badge badge-{{$value->color}}">{{$value->type_order}}</span></td>
                                                    <td>
                                                        <img style="height: 50px;" src="{{$value->image}}" class="image-preview">
                                                        <p>{{$value->product_name}}</p>
                                                        <p>{{$value->order_code}}</p>
                                                    </td>
                                                    <td>{{$value->name}}</td>
                                                    <td>{{$value->created_at}}</td>
                                                    <td>{{$value->status}}</td>
                                                    <td>{{number_format($value->total_price)}} đ</td>
                                                    <td>{{number_format($value->revenue)}} đ</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td>Không có dữ liệu</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>
