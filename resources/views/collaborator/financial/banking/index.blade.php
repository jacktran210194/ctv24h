<?php
$page = 'collaborator_financial_banking';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <a href="{{url('admin/manage_financial/banking/add')}}" class="btn btn-success"><i class="fa fa-plus"
                                                                                           aria-hidden="true"></i> Thêm</a>
{{--                                        <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>--}}
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content">
                <!-- content -->
                <div class="table-responsive table-responsive-lg">
                    <table class="table data-list-view table-sm">
                        <thead>
                        <tr>
                            <th scope="col">Họ và tên</th>
                            <th scope="col">Số tài khoản</th>
                            <th scope="col">Ngân hàng</th>
                            <th scope="col">Quản lý</th>
                            <th scope="col">Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data))
                            @foreach($data as $value)
                                <tr>
                                    <td>{{$value->user_name}}</td>
                                    <td>{{$value->bank_account}}</td>
                                    <td>{{$value->bank_name.' ('.$value->short_name.')'}}</td>
                                    <td>
                                        <?php if($value->is_active == 0) :?>
                                            <button
                                                class="btn btn-delete btn-success"
                                                data-msg="Bạn có muốn liên kết tài khoản này?"
                                                data-url="{{url('admin/manage_financial/banking/link/'.$value->id)}}"
                                            >Liên kết
                                            </button>
                                        <?php else :?>
                                            <button
                                                class="btn btn-delete btn-danger"
                                                data-msg="Bạn có muốn hủy liên kết tài khoản này?"
                                                data-url="{{url('admin/manage_financial/banking/unlink/'.$value->id)}}"
                                            >Hủy liên kết
                                            </button>
                                        <?php endif ;?>
                                    </td>
                                    <td>
                                        <button
                                            class="btn btn-delete btn-danger"
                                            data-msg=""
                                            data-url="{{url('admin/manage_financial/banking/delete/'.$value->id)}}"
                                        >
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>Không có dữ liệu</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.modal')
@include('admin.base.script')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-active", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('.message-modal').text($(this).attr('data-msg'));
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>

