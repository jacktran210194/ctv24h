<?php
$page = 'collaborator_financial_payment_setting';
$dataLogin = Session::get('data_collaborator');
use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/financial/style.css')}}">
<script src="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.js"></script>
<link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.css"/>
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="d-flex bank-account ml-3 mt-3">
                    <div>
                        <img src="../../assets/admin/app-assets/images/ncc/financial/wallet.png">
                    </div>
                    <div class="ml-1">
                        <p class="title">Thiết lập thanh toán</p>
                    </div>
                </div>
                <div class="box-setting d-flex ml-3 mt-3 mb-3">
                    <div class="col-lg-3 col-md-12 setting-unit">
                        <div class="setting-title mt-2 ml-1 mr-1">
                            <span>Thanh toán bằng thẻ tín dụng/ Thẻ ghi nợ </span>
                        </div>
                        <div class="setting-credit-card mt-1 mb-2 ml-1">
                            <span>Bật tùy chọn này để cho phép Người mua thanh toán bằng thẻ Tín dụng/Ghi nợ.</span>
                        </div>
                        <div class="mb-2 ml-1 float-right">
                            <label class="switch">
                                <input type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 setting-unit ml-2">
                        <div class="setting-title mt-2 ml-1 mr-1">
                            <span>Mã Pin</span>
                        </div>
                        <div class="setting-credit-card mt-1 mb-2 ml-1">
                            <span>Cập nhật mã pin</span>
                        </div>
                        <div class="d-flex mr-2 mt-4">
                            <div class="col-lg-6">

                            </div>
                            <div class="btn-update col-lg-6 col-md-1 float-right">
                                <center>
                                    <a class="btn-update-pin-code">Cập nhật</a>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<div class="popup-setting-pin-code">
    <div class="container-popup-setting-pin-code">
        <div class="header-setting-pin-code">
            <p class="text-bold-700 text-black font-size-24">Thiết lập mã pin</p>
            <p class="mt-3 mb-4">Vui lòng không sử dụng các thông tin như: Dãy số liên tiếp, dãy số lặp lại, ngày sinh nhật hoặc mật khẩu tài khoản ngân hàng làm mật khẩu của ví</p>
        </div>
        <div class="body-setting-pin-code">
            <div class="form-group">
                <label class="lb">Mã pin mới</label>
                <input pattern=" 0+\.[0-9]*[1-9][0-9]*$" name="itemConsumption"
                       onkeypress="return event.charCode >= 48 && event.charCode <= 57"
                    id="pin_code" type="password" class="form-control input-new" placeholder="Nhập mật khẩu">
            </div>
            <div class="form-group">
                <label class="lb">Xác nhận lại mã pin</label>
                <input pattern=" 0+\.[0-9]*[1-9][0-9]*$" name="itemConsumption"
                       onkeypress="return event.charCode >= 48 && event.charCode <= 57"
                    id="re_pin_code" type="password" class="form-control input-new" placeholder="Nhập lại mật khẩu">
            </div>
        </div>
        <div id="recaptcha-container"></div>
        <div class="footer-setting-pin-code d-flex align-items-center justify-content-lg-end mt-3">
            <button class="btn btn-cancel-setting-pin-code">
                Hủy
            </button>
            <button class="btn btn-confirm-setting-pin-code"
                    data-url="{{url('admin/collaborator_financial/payment_setting/create_pin_code')}}"
                    data-phone="<?= $dataLogin ? $dataLogin->phone : '' ?>"
            >
                Tiếp theo
            </button>
        </div>
    </div>
</div>
<div class="popup-verification">
    <div class="container-popup-verification">
        <div class="header-popup-verification mb-2 align-items-center justify-content-center">
            <p class="text-black text-bold-700 text-center font-size-24">Xác minh số điện thoại</p>
            <p class="mt-4 text-center">Vui lòng xác minh danh tính của bạn bằng cách nhập mã OTP được gửi đến số tiện
                thoại của bạn</p>
        </div>
        <div class="body-verification mt-3">
            <div class="form-group">
                <label class="lb">Mã xác minh</label>
                <input id="otp" type="text" class="form-control input-new otp" placeholder="Mã xác minh">
            </div>
        </div>
        <div class="footer-popup d-flex align-items-center justify-content-lg-end mt-3">
            <button class="btn btn-confirm-verification"
                    data-url="{{url('admin/collaborator_financial/payment_setting/save_pin_code')}}"
            >
                Xác nhận
            </button>
            <button class="btn btn-cancel-verification">
                Hủy
            </button>
        </div>
    </div>
</div>
<!-- END: Content-->
<!-- BEGIN: Footer-->
@include('admin.base.footer')
<!-- END: Footer-->
@include('admin.base.script')
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-firestore.js"></script>
<script src="{{url('assets/admin/js/otp/index.js')}}"></script>
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-update-pin-code", function () {
            $('.popup-setting-pin-code').addClass('show-popup');
        });
        $(document).on("click", ".btn-cancel-setting-pin-code", function () {
            $('.popup-setting-pin-code').removeClass('show-popup');
        });
        $(document).on("click", ".btn-cancel-verification", function () {
            $('.popup-verification').removeClass('show-popup-verification');
        });
        let verificationId;
        $(document).on("click", ".btn-confirm-setting-pin-code", async function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('pin_code', $('#pin_code').val());
            form_data.append('re_pin_code', $('#re_pin_code').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: async function (data) {
                    if (data.status) {
                        // swal('Thành công', data.msg, 'success');
                        // window.location.href = data.url;

                        $('#recaptcha-container').show();
                        let applicationVerifier = new firebase.auth.RecaptchaVerifier(
                            'recaptcha-container');
                        let phone = $('.btn-confirm-setting-pin-code').attr('data-phone');
                        let fs = phone.slice(0, 3);
                        if (fs !== '+84') {
                            phone = phone.slice(1, phone.length);
                            phone = '+84' + phone;
                        }
                        let provider = new firebase.auth.PhoneAuthProvider();
                        try {
                            verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                            $('#recaptcha-container').hide();
                            $('.popup-setting-pin-code').removeClass('show-popup');
                            $('.popup-verification').addClass('show-popup-verification');
                        } catch (e) {
                            console.log('e');
                        }
                    } else {
                        swal('Lỗi', data.msg, 'error');
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });

        $(document).on("click", ".btn-confirm-verification", async function () {
            try {
                let verificationCode = $('.otp').val();
                let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
                try {
                    await firebase.auth().signInWithCredential(phoneCredential);
                    let url = $(this).attr('data-url');
                    let form_data = new FormData();
                    form_data.append('pin_code', $('#pin_code').val());
                    form_data.append('re_pin_code', $('#re_pin_code').val());
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: form_data,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status) {
                                swal("Thông báo", data.msg, "success");
                                window.location.href = data.url;
                            } else {
                                swal("Thông báo", data.msg, "error");
                            }
                        },
                        error: function () {
                            console.log('error');
                        },
                    });
                } catch (e) {
                    swal("Thông báo", "Mã OTP không chính xác !", "error");
                }
            } catch (e) {
                swal("Thông báo", "Đã có lỗi xảy ra !", "error");
            }
        });
    });
</script>

<!-- END: Page JS-->


<!-- END: Body-->
</body>
</html>


