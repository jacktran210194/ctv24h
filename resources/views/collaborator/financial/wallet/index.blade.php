<?php
$page = 'collaborator_financial_wallet';
$dataLogin = Session::get('data_collaborator');
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/financial/style.css')}}">
<script src="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.js"></script>
<link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/4.8.0/firebase-ui-auth.css"/>
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<!-- END: Head-->
<style>
    .popup-add-bank-account {
        position: fixed;
        top: 0;
        left: 10%;
        width: 85%;
        height: 100%;
        z-index: 10;
        display: none;
    }

    .popup-add-bank-account .show-pop {
        display: block;
        z-index: 20;
    }

    .popup-add-bank-account .container-popup-add-bank-account {
        width: 40%;
        margin: 20px auto;
        background: #fff;
        border-radius: 16px;
        padding: 30px;
        border: 1px solid #005DB6;
    }

    .container-popup-add-bank-account .header-popup-add-bank-account {
        margin-bottom: 10px;
    }
</style>

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="d-flex">
            <div class="col-lg-9 col-md-12 p-0 pr-2">
                <div class="card">
                    <div class="card-content">
                        <div class="d-flex head-unit justify-content-lg-between mt-2">
                            <div class="box-content-f payment-unit-wait">
                                <div class="d-flex">
                                    <div class="col-lg-6 col-md-12">
                                        <p class="payment-wait mt-2">Ví CTV24</p>
                                        <p class="payment-total">Tổng cộng</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 pt-2">
                                        <p class="payment-price"><?= isset($data) ? number_format($data->wallet) : '' ?>đ</p>
                                    </div>
                                </div>
                                <div class="d-flex align-items-end justify-content-lg-between mb-1 p-1 bt">
                                    <div>
                                        <center>
                                            <a class="btn btn_main btn-withdrawal">Rút tiền</a>
                                        </center>
                                    </div>
                                    <div>
                                        <center>
                                            <a href="{{url('admin/collaborator_financial/wallet/payment')}}" class="btn btn_blue">Lịch sử rút tiền</a>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="box-content-f payment-unit-wait">
                                <div class="d-flex">
                                    <div class="col-lg-6 col-md-12">
                                        <p class="payment-wait mt-2">Ví Point</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 pt-2">
                                        <p class="payment-price"><?= isset($data) ? $data->point : '' ?> điểm</p>
                                    </div>
                                </div>
                                <div class="d-flex align-items-end justify-content-lg-between mb-1 p-1 bt">
                                    <div>
                                        <center>
                                            <a class="btn btn_main">Nạp điểm</a>
                                        </center>
                                    </div>
                                    <div class="btn-point">
                                        <center>
                                            <a class="btn btn_blue">Lịch sử điểm</a>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="box-content-f payment-unit-wait">
                                <div class="d-flex">
                                    <div class="col-lg-6 col-md-12">
                                        <p class="payment-wait mt-2">Ví Xu</p>
                                    </div>
                                    <div class="col-lg-6 col-md-12 pt-2">
                                        <p class="payment-price"><?= isset($data) ? $data->coin : '' ?> xu</p>
                                    </div>
                                </div>
                                <div class="d-flex align-items-end justify-content-lg-end mb-1 p-1 bt">
                                    <div class="btn-point">
                                        <center>
                                            <a class="btn btn_blue">Lịch sử xu</a>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex bank-account ml-2 mt-5">
                            <div>
                                <img src="../../assets/admin/app-assets/images/ncc/financial/wallet.png">
                            </div>
                            <div class="ml-2">
                                <p class="title">Tài khoản ngân hàng</p>
                                <div class=" align-items-center justify-content-lg-around">
                                    @if(isset($dataBankAccount))
                                        @foreach($dataBankAccount as $valueBankAccount)
                                            <a href="{{url('admin/collaborator_financial/banking/add')}}">
                                                <div class="d-flex align-items-center just">
                                                    <p class="text-black font-size-17">{{$valueBankAccount->short_name}} - {{$valueBankAccount->bank_name}}
                                                        - {{'*** ' .$valueBankAccount->sub_bank_account}} @if($valueBankAccount->is_default == 1) <span class="bg-default-bank-acc ml-1">Mặc định</span> @endif
                                                    </p>
                                                </div>
                                            </a>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="body-unit">

                </div>
            </div>
            <div class="col-lg-3 col-md-12 bank-unit p-0">
                <div class="card">
                    <div class="card-content">
                        <div class="logo-card p-2">
                            <?php if(isset($dataBankAccountRight)): ?>
                                <img src="../../assets/admin/app-assets/images/ncc/financial/card.png">
                            <?php endif; ?>
                                <span class="title">Tài khoản ngân hàng</span>
                        </div>
                        <div class="align-items-center justify-content-center mb-2 card-right">
                            <a class="view-bank-account"
                            >
                                <div class="card-unit-mgl-zezo mb-3">
                                    <div class="card-bg">
                                        <img class=""
                                             src="../../assets/admin/app-assets/images/ncc/financial/card_red.png" style="width: 100%">
                                        <div class="card-text p-2">
                                            <p class="text-white text-bold-700"><?= isset($dataBankAccountRight) ? $dataBankAccountRight->bank_name . ' - ' . $dataBankAccountRight->short_name : '' ?></p>
                                            <p class="text-white text-bold-700 number-account"><?= isset($dataBankAccountRight) ? $dataBankAccountRight->first_num_acc . ' *** *** ' . $dataBankAccountRight->last_num_acc : '' ?></p>
                                            <p class="text-white text-bold-700 name-account"><?= isset($dataBankAccountRight) ? mb_strtoupper($dataBankAccountRight->user_name) : '' ?></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="add-card d-flex mb-2">
                            <div class="add-card-tool pt-1" style="width: 100%!important;">
                                <button class="btn btn-delete-bank"
                                        data-url="<?= isset($dataBankAccountRight) ? 'admin/collaborator_financial/wallet/delete_bank_account' : '' ?>"
                                        data-id="<?= isset($dataBankAccountRight) ? $dataBankAccountRight->id : ''?>"
                                >
                                    <div class="text-center">
                                        <img
                                            src="../../assets/admin/app-assets/images/ncc/financial/business_and_finance.png">
                                    </div>
                                    <p class="mt-1">Huỷ liên kết ngân hàng</p>
                                </button>
                            </div>
                            <div class="add-card-tool pt-1" style="width: 100%!important;">
                                <button class="btn btn-add-bank-account">
                                    <div class="text-center">
                                        <img src="../../assets/admin/app-assets/images/ncc/financial/plus.png">
                                    </div>
                                    <p class="mt-1">Thêm tài khoản ngân hàng</p>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->
</div>

<!-- START: Popup Thêm tài khoản ngân hàng-->
<div class="popup-add-bank-account" style="z-index: 1000">
    <div class="container-popup-add-bank-account">
        <div class="header-popup-add-bank-account mb-2">
            <p class="text-black text-bold-700 f-18">Thêm tài khoản ngân hàng</p>
        </div>
        <div class="body-bank-account">
            <div class="form-group">
                <label class="lb">Họ và tên</label>
                <input id="name" type="text" class="form-control input-new" placeholder="Họ và tên">
            </div>
            <div class="form-group">
                <label class="lb">Số CMND</label>
                <input id="identify_card" type="number" class="form-control input-new" placeholder="Số CMND">
            </div>
            <div class="form-group">
                <label class="lb">Tên chủ tài khoản</label>
                <input id="user_name" type="text" class="form-control input-new" placeholder="Tên chủ tài khoản">
            </div>
            <div class="form-group">
                <label class="lb">Tên ngân hàng</label>
                <select id="bank_name" class="form-control option-new">
                    @if(isset($dataBank))
                        @foreach($dataBank as $valueBank)
                            <option
                                value="{{$valueBank->id}}">{{$valueBank->name ." (" .$valueBank->short_name .")"}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label class="lb">Số tài khoản</label>
                <input id="bank_account" type="number" class="form-control input-new" placeholder="Số tài khoản">
            </div>
            <div class="form-group">
                <label class="lb">Khu vực</label>
                <select id="branch" class="form-control option-new">
                    @if(isset($dataCity))
                        @foreach($dataCity as $valueCity)
                            <option value="{{$valueCity->id}}">{{$valueCity->name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div id="recaptcha-container-bank"></div>
        <div class="footer-popup d-flex align-items-center justify-content-lg-end mt-2">
            <button class="btn btn-cancel-bank-account">
                Hủy
            </button>
            <button class="btn btn-confirm-bank-account"
                    data-url="{{url('admin/collaborator_financial/wallet/create_bank_account')}}"
                    data-phone="<?= $dataLogin ? $dataLogin->phone : '' ?>"
            >
                Tiếp theo
            </button>
        </div>
    </div>
</div>
<!-- END: Popup Thêm tài khoản ngân hàng-->

<!-- START: Popup Xác minh SĐT-->
<div class="popup-verification" style="z-index: 1000">
    <div class="container-popup-verification">
        <div class="header-popup-verification mb-2 align-items-center justify-content-center">
            <p class="text-black text-bold-700 text-center font-size-24">Xác minh số điện thoại</p>
            <p class="mt-4 text-center">Vui lòng xác minh danh tính của bạn bằng cách nhập mã OTP được gửi đến số tiện
                thoại của bạn</p>
        </div>
        <div class="body-verification mt-3">
            <div class="form-group">
                <label class="lb">Mã xác minh</label>
                <input id="otp_bank" type="text" class="form-control input-new otp_bank" placeholder="Mã xác minh">
            </div>
        </div>
        <div class="footer-popup d-flex align-items-center justify-content-lg-end mt-3">
            <button class="btn btn-confirm-verification"
                    data-url="{{url('admin/collaborator_financial/wallet/save_bank_account')}}"
            >
                Xác nhận
            </button>
            <button class="btn btn-cancel-verification">
                Hủy
            </button>
        </div>
    </div>
</div>
<!-- END: Popup Xác minh SĐT-->

<!-- START: Popup Rút tiền-->
<div class="popup-withdrawal" style="z-index: 1000">
    <div class="container-popup-withdrawal">
         <div class="header-popup-withdrawal">
             <p class="text-black text-bold-700 font-size-24">Rút tiền</p>
         </div>
         <div class="body-withdrawal mt-2">
             <div class="form-group">
                 <span class="text-grey">Tài khoản ngân hàng</span>
                 <select name="" id="bank_id" class="form-control option-new">
                     @if(isset($dataBankAccount))
                         @foreach($dataBankAccount as $valueBankAcc)
                            <option value="{{$valueBankAcc->id}}">{{$valueBankAcc->short_name . ' - ' . $valueBankAcc->bank_name . ' - ' .$valueBankAcc->bank_account}}</option>
                         @endforeach
                     @endif
                 </select>
                 @if(!count($dataBankAccount))
                     <span class="text-red text-bold-700 text-italic">Thêm tài khoản ngân hàng và thiết lập mã Pin để rút tiền</span>
                 @endif
                 @if(isset($data) && $data->pin_code == null)
                     <span class="text-red text-bold-700 text-italic">Vui lòng thiết lập mã Pin để rút tiền</span>
                 @endif
             </div>
             <div class="form-group">
                 <span class="text-grey">Số tiền muốn rút</span>
                 <input id="price" type="number" class="form-control input-new" placeholder="Số tiền muốn rút" <?= isset($data) && ($data->pin_code == null) ? 'disabled' : ''?>>
             </div>
             <div class="form-group">
                 <p class="text-grey">Phí thanh toán</p>
                 <span class="text-black text-bold-700">11.000đ</span>
                 <span class="text-grey">(phí rút tiền chỉ áp dụng cho các giao dịch thành công)</span>
             </div>
             <div class="form-group">
                 <span class="text-grey">Số tiền chuyển vào tài khoản</span>
                 <p class="text-red text-bold-700 font-size-17 price-show"></p>
             </div>
         </div>
        <div class="footer-popup-withdrawal d-flex align-items-center justify-content-lg-end mt-3">
            <button class="btn btn-confirm-withdrawal" <?= isset($data) && ($data->pin_code == null) ? 'disabled' : '' ?>
                    data-url="{{url('admin/collaborator_financial/wallet/withdrawal')}}"
            >
                Xác nhận
            </button>
            <button class="btn btn-cancel-withdrawal">
                Hủy
            </button>
        </div>
    </div>
</div>
<!-- END: Popup Rút tiền-->

<!-- START: Popup Nhập mã Pin-->
<div class="popup-verification-withdrawal" style="z-index: 1000">
    <div class="container-popup-verification-withdrawal">
        <div class="header-popup-verification-withdrawal mb-2 align-items-center justify-content-center">
            <p class="mt-3 text-center">Để đảm bảo luôn an toàn. Bạn vui lòng nhập mã pin</p>
        </div>
        <div class="body-verification-withdrawal mt-3">
            <div class="form-group">
                <label class="lb">Mã Pin</label>
                <input id="pin_code_input" type="text" class="form-control input-new">
            </div>
        </div>
        <div class="footer-popup-verification-withdrawal d-flex align-items-center justify-content-center mt-3">
            <button class="btn btn-confirm-verification-withdrawal"
                    data-url="{{url('admin/collaborator_financial/wallet/withdrawal/save')}}"
            >
                Xác nhận
            </button>
            <button class="btn btn-cancel-verification-withdrawal">
                Hủy
            </button>
        </div>
        <div class="mt-3">
            <p class="text-black text-bold-700 text-center text-underline forgot-pin-code">Quên mã Pin?</p>
        </div>
    </div>
</div>
<!-- END: Popup Nhập mã Pin-->

<!-- START: Popup Đổi mã Pin-->
<div class="popup-setting-pin-code" style="z-index: 1000">
    <div class="container-popup-setting-pin-code">
        <div class="header-setting-pin-code">
            <p class="text-bold-700 text-black font-size-24">Thiết lập mã pin</p>
            <p class="mt-3 mb-4">Vui lòng không sử dụng các thông tin như: Dãy số liên tiếp, dãy số lặp lại, ngày sinh nhật hoặc mật khẩu tài khoản ngân hàng làm mật khẩu của ví</p>
        </div>
        <div class="body-setting-pin-code">
            <div class="form-group">
                <label class="lb">Mã pin mới</label>
                <input pattern=" 0+\.[0-9]*[1-9][0-9]*$" name="itemConsumption"
                       onkeypress="return event.charCode >= 48 && event.charCode <= 57"
                       id="pin_code" type="password" class="form-control input-new" placeholder="Nhập mật khẩu">
            </div>
            <div class="form-group">
                <label class="lb">Xác nhận lại mã pin</label>
                <input pattern=" 0+\.[0-9]*[1-9][0-9]*$" name="itemConsumption"
                       onkeypress="return event.charCode >= 48 && event.charCode <= 57"
                       id="re_pin_code" type="password" class="form-control input-new" placeholder="Nhập lại mật khẩu">
            </div>
        </div>
        <div id="recaptcha-container"></div>
        <div class="footer-setting-pin-code d-flex align-items-center justify-content-lg-end mt-3">
            <button class="btn btn-cancel-setting-pin-code">
                Hủy
            </button>
            <button class="btn btn-confirm-setting-pin-code"
                    data-url="{{url('admin/collaborator_financial/payment_setting/create_pin_code')}}"
                    data-phone="<?= $dataLogin ? $dataLogin->phone : '' ?>"
            >
                Tiếp theo
            </button>
        </div>
    </div>
</div>
<!-- END: Popup Đổi mã Pin-->

<!-- START: Popup Xác thực mã Pin-->
<div class="popup-verification-pin-code" style="z-index: 1000">
    <div class="container-popup-verification-pin-code">
        <div class="header-popup-verification mb-2 align-items-center justify-content-center">
            <p class="text-black text-bold-700 text-center font-size-24">Xác minh số điện thoại</p>
            <p class="mt-4 text-center">Vui lòng xác minh danh tính của bạn bằng cách nhập mã OTP được gửi đến số tiện
                thoại của bạn</p>
        </div>
        <div class="body-verification-pin-code mt-3">
            <div class="form-group">
                <label class="lb">Mã xác minh</label>
                <input id="otp" type="text" class="form-control input-new otp" placeholder="Mã xác minh">
            </div>
        </div>
        <div class="footer-popup d-flex align-items-center justify-content-lg-end mt-3">
            <button class="btn btn-confirm-verification-pin-code"
                    data-url="{{url('admin/collaborator_financial/payment_setting/save_pin_code')}}"
            >
                Xác nhận
            </button>
            <button class="btn btn-cancel-verification-pin-code">
                Hủy
            </button>
        </div>
    </div>
</div>
<!-- END: Popup Xác thực mã Pin-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-firestore.js"></script>
<script src="{{url('assets/admin/js/otp/index.js')}}"></script>
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-add-bank-account", function () {
            $('.popup-add-bank-account').addClass('show-popup');
            // $('#overlay1').addClass('open');
        });
        $(document).on("click", ".btn-cancel-bank-account", function () {
            $('.popup-add-bank-account').removeClass('show-popup');
            // $('#overlay1').removeClass('open');
        });

        //Bank
        let verificationId;
        $(document).on("click", ".btn-confirm-bank-account", async function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('name', $('#name').val());
            form_data.append('identify_card', $('#identify_card').val());
            form_data.append('user_name', $('#user_name').val());
            form_data.append('bank_name', $('#bank_name').val());
            form_data.append('bank_account', $('#bank_account').val());
            form_data.append('branch', $('#branch').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: async function (data) {
                    if (data.status) {
                        // swal('Thành công', data.msg, 'success');
                        // window.location.href = data.url;

                        // $('.popup-add-bank-account').removeClass('show-popup');
                        // $('.popup-verification').addClass('show-popup-verification');

                        $('#recaptcha-container-bank').show();
                        let applicationVerifier = new firebase.auth.RecaptchaVerifier(
                            'recaptcha-container-bank',{
                                size: "invisible",
                                callback: function(response) {
                                    submitPhoneNumberAuth();
                                }
                            });
                        let phone = $('.btn-confirm-bank-account').attr('data-phone');
                        let fs = phone.slice(0, 3);
                        if (fs !== '+84') {
                            phone = phone.slice(1, phone.length);
                            phone = '+84' + phone;
                        }
                        let provider = new firebase.auth.PhoneAuthProvider();
                        try {
                            verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                            $('#recaptcha-container-bank').hide();
                            $('.popup-add-bank-account').removeClass('show-popup');
                            $('.popup-verification').addClass('show-popup-verification');
                            // $('#overlay1').addClass('open');
                        } catch (e) {
                            console.log('e');
                            let click = 0;
                            $(document).on("click", ".swal-button--confirm", function () {
                                console.log(click++);
                            });
                        }
                    } else {
                        swal('Lỗi', data.msg, 'error');
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-confirm-verification", async function () {
            try {
                let verificationCode = $('.otp_bank').val();
                let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
                try {
                    await firebase.auth().signInWithCredential(phoneCredential);
                    let url = $(this).attr('data-url');
                    let form_data = new FormData();
                    form_data.append('identify_card', $('#identify_card').val());
                    form_data.append('user_name', $('#user_name').val());
                    form_data.append('bank_name', $('#bank_name').val());
                    form_data.append('bank_account', $('#bank_account').val());
                    form_data.append('branch', $('#branch').val());
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: form_data,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status) {
                                $('#overlay1').removeClass('open');
                                swal("Thông báo", data.msg, "success");
                                window.location.href = data.url;
                            } else {
                                swal("Thông báo", data.msg, "error");
                            }
                        },
                        error: function () {
                            console.log('error');
                        },
                    });
                } catch (e) {
                    swal("Thông báo", "Mã OTP không chính xác !", "error");
                }
            } catch (e) {
                swal("Thông báo", "Đã có lỗi xảy ra !", "error");
            }
        });
        //Bank

        $(document).on("click", ".btn-cancel-verification", function () {
            $('.popup-verification').removeClass('show-popup-verification');
            // $('#overlay1').removeClass('open');

        });
        $(document).on("click", ".btn-withdrawal", function () {
            $('.popup-withdrawal').addClass('show-popup-withdrawal');
            // $('#overlay1').addClass('open');
        });
        $(document).on("blur", "#price", function () {
            let view_price = $(this).val();
            $('.price-show').text(Intl.NumberFormat().format(view_price) + 'đ');
        });
        $(document).on("click", ".btn-cancel-withdrawal", function () {
            $('.popup-withdrawal').removeClass('show-popup-withdrawal');
            // $('#overlay1').removeClass('open');
        });
        $(document).on("click", ".btn-confirm-withdrawal", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('bank_id', $('#bank_id').val());
            form_data.append('price', $('#price').val());
            let bank = $('#bank_id').val();
            $('.btn-confirm-verification-withdrawal').attr('data-bank', bank);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: async function (data) {
                    if (data.status) {
                        $('.popup-withdrawal').removeClass('show-popup-withdrawal');
                        $('.popup-verification-withdrawal').addClass('show-popup-verification-withdrawal');
                        // $('#overlay1').addClass('open');
                    } else {
                        swal('Lỗi', data.msg, 'error');
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-cancel-verification-withdrawal", function () {
            $('.popup-verification-withdrawal').removeClass('show-popup-verification-withdrawal');
        });
        $(document).on("click", ".btn-confirm-verification-withdrawal", function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('pin_code_input', $('#pin_code_input').val());
            form_data.append('price', $('#price').val());
            form_data.append('bank_id', $(this).attr('data-bank'));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: async function (data) {
                    if (data.status) {
                        $('.popup-verification-withdrawal').removeClass('show-popup-verification-withdrawal');
                        $('#overlay1').removeClass('open');
                        swal('Thành công', data.msg, 'success');
                        window.location.href = data.url;
                    } else {
                        swal('Lỗi', data.msg, 'error');
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".forgot-pin-code", function () {
            $('.popup-verification-withdrawal').removeClass('show-popup-verification-withdrawal');
            $('.popup-setting-pin-code').addClass('show-popup');
            // $('#overlay1').addClass('open');
        });
        $(document).on("click", ".btn-update-pin-code", function () {
            $('.popup-setting-pin-code').addClass('show-popup');
            // $('#overlay1').addClass('open');
        });
        $(document).on("click", ".btn-cancel-setting-pin-code", function () {
            $('.popup-setting-pin-code').removeClass('show-popup');
            // $('#overlay1').removeClass('open');
        });
        $(document).on("click", ".btn-cancel-verification-pin-code", function () {
            $('.popup-verification-pin-code').removeClass('show-popup-verification-pin-code');
            // $('#overlay1').removeClass('open');
        });

        //Đổi Pin
        $(document).on("click", ".btn-confirm-setting-pin-code", async function () {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('id', $(this).attr('data-id'));
            form_data.append('pin_code', $('#pin_code').val());
            form_data.append('re_pin_code', $('#re_pin_code').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: async function (data) {
                    if (data.status) {
                        // $('.popup-setting-pin-code').removeClass('show-popup');
                        // $('.popup-verification-pin-code').addClass('show-popup-verification-pin-code');

                        $('#recaptcha-container').show();
                        let applicationVerifier = new firebase.auth.RecaptchaVerifier(
                            'recaptcha-container');
                        let phone = $('.btn-confirm-setting-pin-code').attr('data-phone');
                        let fs = phone.slice(0, 3);
                        if (fs !== '+84') {
                            phone = phone.slice(1, phone.length);
                            phone = '+84' + phone;
                        }
                        let provider = new firebase.auth.PhoneAuthProvider();
                        try {
                            verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                            $('#recaptcha-container').hide();
                            $('.popup-setting-pin-code').removeClass('show-popup');
                            $('.popup-verification-pin-code').addClass('show-popup-verification-pin-code');
                            // $('#overlay1').addClass('open');
                        } catch (e) {
                            console.log('e');
                        }
                    } else {
                        swal('Lỗi', data.msg, 'error');
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-confirm-verification-pin-code", async function () {
            try {
                let verificationCode = $('.otp').val();
                let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
                try {
                    await firebase.auth().signInWithCredential(phoneCredential);
                    let url = $(this).attr('data-url');
                    let form_data = new FormData();
                    form_data.append('pin_code', $('#pin_code').val());
                    form_data.append('re_pin_code', $('#re_pin_code').val());
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: form_data,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status) {
                                $('.popup-verification-pin-code').removeClass('show-popup-verification-pin-code');
                                $('#overlay1').removeClass('open');
                                swal("Thông báo", data.msg, "success");
                                window.location.reload();
                            } else {
                                swal("Thông báo", data.msg, "error");
                            }
                        },
                        error: function () {
                            console.log('error');
                        },
                    });
                } catch (e) {
                    swal("Thông báo", "Mã OTP không chính xác !", "error");
                }
            } catch (e) {
                swal("Thông báo", "Đã có lỗi xảy ra !", "error");
            }
        });


        //Lịch sử các giao dịch gần đây
        let url_filter = window.location.origin;
        let url = url_filter + '/admin/collaborator_financial/wallet/status_all';
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $('.status-all').addClass('checked');
                $('.status-revenue').removeClass('checked');
                $('.status-withdrawal').removeClass('checked');
                $('.status-refund').removeClass('checked');
                $('.status-bonus').removeClass('checked');
                $('#body-unit').html(data);
            },
            error: function () {
                console.log('error')
            }
        });

        //Tất cả
        $(document).on("click", ".status-all", function () {
            let url = url_filter + '/admin/collaborator_financial/wallet/status_all';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.status-all').addClass('checked');
                    $('.status-revenue').removeClass('checked');
                    $('.status-withdrawal').removeClass('checked');
                    $('.status-refund').removeClass('checked');
                    $('.status-bonus').removeClass('checked');
                    $('#body-unit').html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Doanh thu
        $(document).on("click", ".status-revenue", function () {
            let url = url_filter + '/admin/collaborator_financial/wallet/status_revenue';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.status-all').removeClass('checked');
                    $('.status-revenue').addClass('checked');
                    $('.status-withdrawal').removeClass('checked');
                    $('.status-refund').removeClass('checked');
                    $('.status-bonus').removeClass('checked');
                    $('#body-unit').html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Rút tiền
        $(document).on("click", ".status-withdrawal", function () {
            let url = url_filter + '/admin/collaborator_financial/wallet/status_withdrawal';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.status-all').removeClass('checked');
                    $('.status-revenue').removeClass('checked');
                    $('.status-withdrawal').addClass('checked');
                    $('.status-refund').removeClass('checked');
                    $('.status-bonus').removeClass('checked');
                    $('#body-unit').html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Hoàn tiền
        // $(document).on("click", ".status-refund", function () {
        //     let url = url_filter + '/admin/collaborator_financial/wallet/status_refund';
        //     $.ajax({
        //         url: url,
        //         type: "GET",
        //         dataType: 'html',
        //         success: function (data) {
        //             $('.status-all').removeClass('checked');
        //             $('.status-revenue').removeClass('checked');
        //             $('.status-withdrawal').removeClass('checked');
        //             $('.status-refund').addClass('checked');
        //             $('.status-bonus').removeClass('checked');
        //             $('#body-unit').html(data);
        //         },
        //         error: function () {
        //             console.log('error')
        //         }
        //     });
        // });

        // Hoa hồng giới thiệu
        // $(document).on("click", ".status-bonus", function () {
        //     let url = url_filter + '/admin/collaborator_financial/wallet/status_bonus';
        //     $.ajax({
        //         url: url,
        //         type: "GET",
        //         dataType: 'html',
        //         success: function (data) {
        //             $('.status-all').removeClass('checked');
        //             $('.status-revenue').removeClass('checked');
        //             $('.status-withdrawal').removeClass('checked');
        //             $('.status-refund').removeClass('checked');
        //             $('.status-bonus').addClass('checked');
        //             $('#body-unit').html(data);
        //         },
        //         error: function () {
        //             console.log('error')
        //         }
        //     });
        // });


        //Lịch sử các giao dịch gần đây | Lọc theo ngày | Tất cả
        function filter_date_all() {
            let url = $('.btn-filter-order-all').attr('data-url');
            let data = {};
            data['time_start'] = $('.time-start').val();
            data['time_end'] = $('.time-end').val();
            if (data['time_start'] == '' || data['time_end'] == '' || (data['time_start'] > data['time_end'])) {
                swal("Thông báo", "Vui lòng chọn khoảng thời gian phù hợp", "error");
                return false;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: data,
                success: function (data) {
                    $('tbody').html(data.table_data);
                },
                error: function () {
                    console.log('error')
                },
            });
        }
        $(document).on('click', '.btn-filter-order-all', function () {
            filter_date_all();
        });

        //Lịch sử các giao dịch gần đây | Lọc theo ngày | Doanh thu
        function filter_date_revenue() {
            let url = $('.btn-filter-order-revenue').attr('data-url');
            let data = {};
            data['time_start'] = $('.time-start').val();
            data['time_end'] = $('.time-end').val();
            if (data['time_start'] == '' || data['time_end'] == '' || (data['time_start'] > data['time_end'])) {
                swal("Thông báo", "Vui lòng chọn khoảng thời gian phù hợp", "error");
                return false;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: data,
                success: function (data) {
                    $('tbody').html(data.table_data);
                },
                error: function () {
                    console.log('error')
                },
            });
        }
        $(document).on('click', '.btn-filter-order-revenue', function () {
            filter_date_revenue();
        });

        //Lịch sử các giao dịch gần đây | Lọc theo ngày | Rút tiền
        function filter_date_withdrawal() {
            let url = $('.btn-filter-order-withdrawal').attr('data-url');
            let data = {};
            data['time_start'] = $('.time-start').val();
            data['time_end'] = $('.time-end').val();
            if (data['time_start'] == '' || data['time_end'] == '' || (data['time_start'] > data['time_end'])) {
                swal("Thông báo", "Vui lòng chọn khoảng thời gian phù hợp", "error");
                return false;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: data,
                success: function (data) {
                    $('tbody').html(data.table_data);
                },
                error: function () {
                    console.log('error')
                },
            });
        }
        $(document).on('click', '.btn-filter-order-withdrawal', function () {
            filter_date_withdrawal();
        });
        $(document).on('click', '.status-refund', function () {
            swal('Thông báo', 'Tính năng đang được phát triển', 'warning');
        });
        $(document).on('click', '.status-bonus', function () {
            swal('Thông báo', 'Tính năng đang được phát triển', 'warning');
        });
        $(document).on('click', '.btn-delete-bank', function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'json',
                data: data,
                success: function (data) {
                    if(data.status){
                        swal('Thông báo', 'Hủy liên kết thành công', 'success');
                        window.location.reload();
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        });
    });
</script>
</body>
<!-- END: Footer-->

<!-- END: Body-->
</html>

