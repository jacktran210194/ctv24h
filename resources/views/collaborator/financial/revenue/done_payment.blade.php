<div class="card">
    <div class="card-content">
        <div class="d-flex table-head m-2">
            <div class="d-flex col-lg-7 p-0 align-items-center justify-content-lg-between">
                <div class="col-lg-3">
                    <span>Chọn ngày</span>
                </div>
                <div>
                    <input type="date" class="form-control mr-1 time-start" value="{{date('Y-m-d')}}">
                </div>
                <span class="ml-1 mr-1">đến</span>
                <div>
                    <input type="date" class="form-control time-end" value="{{date('Y-m-d')}}">
                </div>
                <button type="button" style="width: 8%; line-height: 20px;" class="btn border btn-filter-order ml-1"
                        data-url="{{url('admin/collaborator_financial/revenue/filter_date_done')}}"
                ><i class="fa fa-search"></i>
                </button>
            </div>
            <div class="col-lg-5 col-md-12 p-0 pr-2 d-flex align-items-center justify-content-end">
                <div class="btn-export">
                    <a href="{{url('admin/collaborator_financial/revenue/export_done_payment')}}">Xuất file</a>
                </div>
{{--                <div>--}}
{{--                    <select name="" id="status-done" class="form-control">--}}
{{--                        <option value="all">Tất cả</option>--}}
{{--                        <option value="0">Bán hàng</option>--}}
{{--                        <option value="1">Hoa hồng</option>--}}
{{--                    </select>--}}
{{--                </div>--}}
            </div>
        </div>
        <div class="main">
            <div class="main-unit align-items-center">
                <div class="col-lg-4 col-md-12 d-flex">
                    <div class="tab-menu col-lg-6 align-items-center checked filter-done">
                        <center>
                            <a
                                data-url-done="{{url('admin/collaborator_financial/revenue/done')}}"
                            >Đã thanh toán</a>
                        </center>
                    </div>
                    <div class="tab-menu col-lg-6 align-items-center filter-waiting">
                        <center>
                            <a
                                data-url-waiting="{{url('admin/collaborator_financial/revenue/waiting')}}"
                            >Sẽ thanh toán</a>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <!-- content -->
        <div class="table-responsive table-responsive-lg p-1">
            <table class="table data-list-view table-sm">
                <thead class="text-center">
                <tr>
                    <th scope="col">Loại doanh thu</th>
                    <th scope="col">Đơn hàng</th>
                    <th scope="col">Người mua</th>
                    <th scope="col">Ngày thanh toán</th>
                    <th scope="col">Trạng thái</th>
                    <th scope="col">Số tiền</th>
                </tr>
                </thead>
                <tbody>
                @if(count($data))
                    @foreach($data as $value)
                        <tr class="text-center">
                            <td>
                                <span class="{{$value->color}}">{{$value->type_order}}</span>
                            </td>
                            <td>
                                <div class="d-flex align-items-center justify-content-center">
                                    <img style="width: 60px; height: 60px; object-fit: cover; border-radius: 14px;"
                                         src="{{$value->image_product}}" class="image-preview">
                                    <span class="text-over">{{$value->name_product}}</span>
                                </div>
                            </td>
                            <td>{{$value->name_customer}}</td>
                            <td>
                                <span>{{$value->updated_at}}</span>
                                <br>
                            </td>
                            <td>Đã nhận được hàng</td>
                            <td>
                                <span>{{number_format($value->price)}}đ</span>
                                <br>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>Không có dữ liệu</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>

    </div>
</div>
