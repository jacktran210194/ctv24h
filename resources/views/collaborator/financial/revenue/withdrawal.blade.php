<?php
$page = 'collaborator_financial_revenue';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
{{--                    <a href="{{url('admin/manage_supplier/add')}}" class="btn btn-success"><i class="fa fa-plus"--}}
{{--                                                                                               aria-hidden="true"></i> Thêm</a>--}}
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="card">
            <div class="card-content">
                <!-- content -->
                <div class="table-responsive table-responsive-lg">
                    <table class="table data-list-view table-sm">
                        <thead>
                        <tr>
                            <th scope="col">Ngày</th>
                            <th scope="col">Loại giao dịch | Mô tả</th>
                            <th scope="col">Tình trạng</th>
                            <th scope="col">Số tiền</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($dataWithdrawal))
                            @foreach($dataWithdrawal as $value)
                                <tr>
                                    <td>{{$value->date}}</td>
                                    <td>Rút tiền từ đơn hàng #{{$value->order_code}}</td>
                                    <td><span class="badge badge-{{$value->color}}">{{$value->status}}</span></td>
                                    <td>{{number_format($value->total_price)}} đ</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>Không có dữ liệu</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-active", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('#myModal').modal('show');
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>
