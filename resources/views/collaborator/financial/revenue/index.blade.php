<?php
$page = 'collaborator_financial_revenue';
use Illuminate\Support\Facades\URL;
$dataLogin = Session::get('data_collaborator');
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<link rel="stylesheet" type="text/css" href="{{url('assets/admin/assets/css/ncc/financial/style.css')}}">

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

<!-- BEGIN: Header-->

<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    {{--                    <a href="{{url('admin/manage_supplier/add')}}" class="btn btn-success"><i class="fa fa-plus"--}}
                    {{--                                                                                               aria-hidden="true"></i> Thêm</a>--}}
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')

<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <!-- Dashboard Ecommerce Starts -->
        <div class="d-flex">
            <div class="col-lg-9 col-md-12 p-0">
                <div class="card">
                    <div class="card-content">
                        <div class="d-flex head-unit m-2">
                            <div class="col-lg-4 col-md-12 payment-unit-wait mr-2">
                                <p class="payment-wait mt-1">Sẽ thanh toán</p>
                                <p class="payment-total">Tổng cộng</p>
                                <div class="d-flex mt-3">
                                    <p class="payment-price"><?= isset($countRevenueWaitingPayment) ? number_format($countRevenueWaitingPayment->countWaiting) : '' ?>đ</p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12 payment-unit-wait">
                                <p class="payment-wait mt-1">Đã thanh toán</p>
                                <p class="payment-total">Tuần này</p>
                                <div class="d-flex mt-3">
                                    <p class="payment-price"><?= isset($countRevenueDonePayment) ? number_format($countRevenueDonePayment->countDone) : '' ?>đ</p>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex bank-account ml-2 mt-5">
                            <div>
                                <img src="../../assets/admin/app-assets/images/ncc/financial/wallet.png">
                            </div>
                            <div class="ml-2">
                                <p class="title">Tài khoản ngân hàng</p>
                                @if(isset($dataBankAccount))
                                    @foreach($dataBankAccount as $valueBankAccount)
                                        <a href="{{url('admin/collaborator_financial/banking/add')}}">
                                            <p class="text-black font-size-17">{{$valueBankAccount->short_name}} - {{$valueBankAccount->bank_name}}
                                                - {{'*** ' .$valueBankAccount->sub_bank_account}}</p>
                                        </a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div id="show_content">

                </div>
            </div>
            <div class="col-lg-3 col-md-12 wallet-unit p-0 pl-2">
                <div class="wallet-unit-ctv pl-1 mb-2">
                    <p class="wallet-ctv mt-1">Ví CTV</p>
                    <p class="wallet-total">Tổng cộng</p>
                    <div class="d-flex mt-3 pr-2 align-items-lg-end justify-content-lg-end">
                        <p class="wallet-price"><?= isset($dataCustomer) ? number_format($dataCustomer->wallet) : '' ?>đ</p>
                    </div>
                </div>
                <div class="wallet-unit-ctv pl-1 mb-2">
                    <p class="wallet-ctv mt-1">Ví Point</p>
                    <p class="wallet-total">Tuần này</p>
                    <div class="d-flex mt-3 pr-2 align-items-lg-end justify-content-lg-end">
                        <p class="wallet-price"><?= isset($dataCustomer) ? number_format($dataCustomer->point) : '' ?> điểm</p>
                    </div>
                </div>
                <div class="wallet-unit-ctv pl-1">
                    <p class="wallet-ctv mt-1">Ví Xu</p>
                    <p class="wallet-total">Tuần này</p>
                    <div class="d-flex mt-3 pr-2 align-items-lg-end justify-content-lg-end">
                        <p class="wallet-price"><?= isset($dataCustomer) ? number_format($dataCustomer->coin) : '' ?> xu</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
<script>
    $(document).ready(function () {
        let url_filter = window.location.origin;
        let url = url_filter + '/admin/collaborator_financial/revenue/done';
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $("#show_content").html(data);
            },
            error: function () {
                console.log('error')
            }
        });

        //Đã thanh toán
        $(document).on("click", ".filter-done", function () {
            let url = url_filter + '/admin/collaborator_financial/revenue/done';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.filter-done').addClass('checked');
                    $('.filter-waiting').removeClass('checked');
                    $("#show_content").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        //Sẽ thanh toán
        $(document).on("click", ".filter-waiting", function () {
            let url = url_filter + '/admin/collaborator_financial/revenue/waiting';
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $('.filter-done').removeClass('checked');
                    $('.filter-waiting').addClass('checked');
                    $("#show_content").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
        });

        $(document).on("click", ".btn-active", function () {
            let url = $(this).attr('data-url');
            let data = {};
            data['id'] = $(this).attr('data-id');
            data['type'] = $(this).attr('data-type');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    showLoading();
                },
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        });
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            $('#myModal').modal('show');
        });


        //Lọc theo ngày
        function filter_date() {
            let url = $('.btn-filter-order').attr('data-url');
            let data = {};
            data['time_start'] = $('.time-start').val();
            data['time_end'] = $('.time-end').val();
            if (data['time_start'] == '' || data['time_end'] == '' || (data['time_start'] > data['time_end'])) {
                swal("Thông báo", "Vui lòng chọn khoảng thời gian phù hợp", "error");
                return false;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: data,
                success: function (data) {
                    $('tbody').html(data.table_data);
                },
                error: function () {
                    console.log('error')
                },
            });
        }
        $(document).on('click', '.btn-filter-order', function () {
            filter_date();
        })

        //Lọc theo trạng thái | Đã thanh toán
        $(document).on('change', '#status-done', function () {
            let status_done = $(this).val();
            let data = {};
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: 'admin/collaborator_financial/revenue/filter_done/' + status_done,
                type: "GET",
                dataType: 'json',
                data: data,
                success: function (data) {
                    $('tbody').html(data.table_data);
                },
                error: function () {
                    console.log('error')
                },
            });
        });

        //Lọc theo trạng thái | Chưa thanh toán
        $(document).on('change', '#status-waiting', function () {
            let status_waiting = $(this).val();
            let data = {};
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: 'admin/collaborator_financial/revenue/filter_waiting/' + status_waiting,
                type: "GET",
                dataType: 'json',
                data: data,
                success: function (data) {
                    $('tbody').html(data.table_data);
                },
                error: function () {
                    console.log('error')
                },
            });
        });
    });
</script>
<!-- END: Footer-->

</body>
<!-- END: Body-->

</html>
