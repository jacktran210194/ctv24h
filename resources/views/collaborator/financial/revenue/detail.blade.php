<?php
$page = 'collaborator_financial_revenue';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="pb-1">
                                <label for="">Mã đơn hàng: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? $dataReceived->order_code : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Tên khách hàng: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? $dataReceived->name_customer : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Địa chỉ: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? $dataReceived->Adds : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Số điện thoại: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? $dataReceived->phone_customer : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Mô tả: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? $dataReceived->note : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Hình thức thanh toán: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? $dataReceived->payment : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Trạng thái đơn hàng: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? $dataReceived->status : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Ngày đặt hàng: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? $dataReceived->created_at : '' ?></span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="pb-1">
                                <label for="">Tên sản phẩm: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? $dataReceived->name_product : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Mã SKU: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? $dataReceived->sku_type : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Kho hàng: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? $dataReceived->warehouse : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Loại sản phẩm: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? $dataReceived->product_type : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Số lượng: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? $dataReceived->quantity : '' ?></span>
                            </div>
                            <div class="pb-1">
                                <label for="">Giá bán: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? number_format($dataReceived->price) : '' ?> VNĐ</span>
                            </div>
                            <div class="pb-1">
                                <label for="">Giá gốc: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? number_format($dataReceived->price_discount) : '' ?> VNĐ</span>
                            </div>
                            <div class="pb-1">
                                <label for="">Lợi nhuận: </label>
                                <span class="text-bold-700"><?= isset($dataReceived) ? number_format($dataReceived->quantity*($dataReceived->price) - $dataReceived->quantity*($dataReceived->price_discount)) : '' ?> VNĐ</span>
                            </div>
                            <div class="pb-1 form-group">
                                <label for="">Hình ảnh: </label>
                                <br>
                                <img width="150px" height="150px" class="image-preview" src="<?= isset($dataReceived) ? $dataReceived->image : '' ?>" alt="">
                            </div>
                            <a href="<?= URL::to('admin/manage_financial/revenue') ?>" class="btn btn-danger" style="color: white"><i
                                    class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
<script src="dashboard/index.js"></script>
<script>
    $(document).on("change", "#video", function () {
        let value = $(this).val();
        value = value.replace('watch', 'embed');
        value = value.replace('?v=', '/');
        $("#youtube").attr('src', value);
    });

    $(document).on("click", ".btn-add-account", function () {
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('id', $(this).attr('data-id'));
        form_data.append('name', $('#name').val());
        form_data.append('date', $('#date').val());
        form_data.append('time_feedback', $('#time_feedback').val());
        form_data.append('report', $('#report').val());
        form_data.append('category_id', $('#category_id').val());
        form_data.append('video', $('#video').val());
        form_data.append('desc', CKEDITOR.instances['desc'].getData());
        form_data.append('image', $('#image')[0].files[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                showLoading();
            },
            success: function (data) {
                if (data.status) {
                    window.location.href = data.url;
                } else {
                    alert(data.msg);
                }
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>

