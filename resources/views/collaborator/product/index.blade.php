<?php
$page = 'collaborator_product';

use Illuminate\Support\Facades\URL;

?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')
<style>
    .popup-edit-product{
        opacity: 0;
        z-index: -100;
        transition: 1s;
    }
    .popup-edit-product.active{
        opacity: 1;
        z-index: 10000;
    }
    .switch {
        position: relative;
        display: inline-block;
        width: 50px;
        height: 20px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: -2px;
        bottom: -3px;
        background-color: #ffffff;
        box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #47AA04;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(30px);
        -ms-transform: translateX(30px);
        transform: translateX(30px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 25px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h2>{{$title}}</h2>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <!-- BEGIN: Tab Bar-->
        <div class="tab-bar-page">
            <ul class="tab-bar-content d-flex align-items-center justify-content-lg-start flex-wrap">
                <li class="d-flex align-items-center justify-content-lg-center active">
                    <a href="{{url('/')}}" data-value="all"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Tất cả</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center">
                    <a href="{{url('/')}}" data-value="0"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đang hoạt
                        động</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center">
                    <a href="{{url('/')}}" data-value="1"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Hết hàng</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center">
                    <a href="{{url('/')}}" data-value="2"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Vi phạm</a>
                </li>
                <li class="d-flex align-items-center justify-content-lg-center">
                    <a href="{{url('/')}}" data-value="3"
                       class="w-100 h-100 text-center d-flex align-items-center justify-content-lg-center">Đã ẩn</a>
                </li>
            </ul>
        </div>
        <!-- END: Tab Bar-->
        <div class="bg-white p-1">
            <div
                class="form-label-group has-icon-left position-relative w-25 d-flex justify-content-sm-between align-items-center">
                <input type="text" id="search" class="form-control" name="search" placeholder="Nhập tên sản phẩm...">
                <div class="form-control-position">
                    <i class="feather icon-search font-weight-bold color-red font-size-17"></i>
                </div>
            </div>
            <div class="d-flex justify-content-sm-between align-items-center mg-top-30 w-100">
                {{--                <p class="color-red font-size-24 font-weight-bold m-0">--}}
                {{--                    <span class="total-product">{{$data}}</span> Sản phẩm--}}
                {{--                </p>--}}
            </div>
            <div class="clearfix mb-2"></div>
            <div class="card">
                <div class="card-content">
                    <!-- content -->
                    <div class="table-responsive table-responsive-lg">
                        <table class="table data-list-view table-sm">
                            <thead>
                            <tr>
                                <th class="color-white font-weight-bold bg-red border-top-left border-bottom-left"
                                    scope="col">Tên sản phẩm
                                </th>
                                <th class="color-white font-weight-bold bg-red" scope="col">SKU phân loại</th>
                                <th class="color-white font-weight-bold bg-red">Phân loại hàng</th>
                                <th class="color-white font-weight-bold bg-red text-center">Giá</th>
                                <th class="color-white font-weight-bold bg-red text-center">Kho hàng</th>
                                <th class="color-white font-weight-bold bg-red text-center">Đã bán</th>
                                <th class="border-top-right border-bottom-right color-white font-weight-bold bg-red"
                                    scope="col">Thao tác
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($data_product_dropship && count($data_product_dropship))
                                @foreach($data_product_dropship as $value)
                                    @php $product_attribute = \App\Models\ProductAttributeModel::where('product_id', $value->id)->get(); @endphp
                                    <tr>
                                        <td>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <img src="{{$value->image}}" width="65px">
                                                <p class="m-0 font-weight-bold color-black" style="max-width: 200px;">{{$value->name}}</p>
                                            </div>
                                        </td>
                                        <td><span
                                                class="padding-10 font-weight-bold color-black">{{$value->sku_type}}</span>
                                        </td>
                                        <td>
                                            @foreach($product_attribute as $item)
                                                @php $attr = \App\Models\AttributeValueModel::where('attribute_id', $item->id)->get(); @endphp
                                                @foreach($attr as $v)
                                                    <div class="padding-10 color-gray font-size-14 text-left">
                                                        {{$item->category_1}} : {{$item->category_2}} / {{$v->size_of_value}}
                                                    </div>
                                                @endforeach
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($product_attribute as $item)
                                                @php $attr = \App\Models\AttributeValueModel::where('attribute_id', $item->id)->get(); @endphp
                                                @foreach($attr as $v)
                                                    <div class="padding-10 color-red font-weight-bold text-center">
                                                        {{number_format($v->price)}} đ
                                                    </div>
                                                @endforeach
                                            @endforeach
                                        </td>
                                        <td class="text-center text-bold-700">
                                            @foreach($product_attribute as $item)
                                                @php $attr = \App\Models\AttributeValueModel::where('attribute_id', $item->id)->get(); @endphp
                                                @foreach($attr as $v)
                                                    <div class="padding-10 m-0 text-center font-weight-bold font-size-14">
                                                        {{$v->quantity}}
                                                    </div>
                                                @endforeach
                                            @endforeach
                                        </td>
                                        <td class="text-center text-bold-700">
                                            @foreach($product_attribute as $item)
                                                @php $attr = \App\Models\AttributeValueModel::where('attribute_id', $item->id)->get(); @endphp
                                                @foreach($attr as $v)
                                                    <div class="padding-10 m-0 text-center font-weight-bold font-size-14">
                                                        {{$v->is_sell}}
                                                    </div>
                                                @endforeach
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center position-relative">
                                                @if($value->active_drop_ship == 0)
                                                    <button class="btn btn-popup-edit d-flex align-items-center" title="Sửa"><img src="../../assets/admin/app-assets/images/icons/edit_1.png">
                                                        <span class="color-blue font-size-14 font-weight-bold" style="margin:0 5px; text-decoration: underline; ">Sửa</span>
                                                        <input type="hidden" value="{{$value->id}}">
                                                    </button>
                                                    @else
                                                    <div>
                                                        <p class="font-weight-bold m-0">Đăng bán</p>
                                                        <div style="margin-top: 10px">
                                                            <label class="switch">
                                                                <input type="checkbox" name="status_product" value="{{$value->id}}" @if($value->active_drop_ship == 2) checked @endif>
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="1000">Không có dữ liệu</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="popup-edit-product d-flex justify-content-center align-items-center position-fixed w-100 h-100" style="top: 0; left: 0; background: #33333330;"></div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
@include('admin.base.modal')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).ready(function () {
        $(document).on("click", ".btn-delete", function () {
            let url = $(this).attr('data-url');
            $('.btn-confirm').attr('data-url', url);
            jQuery.noConflict();
            $('#myModal').modal('show');
        });
    });
</script>
<script src="../../assets/collaborator/product.js"></script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
