<?php
$page = 'manage_product';
use Illuminate\Support\Facades\URL;
?>
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('admin.base.head')

<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static data-total-shop"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col="2-columns"
    data-layout="semi-dark-layout"
>

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <h5><?= mb_strtoupper($title ?? '', 'utf-8') ?></h5>
                </div>
                @include('admin.base.header')
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
@include('admin.base.menu_collaborator')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-content">
                <div class="m-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Tên sản phẩm</label>
                                <input required type="text" name="name" id="name"
                                       value="<?= isset($data) ? $data['name'] : '' ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Đơn giá</label>
                                <input required type="number" name="price" id="price"
                                       value="<?= isset($data) ? $data['price'] : '' ?>"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label>SKU phân loại</label>
                                <input required type="text" name="sku_type" id="sku_type"
                                       value="<?= isset($data) ? $data['sku_type'] : '' ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Phân loại hàng</label>
                                <input required type="text" name="product_type" id="product_type"
                                       value="<?= isset($data) ? $data['product_type'] : '' ?>" class="form-control">
                            </div>


                            <div class="form-group">
                                <label>Danh mục sản phẩm</label>
                                <select class="form-control" name="category_id" id="category_id">
                                    @foreach($category as $value)
                                        <option @isset($data) @if($data['category_id'] == $value['id'])  selected
                                                @endif @endisset value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="form-label-group">
                                    <input type='file' value="" accept="image/*" name="image" id="image"
                                           onchange="changeImg(this)">
                                    <br>
                                    <img width="120px" height="120px" id="avatar" class="thumbnail image-show-qr"
                                         src="<?= isset($data) && File::exists(substr($data['image'], 1)) ? $data['image'] : '/uploads/images/avt.jpg' ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label>Mô tả sản phẩm</label>
                                <textarea class="form-control ckeditor" name="desc" id="desc" cols="30"
                                          rows="3"><?= isset($data) ? $data['desc'] : '' ?></textarea>
                            </div>

                            <div class="form-group">
                                <label>Trạng thái</label>
                                <select class="form-control" name="is_status" id="is_status">
                                    <option @isset($data) @if($data['is_status'] == 0) selected @endif @endisset value="0">Sản phẩm còn hàng</option>
                                    <option @isset($data) @if($data['is_status'] == 1) selected @endif @endisset value="1">Sản phẩm hết hàng</option>
                                    <option @isset($data) @if($data['is_status'] == 2) selected @endif @endisset value="2">Sản phẩm bị đánh vi phạm</option>
                                    <option @isset($data) @if($data['is_status'] == 3) selected @endif @endisset value="3">Sản phẩm bị ẩn</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Dropship</label>
                                <select class="form-control" name="" id="dropship">
                                    <option @isset($data) @if($data['dropship']==1) selected @endif @endisset value="1">Có</option>
                                    <option @isset($data) @if($data['dropship']==0) selected @endif @endisset value="0">Không</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Kho</label>
                                <input class="form-control" type="text" name="warehouse" id="warehouse" value="<?= isset($data) ? $data['warehouse'] : '' ?>">
                            </div>

                            <button type="button" class="btn btn-primary btn-add-account"
                                    data-id="<?= isset($data) ? $data['id'] : '' ?>"
                                    data-url="<?= URL::to('admin/manage_product/save') ?>"><i class="fa fa-floppy-o"
                                                                                       aria-hidden="true"></i> Lưu
                            </button>
                            <a href="<?= URL::to('admin/manage_product') ?>" class="btn btn-danger"><i
                                    class="fa fa-undo" aria-hidden="true"></i> Hủy bỏ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
@include('admin.base.footer')
@include('admin.base.script')
{{--<script src="dashboard/index.js"></script>--}}
<script>
    $(document).on("click", ".btn-add-account", function () {
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('id', $(this).attr('data-id'));
        form_data.append('name', $('#name').val());
        form_data.append('price', $('#price').val());
        form_data.append('sku_type', $('#sku_type').val());
        form_data.append('product_type', $('#product_type').val());
        form_data.append('category_id', $('#category_id').val());
        form_data.append('is_status', $('#is_status').val());
        form_data.append('warehouse', $('#warehouse').val());
        form_data.append('dropship', $('#dropship').val());
        form_data.append('desc', CKEDITOR.instances['desc'].getData());
        form_data.append('image', $('#image')[0].files[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                showLoading();
            },
            success: function (data) {
                if (data.status) {
                    window.location.href = data.url;
                } else {
                    alert(data.msg);
                }
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
</script>
<!-- END: Footer-->
</body>
<!-- END: Body-->
</html>
