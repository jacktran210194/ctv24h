<div class="bg-white w-50 p-2 border-radius-16 position-relative">
    <button type="button" class="close-popup color-red" style="position: absolute; top: 10px; right: 10px; border: none; background: transparent; font-size: 20px"><i class="fa fa-times"></i></button>
    <h2 class="text-center">Sửa giá sản phẩm đăng bán</h2>
    <form action="{{url('/admin/collaborator_product/update')}}" method="post" style="margin-top: 20px;">
        @csrf
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="product_id" value="{{ $product->id }}" />
        @php $index = 0; @endphp
        @foreach($product_attribute as $value)
            @php
                $attr = \App\Models\AttributeValueModel::where('attribute_id', $value->id)->get();
            @endphp
            @foreach($attr as $v)
                @php $a = $index++ @endphp
                <div class="d-flex justify-content-between align-items-center" style="margin-top: 10px">
                    <div class="name">
                        {{$value->category_1}} : {{$value->category_2}} / {{$v->size_of_value}}
                    </div>
                    <div class="d-flex justify-content-between align-items-center content-price">
                        <input type="hidden" value="{{$v->id}}" name="attribute[{{$a}}][id_attr]">
                        <div class="w-50 position-relative" style="padding: 5px 10px">
                            <input type="number" value="{{$v->price}}" min="{{$v->price}}" name="attribute[{{$a}}][price]" required class="form-control input_price">
                            <p class="position-absolute m-0 font-size-12" style="top: 50%; right: 15px; transform: translate(-50%, -50%); width: fit-content">Giá</p>
                        </div>
                        <div class="w-50 position-relative" style="padding: 5px 10px">
                            <p class="position-absolute m-0 font-size-12" style="top: 50%; right: 3px; transform: translate(-50%, -50%); width: fit-content">Giá CTV</p>
                            <input type="number" value="{{$v->price_ctv}}" min="{{$v->price_ctv}}" name="attribute[{{$a}}][price_ctv]" required class="form-control input_price_ctv">
                        </div>
                    </div>
                </div>
            @endforeach
        @endforeach
        <div class="d-flex justify-content-center align-items-center" style="margin-top: 20px">
            <button type="submit" class="border-radius-8 bg-red color-white" style="border: none; padding: 10px 20px">Chỉnh sửa</button>
        </div>
    </form>
</div>
