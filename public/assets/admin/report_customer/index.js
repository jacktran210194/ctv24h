let $primary = '#7367F0';
let $success = '#28C76F';
let $danger = '#EA5455';
let $warning = '#FF9F43';
let $info = '#00cfe8';
let $danger_light = '#f29292';
let $success_light = '#55DD92';
let $info_light = '#1fcadb';
let $strok_color = '#b9c3cd';
let $label_color = '#e7e7e7';
let $white = '#fff';


$(document).ready(function () {


    // tổng số cửa hàng
    // ----------------------------------
    let data_logs = JSON.parse($('.data-total-shop').attr('data-logs'));
    let $category = data_logs.time;
    let $data_log = data_logs.data;
    let $data_log_render = loadOption($success_light, $success, $category, $data_log, '#line-area-chart-total-logs', 'Số lượt truy cập');
    $data_log_render.render();

    let customer = data_logs.customer;
    let customer_render = loadOption($info_light, $info, $category, customer, '#line-area-chart-total-customer', 'Số khách hàng');
    customer_render.render();

    $(document).on('change', '.custom-select', function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['city'] = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            dataType: 'json',
            data: data,
            beforeSend: function () {
                showLoading();
            },
            success: async function (data) {

                let data_logs = data.data_logs_user;

                $('.total-logs').text(data_logs.total);
                $data_log_render.updateSeries([{
                    name: 'Số lượt truy cập',
                    data: data_logs.data
                }]);

                $('.total-customer').text(data_logs.total_customer);
                customer_render.updateSeries([{
                    name: 'Số khách hàng',
                    data: data_logs.customer
                }]);

                let data_category_service = data.data_category_service;

                let tr;
                $.each(data_category_service, function (index, value) {

                    tr += '<tr>\n' +
                        '<th scope="row">' + value['name'] + '</th>\n' +
                        '<td><img class="image-preview" src="'+ value['image'] +'" width="50px"></td>\n' +
                        '<td>' + value['view'] + '</td>\n' +
                        '<td>' + value['count'] + '</td>\n' +
                        '</tr>';
                });

                if (!tr) {
                    tr = '<tr><td>Không có dữ liệu</td></tr>';
                }

                $('.table-category-service').html(tr);
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
});

function loadOption($color_light, $color, $categories, $data, $document_id, $name) {
    let options = {
        chart: {
            height: 270,
            toolbar: {show: false},
            type: 'line',
        },
        stroke: {
            curve: 'smooth',
            dashArray: [0, 8],
            width: [4, 2],
        },
        grid: {
            borderColor: $label_color,
        },
        legend: {
            show: false,
        },
        colors: [$color_light],

        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                inverseColors: false,
                gradientToColors: [$color],
                shadeIntensity: 1,
                type: 'horizontal',
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100, 100, 100]
            },
        },
        markers: {
            size: 0,
            hover: {
                size: 5
            }
        },
        xaxis: {
            labels: {
                style: {
                    colors: $strok_color,
                }
            },
            axisTicks: {
                show: false,
            },
            categories: $categories,
            axisBorder: {
                show: false,
            },
            tickPlacement: 'on',
        },
        yaxis: {
            tickAmount: 5,
            labels: {
                style: {
                    color: $strok_color,
                },
                formatter: function (val) {
                    return val > 999 ? (val / 1000).toFixed(1) + 'k' : val;
                }
            }
        },
        tooltip: {
            x: {show: false}
        },
        series: [
            {
                name: $name,
                data: $data
            }
        ]

    };

    let LineChart = new ApexCharts(
        document.querySelector($document_id),
        options
    );

    return LineChart;
}

function optionRevenue($banner, $store_extension, $highlight) {
    let category = JSON.parse($('.data-total-shop').attr('data-month2'));
    let revenueChartoptions = {
        chart: {
            height: 270,
            toolbar: {show: false},
            type: 'line',
        },
        stroke: {
            curve: 'smooth',
            dashArray: [0, 8],
            width: [4, 2],
        },
        grid: {
            borderColor: $label_color,
        },
        legend: {
            show: false,
        },
        colors: [$danger_light, $strok_color, $info_light],

        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                inverseColors: false,
                gradientToColors: [$primary, $strok_color, $info],
                shadeIntensity: 1,
                type: 'horizontal',
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100, 100, 100]
            },
        },
        markers: {
            size: 0,
            hover: {
                size: 5
            }
        },
        xaxis: {
            labels: {
                style: {
                    colors: $strok_color,
                }
            },
            axisTicks: {
                show: false,
            },
            categories: category,
            axisBorder: {
                show: false,
            },
            tickPlacement: 'on',
        },
        yaxis: {
            tickAmount: 5,
            labels: {
                style: {
                    color: $strok_color,
                },
                formatter: function (val) {
                    return val > 999 ? (val / 1000).toFixed(1) + 'k' : val;
                }
            }
        },
        tooltip: {
            x: {show: false}
        },
        series: [
            {
                name: "Banner",
                data: $banner
            },
            {
                name: "Gia hạn cửa hàng",
                data: $store_extension
            },
            {
                name: "Cửa hàng nổi bật",
                data: $highlight
            }
        ],

    };

    let revenueChart = new ApexCharts(
        document.querySelector("#revenue-chart"),
        revenueChartoptions
    );

    return revenueChart;
}

