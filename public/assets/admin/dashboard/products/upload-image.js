$(document).ready(function () {
    let index_element, type;
    let id_product = $("#id_product").val();
   $(".icon_update").click(function () {
       let parent = $(this).parent();
       let html = '<img src="../../assets/admin/app-assets/images/icons/add_image.png">';
       let img = parent.find("img");
       img.remove();
       parent.append(html);
       parent.find(".upload_image").val("");
       $(this).css({
           "opacity" : 0,
           "z-index" : -10
       });
       setTimeout(function () {
           parent.addClass("btn-add-list-image");
       }, 300);
   });
   $(document).on("click", ".group-image .btn-add-list-image", function () {
       index_element = $(this).index();
       type = 2;
       $(".input_upload_image").click();
   });
   $(".btn-image-1").click(function () {
       type = 1;
       $("#image").click();
   });
    $(document).on("change", ".input_upload_image", function () {
        uploadImage(this)
    });
    $(document).on("change", "#image", function () {
        uploadImage(this)
    });
    function uploadImage(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                let formData = new FormData();
                formData.append('file', input.files[0]);
                formData.append('id_product', id_product);
                formData.append('type', type);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url : window.location.origin + '/update-image-variant',
                    data: formData,
                    type: 'POST',
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.status){
                            switch (data.type) {
                                case 1:
                                    $(".btn-image-1 img").attr("src", data.src);
                                    $('input[name="image_product"]').val(data.src);
                                    break;
                                case 2:
                                    let html = '<img class="position-absolute border-radius-8 image-variant" style="width: 100%; height: 100%; top: 0; left: 0; object-fit: cover" src="'+ data.src +'" >';
                                    $(".show-list-image").eq(index_element - 2).append(html);
                                    $(".show-list-image").eq(index_element - 2).removeClass("btn-add-list-image");
                                    $(".show-list-image").eq(index_element - 2).find("label").css({
                                        'opacity' : 1,
                                        'z-index' : 10
                                    });
                                    $(".show-list-image").eq(index_element - 1).find(".upload_image").val(data.src);
                                    break;
                            }
                        }else {
                            swal('Thông báo', 'Đã có lỗi xảy ra', 'error');
                        }
                    }
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".btn-delete-list").click(function () {
        let parent = $(this).closest(".list-container-category");
        let data ={};
        data['type'] = 1;
        data['id'] = parent.attr("data-value");
        addDelete(window.location.origin +'/add-delete-variant', data);
    });
    $(".btn-delete-content").click(function () {
        let parent = $(this).closest("[content_value]");
        let data ={};
        data['type'] = 2;
        data['id'] = parent.attr("data-value");
        addDelete(window.location.origin +'/add-delete-variant', data);
    });
    $(".btn-delete-percent").click(function () {
        let parent = $(this).closest(".content_list");
        let data = {};
        data['type'] = 3;
        data['id'] = parent.attr("data-value");
        addDelete(window.location.origin +'/add-delete-variant', data);
    });

    function addDelete(url, data_id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           url: url,
           data: data_id,
           type: 'POST',
           dataType: "json",
           success:function (data) {
               if (data.status){
                   console.log("success")
               }
           } ,
            error:function () {
                console.log("error");
            }
        });
    }
});
