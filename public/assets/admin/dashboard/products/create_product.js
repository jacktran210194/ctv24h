$(document).ready(function () {
    let form_data = new FormData();
    let category = null;
    let category_1 = null;
    let category_2 = null;
    let name_product, brand_product, sku_product, video;
    let index;
    $(document).on("click", ".btn-delete-image", function () {
        let url = $(this).attr('data-url');
        $('.btn-confirm').attr('data-url', url);
        $('.message-modal').text($(this).attr('data-msg'));
        jQuery.noConflict();
        $('#myModal').modal('show');
    });
    $(document).on("click", ".btn-delete-item", function () {
        let url = $(this).attr('data-url');
        $('.btn-confirm').attr('data-url', url);
        $('.message-modal').text($(this).attr('data-msg'));
        jQuery.noConflict();
        $('#myModal').modal('show');
    });

    $(document).on("click", ".btn-delete-attr", function () {
        let url = $(this).attr('data-url');
        $('.btn-confirm').attr('data-url', url);
        $('.message-modal').text($(this).attr('data-msg'));
        jQuery.noConflict();
        $('#myModal').modal('show');
    });

    $(document).on("click", ".btn-remove", function () {
        let index = $(".btn-remove").index(this);
        let values = form_data.getAll("img[]");
        values.splice(index, 1);
        form_data.delete("img[]");
        for (let i = 0; i < values.length; i++) {
            form_data.append("img[]", values[i]);
        }
        $(this).parent(".pip").remove();
    });
    $(document).on('click', '.btn-add-group', function (e) {
        e.preventDefault();
        let url = $(this).attr('data-url');
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'json',
            success: function (data) {
                $(".more-email").append(data.html);
            },
            error: function (error) {
                console.log(error)
            },
        });
    });
    $(document).on('click', '.btn-add-value', function () {
        let url = $(this).attr('data-url');
        let dom = $(this).parents('.form_attr:first').find('.form-1');
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'json',
            success: function (data) {
                dom.append(data.html);
            },
            error: function (error) {
                console.log(error)
            },
        });
    });

    $(document).on('click', '.btn_add_shipping', function () {
        let checked = [];
        $("[name='shipping_supplier[]']").each(function () {
            if ($(this).is(":checked")) {
                checked.push($(this).attr('data-id'));
            }
        });
        $('.btn-add-account').attr('data-shipping', checked);
        $('.btn-save-hide').attr('data-shipping', checked);
    });
    $(document).on("click", ".category-child-1 li", function () {
        category_1 = $(this).attr("data-value");
        $('.category-child-1 li').removeClass("active");
        $(this).addClass("active");
        let url = window.location.origin + '/admin/product_supplier/category_child';
        let data = {};
        data['category_id'] = $(this).attr("data-value");
        $('.category-child-2').html("");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (data) {
                if (data.status) {
                    for (const element of data.category) {
                        let li = '<li data-value=" ' + element.id + ' " data-name="' + element.name + '">' + element.name + '</li>';
                        $('.category-child-2').append(li);
                    }
                    $(".btn-next").prop("disabled", true);
                }
            }
        });
    });
    $(document).on("click", ".category-child-2 li", function () {
        $('.category-child-2 li').removeClass("active");
        $(this).addClass("active");
        let text = $(this).attr("data-name");
        $(".name-category-2").text(text);
        $(".btn-next").prop("disabled", false);
        category_2 = $(this).attr("data-value");
    });
    $(document).on("blur", ".input_percent", function () {
        let value = parseInt($(this).val());
        if (value > 50 || isNaN(value)){
            swal('Thông báo', 'Giảm giá không được lớn hơn 50%', 'error');
            $(this).val("");
        }
    });
    $(document).on("click", ".btn-add-account", function () {
        if ($('#price_product').val() < 0) {
            swal('Thông báo', 'Giá sản phẩm không được nhỏ hơn 0', 'warning');
            return false;
        }
        if ($('#weight').val() == '') {
            swal('Thông báo', 'Cân nặng không được để trống !', 'warning');
            return false;
        }
        let attr_category = $('.title-group-catagory').val();
        let content_category = $('.content-group-catagory').val();
        let content_group_category = $('.content-group-catagory-2').val();
        let input_price = $('.input-price').val();
        let input_inventory = $('.input-inventory').val();
        if (attr_category == '' || content_category == '' || content_group_category == '' || input_price == '' || input_inventory == '') {
            swal('Thông báo', 'Vui lòng nhập đủ phân loại cho sản phẩm !', 'warning');
            return false;
        }
        if (input_price < 0) {
            swal('Thông báo', 'Giá sản phẩm không được nhỏ hơn 0', 'warning');
            return false;
        }
        let url = $(this).attr('data-url');
        let shipping = $(this).attr('data-shipping');
        if (shipping == undefined) {
            swal('Thông báo', 'Vui lòng chọn ít nhất 1 đơn vị vận chuyển', 'warning');
            return false;
        }
        let active = 1;
        let attribute_item = [];
        let check = true;
        $("[list-category]").each(function () {
            let content_list = $(this).find("[content_value]");
            let _arrcontent = [];
            if ( $(this).find(".input_title").val() == "" || $(this).find(".input_value").val() == ""
                || $(this).find(".list-data-category .input_title").val() == "" || $(this).find('input[name="value_image"]').val() == ""
            ){
                check = false;
                return false;
            }
            content_list.each(function () {
                if ( $(this).find(".input_name").val() == "" || $(this).find(".input_price").val() == "" || $(this).find(".input_price_ctv").val() == "" || $(this).find(".input_quantity").val() == "" ){
                    check = false;
                    return false;
                }
                let value = {
                    "name": $(this).find(".input_name").val(),
                    "price" : $(this).find(".input_price").val().replace(/[$,]+/g,""),
                    "price_ctv" : $(this).find(".input_price_ctv").val().replace(/[$,]+/g,""),
                    "quantity" : $(this).find(".input_quantity").val()
                };
                _arrcontent.push(value);
            });
            let arr = {
                'title': $(this).find(".input_title").val(),
                'name': $(this).find(".input_value").val(),
                'title_value' : $(this).find(".list-data-category .input_title").val(),
                'image_variant': $(this).find('input[name="value_image"]').val(),
                'value': _arrcontent
            };
            attribute_item.push(arr);
        });
        if (!check){
            swal('Thông báo', 'Vui lòng điền thông tin đầy đủ', 'error');
            return false;
        }
        let buy_more_percent = [];
        $(".list-buy-percent .content_list").each(function () {
            let buy_percent = {
                'quantity_1' : $(this).find(".input_quantity_1").val(),
                'quantity_2' : $(this).find(".input_quantity_2").val(),
                'percent' : $(this).find(".input_percent").val()
            }
            buy_more_percent.push(buy_percent);
        });


        form_data.append('buy_more_percent', JSON.stringify(buy_more_percent));
        form_data.append('id', $(this).attr('data-id'));
        form_data.append('supplier_id', $(this).attr('data-supplier'));
        form_data.append('name', $('#name').val());
        form_data.append('sku_type', $('#sku_type').val());
        form_data.append('video', video);
        form_data.append('category_id', category_1);
        form_data.append('category_2', category_2);
        form_data.append('brand', $('#brand').val());
        form_data.append('is_status', $('#is_status').val());
        form_data.append('warehouse', $('#warehouse').val());
        form_data.append('is_desc', CKEDITOR.instances['desc'].getData());
        form_data.append('weight', $('#weight').val());
        form_data.append('height', $('#height').val());
        form_data.append('length', $('#length').val());
        form_data.append('width', $('#width').val());
        form_data.append('original', $('#original').val());
        form_data.append('material', $('#material').val());
        form_data.append('image', $('#image')[0].files[0]);
        form_data.append('attr', JSON.stringify(attribute_item));
        form_data.append('sort_desc', CKEDITOR.instances['sort_desc'].getData());
        form_data.append('category_shop', $('#category_shop').val());
        form_data.append('place', $('#place').val());
        form_data.append('status_product', $('#status_product').val());
        form_data.append('shipping', JSON.stringify(shipping));
        form_data.append('active', active);
        form_data.append('category_product', category);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status) {
                    window.location.href = data.url;
                } else {
                    swal('Thông báo', data.msg, 'error');
                }
            },
            error: function () {
                console.log('error')
            },
        });
    });
    $(".btn-save-hide").click(function () {
        let check_attr = $('.check_attr').is(':checked') ? 1 : 0;
        let url = window.location.origin + '/admin/product_supplier/save';
        let shipping = $(this).attr('data-shipping');
        if (shipping == undefined) {
            swal('Thông báo', 'Vui lòng chọn ít nhất 1 đơn vị vận chuyển', 'warning');
            return false;
        }
        let active = 1;
        let attribute_item = [];
        let check = true;
        $("[list-category]").each(function () {
            let content_list = $(this).find("[content_value]");
            let _arrcontent = [];
            if ( $(this).find(".input_title").val() == "" || $(this).find(".input_value").val() == ""
                || $(this).find(".list-data-category .input_title").val() == "" || $(this).find('input[name="value_image"]').val() == ""
            ){
                check = false;
                return false;
            }
            content_list.each(function () {
                if ( $(this).find(".input_name").val() == "" || $(this).find(".input_price").val() == "" || $(this).find(".input_price_ctv").val() == "" || $(this).find(".input_quantity").val() == "" ){
                    check = false;
                    return false;
                }
                let value = {
                    "name": $(this).find(".input_name").val(),
                    "price" : $(this).find(".input_price").val(),
                    "price_ctv" : $(this).find(".input_price_ctv").val(),
                    "quantity" : $(this).find(".input_quantity").val()
                };
                _arrcontent.push(value);
            });
            let arr = {
                'title': $(this).find(".input_title").val(),
                'name': $(this).find(".input_value").val(),
                'title_value' : $(this).find(".list-data-category .input_title").val(),
                'image_variant': $(this).find('input[name="value_image"]').val(),
                'value': _arrcontent
            };
            attribute_item.push(arr);
        });
        if (!check){
            swal('Thông báo', 'Vui lòng điền thông tin đầy đủ', 'error');
            return false;
        }
        form_data.append('id', $(this).attr('data-id'));
        form_data.append('supplier_id', $(this).attr('data-supplier'));
        form_data.append('name', $('#name').val());
        form_data.append('sku_type', $('#sku_type').val());
        form_data.append('video', video);
        form_data.append('category_id', category_1);
        form_data.append('category_2', category_2);
        form_data.append('brand', $('#brand').val());
        form_data.append('is_status', $('#is_status').val());
        form_data.append('warehouse', $('#warehouse').val());
        form_data.append('is_desc', CKEDITOR.instances['desc'].getData());
        form_data.append('weight', $('#weight').val());
        form_data.append('height', $('#height').val());
        form_data.append('length', $('#length').val());
        form_data.append('width', $('#width').val());
        form_data.append('original', $('#original').val());
        form_data.append('material', $('#material').val());
        form_data.append('image', $('#image')[0].files[0]);
        form_data.append('attr', JSON.stringify(attribute_item));
        form_data.append('sort_desc', CKEDITOR.instances['sort_desc'].getData());
        form_data.append('category_shop', $('#category_shop').val());
        form_data.append('place', $('#place').val());
        form_data.append('status_product', $('#status_product').val());
        form_data.append('shipping', JSON.stringify(shipping));
        form_data.append('active', active);
        form_data.append('category_product', category);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status) {
                    window.location.href = data.url;
                } else {
                    swal('Thông báo', data.msg, 'error');
                }
            },
            error: function () {
                console.log('error')
            },
        });
    });

    function readURL(input) {
        let a = index - 1;
        if (input.files) {
            let filesAmount = input.files.length;
            if (filesAmount > 8) {
                swal('Thông báo', "Không tải quá 8 file ảnh !", 'error');
            } else {
                if (filesAmount == 1) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        let file = e.target;
                        let html = '<img class="position-absolute border-radius-8" style="width: 100%; height: 100%; top: 0; left: 0" src="' + e.target.result + '" title="' + file.name + '" >.';
                        $('.btn-show-list').eq(a).html(html);
                    };
                    form_data.append('img[]', $('#imageQr')[0].files[0]);
                    reader.readAsDataURL(input.files[0]);
                }
                for (let i = a; i < (filesAmount + a); i++) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        let file = e.target;
                        let html = '<img class="position-absolute border-radius-8" style="width: 100%; height: 100%; top: 0; left: 0; object-fit: cover" src="' + e.target.result + '" title="' + file.name + '" >.';
                        $('.btn-show-list').eq(i).html(html);
                    };
                    form_data.append('img[]', $('#imageQr')[0].files[i]);
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }
    }

    $(".btn-show-list").click(function () {
        index = $(this).index();
    });
    $(document).on("change", "#imageQr", function () {
        readURL(this);
    });
    $('.content-category li').click(function () {
        category = $(this).attr("data-value");
        $('.content-category li').removeClass("active");
        $(this).addClass("active");
        let url = window.location.origin + '/admin/product_supplier/category';
        let data = {};
        data['category_id'] = $(this).attr("data-value");
        $('.category-child-1').html("");
        $('.category-child-2').html("");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (data) {
                if (data.status) {
                    for (const element of data.category) {
                        let li = '<li class="before" data-value=" ' + element.id + ' " data-name="' + element.name + '">' + element.name + '</li>';
                        $('.category-child-1').append(li);
                    }
                }
                $(".btn-next").prop("disabled", true);
            }
        });
    });
    $('.btn-next').click(function () {
        name_product = $('#name').val();
        brand_product = $('#brand').val();
        sku_product = $('#sku_type').val();
        // let specialChars = "<>@!#$%^&*_+[]{}?:;|'\"\\/~`=";
        let check = function (string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true;
                }
            }
            return false;
        };
        if (name_product.length < 5 || name_product.length > 120) {
            swal('Thông báo', 'Tên sản phẩm nhập đủ từ 5 - 120 ký tự !', 'warning');
        } else if (name_product == '' || brand_product == '') {
            swal('Thông báo', "Vui lòng nhập đủ thông tin", 'warning');
        } else {
            $('.information-sales').css("display", "block");
            $('.head_form').hide();
        }
    });

    $(document).on('blur', '#video_product', function () {
        let value = $(this).val();
        value = value.replace('watch', 'embed');
        value = value.replace('?v=', '/');
        video = value;
        let html = '<iframe width="560" height="315" src="' + value + '" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
        $(".video-product").html(html);
    });
});
