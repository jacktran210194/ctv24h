$(document).ready(function () {
    $(document).on("click", ".btn-add-list" , function () {
       let parents = $(this).closest('[list-category]');
       let parent = $(this).parent();
       let attr = parent.find(".input_title").attr("name");
       // let string = attr.replace("[title]", "");
       let html = '<div class="d-flex justify-content-between align-items-center" content_value style="margin-top: 20px">' +
                    '<input class="form-control input_name" style="max-width: 300px; margin-right: 5px;" type="text" placeholder="Nhập phân loại, ví dụ: S, M, v.v">' +
                    '<input class="form-control input_price" style="max-width: 300px; margin-right: 5px;" type="text" placeholder="giá">' +
                    '<input class="form-control input_price_ctv" style="max-width: 300px; margin-right: 5px;" type="text" placeholder="giá ctv">' +
                    '<input class="form-control input_quantity" style="max-width: 300px; margin-right: 5px;" type="number" placeholder="tồn kho">' +
                    '<button class="bg-red font-weight-bold font-size-17 color-white text-center border-radius-4 btn-delete-content" style="padding: 8px; border: none; width: 150px"><i class="fa fa-trash-o"></i></button>'
                   +'</div>';
       parents.find(".content-list").append(html);
   });
   $(document).on("click", ".btn-delete-content", function () {
      let parent = $(this).closest(".list-data-category");
      let parents = $(this).closest("[list-category]");
      let length = parents.find(".list-container-category").length;
      let count_element = length - 1;
      let attr = 'attr['+ count_element +'][category_1]';
      $(this).parent().remove();
      let index = 0;
      let children = parent.children();
      children.each(function () {
          let a = index++;
          let title = attr + '[sub]['+a+'][title]';
          let name = attr + '[sub]['+a+'][name]';
          let price = attr + '[sub]['+a+'][price]';
          let price_ctv = attr + '[sub]['+a+'][price_ctv]';
          let quantity = attr + '[sub]['+a+'][quantity]';
          $(this).find(".input_title").attr("name", title);
          $(this).find(".input_name").attr("name", name);
          $(this).find(".input_price").attr("name", price);
          $(this).find(".input_price_ctv").attr("name", price_ctv);
          $(this).find(".input_quantity").attr("name", quantity);
      });
   });
   $(".btn-add-list-attr").click(function () {
       let element = $(this).prev();
       let parent = element.find("[list-category]");
       let value_title = parent.find(".list-container-category .input_title").val();
       let value_size = parent.find(".list-data-category .input_title").val();
       let title, place;
       if (value_title == ""){
           title = 'placeholder="Nhập tên Nhóm phân loại hàng, ví dụ: màu sắc, kích thước v.v"';
       }else {
           title = 'value="'+value_title+'"';
       };
       if (value_size == ""){
           place = 'placeholder="Nhập tên phân loại, ví dụ: Size, v.v"';
       }else {
           place = 'value="'+value_size+'"';
       }
       let html = '<div list-category>' +
                    '<div class="d-flex align-items-center list-container-category" style="margin-bottom: 20px">'+
                        '<input class="form-control input_title" type="text" '+ title +' style="max-width: 250px; margin-right: 10px;">'+
                        '<input class="form-control input_value" type="text" placeholder="Nhập phân loại hàng, ví dụ: Trắng, Đỏ v.v" style="max-width: 250px; margin-right: 10px;"">'+
                        '<input type="hidden" name="value_image" value="">' +
                        '<button class="color-white font-weight-bold bg-red border-radius-4 btn-add-list" style="padding: 8px; border: none; margin-right: 10px; width: 200px"><span style="margin-right: 10px;">Thêm PL</span> <i class="fa fa-plus"></i></button>'+
                        '<button class="bg-red font-weight-bold font-size-17 color-white text-center border-radius-4 btn-delete-list" style="padding: 8px; border: none; width: 150px"><i class="fa fa-trash-o"></i></button>'
                    +'</div>' +
                    '<div data-content-category class="list-data-category">'+
                        '<div class="d-flex justify-content-between align-items-center" style="margin-bottom: 20px">'+
                            '<div style="width: 300px;">' +
                                '<input class="form-control input_title" style="margin-right: 5px;" type="text" '+ place +'>'+
                            '</div>'+
                            '<div class="content-list" style="padding-left: 10px; margin-left: 10px; border-left: 1px solid #DDDDDD">'+
                                '<div class="d-flex justify-content-between align-items-center" content_value>' +
                                '<input class="form-control input_name" style="max-width: 300px; margin-right: 5px;" type="text" placeholder="Nhập phân loại, ví dụ: S, M, v.v">'+
                                '<input class="form-control input_price" style="max-width: 300px; margin-right: 5px;" type="text" placeholder="giá tham khảo">'+
                                '<input class="form-control input_price_ctv" style="max-width: 300px; margin-right: 5px;" type="text" placeholder="giá bán ctv">'+
                                '<input class="form-control input_quantity" style="max-width: 300px; margin-right: 5px;" type="number" placeholder="tồn kho">'+
                                '<div style="width: 220px"></div>'+
                                '</div>' +
                            '</div>'+
                        '</div>'
                    +'</div>'
                  +'</div>';
       $('[data-category]').append(html);
       //---------------------------add image variant --------------//
       let popup = '<div class="content_button" style="width: 120px;margin-right: 15px;">' +
                        '<button class="btn w-100 btn-add-image position-relative border-radius-8 border_box d-flex justify-content-lg-center align-items-center" style="height: 120px; border: 1px solid #ECECEC;">' +
                            '<img src="../../assets/admin/app-assets/images/icons/add_image.png">' +
                        '</button>'+
                        '<p class="font-weight-bold color-black font-size-17 text-center" style="margin-top: 10px; margin-bottom: 0">Phân loại hàng</p>' +
                    '</div>';
       $("[list-image-variant]").append(popup);
   });

   $(document).on("change", ".list-data-category .input_title", function () {
      let parent = $(this).closest("[data-category]");
      let value = $(this).val();
      let child = parent.find("[list-category]");
      child.each(function () {
          $(this).find(".list-data-category .input_title").val(value);
      });
   });

   $(document).on("change", ".list-container-category .input_title", function () {
       let parent = $(this).closest("[data-category]");
       let value = $(this).val();
       let child = parent.find('[list-category]');
       child.each(function () {
          $(this).find(".list-container-category .input_title").val(value);
       });
   });

   $(document).on("change", ".input_price_ctv", function () {
      let value_next = parseInt($(this).prev().val().replace(/[$,]+/g,""));
      let value = parseInt($(this).val().replace(/[$,]+/g,""));
      if (value > value_next){
          swal('Thông báo', 'giá ctv không được lớn hơn giá phân loại hàng', 'error');
          $(this).val("");
      }
   });
   $(document).on("change", ".input_price", function () {
      let value_next = parseInt($(this).next().val().replace(/[$,]+/g,""));
      let value = parseInt($(this).val().replace(/[$,]+/g,""));
      if (value < value_next){
          swal('Thông báo', 'giá ctv không được lớn hơn giá phân loại hàng', 'error');
          $(this).val("");
          $(this).next().val("");
      }
   });

   $(document).on("click", ".btn-delete-list", function () {
       let parent = $(this).closest("[list-category]");
       let index = parent.index();
       $('[list-image-variant] .content_button').eq(index - 1).remove();
       parent.remove();
   });
   $(document).on("change", ".list-container-category .input_value", function () {
      let length = $(this).closest("[list-category]");
      let value = $(this).val();
      let parent = $("[list-image-variant] div").eq(length.index());
      parent.find("p").text(value);
   });
    let index_element;
   $(document).on("click", ".btn-add-image", function () {
       let parent = $(this).parent();
       index_element = parent.index();
       $("#input_image_2").trigger('click');
   });
   $(document).on("change", ".input_image_2", function () {
       let input = this;
       if (input.files && input.files[0]) {
           let reader = new FileReader();
           reader.onload = function (e) {
               let formData = new FormData();
               formData.append('file', input.files[0]);
               $.ajaxSetup({
                   headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   }
               });
               $.ajax({
                   url : window.location.origin + '/update-image',
                   data: formData,
                   type: 'POST',
                   dataType: 'json',
                   contentType: false,
                   processData: false,
                   success: function (data) {
                       if (data.status){
                           let html = '<img class="position-absolute border-radius-8" style="width: 100%; height: 100%; top: 0; left: 0; object-fit: cover" src="'+ data.src +'" >';
                           $(".content_button").eq(index_element - 1).find(".btn-add-image").html(html);
                           $("[list-category]").eq(index_element - 1).find('input[name="value_image"]').val(data.src);
                       }else {
                           swal('Thông báo', 'Đã có lỗi xảy ra', 'error');
                       }
                   }
               });
           }
           reader.readAsDataURL(input.files[0]);
       }
   });

   $(".btn-add-percent").click(function () {
       let parents = $(this).closest(".list-buy-percent");
       let index = parents.find(".content_list").length;
       let quantity_1 = $(".list-buy-percent .content_list").eq(index - 1).find(".input_quantity_1").val();
       let quantity_2 = $(".list-buy-percent .content_list").eq(index - 1).find(".input_quantity_2").val();
       let percent = $(".list-buy-percent .content_list").eq(index - 1).find(".input_percent").val();
       if (quantity_1 == "" || quantity_2 == "" || percent == ""){
           swal('Thông báo', 'Vui lòng điền thông tin', 'error');
       }else {
           let count = $(".list-buy-percent .content_list").length;
           if (count >= 5){
               swal('Thông báo', 'Không quá 5 mức giảm giá', 'error');
           }else {
               let a = parseInt(quantity_2) + 1;
               let html = '<div class="content_list d-flex justify-content-between align-items-center" style="margin-top: 20px">\n' +
                   '                                            <input class="form-control w-25 input_quantity_1" type="number" value="'+ a +'" disabled style="margin-right: 10px">\n' +
                   '                                            <input class="form-control w-25 input_quantity_2" type="number" style="margin-right: 10px">\n' +
                   '                                            <div class="w-25 position-relative" style="margin-right: 10px"><input class="form-control input_percent" type="number"><label class="position-absolute m-0 label_percent">%</label></div>\n' +
                   '                                            <button class="form-control btn-delete-percent bg-red w-25 color-white font-weight-bold" style="border: none">\n' +
                   '                                                <i class="fa fa-trash-o"></i>\n' +
                   '                                                <span style="padding-left: 5px">Xóa</span>\n' +
                   '                                            </button>\n' +
                   '                                        </div>';
               $(".list-buy-percent").append(html);
           }
       }
   });

   $(document).on("click", ".btn-delete-percent", function () {
      let parent = $(this).parent();
      let value = parseInt(parent.find(".input_quantity_1").val());
      let index = parent.index() + 1;
      $(".list-buy-percent .content_list").each(function () {
          if ($(this).index() === index ){
              $(this).find(".input_quantity_1").val(value);
              $(this).find(".input_quantity_2").val(null);
              $(this).find(".input_percent").val(null);
          }else if ( $(this).index() > parent.index() ){
              $(this).find(".input_quantity_2").val(null);
              $(this).find(".input_quantity_1").val(null);
              $(this).find(".input_percent").val(null);
          }
      });
      parent.remove();
   });
   $(document).on("change", ".input_quantity_2", function () {
      let value = parseInt($(this).val());
      let parent = $(this).parent();
      if (value <= parseInt($(this).prev().val())){
          alert("số lượng sản phẩm 2 bắt buộc phải lớn hơn số lượng sản phẩm 1");
          $(this).val("");
      }else {
         let content = parent.next();
         content.find(".input_quantity_1").val(value + 1);
      }
   });

   $(document).on("blur", ".form-control.input_quantity", function () {
      let parents = $(this).closest("[data-category]");
      let quantity = 0;
      parents.find("[list-category]").each(function () {
          $(this).find("[content_value]").each(function () {
              quantity += parseInt($(this).find(".input_quantity").val());
          });
      });
      $("#warehouse").val(quantity);
   });

});

