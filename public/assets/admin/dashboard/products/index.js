$(document).ready(function () {
    let a =1 ;
    let b =1;
   $(".add-category").click(function () {
       a += 1;
       let name =$('.title-group-catagory').val();
       let value= '';
       if (name == ''){ value = 'Loại';}else {value = name;}
       let input = '<input class="content-group-catagory" data-index = "'+a+'" type="text" data-name="1" placeholder="Nhập phân loại hàng, ví dụ: Trắng, Đỏ v.v" style="display: block">';
       $('.data-input').append(input);
       let html = '<div class="data-product d-flex justify-content-lg-center align-items-center w-100 border-bottom" data-category="" data-index = "'+a+'" ' + '>' +
           '<div class="content-box product-type">' + '<span class="type">'+value+'</span> <span class="name-type"></span>' + '</div>' +
           '<div class="container-box">' + '<div class="d-flex w-100 border-bottom data-value" data-size=""  data-price="" data-inventory="" data-input="1">'+ '<div class="content-box content-type">Loại</div>' +
           '<div class="content-box product-price"><input type="number" min="0" class="input-price" placeholder="Đơn giá"><label>vnđ</label></div>' +
           '<div class="content-box product-price"><input type="number" min="0" class="input-price-ctv" placeholder="Giá CTV"><label>vnđ</label></div>' +
           '<div class="content-box product-inventory"><input class="input-inventory" type="number" min="0"></div>' +
           '</div>'+
           '</div>' +
           '</div>';
       $('.body-table').append(html);
   });
   $('.add-category-2').click(function () {
       b+=1;
       let input = '<input class="content-group-catagory-2" type="text" data-name="1" placeholder="Nhập phân loại, ví dụ: S, M, v.v" style="display: block" data-input="'+b+'">';
       let html ='<div class="d-flex w-100 border-bottom form_price_product data-value" data-size="" data-price="" data-inventory="" data-input="'+b+'">\n' +
           '                                                                <div class="content-box content-type">Loại</div>\n' +
           '                                                                <div class="content-box product-price"><input class="input-price" type="number" min="0" placeholder="Đơn giá"><label>vnđ</label></div>\n' +
           '                                                                <div class="content-box product-price"><input class="input-price-ctv" type="number" min="0" placeholder="Giá CTV"><label>vnđ</label></div>\n' +
           '                                                                <div class="content-box product-inventory"><input class="input-inventory" type="number" min="0"></div>\n' +
           '                                                            </div>';
       $('.container-group-2').append(input);
       $(".data-product").each(function () {
          $(this).find(".container-box").append(html);
       });
   });
    $(document).on("keyup", ".container-group .title-group-catagory", function () {
      let name = $(this).val();
        $(this).attr("data-name", name);
        $('.body-table .data-product').each(function () {
            if (name == ''){
                $(this).find(".product-type .type").text("Loại");
            }else {
                $(this).find(".product-type .type").text(name);
            }
        });
   });
   $(document).on("keyup", ".content-group-catagory", function () {
       let name = $(this).val();
       $(this).attr("data-name", name);
       let index = $(this).attr("data-index");
       $('.body-table .data-product').each(function () {
          let data_index = $(this).attr("data-index");
          if (data_index == index){
              $(this).attr("data-category", name);
              $(this).find(".product-type .name-type").text(name);
          }
       });
   });
   $(document).on("keyup", ".container-group-2 .title-group-catagory", function () {
      let name = $(this).val();
      $(this).attr("data-name", name);
   });
    $(document).on("keyup", ".content-group-catagory-2", function () {
        let name = $(this).val();
        let index = $(this).attr("data-input");
        $(this).attr("data-name", name);
        $(".data-product").each(function () {
           $(this).find(".data-value").each(function () {
               let data_index = $(this).attr("data-input");
               if (data_index == index){
                   $(this).find('.content-type').text(name);
                   $(this).attr("data-size", name);
               }
           });
        });
    });
    $(document).on("keyup", ".input-price", function () {
        let value = parseInt($(this).val());
        $(this).parents(".data-value").attr("data-price", value);
    });
    $(document).on("keyup", ".input-price-ctv", function () {
        let value = parseInt($(this).val());
        $(this).parents(".data-value").attr("data-price-ctv", value);
    });
    $(document).on("keyup", ".input-inventory", function () {
        let value = parseInt($(this).val());
        $(this).parents(".data-value").attr("data-inventory", value);
    });
});
