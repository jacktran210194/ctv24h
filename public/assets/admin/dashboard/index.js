let $primary = '#7367F0';
let $success = '#28C76F';
let $danger = '#EA5455';
let $warning = '#FF9F43';
let $info = '#00cfe8';
let $danger_light = '#f29292';
let $success_light = '#55DD92';
let $info_light = '#1fcadb';
let $strok_color = '#b9c3cd';
let $label_color = '#e7e7e7';
let $white = '#fff';


$(document).ready(function () {
    showLoading();
    // setTimeout(function () {
    //     hideLoading();
    // }, 2000);

    let name = 'Số cửa hàng';

    // tổng số cửa hàng
    // ----------------------------------
    let $category = [1,2,3,4,5,6];
    let $shop_active = loadOption($success_light, $success, $category, [100,200,300, 100, 200, 500], '#line-area-chart-total-shop', name);
    $shop_active.render();

    // cửa hàng không hoạt động
    // ----------------------------------
    let $data_shop_inactive = [200,200,100, 500, 800, 200];
    let $shop_in_active = loadOption($danger_light, $danger, $category, $data_shop_inactive, '#line-area-chart-total-shop-dateline', name);
    $shop_in_active.render();

    // dịch vụ
    // ----------------------------------
    let $data_service = [900,600,100, 800, 700, 900];
    let $data_service_render = loadOption($info, $primary, $category, $data_service, '#line-area-chart-total-service', 'Số dịch vụ');
    $data_service_render.render();

    //Doanh thu
    // let $order = JSON.parse($('.data-total-shop').attr('data-revunue'));
    // let $revenueChart = optionRevenue($order);
    // $revenueChart.render();
});

function loadOption($color_light, $color, $categories, $data, $document_id, $name) {
    let options = {
        chart: {
            height: 270,
            toolbar: {show: false},
            type: 'line',
        },
        stroke: {
            curve: 'smooth',
            dashArray: [0, 8],
            width: [4, 2],
        },
        grid: {
            borderColor: $label_color,
        },
        legend: {
            show: false,
        },
        colors: [$color_light],

        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                inverseColors: false,
                gradientToColors: [$color],
                shadeIntensity: 1,
                type: 'horizontal',
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100, 100, 100]
            },
        },
        markers: {
            size: 0,
            hover: {
                size: 5
            }
        },
        xaxis: {
            labels: {
                style: {
                    colors: $strok_color,
                }
            },
            axisTicks: {
                show: false,
            },
            categories: $categories,
            axisBorder: {
                show: false,
            },
            tickPlacement: 'on',
        },
        yaxis: {
            tickAmount: 5,
            labels: {
                style: {
                    color: $strok_color,
                },
                formatter: function (val) {
                    return val > 999 ? (val / 1000).toFixed(1) + 'k' : val;
                }
            }
        },
        tooltip: {
            x: {show: false}
        },
        series: [
            {
                name: $name,
                data: $data
            }
        ]

    };

    let LineChart = new ApexCharts(
        document.querySelector($document_id),
        options
    );

    return LineChart;
}

function optionRevenue($order) {
    let category = JSON.parse($('.data-total-shop').attr('data-month'));
    console.log('category',$order);
    let revenueChartoptions = {
        chart: {
            height: 270,
            toolbar: {show: false},
            type: 'line',
        },
        stroke: {
            curve: 'smooth',
            dashArray: [0, 8],
            width: [4, 2],
        },
        grid: {
            borderColor: $label_color,
        },
        legend: {
            show: false,
        },
        colors: [$success_light],

        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                inverseColors: false,
                gradientToColors: [$success],
                shadeIntensity: 1,
                type: 'horizontal',
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100, 100, 100]
            },
        },
        markers: {
            size: 0,
            hover: {
                size: 5
            }
        },
        xaxis: {
            labels: {
                style: {
                    colors: $strok_color,
                }
            },
            axisTicks: {
                show: false,
            },
            categories: category,
            axisBorder: {
                show: false,
            },
            tickPlacement: 'on',
        },
        yaxis: {
            tickAmount: 5,
            labels: {
                style: {
                    color: $strok_color,
                },
                formatter: function (val) {
                    return val > 999 ? (val / 1000).toFixed(1) + 'k' : val;
                }
            }
        },
        tooltip: {
            x: {show: false}
        },
        series: [
            {
                name: "Danh thu",
                data: $order
            }
        ],

    };

    let revenueChart = new ApexCharts(
        document.querySelector("#revenue-chart"),
        revenueChartoptions
    );

    return revenueChart;
}

