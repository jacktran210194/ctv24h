$(document).ready(function () {
    $(document).on("click", ".btn-delete-image", function () {
        let url = $(this).attr('data-url');
        $('.btn-confirm').attr('data-url', url);
        $('.message-modal').text($(this).attr('data-msg'));
        $('#myModal').modal('show');
    });
    $(document).on("click", ".btn-remove", function () {
        let index = $(".btn-remove").index(this);
        let values = form_data.getAll("img[]");
        values.splice(index, 1);
        form_data.delete("img[]");
        for (let i = 0; i < values.length; i++) {
            form_data.append("img[]", values[i]);
        }
        $(this).parent(".pip").remove();
    });
    $(document).on("change", "#imageQr", function () {
        readURL(this);
    });
    $(document).on("click", ".btn-upload-image-qr", function () {
        $('#imageQr').trigger('click');
    });
    $(document).on('change', '.check_attr', function () {
        if ($(this).is(':checked')) {
            $('.attr-product').show();
        } else {
            $('.attr-product').hide();
        }
    });
    $(document).on('change', '.check_price_discount', function () {
        if ($(this).is(':checked')) {
            $('.form_discount').show();
        } else {
            $('.form_discount').hide();
        }
    });
    $(document).on('change', '.check_auction', function () {
        if ($(this).is(':checked')) {
            $('.form_auction').show();
        } else {
            $('.form_auction').hide();
        }
    });
    $(document).on('change', '.check_booking', function () {
        if ($(this).is(':checked')) {
            $('.form_booking').show();
        } else {
            $('.form_booking').hide();
        }
    });
    $(document).on('click', '.btn-add-value', function () {
        $(this).parents('.form_attr:first').find('.form-1').append("<div class=\"form-group d-flex\">\n" +
            "                                    <label style='width: 8%;' for=\"\">Giá trị</label>\n" +
            "                                    <input style='width: 35%;' class=\"form-control\" type=\"text\">\n" +
            "                                    <button class=\"btn-delete-value btn btn-danger\"><i class='fa fa-times'></i></button>\n" +
            "                                </div>\n");
    })
    $(".btn-delete-group").on("click", function () {
        $(".more-email").children().last().remove();
    });
    $(document).on('click', '.btn-delete-value', function () {
        $(this).parents('.form_attr:first').find('.form-1').children().last().remove();
    });
    $(document).on('click', '.btn-add-group', function () {
        $(".more-email").append("<div class=\"form-group form_attr\">\n" +
            "                                <div class=\"form-group d-flex\">\n" +
            "                                    <label style='width: 8%;' for=\"\">Thuộc tính</label>\n" +
            "                                    <input style='width: 35%;' class=\"attribute_name form-control\" type=\"text\">\n" +
            "                                </div>\n" +
            "                                <div class=\"form-group d-flex\">\n" +
            "                                    <label style='width: 8%;' for=\"\">Giá trị</label>\n" +
            "                                    <input style='width: 35%;' class=\"attribute_value form-control\" type=\"text\">\n" +
            "                                    <button id=\"addEmail1\" class=\"btn-add-value btn btn-primary\"><i class='fa fa-plus'></i></button>\n" +
            "                                </div>\n" +
            "                                <div class=\"form-group\">\n" +
            "                                    <div class=\"form-1\" id=\"more-email1\"></div>\n" +
            "                                </div>\n" +
            "                            </div>");
    });
});

