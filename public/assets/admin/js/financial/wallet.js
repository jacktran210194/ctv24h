$(document).ready(function () {
    $(document).on("click", ".btn-add-bank-account", function () {
        $('.popup-add-bank-account').addClass('show-popup');
        // $('#overlay1').addClass('open');
    });
    $(document).on("click", ".btn-cancel-bank-account", function () {
        $('.popup-add-bank-account').removeClass('show-popup');
        // $('#overlay1').removeClass('open');
    });

    //Bank
    let verificationId;
    $(document).on("click", ".btn-confirm-bank-account", async function () {
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('id', $(this).attr('data-id'));
        form_data.append('name', $('#name').val());
        form_data.append('identify_card', $('#identify_card').val());
        form_data.append('user_name', $('#user_name').val());
        form_data.append('bank_name', $('#bank_name').val());
        form_data.append('bank_account', $('#bank_account').val());
        form_data.append('branch', $('#branch').val());
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                showLoading();
            },
            success: async function (data) {
                if (data.status) {
                    // swal('Thành công', data.msg, 'success');
                    // window.location.href = data.url;

                    // $('.popup-add-bank-account').removeClass('show-popup');
                    // $('.popup-verification').addClass('show-popup-verification');

                    $('#recaptcha-container-bank').show();
                    let applicationVerifier = new firebase.auth.RecaptchaVerifier(
                        'recaptcha-container-bank',{
                            size: "invisible",
                            callback: function(response) {
                                submitPhoneNumberAuth();
                            }
                        });
                    let phone = $('.btn-confirm-bank-account').attr('data-phone');
                    let fs = phone.slice(0, 3);
                    if (fs !== '+84') {
                        phone = phone.slice(1, phone.length);
                        phone = '+84' + phone;
                    }
                    let provider = new firebase.auth.PhoneAuthProvider();
                    try {
                        verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                        $('#recaptcha-container-bank').hide();
                        $('.popup-add-bank-account').removeClass('show-popup');
                        $('.popup-verification').addClass('show-popup-verification');
                        // $('#overlay1').addClass('open');
                    } catch (e) {
                        console.log('e');
                        let click = 0;
                        $(document).on("click", ".swal-button--confirm", function () {
                            console.log(click++);
                        });
                    }
                } else {
                    swal('Lỗi', data.msg, 'error');
                }
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
    $(document).on("click", ".btn-confirm-verification", async function () {
        try {
            let verificationCode = $('.otp_bank').val();
            let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
            try {
                await firebase.auth().signInWithCredential(phoneCredential);
                let url = $(this).attr('data-url');
                let form_data = new FormData();
                let check_pin_code = $(this).attr('data-pin-code');
                form_data.append('identify_card', $('#identify_card').val());
                form_data.append('user_name', $('#user_name').val());
                form_data.append('bank_name', $('#bank_name').val());
                form_data.append('bank_account', $('#bank_account').val());
                form_data.append('branch', $('#branch').val());
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    type: "POST",
                    data: form_data,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.status) {
                            $('#overlay1').removeClass('open');
                            $('.popup-verification').removeClass('show-popup-verification');
                            swal("Thông báo", data.msg, "success");
                            // window.location.href = data.url;
                            $('.popup-setting-pin-code').addClass('show-popup');
                        } else {
                            swal("Thông báo", data.msg, "error");
                        }
                    },
                    error: function () {
                        console.log('error');
                    },
                });
            } catch (e) {
                swal("Thông báo", "Mã OTP không chính xác !", "error");
            }
        } catch (e) {
            swal("Thông báo", "Đã có lỗi xảy ra !", "error");
        }
    });
    //Bank

    $(document).on("click", ".btn-cancel-verification", function () {
        $('.popup-verification').removeClass('show-popup-verification');
        // $('#overlay1').removeClass('open');

    });
    $(document).on("click", ".btn-withdrawal", function () {
        $('.popup-withdrawal').addClass('show-popup-withdrawal');
        // $('#overlay1').addClass('open');
    });
    $(document).on("blur", "#price", function () {
        let view_price = $(this).val();
        $('.price-show').text(Intl.NumberFormat().format(view_price) + 'đ');
    });
    $(document).on("click", ".btn-cancel-withdrawal", function () {
        $('.popup-withdrawal').removeClass('show-popup-withdrawal');
        // $('#overlay1').removeClass('open');
    });
    $(document).on("click", ".btn-confirm-withdrawal", function () {
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('bank_id', $('#bank_id').val());
        form_data.append('price', $('#price').val());
        let bank = $('#bank_id').val();
        $('.btn-confirm-verification-withdrawal').attr('data-bank', bank);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                showLoading();
            },
            success: async function (data) {
                if (data.status) {
                    $('.popup-withdrawal').removeClass('show-popup-withdrawal');
                    $('.popup-verification-withdrawal').addClass('show-popup-verification-withdrawal');
                    // $('#overlay1').addClass('open');
                } else {
                    swal('Lỗi', data.msg, 'error');
                }
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
    $(document).on("click", ".btn-cancel-verification-withdrawal", function () {
        $('.popup-verification-withdrawal').removeClass('show-popup-verification-withdrawal');
    });
    $(document).on("click", ".btn-confirm-verification-withdrawal", function () {
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('pin_code_input', $('#pin_code_input').val());
        form_data.append('price', $('#price').val());
        form_data.append('bank_id', $(this).attr('data-bank'));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                showLoading();
            },
            success: async function (data) {
                if (data.status) {
                    $('.popup-verification-withdrawal').removeClass('show-popup-verification-withdrawal');
                    $('#overlay1').removeClass('open');
                    swal('Thành công', data.msg, 'success');
                    window.location.href = data.url;
                } else {
                    swal('Lỗi', data.msg, 'error');
                }
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
    $(document).on("click", ".forgot-pin-code", function () {
        $('.popup-verification-withdrawal').removeClass('show-popup-verification-withdrawal');
        $('.popup-setting-pin-code').addClass('show-popup');
        // $('#overlay1').addClass('open');
    });
    $(document).on("click", ".btn-update-pin-code", function () {
        $('.popup-setting-pin-code').addClass('show-popup');
        // $('#overlay1').addClass('open');
    });
    $(document).on("click", ".btn-cancel-setting-pin-code", function () {
        $('.popup-setting-pin-code').removeClass('show-popup');
        // $('#overlay1').removeClass('open');
    });
    $(document).on("click", ".btn-cancel-verification-pin-code", function () {
        $('.popup-verification-pin-code').removeClass('show-popup-verification-pin-code');
        // $('#overlay1').removeClass('open');
    });

    //Đổi Pin
    $(document).on("click", ".btn-confirm-setting-pin-code", async function () {
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('id', $(this).attr('data-id'));
        form_data.append('pin_code', $('#pin_code').val());
        form_data.append('re_pin_code', $('#re_pin_code').val());
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                showLoading();
            },
            success: async function (data) {
                if (data.status) {
                    // $('.popup-setting-pin-code').removeClass('show-popup');
                    // $('.popup-verification-pin-code').addClass('show-popup-verification-pin-code');

                    $('#recaptcha-container').show();
                    let applicationVerifier = new firebase.auth.RecaptchaVerifier(
                        'recaptcha-container');
                    let phone = $('.btn-confirm-setting-pin-code').attr('data-phone');
                    let fs = phone.slice(0, 3);
                    if (fs !== '+84') {
                        phone = phone.slice(1, phone.length);
                        phone = '+84' + phone;
                    }
                    let provider = new firebase.auth.PhoneAuthProvider();
                    try {
                        verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                        $('#recaptcha-container').hide();
                        $('.popup-setting-pin-code').removeClass('show-popup');
                        $('.popup-verification-pin-code').addClass('show-popup-verification-pin-code');
                        // $('#overlay1').addClass('open');
                    } catch (e) {
                        console.log('e');
                    }
                } else {
                    swal('Lỗi', data.msg, 'error');
                }
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                hideLoading();
            }
        });
    });
    $(document).on("click", ".btn-confirm-verification-pin-code", async function () {
        try {
            let verificationCode = $('.otp').val();
            let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
            try {
                await firebase.auth().signInWithCredential(phoneCredential);
                let url = $(this).attr('data-url');
                let form_data = new FormData();
                form_data.append('pin_code', $('#pin_code').val());
                form_data.append('re_pin_code', $('#re_pin_code').val());
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    type: "POST",
                    data: form_data,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.status) {
                            $('.popup-verification-pin-code').removeClass('show-popup-verification-pin-code');
                            $('#overlay1').removeClass('open');
                            swal("Thông báo", data.msg, "success");
                            window.location.reload();
                        } else {
                            swal("Thông báo", data.msg, "error");
                        }
                    },
                    error: function () {
                        console.log('error');
                    },
                });
            } catch (e) {
                swal("Thông báo", "Mã OTP không chính xác !", "error");
            }
        } catch (e) {
            swal("Thông báo", "Đã có lỗi xảy ra !", "error");
        }
    });

    //Lịch sử các giao dịch gần đây | Lọc theo ngày | Tất cả
    function filter_date_all() {
        let url = $('.btn-filter-order-all').attr('data-url');
        let data = {};
        data['time_start'] = $('.time-start').val();
        data['time_end'] = $('.time-end').val();
        if (data['time_start'] == '' || data['time_end'] == '' || (data['time_start'] > data['time_end'])) {
            swal("Thông báo", "Vui lòng chọn khoảng thời gian phù hợp", "error");
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            dataType: 'json',
            data: data,
            success: function (data) {
                $('tbody').html(data.table_data);
            },
            error: function () {
                console.log('error')
            },
        });
    }
    $(document).on('click', '.btn-filter-order-all', function () {
        filter_date_all();
    });

    //Lịch sử các giao dịch gần đây | Lọc theo ngày | Doanh thu
    function filter_date_revenue() {
        let url = $('.btn-filter-order-revenue').attr('data-url');
        let data = {};
        data['time_start'] = $('.time-start').val();
        data['time_end'] = $('.time-end').val();
        if (data['time_start'] == '' || data['time_end'] == '' || (data['time_start'] > data['time_end'])) {
            swal("Thông báo", "Vui lòng chọn khoảng thời gian phù hợp", "error");
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            dataType: 'json',
            data: data,
            success: function (data) {
                $('tbody').html(data.table_data);
            },
            error: function () {
                console.log('error')
            },
        });
    }
    $(document).on('click', '.btn-filter-order-revenue', function () {
        filter_date_revenue();
    });

    //Lịch sử các giao dịch gần đây | Lọc theo ngày | Rút tiền
    function filter_date_withdrawal() {
        let url = $('.btn-filter-order-withdrawal').attr('data-url');
        let data = {};
        data['time_start'] = $('.time-start').val();
        data['time_end'] = $('.time-end').val();
        if (data['time_start'] == '' || data['time_end'] == '' || (data['time_start'] > data['time_end'])) {
            swal("Thông báo", "Vui lòng chọn khoảng thời gian phù hợp", "error");
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            dataType: 'json',
            data: data,
            success: function (data) {
                $('tbody').html(data.table_data);
            },
            error: function () {
                console.log('error')
            },
        });
    }
    $(document).on('click', '.btn-filter-order-withdrawal', function () {
        filter_date_withdrawal();
    });
    $(document).on('click', '.btn-delete-bank', function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['id'] = $(this).attr('data-id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'json',
            data: data,
            success: function (data) {
                if(data.status){
                    swal('Thông báo', 'Hủy liên kết thành công', 'success');
                    window.location.reload();
                }
            },
            error: function () {
                console.log('error')
            },
        });
    });
});
