$(document).ready(function () {
    let data = {};
    $(".btn-popup-edit").click(function () {
        let url = window.location.origin + '/admin/collaborator_product/edit';
        data['id'] = $(this).find('input[type="hidden"]').val();
        ajax(url, data);
        $(".popup-edit-product").addClass("active");
    });
    $(document).on("click", ".popup-edit-product .close-popup", function () {
        $(".popup-edit-product").removeClass("active");
    });

    $(document).on("change", ".input_price_ctv", function () {
       let parents = $(this).closest(".content-price");
       let price = parseInt(parents.find(".input_price").val());
       if (parseInt($(this).val()) > price){
           alert("giá ctv không được lớn hơn giá đăng bán");
           $(this).val("");
       }
    });

    $('input[name="status_product"]').change(function () {
        let url = window.location.origin + '/admin/collaborator_product/active';
        data['id_product'] = $(this).val();
        if ($(this).is(":checked")){
            data['active'] = 1;
        }else {
            data['active'] = 2;
        }
        ajax(url, data);
    });

    function ajax(url, data) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           url: url,
           data: data,
           type: 'POST',
           dataType: 'json',
           success: function (data) {
               console.log(data)
               $(".popup-edit-product").html(data.prod);
           }
        });
    }
});
