function formatNumber(nStr, decSeperate, groupSeperate) {
    nStr += '';
    x = nStr.split(decSeperate);
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
    }
    return x1 + x2;
}

$(document).ready(function () {
    let id;
    let total_all_ctv_customer = $('.total-all').attr('data-total');
    let items_cart;
    let address_ctv_id = null;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Tìm sản phẩm tương tự
    // $('.dropdown').click(function () {
    //     if ($(this).hasClass("s-before")) {
    //         $(this).removeClass("s-before");
    //         $(this).parents(".item-cart").removeClass("show-popup");
    //     } else {
    //         $('.dropdown').removeClass("s-before");
    //         $('.item-cart').removeClass("show-popup");
    //         $(this).addClass("s-before");
    //         $(this).parents(".item-cart").addClass("show-popup");
    //     }
    // });
    $('.dropdown').one('click', function () {
        $('.slide-popup').slick({
            speed: 300,
            slidesToShow: 6,
            slidesToScroll: 6,
            prevArrow: '.previous',
            nextArrow: '.next',
            dots: true,
            customPaging: function (slider, i) {
                var thumb = $(slider.$slides[i]).data();
                var a = i + 2;
                return '<a>' + a + '</a>';
            },
            appendDots: '.append-dots'
        });
    });
    //-----------------//
    $(document).on("click", ".btn-edit-location", function () {
        var url = $(this).attr("data-url");
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $("#form-location-customer").html(data);
            },
            error: function () {
                console.log('error');
            }
        });
        setTimeout(function () {
            $(".infor-location-customer").each(function () {
               if ($(this).hasClass("selected")){
                   id = $(this).find(".check-box").attr("data-id");
               }
            });
            $(".check-box").on("click", function () {
                $(".infor-location-customer").removeClass("selected");
                $(this).parent().addClass("selected");
                id = $(this).attr('data-id');
            });
            //----------------------//
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".btn-finish").on("click", function () {
                let url = $(this).attr("data-url");
                let data = {};
                data['id'] = id;
                data['type'] = 1;
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: 'json',
                    data: data,
                    success: function (data) {
                        $("#form-location-customer").html(data.prod);
                    },
                    error: function () {
                        console.log('error')
                    }
                });
            });
            //----------------------//
            $(".btn-back").on("click", function () {
                let data = {};
                data['type'] = 2;
                $.ajax({
                    url: window.location.origin + '/address/default_ajax',
                    type: "POST",
                    dataType: 'json',
                    data: data,
                    success: function (data) {
                        $("#form-location-customer").html(data.prod);
                    },
                    error: function () {
                        console.log('error')
                    }
                });
            });
        }, 500);
    });

    $(document).on("click", ".btn-delete", function () {
        let url = $(this).attr('data-url');
        $('.btn-confirm').attr('data-url', url);
        $('#modalDelete').modal('show');
    });

    $(document).on("click", ".btn-minus", function () {
        let url = $(this).attr('data-url');
        let parent = $(this).closest('.item-cart');
        let element = parent.find('.total-price');
        let input = parent.find("input.input-cart");
        let data = {};
        if (parent.find(".check-product").is(":checked")){
            data['checked'] = 1;
        }else {
            data['checked'] = 2;
        }
        data['type'] = 2;
        data['id'] = $(this).attr('data-id');
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    element.html(data.data_price + 'đ');
                    input.val(data.quantity);
                    if (data.checked){
                        $('span.price-total').html(data.total_price + 'đ');
                    }
                    parent.find(".price-new").text(data.price_cart + 'đ');
                } else {
                    swal("Thông báo", 'Đã có lỗi xảy ra', "error");
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });
   //-----------------------------------------//
    let data = {};
    let parent, price_input_cart, url;
    $(document).on("click", ".btn-plus", function () {
        let url = $(this).attr('data-url');
        parent = $(this).closest(".item-cart");
        let quantity = parent.find("input.input-cart");
        let element = parent.find('.total-price');
        let data = {};
        if (parent.find(".check-product").is(":checked")){
            data['checked'] = 1;
        }else {
            data['checked'] = 2;
        }
        data['type'] = 1;
        data['id'] = $(this).attr('data-id');
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    element.html(data.data_price + "đ");
                    quantity.val(data.quantity);
                    if (data.checked){
                        $('span.price-total').html(data.total_price + 'đ');
                    }
                    parent.find(".price-new").text(data.price_cart + 'đ');
                } else {
                    swal("Thông báo", data.msg, "error");
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });

    $(document).on('blur', '.input-cart', function () {
        let $this = $(this);
        let url = $this.attr('data-url');
        parent = $this.closest(".item-cart");
        let element = parent.find('.total-price');
        let data = {};
        if (parent.find(".check-product").is(":checked")){
            data['checked'] = 1;
        }else {
            data['checked'] = 2;
        }
        data['quantity'] = $this.val();
        data['type'] = 3;
        data['id'] = $this.attr('data-id');
        data['value'] = $this.attr('data-value');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    element.html(data.data_price + "đ");
                    $this.val(data.quantity);
                    if (data.check){
                        swal("Thông báo", data.msg, "error");
                    }
                    if (data.checked){
                        $('span.price-total').html(data.total_price + 'đ');
                    }
                    parent.find(".price-new").text(data.price_cart + 'đ');
                } else {
                    alert('Vui lòng thử lại');
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });

    $('.input-cart').click(function () {
        let price_input = parseInt($(this).attr("data-price"));
        let quantity = parseInt($(this).val());
        price_input_cart = price_input * quantity;
    });

    $(document).on("click", ".btn-checkout", function () {
        let cart = [];
        $("[name='cart[]']").each(function () {
            if ($(this).is(":checked")) {
                cart.push($(this).attr('data-id'));
            }
        });
        if (cart.length === 0) {
            swal("Thông báo", 'Vui lòng chọn ít nhất một sản phẩm', "error");
            return false;
        } else {
            $(this).prop('disabled', true);
            let url = $(this).attr('data-url');
            let data = {};
            data['check_coin'] = $('.check_coin').is(':checked') ? 1 : 0;
            data['total_price'] = $('.price-total').attr('data-value');
            data['customer_id'] = $(this).attr('data-customer');
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        swal("Thành công", data.msg, "success");
                        window.location.href = data.url;
                    } else {
                        swal("Thất bại", data.msg, "error");
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    }
                },
                error: function () {
                    console.log('error')
                }
            });
        }
    });

    $(document).on("change", ".checked-all", function () {
         url = window.location.origin + '/cart/check_cart';
        let check = $(this).is(":checked") ? 1 : 0;
        if (check == 1) {
            data['type'] = 1; //---------------checked all----------------//
            $(".item-cart-shop").each(function () {
                var input = $(this).find(".checked-shop");
                input.prop("checked", true);
            });
            $(".item-cart").each(function () {
                var inputcart = $(this).find(".check-product");
                inputcart.prop("checked", true);
            });
        } else {
            data['type'] = 2;
            $(".item-cart-shop").each(function () {
                var input = $(this).find(".checked-shop");
                input.prop("checked", false);
            });
            $(".item-cart").each(function () {
                var inputcart = $(this).find(".check-product");
                inputcart.prop("checked", false);
            });
        }
        selectProduct();
    });

    $(document).on("change", ".checked-shop", function () {
        url = window.location.origin + '/cart/check_cart_shop';
        var check = $(this).is(":checked") ? 1 : 0;
        var parent = $(this).parents(".container-item-cart");
        var item_cart = parent.find(".item-cart");
        data['shop_id'] = $(this).val();
        if (check == 1) {
            data['type'] = 1;
            item_cart.each(function () {
                var input = $(this).find(".check-product");
                input.prop("checked", true);
            });
        } else {
            data['type'] = 2;
            item_cart.each(function () {
                var input = $(this).find(".check-product");
                input.prop("checked", false);
            });
            $(".checked-all").prop("checked", false);
        }
        selectProduct();
    });

    $(document).on("click", ".checkout-cart", function () {
        let url = $(this).attr('data-url');
        let wallet = $(this).attr('data-wallet');
        let total_pay = $(this).attr('data-total-payment');
        let pay_id = $(this).attr('data-payment');
        let data = {};
        data['note'] = $('.note-order').val();
        data['order_id'] = $(this).attr('data-order-id');
        data['total_payment'] = $(this).attr('data-total-payment');
        data['address'] = $(this).attr('data-address');
        data['customer_id'] = $(this).attr('data-customer');
        data['shipping_id'] = $(this).attr('data-shipping');
        data['delivery_method'] = $(this).attr('data-type-ship');
        data['payment_id'] = $(this).attr('data-payment');
        if (!data['delivery_method']) {
            swal('Thông báo', 'Vui lòng chọn thời gian giao hàng phù hợp', 'error');
            return false;
        }
        if(pay_id == 0 && parseInt(total_pay) > parseInt(wallet)){
            swal('Thông báo', 'Số tiền trong ví không đủ. Vui lòng chọn phương thức thanh toán khác', 'error');
            return false;
        } else {
            $(this).prop('disabled', true);
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                success: function (data) {
                    console.log('data', data);
                    if (data.status) {
                        swal("Thành công", data.msg, "success");
                        window.location.href = data.url;
                    } else {
                        swal("Thất bại", data.msg, "error");
                    }
                },
                error: function (e) {
                    let error = JSON.stringify(e);
                    console.log(error);
                }
            });
        }
    });
    function selectProduct() {
        data['customer_id'] = $(this).attr('data-customer');
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    $('.price-total').html(data.total_price + 'đ');
                    $('.price-total').attr('data-value',data.total_price_order);
                    if (data.checked){
                        if (data.checked_all){
                            $('input.checked-all').prop("checked", true);
                        }else {
                            $('input.checked-all').prop("checked", false);
                        }
                        if (data.checked_shop){
                            if (data.data_shop){
                                items_cart.find('input.checked-shop').prop("checked", true);
                            }else {
                                items_cart.find('input.checked-shop').prop("checked", false);
                            }
                        }
                    }
                }
            },
            error: function (e) {
                console.log(e)
            }
        });
    }

    $(document).on("change", ".check-product", function () {
       url = window.location.origin + '/cart/check_cart_product';
       let parent = $(this).closest(".container-item-cart");
        items_cart = parent.find('.item-cart-shop');
       if ($(this).is(':checked')){
           data['type'] = 1;
       }else {
           data['type'] = 0;
       }
       data['id'] = $(this).val();
        selectProduct();
    });

    $(document).on("click", ".btn-checkout-customer-ctv", function () {
        let cart = [];
        $("[name='cart[]']").each(function () {
            if ($(this).is(":checked")) {
                cart.push($(this).attr('data-id'));
            }
        });
        if (cart.length === 0) {
            swal("Thông báo", 'Vui lòng chọn ít nhất một sản phẩm', "error");
            return false;
        } else {
            $(this).prop('disabled', true);
            let url = $(this).attr('data-url');
            let data = {};
            data['customer_id'] = $(this).attr('data-customer');
            data['total_price_order'] = $('.price-total').attr('data-value');
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        swal("Thành công", data.msg, "success");
                        window.location.href = data.url;
                    } else {
                        swal("Thất bại", data.msg, "error");
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    }
                },
                error: function (e) {
                    let error = JSON.stringify(e);
                    console.log(error);
                }
            });
        }
    });

    $(document).on("click", ".btn-confirm-customer-ctv", function () {
        let data = {};
        let url = window.location.origin + '/cart/save_address_customer';
        data['name'] = $('#name_customer_ctv').val();
        data['phone'] = $('#phone_customer_ctv').val();
        data['city'] = $('#city').val();
        data['district'] = $('#district').val();
        data['ward'] = $('#ward').val();
        data['address_customer_ctv'] = $('#address_customer_ctv').val();
        let phone_check =  data['phone'].slice(0, 1);
        let phone_lenght =  data['phone'].length;
        if (phone_check !== '0') {
            swal('Thông báo', 'Số điện thoại không hợp lệ !', 'warning');
            return false;
        }
        if (phone_lenght < 10 || phone_lenght > 10) {
            swal('Thông báo', 'Độ dài số điện thoại không hợp lệ !', 'warning');
            return false;
        }

        if (!data['name'] || !data['phone'] || !data['city'] || !data['district'] || !data['ward'] || !data['address_customer_ctv']) {
            swal('Thông báo', 'Vui lòng nhập thông tin người nhận', 'error');
            return false;
        }
        $.ajax({
           url: url,
           data: data,
           type: 'POST',
           dataType: 'json',
           success: function (data) {
               if (data.status){
                   address_ctv_id = data.address_id;
                   $(".popup-info-customer-show .content-address").html(data.prod);
                   $('.popup-info-customer').removeClass('show-popup');
                   $('.popup-info-customer-show').addClass('show-popup');
               }
           },
            error: function (data) {
                console.log(data)
            }
        });
    });
    $(document).on("click", ".checkout-cart-customer-ctv", function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['note'] = $('.note-order').val();
        data['cod'] = $("#currency-field").val().replace(/[$,]+/g,"");
        data['order_id'] = $(this).attr('data-order-id');
        data['total_payment'] = $(this).attr('data-total-payment');
        data['address'] = $(this).attr('data-address');
        data['customer_id'] = $(this).attr('data-customer');
        data['address_ctv'] = address_ctv_id;
        data['shipping_id'] = $(this).attr('data-shipping');
        data['payment_id'] = $(this).attr('data-payment');
        data['delivery_method'] = $(this).attr('data-type-ship');
        data['bonus'] = $(this).attr('data-bonus-ctv');
        if (!data['delivery_method']) {
            swal('Thông báo', 'Vui lòng chọn thời gian giao hàng phù hợp', 'error');
            return false;
        }
        if (address_ctv_id == null) {
            swal('Thông báo', 'Vui lòng nhập  thông tin người nhận', 'error');
            return false;
        }
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                console.log('data', data);
                if (data.status) {
                    swal("Thành công", data.msg, "success");
                    window.location.href = data.url;
                } else {
                    swal("Thất bại", data.msg, "error");
                }
            },
            error: function (e) {
                let error = JSON.stringify(e);
                console.log(error);
            }
        });
    });
    $(document).on("click", ".edit-shipping-unit", function () {
        $('.popup-edit-shipping').addClass('show-popup');
    });

    $(document).on("click", ".name-shipping-unit .selected", function () {
        let name_shipping = $(this).attr('data-name');
        $('.name_shipping_show').text(name_shipping);
        if ($(this).hasClass("checked")) {
            $(".name-shipping-unit .selected").removeClass("checked");
            $(".name-shipping-unit").removeClass("rm-border");
            $(".dropdown-shipping").removeClass("fast");
            $(this).addClass("checked");
            $(this).parent().addClass("rm-border");
            $(this).parent().next().addClass("fast");

        } else {
            $(".name-shipping-unit .selected").removeClass("checked");
            $(".name-shipping-unit").removeClass("rm-border");
            $(".dropdown-shipping").removeClass("fast");
            $(this).addClass("checked");
            $(this).parent().addClass("rm-border");
            $(this).parent().next().addClass("fast");
        }
        let shipping = $(this).attr('data-id');
        $('.checkout-cart').attr('data-shipping', shipping);
        $('.checkout-cart-customer-ctv').attr('data-shipping', shipping);
    });


    $(document).on('click', '.btn-cancel', function () {
        $('.popup-edit-shipping').removeClass('show-popup');
    });

    $(document).on("click", ".btn-confirm-shipping", function () {
        $('.popup-edit-shipping').removeClass('show-popup');
    });

    $(document).on('click', '.btn-add-infor-cunstomer', function () {
        $('.popup-info-customer').addClass('show-popup');
        $(".popup-address-ctv").removeClass('active');
    });

    $(document).on('click', '.btn-cancel-customer-ctv', function () {
        $('.popup-info-customer').removeClass('show-popup');
    });

    $(".btn-popup-address-ctv").click(function () {
        $(".popup-address-ctv").addClass('active');
    });
    $(".cancel-popup").click(function () {
       $(".popup-address-ctv").removeClass('active');
    });
    $(".confirm-address").click(function () {
        let data = {};
        let url = window.location.origin + '/cart/check-address-ctv';
        data['id'] = null;
        $(".popup-address-ctv .address").each(function () {
           if ($(this).find('input[name="address_ctv"]').is(':checked')){
               data['id'] = $(this).find('input[name="address_ctv"]').val();
               address_ctv_id = data['id'];
           }
        });
        if (data['id'] == null){
            swal('Thông báo', 'Vui lòng chọn địa chỉ', 'error');
            return false;
        }
        $.ajax({
           type: 'POST',
           data: data,
           url : url,
           dataType: 'json',
           success: function (data) {
                if (data.status){
                    $(".popup-info-customer-show .content-address").html(data.prod);
                    $(".popup-info-customer-show").addClass("show-popup");
                    $(".popup-address-ctv").removeClass('active');
                }else {
                    console.log('error');
                }
           }
        });
    });
    // data['name'] = $(this).attr('data-name');
    // data['phone'] = $(this).attr('data-phone');
    // data['city'] = $(this).attr('data-city');
    // data['district'] = $(this).attr('data-district');
    // data['ward'] = $(this).attr('data-ward');
    // data['address_customer_ctv'] = $(this).attr('data-address');
    /**
     * Chọn quận/huyện
     */
    $('.choose_city').on('change', function () {
        let url = $(this).attr('data-url');
        let action = $(this).attr('id');
        let id = $(this).val();
        let _token = $('input[name="_token"]').val();
        let result = '';
        if (action === 'city') {
            result = 'district';
        }
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                action: action,
                id: id,
                _token: _token
            },
            success: function (data) {
                $('#' + result).html(data);
            }
        });
    });

    /**
     *  Chọn phường/xã
     */
    $('.choose_district').on('change', function () {
        let url = $(this).attr('data-url');
        let action = $(this).attr('id');
        let id = $(this).val();
        let _token = $('input[name="_token"]').val();
        let result = '';
        if (action === 'district') {
            result = 'ward';
        }
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                action: action,
                id: id,
                _token: _token
            },
            success: function (data) {
                $('#' + result).html(data);
            }
        });
    });

    $(document).on("click", ".edit-title-right", function () {
        $('.popup-edit-payment').addClass('show-popup-payment');
    });
    $(document).on('click', '.btn-cancel-payment', function () {
        $('.popup-edit-payment').removeClass('show-popup-payment');
    });

    $(document).on("click", ".btn-confirm-payment", function () {
        $('.popup-edit-payment').removeClass('show-popup-payment');
    });

    $(document).on("click", ".name-payment-unit .selected", function () {
        let name_payment = $(this).attr('data-name');
        $('.name-payment-show').text(name_payment);
        if ($(this).hasClass("checked")) {
            $(".name-payment-unit .selected").removeClass("checked");
            $(".name-payment-unit").removeClass("rm-border");
            $(".dropdown-payment").removeClass("fast");
            $(this).addClass("checked");
            $(this).parent().addClass("rm-border");
            $(this).parent().next().addClass("fast");
        } else {
            $(".name-payment-unit .selected").removeClass("checked");
            $(".name-payment-unit").removeClass("rm-border");
            $(".dropdown-payment").removeClass("fast");
            $(this).addClass("checked");
            $(this).parent().addClass("rm-border");
            $(this).parent().next().addClass("fast");
        }
        let payment = $(this).attr('data-id');
        $('.checkout-cart').attr('data-payment', payment);
        $('.checkout-cart-customer-ctv').attr('data-payment', payment);
    });

    $(document).on("click", ".btn-cancel-customer-ctv-show", function () {
        $('.popup-info-customer-show').removeClass('show-popup');
    });

    $(document).on("click", ".btn-confirm-customer-ctv-show", function () {
        $('.popup-info-customer-show').removeClass('show-popup');
    });
    $(document).on("click", '.create-card', function () {
        $('.popup-edit-payment').removeClass('show-popup-payment');
        $('.popup-credit-card').addClass('show_popup_credit');
    });
    $(document).on("click", ".btn-back-card", function () {
        $('.popup-credit-card').removeClass('show_popup_credit');
    });
    $(document).on("click", ".btn-done", function () {
        $('.popup-credit-card').removeClass('show_popup_credit');
    });
    $('.content-dropdown .ratio-check-box').click(function () {
        $('.content-dropdown .ratio-check-box').removeClass('active');
        $(this).addClass('active');
    });
    $(document).on("click", ".ratio-check-box", function () {
        let type_ship = $(this).attr('data-type-ship');
        if (type_ship == 0) {
            $('.sub-name-infor-shipping').text('Tất cả các ngày trong tuần');
        } else {
            $('.sub-name-infor-shipping').text('Chỉ giao giờ hành chính');
        }
        $('.checkout-cart').attr('data-type-ship', type_ship);
        $('.checkout-cart-customer-ctv').attr('data-type-ship', type_ship);
    });

    //trừ xu
    $(document).on('click', '.check_coin', function () {
        let price_total_cart_1 = $('.price-total').attr('data-value');
        let price_total_cart = parseInt(price_total_cart_1);
        let coins = $(this).val();
        let coin = parseInt(coins);
        let check_coin = $(this).is(":checked") ? 1 : 0;
        if (check_coin == 1) {
            price_total_cart = price_total_cart - coin;
            $('.price-total').text(formatNumber(price_total_cart, '.', ',') + ' đ');
            $('.price-total').attr('data-value', price_total_cart);
        } else {
            price_total_cart = price_total_cart + coin;
            $('.price-total').text(formatNumber(price_total_cart, '.', ',') + ' đ');
            $('.price-total').attr('data-value', price_total_cart);
        }
    });
    // filter address ctv

    $('.btn-filter-ctv').click(function () {
        let data = {};
        data['keyword'] = $('.popup-address-ctv input[name="keyword"]').val();
        $.ajax({
            url: window.location.origin + '/search/address-ctv',
            data: data,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                $(".list-address").html(data.prod);
            },
            error: function (data) {
                console.log(data);
            }
        })
    });

    $("input[data-type='currency']").on({
        keyup: function() {
            formatCurrency($(this));
        },
        blur: function() {
            formatCurrency($(this), "blur");
            checkCurrency($(this));
        }
    });


    function formatNumber(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    function checkCurrency(input) {
        let val = parseInt(input.val().replace(/[$,]+/g,""));
        let max = parseInt(input.attr("max"));
        let min = parseInt(input.attr("min"));
        let parent = input.closest(".list_collection");
        if (val > max ){
            alert('Số tiền thu hộ không được lớn hơn' + ' ' + format2(max) + 'đ');
            input.val(format2(max));
            return false;
        }
        if (val < min){
            alert('Số tiền thu hộ không được nhỏ hơn' + ' ' + format2(min) + 'đ');
            input.val(format2(max));
            return false;
        }
        let price_profit = val - min;
        parent.find('input.profit').val(format2(price_profit));
        getTotalProfit(input.closest(".list_container_cart"), input);
    }

    function getTotalProfit(parents) {
        let total_price = 0;
        let total_profit = 0;
        parents.find(".item-cart").each(function () {
            let price = $(this).find('input[name="currency-field"]').val().replace(/[$,]+/g,"");
            let profit = $(this).find('input.profit').val().replace(/[$,]+/g,"");
            total_price += parseInt(price);
            total_profit += parseInt(profit);
        });
        $('#currency-field').val(format2(total_price));
        $('.bonus-ctv').html(format2(total_profit) + ' ' + 'đ');
        $(".total-price.price-all").html(format2(total_price + 10000) + 'đ');
    }

    function format2(n) {
        let price = n.toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        let a = price.replace(".0", "");
        return a;
    }

    function formatCurrency(input, blur) {

        // get input value
        let input_val = input.val();

        // don't validate empty input
        if (input_val === "") { return; }

        // original length
        let original_len = input_val.length;

        // initial caret position
        let caret_pos = input.prop("selectionStart");

        // check for decimal
        if (input_val.indexOf(".") >= 0) {

            let decimal_pos = input_val.indexOf(".");

            // split number by decimal point
            let left_side = input_val.substring(0, decimal_pos);
            let right_side = input_val.substring(decimal_pos);

            // add commas to left side of number
            left_side = formatNumber(left_side);

            // validate right side
            right_side = formatNumber(right_side);

            // Limit decimal to only 2 digits
            right_side = right_side.substring(0, 2);

            // join number by .
            input_val = left_side + "." + right_side;

        } else {
            input_val = formatNumber(input_val);
        }

        // send updated string to input
        input.val(input_val);

        // put caret back in the right position
        let updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
    }
});
