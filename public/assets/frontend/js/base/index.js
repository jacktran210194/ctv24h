function showLoading() {
    $(".loader-wrapper").show();
}
function hideLoading() {
    $(".loader-wrapper").fadeOut("slow");
}
