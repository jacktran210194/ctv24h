$(document).ready(function () {
    let place='',category='',shipping='',price_filter_1='',price_filter_2='',category_shop='',status='',review='',services='';
    $('.content-filter input').click(function () {
        category = $(this).attr("id");
        $('.content-filter input').prop("checked", false);
        $(this).prop("checked", true);
        filterProduct();
    });
    $('.filter-place input').click(function () {
        place = $(this).val();
        $('.filter-place input').prop("checked", false);
        $(this).prop("checked", true);
        filterProduct();
    });
    $(".filter-shipping input").click(function () {
        shipping = $(this).val();
        $('.filter-shipping input').prop("checked", false);
        $(this).prop("checked", true);
        filterProduct();
    });
    $('.btn-filter').click(function () {
        price_filter_1 = $('.price-filter-1').val();
        price_filter_2 = $('.price-filter-2').val();
        if (price_filter_1 === "" || price_filter_2 === "" || price_filter_1 < 0 || price_filter_2 <= price_filter_1){
            alert("Nhập sai số tiền");
        }else {
            filterProduct();
        }
    });
    $('.filter-status input').click(function () {
        status = $(this).val();
        $('.filter-status input').prop("checked", false);
        $(this).prop("checked", true);
        filterProduct();
    });
    $('.filter-ratings').click(function () {
        review = $(this).val();
        filterProduct();
    });
    $('.filter-catagory .sub-filter').each(function () {
        let count = $(this).find(".dropdown-filter").children().length;
        if (count == 0){
            $(this).css("display", "none");
        }
    });
    $('.sub-filter .title').click(function () {
        $(this).next().toggle("fast");
        $(this).toggleClass("active");
    });
    function filterProduct() {
        let url = window.location.origin + '/filter_category';
        let data={};
        data['filter']={};
        if (category === ""){}else {data['filter']['category_sub_child'] = category;}
        if (place === ""){}else {data['filter']['place'] = place;}
        if (shipping === ""){}else {data['filter']['shipping_id'] = shipping;}
        if (price_filter_1 === "" || price_filter_2 === ""){}else {
            data['filter']['price_filter_1'] = price_filter_1;
            data['filter']['price_filter_2'] = price_filter_2;
        }
        if (status === ""){}else {data['filter']['status_product'] = status}
        if (review === ""){}else {data['filter']['ratings'] = review}

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'GET',
            url: url,
            dataType:'json',
            data: data,
            success: function (data) {
                $('.product-search .container-section-product').html(data.prod);
            }
        });
    }
});
