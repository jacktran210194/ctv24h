$(document).ready(function () {
    let url_product =  window.location.href;
    let url = url_product + '/review-video-image';
    $.ajax({
        url: url,
        type: "GET",
        dataType: 'html',
        success: function (data) {
            $("#review-content").append(data);
        },
        error: function () {
            console.log('error')
        }
    });
    $(document).on("click", ".pagination .page-link", function (ev) {
        ev.preventDefault();
        let url = $(this).attr("href");
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $("#review-content").html(data);
            },
            error: function () {
                console.log('error')
            }
        });
    });
    $(document).on("click", ".btn-ajax-review",function (ev) {
        ev.preventDefault();
        let url = $(this).children().attr("href");
        if ($(this).hasClass("disabled")){
        }else {
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'html',
                success: function (data) {
                    $("#review-content").html(data);
                },
                error: function () {
                    console.log('error')
                }
            });
            $('.btn-ajax-review').removeClass('bg-red');
            $(this).addClass('bg-red');
        }
    });
    $(document).on("click", ".icon-play-video", function () {
        let src = $(this).attr("data-src");
        let video = '<video class="video-popup" controls autoplay><source src="' + src + '"></video>'
        $('.show-video-review')
            .html(video)
            .addClass('open')
            .one('click', function () {
                $(this)
                    .html("")
                    .removeClass('open');
            });
    });
});
