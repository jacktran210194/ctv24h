$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    let parent = null;
    $(document).on("click", ".div-quantity .minus", function () {
         parent = $(this).closest(".item");
        let input = parseInt($(this).next().attr("data-quantity"));
        let quantity = 0;
        if (input < 2) {
             quantity = 1;
        } else {
             quantity = input - 1;
        }
        $(this).next().text(quantity);
        let url = $(this).attr('data-url');
        $(this).next().attr("data-quantity", quantity);
        let data = {};
        data['type'] = 3;
        data['quantity'] = quantity;
        data['id'] = $(this).attr('data-id');
        data['mini_cart'] = 1;
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    parent.find(".price").text(data.data_price + 'đ');
                    $(".total-price .total").text(data.total_cart + 'đ');
                } else {
                    swal("Thông báo", data.msg, "error");
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });

    $(document).on("click", ".div-quantity .plus", function () {
        parent = $(this).closest(".item");
        let input = parseInt($(this).prev().attr("data-quantity"));
        let quantity_attr = parseInt($(this).attr("data-quantity-product"));
        let quantity = input + 1;
        $(this).prev().text(quantity);
        $(this).prev().attr("data-quantity", quantity);
        let url = $(this).attr('data-url');
        let data = {};
        if (quantity > quantity_attr){
            data['quantity'] = quantity_attr;
            alert("không vượt quá số lượng");
            $(this).prev().text(quantity_attr);
        }else {
            data['quantity'] = quantity;
        }
        data['id'] = $(this).attr('data-id');
        data['type'] = 1;
        data['mini_cart'] = 1;
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    parent.find(".price").text(data.data_price + 'đ');
                    $(".total-price .total").text(data.total_cart + 'đ');
                } else {
                    swal("Thông báo", data.msg, "error");
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });
});
