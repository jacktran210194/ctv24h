$(document).ready(function () {
    $(".btn-upload-image").click(function () {
        let url = location.href + "/download-file";
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $("#file-download").append(data);
            },
            error: function () {
                console.log('error')
            }
        });
    });
    $(document).on("click", ".download-all", function () {
        $('.popup-file-image .content-image').each(function () {
            $(this).find('.link-file-image')[0].click()
        });
    });
});

function copy(selector) {
    $("#description-content").attr("contenteditable", true)
        .html($(selector).html()).select()
        .on("focus", function () {
            document.execCommand('selectAll', false, null);
        })
        .focus();
    swal('Thông báo', 'Sao chép thành công !', 'success');
    document.execCommand("copy");
    $("#description-content").attr("contenteditable", false);
}
