$('.slide-content-review').slick({
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '.previous',
    nextArrow: '.next',
    dots: true,
    customPaging: function (slider, i) {
        var thumb = $(slider.$slides[i]).data();
        var a = i + 2;
        return '<a>' + a + '</a>';
    },
    appendDots: '.append-dots'
});
$(document).ready(function () {
    let check_slide = $('.slide-more-product .content').length;
    let check_similar_product = $('.slide-similar-product .content').length;
    let check_slide_product_watched = $('.slide-product-watched .content').length;
    if (check_slide > 5) {
        $('.slide-more-product').slick({
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            nextArrow: '<button class="next-arrow btn-arrow next-arrow-more-product"><img\n' +
                '                        src="Icon/base/image-arrow-next.png"></button>',
            prevArrow: '<button class="prev-arrow btn-arrow prev-arrow-more-product"><img\n' +
                '                        src="Icon/base/image-arrow-prev.png"></button>',
        });
    }
    if (check_similar_product > 5) {
        $('.slide-similar-product').slick({
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            nextArrow: '<button class="next-arrow btn-arrow next-arrow-similar-product"><img\n' +
                '                        src="Icon/base/image-arrow-next.png"></button>',
            prevArrow: '<button class="prev-arrow btn-arrow prev-arrow-similar-product"><img\n' +
                '                        src="Icon/base/image-arrow-prev.png"></button>',
        });
    }
    if (check_slide_product_watched > 4 ) {
        $('.slide-product-watched').slick({
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 1,
            nextArrow: '<button class="next-arrow btn-arrow next-arrow-similar-product"><img\n' +
                '                        src="Icon/base/image-arrow-next.png"></button>',
            prevArrow: '<button class="prev-arrow btn-arrow prev-arrow-similar-product"><img\n' +
                '                        src="Icon/base/image-arrow-prev.png"></button>',
        });
    }
});
$(document).ready(function () {
    let cart_type;
    let cart_size;
    let cart_quantity = 1;
    let id_attribute;
    $(document).on('click', '.color-swath .color', function () {
        cart_quantity = 1;
        $(this).parent().each(function () {
            $(this).children().removeClass("active");
        });
        $(this).addClass("active");
        let value = $(this).attr("data-value");
        $(".container-size .content-size").each(function () {
            $(this).find(".size").removeClass("active");
            $(this).find(".quantity").text("1");
            let size = $(this).attr("data-type");
            if (size === value) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
        if ($(this).attr('data-price') != null){
            id_attribute = $(this).attr('data-id');
            let data_price = $(this).attr('data-price');
            $(".price-product .price-buy").text(new Intl.NumberFormat().format(data_price));
        }
    });
    $(document).on("click", ".content-size .plus", function () {
       let parent = $(this).closest(".content-size");
        id_attribute = parent.attr("data-id");
       let value = parseInt(parent.find(".quantity").val());
       let max_quantity = parseInt(parent.attr("data-quantity"));
       $(".content-size .size").removeClass("active");
        parent.find(".size").addClass("active");
       let quantity = value + 1;
       if (quantity > max_quantity){
           quantity = max_quantity;
       }
        cart_quantity = quantity;
        resetQuantity($(this).closest(".container-size"));
        parent.find(".quantity").val(quantity);
        addPrice(parent);
        addPercent(parseInt(parent.attr("data-price-ctv")));
        let data = {};
        data['quantity'] = quantity;
        data['product_id'] = $('input[name="id_product"]').val();
        data['attribute_id'] = parent.attr("data-id");
        ajaxCheckPercent(data);
    });

    $(document).on("click", ".content-size .minus", function () {
        let parent = $(this).closest(".content-size");
        id_attribute = parent.attr("data-id");
        $(".content-size .size").removeClass("active");
        parent.find(".size").addClass("active");
        let value = parseInt(parent.find(".quantity").val());
        let quantity = value - 1;
        if (quantity < 1 ){
            quantity = 0;
        }
        cart_quantity = quantity;
        parent.find(".quantity").val(quantity);
        addPrice(parent);
        addPercent(parseInt(parent.attr("data-price-ctv")));
        let data = {};
        data['quantity'] = quantity;
        data['product_id'] = $('input[name="id_product"]').val();
        data['attribute_id'] = parent.attr("data-id");
        ajaxCheckPercent(data);
    });
    $(document).on("change", ".content-size .quantity", function () {
        let parent = $(this).closest(".content-size");
        id_attribute = parent.attr("data-id");
        $(".content-size .size").removeClass("active");
        parent.find(".size").addClass("active");
        let max_quantity = parseInt(parent.attr("data-quantity"));
        let value = $(this).val();
        if (value > max_quantity){
            value = max_quantity;
        }
        cart_quantity = value;
        resetQuantity($(this).closest(".container-size"));
        $(this).val(value);
        addPrice(parent);
        addPercent(parseInt(parent.attr("data-price-ctv")));
        let data = {};
        data['quantity'] = value;
        data['product_id'] = $('input[name="id_product"]').val();
        data['attribute_id'] = parent.attr("data-id");
        ajaxCheckPercent(data);
    });
    //-------------set product_attribute---------------//
    let value = $(".color-swath .color.active").attr("data-value");
    $(".container-size .content-size").each(function () {
        let size = $(this).attr("data-type");
        if (size === value) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
    //---------------//
    function addPrice(parent){
        let data_price = parseInt(parent.attr("data-price"));
        let data_price_ctv = parseInt(parent.attr("data-price-ctv"));
        let data_price_custom = data_price;
        let data_price_ctv_custom = data_price_ctv;
        let data_price_profit = data_price_custom - data_price_ctv_custom;
        let percent = (data_price_custom - data_price_ctv_custom) * 100 / data_price_custom;
        let percent_product = Math.ceil(percent);
        $(".price-product .price-buy").text(format2(data_price, 'đ'));
        $(".info_percent .percent_product").text(percent_product +'%');
    }
    //-----------format currency-----------//
    function format2(n, currency) {
        let price = n.toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + ' ' + currency;
        let a = price.replace(".0", "");
        return a;
    }
    //------------reset quantity---------------//
    function resetQuantity(containerSize) {
        containerSize.find(".content-size").each(function () {
           $(this).find("input.quantity").val(0);
        });
    }
    //-----------------//
    function addPercent(price){
        $(".content_percent .percent").each(function () {
            let percent = parseInt($(this).attr("data-percent"));
            let price_percent = price - price*percent/100;
            $(this).find(".price_percent").text(format2(price_percent, 'đ'));
            $(this).find(".price_decoration").text(format2(price, 'đ'));
        });
    }
    //-----------check percent------------//
    function ajaxCheckPercent(data){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: window.location.origin +'/check-price-percent',
            data: data,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.status){
                    $(".sign-up-ctv .price_ctv").text(data.price + ' ' + 'đ');
                    $(".info_price_profit .price_profit").text(data.profit + ' ' + 'đ');
                }
            }
        })
    }
    //-------------end set product_attribute---------------//
    $(document).on("click", ".content-size .size", function () {
        cart_quantity = 1;
        let parent = $(this).closest(".content-size");
        cart_size = $(this).parent().attr("data-size");
        id_attribute = $(this).parent().attr("data-id");
        cart_type = $(this).parent().attr("data-type");
        $(".content-size .size").removeClass("active");
        resetQuantity($(this).closest(".container-size"));
        $(this).addClass("active");
        addPrice($(this).parent());
        addPercent(parseInt(parent.attr("data-price-ctv")));
        let data = {};
        data['quantity'] = 1;
        data['product_id'] = $('input[name="id_product"]').val();
        data['attribute_id'] = parent.attr("data-id");
        ajaxCheckPercent(data);
    });
    $(document).on("click", ".add-to-cart", function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['id'] = $(this).attr('data-id');
        data['shop_id'] = $(this).attr('data-shop');
        data['customer_id'] = $(this).attr('data-customer');
        data['id_attribute'] = id_attribute;
        data['quantity'] = cart_quantity;
        if (data['customer_id'] === '') {
            location.pathname = "login";
        }
        if (data['id_attribute'] === ""){
            swal('Thông báo', 'Vui lòng chọn phân loại sản phẩm', 'error');
            return false;
        }
        if ( data['quantity'] === "" || data['quantity'] === 0 ){
            swal('Thông báo', 'Vui lòng chọn ít nhất một phân loại sản phẩm', 'error');
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    $('.icon_cart .count-item').text(data.items);
                    $('.checked').addClass('show-checked');
                    setTimeout(function () {
                        $('.checked').removeClass('show-checked');
                    }, 800)
                } else {
                    swal("Thông báo", data.msg, "error");
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });

    $(document).on("click", ".btn-save", function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['id'] = $(this).attr('data-id');
        data['shop_id'] = $(this).attr('data-shop');
        data['type'] = $(this).attr('data-type');
        data['customer_id'] = $(this).attr(' data-customer');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                $('.checked').addClass('show-checked');
                setTimeout(function () {
                    $('.checked').removeClass('show-checked');
                }, 800)
            },
            error: function () {
                console.log('error')
            }
        });
    });

    $(document).on("click", ".btn-create-order", function () {
        let location = window.location.origin;
        let url = location + '/details_product/add_to_cart';
        let mini_cart_url = location + '/add_to_mini_cart';
        let data = {};
        data['id'] = $(this).attr('data-id');
        data['shop_id'] = $(this).attr('data-shop');
        data['customer_id'] = $(this).attr('data-customer');
        data['id_attribute'] = id_attribute;
        data['quantity'] = cart_quantity;
        if (data['customer_id'] === '') {
            location.pathname = "login";
        } else {
            if (data['id_attribute'] == null) {
                alert("bạn chưa chọn size");
            } else {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    type: "POST",
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        if (data.status) {
                            let width = $(window).width();
                            $('.icon_cart .count-item').text(data.items);
                            setTimeout(function () {
                                $.ajax({
                                    url: mini_cart_url,
                                    type: 'GET',
                                    dataType: 'html',
                                    success: function (dataMiniCart) {
                                        $('.mini-cart').html(dataMiniCart);
                                    }
                                });
                            }, 500);
                            if (width < 1880) {
                                setTimeout(function () {
                                    $('.mini-cart').addClass('show-mini-cart');
                                    $('#product-page .before').addClass("show-before");
                                }, 800);
                            }
                        } else {
                            swal("Thông báo", data.msg, "error");
                        }
                    },
                    error: function () {
                        console.log('error')
                    }
                });
            }
        }
    });

    $('#product-page .before').click(function () {
        $(this).removeClass("show-before");
        $('.mini-cart').removeClass('show-mini-cart');
    });
    //-----------------------//

    $(document).on("click", "btn-upload-dropship", function () {
        let data = {};
        data['url'] = $(this).attr('data-url');
        data['shop_id'] = $(this).attr('data-shop');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    swal("Thành công", data.msg, "success");
                } else {
                    alert('Vui lòng thử lại');
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });

    let count_like_1 = $('.count_like').val();
    let count_like = parseInt(count_like_1);

    //bỏ thích
    $(document).on("click", ".dislike-product", function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['id'] = $(this).attr('data-id');
        data['customer_id'] = $(this).attr(' data-customer');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    // swal("Thành công", data.msg, "success");
                    $('.icon_like').html(data.html);
                    // $('.show_like').addClass('hide_img');
                    // $('.dislike-product').addClass('hide_img_dislike');
                    count_like -= 1;
                    $('.text_count_like_1').html('('+count_like+')');
                    $('.text_like_1').html('Thích');
                } else {
                    swal("Lỗi", data.msg, "error");
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });


    //like
    $(document).on("click", ".like-product", function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['id'] = $(this).attr('data-id');
        data['customer_id'] = $(this).attr('data-customer');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    $('.icon_like').html(data.html);
                    // $('.show_like').addClass('hide_img');
                    // $('.like-product').addClass('hide_img_like');
                    count_like += 1;
                    $('.text_count_like_1').html('('+count_like+')');
                    $('.text_like_1').html('Đã thích');
                } else {
                    swal("Lỗi", data.msg, "error");
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });

    $(document).on("click", ".container-atc .checkout", function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['id'] = $(this).attr('data-id');
        data['shop_id'] = $(this).attr('data-shop');
        data['customer_id'] = $(this).attr('data-customer');
        data['id_attribute'] = id_attribute;
        data['quantity'] = cart_quantity;
        if (data['customer_id'] === '') {
            location.pathname = "login";
        } else {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            if (data['id_attribute'] == null) {
                alert("bạn chưa chọn size");
            } else {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        if (data.status) {
                            location.pathname = "cart";
                        } else {
                            swal("Thông báo", data.msg, "error");
                        }
                    },
                    error: function () {
                        console.log('error')
                    }
                });
            }
        }
    });

    $(document).on("click", ".link-button a.button", function (ev) {
        ev.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        let id = $(this).attr("data-user-id");
        if (id === '') {
            window.location.href = window.location.origin + '/login';
        } else {
            if ($('.popup-chat').hasClass("active")) {
                $('.popup-chat').removeClass("active");
            } else {
                let url = $(this).attr('data-chat');
                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: 'html',
                    success: function (data) {
                        $('.popup-chat').html(data);
                        $('.chat-content').animate({
                            scrollTop: $('.chat-content')[0].scrollHeight
                        }, 0);
                    },
                    complete: function () {
                        $('.popup-chat').addClass("active");
                    },
                    error: function () {
                        console.log('error')
                    }
                });
            }
        }
    });

    $(document).on("click", ".popup-chat .item-chat-data a", function (ev) {
        ev.preventDefault();
        let url = $(this).attr("href");
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $('.popup-chat').html(data);
                $('.chat-content').animate({
                    scrollTop: $('.chat-content')[0].scrollHeight
                }, 0);
            },
            complete: function () {
                $('.popup-chat').addClass("active");
            },
            error: function () {
                console.log('error')
            }
        });
    });

    $(document).on('click', '.btn-send', function () {
        let mes = $('.input-messenger').val();
        let group_id = $('.customer-active').attr('data-group-id');
        if (mes) {
            let avt = $(".btn-popup-menu img").attr("src");
            $('.input-messenger').val('');
            $('.list-messenger-' + group_id).append('<div class="flex ctn-chat-content shop-chat">\n' +
                '                            <div class="text-chat">\n' +
                '                                <label>' + mes + '</label>\n' +
                '                            </div>\n' +
                '                        <div class="img-customer">\n' +
                '                            <img src="' + avt + '" class="img-avt-chat">\n' +
                '                        </div>\n' +
                '                    </div>');
            $('.list-messenger-' + group_id).animate({
                scrollTop: $('.list-messenger-' + group_id)[0].scrollHeight
            }, 0);
            $('.messenger-last-' + group_id).text(mes);
            let url = window.location.origin + "/chat/send";
            let data = {};
            data['messenger'] = mes;
            data['user_id'] = $('.customer-active').attr('data-id');
            data['group_messenger_id'] = $('.customer-active').attr('data-group-id');
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: data,
                success: function (data) {
                    $(".count-chat").text(data.count);
                },
                error: function () {
                    console.log('error')
                }
            });
        }
    });

    $(document).on("click", ".close-popup-chat", function () {
        $('.popup-chat').removeClass("active");
    });

    let max_width = 0;
    $(".container-size .content-size").each(function () {
        let width = $(this).find(".size").outerWidth();
        if (max_width === 0){
            max_width = width;
        }
        if (max_width < width){
            max_width = width;
        }
    });
    $(".container-size .content-size .size").css("width", max_width);
    //--------------style review---------------//
    let total = parseInt($('#data-review').attr('data-total-review'));
    let five = parseInt($('#data-review').attr('data-five-star'));
    let four = parseInt($('#data-review').attr('data-four-star'));
    let three = parseInt($('#data-review').attr('data-three-star'));
    let two = parseInt($('#data-review').attr('data-two-star'));
    let one = parseInt($('#data-review').attr('data-one-star'));
    $('.five-star .bg-total-review').css('width', (five / total) * 100 + '%');
    $('.for-star .bg-total-review').css('width', (four / total) * 100 + '%');
    $('.three-star .bg-total-review').css('width', (three / total) * 100 + '%');
    $('.tow-star .bg-total-review').css('width', (two / total) * 100 + '%');
    $('.one-star .bg-total-review').css('width', (one / total) * 100 + '%');
});

//----------------slide image product----------------------//
document.addEventListener( 'DOMContentLoaded', function () {
   var secondarySlider = new Splide( '#secondary-slider', {
        fixedWidth : 100,
        height     : 80,
        gap        : 6,
        rewind     : true,
        cover      : true,
        focus      : 'center',
        pagination : false,
       isNavigation: true,
    } );

   var primaryslider = new Splide( '#primary-slider',{
       type       : 'fade',
       cover      : true,
       rewind     : true,
       pagination : false,
       heightRatio: 0.5,
       height : 400,
       arrows: false
    } );
    primaryslider.sync( secondarySlider );
    primaryslider.mount();
    secondarySlider.mount();
} );
