let map;
let marker;
let myLatLng = {lat: 21.0452522, lng: 105.8269441};
let geocoder;
let infoWindow;
let address_maps;

function initMap() {
    geocoder = new google.maps.Geocoder();
    map = new google.maps.Map(document.getElementById("map"), {
        center: myLatLng,
        zoom: 15,
    });
    marker = new google.maps.Marker({
        position: myLatLng,
        map,
    });
    marker.setMap(map);
    infoWindow = new google.maps.InfoWindow();
    geocoderFunc(myLatLng);
};

function geocoderFunc(latLng) {
    // geocoder.geocode({
    //     'latLng': latLng
    // }, function (results, status) {
    //     if (status == google.maps.GeocoderStatus.OK) {
    //         if (results[0]) {
    //             address_maps = results[0].formatted_address;
    //             infoWindow.setContent(results[0].formatted_address);
    //             infoWindow.open(map, marker);
    //         }
    //     }
    // });
}

$(document).ready(function () {
    // click on map and set you marker to that position
    // google.maps.event.addListener(map, 'click', function (event) {
    //     marker.setPosition(event.latLng);
    //     myLatLng = event.latLng;
    //     geocoderFunc(event.latLng);
    // });

    $(document).on('click', '.btn-add-new-address', function () {
        let url = $(this).attr('data-url');
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'json',
            success: function (data) {
                $('#modalAddress').html(data.html);
                // myLatLng['lat'] = $('.btn-add-maps').attr('data-lat');
                // myLatLng['lng'] = $('.btn-add-maps').attr('data-log');
                $('#modalAddress').show();
            },
            error: function () {
                console.log('error')
            }
        });
    });

    // $(document).on('click', '.btn-complete-maps', function () {
    //     $('#addressMaps').val(address_maps);
    //     if (address_maps) {
    //         $('.position-address').text(address_maps)
    //     }
    //     $('#modalMaps').hide();
    //     $('#modalAddress').show();
    // });

    $(document).on('click', '.btn-add-maps', function () {
        let city = $('#citySelect').val();
        let district = $('#districtSelect').val();
        let ward = $('#wardSelect').val();
        let name = $('#name').val();
        let phone = $('#phone').val();
        let addressMaps = $('#addressMaps').val();
        let detailsAddress = $('#address').val();
        let check = $("#default").prop("checked");
        if (city && district && ward && phone && name && detailsAddress) {
            // if (city && district && ward && phone && name && addressMaps && detailsAddress) { //Xóa dòng bên trên, bỏ comment dòng này để trở về ban đầu
            let url = $(this).attr('data-url');
            let data = {};
            data['city_id'] = city;
            data['district_id'] = district;
            data['ward_id'] = ward;
            data['name'] = name;
            data['phone'] = phone;
            // data['address_maps'] = addressMaps;
            data['address'] = detailsAddress;
            // data['longitude'] = myLatLng.lng;
            // data['latitude'] = myLatLng.lat;
            data['id'] = $(this).attr('data-id');
            if (check) {
                data['default'] = 1;
            } else {
                data['default'] = 0;
            }
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: data,
                beforeSend: function () {
                    $('#modalAddress').hide();
                    showLoading();
                },
                success: function (data) {
                    if (data.status) {
                        swal('Thông báo', 'Thêm địa chỉ thành công', 'success');
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000)
                    } else {
                        $('#modalError').show();
                        setTimeout(function () {
                            $('#modalError').fadeOut("slow");
                        }, 1000)
                    }
                },
                error: function () {
                    console.log('error');
                    $('#modalError').show();
                    setTimeout(function () {
                        $('#modalError').fadeOut("slow");
                    }, 1000)
                },
                complete: function () {
                    hideLoading();
                }
            });
        } else {
            if (!$('#citySelect').val()) {
                $('#citySelect').addClass('border-red');
            }
            if (!$('#districtSelect').val()) {
                $('#districtSelect').addClass('border-red');
            }
            if (!$('#wardSelect').val()) {
                $('#wardSelect').addClass('border-red');
            }
            if (!$('#name').val()) {
                $('#name').addClass('border-red');
            }
            if (!$('#phone').val()) {
                $('#phone').addClass('border-red');
            }
            // if (!$('#addressMaps').val()) {
            //     $('.form-maps').addClass('border-red');
            // }
            if (!$('#address').val()) {
                $('#address').addClass('border-red');
            }
        }
    });

    $(document).on('click', '.btn-delete', function () {
        $('.btn-confirm-delete').attr('data-url', $(this).attr('data-url'));
        $('#modalDelete').show();
    });

    $(document).on('click', '.btn-close-modal-delete', function () {
        $('#modalDelete').hide();
    });

    $(document).on('click', '.btn-show-maps', async function () {
        if ($('#citySelect').val() && $('#districtSelect').val() && $('#wardSelect').val()) {
            city_selected = $("#citySelect option:selected").text();
            district_selected = $("#districtSelect option:selected").text();
            ward_selected = $("#wardSelect option:selected").text();
            let address = ward_selected + ',' + district_selected + ',' + city_selected;
            // await getAddress(address);
            $('#modalAddress').hide();
            $('#modalMaps').show();
        } else {
            if (!$('#citySelect').val()) {
                $('#citySelect').addClass('border-red');
            }
            if (!$('#districtSelect').val()) {
                $('#districtSelect').addClass('border-red');
            }
            if (!$('#wardSelect').val()) {
                $('#wardSelect').addClass('border-red');
            }
        }
    });

    // function getAddress(address) {
    //     geocoder.geocode({'address': address}, function (results, status) {
    //         if (status === 'OK') {
    //             address_maps = address;
    //             myLatLng = results[0].geometry.location;
    //             map.setCenter(results[0].geometry.location);
    //             marker.setPosition(results[0].geometry.location);
    //             infoWindow.setContent(address);
    //             infoWindow.open(map, marker);
    //         }
    //     });
    // }
});
