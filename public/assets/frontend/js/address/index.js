$(document).ready(function () {
    $(document).on('change', '.check-default', function () {
        let parents = $(this).closest(".container");
        parents.find(".border-bottom-view").each(function () {
           $(this).find(".check-default").prop("checked", false);
        });
        $(this).prop("checked", true);
        let url = $(this).attr('data-url');
        let data = {}
        data['id'] = $(this).attr('data-id');
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'json',
            success: function (data) {

            },

        });
    });
    let city_selected;
    let district_selected;
    let ward_selected;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on('click', '.btn-back-modal-address', function () {
        $('#modalAddress').hide();
    });

    $(document).on('click', '.btn-back-modal-maps', function () {
        $('#modalMaps').hide();
        $('#modalAddress').show();
    });

    $(document).on('change', '.form-address', function () {
        let url = $(this).attr('data-url');
        let value = $(this).val();
        url += value;
        let val = $(this).attr('data-value');
        $(this).removeClass('border-red');
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'json',
            success: function (data) {
                if (val === 'city') {
                    $('#districtSelect').html(data.html);
                } else {
                    $('#wardSelect').html(data.html);
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });

    $(document).on('click', '.btn-confirm-delete', function () {
        let url = $(this).attr('data-url');
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'json',
            beforeSend: function () {
                $('#modalDelete').hide();
                showLoading();
            },
            success: function (data) {
                $('#modalSuccess').show();
                setTimeout(function () {
                    $('#modalSuccess').fadeOut("slow");
                    window.location.reload();
                }, 1000)
            },
            error: function () {
                console.log('error');
                $('#modalError').show();
                setTimeout(function () {
                    $('#modalError').fadeOut("slow");
                }, 1000)
            },
            complete: function () {
                hideLoading();
            }
        });
    });

});
