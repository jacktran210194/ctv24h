$(document).ready(function () {
    $('.slick-voucher').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        prevArrow: '<button class="slide-arrow arrow prev-arrow"><img src="Icon/ElectronicDevice/Group.png"></button>',
        nextArrow: '<button class="slide-arrow arrow arrow-right next-arrow"><img src="Icon/ElectronicDevice/Vector.png"></button>'
    });
    $('.content-slide').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        prevArrow: '<button class="slide-arrow arrow prev-arrow"><img src="Icon/ElectronicDevice/Group.png"></button>',
        nextArrow: '<button class="slide-arrow arrow next-arrow"><img src="Icon/ElectronicDevice/Vector.png"></button>'
    });
});
