$('.section-product').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    prevArrow: '<button class="slide-arrow prev-arrow p-0"><img src="icon/promotion/icon_prev.png"></button>',
    nextArrow: '<button class="slide-arrow next-arrow p-0"><img src="icon/promotion/icon_next.png"></button>'
});
