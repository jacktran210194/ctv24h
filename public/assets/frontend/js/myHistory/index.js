$(document).ready(function () {
    let product_id;
    let customer_id;
    let ratings_star;
    let comment;
    let commentTara = '';
    let category;
    $('#textarea textarea').keyup(function () {
        commentTara = $(this).val();
        $('#textarea label').css("display", "none");
    });
    $(".btn-review-product").click(function () {
        // $(this).parent().addClass("show-popup");
        // product_id = $(this).attr("data-product-id");
        // customer_id = $(this).attr("data-customer-id");
        // category = $(this).attr("data-category");
        let url = $(this).attr('data-url');
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $(".show_list_order").html(data);
                $('.list_order').show();
            },
            error: function () {
                console.log('error')
            }
        });
    });
    $(document).on('click','.check_order_detail',function () {
        let order_detail_id = $(this).attr('data-id');
        let order_id = $(this).attr('data-order');
        let product_id = $(this).attr('data-product-id');
        let product_name = $(this).attr('data-name');
        let product_image = $(this).attr('data-image');
        let value_1 = $(this).attr('data-value_1');
        let value_2 = $(this).attr('data-value_2');
        if($(this).is(':checked')){
            $('.btn_click_review').prop('disabled',false);
            $('.review_product_image').attr('src',product_image);
            $('.review_product_name').text(product_name);
            $('.review_value_1').text(value_1);
            $('.review_value_2').text(value_2);
            $('.btn_click_review').attr('data-order',order_id);
            $('.btn_click_review').attr('data-order-detail',order_detail_id);
            $('.btn_click_review').attr('data-product-id',product_id);
        }
    });
    $(document).on('click','.btn_back_review',function () {
       $('.list_order').hide();
    });

    $(document).on('click','.btn_click_review',function () {
        $('.btn-up-review').attr('data-order',$(this).attr('data-order'));
        $('.btn-up-review').attr('data-order-detail',$(this).attr('data-order-detail'));
        $('.btn-up-review').attr('data-product-id',$(this).attr('data-product-id'));
        $('.container-popup-review').addClass('show_popup_review');
    });

    $(document).on("click", ".btn-star-rating", function () {
        ratings_star = $(this).attr("data-ratings");
        let rating = $(this).attr("data-ratings");
        $(".btn-star-rating").each(function () {
            var ratings = $(this).attr("data-ratings");
            if (ratings <= rating) {
                $(this).addClass("active");
            } else {
                $(this).removeClass("active");
            }
        });
    });
    $(".btn-comment").click(function () {
        $(".btn-comment").removeClass("selected");
        $(this).addClass("selected");
        comment = $(this).text();
    });

    $(document).on("click", ".btn-up-review", function () {
        if (ratings_star == null) {
            alert("Bạn chưa chọn đánh giá")
        } else if (comment == null && commentTara === "") {
            alert("Bạn chưa bình luận")
        } else {
            let url = $(this).attr("data-url");
            let order_detail = $(this).attr("data-order-detail");
            let order_id = $(this).attr('data-order');
            category = $('.review_value_1').text() + '-' + $('.review_value_2').text();
            let form_data = new FormData();
            form_data.append('order_detail_id', order_detail);
            form_data.append('order_id',order_id);
            form_data.append('customer_id', customer_id);
            form_data.append('product_id', $(this).attr('data-product-id'));
            form_data.append('ratings_star', ratings_star);
            form_data.append('comment', comment);
            form_data.append('customer_comment', commentTara);
            form_data.append('category_product', category);
            // form_data.append('image',JSON.stringify(list_file));
            form_data.append('image', $('#myFile')[0].files[0]);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        swal("Thông báo", data.msg, "success");
                        location.reload();
                    } else {
                        swal("Thông báo", data.msg, "error");
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        }
    });

    $(document).on("click", ".upload-files", function (ev) {
        $("#myFile").click();
    });
    $(".btn-back").click(function () {
        $(".container-popup-review").removeClass("show_popup_review");
    });

    //Đã nhận đc hàng
    $(document).on('click', '.btn_check_product', function () {
        let id = $(this).attr('data-order-detail-id');
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('id', id);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status) {
                    swal("Thông báo", data.msg, "success");
                    window.location.href = data.url;
                } else {
                    swal("Thông báo", data.msg, "error");
                }
            },
            error: function () {
                console.log('error')
            },
        });
    });

//    POP UP HUỶ ĐƠN HÀNg
    $(document).on('click','.btn_cancel_order',function () {
        let cancel = $(this).attr('data-order');
        $('.btn-confirm').attr('data-order',cancel);
        $('.popup-edit-payment').addClass('show-popup');
    });

    $(document).on('click','.name_payment',function () {
        let id_cancel = $(this).attr('data-id');
        $('.btn-confirm').attr('data-cancel-id',id_cancel);
        $('.popup-edit-payment').addClass('show-popup');
    });

    $(document).on('click', '.btn-cancel', function () {
        $('.popup-edit-payment').removeClass('show-popup');
    });

    $(document).on("click", ".btn-confirm", function () {
        let url = $(this).attr('data-url');
        let form_data = new FormData();
        form_data.append('order_id', $(this).attr('data-order'));
        form_data.append('cancel_reason',$(this).attr('data-cancel-id'));
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status) {
                    $('.popup-edit-payment').removeClass('show-popup');
                    swal("Thông báo", data.msg, "success");
                    window.location.href = data.url;
                } else {
                    swal("Thông báo", data.msg, "error");
                }
            },
            error: function () {
                console.log('error')
            },
        });
    });

    $(document).on("click", ".name-payment-unit .selected", function () {
        let name_payment = $(this).attr('data-name');
        $('.name-payment-show').text(name_payment);
        if ($(this).hasClass("checked")) {
            $(this).removeClass("checked");
            $(this).parent().removeClass("rm-border");
            $(this).parent().next().removeClass("fast");
        } else {
            $(".name-payment-unit .selected").removeClass("checked");
            $(".name-payment-unit").removeClass("rm-border");
            $(".dropdown-payment").removeClass("fast");
            $(this).addClass("checked");
            $(this).parent().addClass("rm-border");
            $(this).parent().next().addClass("fast");
        }
        // let payment = $(this).attr('data-id');
        // $('.checkout-cart').attr('data-payment', payment);
        // $('.checkout-cart-customer-ctv').attr('data-payment', payment);
    });

});

// function readURL(input) {
//     $('#video-tag').css('display', 'none');
//     $('#blah').css('display', 'none');
//     if (input.files && input.files[0]) {
//         let reader = new FileReader();
//         reader.onload = function (e) {
//             if (input.files[0]["type"] == "video/mp4") {
//                 let video = '<div data-file="' + e.target.result + '" class="data-file">' +
//                     '<p class="remove-file"><i class="fas fa-times"></i></p>'
//                     + '<video autoplay muted controls>' +
//                     '<source src="' + e.target.result + '">' +
//                     '</video>' + '</div>';
//                 $('.show-popup .content-img-video-review').append(video);
//             } else {
//                 let img = '<div data-file="' + e.target.result + '" class="data-file">' +
//                     '<p class="remove-file"><i class="fas fa-times"></i></p>'
//                     + '<img src="' + e.target.result + '">' + '</div>';
//                 $('.show-popup .content-img-video-review').append(img);
//             }
//         }.bind(this)
//         reader.readAsDataURL(input.files[0]);
//     }
// }

function readURL(input) {
    const videoSrc = document.querySelector("#video-source");
    const videoTag = document.querySelector("#video-tag");
    $('#video-tag').css('display', 'none');
    $('#blah').css('display', 'none');
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            if (input.files[0]["type"] == "video/mp4") {
                videoSrc.src = e.target.result;
                videoTag.load();
                $('.icon-upload').css('display', 'none');
                $('#video-tag').css('display', 'block');
            } else {
                $('#blah')
                    .attr('src', e.target.result)
                    .css('display', 'block');
                $('.icon-upload').css('display', 'none');
            }
        }.bind(this)
        reader.readAsDataURL(input.files[0]);
    }
}
