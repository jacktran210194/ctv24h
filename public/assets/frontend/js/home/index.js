function slideImage() {
    let number_content = $('.container-section .product').length;
    let number_slide_trend = $('.slide-section-trend .container-slide').length;
    if (number_content > 8){
        $('.container-section').slick({
            slidesToShow: 8,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            nextArrow: '<button class="next-arrow btn-arrow next-arrow-catagory"><img style="width: 12px" class="lazy" alt="" src="Icon/home/Danh_muc/next_arrow.png"></button>',
            prevArrow: '<button class="prev-arrow btn-arrow prev-arrow-catagory"><img style="width: 12px" class="lazy" alt="" src="Icon/home/Danh_muc/prev_arrow.png"></button>'
        });
    }
    $('.slide-section-fash').slick({
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 1,
        nextArrow: $('.prev-arrow-fash'),
        prevArrow: $('.next-arrow-fash'),
    });
    $('.slide-section-tu-khoa').slick({
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 1,
        nextArrow: $('.prev-arrow-key-word'),
        prevArrow: $('.next-arrow-key-word'),
    });
    if (number_slide_trend > 6){
        $('.slide-section-trend').slick({
            speed: 300,
            slidesToShow: 6,
            slidesToScroll: 1,
            nextArrow: $('.prev-arrow-trend'),
            prevArrow: $('.next-arrow-trend'),
        });
    }
    $('.slide-banner').slick({
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 2000,
        autoplay: true,
    });
    $('.slide-section').slick({
        slidesToShow: 5,
        speed: 300,
        slidesToScroll: 1,
        nextArrow: $('.prev-arrow-auction'),
        prevArrow: $('.next-arrow-auction'),
    });
    let count_content = $(".container-info-product .info-product").length;
    if (count_content > 5){
        $('.container-info-product').slick({
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            nextArrow: "<button class=\"next-arrow btn-arrow btn-arrow-small prev-arrow-xu-huong\"><img style=\"width: 7px\" src=\"Icon/home/Danh_muc/next_arrow.png\"></button>",
            prevArrow: "<button class=\"prev-arrow btn-arrow btn-arrow-small next-arrow-xu-huong\"><img style=\"width: 7px\" src=\"Icon/home/Danh_muc/prev_arrow.png\"></button>",
        });
    }
    $('.container-gallery-product').slick({
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        nextArrow: $('.prev-arrow-gallery'),
        prevArrow: $('.next-arrow-gallery'),
    });
    $('.container-gallery-product-search').slick({
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        nextArrow: $('.prev-arrow-search'),
        prevArrow: $('.next-arrow-search'),
    });
    $('.slide-section-ctv').slick({
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        nextArrow: $('.prev-arrow-ctv'),
        prevArrow: $('.next-arrow-ctv'),
    });
    $('.slide-section-selling').slick({
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 1,
        nextArrow: $('.prev-arrow-selling'),
        prevArrow: $('.next-arrow-selling'),
    });

    let check_slide_section_sponsor = $('.slide-section-sponsor .content').length;
    if (check_slide_section_sponsor > 6){
        $('.slide-section-sponsor').slick({
            speed: 300,
            slidesToShow: 6,
            slidesToScroll: 1,
            nextArrow: '<button class="next-arrow btn-arrow btn-arrow-small prev-arrow-sponsor"><object data="Icon/base/next_arrow.svg" width="7px"></object></button>',
            prevArrow: '<button class="prev-arrow btn-arrow btn-arrow-small next-arrow-sponsor"><object data="Icon/base/pr_arrow.svg" width="7px"></object></button>',
        });
    }
}

$(window).on("load", function () {
    $(".loader-wrapper").fadeOut("slow");
});
$(document).ready(function () {
    $('.dropdown-item').click(function (event) {
        event.preventDefault();
        var img = $(this).attr('data-src');
        var text = $(this).text();
        $('#dropdownMenuButton .img-nation img').attr('src', img);
        $('#dropdownMenuButton span').text(text);
    });
    //------------------show and hide popup ratings -----------------//
    $('.preventdefault').click(function (ev) {
        ev.preventDefault();
        $('.popup-ratings').addClass('show-popup-ratings');
    });
    $('.close-popup').click(function () {
        $('.popup-ratings').removeClass('show-popup-ratings');
    });
    $(".form-search-section form").submit(function (event) {
        event.preventDefault();
        $('.popup-ratings').addClass('show-popup-ratings');
    });
    //-----------------show popup check order----------//
    $('.check-order').click(function () {
        if ($('.popup-check-order').hasClass("active")) {
            $('.popup-check-order').removeClass("active");
        } else {
            $('.popup-check-order').addClass("active");
        }
    });
    //-----------------show popup chat-----------------//
    $('.btn-chat').click(function (ev) {
        ev.preventDefault();
        $('.popup-ratings').addClass('show-popup-ratings');
        // if ($('.popup-chat').hasClass("active")) {
        //     $('.popup-chat').removeClass("active");
        // } else {
        //     $.ajaxSetup({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         }
        //     });
        //     let url = $(this).attr('data-url');
        //     let data = {};
        //     data['messenger'] = mes;
        //     data['user_id'] = $('.customer-active').attr('data-id');
        //     data['group_messenger_id'] = $('.customer-active').attr('data-group-id');
        //     $.ajax({
        //         url: url,
        //         type: "GET",
        //         dataType: 'json',
        //         success: function (data) {
        //         },
        //         error: function () {
        //             console.log('error')
        //         }
        //     });
        //     $('.popup-chat').addClass("active")
        // }
        // ;
    });
    //-------------show menu-popup --------//
    $('.btn-popup-menu').click(function () {
        if ($(this).next().hasClass("active")) {
            $(this).next().removeClass("active");
        } else $(this).next().addClass("active");
    });
    //-------------slide iamge ---------------//
    slideImage();
    //Show popup ncc
    $(document).on("click", ".channel-ncc", function (ev) {
        ev.preventDefault();
        $(".popup-register").addClass("active");
    });
    $(document).on("click", ".btn-cancel", function () {
        $(".popup-register").removeClass("active");
    });
    //show popup ctv
    $(document).on("click", ".channel-ctv", function (ev) {
        ev.preventDefault();
        $(".popup-register-ctv").addClass("active");
    });
    $(document).on("click", ".btn-cancel", function () {
        $(".popup-register-ctv").removeClass("active");
    });

    //Check order
    // $(document).on("click", ".btn_check_order", function () {
    //     let url = $(this).attr('data-url');
    //     let data = {};
    //     data['email_check'] = $('#email_check').val();
    //     data['order_code'] = $('#order_code').val();
    //     data['customer_id'] = $(this).attr('data-customer');
    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    //     });
    //     $.ajax({
    //         type: 'POST',
    //         url: url,
    //         data: data,
    //         // beforeSend: function () {
    //         //     showLoading();
    //         // },
    //         success: function (data) {
    //             if (data.status) {
    //                 swal("Thông báo", data.msg, "success");
    //                 window.location.href = data.url;
    //             } else {
    //                 swal("Thông báo", data.msg, "error");
    //             }
    //         },
    //         error: function () {
    //             console.log('error')
    //         },
    //         // complete: function () {
    //         //     hideLoading();
    //         // }
    //     });
    // });
    $(document).on('click', '.btn_load_more', function () {
        $('.load_more_city').show();
        $(this).hide();
        $('.btn_hide_more').show();
    });

    $(document).on('click', '.btn_hide_more', function () {
        $('.load_more_city').hide();
        $(this).hide();
        $('.btn_load_more').show();
    });

    $(document).on('click','.clear-all',function () {
        location.reload();
        $('.check_category').prop('checked',false);
        $('.check_place').prop('checked',false);
        $('.check_shipping').prop('checked',false);
        $('.check_price_1').val('');
        $('.check_price_2').val('');
        $('.check_type_shop').prop('checked',false);
        $('.check_type_product').prop('checked',false);
        $('.check_rating').prop('click',false);
        $('.check_service').prop('checked',false);
    });
});

