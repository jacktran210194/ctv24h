function styleImageBanner() {
    var left = $('.container-banner').offset().left - 152;
    var top = $('.container-banner').offset().top + 7;
    $('.image_banner_vertical').css("top", top);
    $('.image-banner-left').css('left', left);
    $('.image-banner-right').css('right', left);
    $('.btn-main.btn-chat').css('right', left);
}
$(document).ready(function () {
    //--------------------------//
    showTime();
    //---------------------style image bannaer vertical---------------------//
    styleImageBanner();
    //-----------------------//
    setTimeout(function () {
        $('.image_banner_vertical').css("opacity", "1");
    }, 3000);
    //-------------slick js ----------------//
    var check = $(".slide-logo a").length;
    if (check >= 6){
        $('.slide-logo').slick({
            speed: 300,
            slidesToShow: 6,
            slidesToScroll: 1,
            nextArrow: "<button class=\"next-arrow btn-arrow prev-arrow-logo\"><object data=\"Icon/base/next_arrow.svg\"></object></button>",
            prevArrow: "<button class=\"prev-arrow btn-arrow next-arrow-logo\"><object data=\"Icon/base/pr_arrow.svg\"></object></button>",
        });
    }
});
function showTime(){
    var date = new Date();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();

    h = (h < 10) ? "0" + h : h;
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;

    document.getElementById("showtime-h").innerText = h;
    document.getElementById("showtime-h").textContent = h;
    document.getElementById("showtime-m").innerText = m;
    document.getElementById("showtime-m").textContent = m;
    document.getElementById("showtime-s").innerText = s;
    document.getElementById("showtime-s").textContent = s;

    setTimeout(showTime, 1000);

}
