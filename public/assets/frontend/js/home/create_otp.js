$(document).ready(function () {
    let verificationId;
    $(document).on('change', '#myRange_ncc', async function () {
        let value = $(this).val();
        if (value > 50) {
            $('#recaptcha-container_ncc').show();
            let applicationVerifier = new firebase.auth.RecaptchaVerifier(
                'recaptcha-container_ncc');
            let phone = $('.input-phone').val();
            let fs = phone.slice(0, 3);
            if (fs !== '+84') {
                phone = phone.slice(1, phone.length);
                phone = '+84' + phone;
            }
            let provider = new firebase.auth.PhoneAuthProvider();
            try {
                verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                $('#recaptcha-container_ncc').hide();
                $('.form-otp_ncc').prop('disabled', false);
            } catch (e) {
                console.log('e');
                swal("Thất bại", "Số điện thoại vừa nhập không đúng !", "error");
                setTimeout(function () {
                    location.reload();
                }, 1500);
            }
        }
    });

    $(document).on('click', '.btn-confirm-ncc', async function () {
        try {
            let verificationCode = $('.form-otp_ncc').val();
            let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
            await firebase.auth().signInWithCredential(phoneCredential);
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('business_license', $('#business_license').val());
            form_data.append('customer_id', $(this).attr('data-id'));
            form_data.append('address', $('#address_ncc').val());
            form_data.append('bank_name', $('#bank_name').val());
            form_data.append('bank_number', $('#bank_number').val());
            form_data.append('bank_account', $('#bank_account').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        swal("Thông báo", data.msg, "success");
                        window.location.href = data.url;
                    } else {
                        swal("Thông báo", data.msg, "error");
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        } catch (e) {
            swal("Thất bại", "Mã OTP không chính xác !", "error");
        }
    });



    ///////////////CTV
    $(document).on('change', '#myRange_ctv', async function () {
        let value = $(this).val();
        if (value > 50) {
            $('#recaptcha-container_ctv').show();
            let applicationVerifier = new firebase.auth.RecaptchaVerifier(
                'recaptcha-container_ctv');
            let phone = $('.input-phone_ctv').val();
            let fs = phone.slice(0, 3);
            if (fs !== '+84') {
                phone = phone.slice(1, phone.length);
                phone = '+84' + phone;
            }
            let provider = new firebase.auth.PhoneAuthProvider();
            try {
                verificationId = await provider.verifyPhoneNumber(phone, applicationVerifier);
                $('#recaptcha-container_ctv').hide();
                $('.form-otp_ctv').prop('disabled', false);
            } catch (e) {
                console.log('e');
                swal("Thất bại", "Số điện thoại vừa nhập không đúng !", "error");
                setTimeout(function () {
                    location.reload();
                }, 1500);
            }
        }
    });

    $(document).on('click', '.btn-confirm-ctv', async function () {
        try {
            let verificationCode = $('.form-otp_ctv').val();
            let phoneCredential = await firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
            await firebase.auth().signInWithCredential(phoneCredential);
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('customer_id', $(this).attr('data-id'));
            form_data.append('address', $('#address_ctv').val());
            form_data.append('bank_name', $('#bank_name_ctv').val());
            form_data.append('bank_number', $('#bank_number_ctv').val());
            form_data.append('bank_account', $('#bank_account_ctv').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status) {
                        swal("Thông báo", data.msg, "success");
                        window.location.href = data.url;
                    } else {
                        swal("Thông báo", data.msg, "error");
                    }
                },
                error: function () {
                    console.log('error')
                },
            });
        } catch (e) {
            swal("Thất bại", "Mã OTP không chính xác !", "error");
        }
    });
});
