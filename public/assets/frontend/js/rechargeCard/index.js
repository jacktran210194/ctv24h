$(document).ready(function () {
    //---------------------stype input range------//
    var range = $(".input-range .range").attr("data-range");
    var width = (range/2500)*100;
    if (width > 100){
        $(".range .before").css("width","100%" );
        $(".range .after").css("left", "100%" );
    }else {
        $(".range .before").css("width", width + "%" );
        $(".range .after").css("left", width + "%" );
    }

    //-------------------------------------------//
    $('.option-popup .radio').click(function () {
        $('.option-popup .radio').removeClass('active');
        $(this).addClass('active');
    });
    //-------------------//
    $('.btn-current-point').click(function (ev) {
        $('.container-popup-current').addClass('active');
        $('.itp-telephone').css("border","1px solid #ECECEC");
    });
    //------------style popup-------------------//
    var withConfirm = $('.popup-confirm-current').outerWidth();
    var heigthConfirm = $('.popup-confirm-current').outerHeight();
    var withPublic = $('.popup-public').outerWidth();
    var heigthPublic = $('.popup-public').outerHeight();
    var withBank = $('.popup-link-bank').outerWidth();
    var heigthBank = $('.popup-link-bank').outerHeight();
    var left = $(window).width() / 2;
    var top = $(window).height() / 2;
    $('.popup-confirm-current').css({
        top: top - (heigthConfirm / 2),
        left: left - (withConfirm /2)
    });
    $('.popup-public').css({
        top: top - (heigthPublic / 2),
        left: left - (withPublic /2)
    });
    $('.popup-link-bank').css({
        top: top - (heigthBank / 2),
        left: left - (withBank /2)
    });
    //---------------------------//

    $(".btn-change-piont").click(function (ev) {
        var point = $(this).attr("data-point");
        var money = $(this).attr("data-money");
        Cookies.set('point',point, { expires: 1 });
        Cookies.set('money',money, { expires: 1 });
    });
    $(document).on("click",".btn-confirm",function () {
        $('.popup-link-bank').removeClass('active');
        $('.screen-cover').addClass('active');
        let url = $(this).attr('data-url');
        let point = parseInt(Cookies.get('point'));
        let data_post = {};
        data_post['point'] = point;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: data_post,
            dataType: 'json',
            success: function (data) {
                $('.popup-public').addClass('active');
            },
            error: function () {
                console.log('error')
            }
        });
    })
});

function showPopup() {
    $('.screen-cover').addClass('active');
    $('.container-popup-current').removeClass('active');
    $('.popup-link-bank').addClass('active');
    let money = Cookies.get('money');
    $('.popup-link-bank .money').val(money);
}
function confirm() {
    var val = $('.passWord').val();
    if (val == ''){
        alert("bạn chưa điền mật khẩu")
    }else{
        $('.popup-confirm-current').removeClass('active');
        $('.popup-public').addClass('active');
    }
}
function editPopup() {
    $('.container-popup-current').addClass('active');
    $('.popup-confirm-current').removeClass('active');
}
function closedPopup() {
    $('.screen-cover').removeClass('active');
    $('.popup-public').removeClass('active');
    location.reload(true);
}
function cancel() {
    $('.container-popup-current').removeClass('active');
    $('.screen-cover').removeClass('active');
}
function editBank() {
    $('.popup-link-bank').removeClass('active');
    $('.container-popup-current').addClass('active');
}
