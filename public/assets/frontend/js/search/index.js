$('.navigation').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    infinite: true,
    prevArrow: $(".next-arrow"),
    nextArrow: $(".prev-arrow")
});
