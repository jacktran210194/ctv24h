$(document).ready(function () {
    $('.slick-voucher').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: '<button class="slide-arrow prev-arrow p-0"><img src="icon/promotion/icon_prev.png"></button>',
        nextArrow: '<button class="slide-arrow next-arrow p-0"><img src="icon/promotion/icon_next.png"></button>'
    });
    $('.item-slick').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: '<button class="slide-arrow prev-item p-0"><img src="icon/promotion/icon_prev.png"></button>',
        nextArrow: '<button class="slide-arrow next-item p-0"><img src="icon/promotion/icon_next.png"></button>'
    });
    $('.item-slick-2').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: '<button class="slide-arrow prev-item p-0"><img src="icon/promotion/icon_prev.png"></button>',
        nextArrow: '<button class="slide-arrow next-item p-0"><img src="icon/promotion/icon_next.png"></button>'
    });
});

