$(document).ready(function () {
    let place='',category='',shipping='',price_filter_1='',price_filter_2='',category_shop='',status='',review='',services='';
    let type = 0;
    let id_shop = $('.id_shop').val();
    $('.content-filter input').click(function () {
        category = $(this).attr("id");
        type = 1;
        $('.content-filter input').prop("checked", false);
        $(this).prop("checked", true);
        filterProduct();
    });
    $('.filter-place input').click(function () {
        place = $(this).val();
        type = 1;
        $('.filter-place input').prop("checked", false);
        $(this).prop("checked", true);
        filterProduct();
    });
    $(".filter-shipping input").click(function () {
        shipping = $(this).val();
        type = 1;
        $('.filter-shipping input').prop("checked", false);
        $(this).prop("checked", true);
        filterProduct();
    });
    $('.btn-filter').click(function () {
        price_filter_1 = $('.price-filter-1').val();
        price_filter_2 = $('.price-filter-2').val();
        type = 1;
        if (price_filter_1 === "" || price_filter_2 === "" || price_filter_1 < 0 || price_filter_2 <= price_filter_1){
            alert("Nhập sai số tiền");
        }else {
            filterProduct();
        }
    });
    $('.filter-status input').click(function () {
        status = $(this).val();
        type = 1;
        $('.filter-status input').prop("checked", false);
        $(this).prop("checked", true);
        filterProduct();
    });
    $('.filter-ratings').click(function () {
        review = $(this).val();
        type = 1;
        filterProduct();
    });
    $('.filter-catagory .sub-filter').each(function () {
        let count = $(this).find(".dropdown-filter").children().length;
        if (count == 0){
            $(this).css("display", "none");
        }
    });
    $('.sub-filter .title').click(function () {
        $(this).next().toggle("fast");
        $(this).toggleClass("active");
    });
    $(".clear-all-filter").click(function () {
        $('.btn_filter_default').addClass("active");
        $('.check_category').prop('checked', false);
        $('.check_place').prop('checked', false);
        $('.check_shipping').prop('checked', false);
        $('.check_price_1').val('');
        $('.check_price_2').val('');
        $('.check_type_shop').prop('checked', false);
        $('.check_type_product').prop('checked', false);
        $('.check_rating').prop('click', false);
        $('.check_service').prop('checked', false);
        type = 0;
        place = null;category=null;shipping=null;price_filter_1=null;price_filter_2=null;category_shop=null;status=null;review=null;services=null;
        filterProduct();
    });
    function filterProduct() {
        let url = window.location.origin + '/filter_product_shop';
        let data={};
        data['id_shop'] = id_shop;
        data['type'] = type;
        data['category_sub_child'] = category;
        data['place'] = place;
        data['shipping_id'] = shipping;
        data['price_filter_1'] = price_filter_1;
        data['price_filter_2'] = price_filter_2;
        data['status_product'] = status;
        data['ratings'] = review;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'GET',
            url: url,
            dataType:'json',
            data: data,
            success: function (data) {
                $('.product-search .container-section-product').html(data.prod);
            }
        });
    }
});
