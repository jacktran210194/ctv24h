$(document).ready(function () {
    //Bỏ theo dõi shop
    $(document).on("click", ".button-unfollow", function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['id'] = $(this).attr('data-id');
        data['customer_id'] = $(this).attr('data-customer-id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    $('.follow-shop').html(data.html);
                    $(".follow_shop .number").html(data.number_follow);
                } else {
                    alert("Vui lòng thử lại");
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });
    //Theo dõi shop
    $(document).on("click", ".button-follow", function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['shop_id'] = $(this).attr('data-id');
        data['customer_id'] = $(this).attr('data-customer-id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    $('.follow-shop').html(data.html);
                    $(".follow_shop .number").html(data.number_follow);
                } else {
                    alert("Vui lòng thử lại");
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });

    //Bỏ thích shop
    $(document).on("click", ".button-dislike", function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['id'] = $(this).attr('data-id');
        data['customer_id'] = $(this).attr('data-customer-id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    $('.like-shop').html(data.html);
                    $(".like_shop .number").html(data.count_like);
                } else {
                    alert("Vui lòng thử lại");
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });
    //Yêu thích shop
    $(document).on("click", ".button-like", function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['shop_id'] = $(this).attr('data-id');
        data['customer_id'] = $(this).attr('data-customer-id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                    $('.like-shop').html(data.html);
                    $(".like_shop .number").html(data.count_like);
                } else {
                    alert("Vui lòng thử lại");
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });

    $(document).on('click', '.clear-all-shop', function () {
        $('.btn_filter_default').click();
        $('.check_category').prop('checked', false);
        $('.check_place').prop('checked', false);
        $('.check_shipping').prop('checked', false);
        $('.check_price_1').val('');
        $('.check_price_2').val('');
        $('.check_type_shop').prop('checked', false);
        $('.check_type_product').prop('checked', false);
        $('.check_rating').prop('click', false);
        $('.check_service').prop('checked', false);
    });

    //xêm thêm
    $('.list_category_shop').hide();
    $('.hide_category_shop').hide();
    $(document).on('click', '.load_category_shop', function () {
        $('.list_category_shop').show();
        $('.load_category_shop').hide();
        $('.hide_category_shop').show();
    });

    $(document).on('click', '.hide_category_shop', function () {
        $('.list_category_shop').hide();
        $('.load_category_shop').show();
        $('.hide_category_shop').hide();
    });

    let url_product = window.location.href;
    let url = url_product + '/sort_related';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: url,
        type: "GET",
        dataType: 'html',
        success: function (data) {
            $(".container-left .container-section-product").append(data);
            $('.paginate-product').appendTo('.sort-container .paginate');
        },
        error: function () {
            console.log('error')
        }
    });
    $(document).on("click", ".btn-sort", function (ev) {
        ev.preventDefault();
        let url = $(this).children().attr("href");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $(".container-left .container-section-product").html(data);
                setTimeout(function () {
                    $('.sort-container .paginate').html('');
                    $('.paginate-product').appendTo('.sort-container .paginate');
                }, 300);
            },
            error: function () {
                console.log('error')
            }
        });
    });
    $(document).on("click", ".btn-fillter", function () {
        $(".btn-fillter").removeClass("active");
        $(this).addClass("active");
        let url = location.href + '/' + $(this).attr("data-url");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'html',
            success: function (data) {
                $(".container-left .container-section-product").html(data);
                setTimeout(function () {
                    $('.sort-container .paginate').html('');
                    $('.paginate-product').appendTo('.sort-container .paginate');
                }, 300);
            },
            error: function () {
                console.log('error')
            }
        });
    });
    let check = $(".slide-suggestion .content").length;
    let checkslide = $(".slide-top-search .content").length;
    let validate_nav_fillter = $('.nav-fillter .sub-nav-fillter').length;
    let validate_slick_image = $('.slick-image .image').length;
    if (check > 6) {
        $(".slide-suggestion").slick({
            speed: 300,
            slidesToShow: 6,
            slidesToScroll: 1,
            nextArrow: "<button class=\"next-arrow btn-arrow btn-arrow-small prev-arrow-sponsor\"><img style=\"width: 7px\" src=\"Icon/home/Danh_muc/next_arrow.png\"></button>",
            prevArrow: "<button class=\"prev-arrow btn-arrow btn-arrow-small next-arrow-sponsor\"><img style=\"width: 7px\" src=\"Icon/home/Danh_muc/prev_arrow.png\"></button>",
        });
    }
    if (validate_nav_fillter > 6) {
        $('.nav-fillter').slick({
            speed: 300,
            slidesToShow: 6,
            slidesToScroll: 1,
            nextArrow: '<button type="button" class="slick-arrow btn-prev"><img src="Icon/seeshop/next_black.png"></button>',
            prevArrow: '<button type="button" class="slick-arrow btn-next"><img src="Icon/seeshop/prev_black.png"></button>',
        });
    }
    if (validate_slick_image > 1) {
        $('.slick-image').slick({
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: "<button type=\"button\" class=\"slick-arrow button-prev\"><img src=\"Icon/seeshop/next_black.png\"></button>",
            prevArrow: "<button type=\"button\" class=\"slick-arrow button-next\"><img src=\"Icon/seeshop/prev_black.png\"></button>",
        });
    }
    if (checkslide >= 6) {
        $('.slide-top-search').slick({
            speed: 300,
            slidesToShow: 6,
            slidesToScroll: 1,
            nextArrow: "<button class=\"next-arrow btn-arrow btn-arrow-small prev-arrow-top-search\"><img style=\"width: 7px\" src=\"Icon/home/Danh_muc/next_arrow.png\"></button>",
            prevArrow: "<button class=\"prev-arrow btn-arrow btn-arrow-small next-arrow-top-search\"><img style=\"width: 7px\" src=\"Icon/home/Danh_muc/prev_arrow.png\"></button>",
        });
    }
});
