$(document).ready(function () {
    $(document).on('click', '.btn-received-code', function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['voucher_id'] = $(this).attr('data-id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                console.log('data', data);
                if (data.status) {
                    $('.btn-save-voucher').removeClass('.btn-main');
                    $('.btn-save-voucher').addClass('.btn-grey');
                    swal("Thành công", data.msg, "success");
                    window.location.href = data.url;
                } else {
                    swal("Lỗi", data.msg, "error");
                    // window.location.href = data.url;
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });
    $(document).on('click', '.btn_save_voucher_ctv_left', function () {
        let url = $(this).attr('data-url');
        let data = {};
        data['voucher_id'] = $(this).attr('data-id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (data) {
                console.log('data', data);
                if (data.status) {
                    swal("Thành công", data.msg, "success");
                    window.location.href = data.url;
                } else {
                    swal("Lỗi", data.msg, "error");
                    // window.location.href = data.url;
                }
            },
            error: function () {
                console.log('error')
            }
        });
    });
});
