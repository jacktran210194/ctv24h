function showTime(){
    var date = new Date();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();

    h = (h < 10) ? "0" + h : h;
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;

    document.getElementById("showtime-h").innerText = h;
    document.getElementById("showtime-h").textContent = h;
    document.getElementById("showtime-m").innerText = m;
    document.getElementById("showtime-m").textContent = m;
    document.getElementById("showtime-s").innerText = s;
    document.getElementById("showtime-s").textContent = s;

    setTimeout(showTime, 1000);

}

showTime();
$(document).ready(function(){
    $('.dropdown-item').click(function(event){
        event.preventDefault();
        var img = $(this).attr('data-src');
        var text = $(this).text();
        $('#dropdownMenuButton .img-nation img').attr('src', img);
        $('#dropdownMenuButton span').text(text);
    });
});
