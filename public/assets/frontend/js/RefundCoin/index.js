$(document).ready(function () {
    $('.slick-voucher').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: false,
        prevArrow:'<button class="btn-prev"> <img src="Icon/RefundCoin/prev.png"> </button>',
        nextArrow:'<button class="btn-next"> <img src="Icon/RefundCoin/next.png"> </button>'
    });
    $(".slide-item").each(function () {
        var length = $(this).children().length;
        if (length > 4){
            $(this).slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                prevArrow:'<button class="btn-prev"> <img src="Icon/RefundCoin/prev.png"> </button>',
                nextArrow:'<button class="btn-next"> <img src="Icon/RefundCoin/next.png"> </button>'
            })
        }
    });
    //-------------------add voucher-----------------//
    $('.btn-get-voucher').click(function () {
        let url = window.location.origin + '/add/voucher/refund/coin';
        let data={};
        data['id'] = $(this).attr('data-value');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           url: url,
           data: data,
           type: 'POST',
           dataType:'json',
           success: function (data) {
               if (data.status){
                   swal('Thành công',data.msg, 'success');
               }else {
                   swal('Thất bại',data.msg, 'error');
               }
           },
            error:function () {
                console.log('error');
            }
        });
    });
});
