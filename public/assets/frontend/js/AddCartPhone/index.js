$(document).ready(function () {
    $('.nav-catagory li').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('.nav-catagory li').removeClass('active');
            $(this).addClass('active');
        }
    });
    $('.type-cart-phone').click(function () {
        let img = $(this).children().attr("src");
        $('.logo-supplier').children().attr("src", img);
    });
    Number.prototype.format = function (n, x) {
        let re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
    };
    $(document).on("click", "#btn-price", function () {
        let price = $(this).attr('data-price');
        let price_format = new Intl.NumberFormat('de-DE', {style: 'currency', currency: 'VND'}).format(price);
        $('.flex-box .price-supplier').text(price_format);
    });

    //mua thẻ đt
    $(document).on('click', '.btn-buy-card', function () {
        let id = $(this).attr('data-id');
        $.get('/recharge-card-phone/filter/' + id, function (data) {
            $('#price_card_phone').html(data);
        })
    });
    $(document).on('click', '.btn-price', function () {
        let price = $(this).attr('data-price');
        $('.btn-checkout-phone').attr('data-price', price);
        $('.btn-confirm-buy-card-phone').attr('data-price', price);
        $('.btn-price').attr('data-checked', 'true');
    });

    $(document).on('click', '.type-cart-phone', function () {
        let category = $(this).attr('data-id');
        let img = $(this).attr('data-img');
        $('.btn-checkout-phone').attr('data-category', category);
        $('.btn-checkout-phone').removeAttr('data-price');
        $('.type-cart-phone').attr('data-checked', 'true');
        $('.btn-price').removeAttr('data-checked');

        $('.btn-confirm-buy-card-phone').attr('data-category', category);
        $('.order-category').attr('src', img);
    });

    $(document).on('click', '.payment', function () {
        let payment = $(this).attr('data-payment');
        $('.btn-confirm-buy-card-phone').attr('data-payment', payment);
    });

    $(document).on('click', '.btn-checkout-phone', function () {
        if (!$('#type-cart-phone').attr('data-checked')) {
            swal("Thất bại", "Vui lòng chọn nhà mạng !", "error");
        } else if (!$('.btn-price').attr('data-checked')) {
            swal("Thất bại", "Vui lòng chọn mệnh giá !", "error");
        } else if (!$('#phone_phone').val() || !$('#email_phone').val()) {
            swal("Thất bại", "Vui lòng nhập đủ thông tin cần thiết !", "error");
        } else {
            let price = $(this).attr('data-price');
            let price_format = new Intl.NumberFormat('de-DE', {style: 'currency', currency: 'VND'}).format(price);
            $('.order-price').text('Mệnh giá: ' + price_format);
            $(".popup-order-information").addClass("show-popup");
        }
    });

    //xác nhận mua thẻ điện thoại
    $(document).on('click', '.btn-confirm-buy-card-phone', function () {
        let payment = $(this).attr('data-payment');
        if (!payment) {
            swal("Thông báo", "Vui lòng chọn hình thức thanh toán !", "error");
        } else {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('customer_id', $(this).attr('data-customer'));
            form_data.append('price', $(this).attr('data-price'));
            form_data.append('category', $(this).attr('data-category'));
            form_data.append('phone', $('#phone_phone').val());
            form_data.append('email', $('#email_phone').val());
            form_data.append('payment', $(this).attr('data-payment'));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                        swal("Thành công", data.msg, "success");
                    } else {
                        swal("Thất bại", data.msg, "error");
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        }
    });

    //Mua thẻ game
    $(document).on('click', '.btn-buy-game', function () {
        let id = $(this).attr('data-id');
        $.get('/recharge-card-phone/filter_game/' + id, function (data) {
            $('#price_card_game').html(data);
        })
    });

    $(document).on("click", ".btn-price-game", function () {
        let price = $(this).attr('data-price_game');
        let price_format = new Intl.NumberFormat('de-DE', {style: 'currency', currency: 'VND'}).format(price);
        $('.flex-box .price-supplier').text(price_format);
    });

    $(document).on('click', '.type-cart-game', function () {
        let category = $(this).attr('data-id');
        let img = $(this).attr('data-img');
        $('.btn-checkout-game').attr('data-category', category);
        $('.type-cart-game').attr('data-checked', 'true');
        $('.btn-price-game').removeAttr('data-checked');
        $('.btn-checkout-game').removeAttr('data-price-game');

        $('.btn-confirm-buy-card-game').attr('data-category', category);
        $('.order-category-game').attr('src', img);
    });

    $(document).on('click', '.btn-price-game', function () {
        let price = $(this).attr('data-price_game');
        $('.btn-checkout-game').attr('data-price-game', price);
        $('.btn-price-game').attr('data-checked', 'true');
        $('.btn-confirm-buy-card-game').attr('data-price', price);

    });

    $(document).on('click', '.payment-game', function () {
        let payment = $(this).attr('data-payment');
        $('.btn-confirm-buy-card-game').attr('data-payment', payment);
    });

    $(document).on('click', '.btn-checkout-game', function () {
        if (!$('.type-cart-game').attr('data-checked')) {
            swal("Thất bại", "Vui lòng chọn nhà mạng !", "error");
        } else if (!$('.btn-price-game').attr('data-checked')) {
            swal("Thất bại", "Vui lòng chọn mệnh giá !", "error");
        } else if (!$('#phone_game').val() || !$('#email_game').val()) {
            swal("Thất bại", "Vui lòng nhập đủ thông tin cần thiết !", "error");
        } else {
            let price = $(this).attr('data-price-game');
            let price_format = new Intl.NumberFormat('de-DE', {style: 'currency', currency: 'VND'}).format(price);
            $('.order-price-game').text('Mệnh giá: ' + price_format);
            $(".popup-order-information-game").addClass("show-popup");
        }
    });

    $(document).on('click', '.btn-confirm-buy-card-game', function () {
        let payment = $(this).attr('data-payment');
        if (!payment) {
            swal("Thông báo", "Vui lòng chọn hình thức thanh toán !", "error");
        } else {
            let url = $(this).attr('data-url');
            let form_data = new FormData();
            form_data.append('customer_id', $(this).attr('data-customer'));
            form_data.append('price', $(this).attr('data-price'));
            form_data.append('category', $(this).attr('data-category'));
            form_data.append('phone', $('#phone_game').val());
            form_data.append('email', $('#email_game').val());
            form_data.append('payment', payment);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: "POST",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading();
                },
                success: function (data) {
                    if (data.status) {
                        window.location.href = data.url;
                        swal("Thành công", data.msg, "success");
                    } else {
                        swal("Thất bại", data.msg, "error");
                    }
                },
                error: function () {
                    console.log('error')
                },
                complete: function () {
                    hideLoading();
                }
            });
        }
    });


    //----------------checked check box ------------//
    $(document).on("click", ".payment .check-box", function () {
        $(".payment").removeClass("checked");
        $(this).parent().addClass("checked");
        $('.content-button').css("display", "block");
    });
    $(document).on("click", ".payment-game .check-box", function () {
        $(".payment-game").removeClass("checked");
        $(this).parent().addClass("checked");
        $('.content-button').css("display", "block");
    });
    $(document).on("click", ".btn-cancel", function () {
        $(".popup-order-information").removeClass("show-popup");
        $(".popup-order-information-game").removeClass("show-popup");
    });

//    + - + -
    $(document).on("click", ".btn-plus-phone", function () {
        let input = parseInt($('#number_phone').val());
        let quantity = input + 1;
        $('#number_phone').val(quantity);
    });

    $(document).on("click", ".btn-devie-phone", function () {
        let input = parseInt($('#number_phone').val());
        let quantity = input - 1;
        $('#number_phone').val(quantity);
        if (quantity < 1) {
            $('#number_phone').val('1');
        }
    });

    $(document).on("click", ".btn-plus-game", function () {
        let input = parseInt($('#number_game').val());
        let quantity = input + 1;
        $('#number_game').val(quantity);
    });

    $(document).on("click", ".btn-devie-game", function () {
        let input = parseInt($('#number_game').val());
        let quantity = input - 1;
        $('#number_game').val(quantity);
        if (quantity < 1) {
            $('#number_game').val('1');
        }
    });
});
