<?php

use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\CardController;
use App\Http\Controllers\Admin\CategoryProductChildController;
use App\Http\Controllers\Admin\Supplier\AuctionSupplierController;
use App\Http\Controllers\Admin\Supplier\ManageCollaboratorController;
use App\Http\Controllers\Admin\Supplier\ProductSupplierController;
use App\Http\Controllers\Admin\Supplier\ProfileSupplierController;
use App\Http\Controllers\Admin\Supplier\ShopSupplierController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\CouponsController;
use App\Http\Controllers\DealsController;
use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\FlashSaleController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InternationalGoodsController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\LoveLyController;
use App\Http\Controllers\MyAuction;
use App\Http\Controllers\MyHistory;
use App\Http\Controllers\PaymentMethodController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\PromotionController;
use App\Http\Controllers\SellWithCTV24;
use App\Http\Controllers\SendMessageController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RankProductController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginUserController;
use App\Http\Controllers\SuggesttionController;
use App\Http\Controllers\MallController;
use App\Models\BankAccountModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\SupplierController;
use App\Http\Controllers\Admin\ShippingController;
use App\Http\Controllers\Admin\CollaboratorController;
use App\Http\Controllers\Admin\AuctionController;
use App\Http\Controllers\Admin\VoucherController;
use App\Http\Controllers\Admin\Collaborator\DashboardCollaboratorController;
use App\Http\Controllers\Admin\Collaborator\LoginCollaboratorController;
use App\Http\Controllers\Admin\Collaborator\WalletCollaboratorController;
use App\Http\Controllers\Admin\Supplier\DashboardSupplierController;
use App\Http\Controllers\Admin\Supplier\LoginSupplierController;
use App\Http\Controllers\Admin\Supplier\VoucherSupplierController;
use App\Http\Controllers\Admin\Supplier\PromotionSupplierController;
use App\Http\Controllers\Admin\Collaborator\ManageSupplierController;
use App\Http\Controllers\Admin\Collaborator\KeySearchCollaboratorController;
use App\Http\Controllers\Admin\Supplier\KeySearchSupplierController;
use App\Http\Controllers\Admin\ShopController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\CategoryProductController;
use App\Http\Controllers\Admin\KeySearchController;
use App\Http\Controllers\Admin\Collaborator\FinancialCollaboratorController;
use App\Http\Controllers\Admin\Collaborator\RevenueCollaboratorController;
use App\Http\Controllers\Admin\Collaborator\OrderCollaboratorController;
use App\Http\Controllers\Admin\Collaborator\ProductCollaboratorController;
use App\Http\Controllers\Admin\Supplier\FinancialSupllierController;
use App\Http\Controllers\Admin\Supplier\WalletSupplierController;
use App\Http\Controllers\Admin\TrendingProductController;
use App\Http\Controllers\Admin\HotKeyController;
use App\Http\Controllers\Admin\SponsoredProductController;
use App\Http\Controllers\Admin\Supplier\TrendingProductSupplierController;
use App\Http\Controllers\SponsoredProductController as SponsoredProductHomePage;
use App\Http\Controllers\Admin\VoucherController as VoucherCTV24H;
use App\Http\Controllers\LoveShopController;
use App\Http\Controllers\FavouriteProductController;
use App\Http\Controllers\Admin\NotificationCTV24Controller;
use App\Http\Controllers\Admin\FeeCTV24HController;
use App\Http\Controllers\Admin\Supplier\MarketingController;
use App\Http\Controllers\Admin\RevenueCTV24HController;
use App\Http\Controllers\Admin\Supplier\PromotionShopController;

//Supplier
use App\Http\Controllers\Admin\Supplier\OrderController as SupplierOrderController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Frontend
|--------------------------------------------------------------------------
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/secondhand', [HomeController::class, 'getSecondhand']);

Route::group(['prefix' => 'cart'], function () {
    Route::get('', [CartController::class, 'index']);
    Route::get('order_delivery_to_ctv', [CartController::class, 'orderDeliveryToCTV']);
    Route::get('order_delivery_customer_ctv', [CartController::class, 'orderDeliveryCustomerCTV']);
    Route::post('/check-address-ctv', [CartController::class, 'getAddress']);
    Route::post('/save_address_customer', [CartController::class, 'createAddressCtv']);
    Route::post('/update/{id}', [CartController::class, 'updateQuantity']);
    Route::get('delete_product/{id}', [CartController::class, 'deleteProduct']);
    Route::post('save', [CartController::class, 'saveCart']);
    Route::post('checkout', [CartController::class, 'checkoutCart']);
    Route::post('check_cart', [CartController::class, 'checkCart']);
    Route::post('check_cart_shop', [CartController::class, 'checkCartShop']);
    Route::post('check_cart_product', [CartController::class, 'checkCartProduct']);
    Route::post('save_order_to_customer_ctv', [CartController::class, 'saveCartCustomerCTV']);
    Route::post('checkout_customer_ctv', [CartController::class, 'checkoutCartCustomerCTV']);
    Route::get('add_info_customer', [CartController::class, 'getModalAddress']);
});
Route::group(['prefix' => 'chat'], function () {
    Route::get('', [ChatController::class, 'index']);
    Route::post('send', [ChatController::class, 'sendMessage']);
    Route::get('messenger/{id}', [ChatController::class, 'messenger']);
    Route::get('messenger/shop/{id}', [ChatController::class, 'messengerShop']);
    Route::get('popup-chat/{id}', [ChatController::class, 'popupChat']);
    Route::get('popup-chat/shop/{id}', [ChatController::class, 'popupChatShop']);
});
Route::group(['prefix' => 'auction'], function () {
    Route::get('/', [AuctionController::class, 'index']);
});
Route::group(['prefix' => 'love_ly'], function () {
    Route::get('/', [LoveLyController::class, 'index']);
    Route::get('/delete_product/{id}', [LoveLyController::class, 'dislike']);
    Route::post('/follow', [\App\Http\Controllers\FollowShopController::class, 'followShop']);
    Route::get('unlike_shop/{id}', [LoveLyController::class, 'unlike_shop']);
});
Route::group(['prefix' => 'my_history'], function () {
    Route::get('/', [MyHistory::class, 'index']);
    Route::get('/by_product', [MyHistory::class, 'by_product']);
    Route::post('/by_product/cancel', [MyHistory::class, 'cancel_order']);
    Route::post('/review-product', [\App\Http\Controllers\ReviewProductController::class, 'review']);
    Route::post('/by_product/check_product', [MyHistory::class, 'check_product']);
    Route::get('/by_product/return_product/{id}', [MyHistory::class, 'return_product']);
    Route::get('/by_product/wait_confirm', [MyHistory::class, 'wait_confirm']);
    Route::get('/by_product/wait_shipping', [MyHistory::class, 'wait_shipping']);
    Route::get('/by_product/shipping', [MyHistory::class, 'shipping']);
    Route::get('/by_product/shipped', [MyHistory::class, 'shipped']);
    Route::get('/by_product/cancel_order', [MyHistory::class, 'cancel']);
    Route::get('/by_product/return_order', [MyHistory::class, 'return_order']);
    Route::get('/by_product/list_order/{id}', [MyHistory::class, 'list_order']);

});
Route::group(['prefix' => 'details_product'], function () {
    Route::get('/{id}', [ProductsController::class, 'index']);
    Route::post('/add_to_cart', [ProductsController::class, 'addToCart']);
    Route::get('/{id}/like', [FavouriteProductController::class, 'like']);
    Route::get('/{id}/dislike', [FavouriteProductController::class, 'dislike']);
    Route::get('/{id}/all_review', [ProductsController::class, 'getAllReview']);
    Route::get('/{id}/review-video-image', [ProductsController::class, 'getDataVideoImageReview']);
    Route::get('/{id}/review-comment', [ProductsController::class, 'getDataCommentReview']);
    Route::get('/{id}/review-one-star', [ProductsController::class, 'getOneStarReview']);
    Route::get('/{id}/review-two-star', [ProductsController::class, 'getTwoStarReview']);
    Route::get('/{id}/review-three-star', [ProductsController::class, 'getThreeStarReview']);
    Route::get('/{id}/review-four-star', [ProductsController::class, 'getFourStarReview']);
    Route::get('/{id}/review-five-star', [ProductsController::class, 'getFiveStarReview']);
    Route::get('/{id}/download-file', [ProductsController::class, 'getImageProduct']);
    Route::post('/add_product_drop_ship', [ProductsController::class, 'addProductDropShip']);
    Route::post('/edit-product-dropship', [ProductsController::class, 'editProductDropship']);
});
Route::get('/add_to_mini_cart', [ProductsController::class, 'add_to_mini_cart']);

Route::group(['prefix' => 'drop-ship'], function () {
    Route::get('', [\App\Http\Controllers\DropShipController::class, 'index']);
    Route::post('add', [\App\Http\Controllers\DropShipController::class, 'addCategory']);
});

Route::get('cart/mini_cart', [ProductsController::class, 'popupMiniCart']);

Route::group(['prefix' => 'search'], function () {
    Route::get('/', [\App\Http\Controllers\SearchController::class, 'index']);
    Route::post('address-ctv', [\App\Http\Controllers\SearchController::class, 'searchAddress']);
});
Route::group(['prefix' => 'category'], function () {
    Route::get('/', [\App\Http\Controllers\CatagoryController::class, 'index']);
});
Route::group(['prefix' => 'ElectronicDevice'], function () {
    Route::get('/{id}', [\App\Http\Controllers\ElectronicDeviceController::class, 'index']);
});

Route::group(['prefix' => 'WelcomeCTV24H'], function () {
    Route::get('/', [\App\Http\Controllers\WelcomeCTV24H::class, 'index']);
});

Route::group(['prefix' => 'seeshop'], function () {
    Route::get('/{shop_id}', [\App\Http\Controllers\SeeShopController::class, 'index']);
    Route::post('/like', [\App\Http\Controllers\LoveShopController::class, 'likeShop']);
    Route::get('/dislike/{id}', [\App\Http\Controllers\LoveShopController::class, 'dislikeShop']);
    Route::post('/follow', [\App\Http\Controllers\FollowShopController::class, 'followShop']);
    Route::get('/unfollow/{id}', [\App\Http\Controllers\FollowShopController::class, 'unfollowShop']);
    Route::get('/{shop_id}/sort_related', [\App\Http\Controllers\SeeShopController::class, 'sortRelated']);
    Route::get('/{shop_id}/sort_new', [\App\Http\Controllers\SeeShopController::class, 'sortNew']);
    Route::get('/{shop_id}/best_seller', [\App\Http\Controllers\SeeShopController::class, 'BestSeller']);
    Route::get('/{shop_id}/price_asc', [\App\Http\Controllers\SeeShopController::class, 'priceASC']);
    Route::get('{shop_id}/category/{cate_id}', [\App\Http\Controllers\SeeShopController::class, 'productCategory']);
});

Route::group(['prefix' => 'CTV24hCart'], function () {
    Route::get('/', [\App\Http\Controllers\CTV24hCartController::class, 'index']);
});

Route::group(['prefix' => 'recharge-card-phone'], function () {
    Route::get('/', [\App\Http\Controllers\AddCartPhoneController::class, 'index']);
    Route::get('filter/{id}', [\App\Http\Controllers\AddCartPhoneController::class, 'filter']);
    Route::post('save_card_phone/', [\App\Http\Controllers\AddCartPhoneController::class, 'saveCardPhone']);
    Route::get('filter_game/{id}', [\App\Http\Controllers\AddCartPhoneController::class, 'filter_game']);
    Route::post('save_card_game/', [\App\Http\Controllers\AddCartPhoneController::class, 'saveCardGame']);

});


Route::group(['prefix' => 'rechargeCard'], function () {
    Route::get('/', [\App\Http\Controllers\AddCartPhoneController::class, 'index']);
});
Route::group(['prefix' => 'recharge-card'], function () {
    Route::get('/', [\App\Http\Controllers\RechargeCardController::class, 'index']);
    Route::post('/update-point', [\App\Http\Controllers\RechargeCardController::class, 'updatePoint']);
});
Route::group(['prefix' => 'notification'], function () {
    Route::get('update_order', [NotificationController::class, 'updateOrder']);
    Route::get('promotion', [NotificationController::class, 'getPromotion']);
    Route::get('update_wallet', [NotificationController::class, 'updateWallet']);
    Route::get('online', [NotificationController::class, 'getOnline']);
    Route::get('update_review', [NotificationController::class, 'updateReview']);
    Route::get('update_floor', [NotificationController::class, 'updateFloor']);
    Route::get('return_review_product/{id}', [NotificationController::class, 'return_review_product']);
    Route::post('review-product/save', [NotificationController::class, 'save']);
});

Route::group(['prefix' => 'user'], function () {
    Route::get('file', [UserController::class, 'getFile']);
    Route::post('file/save', [UserController::class, 'update']);
    Route::get('bank', [UserController::class, 'getBank']);
    Route::get('address', [UserController::class, 'address']);
    Route::get('change_password', [UserController::class, 'change_password']);
    Route::post('change_password/save', [UserController::class, 'savePassword']);
    Route::get('point-wallet', [UserController::class, 'pointWallet']);
    Route::get('change_phone',[UserController::class,'change_phone']);
    Route::post('change_phone/check_pass',[UserController::class,'check_pass']);
    Route::post('change_phone/save',[UserController::class,'save_phone']);

});

Route::group(['prefix' => 'super_sale'], function () {
    Route::get('', [RankProductController::class, 'index']);
});

Route::group(['prefix' => 'promotion'], function () {
    Route::get('', [PromotionController::class, 'index']);
});
Route::group(['prefix' => 'my_auction'], function () {
    Route::get('/', [MyAuction::class, 'index']);
    Route::get('search', [MyAuction::class, 'search']);
    Route::get('guide_auction', [MyAuction::class, 'guideAuction']);
    Route::get('auction_details', [MyAuction::class, 'auctionDetails']);
});

Route::group(['prefix' => 'rank_product'], function () {
    Route::get('', [RankProductController::class, 'index']);
});

Route::group(['prefix' => 'register'], function () {
    Route::get('', [RegisterController::class, 'register']);
    Route::post('save', [RegisterController::class, 'save']);
    Route::get('ncc', [RegisterController::class, 'registerNcc']);
    Route::post('save_ncc', [RegisterController::class, 'saveNCC']);
    Route::post('save_ncc_1', [RegisterController::class, 'saveNCC1']);

    Route::get('ctv', [RegisterController::class, 'registerCtv']);
    Route::post('save_ctv', [RegisterController::class, 'saveCTV']);
    Route::post('save_ctv_1', [RegisterController::class, 'saveCTV1']);
});

Route::group(['prefix' => 'login'], function () {
    Route::get('', [LoginUserController::class, 'login']);
    Route::post('check_login', [LoginUserController::class, 'check']);
});

//Đăng xuất khách hàng
Route::get('logout_customer', [LoginUserController::class, 'logout']);

//Quên mật khẩu khách hàng
Route::group(['prefix' => 'forgot_password'], function () {
    Route::get('', [LoginUserController::class, 'getForgotpassword']);
    Route::post('check_phone', [LoginUserController::class, 'checkPhone']);
    Route::get('new_password', [LoginUserController::class, 'new_password']);
    Route::post('new_password/save', [LoginUserController::class, 'savePassword']);
});


Route::group(['prefix' => 'sell_with_ctv24'], function () {
    Route::get('', [SellWithCTV24::class, 'index']);
});
Route::group(['prefix' => 'deals'], function () {
    Route::get('', [DealsController::class, 'index']);
});
//Route::group(['prefix' => 'coupons'], function () {
//    Route::get('', [CouponsController::class, 'index']);
//});
Route::group(['prefix' => 'international_goods'], function () {
    Route::get('', [InternationalGoodsController::class, 'index']);
});

Route::group(['prefix' => 'coupons'], function () {
    Route::get('', [CouponsController::class, 'index']);
    Route::post('save', [CouponsController::class, 'saveCoupons']);
    Route::post('save_coupons_ctv_left', [CouponsController::class, 'saveCouponsCTVLeft']);
});

Route::group(['prefix' => 'suggestion'], function () {
    Route::get('/', [SuggesttionController::class, 'index']);
});

Route::group(['prefix' => 'flash_sale'], function () {
    Route::get('/', [FlashSaleController::class, 'index']);
});

Route::group(['prefix' => 'mall'], function () {
    Route::get('/', [MallController::class, 'index']);
});

Route::group(['prefix' => 'sponsored_product'], function () {
    Route::get('/', [SponsoredProductHomePage::class, 'index']);
});
Route::get('refund-coin', [\App\Http\Controllers\RefundCoinController::class, 'index']);
Route::post('add/voucher/refund/coin', [\App\Http\Controllers\AddVoucherRefundCoinController::class, 'index']);
/*
|--------------------------------------------------------------------------
| End Frontend
|--------------------------------------------------------------------------
*/

/**
 * Admin
 */
Route::group(['prefix' => 'admin'], function () {
    //Dashboard & Login Controller
    Route::get('', [DashboardController::class, 'index']);
    Route::get('logout', [LoginController::class, 'logout']);
    Route::group(['prefix' => 'login'], function () {
        Route::get('', [LoginController::class, 'index']);
        Route::post('check_login', [LoginController::class, 'login']);
    });

    //refund_coin
    Route::group(['prefix' => 'refund_coin'], function () {
        Route::get('', [\App\Http\Controllers\Admin\CreateRefundCoinController::class, 'index']);
        Route::get('add', [\App\Http\Controllers\Admin\CreateRefundCoinController::class, 'add']);
        Route::post('add/voucher', [\App\Http\Controllers\Admin\CreateRefundCoinController::class, 'addVoucher']);
        Route::get('delete/{id}', [\App\Http\Controllers\Admin\CreateRefundCoinController::class, 'delete']);
    });
    //-----//
    Route::group(['prefix' => 'shop_confirm'], function () {
        Route::get('', [\App\Http\Controllers\Admin\ShopConfirmController::class, 'index']);
        Route::get('detail/{id}', [\App\Http\Controllers\Admin\ShopConfirmController::class, 'detail']);
        Route::post('confirm', [\App\Http\Controllers\Admin\ShopConfirmController::class, 'confirm']);
        Route::post('cancel', [\App\Http\Controllers\Admin\ShopConfirmController::class, 'cancel']);
    });

    // Supplier Controller (NCC)
    Route::group(['prefix' => 'supplier'], function () {
        Route::get('/', [SupplierController::class, 'index']);
        Route::get('add', [SupplierController::class, 'add']);
        Route::post('save', [SupplierController::class, 'save']);
        Route::get('edit/{id}', [SupplierController::class, 'edit']);
        Route::get('delete/{id}', [SupplierController::class, 'delete']);

        Route::group(['prefix' => 'wallet'], function () {
            Route::get('{id}', [SupplierController::class, 'getWallet']);
            Route::get('/{id}/add', [SupplierController::class, 'addBank']);
            Route::post('/{id}/add', [SupplierController::class, 'postAddbank']);
            Route::get('active/{id}', [SupplierController::class, 'active']);
            Route::get('delete/{id}', [SupplierController::class, 'deleteBank']);
        });
    });

    // Shipping Controller
    Route::group(['prefix' => 'shipping'], function () {
        Route::get('/', [ShippingController::class, 'index']);
        Route::get('add', [ShippingController::class, 'add']);
        Route::post('save', [ShippingController::class, 'save']);
        Route::get('edit/{id}', [ShippingController::class, 'edit']);
        Route::get('delete/{id}', [ShippingController::class, 'delete']);
    });

    //Collaborator Controller (CTV)
    Route::group(['prefix' => 'collaborator'], function () {
        Route::get('/', [CollaboratorController::class, 'index']);
        Route::get('add', [CollaboratorController::class, 'add']);
        Route::post('save', [CollaboratorController::class, 'save']);
        Route::get('edit/{id}', [CollaboratorController::class, 'edit']);
        Route::get('delete/{id}', [CollaboratorController::class, 'delete']);
        Route::group(['prefix' => 'supplier'], function () {
            Route::get('', [CollaboratorController::class, 'getSupplier']);
        });
    });

    // Shop Controller
    Route::group(['prefix' => 'shop'], function () {
        Route::get('/', [ShopController::class, 'index']);
        Route::get('add', [ShopController::class, 'add']);
        Route::post('save', [ShopController::class, 'save']);
        Route::get('edit/{id}', [ShopController::class, 'edit']);
        Route::get('delete/{id}', [ShopController::class, 'delete']);
        Route::get('/delete_image/{id}', [ShopController::class, 'delete_image']);
        Route::get('detail/{id}', [ShopController::class, 'detail']);

        Route::group(['prefix' => 'category'], function () {
            Route::get('', [ShopController::class, 'indexCategory']);
            Route::get('add', [ShopController::class, 'addCategory']);
            Route::post('save', [ShopController::class, 'saveCategory']);
            Route::get('edit/{id}', [ShopController::class, 'editCategory']);
            Route::get('delete/{id}', [ShopController::class, 'deleteCategory']);
        });
    });

    //Product controller
    Route::group(['prefix' => 'product'], function () {
        Route::get('/', [ProductController::class, 'index']);
        Route::get('add', [ProductController::class, 'add']);
        Route::post('save', [ProductController::class, 'save']);
        Route::get('edit/{id}', [ProductController::class, 'edit']);
        Route::get('delete/{id}', [ProductController::class, 'delete']);
        Route::get('/delete_image/{id}', [ProductController::class, 'delete_image']);
        Route::get('form_attr', [ProductController::class, 'formAttr']);
        Route::get('form_item', [ProductController::class, 'formItem']);
        Route::get('/delete_item/{id}', [ProductController::class, 'delete_item']);
        Route::get('/delete_attr/{id}', [ProductController::class, 'delete_attr']);
        Route::get('search_product_admin', [ProductController::class, 'search'])->name('search_product_admin');
        Route::get('filter_order/{id}', [ProductController::class, 'filter']);

        Route::group(['prefix' => 'trending'], function () {
            Route::get('', [TrendingProductController::class, 'index']);
            Route::get('add', [TrendingProductController::class, 'add']);
            Route::post('save', [TrendingProductController::class, 'save']);
            Route::get('active/{id}', [TrendingProductController::class, 'active']);
        });
        Route::group(['prefix' => 'sponsored'], function () {
            Route::get('/', [SponsoredProductController::class, 'index']);
            Route::get('add', [SponsoredProductController::class, 'add']);
            Route::post('save', [SponsoredProductController::class, 'save']);
            Route::get('edit/{id}', [SponsoredProductController::class, 'edit']);
            Route::get('delete/{id}', [SponsoredProductController::class, 'delete']);
        });
    });

    //CategoryProduct Controller
    Route::group(['prefix' => 'category_product'], function () {
        Route::get('', [CategoryProductController::class, 'index']);
        Route::get('add', [CategoryProductController::class, 'add']);
        Route::post('save', [CategoryProductController::class, 'save']);
        Route::get('edit/{id}', [CategoryProductController::class, 'edit']);
        Route::get('delete/{id}', [CategoryProductController::class, 'delete']);
    });

    //CategoryProductChild Controller
    Route::group(['prefix' => 'category_product_child'], function () {
        Route::get('', [CategoryProductChildController::class, 'index']);
        Route::get('add', [CategoryProductChildController::class, 'add']);
        Route::post('save', [CategoryProductChildController::class, 'save']);
        Route::get('edit/{id}', [CategoryProductChildController::class, 'edit']);
        Route::get('delete/{id}', [CategoryProductChildController::class, 'delete']);
    });

    //SubCategoryProductChild Controller
    Route::group(['prefix' => 'sub_category_product_child'], function () {
        Route::get('', [\App\Http\Controllers\Admin\SubCategoryProductChildController::class, 'index']);
        Route::get('add', [\App\Http\Controllers\Admin\SubCategoryProductChildController::class, 'add']);
        Route::post('save', [\App\Http\Controllers\Admin\SubCategoryProductChildController::class, 'save']);
        Route::get('edit/{id}', [\App\Http\Controllers\Admin\SubCategoryProductChildController::class, 'edit']);
        Route::get('delete/{id}', [\App\Http\Controllers\Admin\SubCategoryProductChildController::class, 'delete']);
    });

    //Brand Controller
    Route::group(['prefix' => 'brand'], function () {
        Route::get('', [BrandController::class, 'index']);
        Route::get('add', [BrandController::class, 'add']);
        Route::post('save', [BrandController::class, 'save']);
        Route::get('edit/{id}', [BrandController::class, 'edit']);
        Route::get('delete/{id}', [BrandController::class, 'delete']);
    });

    //KeySearch Controller
    Route::group(['prefix' => 'key_search'], function () {
        Route::get('', [KeySearchController::class, 'index']);
        Route::get('delete/{id}', [KeySearchController::class, 'delete']);
    });

    //HotKey Controller
    Route::group(['prefix' => 'hot_key'], function () {
        Route::get('', [HotKeyController::class, 'index']);
        Route::get('add', [HotKeyController::class, 'add']);
        Route::post('save', [HotKeyController::class, 'save']);
        Route::get('edit/{id}', [HotKeyController::class, 'edit']);
        Route::get('delete/{id}', [HotKeyController::class, 'delete']);
    });

    //Voucher Controller
    Route::group(['prefix' => 'voucher'], function () {
        Route::get('', [VoucherCTV24H::class, 'indexVoucher']);
        Route::get('add', [VoucherCTV24H::class, 'addVoucher']);
        Route::post('save', [VoucherCTV24H::class, 'saveVoucher']);
        Route::get('edit/{id}', [VoucherCTV24H::class, 'editVoucher']);
        Route::get('delete/{id}', [VoucherCTV24H::class, 'deleteVoucher']);
        Route::group(['prefix' => 'category'], function () {
            Route::get('', [VoucherCTV24H::class, 'indexCategory']);
            Route::get('add', [VoucherCTV24H::class, 'addCategory']);
            Route::post('save', [VoucherCTV24H::class, 'saveCategory']);
            Route::get('edit/{id}', [VoucherCTV24H::class, 'editCategory']);
            Route::get('delete/{id}', [VoucherCTV24H::class, 'deleteCategory']);
        });
    });

    Route::group(['prefix' => 'card'], function () {
        Route::get('', [\App\Http\Controllers\Admin\CardController::class, 'index']);
        Route::get('add', [CardController::class, 'add']);
        Route::post('save', [CardController::class, 'save']);
        Route::get('edit/{id}', [CardController::class, 'edit']);
        Route::get('delete/{id}', [CardController::class, 'delete']);
        Route::group(['prefix' => 'category'], function () {
            Route::get('', [CardController::class, 'indexCategory']);
            Route::get('add', [CardController::class, 'addCategory']);
            Route::post('save', [CardController::class, 'saveCategory']);
            Route::get('edit/{id}', [CardController::class, 'editCategory']);
            Route::get('delete/{id}', [CardController::class, 'deleteCategory']);
        });
    });

    //NotificationCTV24Controller
    Route::group(['prefix' => 'notification'], function () {
        Route::get('/', [NotificationCTV24Controller::class, 'index']);
        Route::get('add', [NotificationCTV24Controller::class, 'add']);
        Route::post('save', [NotificationCTV24Controller::class, 'save']);
        Route::get('edit/{id}', [NotificationCTV24Controller::class, 'edit']);
        Route::get('delete/{id}', [NotificationCTV24Controller::class, 'delete']);
        Route::get('detail/{id}', [NotificationCTV24Controller::class, 'detail']);
    });

    //Fee CTV24H Controller
    Route::group(['prefix' => 'fee_ctv24h'], function () {
        Route::get('', [FeeCTV24HController::class, 'index']);
        Route::post('save', [FeeCTV24HController::class, 'save']);
    });

    //Doanh thu sàn STC24H
    Route::group(['prefix' => 'revenue_ctv24h'], function () {
        Route::get('/', [RevenueCTV24HController::class, 'index']);
        Route::get('status_all', [RevenueCTV24HController::class, 'statusAll']);
        Route::get('status_revenue', [RevenueCTV24HController::class, 'statusRevenue']);
        Route::get('status_withdrawal', [RevenueCTV24HController::class, 'statusWithdrawal']);
        Route::get('status_refund', [RevenueCTV24HController::class, 'statusRefund']);
        Route::get('status_bonus', [RevenueCTV24HController::class, 'statusBonus']);
    });
});


/**
 *  CTV
 */
Route::get('logout', [LoginController::class, 'logout']);
Route::group(['prefix' => 'admin/collaborator_admin'], function () {
    Route::get('', [DashboardCollaboratorController::class, 'index']);
    Route::get('login', [LoginCollaboratorController::class, 'index']);
    Route::post('check_login', [LoginCollaboratorController::class, 'login']);
});
Route::group(['prefix' => 'admin'], function () {
    //Wallet
    Route::group(['prefix' => 'wallet'], function () {
        Route::get('', [WalletCollaboratorController::class, 'index']);
        Route::get('add', [WalletCollaboratorController::class, 'addBank']);
        Route::post('add', [WalletCollaboratorController::class, 'createBank']);
        Route::get('delete/{id}', [WalletCollaboratorController::class, 'deleteBank']);
        Route::get('link/{id}', [WalletCollaboratorController::class, 'linkBank']);
        Route::get('unlink/{id}', [WalletCollaboratorController::class, 'unlinkBank']);
        Route::get('payment', [WalletCollaboratorController::class, 'payment']);
        Route::get('payment/export', [WalletCollaboratorController::class, 'export']);
    });

    //ManageSupplier Controller
    Route::group(['prefix' => 'manage_supplier'], function () {
        Route::get('', [ManageSupplierController::class, 'index']);
        Route::get('all', [ManageSupplierController::class, 'filterAll']);
        Route::get('selling', [ManageSupplierController::class, 'filterSelling']);
        Route::get('order', [ManageSupplierController::class, 'filterOrder']);
        Route::get('follow', [ManageSupplierController::class, 'filterFollow']);
        Route::get('add', [ManageSupplierController::class, 'add']);
        Route::post('save', [ManageSupplierController::class, 'save']);
        Route::get('edit/{id}', [ManageSupplierController::class, 'edit']);
        Route::get('delete/{id}', [ManageSupplierController::class, 'delete']);
        Route::get('detail/{id}', [ManageSupplierController::class, 'detail']);
        Route::get('export', [ManageSupplierController::class, 'export']);
        Route::get('search_all', [ManageSupplierController::class, 'searchAll'])->name('search_all');
        Route::get('search_pro', [ManageSupplierController::class, 'searchProduct'])->name('search_pro');
    });

    //KeySearch Controller
    Route::group(['prefix' => 'key_search_collaborator'], function () {
        Route::get('', [KeySearchCollaboratorController::class, 'index']);
        Route::get('delete/{id}', [KeySearchCollaboratorController::class, 'delete']);
    });

    //Financial Controller
    Route::group(['prefix' => 'collaborator_financial'], function () {
        Route::group(['prefix' => 'wallet'], function () {
            Route::get('', [FinancialCollaboratorController::class, 'indexWallet']);
            Route::get('payment', [FinancialCollaboratorController::class, 'payment']);
            Route::post('withdrawal', [FinancialCollaboratorController::class, 'createWithdrawal']);
            Route::post('withdrawal/save', [FinancialCollaboratorController::class, 'saveWithdrawal']);
            Route::get('point', [FinancialCollaboratorController::class, 'historyPoint']);
            Route::get('point/change', [FinancialCollaboratorController::class, 'changePoint']);
            Route::post('point/change/save', [FinancialCollaboratorController::class, 'savePoint']);
            Route::get('coin', [FinancialCollaboratorController::class, 'historyCoin']);
            Route::get('coin/change', [FinancialCollaboratorController::class, 'changeCoin']);
            Route::get('payment/export', [FinancialCollaboratorController::class, 'export']);
            Route::get('point/export', [FinancialCollaboratorController::class, 'exportPoint']);
            Route::post('create_bank_account', [FinancialCollaboratorController::class, 'createBanking']);
            Route::post('save_bank_account', [FinancialCollaboratorController::class, 'saveBankAccount']);
            Route::get('delete_bank_account', [FinancialCollaboratorController::class, 'deleteBank']);

            /**
             * Các loại giao dịch gần đây
             */
            //Tất cả
            Route::group(['prefix' => 'status_all'], function () {
                Route::get('/', [FinancialCollaboratorController::class, 'statusAll']);
                Route::post('filter_date_all', [FinancialCollaboratorController::class, 'filterDateStatusAll']);
            });
            //Doanh thu
            Route::group(['prefix' => 'status_revenue'], function () {
                Route::get('/', [FinancialCollaboratorController::class, 'statusRevenue']);
                Route::post('filter_date_revenue', [FinancialCollaboratorController::class, 'filterDateStatusRevenue']);
            });
            //Rút tiền
            Route::group(['prefix' => 'status_withdrawal'], function () {
                Route::get('/', [FinancialCollaboratorController::class, 'statusWithdrawal']);
                Route::post('filter_date_withdrawal', [FinancialCollaboratorController::class, 'filterDateStatusWithdrawal']);
            });
            //Hoàn tiền từ đơn hàng
            Route::group(['prefix' => 'status_refund'], function () {
                Route::get('/', [FinancialCollaboratorController::class, 'statusRefund']);
            });
            //Hoa hồng giới thiệu
            Route::group(['prefix' => 'status_bonus'], function () {
                Route::get('/', [FinancialCollaboratorController::class, 'statusBonus']);
            });
            //Trừ phí quảng cáo
            Route::group(['prefix' => 'status_advertisement'], function () {
                Route::get('/', [FinancialCollaboratorController::class, 'statusAdvertisement']);
            });
            //Điều chỉnh
            Route::group(['prefix' => 'status_adjusted'], function () {
                Route::get('/', [FinancialCollaboratorController::class, 'statusAdjusted']);
            });
        });
        Route::group(['prefix' => 'banking'], function () {
            Route::get('', [FinancialCollaboratorController::class, 'indexBanking']);
            Route::get('add', [FinancialCollaboratorController::class, 'addBanking']);
            Route::get('add/info/{id}', [FinancialCollaboratorController::class, 'infoBankAccount']);
            Route::post('check_card', [FinancialCollaboratorController::class, 'checkBankAccount']);
            Route::get('delete/{id}', [FinancialCollaboratorController::class, 'deleteBank']);
            Route::get('link/{id}', [FinancialCollaboratorController::class, 'linkBank']);
            Route::get('unlink/{id}', [FinancialCollaboratorController::class, 'unlinkBank']);
        });
        Route::group(['prefix' => 'revenue'], function () {
            Route::get('', [FinancialCollaboratorController::class, 'indexRevenue']);
            Route::get('withdrawal', [FinancialCollaboratorController::class, 'getWithdrawal']);
            Route::get('refund', [FinancialCollaboratorController::class, 'getRefund']);
            Route::get('detail/{id}', [FinancialCollaboratorController::class, 'detailRevenue']);
            Route::get('done', [FinancialCollaboratorController::class, 'filterDone']);
            Route::get('waiting', [FinancialCollaboratorController::class, 'filterWaiting']);
            Route::post('filter_date_done', [FinancialCollaboratorController::class, 'filterDateDonePayment']);
            Route::post('filter_date_waiting', [FinancialCollaboratorController::class, 'filterDateWaitingPayment']);
            Route::get('filter_done/{id}', [FinancialCollaboratorController::class, 'filterStatusDone']);
            Route::get('filter_waiting/{id}', [FinancialCollaboratorController::class, 'filterStatusWaiting']);
            Route::get('export_done_payment', [FinancialCollaboratorController::class, 'exportRevenueDonePayment']);
            Route::get('export_waiting_payment', [FinancialCollaboratorController::class, 'exportRevenueWaitingPayment']);
        });
        Route::group(['prefix' => 'payment_setting'], function () {
            Route::get('', [FinancialCollaboratorController::class, 'indexPaymentSetting']);
            Route::post('create_pin_code', [FinancialCollaboratorController::class, 'createPinCode']);
            Route::post('save_pin_code', [FinancialCollaboratorController::class, 'savePinCode']);
        });
    });


//    Route::group(['prefix' => 'rank'],function (){
//        Route::get('', [\App\Http\Controllers\Admin\Collaborator\RankCollaboratorController::class, 'index']);
//    });

    //Order Controller
    Route::group(['prefix' => 'order_collaborator'], function () {
        Route::get('', [OrderCollaboratorController::class, 'index']);
        Route::get('detail/{id}', [OrderCollaboratorController::class, 'detail']);
        Route::post('save', [OrderCollaboratorController::class, 'update']);
        Route::get('filter_order/{id}', [OrderCollaboratorController::class, 'filter']);
        Route::get('search_order_collaborator', [OrderCollaboratorController::class, 'search']);
        Route::post('filter_order_date', [OrderCollaboratorController::class, 'filterOrderDate']);
        Route::get('export', [OrderCollaboratorController::class, 'exportOrder']);

        //filter status
        Route::get('all', [OrderCollaboratorController::class, 'all']);
        Route::get('wait_confirm', [OrderCollaboratorController::class, 'wait_confirm']);
        Route::get('wait_shipping', [OrderCollaboratorController::class, 'wait_shipping']);
        Route::get('shipping', [OrderCollaboratorController::class, 'shipping']);
        Route::get('shipped', [OrderCollaboratorController::class, 'shipped']);
        Route::get('cancel', [OrderCollaboratorController::class, 'cancel']);
        Route::get('return_money', [OrderCollaboratorController::class, 'return_money']);
    });

    //Product Controller
    Route::group(['prefix' => 'collaborator_product'], function () {
        Route::get('/', [ProductCollaboratorController::class, 'indexProduct']);
        Route::post('edit', [ProductCollaboratorController::class, 'editProduct']);
        Route::post('update', [ProductCollaboratorController::class, 'updateProduct']);
        Route::post('active', [ProductCollaboratorController::class, 'activeAndHide']);
//        Route::get('delete/{id}', [ProductCollaboratorController::class, 'deleteProduct']);
//        Route::get('detail/{id}', [ProductCollaboratorController::class, 'detailProduct']);

//        Route::group(['prefix' => 'category'], function () {
//            Route::get('', [ProductCollaboratorController::class, 'indexCategory']);
//            Route::get('add', [ProductCollaboratorController::class, 'addCategory']);
//            Route::post('save', [ProductCollaboratorController::class, 'saveCategory']);
//            Route::get('edit/{id}', [ProductCollaboratorController::class, 'editCategory']);
//            Route::get('delete/{id}', [ProductCollaboratorController::class, 'deleteCategory']);
//        });
    });
    Route::group(['prefix' => 'collaborator_rank'], function () {
        Route::get('', [\App\Http\Controllers\Admin\Collaborator\RankCtvController::class, 'index']);
        Route::post('buy-points', [\App\Http\Controllers\Admin\Collaborator\RankCtvController::class, 'buyPoints']);
        Route::post('/check-wallet', [\App\Http\Controllers\Admin\Collaborator\RankCtvController::class, 'checkRank']);
    });
    Route::group(['prefix' => 'collaborator_shop'], function () {
        Route::get('ratings-shop', [\App\Http\Controllers\Admin\Collaborator\CollaboratorShopController::class, 'ratingShop']);
        Route::get('file-shop', [\App\Http\Controllers\Admin\Collaborator\CollaboratorShopController::class, 'fileShop']);
        Route::group(['prefix' => 'decorate-shop'], function () {
            Route::get('', [\App\Http\Controllers\Admin\Collaborator\CollaboratorShopController::class, 'decorateShop']);
            Route::get('mobile', [\App\Http\Controllers\Admin\Collaborator\CollaboratorShopController::class, 'decorateShopMobile']);
            Route::group(['prefix' => 'desktop'], function () {
                Route::get('', [\App\Http\Controllers\Admin\Collaborator\CollaboratorShopController::class, 'decorateShopDesktop']);
                Route::get('pc', [\App\Http\Controllers\Admin\Collaborator\CollaboratorShopController::class, 'decorateShopPC']);
            });
        });
        Route::get('update-ctv', [\App\Http\Controllers\Admin\Collaborator\CollaboratorShopController::class, 'updateCTV']);
    });
    Route::group(['prefix' => 'collaborator'], function () {
        Route::get('data_sale', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'dataSale']);
        Route::get('my-shop', [\App\Http\Controllers\Admin\Collaborator\MyShopController::class, 'myShop']);
        Route::get('data_sale/access-times', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'accessTimes']);
        Route::group(['prefix' => 'data_sale/products'], function () {
            Route::get('', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'products']);
            Route::get('products-overview', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'productsOverview']);
            Route::get('need-improve', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'needImprove']);
        });
        Route::get('data_sale/revenue', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'revenue']);
        Route::group(['prefix' => 'data_sale/marketing'], function () {
            Route::get('', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'marketing']);
            Route::get('combo-sale', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'comboSale']);
            Route::get('follower-offers', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'followerOffers']);
            Route::get('voucher', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'voucher']);
            Route::get('flash-sale', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'flashSale']);
            Route::get('deal-shock', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'dealShock']);
        });
        Route::get('data_sale/chat', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'chat']);
        Route::get('data_sale/CTV-live/feed', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'CTVLiveFeed']);
        Route::group(['prefix' => 'data_sale/sale-tactician'], function () {
            Route::get('', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'saleTactician']);
            Route::get('competitive-product', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'competitiveProduct']);
            Route::get('top-search', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'topSearch']);
            Route::get('optimize-product', [\App\Http\Controllers\Admin\Collaborator\DataSaleController::class, 'optimizeProduct']);
        });
    });
});


/**
 * NCC
 */
Route::get('logout', [LoginController::class, 'logout']);
Route::group(['prefix' => 'admin/supplier_admin'], function () {
    Route::get('', [DashboardSupplierController::class, 'index']);
    Route::get('login', [LoginSupplierController::class, 'index']);
    Route::post('check_login', [LoginSupplierController::class, 'login']);

});
Route::group(['prefix' => 'admin'], function () {
    //thông báo cập nhật
    Route::get('notification_update_info', [DashboardSupplierController::class, 'notification']);

    //OrderController
    Route::group(['prefix' => 'order'], function () {
        Route::get('', [SupplierOrderController::class, 'index']);
        Route::get('detail/{id}', [SupplierOrderController::class, 'detail']);
        Route::post('save', [SupplierOrderController::class, 'update']);
        Route::post('detail/update', [SupplierOrderController::class, 'update_order']);
        Route::get('search_order', [SupplierOrderController::class, 'search'])->name('search_order');
//        Route::post('confirm_order', [SupplierOrderController::class, 'confirm_order']);
        Route::post('confirm_cancel', [SupplierOrderController::class, 'confirm_cancel']);
        Route::post('filter_order', [SupplierOrderController::class, 'filter_order']);
        Route::get('export', [SupplierOrderController::class, 'export']);

        //filter status
        Route::get('all', [SupplierOrderController::class, 'all']);
        Route::get('wait_confirm', [SupplierOrderController::class, 'wait_confirm']);
        Route::get('wait_shipping', [SupplierOrderController::class, 'wait_shipping']);
        Route::get('shipping', [SupplierOrderController::class, 'shipping']);
        Route::get('shipped', [SupplierOrderController::class, 'shipped']);
        Route::get('cancel', [SupplierOrderController::class, 'cancel']);
        Route::get('return_money', [SupplierOrderController::class, 'return_money']);

        //show popup xác nhận order
        Route::get('show_confirm/{id}', [SupplierOrderController::class, 'show_confirm']);
        //show popup chờ lấy hàng
        Route::get('show_wait_shipping/{id}', [SupplierOrderController::class, 'show_wait_shipping']);

        //confirm trạng thái chờ xác nhận
        Route::post('confirm_order', [SupplierOrderController::class, 'confirm_order']);

        //confirm trạng thái chờ lấy hàng
        Route::post('confirm_wait_shipping', [SupplierOrderController::class, 'confirm_wait_shipping']);

        Route::get('search', [SupplierOrderController::class, 'search_order']);

        //xác nhận đã giao hàng
        Route::post('update_status', [SupplierOrderController::class, 'update_status']);

        //xác nhận lấy hàng thành công
        Route::post('update_shipping', [SupplierOrderController::class, 'update_shipping']);

        Route::get('show_review/{id}', [SupplierOrderController::class, 'show_review']);

    });

    //Voucher Controller
    Route::group(['prefix' => 'my_voucher'], function () {
        Route::get('', [VoucherSupplierController::class, 'indexVoucher']);
        Route::get('add', [VoucherSupplierController::class, 'addVoucher']);
        Route::post('save', [VoucherSupplierController::class, 'saveVoucher']);
        Route::get('edit/{id}', [VoucherSupplierController::class, 'editVoucher']);
        Route::get('delete/{id}', [VoucherSupplierController::class, 'deleteVoucher']);
        Route::group(['prefix' => 'category'], function () {
            Route::get('', [VoucherSupplierController::class, 'indexCategory']);
            Route::get('add', [VoucherSupplierController::class, 'addCategory']);
            Route::post('save', [VoucherSupplierController::class, 'saveCategory']);
            Route::get('edit/{id}', [VoucherSupplierController::class, 'editCategory']);
            Route::get('delete/{id}', [VoucherSupplierController::class, 'deleteCategry']);
        });
    });

    //Promotion Controller
    Route::group(['prefix' => 'promotion'], function () {
        Route::get('', [PromotionSupplierController::class, 'index']);
        Route::get('add', [PromotionSupplierController::class, 'add']);
        Route::post('save', [PromotionSupplierController::class, 'save']);
        Route::get('edit/{id}', [PromotionSupplierController::class, 'edit']);
        Route::get('delete/{id}', [PromotionSupplierController::class, 'delete']);
    });

    //KeySearch Controller
    Route::group(['prefix' => 'key_search_supplier'], function () {
        Route::get('', [KeySearchSupplierController::class, 'index']);
        Route::get('delete/{id}', [KeySearchSupplierController::class, 'delete']);
    });

    //ManageCollaboratorController
    Route::group(['prefix' => 'manage_ctv'], function () {
        Route::get('', [ManageCollaboratorController::class, 'index']);
//        Route::get('detail/{id}', [ManageCollaboratorController::class, 'detail']);
        Route::get('link/{id}', [ManageCollaboratorController::class, 'link']);
        Route::get('search', [ManageCollaboratorController::class, 'search'])->name('search');
        Route::get('export', [ManageCollaboratorController::class, 'exportListCollaborator']);
        Route::post('turn_on', [ManageCollaboratorController::class, 'turnOn']);
        Route::get('filter_rank/{rank}', [ManageCollaboratorController::class, 'filterRank']);
        Route::get('filter_revenue/{sort}', [ManageCollaboratorController::class, 'filterRevenue']);
        Route::get('filter_type/{type}', [ManageCollaboratorController::class, 'filterType']);

        Route::group(['prefix' => 'list_order'], function () {
            Route::get('{id}', [ManageCollaboratorController::class, 'listOrder']);
            Route::get('detail/{id}', [ManageCollaboratorController::class, 'orderDetail']);
        });
        Route::group(['prefix' => 'dropship'], function () {
            Route::get('', [ManageCollaboratorController::class, 'dropship']);
            Route::get('delivery/{id}', [ManageCollaboratorController::class, 'delivery']);
            Route::get('delivery_all', [ManageCollaboratorController::class, 'deliveryAll']);
            Route::get('detail/{id}', [ManageCollaboratorController::class, 'dropshipDetail']);
            Route::get('search_dropship', [ManageCollaboratorController::class, 'searchDropship'])->name('search_dropship');
            Route::get('export_dropship', [ManageCollaboratorController::class, 'exportDropshipCollaborator']);
            Route::group(['prefix' => 'list_order_dropship'], function () {
                Route::get('{id}', [ManageCollaboratorController::class, 'listOrderDropship']);
                Route::get('detail/{id}', [ManageCollaboratorController::class, 'orderDetailDropship']);
            });
        });
    });


    //Financial Controller
    Route::group(['prefix' => 'manage_financial'], function () {
        Route::group(['prefix' => 'wallet'], function () {
//            Route::get('', [FinancialSupllierController::class, 'indexWallet']);
            Route::get('payment', [FinancialSupllierController::class, 'payment']);
            Route::get('payment/export_withdrawal', [FinancialSupllierController::class, 'exportWithdrawal']);
            Route::post('withdrawal/save', [FinancialSupllierController::class, 'saveWithdrawal']);
            Route::post('withdrawal', [FinancialSupllierController::class, 'createWithdrawal']);
            Route::get('point', [FinancialSupllierController::class, 'historyPoint']);
            Route::get('point/change', [FinancialSupllierController::class, 'changePoint']);
            Route::post('point/change/save', [FinancialSupllierController::class, 'savePoint']);
            Route::get('coin', [FinancialSupllierController::class, 'historyCoin']);
            Route::get('coin/change', [FinancialSupllierController::class, 'changeCoin']);
            Route::post('create_bank_account', [FinancialSupllierController::class, 'createBanking']);
            Route::post('save_bank_account', [FinancialSupllierController::class, 'saveBankAccount']);
            Route::post('payment/filter_date_payment', [FinancialSupllierController::class, 'filterDatePayment']);
            Route::get('delete_bank_account', [FinancialSupllierController::class, 'deleteBanking']);

            /**
             * Các loại giao dịch gần đây
             */
            //Tất cả
            Route::group(['prefix' => 'status_all'], function () {
                Route::get('/', [FinancialSupllierController::class, 'indexWallet']);
                Route::post('filter_date_all', [FinancialSupllierController::class, 'filterDateStatusAll']);
            });
            //Doanh thu
            Route::group(['prefix' => 'status_revenue'], function () {
                Route::get('/', [FinancialSupllierController::class, 'statusRevenue']);
                Route::post('filter_date_revenue', [FinancialSupllierController::class, 'filterDateStatusRevenue']);
            });
            //Rút tiền
            Route::group(['prefix' => 'status_withdrawal'], function () {
                Route::get('/', [FinancialSupllierController::class, 'statusWithdrawal']);
                Route::post('filter_date_withdrawal', [FinancialSupllierController::class, 'filterDateStatusWithdrawal']);
            });
            //Hoàn tiền từ đơn hàng
            Route::group(['prefix' => 'status_refund'], function () {
                Route::get('/', [FinancialSupllierController::class, 'statusRefund']);
            });
            //Hoa hồng giới thiệu
            Route::group(['prefix' => 'status_bonus'], function () {
                Route::get('/', [FinancialSupllierController::class, 'statusBonus']);
            });
            //Trừ phí quảng cáo
            Route::group(['prefix' => 'status_advertisement'], function () {
                Route::get('/', [FinancialSupllierController::class, 'statusAdvertisement']);
            });
            //Điều chỉnh
            Route::group(['prefix' => 'status_adjusted'], function () {
                Route::get('/', [FinancialSupllierController::class, 'statusAdjusted']);
            });
        });
        Route::group(['prefix' => 'banking'], function () {
            Route::get('', [FinancialSupllierController::class, 'indexBanking']);
            Route::get('add', [FinancialSupllierController::class, 'addBanking']);
            Route::get('add/info/{id}', [FinancialSupllierController::class, 'infoBankAccount']);
            Route::post('check_card', [FinancialSupllierController::class, 'checkBankAccount']);
//            Route::post('add', [FinancialSupllierController::class, 'createBanking']);
            Route::get('delete/{id}', [FinancialSupllierController::class, 'deleteBanking']);
            Route::get('link/{id}', [FinancialSupllierController::class, 'linkBanking']);
            Route::get('unlink/{id}', [FinancialSupllierController::class, 'unlinkBanking']);
        });
        Route::group(['prefix' => 'revenue'], function () {
            Route::get('', [FinancialSupllierController::class, 'indexRevenue']);
            Route::get('/withdrawal', [FinancialSupllierController::class, 'getWithdrawal']);
            Route::get('/refund', [FinancialSupllierController::class, 'getRefund']);
            Route::get('detail/{id}', [FinancialSupllierController::class, 'detailRevenue']);
            Route::get('done', [FinancialSupllierController::class, 'filterDone']);
            Route::get('waiting', [FinancialSupllierController::class, 'filterWaiting']);
            Route::post('filter_date_done', [FinancialSupllierController::class, 'filterDateDonePayment']);
            Route::post('filter_date_waiting', [FinancialSupllierController::class, 'filterDateWaitingPayment']);
            Route::get('filter_done/{id}', [FinancialSupllierController::class, 'filterStatusDone']);
            Route::get('filter_waiting/{id}', [FinancialSupllierController::class, 'filterStatusWaiting']);
        });
        Route::group(['prefix' => 'payment_setting'], function () {
            Route::get('', [FinancialSupllierController::class, 'indexPaymentSetting']);
            Route::post('create_pin_code', [FinancialSupllierController::class, 'createPinCode']);
            Route::post('save_pin_code', [FinancialSupllierController::class, 'savePinCode']);
        });
    });

    //Profile
    Route::group(['prefix' => 'profile'], function () {
        Route::get('', [\App\Http\Controllers\Admin\Supplier\ProfileSupplierController::class, 'index']);
        Route::post('update', [\App\Http\Controllers\Admin\Supplier\ProfileSupplierController::class, 'update']);
        Route::get('address', [\App\Http\Controllers\Admin\Supplier\ProfileSupplierController::class, 'address']);
        Route::post('address/edit/profile', [\App\Http\Controllers\Admin\Supplier\ProfileSupplierController::class, 'editProfile']);
        Route::post('address/add', [\App\Http\Controllers\Admin\Supplier\ProfileSupplierController::class, 'addAddress']);
//        Route::post('address/edit', [\App\Http\Controllers\Admin\Supplier\ProfileSupplierController::class, 'editAddress']);
        Route::get('get_modal_address', [\App\Http\Controllers\Admin\Supplier\ProfileSupplierController::class, 'getModalAddress']);
        Route::post('select_district', [\App\Http\Controllers\Admin\Supplier\ProfileSupplierController::class, 'district']);
        Route::post('select_ward', [\App\Http\Controllers\Admin\Supplier\ProfileSupplierController::class, 'ward']);
        Route::get('address/delete/{id}', [\App\Http\Controllers\Admin\Supplier\ProfileSupplierController::class, 'delete']);
        Route::get('filter_address', [ProfileSupplierController::class, 'filterAddress']);
        Route::post('address/update', [ProfileSupplierController::class, 'addressUpdate']);
        Route::get('confirm_pass', [ProfileSupplierController::class, 'confirmPass']);
        Route::post('change_phone', [ProfileSupplierController::class, 'changePhone']);
        Route::post('save_phone', [ProfileSupplierController::class, 'savePhone']);

        //Thiết lập shop
        Route::get('setting', [ProfileSupplierController::class, 'setting']);


    });

    //Product Supplier
    Route::group(['prefix' => 'product_supplier'], function () {
        Route::get('/', [\App\Http\Controllers\Admin\Supplier\ProductSupplierController::class, 'index']);
        Route::get('add', [\App\Http\Controllers\Admin\Supplier\ProductSupplierController::class, 'add']);
        Route::post('save', [\App\Http\Controllers\Admin\Supplier\ProductSupplierController::class, 'save']);
        Route::get('edit/{id}', [ProductSupplierController::class, 'edit']);
        Route::post('edit/{id}/update',[ProductSupplierController::class, 'update']);
        Route::get('delete/{id}', [ProductSupplierController::class, 'delete']);
        Route::post('hide-or-active/{id}', [ProductSupplierController::class, 'hideAndActive']);
        Route::get('/delete_image/{id}', [ProductSupplierController::class, 'delete_image']);
        Route::get('form_attr', [ProductSupplierController::class, 'formAttr']);
        Route::get('form_item', [ProductSupplierController::class, 'formItem']);
        Route::get('/delete_item/{id}', [ProductSupplierController::class, 'delete_item']);
        Route::get('/delete_attr/{id}', [ProductSupplierController::class, 'delete_attr']);
        Route::get('search_product', [ProductSupplierController::class, 'search'])->name('search_product');
        Route::get('filter_order/{id}', [ProductSupplierController::class, 'filter']);
        Route::group(['prefix' => 'trending'], function () {
            Route::get('', [TrendingProductSupplierController::class, 'index']);
            Route::get('add', [TrendingProductSupplierController::class, 'add']);
            Route::post('save', [TrendingProductSupplierController::class, 'save']);
            Route::get('active/{id}', [TrendingProductSupplierController::class, 'active']);
        });
        Route::post('/category_child', [ProductSupplierController::class, 'getCategoryChildProduct']);
        Route::post('/category', [ProductSupplierController::class, 'getCategoryProduct']);
    });

    //Wallet
    Route::group(['prefix' => 'manage_wallet'], function () {
        Route::get('', [WalletSupplierController::class, 'index']);
        Route::get('add', [WalletSupplierController::class, 'addBank']);
        Route::post('add', [WalletSupplierController::class, 'createBank']);
        Route::get('delete/{id}', [WalletSupplierController::class, 'deleteBank']);
        Route::get('link/{id}', [WalletSupplierController::class, 'linkBank']);
        Route::get('unlink/{id}', [WalletSupplierController::class, 'unlinkBank']);
        Route::get('payment', [WalletSupplierController::class, 'payment']);
    });

    //data sale
    Route::group(['prefix' => 'data_sale'], function () {
        Route::get('', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'index']);
        Route::get('access-times', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'accessTimes']);
        Route::get('products', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'products']);
        Route::get('products-overview', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'productsOverview']);
        Route::get('marketing', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'marketing']);
        Route::get('need-improve', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'needImprove']);
        Route::get('revenue', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'revenue']);
        Route::get('combo-sale', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'comboSale']);
        Route::get('follower-offers', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'followerOffers']);
        Route::get('voucher', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'voucher']);
        Route::get('flash-sale', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'flashSale']);
        Route::get('deal-shock', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'dealShock']);
        Route::get('CTV-live/feed', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'CTVlifeFeed']);
        Route::get('sale-tactician ', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'saleTactician']);
        Route::get('chat', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'chat']);
        Route::group(['prefix' => 'sale-tactician'], function () {
            Route::get('', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'saleTactician']);
            Route::get('competitive-product', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'competitiveProduct']);
            Route::get('top-search', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'topSearch']);
            Route::group(['prefix' => 'optimize-product'], function () {
                Route::get('', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'optimizeProduct']);
                Route::get('fix-brand-product', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'fixBrandProduct']);
                Route::get('fix-desc-product', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'fixDescProduct']);
                Route::get('fix-image-product', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'fixImgProduct']);
                Route::get('fix-variant-product', [\App\Http\Controllers\Admin\Supplier\DataSaleController::class, 'fixVariantProduct']);
            });
        });
    });
    Route::group(['prefix' => 'shop_supplier'], function () {
        Route::get('/', [\App\Http\Controllers\Admin\Supplier\ShopSupplierController::class, 'index']);
        Route::post('save', [ShopSupplierController::class, 'save']);
        Route::get('edit/{id}', [ShopSupplierController::class, 'edit']);
        Route::get('/delete_image/{id}', [ShopSupplierController::class, 'delete_image']);
        Route::get('detail/{id}', [ShopSupplierController::class, 'detail']);

        //review shop
        Route::get('review', [ShopSupplierController::class, 'review']);
        Route::post('review/reply', [ShopSupplierController::class, 'reply']);

        //get data ajax
        Route::get('review/all_star', [ShopSupplierController::class, 'all_star']);
        Route::get('review/1_star', [ShopSupplierController::class, 'one_star']);
        Route::get('review/2_star', [ShopSupplierController::class, 'two_star']);
        Route::get('review/3_star', [ShopSupplierController::class, 'three_star']);
        Route::get('review/4_star', [ShopSupplierController::class, 'four_star']);
        Route::get('review/5_star', [ShopSupplierController::class, 'five_star']);

        //Cập nhật thông tin để bán hàng
        Route::get('update_info', [ShopSupplierController::class, 'update_info']);
        Route::post('update_info/save', [ShopSupplierController::class, 'save_info']);

        Route::group(['prefix' => 'category'], function () {
            Route::get('', [ShopSupplierController::class, 'indexCategory']);
            Route::get('add', [ShopSupplierController::class, 'addCategory']);
            Route::post('save', [ShopSupplierController::class, 'saveCategory']);
            Route::get('edit/{id}', [ShopSupplierController::class, 'editCategory']);
            Route::get('delete/{id}', [ShopSupplierController::class, 'deleteCategory']);
            Route::post('display', [ShopSupplierController::class, 'displayCategory']);
            Route::get('detail/{id}', [ShopSupplierController::class, 'detail']);
            Route::post('add_product', [ShopSupplierController::class, 'add_product']);
            Route::get('delete_product/{id}', [ShopSupplierController::class, 'delete_product']);
            Route::get('search_product_category', [ShopSupplierController::class, 'search_product_category'])->name('search_product_category');
            Route::post('filter_product', [ShopSupplierController::class, 'filterProduct']);
        });
    });

    Route::group(['prefix' => 'shipping_supplier'], function () {
        Route::get('', [\App\Http\Controllers\Admin\Supplier\ShippingSupplierController::class, 'index']);
        Route::post('save', [\App\Http\Controllers\Admin\Supplier\ShippingSupplierController::class, 'save']);
        Route::get('detail/{id}', [\App\Http\Controllers\Admin\Supplier\ShippingSupplierController::class, 'detail']);

    });

    //Marketing Controller
    Route::group(['prefix' => 'marketing_supplier'], function () {
        Route::get('', [MarketingController::class, 'index']);
        Route::group(['prefix' => 'ctv24_programs'], function () {
            Route::get('', [MarketingController::class, 'ctv24Program']);
            Route::get('register', [MarketingController::class, 'register']);
            Route::get('register/product', [MarketingController::class, 'registerProduct']);
            Route::get('register/product/add', [MarketingController::class, 'addProduct']);
        });
        Route::group(['prefix' => 'ctv24_voucher'], function () {
            Route::get('', [MarketingController::class, 'ctv24Voucher']);
            Route::get('detail', [MarketingController::class, 'detailVoucherCTV24']);
            Route::group(['prefix' => 'register'], function () {
                Route::get('/', [MarketingController::class, 'registerVoucher']);
                Route::get('create_code_available', [MarketingController::class, 'createCodeAvailable']);
                Route::get('create_new_code', [MarketingController::class, 'createNewCode']);
                Route::post('create_new_code/save_new_code', [MarketingController::class, 'saveNewCode']);
            });
        });
        Route::group(['prefix' => 'my_voucher'], function () {
            Route::get('', [MarketingController::class, 'indexMyVoucher']);
            Route::get('add', [MarketingController::class, 'addMyVoucher']);
            Route::get('order', [MarketingController::class, 'orderMyVoucher']);
            Route::get('detail/{id}', [MarketingController::class, 'detailMyVoucher']);
            Route::get('all', [MarketingController::class, 'getAllMyVoucher']);
            Route::get('being', [MarketingController::class, 'getBeingMyVoucher']);
            Route::get('coming_up', [MarketingController::class, 'getComingUpMyVoucher']);
            Route::get('finish', [MarketingController::class, 'getFinishMyVoucher']);
            Route::post('save', [MarketingController::class, 'saveMyVoucher']);
            Route::post('finish_soon', [MarketingController::class, 'finishMyVoucher']);
            Route::post('search_product', [MarketingController::class, 'searchProduct']);
        });
        Route::group(['prefix' => 'offer_follower'], function () {
            Route::get('', [MarketingController::class, 'indexOfferFollower']);
            Route::get('add', [MarketingController::class, 'addOfferFollower']);
            Route::get('detail', [MarketingController::class, 'detailOfferFollower']);
        });
        Route::group(['prefix' => 'promotion_combo'], function () {
            Route::get('', [MarketingController::class, 'indexPromotionCombo']);
            Route::get('add', [MarketingController::class, 'addPromotionCombo']);
            Route::post('order_and_data', [MarketingController::class, 'orderAndData']);
        });

        //flash sale shop
        Route::group(['prefix' => 'flash_sale'], function () {
            Route::get('', [\App\Http\Controllers\Admin\Supplier\FlashSaleSupplierController::class, 'index']);
            Route::get('add_flash_sale', [\App\Http\Controllers\Admin\Supplier\FlashSaleSupplierController::class, 'add_flash_sale']);
            Route::get('add_product_flash_sale', [\App\Http\Controllers\Admin\Supplier\FlashSaleSupplierController::class, 'add_product_flash_sale']);

            //Ajax get view
            Route::get('get_list_product', [\App\Http\Controllers\Admin\Supplier\FlashSaleSupplierController::class, 'get_list_product']);
            Route::get('get_import_product', [\App\Http\Controllers\Admin\Supplier\FlashSaleSupplierController::class, 'get_import_product']);
        });

        //flash sale shop
        Route::group(['prefix' => 'auction'], function () {
            Route::get('', [\App\Http\Controllers\Admin\Supplier\AuctionSupplierController::class, 'index']);
            Route::get('register', [AuctionSupplierController::class, 'register']);
            Route::get('register/product', [AuctionSupplierController::class, 'product']);

            Route::group(['prefix' => 'auction_my'], function () {
                Route::get('', [AuctionSupplierController::class, 'auction_my']);
            });
        });

        //Promotion shop
        Route::group(['prefix' => 'promotion'], function () {
            Route::get('', [PromotionShopController::class, 'index']);
            Route::get('add', [PromotionShopController::class, 'add']);
            Route::post('add_product', [PromotionShopController::class, 'addProduct']);
            Route::get('delete_product/{id}', [PromotionShopController::class, 'deleteProduct']);
            Route::post('save_promotion', [PromotionShopController::class, 'savePromotion']);
            Route::get('all', [PromotionShopController::class, 'getAllPromotion']);
            Route::get('being', [PromotionShopController::class, 'getBeingPromotion']);
            Route::get('coming_up', [PromotionShopController::class, 'getComingUpPromotion']);
            Route::get('finish', [PromotionShopController::class, 'getFinishPromotion']);
        });

        Route::group(['prefix' => 'deal_shock'], function () {
            Route::get('', [MarketingController::class, 'indexDealShock']);
            Route::get('add', [MarketingController::class, 'createDealShock']);
            Route::get('add_product', [MarketingController::class, 'addProductDealShock']);
        });
        Route::group(['prefix' => 'top_best_seller'], function () {
            Route::get('', [MarketingController::class, 'indexTopBestSeller']);
            Route::get('create', [MarketingController::class, 'createTopBestSeller']);
            Route::get('add_product', [MarketingController::class, 'addProductTopBestSeller']);
        });
        Route::group(['prefix' => 'same_price'], function () {
            Route::get('', [MarketingController::class, 'indexSamePrice']);
            Route::get('register', [MarketingController::class, 'registerSamePrice']);
            Route::get('add_product', [MarketingController::class, 'addProductSamePrice']);
        });
    });

    //Introduction Controller
    Route::group(['prefix' => 'introduction'], function () {
        Route::get('member', [\App\Http\Controllers\Admin\Supplier\IntroductionController::class, 'member']);
        Route::get('product', [\App\Http\Controllers\Admin\Supplier\IntroductionController::class, 'product']);
    });
});

Route::get('get_modal_address', [UserController::class, 'getModalAddress']);
Route::get('filter_address', [UserController::class, 'filterAddress']);
Route::post('address/save', [UserController::class, 'saveAddress']);
Route::get('address/delete/{id}', [UserController::class, 'deleteAddress']);
Route::get('address/default/{id}', [UserController::class, 'defaultAddress']);
Route::post('address/default_ajax', [UserController::class, 'defaultAjax']);

Route::group(['prefix' => 'voucher'], function () {
    Route::get('/', [\App\Http\Controllers\VoucherController::class, 'index']);
});
Route::group(['prefix' => 'address-customer'], function () {
    Route::get('', [\App\Http\Controllers\AllAddressCustomer::class, 'index']);
});
Route::group(['prefix' => 'refund-coin'], function () {
    Route::get('', [\App\Http\Controllers\RefundCoinController::class, 'index']);
});
Route::group(['prefix' => 'detail-order'], function () {
    Route::get('', [\App\Http\Controllers\DetailOrderController::class, 'index']);
});
Route::group(['prefix' => 'cancel-order'], function () {
    Route::get('', [\App\Http\Controllers\CancelOrderController::class, 'index']);
});

Route::group(['prefix' => 'admin/delivery'], function () {
    Route::get('ghtk', [DeliveryController::class, 'ghtk']);
    Route::post('ghtk/save', [DeliveryController::class, 'ghtk_save']);
    Route::post('ghtk/connect', [DeliveryController::class, 'ghtk_connect']);
    Route::get('j_and_t', [DeliveryController::class, 'j_and_t']);
    Route::post('j_and_t/save', [DeliveryController::class, 'j_and_t_save']);
});

Route::group(['prefix' => 'admin/payment_method'], function () {
    Route::get('zalo_pay', [PaymentMethodController::class, 'zalo_pay']);
    Route::post('zalo_pay/save', [PaymentMethodController::class, 'zalo_pay_save']);
    Route::post('zalo_pay/connect', [PaymentMethodController::class, 'zalo_pay_connect']);
    Route::get('momo', [PaymentMethodController::class, 'momo']);
    Route::post('momo/save', [PaymentMethodController::class, 'momo_save']);
});

Route::group(['prefix' => 'search_order'], function () {
    Route::get('', [\App\Http\Controllers\HomeController::class, 'check_order']);
});

//List city
Route::post('select_district', [CartController::class, 'district']);
Route::post('select_ward', [CartController::class, 'ward']);

//Login now NCC & CTV
Route::get('admin/supplier_admin/', [LoginController::class, 'login_ncc']);
Route::get('admin/collaborator_admin/', [LoginController::class, 'login_ctv']);
Route::get('filter_category', [\App\Http\Controllers\FilterCategoryController::class, 'index']);
Route::get('filter_product_shop', [\App\Http\Controllers\FilterProductShopController::class, 'index']);
// API update image
Route::post('update-image',[\App\Http\Controllers\UpdateFileImageController::class, 'uploadFile']);
Route::post('update-image-variant',[\App\Http\Controllers\UpdateFileImageController::class, 'uploadFileVariant']);
Route::post('add-delete-variant',[ProductSupplierController::class, 'deleteVariantAndValueProduct']);
Route::post('check-price-percent',[ProductsController::class, 'checkPricePercent']);





